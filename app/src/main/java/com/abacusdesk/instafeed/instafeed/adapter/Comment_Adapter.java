package com.abacusdesk.instafeed.instafeed.adapter;

import android.app.Dialog;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.abacusdesk.instafeed.instafeed.R;
import com.abacusdesk.instafeed.instafeed.Util.SaveSharedPreference;
import com.abacusdesk.instafeed.instafeed.fragments.Explore_detailfragment;
import com.abacusdesk.instafeed.instafeed.fragments.Newsdetail_fragment;
import com.abacusdesk.instafeed.instafeed.fragments.Opiniondetail_fragment;
import com.abacusdesk.instafeed.instafeed.fragments.Petitiondetail_fragment;
import com.abacusdesk.instafeed.instafeed.fragments.Talentdetail_fragment;
import com.abacusdesk.instafeed.instafeed.model.NewsDetail;
import com.bumptech.glide.Glide;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by abacusdesk on 2017-07-15.
 */

public class Comment_Adapter  extends BaseAdapter {

    Context context;
    ArrayList<HashMap<String,String>> arrayList;
    public static String comment="",comment_id;
    Dialog dialog;
    String type;
    public Comment_Adapter(Context context, ArrayList<HashMap<String,String>> arrayList,String type){
        this.context=context;
        this.arrayList=arrayList;
        this.type=type;
        notifyDataSetChanged();
    }
    @Override
    public int getCount() {
        return arrayList.size();
    }

    @Override
    public Object getItem(int position) {
        return position;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        LayoutInflater layoutInflater = LayoutInflater.from(context);
        convertView = layoutInflater.inflate(R.layout.item_rowlist, null, false);

        ImageView imgprofile=(ImageView)convertView.findViewById(R.id.img_profilepic);
        ImageView imgedit=(ImageView)convertView.findViewById(R.id.img_edit);
        TextView txtname=(TextView)convertView.findViewById(R.id.txt_name);
        TextView txtcomment=(TextView)convertView.findViewById(R.id.txt_comment);
        txtcomment.setText(arrayList.get(position).get("comment"));
        txtname.setText(arrayList.get(position).get("username"));
        if (arrayList.get(position).get("avatar")!=null && !arrayList.get(position).get("avatar").isEmpty()){
            Glide.with(context).load(arrayList.get(position).get("avatar")).error(R.drawable.user).into(imgprofile);
        }else {
            imgprofile.setImageResource(R.drawable.user);
        }
        if (arrayList.get(position).get("userid").equals(SaveSharedPreference.getUserID(context))){
            imgedit.setVisibility(View.VISIBLE);
        }else {
            imgedit.setVisibility(View.INVISIBLE);
        }
        imgedit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                comment=arrayList.get(position).get("comment");
                comment_id=arrayList.get(position).get("commentid");
                //edit_dialog();
            }
        });
        return convertView;
    }

/*    public void edit_dialog() {
        dialog = new Dialog(context);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(true);
        dialog.setContentView(R.layout.dialog_comment);
        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        lp.copyFrom(dialog.getWindow().getAttributes());
        lp.width = WindowManager.LayoutParams.MATCH_PARENT;
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
        dialog.getWindow().setAttributes(lp);
        final EditText edcomment=(EditText)dialog.findViewById(R.id.ed_comment);
        final Button btn_edit=(Button)dialog.findViewById(R.id.btn_edit);
        edcomment.setText(comment);
        btn_edit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                comment=edcomment.getText().toString();
                if (comment!=""){
                    if (type.equalsIgnoreCase("explore")){
                        Explore_detailfragment.editComment(context,comment,comment_id);
                        dialog.dismiss();
                    } else if (type.equalsIgnoreCase("opinions")){
                        Opiniondetail_fragment.editComment(context,comment,comment_id);
                        dialog.dismiss();
                    }  else if (type.equalsIgnoreCase("petitions")){
                        Petitiondetail_fragment.editComment(context,comment,comment_id);
                        dialog.dismiss();
                    }else {
                       // Talentdetail_fragment.editComment(context,comment,comment_id);
                        dialog.dismiss();
                    }
                }
            }
        });
        dialog.show();
    }*/

}

