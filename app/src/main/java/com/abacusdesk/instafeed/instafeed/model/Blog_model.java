package com.abacusdesk.instafeed.instafeed.model;

import java.util.List;

/**
 * Created by ishaan on 6/21/2017.
 */

public class Blog_model {

    /**
     * status : 200
     * message : success
     * data : [{"id":"64","title":"Is Ad-Fraud a Threat to Digital World?","slug":"is-adfraud-a-threat-to-digital-world-1847075926","short_description":"Ad fraud is defined as the deliberate practice of attempting to serve ads that have no potential to be viewed by a human user.","image":"http://instafeed.org/storage/blog_banner/256x170-102dc19f9117465f6dd27d88fb841533.jpg","image_zoom":"http://instafeed.org/storage/blog_banner/748x420-102dc19f9117465f6dd27d88fb841533.jpg","image_original":"http://instafeed.org/storage/blog_banner/102dc19f9117465f6dd27d88fb841533.jpg","image_330x210":"http://instafeed.org/storage/blog_banner/330x210-102dc19f9117465f6dd27d88fb841533.jpg","banner_80x80":"http://instafeed.org/storage/blog_banner/80x80-102dc19f9117465f6dd27d88fb841533.jpg","image_264x200":"http://instafeed.org/storage/blog_banner/264x200-102dc19f9117465f6dd27d88fb841533.jpg","image_748x420":"http://instafeed.org/storage/blog_banner/748x420-102dc19f9117465f6dd27d88fb841533.jpg","image_360x200":"http://instafeed.org/storage/blog_banner/360x200-102dc19f9117465f6dd27d88fb841533.jpg","image_256x170":"http://instafeed.org/storage/blog_banner/256x170-102dc19f9117465f6dd27d88fb841533.jpg","image_100x100":"http://instafeed.org/storage/blog_banner/100x100-102dc19f9117465f6dd27d88fb841533.jpg","tags":null,"blog_category_id":"7","total_comments":"1","total_views":"2309","user_id":"2","dt_added":"2017-05-10 12:05:42","dt_modified":"2017-07-22 13:11:35","status":"P"},{"id":"52","title":"Traditional arts of Middle Eastern countries","slug":"traditional-arts-of-middle-eastern-countries-2031711010","short_description":"Different art forms of Middle Eastern countries ","image":"http://instafeed.org/storage/blog_banner/256x170-e9cf82b95bfd5da724e4958da25d864d.jpg","image_zoom":"http://instafeed.org/storage/blog_banner/748x420-e9cf82b95bfd5da724e4958da25d864d.jpg","image_original":"http://instafeed.org/storage/blog_banner/e9cf82b95bfd5da724e4958da25d864d.jpg","image_330x210":"http://instafeed.org/storage/blog_banner/330x210-e9cf82b95bfd5da724e4958da25d864d.jpg","banner_80x80":"http://instafeed.org/storage/blog_banner/80x80-e9cf82b95bfd5da724e4958da25d864d.jpg","image_264x200":"http://instafeed.org/storage/blog_banner/264x200-e9cf82b95bfd5da724e4958da25d864d.jpg","image_748x420":"http://instafeed.org/storage/blog_banner/748x420-e9cf82b95bfd5da724e4958da25d864d.jpg","image_360x200":"http://instafeed.org/storage/blog_banner/360x200-e9cf82b95bfd5da724e4958da25d864d.jpg","image_256x170":"http://instafeed.org/storage/blog_banner/256x170-e9cf82b95bfd5da724e4958da25d864d.jpg","image_100x100":"http://instafeed.org/storage/blog_banner/100x100-e9cf82b95bfd5da724e4958da25d864d.jpg","tags":null,"blog_category_id":"5","total_comments":null,"total_views":"287","user_id":"2","dt_added":"2017-04-28 11:30:45","dt_modified":"2017-07-22 12:58:50","status":"P"},{"id":"50","title":"GOLDEN TEMPLE- SHRINE FOR HUMANITY","slug":"golden-temple-shrine-for-humanity-902398593","short_description":"Golden temple, Amritsar","image":"http://instafeed.org/storage/blog_banner/256x170-295954d7dfafe569bc8eaddbb2175706.jpg","image_zoom":"http://instafeed.org/storage/blog_banner/748x420-295954d7dfafe569bc8eaddbb2175706.jpg","image_original":"http://instafeed.org/storage/blog_banner/295954d7dfafe569bc8eaddbb2175706.jpg","image_330x210":"http://instafeed.org/storage/blog_banner/330x210-295954d7dfafe569bc8eaddbb2175706.jpg","banner_80x80":"http://instafeed.org/storage/blog_banner/80x80-295954d7dfafe569bc8eaddbb2175706.jpg","image_264x200":"http://instafeed.org/storage/blog_banner/264x200-295954d7dfafe569bc8eaddbb2175706.jpg","image_748x420":"http://instafeed.org/storage/blog_banner/748x420-295954d7dfafe569bc8eaddbb2175706.jpg","image_360x200":"http://instafeed.org/storage/blog_banner/360x200-295954d7dfafe569bc8eaddbb2175706.jpg","image_256x170":"http://instafeed.org/storage/blog_banner/256x170-295954d7dfafe569bc8eaddbb2175706.jpg","image_100x100":"http://instafeed.org/storage/blog_banner/100x100-295954d7dfafe569bc8eaddbb2175706.jpg","tags":null,"blog_category_id":"2","total_comments":"1","total_views":"954","user_id":"2","dt_added":"2017-04-21 14:49:34","dt_modified":"2017-07-22 12:57:54","status":"P"}]
     */

    private int status;
    private String message;
    private List<DataBean> data;

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public List<DataBean> getData() {
        return data;
    }

    public void setData(List<DataBean> data) {
        this.data = data;
    }

    public static class DataBean {
        /**
         * id : 64
         * title : Is Ad-Fraud a Threat to Digital World?
         * slug : is-adfraud-a-threat-to-digital-world-1847075926
         * short_description : Ad fraud is defined as the deliberate practice of attempting to serve ads that have no potential to be viewed by a human user.
         * image : http://instafeed.org/storage/blog_banner/256x170-102dc19f9117465f6dd27d88fb841533.jpg
         * image_zoom : http://instafeed.org/storage/blog_banner/748x420-102dc19f9117465f6dd27d88fb841533.jpg
         * image_original : http://instafeed.org/storage/blog_banner/102dc19f9117465f6dd27d88fb841533.jpg
         * image_330x210 : http://instafeed.org/storage/blog_banner/330x210-102dc19f9117465f6dd27d88fb841533.jpg
         * banner_80x80 : http://instafeed.org/storage/blog_banner/80x80-102dc19f9117465f6dd27d88fb841533.jpg
         * image_264x200 : http://instafeed.org/storage/blog_banner/264x200-102dc19f9117465f6dd27d88fb841533.jpg
         * image_748x420 : http://instafeed.org/storage/blog_banner/748x420-102dc19f9117465f6dd27d88fb841533.jpg
         * image_360x200 : http://instafeed.org/storage/blog_banner/360x200-102dc19f9117465f6dd27d88fb841533.jpg
         * image_256x170 : http://instafeed.org/storage/blog_banner/256x170-102dc19f9117465f6dd27d88fb841533.jpg
         * image_100x100 : http://instafeed.org/storage/blog_banner/100x100-102dc19f9117465f6dd27d88fb841533.jpg
         * tags : null
         * blog_category_id : 7
         * total_comments : 1
         * total_views : 2309
         * user_id : 2
         * dt_added : 2017-05-10 12:05:42
         * dt_modified : 2017-07-22 13:11:35
         * status : P
         */

        private String id;
        private String title;
        private String slug;
        private String short_description;
        private String image;
        private String image_zoom;
        private String image_original;
        private String image_330x210;
        private String banner_80x80;
        private String image_264x200;
        private String image_748x420;
        private String image_360x200;
        private String image_256x170;
        private String image_100x100;
        private Object tags;
        private String blog_category_id;
        private String total_comments;
        private String total_views;
        private String user_id;
        private String dt_added;
        private String dt_modified;
        private String status;

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getTitle() {
            return title;
        }

        public void setTitle(String title) {
            this.title = title;
        }

        public String getSlug() {
            return slug;
        }

        public void setSlug(String slug) {
            this.slug = slug;
        }

        public String getShort_description() {
            return short_description;
        }

        public void setShort_description(String short_description) {
            this.short_description = short_description;
        }

        public String getImage() {
            return image;
        }

        public void setImage(String image) {
            this.image = image;
        }

        public String getImage_zoom() {
            return image_zoom;
        }

        public void setImage_zoom(String image_zoom) {
            this.image_zoom = image_zoom;
        }

        public String getImage_original() {
            return image_original;
        }

        public void setImage_original(String image_original) {
            this.image_original = image_original;
        }

        public String getImage_330x210() {
            return image_330x210;
        }

        public void setImage_330x210(String image_330x210) {
            this.image_330x210 = image_330x210;
        }

        public String getBanner_80x80() {
            return banner_80x80;
        }

        public void setBanner_80x80(String banner_80x80) {
            this.banner_80x80 = banner_80x80;
        }

        public String getImage_264x200() {
            return image_264x200;
        }

        public void setImage_264x200(String image_264x200) {
            this.image_264x200 = image_264x200;
        }

        public String getImage_748x420() {
            return image_748x420;
        }

        public void setImage_748x420(String image_748x420) {
            this.image_748x420 = image_748x420;
        }

        public String getImage_360x200() {
            return image_360x200;
        }

        public void setImage_360x200(String image_360x200) {
            this.image_360x200 = image_360x200;
        }

        public String getImage_256x170() {
            return image_256x170;
        }

        public void setImage_256x170(String image_256x170) {
            this.image_256x170 = image_256x170;
        }

        public String getImage_100x100() {
            return image_100x100;
        }

        public void setImage_100x100(String image_100x100) {
            this.image_100x100 = image_100x100;
        }

        public Object getTags() {
            return tags;
        }

        public void setTags(Object tags) {
            this.tags = tags;
        }

        public String getBlog_category_id() {
            return blog_category_id;
        }

        public void setBlog_category_id(String blog_category_id) {
            this.blog_category_id = blog_category_id;
        }

        public String getTotal_comments() {
            return total_comments;
        }

        public void setTotal_comments(String total_comments) {
            this.total_comments = total_comments;
        }

        public String getTotal_views() {
            return total_views;
        }

        public void setTotal_views(String total_views) {
            this.total_views = total_views;
        }

        public String getUser_id() {
            return user_id;
        }

        public void setUser_id(String user_id) {
            this.user_id = user_id;
        }

        public String getDt_added() {
            return dt_added;
        }

        public void setDt_added(String dt_added) {
            this.dt_added = dt_added;
        }

        public String getDt_modified() {
            return dt_modified;
        }

        public void setDt_modified(String dt_modified) {
            this.dt_modified = dt_modified;
        }

        public String getStatus() {
            return status;
        }

        public void setStatus(String status) {
            this.status = status;
        }
    }
}
