package com.abacusdesk.instafeed.instafeed.model;

import java.util.List;

/**
 * Created by abacusdesk on 2017-07-24.
 */

public class Follower_model {

    /**
     * status : 200
     * message : success
     * data : [{"id":"3905","username":"abhishek","first_name":"Abhishek","last_name":"Chauhan","avatar":"https://instafeed.org/storage/avatar/80x80-3844825a5c347c3f039c127d02b9154d.jpg"},{"id":"3436","username":"ayushkathuria254","first_name":"Ayush","last_name":"Kathuria","avatar":"https://instafeed.org/storage/avatar/80x80-default-avatar.jpg"},{"id":"3270","username":"sachinaggarwal6010","first_name":"Sachin","last_name":"Aggarwal","avatar":"https://instafeed.org/storage/avatar/80x80-default-avatar.jpg"},{"id":"5","username":"segitips4u","first_name":"Sagar","last_name":"ARORA","avatar":"https://instafeed.org/storage/avatar/80x80-1f069d56ecce23433d4126aaf7b8719f.jpg"}]
     */

    private int status;
    private String message;
    private List<DataBean> data;

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public List<DataBean> getData() {
        return data;
    }

    public void setData(List<DataBean> data) {
        this.data = data;
    }

    public static class DataBean {
        /**
         * id : 3905
         * username : abhishek
         * first_name : Abhishek
         * last_name : Chauhan
         * avatar : https://instafeed.org/storage/avatar/80x80-3844825a5c347c3f039c127d02b9154d.jpg
         */

        private String id;
        private String username;
        private String first_name;
        private String last_name;
        private String avatar;

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getUsername() {
            return username;
        }

        public void setUsername(String username) {
            this.username = username;
        }

        public String getFirst_name() {
            return first_name;
        }

        public void setFirst_name(String first_name) {
            this.first_name = first_name;
        }

        public String getLast_name() {
            return last_name;
        }

        public void setLast_name(String last_name) {
            this.last_name = last_name;
        }

        public String getAvatar() {
            return avatar;
        }

        public void setAvatar(String avatar) {
            this.avatar = avatar;
        }
    }
}
