package com.abacusdesk.instafeed.instafeed.fontsClass;

import android.content.Context;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.widget.TextView;

/**
 * Created by abacusdesk on 2017-09-01.
 */

    public class CustomLightText  extends TextView {

        public CustomLightText(Context context, AttributeSet attrs, int defStyle) {
            super(context, attrs, defStyle);
            init();
        }

        public CustomLightText(Context context, AttributeSet attrs) {
            super(context, attrs);
            init();
        }

        public CustomLightText(Context context) {
            super(context);
            init();
        }

        public void init() {
            Typeface tf = Typeface.createFromAsset(getContext().getAssets(),"fonts/Roboto-Light.ttf");
            setTypeface(tf);

        }

    }
