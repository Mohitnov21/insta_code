package com.abacusdesk.instafeed.instafeed.fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.abacusdesk.instafeed.instafeed.R;
import com.abacusdesk.instafeed.instafeed.Util.Apis;
import com.abacusdesk.instafeed.instafeed.Util.CommonFunctions;
import com.abacusdesk.instafeed.instafeed.adapter.Blog_Adapter;
import com.abacusdesk.instafeed.instafeed.custom_dialog.Dialogs;
import com.abacusdesk.instafeed.instafeed.model.Blog_allModel;
import com.abacusdesk.instafeed.instafeed.model.Blog_model;
import com.abacusdesk.instafeed.instafeed.webservice.VolleySingleton;
import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.google.gson.Gson;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by ishaan on 6/21/2017.
 */

public class Frag_blogslist extends Fragment {

    private View view;
    Blog_allModel blog_model;
    private  RecyclerView recyclerView;
    ArrayList<Blog_allModel.DataBean> arrayList= new ArrayList<>();
    Blog_Adapter adapter;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view=inflater.inflate(R.layout.frag_blogslist,container,false);
        recyclerView = (RecyclerView) view.findViewById(R.id.recyclerView);

        if (CommonFunctions.isConnected(getActivity())){
            if (!arrayList.isEmpty()){
                arrayList.clear();
            }
            getBlogsList();
        } else {
            //Toast.makeText(getActivity(), "Internet not available", Toast.LENGTH_SHORT).show();
        }
        setRecyclerView();
        return view;
    }

    public void getBlogsList() {
        StringRequest stringRequest = new StringRequest(Request.Method.GET, Apis.blogs,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                       // Log.e("log", "" + response);
                        try {
                            getResponse(response);
                        } catch (JSONException e) {
                           // e.printStackTrace();
                        }
                        Dialogs.disDialog();
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Dialogs.disDialog();
                    }
                })
        {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Content-Type", "application/x-www-form-urlencoded");
                return params;
            }
        };
        VolleySingleton.getInstance(getActivity()).addToRequestQueue(stringRequest);

    }

    private void getResponse(String response ) throws JSONException {
        if(!response.equals(null)) {
           // Log.e("log", "" + response);
            JSONObject jsonObject = new JSONObject(response);
            Object object = jsonObject.get("data");
            if (object instanceof JSONArray) {
                Gson gson = new Gson();
                blog_model = gson.fromJson(response, Blog_allModel.class);
                int status = blog_model.getStatus();
                String msg = blog_model.getMessage();
                if (status==200 && msg.equalsIgnoreCase("success")) {
                    if (blog_model.getData() != null && !blog_model.getData().isEmpty()) {
                        for (int i = 0; i < blog_model.getData().size(); i++) {
                            try {
                                Blog_allModel.DataBean dataBean = blog_model.getData().get(i);
                                arrayList.add(dataBean);
                            } catch (Exception e) {
                               // e.printStackTrace();
                            }
                        }
                    }
                   // Log.e("arrayblog",""+arrayList);
                    adapter.notifyDataSetChanged();
                }
            }
        }
    }

    private void setRecyclerView() {
        try{
            adapter= new Blog_Adapter(getActivity(),arrayList);
        }  catch (Exception e){
            //e.printStackTrace();
        }

        recyclerView.setHasFixedSize(true);
        LinearLayoutManager layoutManager=new LinearLayoutManager(getActivity());
        recyclerView.setLayoutManager(layoutManager);

        layoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        recyclerView.setAdapter(adapter);
    }
}