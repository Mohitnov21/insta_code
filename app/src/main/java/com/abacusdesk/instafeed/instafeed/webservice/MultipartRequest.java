package com.abacusdesk.instafeed.instafeed.webservice;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkResponse;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.toolbox.HttpHeaderParser;

import java.io.ByteArrayOutputStream;
import java.io.UnsupportedEncodingException;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by codecube on 14/12/16.
 */

public class MultipartRequest extends Request<String> {

    // *delimeter and boundary depends on server side format of multipart form request
    private String delimiter = "--";
    private String boundary =  "SwA"+ Long.toString(System.currentTimeMillis())+"SwA";

    private final ByteArrayOutputStream byteArrayOutputStream;
    private final Response.Listener listener;

    public MultipartRequest(int method, String url, Response.Listener listener, Response.ErrorListener errorlistener) {
        super(method, url, errorlistener);
        this.listener = listener;
        byteArrayOutputStream = new ByteArrayOutputStream();
    }

    @Override
    public Map<String, String> getHeaders() throws AuthFailureError {
        Map<String, String> headers = super.getHeaders();
        if (headers == null || headers.equals(Collections.emptyMap())) headers = new HashMap<String, String>();
        headers.put("Connection", "Keep-Alive");
        headers.put("Accept-Charset", "UTF-8");
        headers.put("Content-Type", "application/x-www-form-urlencoded;charset=utf-8");
        headers.put("Content-Type", "multipart/form-data; boundary=" + boundary);
        return headers;
    }

    @Override
    public String getBodyContentType() {
        return "multipart/form-data; charset=utf-8";
    }

    @Override
    public byte[] getBody() throws AuthFailureError {
        if(byteArrayOutputStream == null) return super.getBody();
        try {
            finishMultipart();
            return byteArrayOutputStream.toByteArray();
        } catch (Exception e) {
            e.printStackTrace();
            return super.getBody();
        }
    }



    @Override
    protected Response<String> parseNetworkResponse(NetworkResponse response) {
        try {
            String data = new String(response.data, HttpHeaderParser.parseCharset(response.headers));
            return Response.success(data, HttpHeaderParser.parseCacheHeaders(response));
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
            return Response.error(new ParseError(e));
        }
    }

    @Override
    protected void deliverResponse(String response) {
        listener.onResponse(response);
    }

    public void addFormPart(String paramName, String value) throws Exception {
        byteArrayOutputStream.write( (delimiter + boundary + "\r\n").getBytes());
        byteArrayOutputStream.write( "Content-Type: text/plain\r\n".getBytes());
        byteArrayOutputStream.write( ("Content-Disposition: form-data; name=\"" + paramName + "\"\r\n").getBytes());
        byteArrayOutputStream.write( ("\r\n" + value + "\r\n").getBytes());
    }

    public void addFilePart(String paramName, String fileName, byte[] data) throws Exception {
        byteArrayOutputStream.write( (delimiter + boundary + "\r\n").getBytes());
        byteArrayOutputStream.write( ("Content-Disposition: form-data; name=\"" + paramName +  "\"; filename=\"" + fileName + "\"\r\n"  ).getBytes());
        byteArrayOutputStream.write( ("Content-Type: application/octet-stream\r\n"  ).getBytes());
        byteArrayOutputStream.write( ("Content-Transfer-Encoding: binary\r\n"  ).getBytes());
        byteArrayOutputStream.write("\r\n".getBytes());
        byteArrayOutputStream.write(data);
        byteArrayOutputStream.write("\r\n".getBytes());
    }

    private void finishMultipart() throws Exception {
        byteArrayOutputStream.write( (delimiter + boundary + delimiter + "\r\n").getBytes());
    }



}
