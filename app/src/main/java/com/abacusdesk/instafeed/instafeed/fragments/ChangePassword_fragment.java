package com.abacusdesk.instafeed.instafeed.fragments;

import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.abacusdesk.instafeed.instafeed.NavDrawerActivity;
import com.abacusdesk.instafeed.instafeed.R;
import com.abacusdesk.instafeed.instafeed.Util.Alerts;
import com.abacusdesk.instafeed.instafeed.Util.Apis;
import com.abacusdesk.instafeed.instafeed.Util.CommonFunctions;
import com.abacusdesk.instafeed.instafeed.Util.SaveSharedPreference;
import com.abacusdesk.instafeed.instafeed.adapter.Petition_Adapter;
import com.abacusdesk.instafeed.instafeed.custom_dialog.Dialogs;
import com.abacusdesk.instafeed.instafeed.model.Petition_model;
import com.android.volley.AuthFailureError;
import com.android.volley.NoConnectionError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.bumptech.glide.Glide;
import com.google.gson.Gson;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by abacusdesk on 2017-07-26.
 */

public class ChangePassword_fragment extends Fragment{

    View view;
    EditText ednewpassword,edconfirmpassword;
    Button btnsubmit;
    TextView txtfulname,txtusername;
    FrameLayout frameLayout;
    ImageView imgprofile,imgdemo;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.change_passwordfrag, container, false);
        init();
        getActivity().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_PAN |WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);
        return view;
    }

    private void init(){

        if (NavDrawerActivity.toolbar!=null){
            NavDrawerActivity.toolbar.setLogo(R.drawable.logo_black);
        }

        frameLayout=(FrameLayout)view.findViewById(R.id.img_frame);
        imgprofile=(ImageView) view.findViewById(R.id.img_profile);
        imgdemo=(ImageView) view.findViewById(R.id.imgdemo);

        txtfulname=(TextView) view.findViewById(R.id.txt_fullname);
        txtusername=(TextView) view.findViewById(R.id.txt_username);

       // edoldpassword=(EditText)view.findViewById(R.id.ed_oldpassword);
        ednewpassword=(EditText)view.findViewById(R.id.ed_newpassword);
        edconfirmpassword=(EditText)view.findViewById(R.id.ed_confirmpassword);
        btnsubmit=(Button)view.findViewById(R.id.btn_submit);
        btnsubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (CommonFunctions.isConnected(getContext())){
                    if (valid()){
                        changePassword();
                    }
                }
            }
        });
        if(SaveSharedPreference.getUserName(getContext()).isEmpty()){
            txtusername.setText("#"+SaveSharedPreference.getUserName(getContext()));
        }
        txtfulname.setText(SaveSharedPreference.getFirstName(getContext()));
        if (!SaveSharedPreference.getLastName(getContext()).equals("null")){
            txtfulname.setText(SaveSharedPreference.getFirstName(getContext())+" "+SaveSharedPreference.getLastName(getContext()));
        }
        if (!SaveSharedPreference.getUserIMAGE(getContext()).equals("")){
            imgdemo.setVisibility(View.INVISIBLE);
            Glide.with(getActivity()).load(SaveSharedPreference.getUserIMAGE(getContext())).error(R.drawable.user).into(imgprofile);
        }
    }

    private boolean valid() {
          if (ednewpassword.getText().toString().equals("")) {
            ednewpassword.setError("Please Enter New Password");
            return false;
        } else if (edconfirmpassword.getText().toString().equals("")) {
            edconfirmpassword.setError("Please  Confirm Password");
            return false;
        }else if (ednewpassword.getText().toString().length()<8) {
            ednewpassword.setError("Password cannot be less than 8 characters");
            return false;
        }else if (!edconfirmpassword.getText().toString().equals(ednewpassword.getText().toString())) {
            edconfirmpassword.setError("Password not matched");
            return false;
        } else {
            return true;
        }
    }

    private void changePassword() {
         Dialogs.showProDialog(getActivity(), "Loading");
        StringRequest stringRequest = new StringRequest(Request.Method.POST, Apis.change_password,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.e("log", "" + response);
                        try {
                            getResponse(response);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        Dialogs.disDialog();
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Dialogs.disDialog();
                        DialogInterface.OnClickListener onClickTryAgain = new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                Dialogs.disDialog();
                                changePassword();
                            }
                        };
                        if (error instanceof TimeoutError) {
                            Dialogs.disDialog();
                            Alerts.timeoutErrorAlert(getContext(), onClickTryAgain);
                        } else if (error instanceof NoConnectionError) {
                            Dialogs.disDialog();
                            Alerts.internetConnectionErrorAlert(getContext(), onClickTryAgain);
                        }
                    }
                })
        {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                HashMap<String,String> hashMap=new HashMap<String, String>();
                hashMap.put("user",SaveSharedPreference.getUserID(getContext()));
                hashMap.put("token",SaveSharedPreference.getPrefToken(getContext()));
                hashMap.put("password",ednewpassword.getText().toString());
                hashMap.put("confirm_password",edconfirmpassword.getText().toString());
                return hashMap;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Content-Type", "application/x-www-form-urlencoded");
                return params;
            }
        };

        RequestQueue requestQueue = Volley.newRequestQueue(getActivity());
        requestQueue.add(stringRequest);
    }

    private void getResponse(String response ) throws JSONException {
        if(!response.equals(null)){
            JSONObject jsonObject= new JSONObject(response);
            if (jsonObject.getString("status").equalsIgnoreCase("200") && jsonObject.getString("message").equalsIgnoreCase("success")){
                Toast.makeText(getContext(),jsonObject.getJSONObject("data").getString("message"),Toast.LENGTH_SHORT).show();
                getActivity().getSupportFragmentManager().popBackStack();
            }else {
                Toast.makeText(getContext(),jsonObject.getString("message"),Toast.LENGTH_SHORT).show();
            }
        }
    }

}


