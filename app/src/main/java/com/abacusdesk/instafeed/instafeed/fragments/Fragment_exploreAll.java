package com.abacusdesk.instafeed.instafeed.fragments;

import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.abacusdesk.instafeed.instafeed.NavDrawerActivity;
import com.abacusdesk.instafeed.instafeed.R;
import com.abacusdesk.instafeed.instafeed.Util.Alerts;
import com.abacusdesk.instafeed.instafeed.Util.Apis;
import com.abacusdesk.instafeed.instafeed.Util.CommonFunctions;
import com.abacusdesk.instafeed.instafeed.adapter.ExploreAll_adapter;
import com.abacusdesk.instafeed.instafeed.adapter.RecyclerView_Adapter;
import com.abacusdesk.instafeed.instafeed.custom_dialog.Dialogs;
import com.abacusdesk.instafeed.instafeed.model.Exploreall_model;
import com.abacusdesk.instafeed.instafeed.model.NewsModel;
import com.android.volley.AuthFailureError;
import com.android.volley.NoConnectionError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by abacusdesk on 2017-07-14.
 */

public class Fragment_exploreAll extends Fragment {

        private View view;
        private String title; //String for tab title
        Exploreall_model exploreall_model;
        private static RecyclerView recyclerView;
        ArrayList<Exploreall_model.DataBean> exploredatbean=new ArrayList<>();
        ExploreAll_adapter adapter;
        public static String catid="",postid="";

        @Nullable
        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
            view = inflater.inflate(R.layout.fragment_news, container, false);
            recyclerView = (RecyclerView) view.findViewById(R.id.recyclerView);
            setRecyclerView();
            if (CommonFunctions.isConnected(getActivity())){
                if (!exploredatbean.isEmpty()){
                    exploredatbean.clear();
                }
                getExploreList();
            } else {
                Toast.makeText(getActivity(), "Internet not available", Toast.LENGTH_SHORT).show();
            }
            view.setOnKeyListener(new View.OnKeyListener() {
                @Override
                public boolean onKey(View v, int keyCode, KeyEvent event) {
                    if (keyCode == KeyEvent.KEYCODE_BACK) {
                        // ViewPagerAdapter.stopVideo(getActivity());

                        getActivity().getSupportFragmentManager().popBackStack();
                        return true;
                    } else {
                        return false;
                    }
                }
            });
            return view;
        }

        public void getExploreList() {
            Dialogs.showProDialog(getActivity(), "Loading");
            StringRequest stringRequest = new StringRequest(Request.Method.GET, Apis.explore_all,
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {
                            Log.e("log", "" + response);
                            getResponse(response);
                            Dialogs.disDialog();
                        }
                    },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            DialogInterface.OnClickListener onClickTryAgain = new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    Dialogs.disDialog();
                                    getExploreList();
                                }
                            };
                            if (error instanceof TimeoutError) {
                                Dialogs.disDialog();
                                Alerts.timeoutErrorAlert(getContext(), onClickTryAgain);
                            } else if (error instanceof NoConnectionError) {
                                Dialogs.disDialog();
                                Alerts.internetConnectionErrorAlert(getContext(), onClickTryAgain);
                            }
                        }
                    })
            {
                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    Map<String, String> params = new HashMap<String, String>();
                    params.put("Content-Type", "application/x-www-form-urlencoded");
                    return params;
                }
            };

            RequestQueue requestQueue = Volley.newRequestQueue(getActivity());
            requestQueue.add(stringRequest);
        }

        private void getResponse(String response )  {
            if(!response.equals(null)){
                Log.e("log", "" + response);
                Gson gson = new Gson();
                exploreall_model = gson.fromJson(response, Exploreall_model.class);
                int status = exploreall_model.getStatus();
                String msg = exploreall_model.getMessage();
                if (msg.equalsIgnoreCase("success") && status==200) {
                    if (exploreall_model.getData()!=null && !exploreall_model.getData().isEmpty()){
                        for (int i = 0; i < exploreall_model.getData().size(); i++) {
                            try {
                                Exploreall_model.DataBean dataBean = exploreall_model.getData().get(i);
                                exploredatbean.add(dataBean);
                            } catch (Exception e){
                                e.printStackTrace();
                            }
                        }
                    }
                    adapter.notifyDataSetChanged();
                }

            }
        }

        // Setting recycler view
        private void setRecyclerView() {
            recyclerView.setHasFixedSize(true);
            recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
            // recyclerView.addItemDecoration(mDividerItemDecoration);
            try{
                adapter= new ExploreAll_adapter(getActivity(),exploredatbean);
                recyclerView.setAdapter(adapter);
            }  catch (Exception e){
                e.printStackTrace();
            }
        }
 }
