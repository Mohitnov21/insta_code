package com.abacusdesk.instafeed.instafeed.adapter;

import android.content.Context;
import android.content.DialogInterface;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.abacusdesk.instafeed.instafeed.R;
import com.abacusdesk.instafeed.instafeed.Util.Alerts;
import com.abacusdesk.instafeed.instafeed.Util.Apis;
import com.abacusdesk.instafeed.instafeed.Util.SaveSharedPreference;
import com.abacusdesk.instafeed.instafeed.custom_dialog.Dialogs;
import com.abacusdesk.instafeed.instafeed.fragments.Publicprofile_Fragment;
import com.abacusdesk.instafeed.instafeed.model.Follower_model;
import com.abacusdesk.instafeed.instafeed.model.User_model;
import com.abacusdesk.instafeed.instafeed.webservice.VolleySingleton;
import com.android.volley.AuthFailureError;
import com.android.volley.NoConnectionError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.bumptech.glide.Glide;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by abacusdesk on 2017-07-24.
 */

public class Follow_Adapter extends RecyclerView.Adapter<Follow_Adapter.MyViewHolder> {

    Context context;
    Boolean bool;
    ArrayList<Follower_model.DataBean> arrayList;
    public static String  Apitemp_follow="";
    private static String  userid="";
    public static Boolean  isfollow=false;

    public Follow_Adapter(Context context, ArrayList<Follower_model.DataBean> arrayList,Boolean bool){
        this.context=context;
        this.arrayList=arrayList;
        this.bool=bool;
        Log.e("size array",""+arrayList);
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView txtusername,txtuserhashname;
        public ImageView imgpro;
        public Button btnfollow;
        public LinearLayout lnitem;

        public MyViewHolder(View view) {
            super(view);
            this.lnitem=(LinearLayout)itemView.findViewById(R.id.ln_item);
            this.txtusername = (TextView) itemView.findViewById(R.id.txt_username);
            this.txtuserhashname = (TextView) itemView.findViewById(R.id.txt_userhashname);
            this.imgpro=(ImageView) itemView.findViewById(R.id.user_img);
            this.btnfollow = (Button) itemView.findViewById(R.id.btn_follow);
        }
    }

    @Override
    public Follow_Adapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_userlist, parent, false);
        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final Follow_Adapter.MyViewHolder holder, final int position) {

        final Follower_model.DataBean dataBean = arrayList.get(position);
        holder.txtusername.setText(dataBean.getFirst_name());
        holder.txtuserhashname.setText("#"+dataBean.getUsername());
        if (dataBean.getAvatar()!="null"){
            Glide.with(context).load(dataBean.getAvatar()).error(R.drawable.user).into(holder.imgpro);
        }
        if (bool.equals(true)){
            holder.btnfollow.setVisibility(View.GONE);

        } else {
            holder.btnfollow.setText("UnFollow");
         }
        holder.btnfollow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (holder.btnfollow.getText().toString().equalsIgnoreCase("UnFollow")){
                    Apitemp_follow= Apis.unfollow;
                    userid=dataBean.getId();
                    dofollow(holder,position);
                } else {
                    Apitemp_follow= Apis.follow;
                    userid=dataBean.getId();
                    dofollow(holder,position);
                }
            }
        });
        holder.lnitem.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Publicprofile_Fragment.flag=true;
                Publicprofile_Fragment.username=dataBean.getUsername();
                AppCompatActivity activity = (AppCompatActivity) v.getContext();
                activity.getSupportFragmentManager().beginTransaction()
                        .replace(R.id.frame_container, new Publicprofile_Fragment(),"public").addToBackStack(null).commit();
            }
        });
    }

    @Override
    public int getItemCount() {
        return arrayList.size();
    }

    public void dofollow(final Follow_Adapter.MyViewHolder holder, final int position) {
        //Dialogs.showProDialog(getActivity(), "Loading");
        StringRequest stringRequest = new StringRequest(Request.Method.POST,Apitemp_follow,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.e("log", "" + response);
                        resFollow(response,holder,position);
                        Dialogs.disDialog();
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Dialogs.disDialog();
                        DialogInterface.OnClickListener onClickTryAgain = new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                Dialogs.disDialog();
                                dofollow(holder,position);
                            }
                        };
                        if (error instanceof TimeoutError) {
                            Dialogs.disDialog();
                            Alerts.timeoutErrorAlert(context, onClickTryAgain);
                        } else if (error instanceof NoConnectionError) {
                            Dialogs.disDialog();
                            Alerts.internetConnectionErrorAlert(context, onClickTryAgain);
                        }
                    }
                })
        {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> map = new HashMap<String, String>();
                map.put("user", SaveSharedPreference.getUserID(context));
                map.put("token",SaveSharedPreference.getPrefToken(context));
                map.put("username",userid);
                Log.e("responsesocial",""+map);
                return map;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Content-Type", "application/x-www-form-urlencoded");
                return params;
            }
        };
        VolleySingleton.getInstance(context).addToRequestQueue(stringRequest);
    }

    /*  {
          "status": 200,
              "message": "success",
              "data": {
          "is_following": false
      }
      }*/


   /* public void swap(ArrayList<Follower_model.DataBean> data){
        arrayList.clear();
        arrayList.addAll(data);
        adapter=new Follow_Adapter(context,arrayList,bool);
        adapter.notifyDataSetChanged();
    }*/

    private void resFollow(String response,Follow_Adapter.MyViewHolder holder,int position)  {
        try {
            JSONObject jsonObject= new JSONObject(response);
            if (Apis.isfollow.equals(Apitemp_follow)){
                if (jsonObject.getString("status").equals("200")&& jsonObject.getString("message").equals("success")){
                    isfollow=jsonObject.getJSONObject("data").getBoolean("is_following");
                    if (isfollow==true){
                        holder.btnfollow.setText("UnFollow");
                    }else {
                        holder.btnfollow.setText("follow");
                    }
                    Log.e("isfollow",""+isfollow);
                }
            }else if(Apis.unfollow.equals(Apitemp_follow)){
                if (jsonObject.getString("status").equals("200")&& jsonObject.getString("message").equals("success")){
                    isfollow=false;
                    holder.btnfollow.setText("follow");
                    Log.e("unfollow",""+isfollow);
                }
            } else {
                if (jsonObject.getString("status").equals("200")&& jsonObject.getString("message").equals("success")){
                    isfollow=true;
                    holder.btnfollow.setText("UnFollow");
                    Log.e("follow",""+isfollow);
                } else {
                    Log.e("error",""+jsonObject.getString("message"));
                }
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }
}
