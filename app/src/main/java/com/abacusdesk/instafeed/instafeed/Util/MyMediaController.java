package com.abacusdesk.instafeed.instafeed.Util;

import android.content.Context;
import android.media.session.MediaController;
import android.os.Build;
import android.support.annotation.RequiresApi;
import android.widget.FrameLayout;
import android.widget.RelativeLayout;

/**
 * Created by ishaan on 6/26/2017.
 */
@RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
public class MyMediaController extends android.widget.MediaController {
    private FrameLayout anchorView;


    public MyMediaController(Context context, FrameLayout anchorView)
    {
        super(context);
        this.anchorView = anchorView;
    }


    @Override
    protected void onSizeChanged(int xNew, int yNew, int xOld, int yOld)
    {
        super.onSizeChanged(xNew, yNew, xOld, yOld);

        FrameLayout.LayoutParams lp = (FrameLayout.LayoutParams) anchorView.getLayoutParams();
        lp.setMargins(0, 0, 0, yNew);
        anchorView.setLayoutParams(lp);
        anchorView.requestLayout();
    }
}