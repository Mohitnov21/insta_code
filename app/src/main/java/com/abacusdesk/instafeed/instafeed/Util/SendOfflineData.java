package com.abacusdesk.instafeed.instafeed.Util;

import android.app.Activity;
import android.app.IntentService;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.IBinder;
import android.support.annotation.Nullable;
import android.support.v4.app.NotificationCompat;
import android.support.v4.app.TaskStackBuilder;
import android.util.Log;
import android.widget.Toast;

import com.abacusdesk.instafeed.instafeed.AndroidMultiPartEntity;
import com.abacusdesk.instafeed.instafeed.NavDrawerActivity;
import com.abacusdesk.instafeed.instafeed.R;
import com.abacusdesk.instafeed.instafeed.model.*;
import com.abacusdesk.instafeed.instafeed.model.MasterTblModel;
import com.abacusdesk.instafeed.instafeed.services.TrackGPS;
import com.android.volley.RequestQueue;
import com.google.gson.Gson;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.mime.content.FileBody;
import org.apache.http.entity.mime.content.StringBody;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;

import java.io.File;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import static com.abacusdesk.instafeed.instafeed.Util.ScreenConstants.MEDIA_TYPE_AUDIO;
import static com.abacusdesk.instafeed.instafeed.Util.ScreenConstants.MEDIA_TYPE_IMAGE;
import static com.abacusdesk.instafeed.instafeed.Util.ScreenConstants.MEDIA_TYPE_VIDEO;

/**
 * Created by spsuser4 on 7/14/2016.
 */

public class SendOfflineData extends Service {

    HashMap<String, List<GridModel>> fileList;
    DbHandler dh1;
    String newsId, post_newsId, fileName, filePath, mode, fileType, deviceId, prefToken, userID, isPosted, newsCategoryId, capturedLat, capturedLong, createdOn, title, description;
    public static String IS_POSTED = "0";
    String anonymousMode;
    public Context context;
    Thread SendData = null;
    DbHandler mDatabase;
    Fused fused;
    TrackGPS gps;
    private static boolean isDataSend = false;
    int imageCount = 1, vedioCount = 1, audioCount = 1;

    CreatePostResponseModel createPostResponseModel;
    AppLocationService appLocationService;
    private RequestQueue requestQueue;
    ArrayList<GridModel> mediaList ;
    public static String DATASYNC = "datasync";
    ArrayList<MasterTblModel> masterList;
    private final String TAG = SendOfflineData.class.getName();
    private NotificationCompat.Builder mBuilder; //=new NotificationCompat.Builder(context);
    private int mId;
    private NotificationManager mNotifyManager;
    private String mTitle;
    private long totalSize;
    HashMap<String,ArrayList<GridModel>> arrayhash_data;

    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public void onDestroy() {
        Log.e("start","destroy");
        super.onDestroy();
    }


    @Override
    public void onTaskRemoved(Intent rootIntent) {
        Log.e("ClearFromRecentService", "END");
       // setCompletedNotification("Uploading failed");
       // setNotificationStatus();
        // stopSelf();
    }

    /*

     public SendOfflineData() {
        super("SendOfflineData");
      }

    @Override
    protected void onHandleIntent(@Nullable Intent intent) {
        appLocationService = new AppLocationService(getApplicationContext());
        context = getApplication().getBaseContext();
        Log.i("Service oncreate", "Service oncreate");

        //mDatabase = new DbHandler(getApplicationContext());
        fused = new Fused(context, 1);
        fused.onStart();

        Log.e("start","called primary constructor");
        dh1 = new DbHandler(context);
        dh1.open();

        arrayhash_data=new HashMap<>();
        masterList = new ArrayList<>();
        fileList = new HashMap<>();
        fileList.clear();
        Log.e("testing"," duplicate");
        // SendData = new SendData();
        // SendData.start();
        sendOfflinePost();
    }*/

    @Override
    public void onCreate() {
        super.onCreate();
        appLocationService = new AppLocationService(getApplicationContext());
        context = getApplication().getBaseContext();
        Log.i("Service oncreate", "Service oncreate");

        //mDatabase = new DbHandler(getApplicationContext());
        fused = new Fused(context, 1);
        fused.onStart();

        try {
            mBuilder=new NotificationCompat.Builder(context);
        }catch (Exception e){

        }

        arrayhash_data=new HashMap<>();
        masterList = new ArrayList<>();
        fileList = new HashMap<>();
        fileList.clear();
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        dh1 = new DbHandler(context);
        dh1.open();
        SendData = new SendData();
        SendData.start();
        return START_STICKY;
    }


    class SendData extends Thread {
        @Override
        public void run() {
            Log.e("testing"," thread start");
          //  synchronized (this){
                sendOfflinePost();
           // }
        }
    }

    private void sendOfflinePost(){
         // synchronized (this) {
         Log.e("testing"," sendoffline");
            masterList.clear();
            Log.e("start", "sendoffline");
            Cursor m = dh1.fetch_UnPostedData(IS_POSTED);
            Log.i("UnPostedData_offline", m.getCount() + "");
            if (m.getCount() > 0) {
                try {
                    if (m.moveToFirst()) {
                        do {
                            Log.i("SendOfflinedata 1", "tag 1");
                            newsId = m.getString(0);  ///primary key
                            Log.d("newsId_primaryKey", "sendOfflinePost: " + m.getString(0));
                            deviceId = m.getString(1);
                            Log.d("deviceId", "sendOfflinePost: " + m.getString(1));
                            isPosted = m.getString(5);
                            Log.d("isPosted", "sendOfflinePost: " + m.getString(5));
                            prefToken = m.getString(3);
                            Log.d("prefToken", "sendOfflinePost: " + m.getString(3));
                            userID = m.getString(4);
                            Log.d("userID", "sendOfflinePost: " + m.getString(4));
                            newsCategoryId = m.getString(6);
                            Log.d("newsCategoryId", "sendOfflinePost: " + m.getString(6));
                            capturedLat = m.getString(11);
                            Log.d("capturedLat", "sendOfflinePost: " + m.getString(11));
                            capturedLong = m.getString(12);
                            Log.d("capturedLong", "sendOfflinePost: " + m.getString(12));
                            createdOn = m.getString(9);
                            Log.d("createdOn", "sendOfflinePost: " + m.getString(9));
                            title = m.getString(7);
                            Log.d("title", "sendOfflinePost: " + m.getString(7));
                            description = m.getString(8);
                            Log.d("description", "sendOfflinePost: " + m.getString(8));
                            mode = m.getString(15);
                            Log.d("mode", "sendOfflinePost: " + m.getString(15));
                            anonymousMode = m.getString(16);
                            Log.d("anonymousMode", "sendOfflinePost: " + m.getString(16));
                            int categoryType = m.getInt(17);
                            masterList.add(new MasterTblModel(newsId, deviceId, prefToken, userID, isPosted, newsCategoryId, capturedLat, capturedLong, createdOn, title, description, anonymousMode, mode, categoryType));

                        } while (m.moveToNext());
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
                Log.d(getClass().getSimpleName(), "sendOfflinePost: " + masterList.size());
                if (masterList.size() > 0) {
                    for (int i = 0; i < masterList.size(); i++) {
                        MasterTblModel masterTblModel = masterList.get(i);
                        mediaList = new ArrayList<>();
                        fetchNewsDataAccordingToNewsId(masterTblModel.getNewsId(), masterTblModel);
                    }
                }
            }
    }

    private void fetchNewsDataAccordingToNewsId(String newsId1, MasterTblModel masterTblModel1) {
        mediaList.clear();
        Log.e("testing"," fetch news");
        Log.e("start","fetchnews");
        Cursor m = dh1.fetch_MediaFiles(newsId1);
        Log.i("fetch_MediaFiles", m.getCount() + "" + "newsID " + newsId1);
        if (m.getCount() > 0) {
            try {
                if (m.moveToFirst()) {
                    do {
                        Log.i("fetchNewsAccoToNewsId 1", "tag 1");
                        post_newsId = m.getString(1);   //peimary key of master table
                        Log.d("post_newsId", m.getString(1));
                        fileType = m.getString(2);
                        Log.d("fileType", m.getString(2));
                        fileName = m.getString(3);
                        Log.d("fileName", m.getString(3));
                        filePath = m.getString(4);
                        Log.d("filePath", m.getString(4));
                        mediaList.add(new GridModel(fileType, filePath, fileName));
                    } while (m.moveToNext());
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
            Log.e("dbnewsid",""+newsId1);
            arrayhash_data.put(newsId1,mediaList);

            if (CommonFunctions.isConnected(context)) {
                Log.e("testing"," connect1");
                sendPost(mediaList, masterTblModel1);
                Log.d(getClass().getSimpleName(), "fetchNewsDataAccordingToNewsId:with media file");
            }
        } else {
            if (CommonFunctions.isConnected(context)) {
                Log.e("testing ","connect2");
                sendPost(mediaList, masterTblModel1);
                Log.d(getClass().getSimpleName(), "fetchNewsDataAccordingToNewsId:without media file");
            }
        }
    }

    private void sendPost(ArrayList<GridModel> mediaList1, MasterTblModel masterTblModel) {
        gps = new TrackGPS(SendOfflineData.this);
        if (gps.canGetLocation() == false) {
            try {
                new GpsActivation((Activity) context).enableGPS();
            }catch (Exception e){

            }
        } else {
            Log.d("TAG ofline", "sendPost: " + fused.lon + "" + "lat long" + fused.lat + "");
            if ((fused.lon != null && fused.lat != null && fused.lon != "0.0" && fused.lat != "0.0")) {
                if (CommonFunctions.isConnected(context)) {
                       Log.e("testing","true alwasys");
                       if (true){
                           dh1.update_master("1", capturedLat, capturedLong, "", masterTblModel.getNewsId());
                           new UploadFileToServer
                                   (mediaList1, masterTblModel, fused.lat + "", fused.lon + "", System.currentTimeMillis() + "").execute();
                           //NavDrawerActivity.array_check.add(newsId);
                       }
                   /* if (){
                        Log.e("testing","true alwasys");

                        //  createMultiPartFormRequest(mediaList1, masterTblModel, fused.lat + "", fused.lon+ "", System.currentTimeMillis() + "");
                    }*/
                }
            }
        }
    }

    private void showNotification(String title) {
        int notificationIdentifier = (int) System.currentTimeMillis();
        Intent intent = new Intent(this, NavDrawerActivity.class)
                .setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP|Intent.FLAG_ACTIVITY_NEW_TASK);
        // use System.currentTimeMillis() to have a unique ID for the pending intent
        PendingIntent pIntent = PendingIntent.getActivity(this, (int) System.currentTimeMillis(), intent, 0);
        Uri defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        NotificationCompat.Builder notificationBuilder = null;
        try {
            notificationBuilder = new NotificationCompat.Builder(this).
                    setSmallIcon(R.drawable.logo_black).
                    setContentTitle(URLDecoder.decode(getString(R.string.app_name).toString(), "UTF-8")).
                    setContentText(title).
                    setAutoCancel(true).
                    setSound(defaultSoundUri).
                    setContentIntent(pIntent);
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        NotificationManager notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        notificationManager.notify(notificationIdentifier, notificationBuilder.build());
    }

    private void setNotificationStatus() {
        try {
            Intent notificationIntent = new Intent();
            notificationIntent.setAction(DATASYNC);
            sendBroadcast(notificationIntent);
        }catch (Exception e){
        }
    }

    private void setProgressNotification() {
        try{
            mBuilder.setContentTitle(mTitle)
                    .setContentText("Uploading in progress").setSmallIcon(R.drawable.logo_black);
        }catch (Exception e){

        }
    }

    private void updateProgressNotification(int incr) {
        try {
            mBuilder.setProgress(100, incr, false);
            mNotifyManager.notify(mId, mBuilder.build());
        }catch (Exception e){

        }
    }

    private void setCompletedNotification(String message) {
        try {
            if (mBuilder==null){
                mBuilder=new NotificationCompat.Builder(context);
            }
            mBuilder.setSmallIcon(R.drawable.logo_black).setContentTitle(mTitle)
                    .setContentText(message);
            mBuilder.setProgress(0, 0, false);
            Intent resultIntent = new Intent(context, NavDrawerActivity.class)
                    .setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP|Intent.FLAG_ACTIVITY_NEW_TASK);
            TaskStackBuilder stackBuilder = TaskStackBuilder.create(context);
            // Adds the back stack for the Intent (but not the Intent itself)
            stackBuilder.addParentStack(NavDrawerActivity.class);
            // Adds the Intent that starts the Activity to the top of the stack
            stackBuilder.addNextIntent(resultIntent);
            PendingIntent resultPendingIntent = stackBuilder
                    .getPendingIntent(0, PendingIntent.FLAG_UPDATE_CURRENT);
            mBuilder.setContentIntent(resultPendingIntent);
            mNotifyManager.notify(mId, mBuilder.build());
        }catch (Exception e){

        }
    }

    private void initNotification() {
        mNotifyManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
        mBuilder = new NotificationCompat.Builder(context);
    }

    private class UploadFileToServer extends AsyncTask<Void, Integer, String> {

        ArrayList<GridModel> mediaList1;
        MasterTblModel masterTblModel;
        String postedLatitude;
        String postedLongitude;
        String postedOn;
        Boolean isRunning=true;

        public UploadFileToServer(ArrayList<GridModel> mediaList1, MasterTblModel masterTblModel, String postedLatitude, String postedLongitude, String postedOn) {
            this.mediaList1 = mediaList1;
            this.masterTblModel = masterTblModel;
            this.postedLatitude = postedLatitude;
            this.postedLongitude = postedLongitude;
            this.postedOn = postedOn;
        }

        @Override
        protected void onPreExecute() {
            initNotification();
            super.onPreExecute();
        }

        @Override
        protected void onProgressUpdate(Integer... progress) {
            int incr = progress[0];
            try{
                if (incr == 0) setProgressNotification();

                if (incr % (100 / 5) == 0) {
                    updateProgressNotification(incr);
                }
            }catch (Exception e){

            }
        }

        @Override
        protected String doInBackground(Void... params) {
            return uploadFile();
        }

       // @SuppressWarnings("deprecation")
        private String uploadFile() {
            String responseString = null;
            HttpClient httpclient = new DefaultHttpClient();
            HttpPost httppost;
            int categoryType = masterTblModel.getCategoryType();
            switch (categoryType) {
                case 1:
                    httppost = new HttpPost(WebUrl.UPLOAD_NEWS_URL);
                    break;
                case 2:
                    httppost = new HttpPost(WebUrl.UPLOAD_TALENT_URL);
                    break;
                case 3:
                    httppost = new HttpPost(WebUrl.UPLOAD_EXPLORE_URL);
                    break;
                default:
                    httppost = new HttpPost(WebUrl.UPLOAD_NEWS_URL);
                    break;
            }

            try {
                AndroidMultiPartEntity entity = new AndroidMultiPartEntity(new AndroidMultiPartEntity.ProgressListener() {
                    @Override
                    public void transferred(long num) {
                        publishProgress((int) ((num / (float) totalSize) * 100));
                    }
                });
                if (arrayhash_data!=null && !arrayhash_data.isEmpty() && !mediaList.isEmpty()) {
                    for (int i = 0; i < arrayhash_data.get("" + masterTblModel.getNewsId()).size(); i++) {
                        //Log.d("TAG111", "createMultiPartFormRequest: " + mediaList1.size());
                        GridModel gridModel = arrayhash_data.get("" + masterTblModel.getNewsId()).get(i);
                        byte[] byte1 = null;
                        File sourceFile = null;
                        try {
                            if (gridModel != null && !gridModel.getFilePath().equals("null")) {
                                sourceFile = new File(gridModel.getFilePath());
                                if (!gridModel.getFileType().equals("NA") &&
                                        gridModel.getFileType().equals(MEDIA_TYPE_IMAGE)) {
                                    if (imageCount <= 10) {
                                        String filepathout = CommonUtils.compressImage(gridModel.getFilePath(), gridModel.getFileName());
                                        sourceFile = new File(filepathout);
                                        if (imageCount == 1) {
                                            entity.addPart("image", new FileBody(sourceFile));
                                            imageCount++;
                                        } else {
                                            entity.addPart("image" + imageCount, new FileBody(sourceFile));
                                            imageCount++;
                                        }
                                    }
                                } else if (!gridModel.getFileType().equals("NA") && gridModel.getFileType().equals(MEDIA_TYPE_VIDEO)) {
                                    if (vedioCount <= 10) {
                                        if (vedioCount == 1) {
                                            Log.e("video", "" + sourceFile);
                                            entity.addPart("video", new FileBody(sourceFile));
                                            vedioCount++;
                                        } else {
                                            Log.e("video", "" + sourceFile);
                                            entity.addPart("video" + vedioCount, new FileBody(sourceFile));
                                            vedioCount++;
                                        }
                                    }
                                } else if (!gridModel.getFileType().equals("NA") && gridModel.getFileType().equals(MEDIA_TYPE_AUDIO)) {
                                    if (audioCount <= 10) {
                                        if (audioCount == 1) {
                                            entity.addPart("audio", new FileBody(sourceFile));
                                            audioCount++;
                                        } else {
                                            //  entity.addPart("audio" + audioCount, gridModel.getFileName(), byte1);
                                            entity.addPart("audio" + audioCount, new FileBody(sourceFile));
                                            audioCount++;
                                        }
                                    }
                                }
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                }
                try {
                    Log.e("start","progress");
                    isDataSend = true;
                    entity.addPart("mod", new StringBody(masterTblModel.getMode()));
                    Log.d("Tag", "createMultiPartFormRequest: " + new StringBody(masterTblModel.getMode()));
                    entity.addPart("deviceId", new StringBody(masterTblModel.getDeviceId()));
                    entity.addPart("token", new StringBody(masterTblModel.getPrefToken()));
                    entity.addPart("user", new StringBody(masterTblModel.getUserID()));
                    entity.addPart("title", new StringBody(masterTblModel.getTitle(), Charset.forName("UTF-8")));
                    entity.addPart("description", new StringBody(masterTblModel.getDescription(),Charset.forName("UTF-8")));

                    entity.addPart("category_id", new StringBody(masterTblModel.getNewsCategoryId()));
                    entity.addPart("location_id", new StringBody("3"));
                    entity.addPart("lat", new StringBody(postedLatitude));//latitude+""
                    entity.addPart("long", new StringBody(postedLongitude));//longitude+""
                    entity.addPart("is_an", new StringBody(masterTblModel.getAnonymous()));

                    Log.e("lastprint",""+masterTblModel.getNewsId());
                    Log.e("lastprint",""+masterTblModel.getUserID());
                    //entity.addPart("user", new StringBody(masterTblModel.getUserID()));
                    Log.d("offline posted lat long", "createMultiPartFormRequest: " + postedLatitude + "@@@" + postedLongitude + "@@" + masterTblModel.getAnonymous() + mediaList1.size());
                } catch (Exception e) {
                    e.printStackTrace();
                }

                totalSize = entity.getContentLength();
                Log.e("video total size",""+totalSize);
                httppost.setEntity(entity);

                // Making server call
                HttpResponse response = httpclient.execute(httppost);
                HttpEntity r_entity = response.getEntity();
                Log.e("testing posting","");
                Log.e("video http resonse",""+totalSize);
                Log.e("video http entity",""+r_entity);
                int statusCode = response.getStatusLine().getStatusCode();
                if (statusCode == 200) {
                    Log.e("testing"," success");
                    dh1.update_master("1", capturedLat, capturedLong, postedOn, masterTblModel.getNewsId());
                    dh1.delete_postData(masterTblModel.getNewsId());
                    dh1.delete_masterRowData(masterTblModel.getNewsId());
                    responseString = EntityUtils.toString(r_entity);
                    Log.e("video 200",""+responseString);
                } else {
                    Log.e("testing"," error");
                     // Log.e("video error",""+responseString);
                    responseString = EntityUtils.toString(r_entity);
                    Log.e("video error",""+responseString);
                    responseString = "Error occurred! Http Status Code:" + statusCode;
                }
            } catch (ClientProtocolException e) {
                isDataSend = false;
                //e.printStackTrace();
                responseString = "exception occurred!";
                Log.e("ecxception",""+e);
            } catch (IOException e) {
                //e.printStackTrace();
                isDataSend = false;
                responseString = "exception occurred!";
                Log.e("ecxception",""+e);
            }
            return responseString;
        }

       @Override
        protected void onPostExecute(String result) {
            Log.e("upload offline", result);
            Log.e("upload offline", masterTblModel.getNewsId());
            Log.e("upload offline", masterTblModel.getUserID());
            Log.e("upload offline", masterTblModel.getTitle());
           Log.e("testing postexeciyte","");
            if (result.contains("Error occurred")){
                Toast.makeText(context,""+result,Toast.LENGTH_LONG).show();
                //showNotification("");
                setCompletedNotification("Uploading failed");
                isDataSend = false;
                imageCount = 1;
                vedioCount = 1;
                audioCount = 1;

                dh1.update_master("1", capturedLat, capturedLong, postedOn, masterTblModel.getNewsId());
                dh1.delete_postData(masterTblModel.getNewsId());
                dh1.delete_masterRowData(masterTblModel.getNewsId());
                setNotificationStatus();
            } else {
                Gson gson = new Gson();
                try {
                    Log.e("start","finish");
                    createPostResponseModel = gson.fromJson(result, CreatePostResponseModel.class);
                    if (createPostResponseModel.getStatus() == 200 && createPostResponseModel.getMessage().equals("success")){
                        showNotification("Your Post successfully updated online, it will be reviewed by the content team.");
                        setCompletedNotification("Uploaded successfully!");
                        isDataSend = false;
                        imageCount = 1;
                        vedioCount = 1;
                        audioCount = 1;
                        setNotificationStatus();
                    }else {
                        setCompletedNotification("Uploading failed");
                        isDataSend = false;
                        imageCount = 1;
                        vedioCount = 1;
                        audioCount = 1;
                        dh1.update_master("1", capturedLat, capturedLong, postedOn, masterTblModel.getNewsId());
                        dh1.delete_postData(masterTblModel.getNewsId());
                        dh1.delete_masterRowData(masterTblModel.getNewsId());
                        setNotificationStatus();
                    }
                }catch (Exception e){

                }
            }
            super.onPostExecute(result);
        }
    }

}
     /*  11-27 15:29:42.137 32554-32554/com.abacusdesk.instafeed.instafeed
        E/upload offline:
        {"status":200,"message":"error","data":{"error":"Enter proper details"}}  */
