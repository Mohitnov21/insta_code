package Api;


import com.netmax.user.rivercomic.Activitys.forgot;
import com.netmax.user.rivercomic.Activitys.getTypeandidddd;

import model.Credentials;
import model.Home_serise_episode;
import model.Inkey;
import model.TopSerch;
import model.checkemailmodel;
import model.checkstatusmodel;
import model.comic_book;
import model.delwishlist;
import model.detalispagemodel;
import model.emailcjeckkey;
import model.episode_details_model;
import model.findserchdata;
import model.forgotcredentials;
import model.forgotmodel;
import model.getID;
import model.getInventation;
import model.getRatingmodel;
import model.getSort;
import model.getTypeandid;
import model.getdatareview;
import model.homepagemodel;
import model.invoicekry;
import model.legalmodel;
import model.legaltype;
import model.loginmodel;
import model.motion_books;
import model.paymentkey;
import model.planmodel;
import model.playid;
import model.ratingkey;
import model.redeecodemodel;
import model.registerdata;
import model.reviewmodel;
import model.reviewsetresponcedata;
import model.serchdataC;
import model.signupmodel;
import model.sort_by_book;
import model.sort_by_book_key;
import model.sort_motion_book;
import model.sortmodel;
import model.updatepasskey;
import model.upgrademodel;
import model.userkey;
import model.vediomodel;
import model.wishlistdata;
import model.wishlistmodel;
import retrofit2.http.Body;
import retrofit2.http.POST;


/**
 * Created by ${Gaurav} on 6/10/2017.
 */
public interface RequestInterface {




    @POST("login/login/")
    ErrorCallback.MyCall<loginmodel> login(@Body Credentials credentials);


    @POST("forgotPass/forgot_pass/")
    ErrorCallback.MyCall<forgotmodel> forgot(@Body forgotcredentials fcredentials);


    @POST("registration/register/")
    ErrorCallback.MyCall<forgotmodel> register(@Body forgotcredentials fcredentials);


    @POST("invitationCode/get_inviation_code/")
    ErrorCallback.MyCall<redeecodemodel> get_inviation_code(@Body getInventation getInventationC);


    @POST("MyRiverComic/myRiverComicListing/")
    ErrorCallback.MyCall<motion_books> motionbook(@Body sort_by_book_key sort_by_book_keym);

    @POST("MyRiverComic/myRiverComicListing")
    ErrorCallback.MyCall<comic_book> motionbookc(@Body sort_by_book_key sort_by_book_keym);



    @POST("MyRiverComic/myRiverComicListing/")
    ErrorCallback.MyCall<sort_by_book> motionbookc_sort(@Body sort_by_book_key sort_by_book_keym);
    //http://lawvedic.com/crm/content/data/admin/index.php/api/MyRiverComic/myRiverComicListing

    @POST("MyRiverComic/myRiverComicListing/")
    ErrorCallback.MyCall<sort_motion_book> comic_book_sort(@Body sort_by_book_key sort_by_book_keym);

    @POST("homeCategory/listing/")
    ErrorCallback.MyCall<homepagemodel> homepage();


    @POST("seriesEpiosdeListing/listing/")
    ErrorCallback.MyCall<Home_serise_episode> Episode_series(@Body getID getIDC);

    @POST("comicDetails/details/")
    ErrorCallback.MyCall<detalispagemodel> get_detalis(@Body getTypeandid getTypeandidC);
    @POST("comicDetails/details/")
    ErrorCallback.MyCall<episode_details_model> get_detalisEpisode(@Body getTypeandid getTypeandidC);


    @POST("comicDetails/details/")
    ErrorCallback.MyCall<sortmodel> get_sort(@Body getSort getSortC);



    @POST("wishlist/set_wishlist_data/")
    ErrorCallback.MyCall<forgotmodel> wishlist(@Body getTypeandid getTypeandidC);

    @POST("download/set_download_count/")
    ErrorCallback.MyCall<forgotmodel> set_download_count(@Body getTypeandidddd getTypeandidC);


    @POST("wishlist/delete_wishlist_data/")
    ErrorCallback.MyCall<forgotmodel> wishlist_del(@Body delwishlist delwishlistC);


    @POST("wishlist/get_wishlist_data/")
    ErrorCallback.MyCall<wishlistmodel> get_wishlist_data(@Body wishlistdata wishlistdataC);

    @POST("Cmspages/cmslist/")
    ErrorCallback.MyCall<legalmodel> get_legal(@Body legaltype legaltypeC);

    @POST("Reviews/getReviewList/")
    ErrorCallback.MyCall<getRatingmodel> review_get(@Body getdatareview getdatareviewC);


    @POST("Reviews/setReviewData/")
    ErrorCallback.MyCall<forgotmodel> review_set(@Body reviewsetresponcedata reviewsetresponcedataC);

   @POST("search/topsearchData/")
    ErrorCallback.MyCall<TopSerch> top_serch();


    @POST("search/searchData/")
    ErrorCallback.MyCall<findserchdata> serch_data(@Body serchdataC serchdataCC);


    @POST("Plan/plan_list/")
    ErrorCallback.MyCall<planmodel> Plan();
    @POST("invitationCode/set_inviation_code/")
    ErrorCallback.MyCall<redeecodemodel> set_inviation_code(@Body Inkey mInkey);



    @POST("registration/register/")
    ErrorCallback.MyCall<signupmodel> register(@Body registerdata mregisterdata);




    @POST("panel/episode_panel_list/")
    ErrorCallback.MyCall<vediomodel> episode_panel_list(@Body playid mplayid);

    @POST("rating/set_rating/")
    ErrorCallback.MyCall<forgotmodel> set_rating(@Body ratingkey mratingkey);


    @POST("Subscription/check_user_subscription/")
    ErrorCallback.MyCall<checkstatusmodel> checkstatus(@Body userkey mruserkey);


    @POST("Subscription/subscription_payment_popup/")
    ErrorCallback.MyCall<upgrademodel> subscription_payment_popup(@Body invoicekry minvoicekry);

    @POST("Subscription/update_user_subscription")
    ErrorCallback.MyCall<forgotmodel> update_user_subscription(@Body paymentkey mpaymentkey);


    @POST("invitationCode/check_useremail_exists")
    ErrorCallback.MyCall<checkemailmodel> check_useremail_exists(@Body emailcjeckkey memailcjeckkey);
    @POST("  forgotPass/update_forgot_password")
    ErrorCallback.MyCall<forgotmodel> update_forgot_password(@Body updatepasskey mupdatepasskey);



}
