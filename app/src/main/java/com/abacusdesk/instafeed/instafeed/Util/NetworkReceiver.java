package com.abacusdesk.instafeed.instafeed.Util;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.util.Log;

/**
 * Created by spsuser4 on 6/30/2016.
 */

public class NetworkReceiver extends BroadcastReceiver {

    private static boolean firstConnect = true;

    @Override
    public void onReceive(Context context, Intent intent) {
        boolean isConnected =isNetworkConnected(context);
        if (isConnected) {
            if(firstConnect) {
                firstConnect = false;
                Intent serviceIntent = new Intent(context,SendOfflineData.class);
                context.startService(serviceIntent);
                Log.i("NET", "connecte123 " + isConnected);
            }
        }
        else{
            firstConnect= true;
            Log.i("NET", "not connecte " + isConnected);
        }
    }

    public static NetworkInfo getNetworkInfo(Context context){
        ConnectivityManager connectivityManager = (ConnectivityManager)
                context.getSystemService(Context.CONNECTIVITY_SERVICE );
        return connectivityManager.getActiveNetworkInfo();
    }

    public static boolean isNetworkConnected(Context context){
        NetworkInfo info = getNetworkInfo(context);
        return (info != null && info.isConnected());
    }

}
