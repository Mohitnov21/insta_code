package com.abacusdesk.instafeed.instafeed.model;

import java.util.List;

/**
 * Created by abacusdesk on 2017-08-08.
 */

public class Petitiondetail_model {


    /**
     * status : 200
     * message : success
     * data : [{"id":"7","title":"People do MISUSE of triple talaq.","description":"Talaq is a right given to men by islam to divorce his wife in case if the marriage cant be continued for some reason. It is similar to Khula, a right given to muslim women to seperate from her husband if she feels they cant live together. \r\nThere is one significant different between Talaq & khula . In women\u2019s case Islam give her extra freedom and authority i.e A woman can divorce her husband (khula) with immediate effect.\r\nBut in case of the talaq, once given, the husband has to wait for three months.\r\nThe triple talaq doesnt mean saying or messaging \u2018talaq\u2019 three times and ending marriage. Rather it means the person has to wait for a period of three months. Within the stipulated time if there is change in mind or the concerned problem is resolved mutually, they sure can continue the marriage.\r\nDivorced women remain in waiting for three periods, and it is not lawful for them to conceal what Allah has created in their wombs if they believe in Allah and the Last Day. And their husbands have more right to take them back in this [period] if they want reconciliation. And due to the wives is similar to what is expected of them, according to what is reasonable. But the men have a degree over them [in responsibility and authority]. And Allah is Exalted in Might and Wise\u201d The Quran 2:228\r\n\r\nFor the second time (which is not bound to happen immediately) if they face an arduous problem, talaq can be pronounced again with the same procedure.\r\n\u201cDivorce is twice. Then, either keep [her] in an acceptable manner or release [her] with good treatment\u2026 The Quran 2:229\r\nIn the mean time it the responsibilty of the family members to try to reconcile them. The muslim clerics (The Jama\u2019th) can also be approached .\r\nThe third time will be the final chance given to a muslim. Things become totaly tough for the husband.\r\n\r\nBut unfortunately people are missing it for their own benefits. Many Muslims had misunderstood meaning of Triple Talaq and practicing it in wrong manner. Now a days, many male couple started giving Triple Talaq to their woman just by saying talaq 3 times instantaneous through social platform namely Skype, Facebook, Watsapp etc. But, this is against Islamic way of giving Talaq. And, I term this practise as \u201cInstantaneous Triple Talaq\u201d rather \u201cTriple Talaq\u201d.\r\nThey don\u2019t even release talaq (Divorce ) is the most hated thing by Allah . Islam is spiritually against divorce \r\n","short_description":"Talaq is a right given to men by islam to divorce his wife in case if the marriage cant be continued for some reason. It is similar to Khula, a right given to muslim women to seperate from her husband if she feels they c","total_signs":"3","total_comments":"1","total_views":"348","user_id":"2","slug":"people-do-misuse-of-triple-talaq-911382087","dt_added":"2017-05-16 12:05:24","dt_modified":"2017-05-16 12:05:24","status":"A","first_name":"Instafeed","last_name":" ","nickname":"Instafeed","avatar":"http://instafeed.org/storage/avatar/80x80-d6a43e12b07d7a6cf5c0fca593aff5dc.jpg","username":"instafeed","location_id":"187","location":"Faridabad"},{"comments":[{"id":"11","comment":"Rights of an individual should not vary as per his or her religion. No personal law is above constitution of India. Divorce is a legal matter, all marriages must be registered and all divorce issues will be addressed in courts of law.\r\n\r\nIn democracy there is no scope of triple talak.","petition_id":"7","user_id":"4","comment_id":null,"dt_added":"2017-05-17 12:06:20","dt_modified":"2017-05-17 12:07:19","status":"A"}]},{"images":[{"id":"3","image":"http://instafeed.org/storage/petition_pics/256x170-f752eb584d5b76d116a5c2074880e236.jpg","image_zoom":"http://instafeed.org/storage/petition_pics/748x420-f752eb584d5b76d116a5c2074880e236.jpg","image_original":"http://instafeed.org/storage/petition_pics/f752eb584d5b76d116a5c2074880e236.jpg","image_100x100":"http://instafeed.org/storage/petition_pics/100x100-f752eb584d5b76d116a5c2074880e236.jpg","image_256x170":"http://instafeed.org/storage/petition_pics/256x170-f752eb584d5b76d116a5c2074880e236.jpg","image_330x210":"http://instafeed.org/storage/petition_pics/330x210-f752eb584d5b76d116a5c2074880e236.jpg","image_360x290":"http://instafeed.org/storage/petition_pics/360x290-f752eb584d5b76d116a5c2074880e236.jpg"}]}]
     */

    private int status;
    private String message;
    private List<DataBean> data;

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public List<DataBean> getData() {
        return data;
    }

    public void setData(List<DataBean> data) {
        this.data = data;
    }

    public static class DataBean {
        /**
         * id : 7
         * title : People do MISUSE of triple talaq.
         * description : Talaq is a right given to men by islam to divorce his wife in case if the marriage cant be continued for some reason. It is similar to Khula, a right given to muslim women to seperate from her husband if she feels they cant live together.
         There is one significant different between Talaq & khula . In women’s case Islam give her extra freedom and authority i.e A woman can divorce her husband (khula) with immediate effect.
         But in case of the talaq, once given, the husband has to wait for three months.
         The triple talaq doesnt mean saying or messaging ‘talaq’ three times and ending marriage. Rather it means the person has to wait for a period of three months. Within the stipulated time if there is change in mind or the concerned problem is resolved mutually, they sure can continue the marriage.
         Divorced women remain in waiting for three periods, and it is not lawful for them to conceal what Allah has created in their wombs if they believe in Allah and the Last Day. And their husbands have more right to take them back in this [period] if they want reconciliation. And due to the wives is similar to what is expected of them, according to what is reasonable. But the men have a degree over them [in responsibility and authority]. And Allah is Exalted in Might and Wise” The Quran 2:228

         For the second time (which is not bound to happen immediately) if they face an arduous problem, talaq can be pronounced again with the same procedure.
         “Divorce is twice. Then, either keep [her] in an acceptable manner or release [her] with good treatment… The Quran 2:229
         In the mean time it the responsibilty of the family members to try to reconcile them. The muslim clerics (The Jama’th) can also be approached .
         The third time will be the final chance given to a muslim. Things become totaly tough for the husband.

         But unfortunately people are missing it for their own benefits. Many Muslims had misunderstood meaning of Triple Talaq and practicing it in wrong manner. Now a days, many male couple started giving Triple Talaq to their woman just by saying talaq 3 times instantaneous through social platform namely Skype, Facebook, Watsapp etc. But, this is against Islamic way of giving Talaq. And, I term this practise as “Instantaneous Triple Talaq” rather “Triple Talaq”.
         They don’t even release talaq (Divorce ) is the most hated thing by Allah . Islam is spiritually against divorce

         * short_description : Talaq is a right given to men by islam to divorce his wife in case if the marriage cant be continued for some reason. It is similar to Khula, a right given to muslim women to seperate from her husband if she feels they c
         * total_signs : 3
         * total_comments : 1
         * total_views : 348
         * user_id : 2
         * slug : people-do-misuse-of-triple-talaq-911382087
         * dt_added : 2017-05-16 12:05:24
         * dt_modified : 2017-05-16 12:05:24
         * status : A
         * first_name : Instafeed
         * last_name :
         * nickname : Instafeed
         * avatar : http://instafeed.org/storage/avatar/80x80-d6a43e12b07d7a6cf5c0fca593aff5dc.jpg
         * username : instafeed
         * location_id : 187
         * location : Faridabad
         * comments : [{"id":"11","comment":"Rights of an individual should not vary as per his or her religion. No personal law is above constitution of India. Divorce is a legal matter, all marriages must be registered and all divorce issues will be addressed in courts of law.\r\n\r\nIn democracy there is no scope of triple talak.","petition_id":"7","user_id":"4","comment_id":null,"dt_added":"2017-05-17 12:06:20","dt_modified":"2017-05-17 12:07:19","status":"A"}]
         * images : [{"id":"3","image":"http://instafeed.org/storage/petition_pics/256x170-f752eb584d5b76d116a5c2074880e236.jpg","image_zoom":"http://instafeed.org/storage/petition_pics/748x420-f752eb584d5b76d116a5c2074880e236.jpg","image_original":"http://instafeed.org/storage/petition_pics/f752eb584d5b76d116a5c2074880e236.jpg","image_100x100":"http://instafeed.org/storage/petition_pics/100x100-f752eb584d5b76d116a5c2074880e236.jpg","image_256x170":"http://instafeed.org/storage/petition_pics/256x170-f752eb584d5b76d116a5c2074880e236.jpg","image_330x210":"http://instafeed.org/storage/petition_pics/330x210-f752eb584d5b76d116a5c2074880e236.jpg","image_360x290":"http://instafeed.org/storage/petition_pics/360x290-f752eb584d5b76d116a5c2074880e236.jpg"}]
         */

        private String id;
        private String title;
        private String description;
        private String short_description;
        private String total_signs;
        private String total_comments;
        private String total_views;
        private String user_id;
        private String slug;
        private String dt_added;
        private String dt_modified;
        private String status;
        private String first_name;
        private String last_name;
        private String nickname;
        private String avatar;
        private String username;
        private String location_id;
        private String location;
        private List<CommentsBean> comments;
        private List<ImagesBean> images;

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getTitle() {
            return title;
        }

        public void setTitle(String title) {
            this.title = title;
        }

        public String getDescription() {
            return description;
        }

        public void setDescription(String description) {
            this.description = description;
        }

        public String getShort_description() {
            return short_description;
        }

        public void setShort_description(String short_description) {
            this.short_description = short_description;
        }

        public String getTotal_signs() {
            return total_signs;
        }

        public void setTotal_signs(String total_signs) {
            this.total_signs = total_signs;
        }

        public String getTotal_comments() {
            return total_comments;
        }

        public void setTotal_comments(String total_comments) {
            this.total_comments = total_comments;
        }

        public String getTotal_views() {
            return total_views;
        }

        public void setTotal_views(String total_views) {
            this.total_views = total_views;
        }

        public String getUser_id() {
            return user_id;
        }

        public void setUser_id(String user_id) {
            this.user_id = user_id;
        }

        public String getSlug() {
            return slug;
        }

        public void setSlug(String slug) {
            this.slug = slug;
        }

        public String getDt_added() {
            return dt_added;
        }

        public void setDt_added(String dt_added) {
            this.dt_added = dt_added;
        }

        public String getDt_modified() {
            return dt_modified;
        }

        public void setDt_modified(String dt_modified) {
            this.dt_modified = dt_modified;
        }

        public String getStatus() {
            return status;
        }

        public void setStatus(String status) {
            this.status = status;
        }

        public String getFirst_name() {
            return first_name;
        }

        public void setFirst_name(String first_name) {
            this.first_name = first_name;
        }

        public String getLast_name() {
            return last_name;
        }

        public void setLast_name(String last_name) {
            this.last_name = last_name;
        }

        public String getNickname() {
            return nickname;
        }

        public void setNickname(String nickname) {
            this.nickname = nickname;
        }

        public String getAvatar() {
            return avatar;
        }

        public void setAvatar(String avatar) {
            this.avatar = avatar;
        }

        public String getUsername() {
            return username;
        }

        public void setUsername(String username) {
            this.username = username;
        }

        public String getLocation_id() {
            return location_id;
        }

        public void setLocation_id(String location_id) {
            this.location_id = location_id;
        }

        public String getLocation() {
            return location;
        }

        public void setLocation(String location) {
            this.location = location;
        }

        public List<CommentsBean> getComments() {
            return comments;
        }

        public void setComments(List<CommentsBean> comments) {
            this.comments = comments;
        }

        public List<ImagesBean> getImages() {
            return images;
        }

        public void setImages(List<ImagesBean> images) {
            this.images = images;
        }

        public static class CommentsBean {
            /**
             * id : 11
             * comment : Rights of an individual should not vary as per his or her religion. No personal law is above constitution of India. Divorce is a legal matter, all marriages must be registered and all divorce issues will be addressed in courts of law.

             In democracy there is no scope of triple talak.
             * petition_id : 7
             * user_id : 4
             * comment_id : null
             * dt_added : 2017-05-17 12:06:20
             * dt_modified : 2017-05-17 12:07:19
             * status : A
             */
//"avatar": "http://instafeed.org/storage/avatar/80x80-8da18bfc13125d4737b1a28633ec412b.jpg",
 //       "username": "gautamrampal"
            private String id;
            private String comment;
            private String petition_id;
            private String user_id;
            private Object comment_id;
            private String dt_added;
            private String dt_modified;

            public String getAvatar() {
                return avatar;
            }

            public void setAvatar(String avatar) {
                this.avatar = avatar;
            }

            public String getUsername() {
                return username;
            }

            public void setUsername(String username) {
                this.username = username;
            }

            private String avatar;
            private String username;
            private String status;


            public String getId() {
                return id;
            }

            public void setId(String id) {
                this.id = id;
            }

            public String getComment() {
                return comment;
            }

            public void setComment(String comment) {
                this.comment = comment;
            }

            public String getPetition_id() {
                return petition_id;
            }

            public void setPetition_id(String petition_id) {
                this.petition_id = petition_id;
            }

            public String getUser_id() {
                return user_id;
            }

            public void setUser_id(String user_id) {
                this.user_id = user_id;
            }

            public Object getComment_id() {
                return comment_id;
            }

            public void setComment_id(Object comment_id) {
                this.comment_id = comment_id;
            }

            public String getDt_added() {
                return dt_added;
            }

            public void setDt_added(String dt_added) {
                this.dt_added = dt_added;
            }

            public String getDt_modified() {
                return dt_modified;
            }

            public void setDt_modified(String dt_modified) {
                this.dt_modified = dt_modified;
            }

            public String getStatus() {
                return status;
            }

            public void setStatus(String status) {
                this.status = status;
            }
        }

        public static class ImagesBean {
            /**
             * id : 3
             * image : http://instafeed.org/storage/petition_pics/256x170-f752eb584d5b76d116a5c2074880e236.jpg
             * image_zoom : http://instafeed.org/storage/petition_pics/748x420-f752eb584d5b76d116a5c2074880e236.jpg
             * image_original : http://instafeed.org/storage/petition_pics/f752eb584d5b76d116a5c2074880e236.jpg
             * image_100x100 : http://instafeed.org/storage/petition_pics/100x100-f752eb584d5b76d116a5c2074880e236.jpg
             * image_256x170 : http://instafeed.org/storage/petition_pics/256x170-f752eb584d5b76d116a5c2074880e236.jpg
             * image_330x210 : http://instafeed.org/storage/petition_pics/330x210-f752eb584d5b76d116a5c2074880e236.jpg
             * image_360x290 : http://instafeed.org/storage/petition_pics/360x290-f752eb584d5b76d116a5c2074880e236.jpg
             */

            private String id;
            private String image;
            private String image_zoom;
            private String image_original;
            private String image_100x100;
            private String image_256x170;
            private String image_330x210;
            private String image_360x290;

            public String getId() {
                return id;
            }

            public void setId(String id) {
                this.id = id;
            }

            public String getImage() {
                return image;
            }

            public void setImage(String image) {
                this.image = image;
            }

            public String getImage_zoom() {
                return image_zoom;
            }

            public void setImage_zoom(String image_zoom) {
                this.image_zoom = image_zoom;
            }

            public String getImage_original() {
                return image_original;
            }

            public void setImage_original(String image_original) {
                this.image_original = image_original;
            }

            public String getImage_100x100() {
                return image_100x100;
            }

            public void setImage_100x100(String image_100x100) {
                this.image_100x100 = image_100x100;
            }

            public String getImage_256x170() {
                return image_256x170;
            }

            public void setImage_256x170(String image_256x170) {
                this.image_256x170 = image_256x170;
            }

            public String getImage_330x210() {
                return image_330x210;
            }

            public void setImage_330x210(String image_330x210) {
                this.image_330x210 = image_330x210;
            }

            public String getImage_360x290() {
                return image_360x290;
            }

            public void setImage_360x290(String image_360x290) {
                this.image_360x290 = image_360x290;
            }
        }
    }
}
