package com.abacusdesk.instafeed.instafeed.fragments;

import android.Manifest;
import android.app.Activity;
import android.app.Dialog;
import android.content.ClipData;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.ThumbnailUtils;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.InputType;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.view.inputmethod.EditorInfo;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.GridLayout;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.abacusdesk.instafeed.instafeed.NavDrawerActivity;
import com.abacusdesk.instafeed.instafeed.R;
import com.abacusdesk.instafeed.instafeed.SoundRecordingActivity;
import com.abacusdesk.instafeed.instafeed.Util.AppLocationService;
import com.abacusdesk.instafeed.instafeed.Util.CommonDirectories;
import com.abacusdesk.instafeed.instafeed.Util.CommonFunctions;
import com.abacusdesk.instafeed.instafeed.Util.DbHandler;
import com.abacusdesk.instafeed.instafeed.Util.FilePath;
import com.abacusdesk.instafeed.instafeed.Util.Fused;
import com.abacusdesk.instafeed.instafeed.Util.GpsActivation;
import com.abacusdesk.instafeed.instafeed.Util.RealPathUtil;
import com.abacusdesk.instafeed.instafeed.Util.SaveSharedPreference;
import com.abacusdesk.instafeed.instafeed.Util.SendOfflineData;
import com.abacusdesk.instafeed.instafeed.Util.TimeUtilities;
import com.abacusdesk.instafeed.instafeed.adapter.ImageAdapter;
import com.abacusdesk.instafeed.instafeed.custom_dialog.Dialogs;
import com.abacusdesk.instafeed.instafeed.model.CreatePostResponseModel;
import com.abacusdesk.instafeed.instafeed.model.GridModel;
import com.abacusdesk.instafeed.instafeed.model.MediaDataModel;
import com.abacusdesk.instafeed.instafeed.model.NewCategoryListModel;
import com.abacusdesk.instafeed.instafeed.services.TrackGPS;
import com.abacusdesk.instafeed.instafeed.webservice.MultipartRequest;
import com.android.volley.RequestQueue;
import com.bumptech.glide.Glide;
import com.squareup.picasso.Picasso;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URL;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;

import java.util.logging.Handler;
import java.util.logging.LogRecord;

import static android.app.Activity.RESULT_CANCELED;
import static android.app.Activity.RESULT_OK;
import static com.abacusdesk.instafeed.instafeed.Util.ScreenConstants.ACTIVITY_RECORD_SOUND;
import static com.abacusdesk.instafeed.instafeed.Util.ScreenConstants.GALLERY_MODE;
import static com.abacusdesk.instafeed.instafeed.Util.ScreenConstants.GALLERY_MODE_NO;
import static com.abacusdesk.instafeed.instafeed.Util.ScreenConstants.GALLERY_MODE_YES;
import static com.abacusdesk.instafeed.instafeed.Util.ScreenConstants.KEY_AUDIO_FILE_NAME;
import static com.abacusdesk.instafeed.instafeed.Util.ScreenConstants.KEY_AUDIO_FILE_PATH;
import static com.abacusdesk.instafeed.instafeed.Util.ScreenConstants.MEDIA_FILE_NAME;
import static com.abacusdesk.instafeed.instafeed.Util.ScreenConstants.MEDIA_FILE_PATH;
import static com.abacusdesk.instafeed.instafeed.Util.ScreenConstants.MEDIA_TYPE;
import static com.abacusdesk.instafeed.instafeed.Util.ScreenConstants.MEDIA_TYPE_AUDIO;
import static com.abacusdesk.instafeed.instafeed.Util.ScreenConstants.MEDIA_TYPE_IMAGE;
import static com.abacusdesk.instafeed.instafeed.Util.ScreenConstants.MEDIA_TYPE_VIDEO;
import static com.abacusdesk.instafeed.instafeed.Util.ScreenConstants.PICK_AUDIO_REQUEST;
import static com.abacusdesk.instafeed.instafeed.Util.ScreenConstants.PICK_IMAGE_MULTIPLE;
import static com.abacusdesk.instafeed.instafeed.Util.ScreenConstants.REQUEST_MICROPHONE;
import static com.abacusdesk.instafeed.instafeed.Util.ScreenConstants.REQUEST_VIDEO_CAPTURE;

/**
 * Created by Administrator on 5/28/2017.
 */

public class CreateFragment extends Fragment implements View.OnClickListener {

    private GridLayout conatiner_imageview;
    //RadioButton rb_yes,rb_no;
    //RadioGroup mRgCategory;
    //RadioButton mRbNews, mRbTalent, mRbExplore;
    TextView tv_camera_img, tv_gallery_img, tv_camera_vedio, tv_gallery_vedio, tv_camera_audio, tv_gallery_audio, tv_cancel;
    private String mediaFilePath, mediaType, mediaFileName;
    //Dialog alertDialog;
    //AlertDialog alerttDialog;
    public static final int REQUEST_CAMERA = 3, SELECT_FILE = 4;
    public static String DIRECTORY_NAME = "InstaFeed";
    public static String VIDEO_DIRECTORY_NAME = "InstaFeedVedio";
    public static String AUDIO_DIRECTORY_NAME = "InstaFeedAudio";
    static public boolean active = false;
    private String  uid_image_path, video_path, newsCategoryId, fileName;
    Fused fused;
    private File photoFile = null;
    public static Dialog gpsDialog;
    private File videoFileDirectory;
    ScrollView scrollView;
    private int video_flag = -1;
    int imageCount = 1, vedioCount = 1, audioCount = 1;
    CommonFunctions commonFunctions;
    ArrayList<String> filePathList;
    //Button btn_createPost;
    private static boolean isDataSend;
    private TrackGPS gps;
    //  Double longitude, latitude;
    GridView gridView;
    public static int MAKE_VIDEO_REQUEST = 6, PICK_VIDEO_REQUEST = 5;
    EditText et_postTitle;//, et_description;
    private File videoFile;
    ArrayList<GridModel> imgList;

    private ImageAdapter adapter;
    private RequestQueue requestQueue;
    MultipartRequest multipartRequest;
    CreatePostResponseModel createPostResponseModel;

    View progressView;
    ProgressBar progressBar;
    ArrayList<String> newCategoryList;
    ArrayList<String> newCategoryIdList;
    NewCategoryListModel newCategoryListModel;
    Spinner sp_categoryList;
    public int isRetry = 0;
    //private ImageView vPickvideo;
    DbHandler dbHandler;
    private AppLocationService appLocationService;
    String mCurrentPhotoPath;
    private String Filename;
    private int setCanceltag;

    ArrayList<String> array_typespinner=new ArrayList<String>();
    //TextInputLayout txtinputTitle, txtinputDescription;
    ArrayList<RelativeLayout> relativeList;
    ArrayList<String> cancelList;
    //LinearLayout linearLayout;
    String source_screen = "";
    public static String isAnonymous = "N";
    private ProgressBar progressBar1;
    private TextView txtPercentage;
    long totalSize = 0;
    // CheckBox checkBox_anonym;
    private int mCategoryType = 1;
    String arrat;
    Bitmap bMap;
    private ArrayList<MediaDataModel> imageArray;
    private String imageEncoded;
    private ImageView img_user;
    private TextView txt_username;
    private Spinner spin_type;
    private TextView btn_createPost;
    LinearLayout ln_recordvoice,ln_gallery,ln_takephoto,ln_recordvideo,ln_audio;
    ImageView imgback;
    String generated_sub="";
    String generated_desc="";

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = null;
        view = inflater.inflate(R.layout.upload_fragnew, container, false);

        //setHasOptionsMenu(true);
        /* Toolbar toolbar = (Toolbar) view.findViewById(R.id.toolbar);
        //set toolbar appearance
        toolbar.setBackgroundColor(getResources().getColor(R.color.black));
        toolbar.setTitle("Create new feed");*/

        //for crate home button
        //AppCompatActivity activity = (AppCompatActivity) getActivity();
        //activity.setSupportActionBar(toolbar);
        //activity.getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        //NavDrawerActivity.toolbar.setBackgroundColor(getResources().getColor(R.color.black));
        Log.e("activity","attached");
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        setCanceltag = 0;

       // Toolbar toolbar = (Toolbar) getActivity().findViewById(R.id.toolbar);
        NavDrawerActivity.frame_bottom.setVisibility(View.GONE);
        scrollView = (ScrollView) view.findViewById(R.id.scrollView);

        //  linearLayout = (LinearLayout) view.findViewById(R.id.linear1);
        //  mRgCategory = (RadioGroup) view.findViewById(R.id.rb_category);
        //  mRbNews =     (RadioButton) view.findViewById(R.id.radio_news);
        //    mRbTalent =   (RadioButton) view.findViewById(R.id.radio_talents);
        //   mRbExplore =  (RadioButton) view.findViewById(R.id.radio_explorer);

        spin_type=(Spinner)view.findViewById(R.id.spin_type);
        set_typespinner();
        btn_createPost=(TextView)view.findViewById(R.id.txt_submitfeed);
        txt_username=(TextView)view.findViewById(R.id.txt_username);
        img_user=(ImageView)view.findViewById(R.id.user_img);
        imgback=(ImageView)view.findViewById(R.id.img_back);
        imgback.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (SuccessDialogFragment.state==false){
                    getActivity().getSupportFragmentManager().popBackStack();
                }else {
                    getActivity().finish();
                  NavDrawerActivity.frame_bottom.setVisibility(View.VISIBLE);
                 }

            }
        });

        set_userdetail();
        ln_recordvoice=(LinearLayout)view.findViewById(R.id.ln_recordvoice);
        ln_recordvideo=(LinearLayout)view.findViewById(R.id.ln_recordvideo);
        ln_takephoto  =(LinearLayout)view.findViewById(R.id.ln_takephoto);
        ln_gallery    =(LinearLayout)view.findViewById(R.id.ln_gallery);
        ln_audio      =(LinearLayout)view.findViewById(R.id.ln_audio);

        ln_recordvoice.setOnClickListener(this);
        ln_recordvideo.setOnClickListener(this);
        ln_takephoto.  setOnClickListener(this);
        ln_gallery.    setOnClickListener(this);
        ln_audio.      setOnClickListener(this);

       // mRbNews.setChecked(true);
        relativeList = new ArrayList<>();
        cancelList = new ArrayList<>();
        imgList = new ArrayList<>();

        relativeList.clear();
        imgList.clear();
        cancelList.clear();

        /*if (NavDrawerActivity.toolbar!=null){
            NavDrawerActivity.toolbar.setLogo(null);
            NavDrawerActivity.frame_bottom.setVisibility(View.GONE);
            //NavDrawerActivity.toolbar.setBackground(null);
            NavDrawerActivity.toolbar.setTitle("Create new feed");
            NavDrawerActivity.toolbar.setBackgroundColor(getResources().getColor(R.color.black));
        }*/

        fused = new Fused(getContext(), 1);
        fused.onStart();

        dbHandler = new DbHandler(getActivity());
        dbHandler.open();

        if (!isViewShown) {
            // linearLayout.setOnClickListener(this);
        }

        //vPickvideo = (ImageView) view.findViewById(R.id.vPickVideo);
       // btn_createPost = (Button) view.findViewById(R.id.btn_createPost);

        et_postTitle = (EditText) view.findViewById(R.id.et_title);
       //  et_description = (EditText) view.findViewById(R.id.et_description);

       // et_postTitle.setImeOptions(EditorInfo.IME_ACTION_NEXT);
        et_postTitle.setRawInputType(InputType.TYPE_CLASS_TEXT|
                InputType.TYPE_TEXT_FLAG_CAP_SENTENCES);

       /* et_description.setImeOptions(EditorInfo.IME_ACTION_DONE);
        et_description.setRawInputType(InputType.TYPE_CLASS_TEXT| InputType.TYPE_TEXT_FLAG_CAP_SENTENCES);
       */
       // checkBox_anonym = (CheckBox) view.findViewById(R.id.check_anonymous);

        setPermission(view);

        progressBar = (ProgressBar) view.findViewById(R.id.progress);
        progressView = (View) view.findViewById(R.id.view_progress);
        sp_categoryList = (Spinner) view.findViewById(R.id.spin_category);
        spin_type.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                newsCategoryId = newCategoryIdList.get(i);
                 if (array_typespinner.get(i).equalsIgnoreCase("ifeed")) {
                    mCategoryType = 1;
                    //checkBox_anonym.setVisibility(View.VISIBLE);
                    btn_createPost.setEnabled(true);
                    //vPickvideo.setEnabled(true);
                } else if (array_typespinner.get(i).equalsIgnoreCase("talents")) {
                    mCategoryType = 2;
                    //checkBox_anonym.setVisibility(View.GONE);
                    btn_createPost.setEnabled(true);
                    //vPickvideo.setEnabled(true);
                } else if (array_typespinner.get(i).equalsIgnoreCase("lifestyle")) {
                    mCategoryType = 3;
                    //checkBox_anonym.setVisibility(View.GONE);
                    btn_createPost.setEnabled(true);
                    //vPickvideo.setEnabled(true);
                } else {
                    Toast.makeText(getActivity(), "Invalid Category", Toast.LENGTH_SHORT).show();
                }
                setCategoryAdapter();
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        btn_createPost.setOnClickListener(this);
        /*checkBox_anonym.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    isAnonymous = "Y";
                } else {
                    isAnonymous = "N";
                }
             }
        });*/
   /*
        mRgCategory.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup radioGroup, int i) {
                if (mRbNews.isChecked()) {
                    mCategoryType = 1;
                    checkBox_anonym.setVisibility(View.VISIBLE);
                    btn_createPost.setEnabled(true);
                    vPickvideo.setEnabled(true);
                } else if (mRbTalent.isChecked()) {
                    mCategoryType = 2;
                    checkBox_anonym.setVisibility(View.GONE);
                    btn_createPost.setEnabled(true);
                    vPickvideo.setEnabled(true);
                } else if (mRbExplore.isChecked()) {
                    mCategoryType = 3;
                    checkBox_anonym.setVisibility(View.GONE);
                    btn_createPost.setEnabled(true);
                    vPickvideo.setEnabled(true);
                } else {
                    Toast.makeText(getActivity(), "Invalid Category", Toast.LENGTH_SHORT).show();
                }
                setCategoryAdapter();
            }
        });*/

        newCategoryList = new ArrayList<>();
        newCategoryIdList = new ArrayList<>();
        // fetchCategoryList();
        setCategoryAdapter();
        getActivity().getWindow()
                .setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_PAN |
                        WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);

      /*  et_description.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if (v.getId() == R.id.et_description) {
                    v.getParent().requestDisallowInterceptTouchEvent(true);
                    switch (event.getAction() & MotionEvent.ACTION_MASK) {
                        case MotionEvent.ACTION_UP:
                            v.getParent().requestDisallowInterceptTouchEvent(false);
                            break;
                    }
                }
                return false;
            }
        });*/

        sp_categoryList.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                newsCategoryId = newCategoryIdList.get(i);
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });


        conatiner_imageview = (GridLayout) view.findViewById(R.id.container_image_view);

        commonFunctions = new CommonFunctions();
        filePathList = new ArrayList<>();
        ArrayList<HashMap<String,String>>arrayPassed = new ArrayList<HashMap<String,String>>();
        try {
            if (getArguments() != null) {
                try {
                    if (getArguments().containsKey("arraylist")) {
                        arrayPassed= (ArrayList<HashMap<String,String>>) getArguments().getSerializable("arraylist");
                        for (int i=0;i<arrayPassed.size();i++){
                            if (arrayPassed.get(i).containsKey("image")){

                                imgList.add(new GridModel(MEDIA_TYPE_IMAGE, arrayPassed.get(i).get("image"),new File(arrayPassed.get(i).get("image")).getName()));
                                setImage(arrayPassed.get(i).get("image"));
                            } else if (arrayPassed.get(i).containsKey("video")){
                                arrat =arrayPassed.get(i).get("video");
                                //imgList.add(new GridModel(MEDIA_TYPE_VIDEO, arrat, new File(arrat).getName()));

                                bMap = ThumbnailUtils.createVideoThumbnail(arrat, MediaStore.Video.Thumbnails.MICRO_KIND);
                                Log.e("bitmap1",""+bMap);

                                if (bMap!=null)
                                {
                                    imgList.add(new GridModel(MEDIA_TYPE_VIDEO, arrat, new File(arrat).getName()));
                                    setVideo(bMap);
                                }else {
                                    imgList.add(new GridModel(MEDIA_TYPE_VIDEO, arrat, new File(arrat).getName()));
                                    setVideo_byUri(arrat);
                                }

                                Log.e("bitmap2",""+imgList+""+imgList.size());
                                Log.e("bitmap3",""+bMap);
                            } else if (arrayPassed.get(i).containsKey("audio")) {
                                imgList.add(new GridModel(MEDIA_TYPE_AUDIO, arrayPassed.get(i).get("audio"), new File(arrayPassed.get(i).get("audio")).getName()));
                                setAudio(new File(arrayPassed.get(i).get("audio")).getName(),arrayPassed.get(i).get("audio"));
                            }
                        }
                    } else if(getArguments().containsKey("imageList")){
                        imageArray= (ArrayList<MediaDataModel>) getArguments().getSerializable("imageList");
                        Log.e("arraypased",""+imageArray);
                        for (int i=0;i<imageArray.size();i++){
                            imgList.add(new GridModel(MEDIA_TYPE_IMAGE, imageArray.get(i).getImgPath(), imageArray.get(i).getFileName()));
                            setImage(imageArray.get(i).getImgPath());
                        }
                    }else {
                        Log.e("splash","called");
                        String mediaType = getArguments().getString(MEDIA_TYPE);
                        if (mediaType != null && mediaType.equals(MEDIA_TYPE_IMAGE)) {
                            mediaFilePath = getArguments().getString(MEDIA_FILE_PATH);
                            mediaFileName = getArguments().getString(MEDIA_FILE_NAME);
                            Log.e("imge path", "onCreateView: " + mediaFilePath);
                            imgList.add(new GridModel(mediaType, mediaFilePath, mediaFileName));
                            setImage(mediaFilePath);
                        } else if (mediaType != null && mediaType.equals(MEDIA_TYPE_VIDEO)) {
                            mediaFilePath = getArguments().getString(MEDIA_FILE_PATH);
                            mediaFileName = getArguments().getString(MEDIA_FILE_NAME);
                            Bitmap bMap = ThumbnailUtils.createVideoThumbnail(mediaFilePath, MediaStore.Video.Thumbnails.MICRO_KIND);
                            imgList.add(new GridModel(mediaType, mediaFilePath, mediaFileName));

                            setVideo(bMap);
                        } else if (mediaType != null && mediaType.equals(MEDIA_TYPE_AUDIO)) {
                            String AudioFileName = getArguments().getString(MEDIA_FILE_NAME);
                            String AudioFilePath = getArguments().getString(MEDIA_FILE_PATH);

                            Log.e("fragment param ", mediaType + "mediaFilePath" + AudioFilePath);
                            imgList.add(new GridModel(mediaType, AudioFilePath, AudioFileName));
                            setAudio(AudioFileName, AudioFilePath);
                        }
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        //vPickvideo.setOnClickListener(this);
    }


    /* @Override
    public void onResume() {
        super.onResume();
        ((AppCompatActivity)getActivity()).getSupportActionBar().hide();
    }

    @Override
    public void onStop() {
        super.onStop();
        ((AppCompatActivity)getActivity()).getSupportActionBar().show();
    }*/


   /* @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup parent, Bundle savedInstanceState)
    {
        View v = inflater.inflate(R.layout.library_fragment, parent, false);
        setHasOptionsMenu(true);
        return v;
    }*/


   private void set_userdetail(){
   if (getActivity()!=null)
       if (!SaveSharedPreference.getUserName(getContext()).isEmpty()
               && SaveSharedPreference.getUserName(getContext())!=null){
           String temp_str=SaveSharedPreference.getUserName(getContext()).toString().trim();
           temp_str = temp_str.substring(0,1).toUpperCase() + temp_str.substring(1);
           txt_username.setText(temp_str);
       }
      // txt_username.setText(SaveSharedPreference.getUserName(getContext()));
       if (SaveSharedPreference.getUserIMAGE(getContext())!=null)
       Picasso.with(getContext()).load(SaveSharedPreference
               .getUserIMAGE(getContext())).error(R.drawable.user).into(img_user);
   }

   private void set_typespinner(){
       array_typespinner.clear();
       array_typespinner.add("Ifeed");
       array_typespinner.add("Talents");
       array_typespinner.add("Lifestyle");
       ArrayAdapter<String> newCategoryAdapter =
               new ArrayAdapter<String>(getActivity(),
                       R.layout.spinner_item, R.id.sp_tv_view, array_typespinner);
       spin_type.setAdapter(newCategoryAdapter);

       Log.e("position spinner",""+HomeTab_fragment.positioncurrent);
           if (HomeTab_fragment.positioncurrent==0 || HomeTab_fragment.positioncurrent==1){
               spin_type.setSelection(0);
           }else if (HomeTab_fragment.positioncurrent==2){
               spin_type.setSelection(1);
           }else if (HomeTab_fragment.positioncurrent==3){
               spin_type.setSelection(2);
           }else {
           spin_type.setSelection(0);
       }
   }

    @Override
   public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.nav_drawer, menu);
        super.onCreateOptionsMenu(menu, inflater);
    }


    /*  @Override
    public boolean onTouchEvent(MotionEvent event) {
        switch (event.getAction()) {
            case MotionEvent.ACTION_DOWN:
                startY = event.getY();
                break;
            case MotionEvent.ACTION_UP: {
                float endY = event.getY();

                if (endY < startY) {
                    System.out.println("Move UP");
                    ll.setVisibility(View.VISIBLE);
                }
                else {
                    ll.setVisibility(View.GONE);
                }
            }
        }
        return true;
    }*/


    private void setCategoryAdapter() {
        newCategoryList.clear();
        newCategoryIdList.clear();
        Cursor m;
        switch (mCategoryType) {
            case 1:
                m = dbHandler.categroryData();
                break;
            case 2:
                m = dbHandler.talentCategroryData();
                break;
            case 3:
                m = dbHandler.exploreCategroryData();
                break;
            default:
                m = dbHandler.categroryData();
                break;
        }
        if (m.getCount() > 0) {
            try {
                if (m.moveToFirst()) {
                    do {
                        newCategoryList.add(m.getString(0));
                        newCategoryIdList.add(m.getString(1));
                    } while (m.moveToNext());
                    ArrayAdapter<String> newCategoryAdapter =
                            new ArrayAdapter<String>(getActivity(),
                                    R.layout.spinner_item, R.id.sp_tv_view, newCategoryList);
                    sp_categoryList.setAdapter(newCategoryAdapter);
                    newCategoryAdapter.notifyDataSetChanged();
                    sp_categoryList.setSelection(1);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    private void setPermission(View view) {
        if (ContextCompat.checkSelfPermission(getActivity(), Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
            //vPickvideo.setEnabled(false);
            ActivityCompat.requestPermissions(getActivity(), new String[]{Manifest.permission.CAMERA, Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.RECORD_AUDIO}, 0);
        }
    }

    private void setAudio(String fileName, String filePath) {
        filePathList.add(filePath);
        RelativeLayout imgLayout = new RelativeLayout(getActivity());
        RelativeLayout.LayoutParams params1 = new RelativeLayout.LayoutParams(200, 200);
        imgLayout.setLayoutParams(params1);
        try {
            final ImageView imageView_cancel = new ImageView(getActivity());
            RelativeLayout.LayoutParams lp_cancel = new RelativeLayout.LayoutParams(70, 70);
            lp_cancel.addRule(RelativeLayout.ALIGN_PARENT_RIGHT);
            lp_cancel.addRule(RelativeLayout.ALIGN_PARENT_TOP);
            imageView_cancel.setImageResource(R.drawable.cancel_icon);
            imageView_cancel.setLayoutParams(lp_cancel);
            imageView_cancel.setTag(setCanceltag++);

            ImageView imageView = new ImageView(getActivity());

            imageView.setPadding(10, 10, 10, 10);
            imageView.setLayoutParams(params1);
            imageView.setScaleType(ImageView.ScaleType.FIT_XY);
            imageView.setAdjustViewBounds(true);
            imageView.setImageResource(R.drawable.music_player_plus);

            imgLayout.addView(imageView);
            imgLayout.addView(imageView_cancel);
            conatiner_imageview.addView(imgLayout);
            relativeList.add(imgLayout);

            imageView_cancel.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    int position = (Integer) imageView_cancel.getTag();
                    Log.d("TAG", "onClick: " + position);
                    conatiner_imageview.removeView(relativeList.get(position));
                    Log.d("onClick aftere", "conatiner_imageview: " + conatiner_imageview.getChildCount());
                    cancelList.add(position + "");
                    imgList.set(position,null);
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.vPickVideo:
                // showMediaDialog();
                break;
            case R.id.linear1:
                break;

            case R.id.txt_submitfeed:
                Log.e("clcicked","button post");
                btn_createPost.setEnabled(false);
                gps = new TrackGPS(getActivity());
                if (gps.canGetLocation()) {
                    if (fused.lon != null && fused.lat != null && fused.lon != "0.0" && fused.lat != "0.0") {
                       Log.e("title and other",""+et_postTitle.getText().toString());
                        Log.e("title and newscat",""+newsCategoryId);
                        //Toast.makeText(getActivity(), "Longitude:" + fused.lon + "\nfused.lat:" + fused.lat, Toast.LENGTH_SHORT).show();
                        if (et_postTitle.getText().toString() != null && et_postTitle.getText().length() > 0) {
                            if (et_postTitle.getText().toString().length() > 3) {
                                if (newsCategoryId != null && newsCategoryId.length() > 0) {
                                    try {
                                        if (SaveSharedPreference.getUserID(getContext()).equals("")){
                                            Toast.makeText(getContext(),"please Login to continue", Toast.LENGTH_SHORT).show();
                                        }else {
                                            if (checkaudioexist()==1 && mCategoryType!=1){
                                                Log.e("hesdcsdcr ","ddsd");
                                                btn_createPost.setEnabled(true);
                                                Toast.makeText(getActivity(),"Audio upload is only available in news!", Toast.LENGTH_SHORT).show();
                                            }else {
                                                Log.e("her ","ddsd");
                                                if (et_postTitle.getText()
                                                        .toString().trim().length()>75){
                                                    String temp=et_postTitle.getText()
                                                            .toString().trim();
                                                    generated_sub=temp.substring(0,75);
                                                }else {
                                                    generated_sub=et_postTitle.getText()
                                                            .toString().trim();
                                                }
                                                generated_desc=et_postTitle.getText()
                                                        .toString().trim();
                                                savePost();
                                            }
                                        }
                                    } catch (Exception e) {
                                        e.printStackTrace();
                                    }
                                } else {
                                    btn_createPost.setEnabled(true);
                                    Toast.makeText(getActivity(), "Select Category", Toast.LENGTH_LONG).show();
                                }
                            }
                            else {
                                et_postTitle.setError(getResources().getString(R.string.not_valid_title));
                                btn_createPost.setEnabled(true);
                            }
                        } else {
                            et_postTitle.setError(getResources().getString(R.string.empty_field));
                            btn_createPost.setEnabled(true);
                        }
                    } else {
                        Toast.makeText(getActivity(), "Not Able to get location.please turn on your GPS", Toast.LENGTH_SHORT).show();
                        btn_createPost.setEnabled(true);
                    }
                } else {
                    new GpsActivation(getActivity()).enableGPS();
                    btn_createPost.setEnabled(true);
                }
                break;
            case R.id.ln_takephoto:
                if (ContextCompat.checkSelfPermission(getActivity(), Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
                    ActivityCompat.requestPermissions(getActivity(), new String[]{Manifest.permission.CAMERA}, 0);
                } else {
                    cameraIntent();
                }
                //alertDialog.dismiss();
                break;

            case R.id.ln_gallery:
                if (ContextCompat.checkSelfPermission(getActivity(), Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
                    ActivityCompat.requestPermissions(getActivity(), new String[]{Manifest.permission.CAMERA}, 0);

                } else {
                    galleryIntent();
                }
               // alertDialog.dismiss();
                break;

            case R.id.ln_recordvideo:
                if (ContextCompat.checkSelfPermission(getActivity(), Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
                    ActivityCompat.requestPermissions(getActivity(), new String[]{Manifest.permission.CAMERA}, 0);
                } else {
                    createVideo();
                }
                //alertDialog.dismiss();
                break;

            case R.id.tv_gallery_video:
                if (ContextCompat.checkSelfPermission(getActivity(), Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
                    ActivityCompat.requestPermissions(getActivity(), new String[]{Manifest.permission.CAMERA}, 0);
                } else {
                    picVideo();
                }
                //alertDialog.dismiss();
                break;

            case R.id.ln_recordvoice:

                if (ContextCompat.checkSelfPermission(getActivity(), Manifest.permission.RECORD_AUDIO) != PackageManager.PERMISSION_GRANTED) {

                    ActivityCompat.requestPermissions(getActivity(), new String[]{Manifest.permission.RECORD_AUDIO}, REQUEST_MICROPHONE);
                } else {
                    recordAudio();
                }

               // alertDialog.dismiss();
                break;

            case R.id.ln_audio:
                if (ContextCompat.checkSelfPermission(getActivity(), Manifest.permission.RECORD_AUDIO) != PackageManager.PERMISSION_GRANTED) {

                    ActivityCompat.requestPermissions(getActivity(), new String[]{Manifest.permission.RECORD_AUDIO}, REQUEST_MICROPHONE);
                } else {
                    picAudio();
                }
                //alertDialog.dismiss();
                break;

            case R.id.tv_cancel:
                //alertDialog.dismiss();

            default:
                break;
        }

    }

    private void savePost() {
        long id;
        if (mCategoryType==3){
            GALLERY_MODE = false;
            id = dbHandler.insertPost(CommonFunctions.getDeviceId(), SaveSharedPreference.getPrefToken(getActivity()), SaveSharedPreference.getUserID(getActivity()), "0", newsCategoryId, fused.lat, fused.lon, CommonFunctions.ConvertMilliSecondsToFormattedDate(),generated_sub, generated_desc, GALLERY_MODE_NO, isAnonymous, mCategoryType);
        }else if(mCategoryType==2){
             GALLERY_MODE = false;
            id = dbHandler.insertPost(CommonFunctions.getDeviceId(), SaveSharedPreference.getPrefToken(getActivity()), SaveSharedPreference.getUserID(getActivity()), "0", newsCategoryId, fused.lat, fused.lon, CommonFunctions.ConvertMilliSecondsToFormattedDate(),generated_sub, generated_desc, GALLERY_MODE_NO, isAnonymous, mCategoryType);
        }else {
            if (GALLERY_MODE == true) {
                id = dbHandler.insertPost(CommonFunctions.getDeviceId(), SaveSharedPreference.getPrefToken(getActivity()), SaveSharedPreference.getUserID(getActivity()), "0", newsCategoryId, fused.lat, fused.lon, CommonFunctions.ConvertMilliSecondsToFormattedDate(),generated_sub, generated_desc, GALLERY_MODE_YES, isAnonymous, mCategoryType);
            } else {
                id = dbHandler.insertPost(CommonFunctions.getDeviceId(), SaveSharedPreference.getPrefToken(getActivity()), SaveSharedPreference.getUserID(getActivity()), "0", newsCategoryId, fused.lat, fused.lon, CommonFunctions.ConvertMilliSecondsToFormattedDate(),generated_sub, generated_desc, GALLERY_MODE_NO, isAnonymous, mCategoryType);
            }
        }

        String primaryKey = fetch_LastRecord();
        Log.d("lastKey", "savePostDataInDb: " + primaryKey);
        //fetch last row rom master table and get primary key then insert data in post table
        //delete data if status==200
        for (int i = 0; i < imgList.size(); i++) {
            try {
                GridModel gridModel = imgList.get(i);
                Log.e("data",imgList.get(i).getFileName()+"");
                dbHandler.insert_filesData(primaryKey + "", gridModel.getFileType(), gridModel.getFileName(), gridModel.getFilePath());
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        if (id != -1 && id != 0) {
            if (CommonFunctions.isConnected(getActivity())) {
                Intent serviceIntent = new Intent(getActivity(), SendOfflineData.class);
                getActivity().startService(serviceIntent);

                //SuccessDialogFragment.state = false;
                if (SuccessDialogFragment.state){
                    getActivity().getSupportFragmentManager()
                            .beginTransaction().
                            replace(R.id.relativecontainer, new SuccessDialogFragment()).commit();
                }else {
                    getActivity().getSupportFragmentManager()
                            .beginTransaction().
                            replace(R.id.frame_container, new SuccessDialogFragment()).commit();
                }

            } else {
                showRetry_Offline_Dialog();
            }
        }
    }

    public  void createVideo() {
        Intent intent = new Intent(MediaStore.ACTION_VIDEO_CAPTURE);
        File dir = CommonDirectories.getCommonPicturesDir();
        fileName = "VID_" + TimeUtilities.getTimeStampInString() + ".mp4";

        File videoDir = new File(dir.getAbsolutePath() + "/" + VIDEO_DIRECTORY_NAME);
        if (!videoDir.exists()) videoDir.mkdirs();

        videoFile = new File(videoDir, fileName);
        intent.putExtra(MediaStore.EXTRA_VIDEO_QUALITY, 1);
        intent.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(videoFile));
        startActivityForResult(intent, REQUEST_VIDEO_CAPTURE);
        return;
    }

    public  void picVideo() {
        Intent intent = new Intent(Intent.ACTION_GET_CONTENT).setType("video/*");
        intent.putExtra(Intent.EXTRA_ALLOW_MULTIPLE,true);
        startActivityForResult(Intent.createChooser(intent, "Select videos"), PICK_VIDEO_REQUEST);
        Log.e("intent passed","asdasdasdas");
        // Intent intent = new Intent(Intent.ACTION_PICK, MediaStore.Video.Media.EXTERNAL_CONTENT_URI);
    }

    private void cameraIntent() {
        Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);

        if (takePictureIntent.resolveActivity(getActivity().getPackageManager()) != null) {
            // Create the File where the photo should go

            try {
                photoFile = createImageFile();
                Log.d("PHOTO_FILE", photoFile.getAbsolutePath());
            } catch (IOException ex) {
                // Error occurred while creating the File
            }
            // Continue only if the File was successfully created
            if (photoFile != null) {
                takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(photoFile));
                startActivityForResult(takePictureIntent, REQUEST_CAMERA);
            }
        }
    }

    private void galleryIntent() {
        Intent intent = new Intent(Intent.ACTION_GET_CONTENT).setType("image/*");
        intent.putExtra(Intent.EXTRA_ALLOW_MULTIPLE,true);
        startActivityForResult(Intent.createChooser(intent, "Select Picture"), SELECT_FILE);
    /*    Intent intent = new Intent(Intent.ACTION_GET_CONTENT).setType("image*//*");
        startActivityForResult(Intent.createChooser(intent,"Select Picture"), SELECT_FILE);*/
/*
        Intent intent1 = new Intent();
// Show only images, no videos or anything else
        intent.setType("image*//*");
        intent.setAction(Intent.ACTION_GET_CONTENT);
// Always show the chooser (if there are multiple options available)
        startActivityForResult(Intent.createChooser(intent, "Select Picture"), SELECT_FILE);*/
    }

   /* public  void AlertDialg(String s){
        alerttDialog=new AlertDialog.Builder(getActivity()).setMessage(s).setCancelable(false).setPositiveButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                alerttDialog.dismiss();
            }
        }).show();
    }
  */

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        switch (requestCode) {
            case REQUEST_VIDEO_CAPTURE:
                if (resultCode == RESULT_OK) {
                    if (videoFile == null || !videoFile.exists()) {
                        Log.i(getClass().getSimpleName(), "videoFile is null ");
                        return;
                    }
                    if (canselect_videos()!=0){
                        Bitmap bMap = ThumbnailUtils.createVideoThumbnail(videoFile.getAbsolutePath(), MediaStore.Video.Thumbnails.MICRO_KIND);
                        imgList.add(new GridModel(MEDIA_TYPE_VIDEO, videoFile.getAbsolutePath(), fileName));
                        setVideo(bMap);
                        Toast.makeText(getActivity(), "File path" + videoFile.getAbsolutePath(), Toast.LENGTH_LONG).show();
                        Log.i(getClass().getSimpleName(), "File path: " + videoFile.getAbsolutePath());
                    }
                }
                break;

            case SELECT_FILE:
                if (data!=null) {
                    //AlertDialg(data+" "+"Data is not null(data)");
                    if (resultCode == RESULT_OK) {
                        String[] filePathColumn = { MediaStore.Images.Media.DATA };
                        Log.e("file",filePathColumn+"");
                        Date date = new Date();
                        ArrayList<MediaDataModel> arrayList=new ArrayList<>();
                        if(data.getClipData() != null){

                            //AlertDialg(data+" "+"Data is not null (Clip)");
                            int maxlimit = 0;
                            if (canSelectImage() == 0) {
                                maxlimit = 0;
                            } else if (data.getClipData().getItemCount() >= canSelectImage()) {
                                maxlimit = canSelectImage();
                                Toast.makeText(getContext(), "Maximum 10 Images can be selected", Toast.LENGTH_LONG).show();
                            } else if (data.getClipData().getItemCount() < canSelectImage()) {
                                maxlimit = data.getClipData().getItemCount();
                            }
                            ArrayList<Uri> mArrayUri = new ArrayList<Uri>();
                            arrayList=new ArrayList<>();
                            for (int i = 0; i < maxlimit; i++) {

                                ClipData.Item item = data.getClipData().getItemAt(i);
                                Uri uri = item.getUri();
                                mArrayUri.add(uri);
                                // Get the cursor
                          /*  Cursor cursor = getActivity().getContentResolver().query(uri, filePathColumn, null, null, null);
                            if(cursor!=null){
                                // Move to first row
                                cursor.moveToFirst();

                                int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
                                imageEncoded  = cursor.getString(columnIndex);
                                cursor.close();
                            }*/

                                Log.e("data",data+"  exp  "+mArrayUri.get(i)+"");
                                //  uid_image_path = rename(path(data), DIRECTORY_NAME + "-" + date.getTime() + ".jpg");
                                String s=i+"";
                                if(mArrayUri.get(i).toString().length()>4){
                                    s=mArrayUri.get(i).toString().substring(mArrayUri.get(i).toString().length()-3,mArrayUri.get(i).toString().length());
                                }else{
                                    s=i+"";
                                }
                                File mFileTemp = new File(Environment.getExternalStorageDirectory(),s +""+i+ date.getTime() + ".jpg");
                                InputStream inputStream = null; // Got the bitmap .. Copy it to the temp file for cropping
                                try {
                                    inputStream = getActivity().getContentResolver().openInputStream(mArrayUri.get(i));
                                    FileOutputStream fileOutputStream = new FileOutputStream(mFileTemp);
                                    copyStream(inputStream, fileOutputStream);
                                    fileOutputStream.close();
                                    fileOutputStream.flush();
                                    inputStream.close();
                                    uid_image_path = mFileTemp.getPath();
                                    Log.e("uid_image_path",uid_image_path+" - "+mFileTemp.getName());
                                    arrayList.add(new MediaDataModel(uid_image_path,mFileTemp.getName()));
                                    GALLERY_MODE = true;
                                } catch (FileNotFoundException e) {
                                    e.printStackTrace();
                                } catch (IOException e) {
                                    e.printStackTrace();
                                }
                            }
                        }else if(data.getData()!= null){
                            //AlertDialg(data+" "+"Data is not null ");
                            if (canSelectImage()!=0){
                                File mFileTemp = new File(Environment.getExternalStorageDirectory(),  date.getTime() + ".jpg");
                                InputStream inputStream = null; // Got the bitmap .. Copy it to the temp file for cropping
                                try {
                                    inputStream = getActivity().getContentResolver().openInputStream(data.getData());
                                    FileOutputStream fileOutputStream = new FileOutputStream(mFileTemp);
                                    copyStream(inputStream, fileOutputStream);
                                    fileOutputStream.close();
                                    fileOutputStream.flush();
                                    inputStream.close();
                                    uid_image_path = mFileTemp.getPath();
                                    arrayList.add(new MediaDataModel(uid_image_path,DIRECTORY_NAME + "-" + date.getTime() + ".jpg"));
                                    GALLERY_MODE = true;
                                } catch (FileNotFoundException e) {
                                    e.printStackTrace();
                                } catch (IOException e) {
                                    e.printStackTrace();
                                }
                            }
                        }
                        for (int i=0;i<arrayList.size();i++){
                            imgList.add(new GridModel(MEDIA_TYPE_IMAGE, arrayList.get(i).getImgPath(), arrayList.get(i).getFileName()));
                            setImage(arrayList.get(i).getImgPath());
                        }
                    }
                }
                break;

            case REQUEST_CAMERA:
                if (resultCode == RESULT_OK) {
                    onCaptureImageResult(data);
                }
                break;

            case 112:
                if (resultCode == RESULT_OK) {
                    if (data != null) getRecordData(data);
                }
                break;

            case Fused.REQUEST_CHECK_SETTINGS:
                switch (resultCode) {
                    case RESULT_OK:
                        Log.i("Fused", "User agreed to make required location settings changes.");
                        createGPSPushDialog(getActivity());
                        fused.startLocationUpdates();
                        break;
                    case RESULT_CANCELED:
                        Log.i("Fused", "User chose not to make required location settings changes.");
                        break;
                }
                break;
        }
       if (requestCode == MAKE_VIDEO_REQUEST) {
            if (resultCode == RESULT_OK) {
                if (videoFileDirectory == null || !videoFileDirectory.exists()) return;
                video_path = commonFunctions.getRealPathFromURI(getActivity(), data.getData());
                video_flag = 0;
                Bitmap bMap = ThumbnailUtils.createVideoThumbnail(video_path, MediaStore.Video.Thumbnails.MICRO_KIND);
                //  setVideo(bMap);
            } else if (resultCode == RESULT_CANCELED) {
                // User cancelled the video capture
                Toast.makeText(getActivity(), "User cancelled the video capture.", Toast.LENGTH_LONG).show();
            } else {
                // Video capture failed, advise user
                Toast.makeText(getActivity(), "Video capture failed.", Toast.LENGTH_LONG).show();
            }
        }else if (requestCode == PICK_VIDEO_REQUEST) {
           if (data != null){
               if (resultCode == RESULT_OK) {
                   if (data.getClipData() != null) {
                       Log.e("intent passed enter","asdsdasdasd");

                       int maxlimit = 0;
                       if (canselect_videos() == 0) {
                           maxlimit = 0;
                       } else if (data.getClipData().getItemCount() >= canselect_videos()) {
                           maxlimit = canselect_videos();
                           Toast.makeText(getContext(), "Maximum 10 videos can be selected", Toast.LENGTH_LONG).show();
                       } else if (data.getClipData().getItemCount() < canselect_videos()) {
                           maxlimit = data.getClipData().getItemCount();
                       }
                       ArrayList<String> pathList=new ArrayList<>();
                       if (data.getClipData() != null) {
                           Log.e("Elseif", data.getClipData() + "");
                           ClipData mClipData = data.getClipData();
                           for (int i = 0; i <maxlimit; i++) {
                               ClipData.Item item = mClipData.getItemAt(i);
                               Uri uri = item.getUri();
                               try {
                                   Date date = new Date();
                                   String s=i+"";
                                   if(uri.toString().length()>4){
                                       s=uri.toString().substring(uri.toString().length()-3,uri.toString().length());
                                   }else{
                                       s=i+"";
                                   }
                                   File mFileTemp = new File(Environment.getExternalStorageDirectory(),s+""+i+date.getTime() + ".mp4");
                                   InputStream inputStream = null; // Got the bitmap .. Copy it to the temp file for cropping
                                   Log.e("datas", data + "  exp  " + uri+ "");
                                   inputStream = getActivity().getContentResolver().openInputStream(uri);
                                   FileOutputStream fileOutputStream = new FileOutputStream(mFileTemp);
                                   copyStream(inputStream, fileOutputStream);
                                   fileOutputStream.close();
                                   fileOutputStream.flush();
                                   inputStream.close();
                                   video_path=mFileTemp.getPath();
                                   pathList.add(video_path);
                                   Log.e("pathList",pathList+" "+mFileTemp.getPath());
                               } catch (Exception e) {
                               }
                           }
                           Log.e("pathList",pathList.size()+"");
                           for (int j=0;j<pathList.size();j++){
                               Bitmap bMap = ThumbnailUtils.createVideoThumbnail(pathList.get(j), MediaStore.Video.Thumbnails.MICRO_KIND);
                               imgList.add(new GridModel(MEDIA_TYPE_VIDEO, pathList.get(j), new File(pathList.get(j)).getName()));
                               setVideo(bMap);
                               Log.e("intent setting enter", "shdcjhdsbc");
                           }
                       }
                   }else if (data.getData()!= null) {
                       GALLERY_MODE = true;
                       Log.e("data",""+data.getData());
                       //video_path=getPath(data.getData());
                       if (canselect_videos() != 0) {
                           try {
                               Date date = new Date();
                               File mFileTemp = new File(Environment.getExternalStorageDirectory(),  date.getTime() + ".mp4");
                               InputStream inputStream = null; // Got the bitmap .. Copy it to the temp file for cropping
                               Log.e("datas",data+"  exp  "+data.getData()+"");
                               inputStream = getActivity().getContentResolver().openInputStream(data.getData());
                               FileOutputStream fileOutputStream = new FileOutputStream(mFileTemp);
                               copyStream(inputStream, fileOutputStream);
                               fileOutputStream.close();
                               fileOutputStream.flush();
                               inputStream.close();
                               video_path =mFileTemp.getPath();
                           } catch (Exception e) {
                           }
                           Log.e("path", "" + video_path);
                           video_flag = 0;
                           Bitmap bMap = ThumbnailUtils.createVideoThumbnail(video_path, MediaStore.Video.Thumbnails.MICRO_KIND);
                           if (bMap != null) {
                               imgList.add(new GridModel(MEDIA_TYPE_VIDEO, video_path, new File(video_path).getName()));
                               setVideo(bMap);
                           }
                       }
                   }

               } else if (resultCode == RESULT_CANCELED) {
                   Toast.makeText(getActivity(), "User cancelled the video capture.", Toast.LENGTH_LONG).show();

               } else {
                   Toast.makeText(getActivity(), "Video capture failed.", Toast.LENGTH_LONG).show();
               }
           }
        } else if (requestCode == PICK_AUDIO_REQUEST) {
            if (resultCode == RESULT_OK) {
                Date date = new Date();
                GALLERY_MODE = true;
                String fileName = AUDIO_DIRECTORY_NAME + "-" + CommonFunctions.getTimeStampInString() + ".mp3";
                String audio_path = rename(path(data), AUDIO_DIRECTORY_NAME + "-" + CommonFunctions.getTimeStampInString() + ".mp3");

                Log.d("PICK_AUDIO_REQUEST", "onActivityResult: " + audio_path);
                imgList.add(new GridModel(MEDIA_TYPE_AUDIO, audio_path, fileName));
                setAudio(fileName, audio_path);
            }
        } else if (requestCode == ACTIVITY_RECORD_SOUND) {
            if (resultCode == RESULT_OK) {
                Date date = new Date();
                String fileName = AUDIO_DIRECTORY_NAME + "-" + CommonFunctions.getTimeStampInString() + ".mp3";
                String audio_path = rename(path(data), AUDIO_DIRECTORY_NAME + "-" + CommonFunctions.getTimeStampInString() + ".mp3");

                imgList.add(new GridModel(MEDIA_TYPE_AUDIO, audio_path, fileName));
                setAudio(fileName, audio_path);
            }
        }
    }

    public String getPath(Uri uri) {
        try{
            String[] projection = { MediaStore.Video.Media.DATA };
            Cursor cursor = getActivity().getContentResolver().query(uri, projection, null, null, null);
            if (cursor != null) {
                // HERE YOU WILL GET A NULLPOINTER IF CURSOR IS NULL
                // THIS CAN BE, IF YOU USED OI FILE MANAGER FOR PICKING THE MEDIA
                int column_index = cursor
                        .getColumnIndexOrThrow(MediaStore.Video.Media.DATA);
                cursor.moveToFirst();
                return cursor.getString(column_index);
            } else
                return null;
        }catch (Exception e){
            return null;
        }
    }

    private void setVideo_byUri(String path) {
        RelativeLayout imgLayout = new RelativeLayout(getActivity());
        RelativeLayout.LayoutParams params1 = new RelativeLayout.LayoutParams(200, 200);
        imgLayout.setLayoutParams(params1);
        try {
            final ImageView imageView_cancel = new ImageView(getActivity());
            RelativeLayout.LayoutParams lp_cancel = new RelativeLayout.LayoutParams(70, 70);
            lp_cancel.addRule(RelativeLayout.ALIGN_PARENT_RIGHT);
            lp_cancel.addRule(RelativeLayout.ALIGN_PARENT_TOP);
            imageView_cancel.setImageResource(R.drawable.cancel_icon);
            imageView_cancel.setLayoutParams(lp_cancel);
            imageView_cancel.setTag(setCanceltag++);

            ImageView imageView = new ImageView(getActivity());
           // Bitmap bitmap1 = bitmap.createScaledBitmap(bitmap, 200, 200, true);
            imageView.setPadding(10, 10, 10, 10);
            imageView.setScaleType(ImageView.ScaleType.FIT_XY);
            imageView.setAdjustViewBounds(true);
            imageView.setLayoutParams(params1);
            imageView.setImageURI(Uri.parse(path));

            imgLayout.addView(imageView);
            imgLayout.addView(imageView_cancel);

            conatiner_imageview.addView(imgLayout);
            relativeList.add(imgLayout);

            imageView_cancel.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    int position = (Integer) imageView_cancel.getTag();
                    Log.d("TAG", "onClick: " + position);
                    conatiner_imageview.removeView(relativeList.get(position));
                    Log.d("onClick aftere", "conatiner_imageview: " + conatiner_imageview.getChildCount());
                    cancelList.add(position + "");

                    imgList.set(position,new GridModel("","",""));
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void setVideo(Bitmap bitmap) {
        RelativeLayout imgLayout = new RelativeLayout(getActivity());
        RelativeLayout.LayoutParams params1 = new RelativeLayout.LayoutParams(200, 200);
        imgLayout.setLayoutParams(params1);
        try {
            final ImageView imageView_cancel = new ImageView(getActivity());
            RelativeLayout.LayoutParams lp_cancel = new RelativeLayout.LayoutParams(70, 70);
            lp_cancel.addRule(RelativeLayout.ALIGN_PARENT_RIGHT);
            lp_cancel.addRule(RelativeLayout.ALIGN_PARENT_TOP);
            imageView_cancel.setImageResource(R.drawable.cancel_icon);
            imageView_cancel.setLayoutParams(lp_cancel);
            imageView_cancel.setTag(setCanceltag++);

            ImageView imageView = new ImageView(getActivity());
            Bitmap bitmap1 = bitmap.createScaledBitmap(bitmap, 200, 200, true);
            imageView.setPadding(10, 10, 10, 10);
            imageView.setScaleType(ImageView.ScaleType.FIT_XY);
            imageView.setAdjustViewBounds(true);
            imageView.setLayoutParams(params1);
            imageView.setImageBitmap(bitmap1);

            imgLayout.addView(imageView);
            imgLayout.addView(imageView_cancel);

            conatiner_imageview.addView(imgLayout);
            relativeList.add(imgLayout);

            imageView_cancel.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    int position = (Integer) imageView_cancel.getTag();
                    Log.d("TAG", "onClick: " + position);
                    conatiner_imageview.removeView(relativeList.get(position));
                    Log.d("onClick aftere", "conatiner_imageview: " + conatiner_imageview.getChildCount());
                    cancelList.add(position + "");


                    imgList.set(position,new GridModel("","",""));
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    private static void copyStream(InputStream input, OutputStream output) throws IOException {
        byte[] buffer = new byte[1024];
        int bytesRead;
        while ((bytesRead = input.read(buffer)) != -1) {
            output.write(buffer, 0, bytesRead);
        }
    }

    private void onSelectFromGalleryResult(Intent data) {
         try {
             Log.e("data",""+data);
             GALLERY_MODE = true;
            if (data.getData() != null) {

                Log.e("data",""+data.getData());

                if (canSelectImage() != 0) {
                    Date date = new Date();
                    uid_image_path = rename(path(data), DIRECTORY_NAME + "-" + date.getTime() + ".jpg");
                    File mFileTemp = new File(Environment.getExternalStorageDirectory(),  date.getTime() + ".jpg");
                    InputStream inputStream = getActivity().getContentResolver().openInputStream(data.getData()); // Got the bitmap .. Copy it to the temp file for cropping
                    FileOutputStream fileOutputStream = new FileOutputStream(mFileTemp);
                    copyStream(inputStream, fileOutputStream);
                    fileOutputStream.close();
                    fileOutputStream.flush();
                    inputStream.close();
                    uid_image_path = mFileTemp.getPath();
                    imgList.add(new GridModel(MEDIA_TYPE_IMAGE, uid_image_path, DIRECTORY_NAME + "-" + date.getTime() + ".jpg"));
                    setImage(uid_image_path);
                }
            } else {
                if (data.getClipData() != null) {
                    ClipData mClipData = data.getClipData();
                    int maxlimit = 0;
                    if (canSelectImage() == 0) {
                        maxlimit = 0;
                    } else if (mClipData.getItemCount() >= canSelectImage()) {
                        maxlimit = canSelectImage();
                        Toast.makeText(getContext(), "Maximum 10 images can be selected", Toast.LENGTH_SHORT).show();
                    } else if (mClipData.getItemCount() < canSelectImage()) {
                        maxlimit = mClipData.getItemCount();
                    }
                    for (int i = 0; i < maxlimit; i++) {

                        ClipData.Item item = mClipData.getItemAt(i);
                        Uri uri = item.getUri();

                        Date date = new Date();
                        String uris = RealPathUtil.getRealPathFromURI_API19(getContext(), uri);
                        Log.e("uris", "" + uris);
                        uid_image_path = rename(uris, DIRECTORY_NAME + "-" + date.getTime() + ".jpg");

                        imgList.add(new GridModel(MEDIA_TYPE_IMAGE, uid_image_path, DIRECTORY_NAME + "-" + date.getTime() + ".jpg"));

                        setImage(uid_image_path);
                    }
                }
            }
        }catch (Exception e){

        }

    }

    private int canSelectImage() {
        int numimages=checkTotalImages();
        int canselect=0;
        //mClipData.getItemCount()
        if (numimages<10){
            canselect=10-numimages;
        }else if (numimages==10){
            Toast.makeText(getContext(),"Max images limit reached", Toast.LENGTH_SHORT).show();
            canselect=0;
        }
        Log.e("canselect",""+canselect);
        return canselect;
    }

    private  int checkTotalImages(){
        int imageCount =0;
        if (imgList!=null){
            if (imgList.isEmpty()){
                imageCount=0;
            }else {
                for (int i=0;i<imgList.size();i++) {
                    if (imgList.get(i).getFileType()!=null)
                    {
                        if (imgList.get(i).getFileType().equals(MEDIA_TYPE_IMAGE)){
                            imageCount++;
                        }
                    }
                }
            }
        }
        Log.e("totalImages",""+imageCount);
        return imageCount;
    }

    private int canselect_videos(){
        int numimages=checkTotalvideos();
        int canselect=0;
        //mClipData.getItemCount()
        if (numimages<10){
            canselect=10-numimages;
        }else if (numimages==10){
            Toast.makeText(getContext(),"Max videos limit reached", Toast.LENGTH_SHORT).show();
            canselect=0;
        }
        Log.e("canselect",""+canselect);
        return canselect;
    }

    private  int checkTotalvideos(){
        int imageCount =0;
        if (imgList!=null){
            if (imgList.isEmpty()){
                imageCount=0;
            }else {
                for (int i=0;i<imgList.size();i++) {
                    if (imgList.get(i).getFileType()!=null)
                    {
                        if (imgList.get(i).getFileType().equals(MEDIA_TYPE_VIDEO)){
                            imageCount++;
                        }
                    }
                }
            }
        }
        return imageCount;
    }

    private  int checkaudioexist() {
        int audiocount =0;
        try {
            if (imgList!=null){
                if (imgList.isEmpty()){
                    audiocount=0;
                }else {
                    for (int i=0;i<imgList.size();i++) {
                        if (!imgList.get(i).getFileType().equals(""))
                        {
                            if (imgList.get(i).getFileType().equals(MEDIA_TYPE_AUDIO)){
                                audiocount=1;
                            }
                        }
                    }
                }
            }
        }catch (Exception e){
        }
        return audiocount;
    }

    public void setImage(String file) {
        RelativeLayout imgLayout = new RelativeLayout(getActivity());
        RelativeLayout.LayoutParams params1 = new RelativeLayout.LayoutParams(160, 160);
        imgLayout.setLayoutParams(params1);
        try {
            final ImageView imageView_cancel = new ImageView(getActivity());
            RelativeLayout.LayoutParams lp_cancel = new RelativeLayout.LayoutParams(50, 50);
            lp_cancel.addRule(RelativeLayout.ALIGN_PARENT_RIGHT);
            lp_cancel.addRule(RelativeLayout.ALIGN_PARENT_TOP);
            imageView_cancel.setImageResource(R.drawable.cancel_icon);
            imageView_cancel.setLayoutParams(lp_cancel);
            imageView_cancel.setTag(setCanceltag++);

            ImageView imageView = new ImageView(getActivity());
            imageView.setPadding(10, 10, 10, 10);
            imageView.setScaleType(ImageView.ScaleType.FIT_XY);
            imageView.setMaxHeight(100);
            imageView.setMaxWidth(100);
            imageView.setLayoutParams(params1);

            imgLayout.addView(imageView);
            imgLayout.addView(imageView_cancel);
            Bitmap bitmap = ThumbnailUtils.extractThumbnail(BitmapFactory.decodeFile(file), 200, 200);
            imageView.setImageBitmap(bitmap);
            Log.i("File image", file);

            try {
                Glide.with(this).load(new File(file)).into(imageView);
                conatiner_imageview.addView(imgLayout);
                Log.d("TAG", "conatiner_imageview: " + conatiner_imageview.getChildCount());
                relativeList.add(imgLayout);

            } catch (Exception e) {
                e.printStackTrace();
            }

         imageView_cancel.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    int position = (Integer) imageView_cancel.getTag();
                    Log.d("TAG", "onClick: " + position);
                    conatiner_imageview.removeView(relativeList.get(position));
                    Log.d("onClick aftere", "conatiner_imageview: " + conatiner_imageview.getChildCount());

                    // cancelList.add(position + "");
                    /*Log.e("imagepos",""+position);
                    Log.e("imagearray",""+imgList.size());
                    Log.e("imagearrayposition",""+imgList.get(position));*/

                    try {
                        if (imgList.size()==1){
                            imgList.clear();
                        }else {
                            imgList.set(position,new GridModel("","",""));
                        }
                    }catch (Exception e){

                    }
                    Log.e("imagearray2",""+imgList.size());

                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    public String path(Intent data) {
        String realPath = null;
        try {
            realPath = FilePath.getPath(getActivity(), data.getData());

        } catch (Exception ex) {
            ex.printStackTrace();
        }

        return realPath;
    }

    private void onCaptureImageResult(Intent data) {
        notifyMediaStoreScanner(photoFile.getAbsolutePath());
        if (canSelectImage() != 0) {
            imgList.add(new GridModel(MEDIA_TYPE_IMAGE, photoFile.getAbsolutePath(), photoFile.getName()));

            setImage(photoFile.getAbsolutePath());
        }
    }

    public String rename(String path, String name) {

        try {
            File dir = new File(path);
            File from = new File(dir.getParent(), dir.getName());
            String extStorageDirectory = Environment.getExternalStorageDirectory().toString();
            File directory = new File(extStorageDirectory + "/" + DIRECTORY_NAME);
            if (!directory.exists()) {
                directory.mkdir();
            }
            File to = new File(directory, name);
            InputStream in = new FileInputStream(from);
            OutputStream out = new FileOutputStream(to);

            // Transfer bytes from in to out
            byte[] buf = new byte[1024];
            int len;
            while ((len = in.read(buf)) > 0) {
                out.write(buf, 0, len);
            }
            in.close();
            out.close();

            return to.getAbsolutePath();
        } catch (Exception excep) {
            excep.printStackTrace();
            return "NOO" + excep;
        }
    }

    public static void update() {
        if (gpsDialog != null && gpsDialog.isShowing()) gpsDialog.cancel();
        active = false;
    }

    @Override
    public void onStart() {
        active = true;
        if (fused != null) fused.onStart();
        super.onStart();
    }

    @Override
    public void onPause() {
        super.onPause();
        if (fused != null) fused.onPause();
    }

    @Override
    public void onResume() {
        super.onResume();
        if (fused != null) fused.onResume();
        if(((AppCompatActivity)getActivity()).getSupportActionBar()!=null)
        ((AppCompatActivity)getActivity()).getSupportActionBar().hide();
    }

    @Override
    public void onStop() {
        super.onStop();
        active = false;
        if (gps != null) gps.stopUsingGPS();

        if (fused != null)
        fused.stopLocationUpdates();
        if (((AppCompatActivity)getActivity()).getSupportActionBar()!=null)
        ((AppCompatActivity)getActivity()).getSupportActionBar().show();
    }

    @Override
    public void onDestroy() {
        if (fused != null) fused.onDestroy();
        GALLERY_MODE = false;

        super.onDestroy();
        // getActivity().getSupportFragmentManager().popBackStackImmediate();
    }

    public void createGPSPushDialog(final Activity context) {
        if (gpsDialog != null && gpsDialog.isShowing()) gpsDialog.cancel();
        gpsDialog = new Dialog(context, R.style.CustomDialogTheme);
        gpsDialog.setCancelable(false);
        gpsDialog.setContentView(R.layout.popup);
        gpsDialog.show();
    }

    /*    private void showMediaDialog() {
        alertDialog = new Dialog(getActivity());
        alertDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        alertDialog.setContentView(R.layout.select_media_dialog);
        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        lp.copyFrom(alertDialog.getWindow().getAttributes());
        lp.width = WindowManager.LayoutParams.MATCH_PARENT;
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;

        alertDialog.getWindow().setAttributes(lp);
        alertDialog.show();
        alertDialog.setCancelable(false);

        tv_cancel = (TextView) alertDialog.findViewById(R.id.tv_cancel);
        tv_camera_img = (TextView) alertDialog.findViewById(R.id.tv_camera_img);
        tv_gallery_img = (TextView) alertDialog.findViewById(R.id.tv_gallery_img);
        tv_camera_vedio = (TextView) alertDialog.findViewById(R.id.tv_camera_video);
        tv_gallery_vedio = (TextView) alertDialog.findViewById(R.id.tv_gallery_video);
        tv_camera_audio = (TextView) alertDialog.findViewById(R.id.tv_camera_audio);
        tv_gallery_audio = (TextView) alertDialog.findViewById(R.id.tv_gallery_audio);

        switch (mCategoryType) {
            case 1:
                tv_camera_img.setVisibility(View.VISIBLE);
                tv_gallery_img.setVisibility(View.VISIBLE);
                tv_camera_vedio.setVisibility(View.VISIBLE);
                tv_gallery_vedio.setVisibility(View.VISIBLE);
                tv_camera_audio.setVisibility(View.VISIBLE);
                tv_gallery_audio.setVisibility(View.VISIBLE);
                break;
            case 2:
                tv_camera_img.setVisibility(View.VISIBLE);
                tv_gallery_img.setVisibility(View.VISIBLE);
                tv_camera_vedio.setVisibility(View.VISIBLE);
                tv_gallery_vedio.setVisibility(View.VISIBLE);
                tv_camera_audio.setVisibility(View.GONE);
                tv_gallery_audio.setVisibility(View.GONE);
                break;
            case 3:
                tv_camera_img.setVisibility(View.VISIBLE);
                tv_gallery_img.setVisibility(View.VISIBLE);
                tv_camera_vedio.setVisibility(View.VISIBLE);
                tv_gallery_vedio.setVisibility(View.VISIBLE);
                tv_camera_audio.setVisibility(View.GONE);
                tv_gallery_audio.setVisibility(View.GONE);
                break;
            default:
                tv_camera_img.setVisibility(View.VISIBLE);
                tv_gallery_img.setVisibility(View.VISIBLE);
                tv_camera_vedio.setVisibility(View.VISIBLE);
                tv_gallery_vedio.setVisibility(View.VISIBLE);
                tv_camera_audio.setVisibility(View.VISIBLE);
                tv_gallery_audio.setVisibility(View.VISIBLE);
                break;
        }
        tv_cancel.setOnClickListener(this);
        tv_camera_img.setOnClickListener(this);
        tv_gallery_img.setOnClickListener(this);
        tv_camera_vedio.setOnClickListener(this);
        tv_gallery_vedio.setOnClickListener(this);
        tv_camera_audio.setOnClickListener(this);
        tv_gallery_audio.setOnClickListener(this);
    }*/

    private void recordAudio() {
        Intent intent = new Intent(getActivity(), SoundRecordingActivity.class);
        startActivityForResult(intent, 112);
    }

    private void picAudio() {
        Intent intent_upload = new Intent();
        intent_upload.setAction(Intent.ACTION_GET_CONTENT);
        intent_upload.setType("audio/mpeg");
        startActivityForResult(intent_upload, PICK_AUDIO_REQUEST);
    }

    private void getRecordData(Intent data) {
        String MEDIA_TYPE_ = data.getStringExtra(MEDIA_TYPE);
        String MEDIA_FILE_PATH_ = data.getStringExtra(MEDIA_FILE_PATH);
        String KEY_AUDIO_FILE_NAME_ = data.getStringExtra(KEY_AUDIO_FILE_NAME);
        String KEY_AUDIO_FILE_PATH_ = data.getStringExtra(KEY_AUDIO_FILE_PATH);

        imgList.add(new GridModel(MEDIA_TYPE_AUDIO, MEDIA_FILE_PATH_, KEY_AUDIO_FILE_NAME_));

        setAudio(MEDIA_TYPE_, MEDIA_FILE_PATH_);

    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();

    }

    public byte[] convert(String path) throws IOException {
        FileInputStream fis = new FileInputStream(path);
        ByteArrayOutputStream bos = new ByteArrayOutputStream();
        byte[] b = new byte[1024];
        for (int readNum; (readNum = fis.read(b)) != -1; ) {
            bos.write(b, 0, readNum);
        }
        byte[] bytes = bos.toByteArray();
        return bytes;
    }

    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        if (requestCode == 0) {
            if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED && grantResults[1] == PackageManager.PERMISSION_GRANTED) {
                //vPickvideo.setEnabled(true);
            }
        }
    }

    public void showRetry_Offline_Dialog() {
        final AlertDialog.Builder alertDialog = new AlertDialog.Builder(getActivity());
        alertDialog.setTitle("Message");
        alertDialog.setMessage("No Network Connection");
        alertDialog.setCancelable(false);
        alertDialog.setPositiveButton("Retry", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {

                if (CommonFunctions.isConnected(getActivity())) {
                    if (isDataSend == false) {
                        Intent serviceIntent = new Intent(getActivity(), SendOfflineData.class);
                        getActivity().startService(serviceIntent);
                        {
                            SuccessDialogFragment.state = false;
                            getActivity().getSupportFragmentManager().beginTransaction().replace(R.id.frame_container,
                                    new SuccessDialogFragment()).commit();
                        }
                    }
                } else {
                    btn_createPost.setEnabled(true);
                    Toast.makeText(getActivity(), "Please connect your device to internet", Toast.LENGTH_SHORT).show();
                }
                dialog.dismiss();
            }
        });
        alertDialog.setNegativeButton("Save Offline", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {

                //save data in database
                savePostDataInDb(imgList, CommonFunctions.getDeviceId(), SaveSharedPreference.getPrefToken(getActivity()),
                        SaveSharedPreference.getUserID(getActivity()), "0", newsCategoryId, fused.lat, fused.lon + "", CommonFunctions.ConvertMilliSecondsToFormattedDate(),generated_sub, generated_desc);
                dialog.dismiss();
            }
        });
        AlertDialog alert = alertDialog.create();
        alert.show();
    }

    private void savePostDataInDb(ArrayList<GridModel> imgList,String deviceId, String prefToken, String userID, String isPosted, String newsCategoryId, String capturedLat, String capturedLong, String createdOn, String title, String description) {
        //isPosted==0 (means post is not sent on server
        DialogInterface.OnClickListener onClickTryAgain = new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                //  getActivity().getSupportFragmentManager().popBackStack();

                Intent i = new Intent(getActivity(), NavDrawerActivity.class);
                startActivity(i);

            }
        };
        Dialogs.showDialog_Offline(getActivity(), "Your post has been stored offline, will be updated online when internet is available.", onClickTryAgain);
        Dialogs.disDialog();
    }

    private String fetch_LastRecord() {
        String newsId = null;
        Cursor m = dbHandler.fetch_LastRecord();
        Log.i("fetch_MediaFiles", m.getCount() + "");
        if (m.getCount() > 0) {
            try {
                newsId = m.getString(0);
                Log.d("newsId primarykey", "fetch_LastRecord: " + newsId);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return newsId;

    }

    private File createImageFile() throws IOException {
        Date date = new Date();
        // Create an image file name
        String extStorageDirectory = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES).toString();
        File directory = new File(extStorageDirectory + "/" + DIRECTORY_NAME);
        Log.d("TAG", "createImageFile: " + directory + "");
        if (!directory.exists()) {
            directory.mkdir();
        }
        Filename = DIRECTORY_NAME + "-" + date.getTime();

        File image = File.createTempFile(Filename,  /* prefix */
                ".jpg",         /* suffix */
                directory      /* directory */);

        // Save a file: path for use with ACTION_VIEW intents
        mCurrentPhotoPath = image.getAbsolutePath();
        Log.d("TAG", "createImageFile: " + mCurrentPhotoPath);
        return image;
    }

    public final void notifyMediaStoreScanner(final String fileUri) {
        // Convert to file Object
        File file = new File(fileUri);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            // Write Kitkat version specific code for add entry to gallery database
            // Check for file existence
            if (file.exists()) {
                // Add / Move File
                Intent mediaScanIntent = new Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE);
                Uri contentUri = Uri.fromFile(new File(fileUri));
                mediaScanIntent.setData(contentUri);
                getActivity().sendBroadcast(mediaScanIntent);
            } else {
                // Delete File
                try {
                    getActivity().getContentResolver().delete(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, MediaStore.Images.Media.DATA + "='" + new File(fileUri).getPath() + "'", null);
                } catch (Exception e) {
                    e.printStackTrace();

                }
            }
        } else {
            getActivity().sendBroadcast(new Intent(Intent.ACTION_MEDIA_MOUNTED, Uri.parse("file://" + new File(fileUri).getParent())));
        }
    }

    // create boolean for fetching data
    private boolean isViewShown = false;

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        if (getView() != null) {
            isViewShown = true;

        } else {
            isViewShown = false;
        }
    }

    /**
     * Method to show alert dialog
     */
}
