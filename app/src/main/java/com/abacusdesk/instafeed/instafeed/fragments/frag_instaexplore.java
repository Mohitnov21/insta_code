package com.abacusdesk.instafeed.instafeed.fragments;

import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.abacusdesk.instafeed.instafeed.R;
import com.abacusdesk.instafeed.instafeed.Util.Alerts;
import com.abacusdesk.instafeed.instafeed.Util.Apis;
import com.abacusdesk.instafeed.instafeed.Util.CommonFunctions;
import com.abacusdesk.instafeed.instafeed.Util.SaveSharedPreference;
import com.abacusdesk.instafeed.instafeed.adapter.Explore_Adapter;
import com.abacusdesk.instafeed.instafeed.adapter.Recycler_newAdapter;
import com.abacusdesk.instafeed.instafeed.custom_dialog.Dialogs;
import com.abacusdesk.instafeed.instafeed.model.Explorer_model;
import com.abacusdesk.instafeed.instafeed.model.NewsModel;
import com.android.volley.AuthFailureError;
import com.android.volley.NoConnectionError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.gson.Gson;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by ishaan on 6/19/2017.
 */

public class frag_instaexplore extends Fragment{

    View view;
    RecyclerView recyclerView;
    Explore_Adapter adapter;
    ArrayList<Explorer_model.DataBean> array_explore=new ArrayList<>();
    Explorer_model explorer_model;
    public  String  Api_temp="";
    Map<String, String> map ;
    TextView txtrecord;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.frag_recycler, container, false);
        recyclerView = (RecyclerView) view.findViewById(R.id.recyclerView);
        txtrecord=(TextView)view.findViewById(R.id.txt_records);
        if (Publicprofile_Fragment.flag==true){
            Api_temp=Apis.public_explorer;
            map=new HashMap<>();
            map.put("username",Publicprofile_Fragment.username);
            Log.e("printmappublicprofile",""+map);
        } else {
            Api_temp=Apis.private_explorer;
            // map.clear();
            map=new HashMap<>();
            map.put("user", SaveSharedPreference.getUserID(getActivity()));
            map.put("token",SaveSharedPreference.getPrefToken(getActivity()));
            Log.e("printmappublicprofile",""+map);
        }
        if (CommonFunctions.isConnected(getActivity())){
            if (!array_explore.isEmpty()){
                array_explore.clear();
            }
            getExploreList();
        }else {
            Toast.makeText(getActivity(), "Internet not available", Toast.LENGTH_SHORT).show();
        }
        setRecyclerView();
        return view;
    }

    public void getExploreList() {
        //  Dialogs.showProDialog(getActivity(), "Loading");
        StringRequest stringRequest = new StringRequest(Request.Method.POST, Api_temp,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.e("log", "" + response);
                        try {
                            getResponse(response);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        // Dialogs.disDialog();
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        ///   Dialogs.disDialog();
                    }
                })
        {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                return map;
            }
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Content-Type", "application/x-www-form-urlencoded");
                return params;
            }
        };

        RequestQueue requestQueue = Volley.newRequestQueue(getActivity());
        requestQueue.add(stringRequest);
    }

    private void getResponse(String response ) throws JSONException {
        if(!response.equals(null)) {
            Log.e("log", "" + response);
            JSONObject jsonObject = new JSONObject(response);
            Object object = jsonObject.get("data");
            if (object instanceof JSONArray) {
                Gson gson = new Gson();
                explorer_model = gson.fromJson(response, Explorer_model.class);
                int status = explorer_model.getStatus();
                String msg = explorer_model.getMessage();
                if (msg.equalsIgnoreCase("success") && status == 200) {
                    if (explorer_model.getData() != null && !explorer_model.getData().isEmpty()) {
                        for (int i = 0; i < explorer_model.getData().size(); i++) {
                            try {
                                Explorer_model.DataBean dataBean = explorer_model.getData().get(i);
                                array_explore.add(dataBean);
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }
                    }
                    Log.e("databen data", "" + array_explore);
                    if (array_explore.isEmpty()) {
                        txtrecord.setVisibility(View.VISIBLE);
                    }
                    adapter.notifyDataSetChanged();
                }
            }
        }
    }

    // poills blogs opiunions
    // working petition talents  news
    // Setting recycler view

    private void setRecyclerView() {
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        // recyclerView.addItemDecoration(mDividerItemDecoration);
        try{
            adapter= new Explore_Adapter(getActivity(),array_explore);
            recyclerView.setAdapter(adapter);
            adapter.notifyDataSetChanged();
           // Log.e("responseset",""+newsDataBeanList);
        }catch (Exception e){
            e.printStackTrace();
        }
    }
}
