package com.abacusdesk.instafeed.instafeed.model;

/**
 * Created by Administrator on 6/29/2017.
 */

public class MasterTblModel {
    String newsId,deviceId,prefToken,userID,isPosted,newsCategoryId,capturedLat,capturedLong,createdOn,title,description,anonymous,mode;
    int categoryType;

    public String getNewsId() {
        return newsId;
    }

    public String getMode() {
        return mode;
    }

    public void setMode(String mode) {
        this.mode = mode;
    }

    public MasterTblModel(String newsId, String deviceId, String prefToken, String userID, String isPosted, String newsCategoryId, String capturedLat, String capturedLong, String createdOn, String title, String description, String anonymous, String mode, int categoryType) {
        this.newsId = newsId;
        this.deviceId = deviceId;
        this.prefToken = prefToken;
        this.userID = userID;
        this.isPosted = isPosted;
        this.newsCategoryId = newsCategoryId;
        this.capturedLat = capturedLat;
        this.capturedLong = capturedLong;
        this.createdOn = createdOn;
        this.title = title;
        this.description = description;
        this.anonymous = anonymous;
        this.mode = mode;
        this.categoryType = categoryType;
    }

    public void setNewsId(String newsId) {
        this.newsId = newsId;
    }


    public String getDeviceId() {
        return deviceId;
    }

    public void setDeviceId(String deviceId) {
        this.deviceId = deviceId;
    }

    public String getPrefToken() {
        return prefToken;
    }

    public void setPrefToken(String prefToken) {
        this.prefToken = prefToken;
    }

    public String getUserID() {
        return userID;
    }

    public void setUserID(String userID) {
        this.userID = userID;
    }

    public String getIsPosted() {
        return isPosted;
    }

    public void setIsPosted(String isPosted) {
        this.isPosted = isPosted;
    }

    public String getNewsCategoryId() {
        return newsCategoryId;
    }

    public void setNewsCategoryId(String newsCategoryId) {
        this.newsCategoryId = newsCategoryId;
    }

    public String getCapturedLat() {
        return capturedLat;
    }

    public void setCapturedLat(String capturedLat) {
        this.capturedLat = capturedLat;
    }

    public String getCapturedLong() {
        return capturedLong;
    }

    public void setCapturedLong(String capturedLong) {
        this.capturedLong = capturedLong;
    }

    public String getCreatedOn() {
        return createdOn;
    }

    public void setCreatedOn(String createdOn) {
        this.createdOn = createdOn;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getAnonymous() {
        return anonymous;
    }

    public void setAnonymous(String anonymous) {
        this.anonymous = anonymous;
    }

    public int getCategoryType() {
        return categoryType;
    }

    public void setCategoryType(int categoryType) {
        this.categoryType = categoryType;
    }

}

