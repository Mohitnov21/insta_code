package com.abacusdesk.instafeed.instafeed.fragments;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.support.annotation.Nullable;
import android.support.annotation.RequiresApi;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.text.Spanned;
import android.text.method.ScrollingMovementMethod;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.MediaController;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.abacusdesk.instafeed.instafeed.NavDrawerActivity;
import com.abacusdesk.instafeed.instafeed.R;
import com.abacusdesk.instafeed.instafeed.Util.Alerts;
import com.abacusdesk.instafeed.instafeed.Util.Apis;
import com.abacusdesk.instafeed.instafeed.Util.CommonFunctions;
import com.abacusdesk.instafeed.instafeed.Util.SaveSharedPreference;
import com.abacusdesk.instafeed.instafeed.adapter.Comment_Adapter;
import com.abacusdesk.instafeed.instafeed.adapter.List_Adapter;
import com.abacusdesk.instafeed.instafeed.adapter.RecyclerCommentAdapter;
import com.abacusdesk.instafeed.instafeed.adapter.ViewPagerAdapter;
import com.abacusdesk.instafeed.instafeed.application.Change_like;
import com.abacusdesk.instafeed.instafeed.custom_dialog.Dialogs;
import com.abacusdesk.instafeed.instafeed.model.Opiniondetail_model;
import com.android.volley.AuthFailureError;
import com.android.volley.NoConnectionError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.gson.Gson;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Timer;


/**
 * Created by abacusdesk on 2017-08-08.
 */

public class Opiniondetail_fragment extends Fragment implements View.OnClickListener{

    View view;
    Opiniondetail_model opiniondetail_model;
    TextView txtname,txtdescr,txtshortdescr,txtdate,txtplace,txttitle;
    TextView txtlike,txtdislike,txtcomment,txtfav,txtvisible,txtusername;
    ImageView img_back,img_forward;
    public  EditText edcomment;
    public  RecyclerView listcomment;

    ArrayList<Opiniondetail_model.DataBean> arrayList=new ArrayList<>();
    ImageView imgmain,imgshare,img_onView,imguser;

    public  String id="",temp,temp2,comment="";

    private String vote="";
    public  ArrayList<Opiniondetail_model.DataBean.CommentsBean> arrayComment= new ArrayList<>();
    // ArrayList<NewsDetail.DataBean.ImagesBean> arrayImages= new ArrayList<>();
    Opiniondetail_model.DataBean dataBean_detail,databean_comment,databean_images,databean_videos;
    public  ArrayList<HashMap<String,String>> arrayhashComent=new ArrayList<>();
    public  RecyclerCommentAdapter adapter;
    ViewPagerAdapter viewPagerAdapter;
    RelativeLayout lnlistview;
    ViewPager imgviewpager;
    Boolean flag=false;

    LinearLayout ln_userdetail,lnlocation,lncomment,lnlike,lndislike,ln_recyclerrelated;
    public  ArrayList<HashMap<String,String>> arrayviewcontainer=new ArrayList<>();
    Boolean like=false,dislike=false,fav=false;
    TextView txt_ownername;
    ImageView img_userhead,imgonview;
    String location,imagetemp="";
    Bitmap imagebit;
    ImageView img_like,imgdislike;
    Change_like change_like;
    String likestatus;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
            view = inflater.inflate(R.layout.detail_finallayout, container, false);
            if ( NavDrawerActivity.frame_bottom!=null)
            NavDrawerActivity.frame_bottom.setVisibility(View.GONE);
            init();
            getActivity().getWindow()
            .setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_PAN
                    |WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);
            return view;
        }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        try {
            change_like = (Change_like) getActivity();
        } catch (Exception exception) {

        }
    }


    private void init(){
           comment_click();
            if (!arrayviewcontainer.isEmpty()){
                arrayviewcontainer.clear();
            }
            if (!arrayhashComent.isEmpty()){
                arrayhashComent.clear();
            }
            Bundle bundle = this.getArguments();
            if (bundle != null) {
                id=bundle.getString("id");
            }

            if (NavDrawerActivity.toolbar!=null){
                NavDrawerActivity.toolbar.setLogo(null);
                NavDrawerActivity.toolbar.setTitle("Opinions");
            }
            ln_recyclerrelated=(LinearLayout)view.findViewById(R.id.ln_recycler);
            ln_userdetail=(LinearLayout)view.findViewById(R.id.ln_publicprofile);
            imgviewpager=(ViewPager)view.findViewById(R.id.img_viewpager);
            imgshare=(ImageView)view.findViewById(R.id.img_share);

            lncomment=(LinearLayout)view.findViewById(R.id.ln_comment);
            img_like=(ImageView)view.findViewById(R.id.img_like);
            imgdislike=(ImageView)view.findViewById(R.id.img_dislike);
            lnlike=(LinearLayout)view.findViewById(R.id.ln_like);
            lndislike=(LinearLayout)view.findViewById(R.id.ln_down);

            lnlike.setOnClickListener(this);
            lndislike.setOnClickListener(this);

            imgonview=(ImageView)view.findViewById(R.id.img_view);
            txt_ownername=(TextView)view.findViewById(R.id.txt_owner);

            img_back=(ImageView)view.findViewById(R.id.img_left);
            img_forward=(ImageView)view.findViewById(R.id.img_right);
            img_back.setOnClickListener(this);
            img_forward.setOnClickListener(this);
            ln_userdetail.setOnClickListener(this);

            lnlocation=(LinearLayout)view.findViewById(R.id.ln_location);
            txtlike=(TextView)view.findViewById(R.id.txt_like);
            txtdislike=(TextView)view.findViewById(R.id.txt_dislike);

            txtcomment=(TextView)view.findViewById(R.id.txt_comment);
            //view1=(View)view.findViewById(R.id.view1);
            imgshare.setOnClickListener(this);

            lncomment.setOnClickListener(this);
            txtcomment.setOnClickListener(this);

            ln_recyclerrelated.setVisibility(View.GONE);

            //txtname=(TextView)view.findViewById(R.id.txt_name);
            txtdescr=(TextView)view.findViewById(R.id.txt_shortdescr);
            txtshortdescr=(TextView)view.findViewById(R.id.txt_descrshort);
            txtdate=(TextView)view.findViewById(R.id.txt_date);
            txtplace=(TextView)view.findViewById(R.id.txt_place);
            txttitle=(TextView)view.findViewById(R.id.txt_title);

            //txtname.setVisibility(View.VISIBLE);
            //view1.setVisibility(View.VISIBLE);

            temp= Apis.opinions_detail.replace("id",id);

            view.setFocusableInTouchMode(true);
            view.requestFocus();
            view.setOnKeyListener(new View.OnKeyListener() {
                @Override
                public boolean onKey(View v, int keyCode, KeyEvent event) {
                    if (keyCode == KeyEvent.KEYCODE_BACK) {

                        getActivity().getSupportFragmentManager().popBackStack();
                        return true;
                    } else {
                        return false;
                    }
                }
            });
            isLiked();
            getOpiniondetail(getActivity());
             try{
                viewPagerAdapter=new ViewPagerAdapter(getActivity(),arrayviewcontainer,"opinions");
                imgviewpager.setAdapter(viewPagerAdapter);

            } catch (Exception e){
                e.printStackTrace();
            }

            // JCVideoPlayer.releaseAllVideos();
            imgviewpager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
                @Override
                public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

                }
                @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
                @Override
                public void onPageSelected(int position) {
                    if (arrayviewcontainer.get(position).containsKey("video")){

                       // JCVideoPlayer.releaseAllVideos();
                    }
                    int currentPage = imgviewpager.getCurrentItem();
                    int totalPages = imgviewpager.getAdapter().getCount();

                    int nextPage = currentPage+1;
                    int previousPage = currentPage-1;

                    if (nextPage >= totalPages) {
                        img_forward.setVisibility(View.INVISIBLE);
                        //nextPage = 0;
                    }else {
                        img_forward.setVisibility(View.VISIBLE);
                    }

                    if (previousPage < 0) {
                        img_back.setVisibility(View.INVISIBLE);
                    }else {
                        img_back.setVisibility(View.VISIBLE);
                    }

                }

                @Override
                public void onPageScrollStateChanged(int state) {

                }

            });

        }

    public  void getResponse(String response,Context context ) throws JSONException {
        if(!response.equals(null)) {
            Log.e("log", "" + response);
            JSONObject jsonObject=new JSONObject(response);
            Object object=jsonObject.get("data");
            if (object instanceof JSONArray) {
                Gson gson = new Gson();
                opiniondetail_model = gson.fromJson(response,Opiniondetail_model.class);
                int status = opiniondetail_model.getStatus();
                String msg = opiniondetail_model.getMessage();

                if (msg.equalsIgnoreCase("success") && status == 200) {
                    dataBean_detail = opiniondetail_model.getData().get(0);
                    databean_comment = opiniondetail_model.getData().get(1);
                    databean_images = opiniondetail_model.getData().get(2);
                    databean_videos = opiniondetail_model.getData().get(3);

                    if (dataBean_detail != null) {


                        lnlocation.setVisibility(View.VISIBLE);
                        ln_userdetail.setVisibility(View.VISIBLE);
                        txt_ownername.setText(dataBean_detail.getUsername());
                        try{
                            if (dataBean_detail.getLocation()!=null)
                            {
                                txtplace.setText(dataBean_detail.getLocation());
                            }else {
                                lnlocation.setVisibility(View.GONE);
                            }
                        }
                        catch (Exception e){
                            e.printStackTrace();
                        }
                    }


                    try {
                        txtdate.setText("" + getDate(dataBean_detail.getDt_added()));
                    } catch (ParseException e) {
                        e.printStackTrace();
                    }
                    try {
                        txttitle.setText(dataBean_detail.getTitle());
                    }catch (Exception e){

                    }

                    txtdescr.setText(dataBean_detail.getDescription());

                    txtdescr.setMovementMethod(new ScrollingMovementMethod());

                    //txtshortdescr.setText(dataBean_detail.getShort_description());

                    if (txtshortdescr.getText().toString().equals("")){
                        txtshortdescr.setVisibility(View.GONE);
                    }
                    if (dataBean_detail.getTotal_up_votes() != null) {
                        txtlike.setText("" + dataBean_detail.getTotal_up_votes());
                    }
                    if (dataBean_detail.getTotal_down_votes() != null) {
                        txtdislike.setText("" + dataBean_detail.getTotal_down_votes());
                    }

                    if (dataBean_detail.getTotal_comments()!=null){
                        txtcomment.setText(dataBean_detail.getTotal_comments());
                    }

                } else {
                    ln_userdetail.setEnabled(false);
                    ln_userdetail.setClickable(false);
                }
                showCommentList();
                if (!arrayviewcontainer.isEmpty()) {
                    arrayviewcontainer.clear();
                }
                showImages();
            }
        }
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Log.e("oncreate","");
        setHasOptionsMenu(true);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        // TODO your code to hide item here
        Log.e("settings","");
        menu.findItem(R.id.menu_comment).setVisible(true);
        menu.findItem(R.id.menu_share).setVisible(true);
        menu.findItem(R.id.action_form).setVisible(false);
        menu.findItem(R.id.action_settings).setVisible(false);
        super.onCreateOptionsMenu(menu, inflater);
    }

    private void comment_click(){
        EditText edcomment=(EditText)view.findViewById(R.id.ed_comment);
        edcomment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getcommentlist();
            }
        });
    }

    /*
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.menu_comment:
                try{
                    CommentsList_fragment fragment = new CommentsList_fragment();
                    Bundle bundle = new Bundle();
                    bundle.putSerializable("arrayhash",arrayhashComent);
                    bundle.putString("postid",id);
                    bundle.putString("type","opinions");
                    fragment.setArguments(bundle);
                    AppCompatActivity activity = (AppCompatActivity) getContext();
                    activity.getSupportFragmentManager().beginTransaction()
                            .add(R.id.frame_container,fragment,"comment").addToBackStack(null).commit();

                }catch (Exception e){

                }

                return true;
            case R.id.menu_share:
                try{
                    MyAsync obj = new MyAsync() {
                        @Override
                        protected void onPostExecute(Bitmap bmp) {
                            super.onPostExecute(bmp);

                            imagebit = bmp;
                            Log.e("imagebit2", "" + imagebit);
                            ByteArrayOutputStream bytes = new ByteArrayOutputStream();
                            imagebit.compress(Bitmap.CompressFormat.JPEG, 100, bytes);
                            File f = new File(Environment.getExternalStorageDirectory() + File.separator + "temporary_file.jpg");
                            try {
                                f.createNewFile();
                                FileOutputStream fo = new FileOutputStream(f);
                                fo.write(bytes.toByteArray());

                            } catch (IOException e) {
                                e.printStackTrace();
                            }
                        }
                    };
                    if (!databean_images.getImages().isEmpty())
                        obj.execute();
                }catch (Exception e){

                }
                Intent sharingIntent = new Intent(Intent.ACTION_SEND);

                try{
                    sharingIntent.setType("
                    if (!databean_images.getImages().isEmpty())
                        sharingIntent.putExtra(Intent.EXTRA_STREAM, Uri.parse("file:///sdcard/temporary_file.jpg"));
                    //sharingIntent.putExtra(Intent.EXTRA_SUBJECT,dataBean_detail.getTitle());
                    sharingIntent.putExtra(Intent.EXTRA_TEXT, dataBean_detail.getTitle()+"\n"+Apis.opinions_share+dataBean_detail.getSlug());
                    Log.e("shreinterjyt",""+sharingIntent);
                    startActivity(Intent.createChooser(sharingIntent, "Share via"));
                }catch (Exception e){
                    Log.e("exception",""+e);
                }
               return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
   */

    public void sharecontent(){
        try{
            imagetemp="";
            if (!databean_videos.getVideos().isEmpty()){
                imagetemp=databean_videos.getVideos().get(0).getVideo_thumb();
            }else if (!databean_images.getImages().isEmpty()){
                imagetemp=databean_images.getImages().get(0).getImage();
            }

            MyAsync obj = new MyAsync() {
                @Override
                protected void onPostExecute(Bitmap bmp) {
                    super.onPostExecute(bmp);
                    ByteArrayOutputStream bytes = new ByteArrayOutputStream();
                    bmp.compress(Bitmap.CompressFormat.JPEG, 100, bytes);
                    File f = new File(Environment.getExternalStorageDirectory() + File.separator + "temporary_file.jpg");
                    try {
                        f.createNewFile();
                        FileOutputStream fo = new FileOutputStream(f);
                        fo.write(bytes.toByteArray());

                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            };
            if (!imagetemp.equals(""))
                obj.execute();
        }catch (Exception e){

        }
        Intent sharingIntent = new Intent(Intent.ACTION_SEND);
        try{
            sharingIntent.setType("*/*");
            if (!imagetemp.equals("")){
                sharingIntent.putExtra(Intent.EXTRA_STREAM, Uri.parse("file:///sdcard/temporary_file.jpg"));
            }else {
                sharingIntent.setType("text/plain");
            }
            sharingIntent.putExtra(Intent.EXTRA_TEXT, dataBean_detail.getTitle()+"\n"+Apis.opinions_share+dataBean_detail.getSlug());
            Log.e("shreinterjyt",""+sharingIntent);
            startActivity(Intent.createChooser(sharingIntent, "Share via"));
        }catch (Exception e){
            Log.e("exception",""+e);
        }
    }

    public void getcommentlist(){
        try{
            CommentsList_fragment fragment = new CommentsList_fragment();
            Bundle bundle = new Bundle();
            bundle.putSerializable("arrayhash",arrayhashComent);
            bundle.putString("postid",id);
            bundle.putString("type","opinions");
            fragment.setArguments(bundle);
            AppCompatActivity activity = (AppCompatActivity) getContext();
            activity.getSupportFragmentManager().beginTransaction()
                    .add(R.id.frame_container,fragment,"comment").addToBackStack(null).commit();

        }catch (Exception e){

        }

    }

    private void nextPage() {
            int currentPage = imgviewpager.getCurrentItem();
            int totalPages = imgviewpager.getAdapter().getCount();

            int nextPage = currentPage+1;
            if (nextPage >= totalPages) {
                // We can't go forward anymore.
                // Loop to the first page. If you don't want looping just
                // return here.
                nextPage = 0;
            }
            imgviewpager.setCurrentItem(nextPage, true);
        }

    private void previousPage() {
            int currentPage = imgviewpager.getCurrentItem();
            int totalPages = imgviewpager.getAdapter().getCount();

            int previousPage = currentPage-1;
            if (previousPage < 0) {
                previousPage = totalPages - 1;
            }
            imgviewpager.setCurrentItem(previousPage, true);
        }

    @Override
    public void onResume() {
            super.onResume();

        }

    public void getOpiniondetail(final Context context) {
            Dialogs.showProDialog(context, "Loading");
            Log.e("temporaryurl",""+temp);
            StringRequest stringRequest = new StringRequest(Request.Method.GET,temp ,
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {
                            Log.e("log", "" + response);
                            try {
                                getResponse(response,context);
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                            Dialogs.disDialog();
                        }
                    },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            Dialogs.disDialog();
                            DialogInterface.OnClickListener onClickTryAgain = new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    Dialogs.disDialog();
                                    getOpiniondetail(context);
                                }
                            };
                            if (error instanceof NoConnectionError) {
                                Dialogs.disDialog();
                                Alerts.internetConnectionErrorAlert(context, onClickTryAgain);
                            }
                        }
                    })

            {
                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    Map<String, String> params = new HashMap<String, String>();
                    params.put("Content-Type", "application/x-www-form-urlencoded");
                    return params;
                }
            };
            RequestQueue requestQueue = Volley.newRequestQueue(context);
            requestQueue.add(stringRequest);
        }

    public String getDate(String date) throws ParseException {
            String myFormat = "yyyy-MM-dd HH:mm:ss";
            DateFormat sdformat = new SimpleDateFormat(myFormat);
            DateFormat targetFormat = new SimpleDateFormat("MMMM dd, yyyy");
            String formattedDate = targetFormat.format(sdformat.parse(date));
            return formattedDate;
        }


    private  void showCommentList(){
        if (!arrayhashComent.isEmpty()){
            arrayhashComent.clear();
        }
        if (!databean_comment.getComments().isEmpty()){
            HashMap<String,String> hashMap;
            for (int i = 0; i < databean_comment.getComments().size(); i++) {
                try {
                    hashMap=new HashMap<>();
                    Opiniondetail_model.DataBean.CommentsBean commentsBean= databean_comment.getComments().get(i);
                    hashMap.put("commentid",commentsBean.getId());
                    hashMap.put("comment",commentsBean.getComment());
                    hashMap.put("userid",commentsBean.getUser_id());
                    hashMap.put("avatar",commentsBean.getAvatar());
                    hashMap.put("username",commentsBean.getUsername());
                    arrayhashComent.add(hashMap);
                 } catch (Exception e){
                    e.printStackTrace();
                }
            }
           // getComments(context);
            // updateDisplay(new CommentsList_fragment(),getApplicationContext());
           // adapter.notifyDataSetChanged();
        }
    }


    private  void CommentList(){
        arrayhashComent.clear();
        if (!databean_comment.getComments().isEmpty()){
            HashMap<String,String> hashMap;
            for (int i = 0; i < databean_comment.getComments().size(); i++) {
                try {
                    hashMap=new HashMap<>();
                    Opiniondetail_model.DataBean.CommentsBean commentsBean= databean_comment.getComments().get(i);
                    hashMap.put("commentid",commentsBean.getId());
                    hashMap.put("comment",commentsBean.getComment());
                    hashMap.put("userid",commentsBean.getUser_id());
                    hashMap.put("avatar",commentsBean.getAvatar());
                    hashMap.put("username",commentsBean.getUsername());
                    arrayhashComent.add(hashMap);
                } catch (Exception e){
                    e.printStackTrace();
                }
            }
            getActivity().getSupportFragmentManager().beginTransaction().add(R.id.frame_container,new CommentsList_fragment()).addToBackStack(null).commit();
            //getComments(context);
            // updateDisplay(new CommentsList_fragment(),getApplicationContext());
            //adapter.notifyDataSetChanged();
        }
    }

    /*  public   void updateDisplay(Fragment fragment,Context context) {
        context.getSupportFragmentManager().beginTransaction().add(R.id.frame_container,fragment).addToBackStack(null).commit();
    }*/

    private  void showImages( ){
            if (databean_images.getImages().size()!=0){
                HashMap<String,String> hashMap;
                for (int i = 0; i < databean_images.getImages().size(); i++) {
                    try {
                        hashMap=new HashMap<>();
                        Opiniondetail_model.DataBean.ImagesBean imagesBean= databean_images.getImages().get(i);
                        hashMap.put("image",imagesBean.getImage_256x170());
                        hashMap.put("imagezoom",imagesBean.getImage_zoom());
                        arrayviewcontainer.add(hashMap);
                    }catch (Exception e){
                        e.printStackTrace();
                    }
                }
            }
            getVideos();
            //viewPagerAdapter.notifyDataSetChanged();
        }

    private  void getVideos( ) {
        if (databean_videos.getVideos()!=null && !databean_videos.getVideos().isEmpty()){
            HashMap<String,String> hashMap;
            for (int i = 0; i < databean_videos.getVideos().size(); i++) {
                try {
                    hashMap=new HashMap<>();
                    Opiniondetail_model.DataBean.VideosBean videosBean= databean_videos.getVideos().get(i);
                    hashMap.put("video",videosBean.getVideo());
                    hashMap.put("image",videosBean.getVideo_thumb());
                    arrayviewcontainer.add(hashMap);
                }catch (Exception e){
                    e.printStackTrace();
                }
            }
        }
        Log.e("total_container",""+arrayviewcontainer);
        viewPagerAdapter.notifyDataSetChanged();
        if (arrayviewcontainer.isEmpty()){
            imgviewpager.setVisibility(View.GONE);
            imgonview.setVisibility(View.VISIBLE);
            imgonview.setImageResource(R.drawable.opiniondefault);
            img_back.setVisibility(View.INVISIBLE);
            img_forward.setVisibility(View.INVISIBLE);
        }else if (arrayviewcontainer.size()>=1){

            int currentPage = imgviewpager.getCurrentItem();
            int totalPages = imgviewpager.getAdapter().getCount();

            int nextPage = currentPage+1;
            int previousPage = currentPage-1;

            if (nextPage >= totalPages) {
                img_forward.setVisibility(View.INVISIBLE);
                //nextPage = 0;
            }else {
                img_forward.setVisibility(View.VISIBLE);
            }

            if (previousPage < 0) {
                img_back.setVisibility(View.INVISIBLE);
            }else {
                img_back.setVisibility(View.VISIBLE);
            }
        }
    }

    public void likePost() {
        Log.e("temporaryurl",""+temp2);
        StringRequest stringRequest = new StringRequest(Request.Method.POST, Apis.opinions_vote ,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.e("log", "" + response);
                        try {
                            JSONObject jsonObject=new JSONObject(response);
                            if (jsonObject.getString("status").equalsIgnoreCase("200")
                                    &&jsonObject.getString("message").equalsIgnoreCase("success")){
                                if (vote.equalsIgnoreCase("u")){
                                    img_like.setImageResource(R.drawable.thumb_black);
                                     likestatus="1";
                                     change_like.changelike("opinions");
                                }else {

                                }
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                },

                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Dialogs.disDialog();
                    }
                })
          {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> map = new HashMap<String, String>();
                map.put("user", SaveSharedPreference.getUserID(getActivity()));
                map.put("token", SaveSharedPreference.getPrefToken(getActivity()));
                map.put("id", id);
                map.put("vote",vote);
                Log.e("mapcomment",""+map);
                return map;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Content-Type", "application/x-www-form-urlencoded");
                return params;
            }
        };

        RequestQueue requestQueue = Volley.newRequestQueue(getActivity());
        requestQueue.add(stringRequest);
    }


    public void revert_like() {
        StringRequest stringRequest = new StringRequest(Request.Method.POST,
                Apis.opinions_delvote,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.e("log", "" + response);
                        try {
                            JSONObject jsonObject = new JSONObject(response);
                            if (jsonObject.getString("status").equals("200") &&
                                    jsonObject.getString("message").equals("success")) {
                               img_like.setImageResource(R.drawable.like_rr);
                               likestatus="0";
                               change_like.changelike("opinions");
                            }else {

                            }
                        } catch (JSONException e) {
                            // e.printStackTrace();
                        } catch (Exception e) {

                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        //Dialogs.disDialog();
                    }
                })
        {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> map = new HashMap<String, String>();
                map.put("user", SaveSharedPreference.getUserID(getContext()));
                map.put("token", SaveSharedPreference.getPrefToken(getContext()));
                map.put("id", id);
                Log.e("mapvote", "" + map);
                return map;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Content-Type", "application/x-www-form-urlencoded");
                return params;
            }
        };
        RequestQueue requestQueue = Volley.newRequestQueue(getContext());
        requestQueue.add(stringRequest);
    }

    public void isLiked() {
        Log.e("temporaryurl",""+temp2);
        StringRequest stringRequest = new StringRequest(Request.Method.POST,
                Apis.opinions_isvote ,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.e("log", "" + response);
                        try {
                            JSONObject jsonObject=new JSONObject(response);
                            if (jsonObject.getString("status").equals("200")){
                                String status= jsonObject.getJSONObject("data").getString("vote_status");
                                if (status.equalsIgnoreCase("d")){
                                   likestatus="0";
                                }else if (status.equalsIgnoreCase("u")){
                                    img_like.setImageResource(R.drawable.thumb_black);
                                    likestatus="1";
                                }else {
                                    likestatus="0";
                                }
                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }catch (Exception e){

                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Dialogs.disDialog();

                    }
                })
        {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> map = new HashMap<String, String>();
                map.put("user", SaveSharedPreference.getUserID(getActivity()));
                map.put("token", SaveSharedPreference.getPrefToken(getActivity()));
                map.put("id", id);
                Log.e("mapvote",""+map);
                return map;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Content-Type", "application/x-www-form-urlencoded");
                return params;
            }
        };
        RequestQueue requestQueue = Volley.newRequestQueue(getActivity());
        requestQueue.add(stringRequest);
    }

    public  class MyAsync extends AsyncTask<Void, Void, Bitmap> {
        @Override
        protected Bitmap doInBackground(Void... params) {
            try {
                if (!imagetemp.equals("")){
                    URL url = new URL(imagetemp);
                    HttpURLConnection connection = (HttpURLConnection) url.openConnection();
                    connection.setDoInput(true);
                    connection.connect();
                    InputStream input = connection.getInputStream();
                    Bitmap myBitmap = BitmapFactory.decodeStream(input);
                    return myBitmap;
                }else {
                    return null;
                }
            } catch (IOException e) {
                e.printStackTrace();
                return null;
            }catch (Exception e){
                return  null;
            }
        }
    }

    @Override
    public void onClick(View v) {
            if (v==lnlike){
                if (SaveSharedPreference.getUserID(getContext()).equals("")){
                    Toast.makeText(getContext(),"Please Login to continue!", Toast.LENGTH_SHORT).show();
                }else {
                  if (likestatus.equalsIgnoreCase("1")){
                     revert_like();
                 } else {
                     vote="u";
                     likePost();
                    }
                }
            }else if(v==lncomment){
                try{
                    CommentsList_fragment fragment = new CommentsList_fragment();
                    Bundle bundle = new Bundle();
                    bundle.putSerializable("arrayhash",arrayhashComent);
                    bundle.putString("postid",id);
                    bundle.putString("type","opinions");
                    fragment.setArguments(bundle);
                    AppCompatActivity activity = (AppCompatActivity) v.getContext();
                    activity.getSupportFragmentManager().beginTransaction()
                            .add(R.id.frame_container,fragment,"comment").addToBackStack(null).commit();

                }catch (Exception e){

                }
            } else if (v==imgshare){

                try{
                    MyAsync obj = new MyAsync() {
                        @Override
                        protected void onPostExecute(Bitmap bmp) {
                            super.onPostExecute(bmp);

                            imagebit = bmp;
                            Log.e("imagebit2", "" + imagebit);
                            ByteArrayOutputStream bytes = new ByteArrayOutputStream();
                            imagebit.compress(Bitmap.CompressFormat.JPEG, 100, bytes);
                            File f = new File(Environment.getExternalStorageDirectory() + File.separator + "temporary_file.jpg");
                            try {
                                f.createNewFile();
                                FileOutputStream fo = new FileOutputStream(f);
                                fo.write(bytes.toByteArray());

                            } catch (IOException e) {
                                e.printStackTrace();
                            }
                        }
                    };
                    if (!databean_images.getImages().isEmpty())
                     obj.execute();
                }catch (Exception e){

                }
                Intent sharingIntent = new Intent(Intent.ACTION_SEND);

                try{
                    sharingIntent.setType("*/*");
                    if (!databean_images.getImages().isEmpty())
                    sharingIntent.putExtra(Intent.EXTRA_STREAM, Uri.parse("file:///sdcard/temporary_file.jpg"));
                    //sharingIntent.putExtra(Intent.EXTRA_SUBJECT,dataBean_detail.getTitle());
                    sharingIntent.putExtra(Intent.EXTRA_TEXT, dataBean_detail.getTitle()+"\n"+Apis.opinions_share+dataBean_detail.getSlug());
                    Log.e("shreinterjyt",""+sharingIntent);
                    startActivity(Intent.createChooser(sharingIntent, "Share via"));
                }catch (Exception e){
                    Log.e("exception",""+e);
                }
            } else if (v==img_back){
                previousPage();
            } else if (v==img_forward){
                nextPage();
            }
        }
 }

