package com.abacusdesk.instafeed.instafeed.fragments;


import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.database.Cursor;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.abacusdesk.instafeed.instafeed.NavDrawerActivity;
import com.abacusdesk.instafeed.instafeed.R;
import com.abacusdesk.instafeed.instafeed.Util.DbHandler;
import com.abacusdesk.instafeed.instafeed.Util.MasterTblModel;
import com.abacusdesk.instafeed.instafeed.Util.SendOfflineData;
import com.abacusdesk.instafeed.instafeed.adapter.SaveOfflineAdapter;
import com.abacusdesk.instafeed.instafeed.model.GridModel;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import static com.abacusdesk.instafeed.instafeed.Util.SendOfflineData.IS_POSTED;

/**
 * Created by Administrator on 6/26/2017.
 */

public class SaveOfflineFragment extends Fragment {

    RecyclerView recyclerView_saveOffline;
    MasterTblModel masterTblModel;
    ArrayList<GridModel>dbList;
    ArrayList<MasterTblModel>masterTableList;
    HashMap<String,List<GridModel>> fileList;
    TextView emptyView;
    String newsId,post_newsId,fileName,filePath,fileType ,deviceId,prefToken,userID,newsCategoryId,capturedLat,capturedLong,createdOn,title,description;
    public static SaveOfflineAdapter saveOfflineAdapter;
    DbHandler dbHandler;

    private BroadcastReceiver notificationReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            refreshData();
            Log.d("TAG", "onReceive: ");
        }
    };

    private void refreshData() {
        int count= fetchOfflineDataFromMasterTable();
        if(count<1){
            recyclerView_saveOffline.setVisibility(View.GONE);
            emptyView.setVisibility(View.VISIBLE);
        }else {
            recyclerView_saveOffline.setVisibility(View.VISIBLE);
            emptyView.setVisibility(View.GONE);
        }
        // saveOfflineAdapter.notifyDataSetChanged();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view=inflater.inflate(R.layout.fragment_save_offline,container,false);
        IntentFilter notificationFilter = new IntentFilter(SendOfflineData.DATASYNC);
        getActivity().registerReceiver(notificationReceiver, notificationFilter);

        if (NavDrawerActivity.toolbar!=null){
            NavDrawerActivity.toolbar.setLogo(R.drawable.logo_black);
        }
        dbList=new ArrayList<>();
        masterTableList=new ArrayList<>();
        fileList= new HashMap<>();
        fileList.clear();
        masterTableList.clear();
        dbList.clear();

        dbHandler=new DbHandler(getActivity());
        dbHandler.open();
        // setRecyclerView(view);
        emptyView=(TextView)view.findViewById(R.id.empty_view);
        recyclerView_saveOffline=(RecyclerView)view.findViewById(R.id.save_recyclerList);
        RecyclerView.LayoutManager linearLayoutManager=new LinearLayoutManager(getActivity());
        recyclerView_saveOffline.setLayoutManager(linearLayoutManager);
        recyclerView_saveOffline.hasFixedSize();
        saveOfflineAdapter = new SaveOfflineAdapter(getActivity(),masterTableList,fileList);
        recyclerView_saveOffline.setAdapter(saveOfflineAdapter);
        recyclerView_saveOffline.addItemDecoration(new DividerItemDecoration(getActivity(),
                DividerItemDecoration.VERTICAL));
        saveOfflineAdapter.notifyDataSetChanged();


        int count= fetchOfflineDataFromMasterTable();
        if(count<1){
            recyclerView_saveOffline.setVisibility(View.GONE);
            emptyView.setVisibility(View.VISIBLE);
        }else {
            recyclerView_saveOffline.setVisibility(View.VISIBLE);
            emptyView.setVisibility(View.GONE);
        }
        return view;
    }

    private int fetchOfflineDataFromMasterTable() {
        masterTableList.clear();
        Cursor m = dbHandler.fetch_UnPostedData(IS_POSTED);
        Log.i("oflineData4rmMsterTble", m.getCount() + "");
        if (m.getCount() > 0) {
            try {
                if (m.moveToFirst()) {
                    do {
                        Log.i("SendOfflinedata 1", "tag 1");
                        newsId = m.getString(0);  ///primary key
                        Log.d("newsId_primaryKey", "sendOfflinePost: " + m.getString(0));
                        deviceId = m.getString(1);
                        Log.d("deviceId", "sendOfflinePost: " + m.getString(1));
                        prefToken = m.getString(3);
                        Log.d("prefToken", "sendOfflinePost: " + m.getString(3));
                        userID = m.getString(4);
                        Log.d("userID", "sendOfflinePost: " + m.getString(4));
                        newsCategoryId = m.getString(6);
                        Log.d("newsCategoryId", "sendOfflinePost: " + m.getString(6));
                        capturedLat = m.getString(11);
                        Log.d("capturedLat", "sendOfflinePost: " + m.getString(11));
                        capturedLong = m.getString(12);
                        Log.d("capturedLong", "sendOfflinePost: " + m.getString(12));
                        createdOn = m.getString(9);
                        Log.d("createdOn", "sendOfflinePost: " + m.getString(9));
                        title = m.getString(7);
                        Log.d("title", "sendOfflinePost: " + m.getString(7));
                        description = m.getString(8);
                        Log.d("description", "sendOfflinePost: " + m.getString(8));
                        masterTableList.add(new MasterTblModel(newsId,title,description));
                        Log.d("TAG", "fetchOfflineDataFromMasterTable: "+masterTableList.size()+"");
                    } while (m.moveToNext());
                }
            } catch (Exception e) {
                e.printStackTrace();
            }

            for (int j=0;j<masterTableList.size();j++){
                MasterTblModel masterTblModel=masterTableList.get(j);
                //fetch media files
                fetchNewsDataAccordingToNewsId(masterTblModel.getMasterPrimaryKey());
                Log.d("TAG", "fetchOfflineDataFromMasterTable: "+masterTblModel.getMasterPrimaryKey());

            }
        }else{
            //handle  tilte and descrip without media files
            saveOfflineAdapter.notifyDataSetChanged();
            //logic
            return 0;
        }
        return m.getCount();
    }

    private void fetchNewsDataAccordingToNewsId(String newsId) {
        ArrayList<GridModel>  mediaFile=new ArrayList<>();
        mediaFile.clear();
        Cursor m = dbHandler.fetch_MediaFiles(newsId);
        Log.i("fetchNewsDataAccNewsId", m.getCount() + ""+"***"+newsId);
        if (m.getCount() > 0) {
            try {
                if (m.moveToFirst()) {
                    do {

                        Log.i("fetchNewsAccoToNewsId 1", "tag 1");
                        String post_newsId1 = m.getString(1);   //peimary key of master table
                        Log.d("post_newsId", m.getString(1));
                        String   fileType = m.getString(2);
                        Log.d("fileType", m.getString(2));
                        String fileName = m.getString(3);
                        Log.d("fileName", m.getString(3));
                        String filePath = m.getString(4);
                        Log.d("filePath", m.getString(4));
                        mediaFile.add(new GridModel(fileType,filePath,fileName));

                    } while (m.moveToNext());
                }
                // dbList.add(new GridModel(mediaList,title,description,newsId));
                // saveOfflineAdapter.notifyDataSetChanged();
            } catch (Exception e) {
                e.printStackTrace();
            }
            fileList.put(newsId,mediaFile);
            saveOfflineAdapter.notifyDataSetChanged();
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        IntentFilter notificationFilter = new IntentFilter(SendOfflineData.DATASYNC);
        getActivity().registerReceiver(notificationReceiver, notificationFilter);
    }

    @Override
    public void onStop() {
        super.onStop();
        try {
            if(notificationReceiver != null)
                getActivity().unregisterReceiver(notificationReceiver);
        } catch (IllegalArgumentException e) {
            e.printStackTrace();
        }
    }
}