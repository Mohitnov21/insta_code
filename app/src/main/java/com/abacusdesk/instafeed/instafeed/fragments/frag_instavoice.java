package com.abacusdesk.instafeed.instafeed.fragments;

import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.abacusdesk.instafeed.instafeed.R;
import com.abacusdesk.instafeed.instafeed.Util.Alerts;
import com.abacusdesk.instafeed.instafeed.Util.Apis;
import com.abacusdesk.instafeed.instafeed.Util.CommonFunctions;
import com.abacusdesk.instafeed.instafeed.Util.SaveSharedPreference;
import com.abacusdesk.instafeed.instafeed.adapter.Explore_Adapter;
import com.abacusdesk.instafeed.instafeed.adapter.Petition_Adapter;
import com.abacusdesk.instafeed.instafeed.custom_dialog.Dialogs;
import com.abacusdesk.instafeed.instafeed.model.Explorer_model;
import com.abacusdesk.instafeed.instafeed.model.Petition_model;
import com.android.volley.AuthFailureError;
import com.android.volley.NoConnectionError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by ishaan on 6/19/2017.
 */



import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.abacusdesk.instafeed.instafeed.R;
import com.abacusdesk.instafeed.instafeed.Util.Alerts;
import com.abacusdesk.instafeed.instafeed.Util.Apis;
import com.abacusdesk.instafeed.instafeed.Util.CommonFunctions;
import com.abacusdesk.instafeed.instafeed.adapter.Explore_Adapter;
import com.abacusdesk.instafeed.instafeed.adapter.Recycler_newAdapter;
import com.abacusdesk.instafeed.instafeed.custom_dialog.Dialogs;
import com.abacusdesk.instafeed.instafeed.model.Explorer_model;
import com.abacusdesk.instafeed.instafeed.model.NewsModel;
import com.android.volley.AuthFailureError;
import com.android.volley.NoConnectionError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.gson.Gson;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;


/**
 * Created by ishaan on 6/19/2017.
 */

public class frag_instavoice extends Fragment{

        View view;
        RecyclerView recyclerView;
        Petition_Adapter adapter;
        ArrayList<Petition_model.DataBean> array_petition=new ArrayList<>();
        Petition_model petitionModel;
        public  String  Api_temp="";
        Map<String, String> map ;
        TextView txtrecords;
        @Nullable
        @Override
        public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
            view = inflater.inflate(R.layout.frag_recycler, container, false);
            recyclerView = (RecyclerView) view.findViewById(R.id.recyclerView);
            txtrecords=(TextView)view.findViewById(R.id.txt_records);
            if (Publicprofile_Fragment.flag==true){
                Api_temp=Apis.public_petitions;
              //  map.clear();
                map=new HashMap<>();
                map.put("username",Publicprofile_Fragment.username);
            }else {
                Api_temp=Apis.private_petitions;
              //  map.clear();
                map=new HashMap<>();
                map.put("user", SaveSharedPreference.getUserID(getActivity()));
                map.put("token",SaveSharedPreference.getPrefToken(getActivity()));
            }
            if (CommonFunctions.isConnected(getActivity())){
                if (!array_petition.isEmpty()){
                    array_petition.clear();
                }
                getPetitionList();
            }else {
                Toast.makeText(getActivity(), "Internet not available", Toast.LENGTH_SHORT).show();
            }
            setRecyclerView();
            return view;
        }

        public void getPetitionList() {
           // Dialogs.showProDialog(getActivity(), "Loading");
            StringRequest stringRequest = new StringRequest(Request.Method.POST, Api_temp,
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {
                            Log.e("log", "" + response);
                            try {
                                getResponse(response);
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                            Dialogs.disDialog();
                        }
                    },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            Dialogs.disDialog();
                            DialogInterface.OnClickListener onClickTryAgain = new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    Dialogs.disDialog();
                                    getPetitionList();
                                }
                            };
                            if (error instanceof TimeoutError) {
                                Dialogs.disDialog();
                                Alerts.timeoutErrorAlert(getContext(), onClickTryAgain);
                            } else if (error instanceof NoConnectionError) {
                                Dialogs.disDialog();
                                Alerts.internetConnectionErrorAlert(getContext(), onClickTryAgain);
                            }
                        }
                    })
            {
                @Override
                protected Map<String, String> getParams() throws AuthFailureError {
                    return map;
                }
                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    Map<String, String> params = new HashMap<String, String>();
                    params.put("Content-Type", "application/x-www-form-urlencoded");
                    return params;
                }
            };

            RequestQueue requestQueue = Volley.newRequestQueue(getActivity());
            requestQueue.add(stringRequest);
        }

        private void getResponse(String response ) throws JSONException {
            if(!response.equals(null)) {
                Log.e("log", "" + response);
                JSONObject jsonObject = new JSONObject(response);
                Object object = jsonObject.get("data");
                if (object instanceof JSONArray) {
                    Gson gson = new Gson();
                    petitionModel = gson.fromJson(response, Petition_model.class);
                    int status = petitionModel.getStatus();
                    String msg = petitionModel.getMessage();
                    if (msg.equalsIgnoreCase("success") && status == 200) {
                        if (petitionModel.getData() != null && !petitionModel.getData().isEmpty()) {
                            for (int i = 0; i < petitionModel.getData().size(); i++) {
                                try {
                                    Petition_model.DataBean dataBean = petitionModel.getData().get(i);
                                    array_petition.add(dataBean);
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }
                            }
                        }
                        if (array_petition.isEmpty()) {
                            txtrecords.setVisibility(View.VISIBLE);
                        }
                        Log.e("databen data", "" + array_petition);
                        adapter.notifyDataSetChanged();
                    }
                }
            }
        }

        // poills blogs opiunions
        // working petition talents  news
        // Setting recycler view

        private void setRecyclerView() {
            recyclerView.setHasFixedSize(true);
            recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
            // recyclerView.addItemDecoration(mDividerItemDecoration);
            try{
                adapter= new Petition_Adapter(getActivity(),array_petition);
                recyclerView.setAdapter(adapter);
                adapter.notifyDataSetChanged();
                // Log.e("responseset",""+newsDataBeanList);
            }catch (Exception e){
                e.printStackTrace();
            }
        }
    }

