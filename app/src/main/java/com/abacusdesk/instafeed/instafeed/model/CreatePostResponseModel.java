package com.abacusdesk.instafeed.instafeed.model;

/**
 * Created by Administrator on 6/11/2017.
 */

public class CreatePostResponseModel {


    /**
     * status : 200
     * message : success
     * data : {"news_id":398,"user_id":"3924","slug":"testing-sonam3-1497165454"}
     */

    @com.google.gson.annotations.SerializedName("status")
    private int status;
    @com.google.gson.annotations.SerializedName("message")
    private String message;
    @com.google.gson.annotations.SerializedName("data")
    private DataBean data;

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public DataBean getData() {
        return data;
    }

    public void setData(DataBean data) {
        this.data = data;
    }

    public static class DataBean {
        /**
         * news_id : 398
         * user_id : 3924
         * slug : testing-sonam3-1497165454
         */

        @com.google.gson.annotations.SerializedName("news_id")
        private int newsId;
        @com.google.gson.annotations.SerializedName("user_id")
        private String userId;
        @com.google.gson.annotations.SerializedName("slug")
        private String slug;

        public int getNewsId() {
            return newsId;
        }

        public void setNewsId(int newsId) {
            this.newsId = newsId;
        }

        public String getUserId() {
            return userId;
        }

        public void setUserId(String userId) {
            this.userId = userId;
        }

        public String getSlug() {
            return slug;
        }

        public void setSlug(String slug) {
            this.slug = slug;
        }
    }
}
