package com.abacusdesk.instafeed.instafeed.adapter;

import android.app.Dialog;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.abacusdesk.instafeed.instafeed.R;
import com.abacusdesk.instafeed.instafeed.Util.SaveSharedPreference;
import com.abacusdesk.instafeed.instafeed.fragments.Newsdetail_fragment;
import com.abacusdesk.instafeed.instafeed.model.NewsDetail;
import com.bumptech.glide.Glide;

import java.util.ArrayList;


/**
 * Created by ishaan on 6/12/2017.
 */

public class List_Adapter extends BaseAdapter {

   Context context;
   ArrayList<NewsDetail.DataBean.CommentsBean> arrayList;
   public static String comment="",comment_id;
    Dialog dialog;
   public List_Adapter(Context context, ArrayList<NewsDetail.DataBean.CommentsBean> arrayList){
       this.context=context;
       this.arrayList=arrayList;
       notifyDataSetChanged();
   }
    @Override
    public int getCount() {
        return arrayList.size();
    }

    @Override
    public Object getItem(int position) {
        return position;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        LayoutInflater layoutInflater = LayoutInflater.from(context);
        convertView = layoutInflater.inflate(R.layout.item_rowlist, null, false);
        final NewsDetail.DataBean.CommentsBean dataBean=arrayList.get(position);

        ImageView imgprofile=(ImageView)convertView.findViewById(R.id.img_profilepic);
        ImageView imgedit=(ImageView)convertView.findViewById(R.id.img_edit);
        TextView txtname=(TextView)convertView.findViewById(R.id.txt_name);
        TextView txtcomment=(TextView)convertView.findViewById(R.id.txt_comment);
        txtcomment.setText(dataBean.getComment());
        txtname.setText(dataBean.getUsername());
        if (dataBean.getAvatar()!=null && !dataBean.getAvatar().isEmpty()){
            Glide.with(context).load(dataBean.getAvatar()).error(R.drawable.user).into(imgprofile);
        }else {
            imgprofile.setImageResource(R.drawable.user);
        }
        if (dataBean.getUser_id().equals(SaveSharedPreference.getUserID(context))){
             imgedit.setVisibility(View.VISIBLE);
        }else {
            imgedit.setVisibility(View.INVISIBLE);
        }
        imgedit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                comment=dataBean.getComment();
                comment_id=dataBean.getId();
                edit_dialog();
            }
        });
        return convertView;
    }

    public void edit_dialog() {
        dialog = new Dialog(context);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(true);
        dialog.setContentView(R.layout.dialog_comment);
        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        lp.copyFrom(dialog.getWindow().getAttributes());
        lp.width = WindowManager.LayoutParams.MATCH_PARENT;
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
        dialog.getWindow().setAttributes(lp);
        final EditText edcomment=(EditText)dialog.findViewById(R.id.ed_comment);
        final Button btn_edit=(Button)dialog.findViewById(R.id.btn_edit);
        edcomment.setText(comment);
        btn_edit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                comment=edcomment.getText().toString();
                if (comment!=""){
                   // Newsdetail_fragment.editComment(context,comment,comment_id);
                    dialog.dismiss();
                }
            }
        });
        dialog.show();
    }

    public void refreshEvents(ArrayList<NewsDetail.DataBean.CommentsBean> arrayList) {
        this.arrayList.clear();
        this.arrayList.addAll(arrayList);
        notifyDataSetChanged();
    }
}
