package com.abacusdesk.instafeed.instafeed.adapter;

/**
 * Created by abacusdesk on 2017-08-28.
 */


import android.content.Context;
import android.content.Intent;
import android.support.annotation.IntegerRes;
import android.support.v4.view.PagerAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;


import com.abacusdesk.instafeed.instafeed.R;

import java.util.ArrayList;


public class SlidAdapter extends PagerAdapter {


    private int[] arrayimages;
    private LayoutInflater inflater;
    private Context context;


    public SlidAdapter(Context context, int[] arrayimages) {
        this.context = context;
        this.arrayimages = arrayimages;
        inflater = LayoutInflater.from(context);
    }


    @Override
    public int getCount() {
        return arrayimages.length;
    }

    @Override
    public Object instantiateItem(ViewGroup view, final int position) {
        View imageLayout = inflater.inflate(R.layout.singleimage_full, view, false);


        assert imageLayout != null;
        final ImageView imageView = (ImageView) imageLayout.findViewById(R.id.image);

        imageView.setBackgroundResource(arrayimages[position]);

        view.addView(imageLayout);

        return imageLayout;
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == ((FrameLayout) object);
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        container.removeView((FrameLayout) object);
    }
}