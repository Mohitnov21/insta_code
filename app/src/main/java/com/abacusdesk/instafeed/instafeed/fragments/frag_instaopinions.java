package com.abacusdesk.instafeed.instafeed.fragments;

import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.abacusdesk.instafeed.instafeed.R;
import com.abacusdesk.instafeed.instafeed.Util.Alerts;
import com.abacusdesk.instafeed.instafeed.Util.Apis;
import com.abacusdesk.instafeed.instafeed.Util.CommonFunctions;
import com.abacusdesk.instafeed.instafeed.Util.SaveSharedPreference;
import com.abacusdesk.instafeed.instafeed.adapter.Opinion_Adapter;
import com.abacusdesk.instafeed.instafeed.custom_dialog.Dialogs;
import com.abacusdesk.instafeed.instafeed.model.OpinionModel_public;
import com.android.volley.AuthFailureError;
import com.android.volley.NoConnectionError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.gson.Gson;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by ishaan on 6/19/2017.
 */

public class frag_instaopinions extends Fragment{

    View view;
    RecyclerView recyclerView;
    Opinion_Adapter adapter;
    ArrayList<OpinionModel_public.DataBean> arrayOpinion=new ArrayList<>();
    OpinionModel_public opinionModel_public;
    public  String  Api_temp="";
    Map<String, String> map ;
    TextView txtrecords;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.frag_recycler, container, false);
        recyclerView = (RecyclerView) view.findViewById(R.id.recyclerView);


        txtrecords=(TextView)view.findViewById(R.id.txt_records);
        if (Publicprofile_Fragment.flag==true){
            Api_temp=Apis.public_opinions;
           // map.clear();
            map=new HashMap<>();
            map.put("username",Publicprofile_Fragment.username);
        }else {
            Api_temp=Apis.private_opinions;
           // map.clear();
            map=new HashMap<>();
            map.put("user", SaveSharedPreference.getUserID(getActivity()));
            map.put("token",SaveSharedPreference.getPrefToken(getActivity()));
        }

        if (CommonFunctions.isConnected(getActivity())){
            if (!arrayOpinion.isEmpty()){
                arrayOpinion.clear();
            }
            getopinionslist();
        }else {
            Toast.makeText(getActivity(), "Internet not available", Toast.LENGTH_SHORT).show();
        }
        setRecyclerView();
        return view;
    }

    public void getopinionslist() {
       // Dialogs.showProDialog(getActivity(), "Loading");
        StringRequest stringRequest = new StringRequest(Request.Method.POST, Api_temp,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.e("response_opinion", "" + response);
                        try {
                            getResponse(response);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        Dialogs.disDialog();
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {

                    }
                })
        {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                return map;
            }
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Content-Type", "application/x-www-form-urlencoded");
                return params;
            }
        };

        RequestQueue requestQueue = Volley.newRequestQueue(getActivity());
        requestQueue.add(stringRequest);
    }

    private void getResponse(String response ) throws JSONException {
        if(!response.equals(null)) {
           // Log.e("log", "" + response);
            JSONObject jsonObject = new JSONObject(response);
            Object object = jsonObject.get("data");
            if (object instanceof JSONArray) {
                Gson gson = new Gson();
                opinionModel_public = gson.fromJson(response, OpinionModel_public.class);
                int status = opinionModel_public.getStatus();
                String msg = opinionModel_public.getMessage();
                if (msg.equalsIgnoreCase("success") && status == 200) {
                    if (opinionModel_public.getData() != null && !opinionModel_public.getData().isEmpty()) {
                        for (int i = 0; i < opinionModel_public.getData().size(); i++) {
                            try {
                                OpinionModel_public.DataBean dataBean = opinionModel_public.getData().get(i);
                                arrayOpinion.add(dataBean);
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }
                    }
                    if (arrayOpinion.isEmpty()) {
                        txtrecords.setVisibility(View.VISIBLE);
                    }
                    adapter.notifyDataSetChanged();
                }
            }
        }
    }

    // poills blogs opiunions
    // working petition talents  news
    // Setting recycler view

    private void setRecyclerView() {
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        // recyclerView.addItemDecoration(mDividerItemDecoration);
        try{
            adapter= new Opinion_Adapter(getActivity(),arrayOpinion);
            recyclerView.setAdapter(adapter);
            adapter.notifyDataSetChanged();
            // Log.e("responseset",""+newsDataBeanList);
        }catch (Exception e){
            e.printStackTrace();
        }
    }
}
