package com.abacusdesk.instafeed.instafeed.Util;

/**
 * Created by ishaan on 6/7/2017.
 */

public class Apis {

  //http://139.59.79.29/api/
   public static String Base2 ="http://instafeed.org/";
  // public static String Base ="http://139.59.79.29/api/";

   public static String Base ="http://instafeed.org/api/";

   public static String login =Base+"login";
   public static String signup =Base+"signup";
   public static String social =Base+"social";
   public static String profile =Base+"profile";
   public static String news =Base+"news/all";
   public static String comment =Base+"news/comment";
   public static String vote =Base+"news/vote";
   public static String fav =Base+"news/fav";
   public static String logout =Base+"logout";
   public static String blogs =Base+"blogs/all";
   public static String comment_edit =Base+"news/comment/edit";
   public static String profile_update =Base+"profile/update";
   public static String change_password =Base+"change_password";
   public static String update_pic =Base+"profile/update_pic";
   public static String getlocation =Base+"locations/id/id2";
   public static String mobilesignup =Base+"signup/otp";
   public static String verifyOtp =Base+"signup/otp/verify";


    //Rss apis
    public static String newsrss =Base+"news/allrss";
    public static String newsrss_detail =Base+"news/rss/id/posts";

    //news
    public static String news_allbookmark =Base+"news/getbookmarks";
    public static String news_isbookmark =Base+"news/isbookmark";
    public static String news_bookmark =Base+"news/bookmark";
    public static String news_unbookmark =Base+"news/unbookmark";

    public static String news_detail =Base+"news/category/catid/posts/postid";
    public static String news_Share=Base2+"news/";
    public static String isVote =Base+"news/isvote";
    public static String delete_Vote =Base+"news/delvote";
    public static String related_news =Base+"news/category/id/related";
    public static String related_talents =Base+"talents/category/id/related";
    public static String related_explore =Base+"explore/category/id/related";
    public static String related_blogs =Base+"blogs/category/id/related";

    //explore
    public static String explore_detail= Base+"explore/category/catid/posts/postid";
    public static String explore_share= Base2+"explorer/";

   // public profile apis
   public static String public_profile =Base+"profile/public";
   public static String public_news =Base+"profile/public/news";
   public static String public_opinions =Base+"profile/public/opinions";
   public static String public_petitions =Base+"profile/public/petitions";
   public static String public_talents =Base+"profile/public/talents";
   public static String public_explorer =Base+"profile/public/explorer";
   public static String public_polls =Base+"profile/public/polls";
   public static String public_blogs =Base+"profile/public/blogs";

   // Private profile apis
   public static String private_profile =Base+"profile";
   public static String private_news =Base+"profile/news";
   public static String private_opinions =Base+"profile/opinions";
   public static String private_petitions =Base+"profile/petitions";
   public static String private_talents =Base+"profile/talents";
   public static String private_explorer =Base+"profile/explorer";
   public static String private_polls =Base+"profile/polls";
   public static String private_blogs =Base+"profile/blogs";
   public static String private_newsdelete =Base+"profile/news/delete";
   public static String private_exploredelete =Base+"profile/explorer/delete";
   public static String private_talentdelete =Base+"profile/talents/delete";
   public static String private_petitionsdelete =Base+"profile/petitions/delete";
   public static String private_opinionsdelete =Base+"profile/opinions/delete";
   public static String private_picdelete =Base+"profile/delete_pic";

   // Follow and follwers of mine
   public static String private_followers=Base+"profile/followers";
   public static String private_following=Base+"profile/following";


   //all opinions
   public static String opinions_all =Base+"opinions/all";
   public static String opinions_detail =Base+"opinions/post/id";
   public static String opinions_comment =Base+"opinions/comment";
   public static  String opinions_commentedit =Base+"opinions/comment/edit";
   public static String opinions_vote =Base+"opinions/vote";
   public static String opinions_delvote =Base+"opinions/delvote";
   public static String opinions_isvote =Base+"opinions/isvote";
   public static String opinions_share =Base2+"opinions/";
   //serach
   public static String search =Base+"search";

   //profile follow and unfolllow
   public static String follow =Base+"profile/public/follow";
   public static String unfollow =Base+"profile/public/unfollow";
   public static String isfollow =Base+"profile/public/isfollow";

   //home apis
   public static String homenews_slider =Base+"home/news/slider";
   public static String homenews_posts =Base+"home/news/posts";
   public static String hometalent_slider =Base+"home/talents/slider";
   public static String hometalent_posts =Base+"home/talents/posts";
   public static String homeexplorer_slider =Base+"home/explorer/slider";
   public static String homeexplorer_posts =Base+"home/explorer/posts";

   //forgot password
   public static String forgot_password =Base+"forgot";

   // all explore list apis
   public static String explore_all=Base+"explore/all";
   public static String exploreall_editcom=Base+"explore/comment/edit/";
   public static String exploreall_fav=Base+"explore/fav";
   public static String exploreall_vote=Base+"explore/vote";
   public static String exploreall_delvote=Base+"explore/delvote";
   public static String exploreall_isvote=Base+"explore/isvote";
   public static String exploreall_comment=Base+"explore/comment";

   // all talent list apis
   public static String talent_all=Base+"talents/all";
   public static String talentall_editcom=Base+"talents/comment/edit/";
   public static String talentall_fav=Base+"talents/fav";
   public static String talentall_vote=Base+"talents/vote";
   public static String talentall_deletevote=Base+"talents/delvote";
   public static String talentall_isvote=Base+"talents/isvote";
   public static String talentall_comment=Base+"talents/comment";
   public static String talent_detail=Base+"talents/category/catid/posts/postid";
   public static String talent_share= Base2+"talents/";
   // Apis location set
   public static String location_get=Base+"locations/name/id";
   public static String location_set=Base+"locations/set";

   //Apis petitions
   public static String petition_all=Base+"petitions/all";
   public static String petition_closed=Base+"petitions/closed";
   public static String petition_detail=Base+"petitions/post/id";
   public static String petition_comment=Base+"petitions/comment";
   public static String petition_commentedit=Base+"petitions/comment/edit";
   public static String petition_sign=Base+"petitions/sign";
    public static String petition_Issign=Base+"petitions/is_sign";
   public static String petition_share=Base2+"petitions/";

    //Apis polls
    public static String Polls_list=Base+"polls/all";
    public static String Polls_ongoing=Base+"polls/ongoing";
    public static String Polls_upcoming=Base+"polls/upcoming";
    public static String Polls_closed=Base+"polls/closed";
    public static String Polls_detail=Base+"polls/post/id";
    public static String Polls_vote=Base+"polls/vote";
    public static String Polls_Isvote=Base+"polls/is_vote";
    public static String Polls_result=Base+"polls/id/result";

  //Apis Blogs
  public static String Blogs_list=Base+"blogs/all";
  public static String Blogs_detail=Base+"blogs/category/catid/posts/postid";
  public static String Blogs_comment=Base+"blogs/comment";
  public static String Blogs_fav=Base+"blogs/fav";



}
