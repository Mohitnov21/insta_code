package com.abacusdesk.instafeed.instafeed.fragments;

import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.annotation.RequiresApi;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.abacusdesk.instafeed.instafeed.NavDrawerActivity;
import com.abacusdesk.instafeed.instafeed.R;
import com.abacusdesk.instafeed.instafeed.model.NewsFeed;

import java.util.ArrayList;
import java.util.List;

public class HomeTab_fragment extends Fragment {

    private  ViewPager viewPager;
    private  TabLayout tabLayout;
    public static int positioncurrent;
    public ViewPagerAdapter adapter;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        final View view = inflater.inflate(R.layout.activity_tabnews, container, false);

        viewPager = (ViewPager) view.findViewById(R.id.viewPager);
        setupViewPager(viewPager);

        tabLayout = (TabLayout) view.findViewById(R.id.tabLayout);
        tabLayout.setupWithViewPager(viewPager);

        settabicon();
        //setting tab over viewpager
        //Implementing tab selected listener over tablayout
        viewPager.setOffscreenPageLimit(2);
        viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                positioncurrent=position;
                //Log.e("current fragment",""+positioncurrent);
                //JCVideoPlayer.releaseAllVideos();
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });
        return view;
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    //Setting View Pager
    private void setupViewPager(ViewPager viewPager) {
        adapter = new ViewPagerAdapter(getFragmentManager());
        adapter.addFrag(new NewsRss(),"News");
        adapter.addFrag(new HomeFrag_news(),"Ifeed");
        adapter.addFrag(new HomeFrag_talents(),"Talents");
        adapter.addFrag(new HomeFrag_explorer(),"Lifestyle");
        adapter.addFrag(new frag_opinionlist(),"Opinions");

        //adapter.addFrag(new frag_pollslist(),        "  Polls");
        //adapter.addFrag(new Frag_blogslist(),          "Blogs");
        //adapter.addFrag(new PetitionAll_fragment(),"Petitions");
        viewPager.setAdapter(adapter);
    }

   /* public Fragment findFragmentByPosition(int position) {
        FragmentPagerAdapter fragmentPagerAdapter = getfra();
        return getSupportFragmentManager().findFragmentByTag(
                "android:switcher:" + getViewPager().getId() + ":"
                        + fragmentPagerAdapter.getItemId(position));
    }*/

    private static String makeFragmentName(int viewId, long id) {
        return "android:switcher:" + viewId + ":" + id;
    }

    public void fragmentname(String pos){
        if (pos.equalsIgnoreCase("1")){
            Fragment activeFragment = adapter.getItem(1);
            Log.e("active frag",""+activeFragment);
            ((HomeFrag_news)activeFragment).refresh_Adapter();
        }else if (pos.equalsIgnoreCase("2")){
            Fragment activeFragment = adapter.getItem(2);
            Log.e("active frag",""+activeFragment);
            ((HomeFrag_talents)activeFragment).refresh_Adapter();
        }else if (pos.equalsIgnoreCase("3")){
            Fragment activeFragment = adapter.getItem(3);
            Log.e("active frag",""+activeFragment);
            ((HomeFrag_explorer)activeFragment).refresh_Adapter();
        }else if (pos.equalsIgnoreCase("4")){
            Fragment activeFragment = adapter.getItem(4);
            Log.e("active frag",""+activeFragment);
            ((frag_opinionlist)activeFragment).refresh_Adapter();
        }
    }

    private void settabicon(){
        try {
            for (int i = 0; i < tabLayout.getTabCount(); i++) {
                //  noinspection ConstantConditions
                TextView tv = (TextView) LayoutInflater.from(getContext()).inflate(R.layout.tab_text, null);
                if (Build.VERSION.SDK_INT > Build.VERSION_CODES.KITKAT) {
                    tv.setLetterSpacing((float) 0.2);
                }
                tabLayout.getTabAt(i).setCustomView(tv);
            }
        }catch(Exception e){

            }
        }

    class ViewPagerAdapter extends FragmentStatePagerAdapter {
        private final List<Fragment> mFragmentList = new ArrayList<>();//fragment arraylist
        private final List<String> mFragmentTitleList = new ArrayList<>();//title arraylist

        public ViewPagerAdapter(FragmentManager manager) {
            super(manager);
        }

        @Override
        public Fragment getItem(int position) {
            return mFragmentList.get(position);
        }

        @Override
        public int getCount() {

            return mFragmentList.size();
        }

        //adding fragments and title method
        public void addFrag(Fragment fragment, String title) {
            mFragmentList.add(fragment);
            mFragmentTitleList.add(title);
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return mFragmentTitleList.get(position);
        }
    }

}


