package com.abacusdesk.instafeed.instafeed.model;

import java.util.List;

/**
 * Created by abacusdesk on 2017-08-08.
 */

public class Opiniondetail_model {


    /**
     * status : 200
     * message : success
     * data : [{"id":"101","title":"demo text title","description":"ly<\/B>\\r\\n\\r\\nAs an unwritten rule, most temples will never make cash donations to those who are financially distressed or have been affected by a natural calamity or some major accident. Instead, they will spend huge sums of money on religious programs such as Yagnas and Poojas; give away money and alms to the saints and sadhus performing such Yagnas and spend more on creating the right religious ambiance by erecting mega pandals or hiring religious heads from other parts of the country and even from abroad. \\r\\n\\r\\nTrusts of some of the big temples of India do contribute to the welfare of the society through educational, medical and social service institutions, the activities of which are largely controlled by a body appointed by the temple. Providing free meal to all visitors to the temple every day is a tradition observed by many temples. However, on an average, temples spend only a little over 10 per cent of its cash component.\\r\\n\\r\\n<B>No Accountability<\/B>\\r\\n\\r\\nMisuse of funds within the trust itself is a common phenomenon, not only among in temples but across various religious organizations. In most temples, there are groups within groups that form their own cartels which push their own agenda and use the money according to their whims and fancies. As they are not answerable to any authority, they know that no questions will be asked. \\r\\nTo deal with the problem, a concerted movement is needed.  Like-minded individuals, people forums, social service groups ","location_id":"12","total_up_votes":"1","total_down_votes":null,"total_comments":"2","total_views":"1","total_flags":null,"slug":"IN00101","user_id":"3924","dt_added":"2017-08-08 11:13:16","dt_modified":"2017-08-08 13:22:54","status":"A","first_name":"jitu","last_name":"kumar","nickname":"jitu","avatar":"https://instafeed.org/storage/avatar/80x80-1502037939-bt.jpg","username":"jkg","location":"Kanpur"},{"comments":[{"id":"38","issue_id":"101","user_id":"3924","comment":"vow w,\r\n","comment_id":null,"dt_added":"2017-08-08 13:23:21","dt_modified":"2017-08-08 13:23:21","status":"A"},{"id":"39","issue_id":"101","user_id":"3924","comment":"rhrthrth","comment_id":null,"dt_added":"2017-08-08 13:23:29","dt_modified":"2017-08-08 13:23:29","status":"A"}]},{"images":[{"id":"76","image":"https://instafeed.org/storage/issue_pics/256x170-dfb12d3030e624a84286fbb4c0770b03.jpg","image_zoom":"https://instafeed.org/storage/issue_pics/748x420-dfb12d3030e624a84286fbb4c0770b03.jpg","image_original":"https://instafeed.org/storage/issue_pics/dfb12d3030e624a84286fbb4c0770b03.jpg","image_100x100":"https://instafeed.org/storage/issue_pics/100x100-dfb12d3030e624a84286fbb4c0770b03.jpg","image_256x170":"https://instafeed.org/storage/issue_pics/256x170-dfb12d3030e624a84286fbb4c0770b03.jpg","image_264x200":"https://instafeed.org/storage/issue_pics/264x200-dfb12d3030e624a84286fbb4c0770b03.jpg","image_360x290":"https://instafeed.org/storage/issue_pics/360x290-dfb12d3030e624a84286fbb4c0770b03.jpg"},{"id":"77","image":"https://instafeed.org/storage/issue_pics/256x170-3da5a60b95c35c142c760a7c1a06d90e.jpg","image_zoom":"https://instafeed.org/storage/issue_pics/748x420-3da5a60b95c35c142c760a7c1a06d90e.jpg","image_original":"https://instafeed.org/storage/issue_pics/3da5a60b95c35c142c760a7c1a06d90e.jpg","image_100x100":"https://instafeed.org/storage/issue_pics/100x100-3da5a60b95c35c142c760a7c1a06d90e.jpg","image_256x170":"https://instafeed.org/storage/issue_pics/256x170-3da5a60b95c35c142c760a7c1a06d90e.jpg","image_264x200":"https://instafeed.org/storage/issue_pics/264x200-3da5a60b95c35c142c760a7c1a06d90e.jpg","image_360x290":"https://instafeed.org/storage/issue_pics/360x290-3da5a60b95c35c142c760a7c1a06d90e.jpg"}]},{"videos":[{"id":"17","video":"https://instafeed.org/storage/issue_videos/6c2a90768a0a005b82543fcb4a42361c.mp4","video_thumb":"https://instafeed.org/storage/issue_videos/256x170-6c2a90768a0a005b82543fcb4a42361c.jpg","vimeo_video_id":null,"vimeo_response":null}]}]
     */

    private int status;
    private String message;
    private List<DataBean> data;

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public List<DataBean> getData() {
        return data;
    }

    public void setData(List<DataBean> data) {
        this.data = data;
    }

    public static class DataBean {
        /**
         * id : 101
         * title : demo text title
         * description : ly</B>\r\n\r\nAs an unwritten rule, most temples will never make cash donations to those who are financially distressed or have been affected by a natural calamity or some major accident. Instead, they will spend huge sums of money on religious programs such as Yagnas and Poojas; give away money and alms to the saints and sadhus performing such Yagnas and spend more on creating the right religious ambiance by erecting mega pandals or hiring religious heads from other parts of the country and even from abroad. \r\n\r\nTrusts of some of the big temples of India do contribute to the welfare of the society through educational, medical and social service institutions, the activities of which are largely controlled by a body appointed by the temple. Providing free meal to all visitors to the temple every day is a tradition observed by many temples. However, on an average, temples spend only a little over 10 per cent of its cash component.\r\n\r\n<B>No Accountability</B>\r\n\r\nMisuse of funds within the trust itself is a common phenomenon, not only among in temples but across various religious organizations. In most temples, there are groups within groups that form their own cartels which push their own agenda and use the money according to their whims and fancies. As they are not answerable to any authority, they know that no questions will be asked. \r\nTo deal with the problem, a concerted movement is needed.  Like-minded individuals, people forums, social service groups
         * location_id : 12
         * total_up_votes : 1
         * total_down_votes : null
         * total_comments : 2
         * total_views : 1
         * total_flags : null
         * slug : IN00101
         * user_id : 3924
         * dt_added : 2017-08-08 11:13:16
         * dt_modified : 2017-08-08 13:22:54
         * status : A
         * first_name : jitu
         * last_name : kumar
         * nickname : jitu
         * avatar : https://instafeed.org/storage/avatar/80x80-1502037939-bt.jpg
         * username : jkg
         * location : Kanpur
         * comments : [{"id":"38","issue_id":"101","user_id":"3924","comment":"vow w,\r\n","comment_id":null,"dt_added":"2017-08-08 13:23:21","dt_modified":"2017-08-08 13:23:21","status":"A"},{"id":"39","issue_id":"101","user_id":"3924","comment":"rhrthrth","comment_id":null,"dt_added":"2017-08-08 13:23:29","dt_modified":"2017-08-08 13:23:29","status":"A"}]
         * images : [{"id":"76","image":"https://instafeed.org/storage/issue_pics/256x170-dfb12d3030e624a84286fbb4c0770b03.jpg","image_zoom":"https://instafeed.org/storage/issue_pics/748x420-dfb12d3030e624a84286fbb4c0770b03.jpg","image_original":"https://instafeed.org/storage/issue_pics/dfb12d3030e624a84286fbb4c0770b03.jpg","image_100x100":"https://instafeed.org/storage/issue_pics/100x100-dfb12d3030e624a84286fbb4c0770b03.jpg","image_256x170":"https://instafeed.org/storage/issue_pics/256x170-dfb12d3030e624a84286fbb4c0770b03.jpg","image_264x200":"https://instafeed.org/storage/issue_pics/264x200-dfb12d3030e624a84286fbb4c0770b03.jpg","image_360x290":"https://instafeed.org/storage/issue_pics/360x290-dfb12d3030e624a84286fbb4c0770b03.jpg"},{"id":"77","image":"https://instafeed.org/storage/issue_pics/256x170-3da5a60b95c35c142c760a7c1a06d90e.jpg","image_zoom":"https://instafeed.org/storage/issue_pics/748x420-3da5a60b95c35c142c760a7c1a06d90e.jpg","image_original":"https://instafeed.org/storage/issue_pics/3da5a60b95c35c142c760a7c1a06d90e.jpg","image_100x100":"https://instafeed.org/storage/issue_pics/100x100-3da5a60b95c35c142c760a7c1a06d90e.jpg","image_256x170":"https://instafeed.org/storage/issue_pics/256x170-3da5a60b95c35c142c760a7c1a06d90e.jpg","image_264x200":"https://instafeed.org/storage/issue_pics/264x200-3da5a60b95c35c142c760a7c1a06d90e.jpg","image_360x290":"https://instafeed.org/storage/issue_pics/360x290-3da5a60b95c35c142c760a7c1a06d90e.jpg"}]
         * videos : [{"id":"17","video":"https://instafeed.org/storage/issue_videos/6c2a90768a0a005b82543fcb4a42361c.mp4","video_thumb":"https://instafeed.org/storage/issue_videos/256x170-6c2a90768a0a005b82543fcb4a42361c.jpg","vimeo_video_id":null,"vimeo_response":null}]
         */

        private String id;
        private String title;
        private String description;
        private String location_id;
        private String total_up_votes;
        private String total_down_votes;
        private String total_comments;
        private String total_views;
        private Object total_flags;
        private String slug;
        private String user_id;
        private String dt_added;
        private String dt_modified;
        private String status;
        private String first_name;
        private String last_name;
        private String nickname;
        private String avatar;
        private String username;
        private String location;
        private List<CommentsBean> comments;
        private List<ImagesBean> images;
        private List<VideosBean> videos;

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getTitle() {
            return title;
        }

        public void setTitle(String title) {
            this.title = title;
        }

        public String getDescription() {
            return description;
        }

        public void setDescription(String description) {
            this.description = description;
        }

        public String getLocation_id() {
            return location_id;
        }

        public void setLocation_id(String location_id) {
            this.location_id = location_id;
        }

        public String getTotal_up_votes() {
            return total_up_votes;
        }

        public void setTotal_up_votes(String total_up_votes) {
            this.total_up_votes = total_up_votes;
        }

        public String getTotal_down_votes() {
            return total_down_votes;
        }

        public void setTotal_down_votes(String total_down_votes) {
            this.total_down_votes = total_down_votes;
        }

        public String getTotal_comments() {
            return total_comments;
        }

        public void setTotal_comments(String total_comments) {
            this.total_comments = total_comments;
        }

        public String getTotal_views() {
            return total_views;
        }

        public void setTotal_views(String total_views) {
            this.total_views = total_views;
        }

        public Object getTotal_flags() {
            return total_flags;
        }

        public void setTotal_flags(Object total_flags) {
            this.total_flags = total_flags;
        }

        public String getSlug() {
            return slug;
        }

        public void setSlug(String slug) {
            this.slug = slug;
        }

        public String getUser_id() {
            return user_id;
        }

        public void setUser_id(String user_id) {
            this.user_id = user_id;
        }

        public String getDt_added() {
            return dt_added;
        }

        public void setDt_added(String dt_added) {
            this.dt_added = dt_added;
        }

        public String getDt_modified() {
            return dt_modified;
        }

        public void setDt_modified(String dt_modified) {
            this.dt_modified = dt_modified;
        }

        public String getStatus() {
            return status;
        }

        public void setStatus(String status) {
            this.status = status;
        }

        public String getFirst_name() {
            return first_name;
        }

        public void setFirst_name(String first_name) {
            this.first_name = first_name;
        }

        public String getLast_name() {
            return last_name;
        }

        public void setLast_name(String last_name) {
            this.last_name = last_name;
        }

        public String getNickname() {
            return nickname;
        }

        public void setNickname(String nickname) {
            this.nickname = nickname;
        }

        public String getAvatar() {
            return avatar;
        }

        public void setAvatar(String avatar) {
            this.avatar = avatar;
        }

        public String getUsername() {
            return username;
        }

        public void setUsername(String username) {
            this.username = username;
        }

        public String getLocation() {
            return location;
        }

        public void setLocation(String location) {
            this.location = location;
        }

        public List<CommentsBean> getComments() {
            return comments;
        }

        public void setComments(List<CommentsBean> comments) {
            this.comments = comments;
        }

        public List<ImagesBean> getImages() {
            return images;
        }

        public void setImages(List<ImagesBean> images) {
            this.images = images;
        }

        public List<VideosBean> getVideos() {
            return videos;
        }

        public void setVideos(List<VideosBean> videos) {
            this.videos = videos;
        }

        public static class CommentsBean {
            /**
             * id : 38
             * issue_id : 101
             * user_id : 3924
             * comment : vow w,

             * comment_id : null
             * dt_added : 2017-08-08 13:23:21
             * dt_modified : 2017-08-08 13:23:21
             * status : A
             */

            private String id;
            private String issue_id;
            private String user_id;
            private String comment;
            private Object comment_id;
            private String dt_added;
            private String dt_modified;
            private String status;

            public String getAvatar() {
                return avatar;
            }

            public void setAvatar(String avatar) {
                this.avatar = avatar;
            }

            public String getUsername() {
                return username;
            }

            public void setUsername(String username) {
                this.username = username;
            }

            private String avatar;
            private String username;

            public String getId() {
                return id;
            }

            public void setId(String id) {
                this.id = id;
            }

            public String getIssue_id() {
                return issue_id;
            }

            public void setIssue_id(String issue_id) {
                this.issue_id = issue_id;
            }

            public String getUser_id() {
                return user_id;
            }

            public void setUser_id(String user_id) {
                this.user_id = user_id;
            }

            public String getComment() {
                return comment;
            }

            public void setComment(String comment) {
                this.comment = comment;
            }

            public Object getComment_id() {
                return comment_id;
            }

            public void setComment_id(Object comment_id) {
                this.comment_id = comment_id;
            }

            public String getDt_added() {
                return dt_added;
            }

            public void setDt_added(String dt_added) {
                this.dt_added = dt_added;
            }

            public String getDt_modified() {
                return dt_modified;
            }

            public void setDt_modified(String dt_modified) {
                this.dt_modified = dt_modified;
            }

            public String getStatus() {
                return status;
            }

            public void setStatus(String status) {
                this.status = status;
            }
        }

        public static class ImagesBean {
            /**
             * id : 76
             * image : https://instafeed.org/storage/issue_pics/256x170-dfb12d3030e624a84286fbb4c0770b03.jpg
             * image_zoom : https://instafeed.org/storage/issue_pics/748x420-dfb12d3030e624a84286fbb4c0770b03.jpg
             * image_original : https://instafeed.org/storage/issue_pics/dfb12d3030e624a84286fbb4c0770b03.jpg
             * image_100x100 : https://instafeed.org/storage/issue_pics/100x100-dfb12d3030e624a84286fbb4c0770b03.jpg
             * image_256x170 : https://instafeed.org/storage/issue_pics/256x170-dfb12d3030e624a84286fbb4c0770b03.jpg
             * image_264x200 : https://instafeed.org/storage/issue_pics/264x200-dfb12d3030e624a84286fbb4c0770b03.jpg
             * image_360x290 : https://instafeed.org/storage/issue_pics/360x290-dfb12d3030e624a84286fbb4c0770b03.jpg
             */

            private String id;
            private String image;
            private String image_zoom;
            private String image_original;
            private String image_100x100;
            private String image_256x170;
            private String image_264x200;
            private String image_360x290;

            public String getId() {
                return id;
            }

            public void setId(String id) {
                this.id = id;
            }

            public String getImage() {
                return image;
            }

            public void setImage(String image) {
                this.image = image;
            }

            public String getImage_zoom() {
                return image_zoom;
            }

            public void setImage_zoom(String image_zoom) {
                this.image_zoom = image_zoom;
            }

            public String getImage_original() {
                return image_original;
            }

            public void setImage_original(String image_original) {
                this.image_original = image_original;
            }

            public String getImage_100x100() {
                return image_100x100;
            }

            public void setImage_100x100(String image_100x100) {
                this.image_100x100 = image_100x100;
            }

            public String getImage_256x170() {
                return image_256x170;
            }

            public void setImage_256x170(String image_256x170) {
                this.image_256x170 = image_256x170;
            }

            public String getImage_264x200() {
                return image_264x200;
            }

            public void setImage_264x200(String image_264x200) {
                this.image_264x200 = image_264x200;
            }

            public String getImage_360x290() {
                return image_360x290;
            }

            public void setImage_360x290(String image_360x290) {
                this.image_360x290 = image_360x290;
            }
        }

        public static class VideosBean {
            /**
             * id : 17
             * video : https://instafeed.org/storage/issue_videos/6c2a90768a0a005b82543fcb4a42361c.mp4
             * video_thumb : https://instafeed.org/storage/issue_videos/256x170-6c2a90768a0a005b82543fcb4a42361c.jpg
             * vimeo_video_id : null
             * vimeo_response : null
             */

            private String id;
            private String video;
            private String video_thumb;
            private Object vimeo_video_id;
            private Object vimeo_response;

            public String getId() {
                return id;
            }

            public void setId(String id) {
                this.id = id;
            }

            public String getVideo() {
                return video;
            }

            public void setVideo(String video) {
                this.video = video;
            }

            public String getVideo_thumb() {
                return video_thumb;
            }

            public void setVideo_thumb(String video_thumb) {
                this.video_thumb = video_thumb;
            }

            public Object getVimeo_video_id() {
                return vimeo_video_id;
            }

            public void setVimeo_video_id(Object vimeo_video_id) {
                this.vimeo_video_id = vimeo_video_id;
            }

            public Object getVimeo_response() {
                return vimeo_response;
            }

            public void setVimeo_response(Object vimeo_response) {
                this.vimeo_response = vimeo_response;
            }
        }
    }
}
