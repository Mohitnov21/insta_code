package com.abacusdesk.instafeed.instafeed.model;

import java.util.List;

/**
 * Created by abacusdesk on 2017-08-10.
 */

public class Polls_listmodel {


    /**
     * status : 200
     * message : success
     * data : [{"id":"46","title":"You\u2019ve gone incognito","description":"You\u2019ve gone incognito\r\n\r\nPages that you view in incognito tabs won\u2019t stick around in your browser\u2019s history, cookie store or search history after you\u2019ve closed all of your incognito tabs. Any files you download or bookmarks you create will be kept.\r\n\r\nHowever, you aren\u2019t invisible. Going incognito doesn\u2019t hide your browsing from your employer, your Internet service provider or the websites that you visit.","location_id":"6","is_loc_poll":null,"user_id":"2","slug":"youve-gone-incognito-1291348372","dt_start":"2017-07-18 13:57:21","dt_end":"2017-07-24 13:57:21","total_views":"0","dt_added":"2017-07-24 13:58:13","dt_modified":"2017-07-24 15:01:07","status":"X"},{"id":"45","title":"binsar","description":"You\u2019ve gone incognito\r\n\r\nPages that you view in incognito tabs won\u2019t stick around in your browser\u2019s history, cookie store or search history after you\u2019ve closed all of your incognito tabs. Any files you download or bookmarks you create will be kept.\r\n\r\nHowever, you aren\u2019t invisible. Going incognito doesn\u2019t hide your browsing from your employer, your Internet service provider or the websites that you visit.","location_id":"1","is_loc_poll":null,"user_id":"2","slug":"binsar-1821674093","dt_start":"2017-07-17 13:54:49","dt_end":"2017-07-25 13:54:49","total_views":"0","dt_added":"2017-07-24 13:57:05","dt_modified":"2017-07-24 13:57:05","status":"A"},{"id":"44","title":"Metro rides in Delhi get costlier today, starting at Rs 10 & going up till Rs 50. Do you think this increment in rates will affect the number of passengers traveling everyday?","description":"Over 20 lakh people travel via metro in Delhi, every day.","location_id":"141","is_loc_poll":null,"user_id":"2","slug":"metro-rides-in-delhi-get-costlier-today-starting-at-rs-10-going-up-till-rs-50-do-you-think-this-inc-1646880484","dt_start":"2017-05-10 10:00:56","dt_end":"2017-05-17 10:00:56","total_views":"0","dt_added":"2017-05-10 10:07:05","dt_modified":"2017-05-10 10:14:47","status":"A"},{"id":"43","title":"5tydr5uf67i","description":"puja                                                                                             vibhuti","location_id":"384","is_loc_poll":null,"user_id":"2","slug":"5tydr5uf67i-1746744637","dt_start":"2017-05-09 14:25:55","dt_end":"2017-05-17 14:25:55","total_views":"0","dt_added":"2017-05-09 14:27:31","dt_modified":"2017-05-09 14:34:09","status":"X"},{"id":"42","title":"OnePlus 3T or Xiaomi Redmi Note 4. Which is a better smartphone?","description":"","location_id":"187","is_loc_poll":null,"user_id":"2","slug":"oneplus-3t-or-xiaomi-redmi-note-4-which-is-better-smartphone-with-a-camera-1836837211","dt_start":"2017-05-09 14:03:08","dt_end":"2017-05-16 14:03:08","total_views":"0","dt_added":"2017-05-09 14:11:16","dt_modified":"2017-05-09 14:33:47","status":"A"},{"id":"41","title":"Testing pole","description":"desc!!!!!!!!!!!!!!!","location_id":"187","is_loc_poll":null,"user_id":"2","slug":"testing-pole-1677709359","dt_start":"2017-05-03 12:25:11","dt_end":"2017-05-09 12:25:11","total_views":"0","dt_added":"2017-05-04 12:27:14","dt_modified":"2017-05-04 13:27:00","status":"C"},{"id":"40","title":"Should Triple Talaaq be banned ?","description":"Triple Talaq is a practice misinterpreted by many Muslims. It creates inequality between man and women. In Islamic religion, where most of the muslim woman are dependent on their husband, such practice of triple talaaq makes the woman suffer. Such practices are immortal towards women. ","location_id":"141","is_loc_poll":null,"user_id":"2","slug":"should-triple-talaaq-be-banned-1194769998","dt_start":"2017-05-03 14:20:17","dt_end":"2017-05-08 14:20:17","total_views":"0","dt_added":"2017-05-03 14:26:00","dt_modified":"2017-05-04 14:41:28","status":"A"},{"id":"39","title":"Should betting on sports be legalized in India?","description":"","location_id":"141","is_loc_poll":null,"user_id":"2","slug":"should-betting-on-sports-be-legalized-in-india-1318001956","dt_start":"2017-05-02 14:33:51","dt_end":"2017-05-16 14:33:51","total_views":"0","dt_added":"2017-05-02 14:37:05","dt_modified":"2017-05-02 14:37:05","status":"A"}]
     */

    private int status;
    private String message;
    private List<DataBean> data;

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public List<DataBean> getData() {
        return data;
    }

    public void setData(List<DataBean> data) {
        this.data = data;
    }

    public static class DataBean {
        /**
         * id : 46
         * title : You’ve gone incognito
         * description : You’ve gone incognito

         Pages that you view in incognito tabs won’t stick around in your browser’s history, cookie store or search history after you’ve closed all of your incognito tabs. Any files you download or bookmarks you create will be kept.

         However, you aren’t invisible. Going incognito doesn’t hide your browsing from your employer, your Internet service provider or the websites that you visit.
         * location_id : 6
         * is_loc_poll : null
         * user_id : 2
         * slug : youve-gone-incognito-1291348372
         * dt_start : 2017-07-18 13:57:21
         * dt_end : 2017-07-24 13:57:21
         * total_views : 0
         * dt_added : 2017-07-24 13:58:13
         * dt_modified : 2017-07-24 15:01:07
         * status : X
         */

        private String id;
        private String title;
        private String description;
        private String location_id;
        private Object is_loc_poll;
        private String user_id;
        private String slug;
        private String dt_start;
        private String dt_end;
        private String total_views;
        private String dt_added;
        private String dt_modified;
        private String status;

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getTitle() {
            return title;
        }

        public void setTitle(String title) {
            this.title = title;
        }

        public String getDescription() {
            return description;
        }

        public void setDescription(String description) {
            this.description = description;
        }

        public String getLocation_id() {
            return location_id;
        }

        public void setLocation_id(String location_id) {
            this.location_id = location_id;
        }

        public Object getIs_loc_poll() {
            return is_loc_poll;
        }

        public void setIs_loc_poll(Object is_loc_poll) {
            this.is_loc_poll = is_loc_poll;
        }

        public String getUser_id() {
            return user_id;
        }

        public void setUser_id(String user_id) {
            this.user_id = user_id;
        }

        public String getSlug() {
            return slug;
        }

        public void setSlug(String slug) {
            this.slug = slug;
        }

        public String getDt_start() {
            return dt_start;
        }

        public void setDt_start(String dt_start) {
            this.dt_start = dt_start;
        }

        public String getDt_end() {
            return dt_end;
        }

        public void setDt_end(String dt_end) {
            this.dt_end = dt_end;
        }

        public String getTotal_views() {
            return total_views;
        }

        public void setTotal_views(String total_views) {
            this.total_views = total_views;
        }

        public String getDt_added() {
            return dt_added;
        }

        public void setDt_added(String dt_added) {
            this.dt_added = dt_added;
        }

        public String getDt_modified() {
            return dt_modified;
        }

        public void setDt_modified(String dt_modified) {
            this.dt_modified = dt_modified;
        }

        public String getStatus() {
            return status;
        }

        public void setStatus(String status) {
            this.status = status;
        }
    }
}
