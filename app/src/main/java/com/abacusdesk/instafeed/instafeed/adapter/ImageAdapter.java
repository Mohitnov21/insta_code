package com.abacusdesk.instafeed.instafeed.adapter;

import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;
import android.media.ThumbnailUtils;
import android.provider.MediaStore;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;

import com.abacusdesk.instafeed.instafeed.R;
import com.abacusdesk.instafeed.instafeed.Util.CommonFunctions;
import com.abacusdesk.instafeed.instafeed.model.GridModel;

import java.io.File;
import java.util.ArrayList;

import static com.abacusdesk.instafeed.instafeed.Util.ScreenConstants.MEDIA_TYPE_AUDIO;
import static com.abacusdesk.instafeed.instafeed.Util.ScreenConstants.MEDIA_TYPE_IMAGE;
import static com.abacusdesk.instafeed.instafeed.Util.ScreenConstants.MEDIA_TYPE_VIDEO;

/**
 * Created by Administrator on 5/28/2017.
 */

public class ImageAdapter extends BaseAdapter {
    Activity context;
    ArrayList<GridModel> img_list;
    ArrayList<String> audioPathList;


    public ImageAdapter(Activity context, ArrayList<GridModel>img_list) {
        this.context=context;
        this.img_list=img_list;
    }

    @Override
    public int getCount() {
        return (null != img_list ? img_list.size() : 0);
    }

    @Override
    public Object getItem(int i) {
        return null;
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }

    @Override
    public View getView(int position, View view, ViewGroup viewGroup) {
        // TODO Auto-generated method stub
        View grid;
        LayoutInflater inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);

            grid = new View(context);
            grid = inflater.inflate(R.layout.image_gallery_row, null);
            audioPathList=new ArrayList<>();
            ImageView imageView = (ImageView)grid.findViewById(R.id.image_gallery_image_view);
            GridModel gridModel=img_list.get(position);
           if(gridModel.getFileType().equals(MEDIA_TYPE_IMAGE)) {
               Log.d("image Name",MEDIA_TYPE_IMAGE+",,,,,"+gridModel.getFileType());

               File file = new File(gridModel.getFilePath());

             /*  try {
                Glide.with(context)
                        .load(new File(gridModel.getFilePath()))
                        .into(imageView);
            }catch (Exception e)
            {e.printStackTrace();}*/

               Bitmap bitmap = CommonFunctions.extractThumbnail(file, 250, 250);
               imageView.setImageBitmap(bitmap);
               Log.d("video Name",MEDIA_TYPE_VIDEO+",,,,,"+gridModel.getFileType());
           }else if(gridModel.getFileType().equals(MEDIA_TYPE_VIDEO)){

               Bitmap bMap = ThumbnailUtils.createVideoThumbnail(gridModel.getFilePath(), MediaStore.Video.Thumbnails.MICRO_KIND);

               imageView.setImageBitmap(bMap);
           }else  if(gridModel.getFileType().equals(MEDIA_TYPE_AUDIO)){
               Log.d("audio Name",MEDIA_TYPE_AUDIO+",,,,,"+gridModel.getFileType());
               audioPathList.add(gridModel.getFilePath());
               imageView.setImageResource(R.drawable.music_player_plus);
           }

        return grid;
    }
}
