package com.abacusdesk.instafeed.instafeed;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TextInputEditText;
import android.support.design.widget.TextInputLayout;
import android.support.v4.content.LocalBroadcastManager;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.abacusdesk.instafeed.instafeed.Util.Alerts;
import com.abacusdesk.instafeed.instafeed.Util.Apis;
import com.abacusdesk.instafeed.instafeed.Util.CommonFunctions;
import com.abacusdesk.instafeed.instafeed.Util.SaveSharedPreference;
import com.abacusdesk.instafeed.instafeed.custom_dialog.Dialogs;
import com.abacusdesk.instafeed.instafeed.fragments.Forgot_passwordfrag;
import com.abacusdesk.instafeed.instafeed.fragments.HomeTab_fragment;
import com.abacusdesk.instafeed.instafeed.webservice.VolleySingleton;
import com.android.volley.AuthFailureError;
import com.android.volley.NoConnectionError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by abacusdesk on 2017-07-28.
 */

public class Otp_verification extends Activity implements View.OnClickListener {

    EditText ed_otp;
    Button btnconfirm;
    String  otp;
    TextView txt_resend;
    BroadcastReceiver receiver;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.otp_verify);
        init();
    }


    void init() {
        btnconfirm = (Button) findViewById(R.id.btn_confirm);
        txt_resend=(TextView)findViewById(R.id.txt_resend);
        ed_otp = (EditText) findViewById(R.id.ed1);

        btnconfirm.setOnClickListener(this);
        txt_resend.setOnClickListener(this);
        receiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                if (intent.getAction().equalsIgnoreCase("otp")) {
                    final String message = intent.getStringExtra("message");
                    Log.e("message",""+message);
                    ed_otp.setText(message);
                    //Do whatever you want with the code here
                }
            }
        };
    }

    public void OtpVerify() {
        Dialogs.showProDialog(Otp_verification.this, "Loading");
        StringRequest stringRequest = new StringRequest(Request.Method.POST, Apis.verifyOtp,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {

                        Log.e("log", "" + response);
                        try {
                            responseOtp(response);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        Dialogs.disDialog();
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Dialogs.disDialog();
                        DialogInterface.OnClickListener onClickTryAgain = new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                Dialogs.disDialog();
                                OtpVerify();
                            }
                        };
                        if (error instanceof TimeoutError) {
                            Dialogs.disDialog();
                            Alerts.timeoutErrorAlert(Otp_verification.this, onClickTryAgain);
                        } else if (error instanceof NoConnectionError) {
                            Dialogs.disDialog();
                            Alerts.internetConnectionErrorAlert(Otp_verification.this, onClickTryAgain);
                          }
                        }
                })

        {

            @Override
            final protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> map = new HashMap<String, String>();
                map.put("mobile", Mobile_Signup.mobileno);
                map.put("otp",otp);
                Log.e("mapvalue", "" + map);
                return map;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Content-Type", "application/x-www-form-urlencoded");
                return params;
            }
        };

        VolleySingleton.getInstance(Otp_verification.this).addToRequestQueue(stringRequest);
    }

    // {"status":200,"message":"success","data":{"mobile":"7210747102","otp":"122975","is_verified":"Y",
    //  "user_id":4003,"token":"$1$5dGQ4KyR$rmET8x\/F\/hOuyB3qO9RlX0","username":"15013115821644658651"}}

    private void responseOtp(String response) throws JSONException {
        JSONObject jsonObject = new JSONObject(response);
        if (jsonObject.getString("status").equals("200") &&jsonObject.getString("message").equalsIgnoreCase("success")){
            SaveSharedPreference.setUserID(Otp_verification.this,jsonObject.getJSONObject("data").getString("user_id"));
            SaveSharedPreference.setPrefToken(Otp_verification.this,jsonObject.getJSONObject("data").getString("token"));
            Toast.makeText(Otp_verification.this,"You have been Successfully registered",Toast.LENGTH_SHORT).show();
            startActivity(new Intent(Otp_verification.this, NavDrawerActivity.class));
            finish();
        }else {
            Toast.makeText(Otp_verification.this,jsonObject.getString("error"),Toast.LENGTH_SHORT).show();
        }
        /* if () {
        } else if (jsonObject.getString("statusCode").equals("0")) {
           // Dialogs.showDialog(Otp_enter.this, jsonObject.getString("msg"));
        } else {
           // Dialogs.showDialog(Otp_enter.this, "Server Failed");
        }*/
    }


    @Override
    public void onResume() {
        LocalBroadcastManager.getInstance(this).registerReceiver(receiver, new IntentFilter("otp"));
        super.onResume();
    }

    @Override
    public void onPause() {
        super.onPause();
        LocalBroadcastManager.getInstance(this).unregisterReceiver(receiver);
    }
    @Override
    public void onClick(View view) {
        if (view == btnconfirm) {
             otp=ed_otp.getText().toString();
            if (otp.equals("")){
                ed_otp.setError("please enter your Otp");
            }else if (otp.equals(Mobile_Signup.otp_get))
                OtpVerify();
            else
                Dialogs.showCenterToast(getApplicationContext(), "Incorrect OTP");
        } else if (view == txt_resend) {
            Mobile_Signup.registerMobile(Otp_verification.this,"true");
        }
    }


}