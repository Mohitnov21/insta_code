package com.abacusdesk.instafeed.instafeed.adapter;

import android.app.Dialog;
import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.abacusdesk.instafeed.instafeed.R;
import com.abacusdesk.instafeed.instafeed.Util.SaveSharedPreference;
import com.abacusdesk.instafeed.instafeed.fragments.Blogdetail_fragment;
import com.abacusdesk.instafeed.instafeed.fragments.Explore_detailfragment;
import com.abacusdesk.instafeed.instafeed.fragments.Opiniondetail_fragment;
import com.abacusdesk.instafeed.instafeed.fragments.Petitiondetail_fragment;
import com.abacusdesk.instafeed.instafeed.fragments.Talentdetail_fragment;
import com.bumptech.glide.Glide;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by abacusdesk on 2017-08-09.
 */

public class RecyclerCommentAdapter extends RecyclerView.Adapter<RecyclerCommentAdapter.MyViewHolder> {

    Context context;
    ArrayList<HashMap<String,String>> arrayList;
    public static String comment="",comment_id;
    Dialog dialog;
    String type;

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView txtname,txtcomment;
        public ImageView imgprofile, imgedit;
        //public LinearLayout lnrow;

        public MyViewHolder(View view) {
            super(view);
             this.imgprofile=(ImageView)itemView.findViewById(R.id.img_profilepic);
            this.imgedit=(ImageView)itemView.findViewById(R.id.img_edit);
            this.txtname=(TextView)itemView.findViewById(R.id.txt_name);
            this.txtcomment=(TextView)itemView.findViewById(R.id.txt_comment);
        }
    }


    public RecyclerCommentAdapter(Context context,ArrayList<HashMap<String,String>> arrayList,String type) {
        this.context=context;
        this.arrayList=arrayList;
        this.type=type;
    }

   /* public void edit_dialog() {
        dialog = new Dialog(context);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(true);
        dialog.setContentView(R.layout.dialog_comment);
        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        lp.copyFrom(dialog.getWindow().getAttributes());
        lp.width = WindowManager.LayoutParams.MATCH_PARENT;
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
        dialog.getWindow().setAttributes(lp);
        final EditText edcomment=(EditText)dialog.findViewById(R.id.ed_comment);
        final Button btn_edit=(Button)dialog.findViewById(R.id.btn_edit);
        edcomment.setText(comment);
        btn_edit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                comment=edcomment.getText().toString();
                if (comment!=""){
                    if (type.equalsIgnoreCase("explore")){
                        Explore_detailfragment.editComment(context,comment,comment_id);
                        dialog.dismiss();
                    } else if (type.equalsIgnoreCase("opinions")){
                        Opiniondetail_fragment.editComment(context,comment,comment_id);
                        dialog.dismiss();
                    }  else if (type.equalsIgnoreCase("petitions")){
                        Petitiondetail_fragment.editComment(context,comment,comment_id);
                        dialog.dismiss();
                    } else if (type.equalsIgnoreCase("blogs")){
                        Blogdetail_fragment.editComment(context,comment,comment_id);
                        dialog.dismiss();
                    }else {
                        //Talentdetail_fragment.editComment(context,comment,comment_id);
                        dialog.dismiss();
                    }
                }
            }
        });
        dialog.show();
    }*/
    @Override
    public RecyclerCommentAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_rowlist, parent, false);
        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(RecyclerCommentAdapter.MyViewHolder holder, final int position) {

        String myFormat = "yyyy-MM-dd HH:mm:ss";
        DateFormat sdformat = new SimpleDateFormat(myFormat);
        DateFormat targetFormat = new SimpleDateFormat("MMMM dd, yyyy");

        holder.txtcomment.setText(arrayList.get(position).get("comment"));
        holder.txtname.setText(arrayList.get(position).get("username"));
        if (arrayList.get(position).get("avatar")!=null && !arrayList.get(position).get("avatar").isEmpty()){
            Glide.with(context).load(arrayList.get(position).get("avatar")).error(R.drawable.user).into(holder.imgprofile);
        }else {
            holder.imgprofile.setImageResource(R.drawable.user);
        }
        /*try{
            if (!arrayList.get(position).get("userid").equals(null)){
                if (arrayList.get(position).get("userid").equals(SaveSharedPreference.getUserID(context))){
                    holder.imgedit.setVisibility(View.INVISIBLE);
                }else {
                    holder.imgedit.setVisibility(View.INVISIBLE);
                }
            }
        }catch (Exception e){

        }*/


        holder.imgedit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                comment=arrayList.get(position).get("comment");
                comment_id=arrayList.get(position).get("commentid");
                //edit_dialog();
            }
        });
    }


    @Override
    public int getItemCount() {
        return arrayList.size();
    }
}
