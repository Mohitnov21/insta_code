package com.abacusdesk.instafeed.instafeed.fragments;

import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.annotation.RequiresApi;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.abacusdesk.instafeed.instafeed.NavDrawerActivity;
import com.abacusdesk.instafeed.instafeed.R;
import com.abacusdesk.instafeed.instafeed.Util.Apis;
import com.abacusdesk.instafeed.instafeed.Util.AppSingleton;
import com.abacusdesk.instafeed.instafeed.Util.CommonUtils;
import com.abacusdesk.instafeed.instafeed.adapter.ViewPagerAdapter;
import com.abacusdesk.instafeed.instafeed.custom_dialog.Dialogs;
import com.abacusdesk.instafeed.instafeed.model.NewsModel;
import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.google.gson.Gson;
import com.squareup.picasso.Picasso;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by abacusdesk on 2017-12-14.
 */

public class FeedDetail_fragment  extends Fragment {

    View view;
    TextView txtdescr, txtshortdescr, txtdate, txtplace, txttitle;
    TextView txtlike, txtdislike, txtcomment;
    ImageView img_back, img_forward;
    ImageView imgshare, imgonview;
    private String catid = "", postid = "", temp;

    ViewPagerAdapter viewPagerAdapter;
    ViewPager imgviewpager;
    Boolean flag = false;

    LinearLayout lncomment, lnlocation, lnlike, lndislike, ln_userdetail;
    TextView txt_ownername;
    ImageView img_like, imgdislike;
    String temp_related = "";

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.feeddetail_layout, container, false);
        init();
        getActivity().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_PAN | WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);
        return view;
    }

    private void init() {
      /*  arrayviewcontainer = new ArrayList<>();

        if (!arrayviewcontainer.isEmpty()) {
            arrayviewcontainer.clear();
        }*/

        Bundle bundle = this.getArguments();
        if (bundle != null) {
            postid = bundle.getString("id");
        }

        ln_userdetail = (LinearLayout) view.findViewById(R.id.ln_publicprofile);
        txt_ownername = (TextView) view.findViewById(R.id.txt_owner);
        txtdate = (TextView) view.findViewById(R.id.txt_date);

        imgviewpager = (ViewPager) view.findViewById(R.id.img_viewpager);
        imgshare = (ImageView) view.findViewById(R.id.img_share);

        imgonview = (ImageView) view.findViewById(R.id.img_view);
        imgonview.setVisibility(View.VISIBLE);
        if (NavDrawerActivity.toolbar != null) {
            NavDrawerActivity.toolbar.setLogo(null);
            NavDrawerActivity.toolbar.setTitle("Global News");
        }
        img_back = (ImageView) view.findViewById(R.id.img_left);
        img_forward = (ImageView) view.findViewById(R.id.img_right);

        lnlocation = (LinearLayout) view.findViewById(R.id.ln_location);
        txtlike = (TextView) view.findViewById(R.id.txt_like);
        txtdislike = (TextView) view.findViewById(R.id.txt_dislike);

        txtcomment = (TextView) view.findViewById(R.id.txt_comment);
        //imgshare.setOnClickListener(this);
        //txtlike.setOnClickListener(this);
        //txtdislike.setOnClickListener(this);
        //lncomment.setOnClickListener(this);
        //txtcomment.setOnClickListener(this);

        //txtname=(TextView)view.findViewById(R.i+d.txt_name);
        txtshortdescr = (TextView) view.findViewById(R.id.txt_descrshort);
        txtdescr = (TextView) view.findViewById(R.id.txt_shortdescr);

        txtplace = (TextView) view.findViewById(R.id.txt_place);
        txttitle = (TextView) view.findViewById(R.id.txt_title);

        temp = Apis.newsrss_detail.replace("id", postid);
        getFeeddetail();
        view.setFocusableInTouchMode(true);
        view.requestFocus();

    }

    public void getFeeddetail() {
         Dialogs.showProDialog(getContext(), "Loading");
        StringRequest stringRequest = new StringRequest(Request.Method.GET,temp,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        // Log.e("log", "" + response);
                        getResponse(response);
                        Dialogs.disDialog();
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                    }
                })
        {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Content-Type", "application/x-www-form-urlencoded");
                return params;
            }
        };

        AppSingleton.getInstance(getContext()).addToRequestQueue(stringRequest);
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(
                6000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

    }

    /*
    {
        "status": 200,
            "message": "success",
                "data": {
                "id": "98",
                "title": "PRETTYMUCH And French Montana Throw A Chill House Party In 'No More' Video",
                "description": "It's a stylish clip shot from different perspectives",
                "image_url": "https://imagesmtv-a.akamaihd.net/uri/mgid:ao:image:mtv.com:259890?quality=0.8&format=jpg&width=1440&height=810&.jpg",
                "dt_published": "2017-12-12T16:48:03Z",
                "news_source": "http://www.mtv.com/news/3052598/prettymuch-no-more-video-french-montana/",
                "apiKey": "mtv-news"
                       }
    }*/

    private void getResponse(String response ) {
        try {
            JSONObject jsonObject=new JSONObject(response);
            if (jsonObject.getString("status").equals("200") && jsonObject.getString("message").equalsIgnoreCase("success")){
               txtdescr.setText(jsonObject.getJSONObject("data").getString("description"));
               txttitle.setText(jsonObject.getJSONObject("data").getString("title"));
               if (!jsonObject.getJSONObject("data").getString("image_url").isEmpty()
                       && jsonObject.getJSONObject("data").getString("image_url")!=null){
                   Picasso.with(getContext()).load(jsonObject.getJSONObject("data").getString("image_url")).fit()
                           .error(R.drawable.newsdefault).into((imgonview));
               }else {
                   imgonview.setImageResource(R.drawable.newsdefault);
               }
               if (!jsonObject.getJSONObject("data").getString("dt_published").isEmpty())
              txtdate.setText(CommonUtils.dateformat(jsonObject.getJSONObject("data").getString("dt_published")));
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }catch (Exception e){

        }
    }

  }

