package com.abacusdesk.instafeed.instafeed.fragments;

import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.abacusdesk.instafeed.instafeed.NavDrawerActivity;
import com.abacusdesk.instafeed.instafeed.R;
import com.abacusdesk.instafeed.instafeed.Util.Alerts;
import com.abacusdesk.instafeed.instafeed.Util.Apis;
import com.abacusdesk.instafeed.instafeed.Util.CommonFunctions;
import com.abacusdesk.instafeed.instafeed.Util.SaveSharedPreference;
import com.abacusdesk.instafeed.instafeed.adapter.Follow_Adapter;
import com.abacusdesk.instafeed.instafeed.adapter.Opinion_Adapter;
import com.abacusdesk.instafeed.instafeed.custom_dialog.Dialogs;
import com.abacusdesk.instafeed.instafeed.model.Follower_model;
import com.abacusdesk.instafeed.instafeed.model.OpinionModel_public;
import com.abacusdesk.instafeed.instafeed.model.Search_model;
import com.abacusdesk.instafeed.instafeed.model.User_model;
import com.android.volley.AuthFailureError;
import com.android.volley.NoConnectionError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.android.gms.common.api.Api;
import com.google.gson.Gson;
import com.google.gson.JsonObject;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by abacusdesk on 2017-07-24.
 */

public class Follow_Fragment extends Fragment {

        View view;
        RecyclerView recyclerView;
        Follow_Adapter adapter;
        ArrayList<Follower_model.DataBean> array_follower=new ArrayList<>();
        Follower_model follower_model;
        public  String  Api_temp="";
        Map<String, String> map ;

        @Nullable
        @Override
        public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
            view = inflater.inflate(R.layout.frag_recycler, container, false);
            recyclerView = (RecyclerView) view.findViewById(R.id.recyclerView);

                if (NavDrawerActivity.follower==true){
                    Api_temp= Apis.private_followers;
                }else {
                    Api_temp= Apis.private_following;
                }

            if (NavDrawerActivity.toolbar!=null){
                NavDrawerActivity.toolbar.setLogo(R.drawable.logo_black);
            }

            if (CommonFunctions.isConnected(getActivity())){
                if (!array_follower.isEmpty()){
                    array_follower.clear();
                }
                getfollowerslist();
            }else {
                Toast.makeText(getActivity(), "Internet not available", Toast.LENGTH_SHORT).show();
            }
            setRecyclerView();
            return view;
        }

        public void getfollowerslist() {
            // Dialogs.showProDialog(getActivity(), "Loading");
            StringRequest stringRequest = new StringRequest(Request.Method.POST, Api_temp,
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {
                            Log.e("log", "" + response);
                            try {
                                getResponse(response);
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                            Dialogs.disDialog();
                        }
                    },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            Dialogs.disDialog();
                            DialogInterface.OnClickListener onClickTryAgain = new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    Dialogs.disDialog();
                                    getfollowerslist();
                                }
                            };
                            if (error instanceof TimeoutError) {
                                Dialogs.disDialog();
                                Alerts.timeoutErrorAlert(getContext(), onClickTryAgain);
                            } else if (error instanceof NoConnectionError) {
                                Dialogs.disDialog();
                                Alerts.internetConnectionErrorAlert(getContext(), onClickTryAgain);
                            }
                        }
                    })
            {
                @Override
                protected Map<String, String> getParams() throws AuthFailureError {
                    Map<String, String> map = new HashMap<String, String>();
                    map.put("user", SaveSharedPreference.getUserID(getActivity()));
                    map.put("token", SaveSharedPreference.getPrefToken(getActivity()));
                    return map;
                }
                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    Map<String, String> params = new HashMap<String, String>();
                    params.put("Content-Type", "application/x-www-form-urlencoded");
                    return params;
                }
            };
            RequestQueue requestQueue = Volley.newRequestQueue(getActivity());
            requestQueue.add(stringRequest);
        }

        private void getResponse(String response ) throws JSONException {
            if(!response.equals(null)){
                Log.e("log", "" + response);
                    JSONObject jsonObject=new JSONObject(response);
                     Object object=jsonObject.get("data");
                    if (object instanceof JSONArray){
                      Gson gson = new Gson();
                      follower_model = gson.fromJson(response, Follower_model.class);
                      int status = follower_model.getStatus();
                      String msg = follower_model.getMessage();
                      if (msg.equalsIgnoreCase("success") && status==200) {
                          if (follower_model.getData()!=null && !follower_model.getData().isEmpty()){
                              for (int i = 0; i < follower_model.getData().size(); i++) {
                                  try {
                                      Follower_model.DataBean dataBean = follower_model.getData().get(i);
                                      array_follower.add(dataBean);
                                  } catch (Exception e){
                                      e.printStackTrace();
                                  }
                              }
                          }
                          TextView txtrecord=(TextView)view.findViewById(R.id.txt_records);
                          if (array_follower.isEmpty()){
                              txtrecord.setVisibility(View.VISIBLE);
                          }
                          adapter.notifyDataSetChanged();
                  }
                }
            }
        }


        private void setRecyclerView() {
            recyclerView.setHasFixedSize(true);
            recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
             try{
                adapter= new Follow_Adapter(getActivity(),array_follower,NavDrawerActivity.follower);
                recyclerView.setAdapter(adapter);
                adapter.notifyDataSetChanged();
            }catch (Exception e){
                e.printStackTrace();
            }
        }
    }
