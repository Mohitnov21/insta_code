package com.abacusdesk.instafeed.instafeed.customcamera;

/**
 * Created by abacusdesk on 2017-08-28.
 */


import android.content.Context;
import android.net.Uri;
import android.support.v4.view.PagerAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;


import com.abacusdesk.instafeed.instafeed.R;

import java.util.ArrayList;
import java.util.HashMap;

public class SlidAdapter extends PagerAdapter {

    private LayoutInflater inflater;
    private Context context;
    private ArrayList<HashMap<String,String>> arrayimages;


    public SlidAdapter(Context context, ArrayList<HashMap<String,String>> arrayimages) {
        this.context = context;
        this.arrayimages = arrayimages;
        inflater = LayoutInflater.from(context);
    }

    @Override
    public int getCount() {
        return arrayimages.size();
    }

    @Override
    public Object instantiateItem(ViewGroup view, final int position) {
        View imageLayout = inflater.inflate(R.layout.singleimage_full, view, false);

        assert imageLayout != null;
        final ImageView imageView = (ImageView) imageLayout.findViewById(R.id.image);

        final ImageView image_videoicon = (ImageView) imageLayout.findViewById(R.id.img_videoicon);

        if (arrayimages.get(position).containsKey("image")){
            image_videoicon.setVisibility(View.GONE);
            imageView
                    .setImageURI(Uri.parse(arrayimages.get(position).get("image")));
        }else {
            image_videoicon.setVisibility(View.VISIBLE);
            imageView
                    .setImageURI(Uri.parse(arrayimages.get(position).get("video")));
        }

        view.addView(imageLayout);
        return imageLayout;
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == ((FrameLayout) object);
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        container.removeView((FrameLayout) object);
    }
}