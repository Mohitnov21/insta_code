package com.abacusdesk.instafeed.instafeed.vedio_recording;

import android.app.Activity;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.abacusdesk.instafeed.instafeed.R;
import com.abacusdesk.instafeed.instafeed.Util.FilePath;
import com.abacusdesk.instafeed.instafeed.Util.Fused;
import com.abacusdesk.instafeed.instafeed.Util.Utility;
import com.bumptech.glide.Glide;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Date;

import static android.R.attr.width;

public class CreatePostActivity extends AppCompatActivity implements View.OnClickListener {

    ImageView imageView;
    public static final int REQUEST_CAMERA = 3, SELECT_FILE = 4;
    public static String DIRECTORY_NAME="InstaFeed";
    static public boolean active = false;
    private String userChoosenTask,uid_image_path;
    Fused fused;
    public  static  Dialog gpsDialog;
    LinearLayout conatiner_imageview;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_post);
        imageView = (ImageView) findViewById(R.id.image_view);
        conatiner_imageview=(LinearLayout)findViewById(R.id.container_image_view);
        imageView.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.image_view:
                    selectImage();
                Log.d("tage", "onClick: select iamge ");
                break;
            default:
                break;
        }
    }

    private void selectImage() {
        final CharSequence[] items = {"Take Photo", "Choose from Library",
                "Cancel"};

        AlertDialog.Builder builder = new AlertDialog.Builder(CreatePostActivity.this);
        builder.setTitle("Add Photo!");
        builder.setItems(items, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int item) {
                boolean result = Utility.checkPermission(CreatePostActivity.this);


                if (items[item].equals("Take Photo")) {
                    userChoosenTask = "Take Photo";
                    if (result)
                        cameraIntent();

                } else if (items[item].equals("Choose from Library")) {
                    userChoosenTask = "Choose from Library";
                    if (result)
                        galleryIntent();

                } else if (items[item].equals("Cancel")) {
                    dialog.dismiss();
                }
            }
        });
       // if (Fused.lat != null)
            builder.show();
       /* else
            Toast.makeText(this, "Please wait for latitude longitude.", Toast.LENGTH_LONG).show();*/
    }

    private void cameraIntent() {
        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        startActivityForResult(intent, REQUEST_CAMERA);
    }

    private void galleryIntent() {
        Intent intent = new Intent();
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);//
        startActivityForResult(Intent.createChooser(intent, "Select File"), SELECT_FILE);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        switch (requestCode) {
            case Utility.MY_PERMISSIONS_REQUEST_READ_EXTERNAL_STORAGE:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    if (userChoosenTask.equals("Take Photo"))
                        cameraIntent();
                    else if (userChoosenTask.equals("Choose from Library"))
                        galleryIntent();
                } else {
                    //code for deny
                }
                break;
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        switch (requestCode) {

            case SELECT_FILE:
                if (data != null)
                    onSelectFromGalleryResult(data);
                break;
            case REQUEST_CAMERA:
                if (data != null)
                    onCaptureImageResult(data);

                break;
        }
    }

    private void onSelectFromGalleryResult(Intent data) {

        Bitmap bm=null;
        Uri uri = data.getData();
        if (data != null) {
            try {
                bm = MediaStore.Images.Media.getBitmap(getContentResolver(), data.getData());
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        Date date=new Date();
        uid_image_path=   rename(path(data),DIRECTORY_NAME+"-"+date.getTime()+".jpg");
       // uid_image_path=   rename(path(data),DIRECTORY_NAME+"-"+Fused.lat.replace(".","_")+"-"+Fused.lon.replace(".","_")+"-"+date.getTime()+".jpg");

        setImage(uid_image_path);

    }

    public void setImage(String file )
    {
        ImageView imageView = new ImageView(this);
        imageView.setLayoutParams(new LinearLayout.LayoutParams(200,200));
        ImageView v_imageCancel = new ImageView(this);

        /*LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        layoutParams.gravity= Gravity.RIGHT|Gravity.CENTER_HORIZONTAL|Gravity.TOP;
        v_imageCancel.setLayoutParams(layoutParams);
        v_imageCancel.setBackgroundResource(R.drawable.cancel_icon);*/

        Log.i("File Not",file);
        try {
            Glide.with(this)
                    .load(new File(file))
                    .into(imageView);
            conatiner_imageview.addView(imageView);
          //  conatiner_imageview.addView(v_imageCancel);
        }catch (Exception e)
        {e.printStackTrace();}

    }

    public String path(Intent data)
    {

        String realPath=null;
        try
        {

            realPath= FilePath.getPath(CreatePostActivity.this,data.getData());
        }
        catch (Exception ex)
        {
            Log.i("Exception PATA",ex.getMessage());
        }

        return realPath;
    }

    private void onCaptureImageResult(Intent data) {

        Bitmap thumbnail = (Bitmap) data.getExtras().get("data");
        // ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        // thumbnail.compress(Bitmap.CompressFormat.JPEG, 90, bytes);
        String file_name = null;
     //   file_name=DIRECTORY_NAME+"-"+Fused.lat.replace(".","_")+"-"+Fused.lon.replace(".","_")+"-"+date.getTime()+".jpg";


        OutputStream outStream = null;
        //  ContextWrapper cw = new ContextWrapper(getApplicationContext());
        String extStorageDirectory = Environment.getExternalStorageDirectory().toString();
        File directory = new File(extStorageDirectory+"/"+DIRECTORY_NAME);
        if (!directory.exists()) {
            directory.mkdir();
        }
        Date date=new Date();
        file_name=   rename(path(data),DIRECTORY_NAME+"-"+date.getTime()+".jpg");
        File destination = new File(directory, file_name);

        // uid_image_path=   rename(path(data),DIRECTORY_NAME+"-"+Fused.lat.replace(".","_")+"-"+Fused.lon.replace(".","_")+"-"+date.getTime()+".jpg");
        setImage(destination.getAbsolutePath());

        try {
            outStream = new FileOutputStream(destination);
            thumbnail.compress(Bitmap.CompressFormat.PNG, 100, outStream);
            outStream.flush();
            outStream.close();

        } catch (Exception e) {
            e.printStackTrace();

        }
    }

    public String rename(String path,String name) {

        try{
            File dir=new File(path);

            File from = new File(dir.getParent(),dir.getName());
            String extStorageDirectory = Environment.getExternalStorageDirectory().toString();
            File directory = new File(extStorageDirectory+"/"+DIRECTORY_NAME);
            if (!directory.exists()) {
                directory.mkdir();
            }
            File to = new File(directory,name);
            InputStream in = new FileInputStream(from);
            OutputStream out = new FileOutputStream(to);

            // Transfer bytes from in to out
            byte[] buf = new byte[1024];
            int len;
            while ((len = in.read(buf)) > 0) {
                out.write(buf, 0, len);
            }
            in.close();
            out.close();

            return to.getAbsolutePath();
        }
        catch (Exception excep)
        {
            Log.i("Exception ",excep.getMessage());
            return "NOO"+excep;
        }
    }

    public static void update() {
       /* lati_c.setText(Fused.lat);
        longi_c.setText(Fused.lon);*/

        if (gpsDialog != null && gpsDialog.isShowing())
            gpsDialog.cancel();
    }

    @Override
    protected void onStart() {
        active=true;
        if (fused != null)
            fused.onStart();

        super.onStart();

    }

    @Override
    protected void onPause() {
        super.onPause();
        if (fused != null)
            fused.onPause();
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (fused != null)
            fused.onResume();
    }

    @Override
    protected void onStop() {
        super.onStop();
        active=false;
    }

    @Override
    protected void onDestroy() {
        if (fused != null)
            fused.onDestroy();

        super.onDestroy();

    }
    public void createGPSPushDialog(final Activity context) {

        if (gpsDialog != null && gpsDialog.isShowing())
            gpsDialog.cancel();

        gpsDialog = new Dialog(context, R.style.CustomDialogTheme);
        gpsDialog.setCancelable(false);
        gpsDialog.setContentView(R.layout.popup);
        gpsDialog.show();
    }

}
