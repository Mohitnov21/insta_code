package com.abacusdesk.instafeed.instafeed.adapter;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.abacusdesk.instafeed.instafeed.R;
import com.abacusdesk.instafeed.instafeed.Util.Apis;
import com.abacusdesk.instafeed.instafeed.fragments.Blogdetail_fragment;
import com.abacusdesk.instafeed.instafeed.fragments.Explore_detailfragment;
import com.abacusdesk.instafeed.instafeed.fragments.Newsdetail_fragment;
import com.abacusdesk.instafeed.instafeed.model.Blog_allModel;
import com.abacusdesk.instafeed.instafeed.model.Blog_model;
import com.abacusdesk.instafeed.instafeed.model.Exploreall_model;
import com.squareup.picasso.Picasso;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;

public class Blog_Adapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    ArrayList<Blog_allModel.DataBean> arrayList;
    private Activity activity;
    private final int NORMAL_ROW = 0, FIRST_ROW = 1;
    FragmentManager parentFragment;
    Blog_allModel.DataBean dataBean2;
    String imagetemp;
    public Blog_Adapter(Activity activity, ArrayList<Blog_allModel.DataBean> arrayList) {
        this.activity = activity;
        this.arrayList = arrayList;
        Log.e("arraylist size", "" + arrayList.size());
    }

    @Override
    public int getItemViewType(int position) {

            return NORMAL_ROW;

    }

    @Override
    public int getItemCount() {
        return (null != arrayList ? arrayList.size() : 0);
    }

    @Override
    public void onBindViewHolder(final RecyclerView.ViewHolder holder, final int position) {
        String myFormat = "yyyy-MM-dd HH:mm:ss";
        DateFormat sdformat = new SimpleDateFormat(myFormat);
        DateFormat targetFormat = new SimpleDateFormat("MMMM dd, yyyy");

        final Blog_allModel.DataBean dataBean = arrayList.get(position);
            ((NormalHolder)holder).title.setText(dataBean.getTitle());

            if (dataBean.getBanner()!=null){
                Picasso.with(activity).load(dataBean.getBanner()).into(((NormalHolder) holder).imgmain);
            }
            ((NormalHolder) holder).lnvideo.setVisibility(View.GONE);
             ((NormalHolder) holder).date.setVisibility(View.VISIBLE);
            try {
                String formattedDate = targetFormat.format(sdformat.parse(dataBean.getDt_added()));
                ((NormalHolder)holder).date.setText(formattedDate);
            } catch (ParseException e) {
                e.printStackTrace();
            }

        ((NormalHolder)holder).imgshare.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dataBean2=arrayList.get(position);
                if (!dataBean2.getBanner().isEmpty()){
                    imagetemp=dataBean2.getBanner();
                }
                shareimage();
            }
        });
            ((NormalHolder) holder).lnone.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    Blogdetail_fragment fragment = new Blogdetail_fragment();
                    Bundle bundle = new Bundle();
                    bundle.putString("catid",dataBean.getBlog_category_id());
                    bundle.putString("postid",dataBean.getId());
                    fragment.setArguments(bundle);

                    AppCompatActivity activity = (AppCompatActivity) v.getContext();
                    activity.getSupportFragmentManager().beginTransaction()
                            .add(R.id.frame_container, fragment,"tempfrag").addToBackStack(null).commit();
                }
            });

    }

    public  class MyAsync extends AsyncTask<Void, Void, Bitmap> {

        @Override
        protected Bitmap doInBackground(Void... params) {

            try {
                if (!imagetemp.isEmpty()){
                    URL url = new URL(imagetemp);
                    HttpURLConnection connection = (HttpURLConnection) url.openConnection();
                    connection.setDoInput(true);
                    connection.connect();
                    InputStream input = connection.getInputStream();
                    Bitmap myBitmap = BitmapFactory.decodeStream(input);
                    return myBitmap;
                }else {
                    return null;
                }
            } catch (IOException e) {
                e.printStackTrace();
                return null;
            }catch(Exception e){
                return null;
            }
        }
    }


    private void shareimage(){
        try{
            MyAsync obj = new MyAsync() {
                @Override
                protected void onPostExecute(Bitmap bmp) {
                    super.onPostExecute(bmp);

                    ByteArrayOutputStream bytes = new ByteArrayOutputStream();
                    bmp.compress(Bitmap.CompressFormat.JPEG, 100, bytes);
                    File f = new File(Environment.getExternalStorageDirectory() + File.separator + "temporary_file.jpg");
                    try {
                        f.createNewFile();
                        FileOutputStream fo = new FileOutputStream(f);
                        fo.write(bytes.toByteArray());

                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            };
            if (!imagetemp.isEmpty())
                obj.execute();
        }catch (Exception e){

        }
        Intent sharingIntent = new Intent(Intent.ACTION_SEND);
        try{
            sharingIntent.setType("*/*");
            if (!imagetemp.isEmpty())
                sharingIntent.putExtra(Intent.EXTRA_STREAM, Uri.parse("file:///sdcard/temporary_file.jpg"));
            //sharingIntent.putExtra(Intent.EXTRA_SUBJECT,dataBean_detail.getTitle());
            sharingIntent.putExtra(Intent.EXTRA_TEXT,dataBean2.getTitle()+"\n"+ Apis.Base2+"blogs/"+dataBean2.getSlug());
            Log.e("shreinterjyt",""+sharingIntent);
            activity.startActivity(Intent.createChooser(sharingIntent, "Share via"));
        }catch (Exception e){
            Log.e("exception",""+e);
        }
    }
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = null;
        //Based on view type decide which type of view to supply with viewHolder
        switch (viewType) {
            case NORMAL_ROW:
                view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_finalitem, parent, false);
                return new NormalHolder(view);

        }
        return null;
    }

    public static class NormalHolder extends RecyclerView.ViewHolder {

        public TextView txt_duration;
        public TextView title;
        public ImageView imgshare, imgmain;
        public TextView date;
        public LinearLayout lnone,lnvideo;

        public NormalHolder(View itemView) {
            super(itemView);

            this.title = (TextView) itemView.findViewById(R.id.txt_title);
            this.date = (TextView) itemView.findViewById(R.id.txt_date);

            this.imgshare = (ImageView) itemView.findViewById(R.id.img_share);

            this.imgmain = (ImageView) itemView.findViewById(R.id.img_main);
            this.lnone = (LinearLayout) itemView.findViewById(R.id.ln_one);
            this.lnvideo = (LinearLayout) itemView.findViewById(R.id.ln_video);
        }
    }
}