package com.abacusdesk.instafeed.instafeed.adapter;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.TextView;

import com.abacusdesk.instafeed.instafeed.NavDrawerActivity;
import com.abacusdesk.instafeed.instafeed.R;
import com.abacusdesk.instafeed.instafeed.model.Location_model;

import java.util.ArrayList;
import java.util.zip.Inflater;

import static android.R.attr.data;

/**
 * Created by abacusdesk on 2017-07-26.
 */

public class CustomAdapter extends BaseAdapter implements Filterable{

    Context context;
    ArrayList<Location_model.DataBean> arrayLocation;
     static String locationid;
    public CustomAdapter(Context context, ArrayList<Location_model.DataBean> arrayLocation){
        this.context=context;
        this.arrayLocation=arrayLocation;
    }
    @Override
    public int getCount() {
        return arrayLocation.size();
    }

    @Override
    public Object getItem(int position) {
        return position;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        if (convertView == null) {
            convertView = LayoutInflater.from(context).
                    inflate(R.layout.item_list, parent, false);
        }
        TextView txtlocationname=(TextView)convertView.findViewById(R.id.txt_name);
        txtlocationname.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                locationid= NavDrawerActivity.arrayLocation.get(position).getId();
                Log.e("location id",""+locationid+""+arrayLocation.get(position).getDistrict_name());
            }
        });
        txtlocationname.setText(arrayLocation.get(position).getDistrict_name());
        return convertView;
    }

    @Override
    public Filter getFilter() {
        return null;
    }
}
