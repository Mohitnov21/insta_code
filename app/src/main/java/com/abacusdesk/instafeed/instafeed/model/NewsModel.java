package com.abacusdesk.instafeed.instafeed.model;

import com.google.gson.Gson;

import java.io.StringReader;
import java.util.List;

/**
 * Created by ishaan on 6/10/2017.
 */

public class NewsModel {

    /**
     * status : 200
     * message : success
     * data : [{"id":"533","news_category_id":"4","title":"test final test","slug":"test-final-test-1497192616","short_description":"test final test minimum requirements fulfilled","location_id":"3","total_likes":null,"total_dislikes":null,"total_comments":null,"total_views":"0","total_flags":null,"dt_added":"2017-06-11 20:20:16","dt_modified":"0000-00-00 00:00:00","status":"P","user_id":"share_in","first_name":"Vikas","last_name":"Behl","nickname":"Vikas","avatar":null,"username":"vikasbehl","image":"http://instafeed.org/storage/news_mime/1497192616-1497192490414.jpg"},{"id":"532","news_category_id":"4","title":"test final test","slug":"test-final-test-1497192615","short_description":"test final test minimum requirements fulfilled","location_id":"3","total_likes":null,"total_dislikes":null,"total_comments":null,"total_views":"0","total_flags":null,"dt_added":"2017-06-11 20:20:15","dt_modified":"0000-00-00 00:00:00","status":"P","user_id":"share_in","first_name":"Vikas","last_name":"Behl","nickname":"Vikas","avatar":null,"username":"vikasbehl","image":"http://instafeed.org/storage/news_mime/1497192615-1497192489850.jpg"},{"id":"531","news_category_id":"4","title":"test final test","slug":"test-final-test-1497192496","short_description":"test final test minimum requirements fulfilled","location_id":"3","total_likes":null,"total_dislikes":null,"total_comments":null,"total_views":"0","total_flags":null,"dt_added":"2017-06-11 20:18:16","dt_modified":"0000-00-00 00:00:00","status":"P","user_id":"share_in","first_name":"Vikas","last_name":"Behl","nickname":"Vikas","avatar":null,"username":"vikasbehl","image":"http://instafeed.org/storage/news_mime/1497192496-1497192384554.jpg"},{"id":"530","news_category_id":"4","title":"test final test","slug":"test-final-test-1497192441","short_description":"test final test minimum requirements fulfilled","location_id":"3","total_likes":null,"total_dislikes":null,"total_comments":null,"total_views":"1","total_flags":null,"dt_added":"2017-06-11 20:18:33","dt_modified":"0000-00-00 00:00:00","status":"P","user_id":"share_in","first_name":"Vikas","last_name":"Behl","nickname":"Vikas","avatar":null,"username":"vikasbehl","image":"http://instafeed.org/storage/news_mime/1497192441-1497192384554.jpg"},{"id":"246","news_category_id":"7","title":"You\u2019ve gone incognito........News!","slug":"youve-gone-incognitonews-1794619679","short_description":"Pages that you view in incognito tabs won\u2019t stick around in your browser\u2019s history, cookie store or search history after you\u2019ve closed all of your incognito tabs. Any files you download or bookmarks you create will be kept.\r\n\r\nHowever, you aren\u2019t invisible. Going incognito doesn\u2019t hide you","location_id":"141","total_likes":null,"total_dislikes":null,"total_comments":"3","total_views":"21","total_flags":null,"dt_added":"2017-06-11 17:46:42","dt_modified":"2017-05-31 14:39:10","status":"P","user_id":"3905","first_name":"Abhishek","last_name":"Chauhan","nickname":"Abhishek","avatar":null,"username":"abhishek","image":"http://instafeed.org/storage/news_mime/37a0740decfb0a0ffbf19d93cdb00662.jpg"},{"id":"241","news_category_id":"1","title":"Celebrate mothers day everyday....all mom are special :)","slug":"celebrate-mothers-day-everydayall-mom-are-special-878249599","short_description":null,"location_id":"187","total_likes":null,"total_dislikes":null,"total_comments":"1","total_views":"56","total_flags":null,"dt_added":"2017-06-12 00:46:27","dt_modified":"2017-05-15 11:05:share_in","status":"P","user_id":"31","first_name":"Pooja","last_name":"Sg","nickname":"Pooja","avatar":"e4a1302befaa3249e58242b348176ad4.jpg","username":"Poojasg","image":"http://instafeed.org/storage/news_mime/5.jpg6.jpg7.jpg8.jpg"},{"id":"240","news_category_id":"8","title":"Arti at triveni ghat ","slug":"arti-at-triveni-ghat-218728401","short_description":"Triveni Ghat is situated bank of the holy river Ganges. This scared ghat used for bath for most of pilgrims. The main daily event of the attraction is the evening Aarti of Goddess Ganga also commonly called as \"Maha Aarti\". The aarti is accompanied with chanting of bhajans, beating drums and bells. ","location_id":"406","total_likes":"4","total_dislikes":null,"total_comments":null,"total_views":"41","total_flags":null,"dt_added":"2017-06-11 17:48:07","dt_modified":"2017-05-13 12:27:56","status":"P","user_id":"3910","first_name":"Kavita","last_name":"Bhatia","nickname":"Kavita","avatar":null,"username":"kbhatia","image":"http://instafeed.org/storage/news_mime/a4b4b7f6c21d9e51152a41fb2a543e82.jpeg"},{"id":"239","news_category_id":"6","title":"(राष्\u200dट्रीय प्रौधोगिकी दिवस) National Technology Day ","slug":"national-technology-day-2004610344","short_description":"11 मई  1998 को भारत ने अपना दूसरा सफल परमाणु परीक्षण पोखरण (राजस्थान) में किया था।  यह सफल परीक्षण टेक्नोलॉजी के क्षेत्र ","location_id":"187","total_likes":"3","total_dislikes":null,"total_comments":"1","total_views":"38","total_flags":null,"dt_added":"2017-06-11 11:33:07","dt_modified":"2017-05-11 15:57:52","status":"P","user_id":"46","first_name":"Vijay","last_name":"Mishra","nickname":"Vijay","avatar":"984ff9feb960e625d401340b76d75458.jpg","username":"mishravijay15","image":"http://instafeed.org/storage/news_mime/e30aa4d8d51769c404371cf2264de92f.jpg"},{"id":"238","news_category_id":"9","title":"What Do You think?","slug":"what-do-you-think-482758710","short_description":null,"location_id":"2","total_likes":"3","total_dislikes":null,"total_comments":"6","total_views":"69","total_flags":null,"dt_added":"2017-06-10 22:27:19","dt_modified":"2017-05-11 11:06:57","status":"P","user_id":"31","first_name":"Pooja","last_name":"Sg","nickname":"Pooja","avatar":"e4a1302befaa3249e58242b348176ad4.jpg","username":"Poojasg","image":"http://instafeed.org/storage/news_mime/5.jpg6.jpg7.jpg8.jpg"},{"id":"236","news_category_id":"5","title":"Has Imran Khan 'proved' that Pakistan Prime Minister promotes Jihad in Kashmir?","slug":"has-imran-khan-proved-that-pakistan-prime-minister-promotes-jihad-in-kashmir-1607056370","short_description":"The party of Pakistan's opposition leader Imran Khan has been calling for Sharif's resignation for alleged corruption.","location_id":"1","total_likes":null,"total_dislikes":null,"total_comments":"1","total_views":"38","total_flags":null,"dt_added":"2017-06-12 02:29:19","dt_modified":"2017-05-10 10:24:23","status":"P","user_id":"2","first_name":"Instafeed","last_name":" ","nickname":"Instafeed","avatar":"d6a43e12b07d7a6cf5c0fca593aff5dc.jpg","username":"instafeed","image":"http://instafeed.org/storage/news_mime/b7112139cd277a02a475de702245b004.jpg"},{"id":"235","news_category_id":"5","title":"Allahabad HC terms triple talaq unconstitutional","slug":"allahabad-hc-terms-triple-talaq-unconstitutional-58195059","short_description":"The Allahabad High Court strongly spoke on the issue of triple talaq, saying that rights of a person cannot be violated under the name of \"personal law.\" This opinion was outspoken by the court after hearing a case involving a divorce granted through the contentious practice of triple talaq. Triple ","location_id":"187","total_likes":"1","total_dislikes":null,"total_comments":"1","total_views":"74","total_flags":"1","dt_added":"2017-06-11 03:21:39","dt_modified":"2017-05-09 15:15:43","status":"P","user_id":"2","first_name":"Instafeed","last_name":" ","nickname":"Instafeed","avatar":"d6a43e12b07d7a6cf5c0fca593aff5dc.jpg","username":"instafeed","image":"http://instafeed.org/storage/news_mime/2e6527a546fd27020de64bce3f32d128.jpg"},{"id":"234","news_category_id":"1","title":"Google doodle on Ferdinand Monoyer","slug":"google-doodle-on-ferdinand-monoyer-554116758","short_description":"Ferdinand was born on 9 May 1836.\r\n\r\nOn the French ophthalmologist Ferdinand Monoyer's 181st birthday, Google paid tribute with an animated doodle of the eyes and the visual acuity chart. Ferdinand was born in France in 1836, developed the way we measure eye sight. He grew up in Lyon before moving t","location_id":"187","total_likes":null,"total_dislikes":null,"total_comments":null,"total_views":"84","total_flags":null,"dt_added":"2017-06-12 00:06:00","dt_modified":"2017-05-09 10:01:40","status":"P","user_id":"2","first_name":"Instafeed","last_name":" ","nickname":"Instafeed","avatar":"d6a43e12b07d7a6cf5c0fca593aff5dc.jpg","username":"instafeed","image":"http://instafeed.org/storage/news_mime/a09d1392ab3c196ecd185b7a1340b84b.jpg"},{"id":"232","news_category_id":"10","title":"Will Justice be given to India's daughter? Nirbhaya Gangrapists should be hanged. ","slug":"will-justice-be-given-to-indias-daughter-nirbhaya-gangrapists-should-be-hanged-1466261101","short_description":"\r\nThe Supreme Court will decide today whether to uphold the death penalty for the four convicts in the 2012 Nirbhaya gang-rape and murder case even as Nirbhaya's parents said that it is time she got justice.\r\nThe Supreme Court will today hear an appeal filed by four convicts - Akshay, Pawan, Vinay S","location_id":"141","total_likes":null,"total_dislikes":null,"total_comments":null,"total_views":"85","total_flags":"1","dt_added":"2017-06-11 03:53:29","dt_modified":"2017-05-05 14:26:26","status":"P","user_id":"2","first_name":"Instafeed","last_name":" ","nickname":"Instafeed","avatar":"d6a43e12b07d7a6cf5c0fca593aff5dc.jpg","username":"instafeed","image":"http://instafeed.org/storage/news_mime/ebf79ec420f0ee3377e51e32a22b0b70.jpg"},{"id":"231","news_category_id":"1","title":"CLEAN INDIA","slug":"clean-india-2114929955","short_description":null,"location_id":"141","total_likes":"1","total_dislikes":null,"total_comments":"0","total_views":"55","total_flags":null,"dt_added":"2017-06-11 05:51:18","dt_modified":"2017-05-05 11:39:04","status":"P","user_id":"2","first_name":"Instafeed","last_name":" ","nickname":"Instafeed","avatar":"d6a43e12b07d7a6cf5c0fca593aff5dc.jpg","username":"instafeed","image":null},{"id":"230","news_category_id":"1","title":"Most Annoying things these dayz....","slug":"most-annoying-things-these-dayz-601017690","short_description":null,"location_id":"187","total_likes":"2","total_dislikes":null,"total_comments":null,"total_views":"56","total_flags":null,"dt_added":"2017-05-04 16:31:55","dt_modified":"2017-05-04 16:31:55","status":"P","user_id":"31","first_name":"Pooja","last_name":"Sg","nickname":"Pooja","avatar":"e4a1302befaa3249e58242b348176ad4.jpg","username":"Poojasg","image":null},{"id":"229","news_category_id":"1","title":"Cleanest cities in India: Indore and Bhopal","slug":"cleanest-cities-in-india-indore-and-bhopal-123805574","short_description":"\r\nBoth the cities are located in Madhya Pradesh, Indore and Bhopal have appeared as the cleanest cities in the country as per a massive cleanliness survey ordered by the Union Urban Development Ministry. \r\n\r\nThe survey- Swachh Survekshan 2017- was executed by the Quality Council of India across 434 ","location_id":"141","total_likes":null,"total_dislikes":null,"total_comments":null,"total_views":"60","total_flags":null,"dt_added":"2017-05-04 15:51:17","dt_modified":"2017-05-04 15:51:17","status":"P","user_id":"2","first_name":"Instafeed","last_name":" ","nickname":"Instafeed","avatar":"d6a43e12b07d7a6cf5c0fca593aff5dc.jpg","username":"instafeed","image":"http://instafeed.org/storage/news_mime/907e1361dff973b65b537fdbcf5a98cc.jpg"},{"id":"228","news_category_id":"10","title":"Some General Knowledge Point You Should Know V-1.2","slug":"some-general-knowledge-point-you-should-know-v12-2054557651","short_description":null,"location_id":"141","total_likes":"2","total_dislikes":null,"total_comments":"2","total_views":"53","total_flags":null,"dt_added":"2017-05-04 14:38:59","dt_modified":"2017-05-04 14:38:59","status":"P","user_id":"5","first_name":"Sagar","last_name":"ARORA","nickname":"Sagar","avatar":"1f069d56ecce23433d4126aaf7b8719f.jpg","username":"segitips4u","image":null},{"id":"227","news_category_id":"5","title":"testing !","slug":"testing-1646462702","short_description":null,"location_id":"187","total_likes":null,"total_dislikes":null,"total_comments":null,"total_views":"23","total_flags":null,"dt_added":"2017-06-11 22:52:47","dt_modified":"2017-05-04 13:58:29","status":"P","user_id":"3","first_name":"Mohit","last_name":"Kashyap","nickname":"Mohit","avatar":"63665cbb0dca0a4bac9913b7e01a08c8.jpg","username":"mohitk","image":null},{"id":"226","news_category_id":"5","title":"Burqa wearing woman ACP leads a raid on gambling den in Ahmedabad","slug":"burqa-wearing-woman-acp-leads-a-raid-on-gambling-den-in-ahmedabad-1870979439","short_description":"Cops arrested 28 men and recovered Rs 28 lakh cash during the operation. \r\n\r\nDressed in a bueqa, ACP Manjita Vanzara raided a gambling den in Ahmedabad arresting 28 people. Acting on specific inputs, a team of Ahmedabad police raided a building in Millatnagar area of the city on April 3. \r\n\r\nEarlier","location_id":"141","total_likes":null,"total_dislikes":null,"total_comments":null,"total_views":"46","total_flags":null,"dt_added":"2017-06-12 06:33:23","dt_modified":"2017-05-04 11:58:01","status":"P","user_id":"2","first_name":"Instafeed","last_name":" ","nickname":"Instafeed","avatar":"d6a43e12b07d7a6cf5c0fca593aff5dc.jpg","username":"instafeed","image":"http://instafeed.org/storage/news_mime/47b32a8c171a8e3076c8cdd73918a828.jpg"},{"id":"225","news_category_id":"7","title":"Gujarat Lions in danger of elimination against Delhi Daredevils","slug":"gujarat-lions-in-danger-of-elimination-against-delhi-daredevils-1732631305","short_description":"\r\n\r\nDelhi Daredevils are not out of the counting for the IPL 2017 play- offs, but Gujarat Lions are in a unsafe spot as the league stage of the tournament builds up to a possibly dizzying finale. An exciting chase of 186 against defending champions at the Feroz Shah Kotla on Tuesday has moved Darede","location_id":"141","total_likes":"1","total_dislikes":null,"total_comments":null,"total_views":"83","total_flags":null,"dt_added":"2017-05-04 11:11:24","dt_modified":"2017-05-04 11:11:24","status":"P","user_id":"2","first_name":"Instafeed","last_name":" ","nickname":"Instafeed","avatar":"d6a43e12b07d7a6cf5c0fca593aff5dc.jpg","username":"instafeed","image":"http://instafeed.org/storage/news_mime/3eb371b4d7b1c5d8523bceb7440947be.jpg"}]
     */

    private int status;
    private String message;
    private List<DataBean> data;

    public static NewsModel objectFromData(String str) {

        return new Gson().fromJson(str, NewsModel.class);
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public List<DataBean> getData() {
        return data;
    }

    public void setData(List<DataBean> data) {
        this.data = data;
    }

    public static class DataBean {
        /**
         * id : 533
         * news_category_id : 4
         * title : test final test
         * slug : test-final-test-1497192616
         * short_description : test final test minimum requirements fulfilled
         * location_id : 3
         * total_likes : null
         * total_dislikes : null
         * total_comments : null
         * total_views : 0
         * total_flags : null
         * dt_added : 2017-06-11 20:20:16
         * dt_modified : 0000-00-00 00:00:00
         * status : P
         * user_id : share_in
         * first_name : Vikas
         * last_name : Behl
         * nickname : Vikas
         * avatar : null
         * username : vikasbehl
         * image : http://instafeed.org/storage/news_mime/1497192616-1497192490414.jpg
         */

        private String id;
        private String news_category_id;
        private String title;
        private String slug;
        private String short_description;
        private String location_id;
        private String total_likes;
        private String total_dislikes;
        private String total_comments;
        private String total_views;
        private Object total_flags;
        private String dt_added;
        private String dt_modified;
        private String status;
        private String user_id;
        private String first_name;

        public String getIs_like() {
            return is_like;
        }

        public void setIs_like(String is_like) {
            this.is_like = is_like;
        }

        private String is_like="";

        private String video_360x290;
        private String image_360x290;

        public String getVideo_thumb() {
            return video_thumb;
        }

        public void setVideo_thumb(String video_thumb) {
            this.video_thumb = video_thumb;
        }

        private String video_thumb;

        public String getIs_anonymous() {
            return is_anonymous;
        }

        public void setIs_anonymous(String is_anonymous) {
            this.is_anonymous = is_anonymous;
        }

        private String is_anonymous;

        private String last_name;
        private String nickname;
        private String avatar;
        private String username;
        private String image;

        public static DataBean objectFromData(String str) {

            return new Gson().fromJson(str, DataBean.class);
        }

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getNews_category_id() {
            return news_category_id;
        }

        public void setNews_category_id(String news_category_id) {
            this.news_category_id = news_category_id;
        }

        public String getTitle() {
            return title;
        }

        public void setTitle(String title) {
            this.title = title;
        }

        public String getSlug() {
            return slug;
        }

        public void setSlug(String slug) {
            this.slug = slug;
        }

        public String getShort_description() {
            return short_description;
        }

        public void setShort_description(String short_description) {
            this.short_description = short_description;
        }

        public String getLocation_id() {
            return location_id;
        }

        public void setLocation_id(String location_id) {
            this.location_id = location_id;
        }

        public String getTotal_likes() {
            return total_likes;
        }

        public void setTotal_likes(String total_likes) {
            this.total_likes = total_likes;
        }

        public String getTotal_dislikes() {
            return total_dislikes;
        }

        public void setTotal_dislikes(String total_dislikes) {
            this.total_dislikes = total_dislikes;
        }

        public String getTotal_comments() {
            return total_comments;
        }

        public void setTotal_comments(String total_comments) {
            this.total_comments = total_comments;
        }

        public String getTotal_views() {
            return total_views;
        }

        public void setTotal_views(String total_views) {
            this.total_views = total_views;
        }

        public Object getTotal_flags() {
            return total_flags;
        }

        public void setTotal_flags(Object total_flags) {
            this.total_flags = total_flags;
        }

        public String getDt_added() {
            return dt_added;
        }

        public void setDt_added(String dt_added) {
            this.dt_added = dt_added;
        }

        public String getDt_modified() {
            return dt_modified;
        }

        public void setDt_modified(String dt_modified) {
            this.dt_modified = dt_modified;
        }

        public String getStatus() {
            return status;
        }

        public void setStatus(String status) {
            this.status = status;
        }

        public String getUser_id() {
            return user_id;
        }

        public void setUser_id(String user_id) {
            this.user_id = user_id;
        }

        public String getFirst_name() {
            return first_name;
        }

        public void setFirst_name(String first_name) {
            this.first_name = first_name;
        }

        public String getLast_name() {
            return last_name;
        }

        public void setLast_name(String last_name) {
            this.last_name = last_name;
        }

        public String getNickname() {
            return nickname;
        }

        public void setNickname(String nickname) {
            this.nickname = nickname;
        }

        public String getAvatar() {
            return avatar;
        }

        public void setAvatar(String avatar) {
            this.avatar = avatar;
        }

        public String getUsername() {
            return username;
        }

        public void setUsername(String username) {
            this.username = username;
        }

        public String getImage() {
            return image;
        }

        public void setImage(String image) {
            this.image = image;
        }

        public String getVideo_360x290() {
            return video_360x290;
        }

        public void setVideo_360x290(String video_360x290) {
            this.video_360x290 = video_360x290;
        }

        public String getImage_360x290() {
            return image_360x290;
        }

        public void setImage_360x290(String image_360x290) {
            this.image_360x290 = image_360x290;
        }

    }
}
