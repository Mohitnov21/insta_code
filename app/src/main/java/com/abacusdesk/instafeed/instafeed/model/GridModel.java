package com.abacusdesk.instafeed.instafeed.model;

import java.util.ArrayList;

/**
 * Created by Administrator on 6/7/2017.
 */

public class GridModel {

    public String getTitle() {
        return title;
    }

    public ArrayList<GridModel> getMediaList() {
        return mediaList;
    }

    public void setMediaList(ArrayList<GridModel> mediaList) {
        this.mediaList = mediaList;
    }

    public String getPost_newsId() {
        return post_newsId;
    }

    public void setPost_newsId(String post_newsId) {
        this.post_newsId = post_newsId;
    }

    public void setTitle(String title) {
        this.title = title;

    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

   /* public GridModel(ArrayList<GridModel> mediaList, String title, String description, String post_newsId) {
        this.mediaList = mediaList;
        this.title = title;
        this.description = description;
        this.post_newsId = post_newsId;
    }
*/
    ArrayList<GridModel>mediaList;
    String title,description,post_newsId ;
    String FilePath,fileType,fileName;

    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    public GridModel(String fileType, String filePath, String fileName) {
        FilePath = filePath;
        this.fileType = fileType;
        this.fileName = fileName;
    }

    public GridModel(String fileType, String filePath ) {
        FilePath = filePath;
        this.fileType = fileType;
    }

    public String getFilePath() {
        return FilePath;
    }

    public void setFilePath(String filePath) {
        FilePath = filePath;
    }

    public String getFileType() {
        return fileType;
    }

    public void setFileType(String fileType) {
        this.fileType = fileType;
    }


}
