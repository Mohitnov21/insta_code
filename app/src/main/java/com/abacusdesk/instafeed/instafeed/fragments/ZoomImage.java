package com.abacusdesk.instafeed.instafeed.fragments;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.Point;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.media.Image;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Display;
import android.view.GestureDetector;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.abacusdesk.instafeed.instafeed.R;
import com.abacusdesk.instafeed.instafeed.adapter.SlidAdapter;
import com.abacusdesk.instafeed.instafeed.adapter.ZoomSlide_Adapter;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;

import me.relex.circleindicator.CircleIndicator;

public class ZoomImage extends AppCompatActivity  {

    //public static ImageView img;
    Bitmap scaled;
    FrameLayout frameLayout;
    Bitmap bm1;
    public static String imageShow;
    ViewPager viewimage;
     ImageView imageView;
    private int position;
    private ArrayList<HashMap<String,String>> arrayList=new ArrayList<>();
    private ArrayList<String> arrayImages=new ArrayList<>();
    CircleIndicator indicator;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.dialog_imageview);
        frameLayout = (FrameLayout) findViewById(R.id.frame_one);
        viewimage=(ViewPager)findViewById(R.id.img_viewpager);
        indicator = (CircleIndicator) findViewById(R.id.indicator);

        Intent intent = this.getIntent();
        Bundle bundle = intent.getExtras();
        if (bundle!=null){
           arrayList= (ArrayList<HashMap<String, String>>)bundle.getSerializable("arrayimages");
           position= Integer.parseInt(bundle.getString("position"));
        }

        if (arrayImages.isEmpty() && !arrayList.isEmpty()){
            for (int i=0;i<arrayList.size();i++){
                if (arrayList.get(i).containsKey("imagezoom")){
                    arrayImages.add(arrayList.get(i).get("imagezoom"));
                }
            }
        }
        Log.e("arrayimage",""+arrayImages);
        setViewpagerImages();
    }

    private void setViewpagerImages() {
        viewimage.setAdapter(new ZoomSlide_Adapter(ZoomImage.this,arrayImages,position));
        viewimage.setCurrentItem(position);
        viewimage.setOffscreenPageLimit(1);
        viewimage.setPageMargin(10); // TODO Convert 'px' to 'dp'
        viewimage.setPageMarginDrawable(R.color.black);
        indicator.setViewPager(viewimage);
    }



    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        //getMenuInflater().inflate(R.menu.menu_zoom_image, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
