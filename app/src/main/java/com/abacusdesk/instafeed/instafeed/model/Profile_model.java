package com.abacusdesk.instafeed.instafeed.model;

/**
 * Created by abacusdesk on 2017-07-25.
 */

public class Profile_model {

    /**
     * status : 200
     * message : success
     * data : {"user_id":"3924","username":"jkg","profile_url":"https://instafeed.org/profile/jkg","email":"jkg@gmail.com","avatar":"https://instafeed.org/storage/avatar/1501130303-bt.jpg","first_name":"jitu","middle_name":"andro","last_name":"kumar","birth_date":"2017-07-08","sex":"M","phone":"2582553538","dt_added":"0000-00-00 00:00:00","dt_modified":"2017-07-27 11:05:33","status":"A","group_id":"4","email_verified":"0","expired_at":"2017-07-29 03:27:44","created_at":"2017-07-28 15:27:44","updated_at":"2017-07-28 15:27:44","location_id":"259","bio":"","website":"","totals":{"total_news":"387","total_issues":"0","total_polls":"1","total_blogs":"2","total_petitions":"0","total_followers":"2","total_following":"9"}}
     */

    private int status;
    private String message;
    private DataBean data;

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public DataBean getData() {
        return data;
    }

    public void setData(DataBean data) {
        this.data = data;
    }

    public static class DataBean {
        /**
         * user_id : 3924
         * username : jkg
         * profile_url : https://instafeed.org/profile/jkg
         * email : jkg@gmail.com
         * avatar : https://instafeed.org/storage/avatar/1501130303-bt.jpg
         * first_name : jitu
         * middle_name : andro
         * last_name : kumar
         * birth_date : 2017-07-08
         * sex : M
         * phone : 2582553538
         * dt_added : 0000-00-00 00:00:00
         * dt_modified : 2017-07-27 11:05:33
         * status : A
         * group_id : 4
         * email_verified : 0
         * expired_at : 2017-07-29 03:27:44
         * created_at : 2017-07-28 15:27:44
         * updated_at : 2017-07-28 15:27:44
         * location_id : 259
         * bio :
         * website :
         * totals : {"total_news":"387","total_issues":"0","total_polls":"1","total_blogs":"2","total_petitions":"0","total_followers":"2","total_following":"9"}
         */

        private String user_id;
        private String username;
        private String profile_url;
        private String email;
        private String avatar;
        private String first_name;
        private String middle_name;
        private String last_name;
        private String birth_date;
        private String sex;
        private String phone;
        private String dt_added;
        private String dt_modified;
        private String status;
        private String group_id;
        private String email_verified;
        private String expired_at;
        private String created_at;
        private String updated_at;
        private String location_id;
        private String bio;
        private String website;
        private TotalsBean totals;

        public String getUser_id() {
            return user_id;
        }

        public void setUser_id(String user_id) {
            this.user_id = user_id;
        }

        public String getUsername() {
            return username;
        }

        public void setUsername(String username) {
            this.username = username;
        }

        public String getProfile_url() {
            return profile_url;
        }

        public void setProfile_url(String profile_url) {
            this.profile_url = profile_url;
        }

        public String getEmail() {
            return email;
        }

        public void setEmail(String email) {
            this.email = email;
        }

        public String getAvatar() {
            return avatar;
        }

        public void setAvatar(String avatar) {
            this.avatar = avatar;
        }

        public String getFirst_name() {
            return first_name;
        }

        public void setFirst_name(String first_name) {
            this.first_name = first_name;
        }

        public String getMiddle_name() {
            return middle_name;
        }

        public void setMiddle_name(String middle_name) {
            this.middle_name = middle_name;
        }

        public String getLast_name() {
            return last_name;
        }

        public void setLast_name(String last_name) {
            this.last_name = last_name;
        }

        public String getBirth_date() {
            return birth_date;
        }

        public void setBirth_date(String birth_date) {
            this.birth_date = birth_date;
        }

        public String getSex() {
            return sex;
        }

        public void setSex(String sex) {
            this.sex = sex;
        }

        public String getPhone() {
            return phone;
        }

        public void setPhone(String phone) {
            this.phone = phone;
        }

        public String getDt_added() {
            return dt_added;
        }

        public void setDt_added(String dt_added) {
            this.dt_added = dt_added;
        }

        public String getDt_modified() {
            return dt_modified;
        }

        public void setDt_modified(String dt_modified) {
            this.dt_modified = dt_modified;
        }

        public String getStatus() {
            return status;
        }

        public void setStatus(String status) {
            this.status = status;
        }

        public String getGroup_id() {
            return group_id;
        }

        public void setGroup_id(String group_id) {
            this.group_id = group_id;
        }

        public String getEmail_verified() {
            return email_verified;
        }

        public void setEmail_verified(String email_verified) {
            this.email_verified = email_verified;
        }

        public String getExpired_at() {
            return expired_at;
        }

        public void setExpired_at(String expired_at) {
            this.expired_at = expired_at;
        }

        public String getCreated_at() {
            return created_at;
        }

        public void setCreated_at(String created_at) {
            this.created_at = created_at;
        }

        public String getUpdated_at() {
            return updated_at;
        }

        public void setUpdated_at(String updated_at) {
            this.updated_at = updated_at;
        }

        public String getLocation_id() {
            return location_id;
        }

        public void setLocation_id(String location_id) {
            this.location_id = location_id;
        }

        public String getBio() {
            return bio;
        }

        public void setBio(String bio) {
            this.bio = bio;
        }

        public String getWebsite() {
            return website;
        }

        public void setWebsite(String website) {
            this.website = website;
        }

        public TotalsBean getTotals() {
            return totals;
        }

        public void setTotals(TotalsBean totals) {
            this.totals = totals;
        }

        public static class TotalsBean {
            /**
             * total_news : 387
             * total_issues : 0
             * total_polls : 1
             * total_blogs : 2
             * total_petitions : 0
             * total_followers : 2
             * total_following : 9
             */

            private String total_news;
            private String total_issues;
            private String total_polls;
            private String total_blogs;
            private String total_petitions;
            private String total_followers;
            private String total_following;

            public String getTotal_news() {
                return total_news;
            }

            public void setTotal_news(String total_news) {
                this.total_news = total_news;
            }

            public String getTotal_issues() {
                return total_issues;
            }

            public void setTotal_issues(String total_issues) {
                this.total_issues = total_issues;
            }

            public String getTotal_polls() {
                return total_polls;
            }

            public void setTotal_polls(String total_polls) {
                this.total_polls = total_polls;
            }

            public String getTotal_blogs() {
                return total_blogs;
            }

            public void setTotal_blogs(String total_blogs) {
                this.total_blogs = total_blogs;
            }

            public String getTotal_petitions() {
                return total_petitions;
            }

            public void setTotal_petitions(String total_petitions) {
                this.total_petitions = total_petitions;
            }

            public String getTotal_followers() {
                return total_followers;
            }

            public void setTotal_followers(String total_followers) {
                this.total_followers = total_followers;
            }

            public String getTotal_following() {
                return total_following;
            }

            public void setTotal_following(String total_following) {
                this.total_following = total_following;
            }
        }
    }
}
