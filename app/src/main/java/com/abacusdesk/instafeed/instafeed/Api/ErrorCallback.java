package Api;

import android.app.Activity;
import android.content.Context;


import com.netmax.user.rivercomic.R;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.lang.annotation.Annotation;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;

import mycomic.CustomProgressDialog;
import retrofit2.Call;
import retrofit2.CallAdapter;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;

/**
 * Created by user on 8/19/2016.
 */

public class ErrorCallback {
    /**
     * A callback which offers granular callbacks for various conditions.
     */
    public interface MyCallback<T> {
        /**
         * Called for [200, 300) responses.
         */
        void success(Response<T> response);

        /**
         * @param errorMessage, message of error
         */
        void error(String errorMessage);

//            /** Called for 401 responses. */
//            void unauthenticated(Response<?> response);
//            /** Called for [400, 500) responses, except 401. */
//            void clientError(Response<?> response);
//            /** Called for [500, 600) response. */
//            void serverError(Response<?> response);
//            /** Called for network errors while making the call. */
//            void networkError(IOException e);
//            /** Called for unexpected errors while making the call. */
//            void unexpectedError(Throwable t);
    }

    public interface MyCall<T> {
        void cancel();

        void enqueue(MyCallback<T> callback, Context context, boolean isProgress);

        MyCall<T> clone();

    }

    public static class ErrorHandlingCallAdapterFactory extends CallAdapter.Factory {
        @Override
        public CallAdapter<MyCall<?>> get(Type returnType, Annotation[] annotations, Retrofit retrofit) {
            if (getRawType(returnType) != MyCall.class) {
                return null;
            }
            if (!(returnType instanceof ParameterizedType)) {
                throw new IllegalStateException(
                        "MyCall must have generic type (e.g., MyCall<ResponseBody>)");
            }
            final Type responseType = getParameterUpperBound(0, (ParameterizedType) returnType);
            return new CallAdapter<MyCall<?>>() {
                @Override
                public Type responseType() {
                    return responseType;
                }

                @Override
                public <R> MyCall<R> adapt(Call<R> call) {
                    return new MyCallAdapter<>(call);
                }
            };
        }
    }


    /**
     * Adapts a {@link Call} to {@link MyCall}.
     */
    static class MyCallAdapter<T> implements MyCall<T> {
        private final Call<T> call;
        private Context context;
        private CustomProgressDialog progressDialog;
        MyCallAdapter(Call<T> call) {
            this.call = call;
//                this.context=context;
        }

        @Override
        public void cancel() {
            call.cancel();
        }

        @Override
        public void enqueue(final MyCallback<T> callback, final Context context, boolean isProgress) {
            this.context = context;
            if (isProgress) {
               progressDialog = new CustomProgressDialog(context, R.style.Theme_AppCompat_Dialog);
               //
               // progressDialog.setMessage("Loading...");
                progressDialog.setCancelable(Boolean.FALSE);
                if (!((Activity) context).isFinishing()) {
                    progressDialog.show();
                }
            }
            call.enqueue(new Callback<T>() {
                @Override
                public void onResponse(Call<T> call, final Response<T> response) {
                    try {
                        if (progressDialog != null && progressDialog.isShowing()) {
                            progressDialog.dismiss();
                        }
                        int code = response.code();
                        if (code >= 200 && code < 300) {
                            callback.success(response);
                        } else if (code == 401) {
                            serverError(response);
                            callback.error(response.message());
                        } else if (code >= 400 && code < 500) {
                            serverError(response);
                            callback.error(response.message());
                        } else if (code >= 500 && code < 600) {
                            serverError(response);
                            callback.error(response.message());
                        } else {
                            unexpectedError(new RuntimeException("Unexpected response " + response));
                            callback.error("Unexpected response " + response);
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }

                @Override
                public void onFailure(Call<T> call, Throwable t) {
                    //Utility.dismissProgressDialog();
                    if (progressDialog != null && progressDialog.isShowing()) {
                        progressDialog.dismiss();
                    }
                    if (t instanceof IOException) {
                        networkError((IOException) t);
                        callback.error("Network Error");
                    } else {
                        unexpectedError(t);
                        callback.error("Network Error");
                    }
                }
            });
        }

        private void unexpectedError(Throwable t) {
            t.printStackTrace();
            displayError(context.getString(R.string.unexpected_error));//+ t.getMessage());
        }

        private void networkError(IOException t) {
            t.printStackTrace();
            displayError(context.getString(R.string.network_error));//+ t.getMessage());
        }

        private void unexpectedError(RuntimeException e) {
            displayError(context.getString(R.string.unexpected_error));
        }

        private void serverError(Response<T> response) {
            //     Log.e("Error", response.message());
            try {

                String errorMessage = response.errorBody().string();
                JSONObject jsonObject = new JSONObject(errorMessage);

                /*if (response.code() == 401)
                    displayAuthErrorPopup(jsonObject.getString("message"));
                else*/
                displayError(jsonObject.getString("message"));


            } catch (JSONException | IOException e) {
                e.printStackTrace();
            }

        }

        @Override
        public MyCall<T> clone() {
            return new MyCallAdapter<>(call.clone());
        }

        private void displayError(final String message) {
            if (context instanceof Activity)
                ((Activity) context).runOnUiThread(new Runnable() {
                    public void run() {
                      //  App.getInstance().showErrorDialog(message, context);
                    }
                });
        }
    }
}
