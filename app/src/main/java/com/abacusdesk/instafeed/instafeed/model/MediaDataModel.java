package com.abacusdesk.instafeed.instafeed.model;

import java.io.Serializable;

/**
 * Created by developer on 27/11/17.
 */
public class MediaDataModel implements Serializable {
    String imgPath;
    String fileName;

    public MediaDataModel(String imgPath, String fileName) {
        this.imgPath = imgPath;
        this.fileName = fileName;
    }

    public String getImgPath() {
        return imgPath;
    }

    public String getFileName() {
        return fileName;
    }
}
