package com.abacusdesk.instafeed.instafeed;

import android.content.Intent;
import android.content.res.ColorStateList;
import android.media.MediaRecorder;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.SystemClock;
import android.support.annotation.RequiresApi;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.Chronometer;
import android.widget.TextView;
import android.widget.Toast;

import com.abacusdesk.instafeed.instafeed.Util.CommonFunctions;
import com.abacusdesk.instafeed.instafeed.model.RecordingItemModel;
import com.abacusdesk.instafeed.instafeed.services.RecordingService;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Locale;
import java.util.Timer;
import java.util.TimerTask;

import static com.abacusdesk.instafeed.instafeed.Util.ScreenConstants.AUDIO_DIRECTORY_NAME;
import static com.abacusdesk.instafeed.instafeed.Util.ScreenConstants.DIRECTORY_NAME;
import static com.abacusdesk.instafeed.instafeed.Util.ScreenConstants.INSTAFEED_AUDIO_DIRECTORY;
import static com.abacusdesk.instafeed.instafeed.Util.ScreenConstants.KEY_AUDIO_FILE_FIELD_ID;
import static com.abacusdesk.instafeed.instafeed.Util.ScreenConstants.KEY_AUDIO_FILE_LENGTH;
import static com.abacusdesk.instafeed.instafeed.Util.ScreenConstants.KEY_AUDIO_FILE_NAME;
import static com.abacusdesk.instafeed.instafeed.Util.ScreenConstants.KEY_AUDIO_FILE_TIME;
import static com.abacusdesk.instafeed.instafeed.Util.ScreenConstants.MEDIA_FILE_PATH;
import static com.abacusdesk.instafeed.instafeed.Util.ScreenConstants.MEDIA_TYPE;
import static com.abacusdesk.instafeed.instafeed.Util.ScreenConstants.MEDIA_TYPE_AUDIO;

public class SoundRecordingActivity extends AppCompatActivity {
    private static final String LOG_TAG = SoundRecordingActivity.class.getSimpleName();
    //Recording controls
    private FloatingActionButton mRecordButton = null;
    private Button mPauseButton = null;

    private TextView mRecordingPrompt;
    private int mRecordPromptCount = 0;
    private  boolean favSelected =false;

    private boolean mStartRecording = true;
    private boolean mPauseRecording = true;

    private Chronometer mChronometer = null;
    long timeWhenPaused = 0; //stores time when user clicks pause button



    private String mFileName = null;
    private String mFilePath = null;

    private MediaRecorder mRecorder = null;

    // private DBHelper mDatabase;

    private long mStartingTimeMillis = 0;
    private long mElapsedMillis = 0;
    private int  mElapsedSeconds = 0;
    private int  mId=0;

    private RecordingService.OnTimerChangedListener onTimerChangedListener = null;
    private static final SimpleDateFormat mTimerFormat = new SimpleDateFormat("mm:ss", Locale.getDefault());

    private Timer mTimer = null;
    private TimerTask mIncrementTimerTask = null;

    ArrayList<RecordingItemModel> recordingFileList;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sound_recording);
        recordingFileList=new ArrayList<>();


        // Inflate the layout for this fragment

        mChronometer = (Chronometer)findViewById(R.id.chronometer);
        //update recording prompt text
        mRecordingPrompt = (TextView) findViewById(R.id.recording_status_text);

        mRecordButton = (FloatingActionButton) findViewById(R.id.btnRecord);
        mRecordButton.setOnClickListener(new View.OnClickListener() {
            @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
            @Override
            public void onClick(View v) {
                onRecord(mStartRecording);
                mStartRecording = !mStartRecording;


            }
        });

        mPauseButton = (Button) findViewById(R.id.btnPause);
        mPauseButton.setVisibility(View.GONE); //hide pause button before recording starts
        mPauseButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onPauseRecord(mPauseRecording);
                mPauseRecording = !mPauseRecording;
            }
        });

    }

    // Recording Start/Stop
    //TODO: recording pause
    private void onRecord(boolean start){

        /// Intent intent = new Intent(getActivity(), RecordingService.class);

        if (start) {
            // start recording
            mRecordButton.setImageResource(R.drawable.ic_media_stop);

            favSelected = true;
            mRecordButton.setBackgroundTintList(new ColorStateList(new int[][]
                    {new int[]{0}}, new int[]{getResources().getColor(R.color.colorAccent)}));
            //mPauseButton.setVisibility(View.VISIBLE);
            Toast.makeText(SoundRecordingActivity.this,R.string.toast_recording_start,Toast.LENGTH_SHORT).show();
            File folder = new File(Environment.getExternalStorageDirectory() + "/"+INSTAFEED_AUDIO_DIRECTORY);
            if (!folder.exists()) {
                //folder /SoundRecorder doesn't exist, create the folder
                folder.mkdir();
            }

            //start Chronometer
            mChronometer.setBase(SystemClock.elapsedRealtime());
            mChronometer.start();
            mChronometer.setOnChronometerTickListener(new Chronometer.OnChronometerTickListener() {
                @Override
                public void onChronometerTick(Chronometer chronometer) {
                    if (mRecordPromptCount == 0) {
                        mRecordingPrompt.setText(getString(R.string.record_in_progress) + ".");
                    } else if (mRecordPromptCount == 1) {
                        mRecordingPrompt.setText(getString(R.string.record_in_progress) + "..");
                    } else if (mRecordPromptCount == 2) {
                        mRecordingPrompt.setText(getString(R.string.record_in_progress) + "...");
                        mRecordPromptCount = -1;
                    }

                    mRecordPromptCount++;
                }
            });
            startRecording();
            //start RecordingService
            // getActivity().startService(intent);
            //keep screen on while recording
            getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);

            mRecordingPrompt.setText(getString(R.string.record_in_progress) + ".");
            mRecordPromptCount++;

        } else {
            //stop recording
            mRecordButton.setImageResource(R.drawable.ic_mic_white_36dp);
            favSelected = false;
            mRecordButton.setBackgroundTintList(new ColorStateList(new int[][]{new int[]{0}}, new int[]{getResources().getColor(R.color.colorPrimary)}));

            //mPauseButton.setVisibility(View.GONE);
            mChronometer.stop();
            mChronometer.setBase(SystemClock.elapsedRealtime());
            timeWhenPaused = 0;
            mRecordingPrompt.setText(getString(R.string.record_prompt));
            stopRecording();
            // getActivity().stopService(intent);


            //allow the screen to turn off again once recording is finished
            getWindow().clearFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
        }
    }
    //TODO: implement pause recording
    private void onPauseRecord(boolean pause) {
        if (pause) {
            //pause recording
            mPauseButton.setCompoundDrawablesWithIntrinsicBounds
                    (R.drawable.ic_media_play ,0 ,0 ,0);
            mRecordingPrompt.setText((String)getString(R.string.resume_recording_button).toUpperCase());
            timeWhenPaused = mChronometer.getBase() - SystemClock.elapsedRealtime();
            mChronometer.stop();
        } else {
            //resume recording
            mPauseButton.setCompoundDrawablesWithIntrinsicBounds
                    (R.drawable.ic_media_pause ,0 ,0 ,0);
            mRecordingPrompt.setText((String)getString(R.string.pause_recording_button).toUpperCase());
            mChronometer.setBase(SystemClock.elapsedRealtime() + timeWhenPaused);
            mChronometer.start();
        }
    }

    public void startRecording() {
        setFileNameAndPath();

        mRecorder = new MediaRecorder();
        mRecorder.setAudioSource(MediaRecorder.AudioSource.MIC);
        mRecorder.setOutputFormat(MediaRecorder.OutputFormat.MPEG_4);
        mRecorder.setOutputFile(mFilePath);
        mRecorder.setAudioEncoder(MediaRecorder.AudioEncoder.AAC);
        mRecorder.setAudioChannels(1);

        // use this in settings
        /*if (MySharedPreferences.getPrefHighQuality(this)) {
            mRecorder.setAudioSamplingRate(44100);
            mRecorder.setAudioEncodingBitRate(192000);
        }*/

        //to set high quality audio
        mRecorder.setAudioSamplingRate(44100);
        mRecorder.setAudioEncodingBitRate(192000);
        try {
            mRecorder.prepare();
            mRecorder.start();
            mStartingTimeMillis = System.currentTimeMillis();


            //startTimer();
            //startForeground(1, createNotification());
        } catch (IOException e) {
            Log.e(LOG_TAG, "prepare() failed");
        }
    }

    public void setFileNameAndPath(){
        int count = 0;
        File f;
        do{
            count++;

            mFileName = AUDIO_DIRECTORY_NAME
                    + "_" + (CommonFunctions.getTimeStampInString() + count) + ".mp3";
            mFilePath = Environment.getExternalStorageDirectory().getAbsolutePath();
            Log.d(LOG_TAG, "setFileNameAndPath_AbsolutePath: "+mFilePath);// /storage/emulated/0
            mFilePath += "/"+DIRECTORY_NAME+"/" + mFileName;
            Log.d(LOG_TAG, "AbsolutePath_Directory: "+mFilePath);
            // /storage/emulated/0/InstaFeedAudio/InstaFeedAudio_20170603_1341541.mp3

            f = new File(mFilePath);
        }while (f.exists() && !f.isDirectory());
        //test
    }

    public void stopRecording() {
      try {
        mRecorder.stop();
        mElapsedMillis = (System.currentTimeMillis() - mStartingTimeMillis);
        mRecorder.release();
        Toast.makeText(SoundRecordingActivity.this, getString(R.string.toast_recording_finish) + " " + mFilePath, Toast.LENGTH_LONG).show();
        recordingFileList.add(new RecordingItemModel(mFileName,mFilePath,mId,mElapsedSeconds,System.currentTimeMillis()));
        Intent intent=new Intent();
        intent.putExtra(MEDIA_TYPE, MEDIA_TYPE_AUDIO);
        intent.putExtra(MEDIA_FILE_PATH,mFilePath);
        intent.putExtra(KEY_AUDIO_FILE_NAME,mFileName);

        intent.putExtra(KEY_AUDIO_FILE_FIELD_ID,mId);
        intent.putExtra(KEY_AUDIO_FILE_LENGTH,mElapsedSeconds);
        intent.putExtra(KEY_AUDIO_FILE_TIME,System.currentTimeMillis());
        setResult(RESULT_OK,intent);
        finish();
        //remove notification
        if (mIncrementTimerTask != null) {
            mIncrementTimerTask.cancel();
            mIncrementTimerTask = null;
        }

        mRecorder = null;
            //use this code when create in db
        } catch (Exception e){
            Log.e(LOG_TAG, "exception", e);
        }
    }
}
