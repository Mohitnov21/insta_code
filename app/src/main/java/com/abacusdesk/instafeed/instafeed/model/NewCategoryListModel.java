package com.abacusdesk.instafeed.instafeed.model;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by Administrator on 6/12/2017.
 */

public class NewCategoryListModel {

    /**
     * status : 200
     * message : success
     * data : [{"id":"1","name":"Life","description":"Life is a characteristic distinguishing physical entities having biological processes, such as signaling and self-sustaining processes, from those that do not, either because such functions have ceased, or because they never had such functions and are classified as inanimate.","dt_added":"2017-01-23 09:42:41","dt_modified":"2017-01-23 09:42:41","status":"A"},{"id":"3","name":"Technology","description":"Technology is the collection of techniques, skills, methods and processes used in the production of goods or services or in the accomplishment of objectives, such as scientific investigation.","dt_added":"2017-01-23 09:50:01","dt_modified":"2017-01-23 09:50:01","status":"A"},{"id":"4","name":"Health","description":"Health is the level of functional and metabolic efficiency of a living organism. In humans it is the ability of individuals or communities to adapt and self-manage when facing physical, mental or social changes.","dt_added":"2017-01-23 09:50:31","dt_modified":"2017-01-23 09:54:31","status":"A"},{"id":"5","name":"Politics","description":"Politics is the process of making decisions applying to all members of each group. More narrowly, it refers to achieving and exercising positions of governance organized control over a human community, particularly a state. Furthermore, politics is the study or practice of the distribution of power and resources within a given community (a usually hierarchically organized population) as well as the interrelationship(s) between communities.","dt_added":"2017-01-30 08:18:02","dt_modified":"2017-01-30 08:18:02","status":"A"},{"id":"6","name":"Nation","description":"National News","dt_added":"2017-03-23 10:26:58","dt_modified":"2017-04-18 15:14:48","status":"A"},{"id":"7","name":"Sports","description":"Sports News","dt_added":"2017-03-23 10:28:33","dt_modified":"2017-04-17 14:13:43","status":"A"},{"id":"8","name":"Travel","description":"Travel News","dt_added":"2017-03-23 10:28:33","dt_modified":"2017-04-17 11:17:19","status":"A"},{"id":"9","name":"Lifestyle","description":"Lifestyle News","dt_added":"2017-03-23 10:28:33","dt_modified":"2017-04-19 13:25:08","status":"A"},{"id":"10","name":"Entertainment","description":"Entertainment News","dt_added":"2017-03-23 10:28:33","dt_modified":"2017-04-19 13:25:01","status":"A"}]
     */

    @SerializedName("status")
    private int status;
    @SerializedName("message")
    private String message;
    @SerializedName("data")
    private List<DataBean> data;

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public List<DataBean> getData() {
        return data;
    }

    public void setData(List<DataBean> data) {
        this.data = data;
    }

    public static class DataBean {
        /**
         * id : 1
         * name : Life
         * description : Life is a characteristic distinguishing physical entities having biological processes, such as signaling and self-sustaining processes, from those that do not, either because such functions have ceased, or because they never had such functions and are classified as inanimate.
         * dt_added : 2017-01-23 09:42:41
         * dt_modified : 2017-01-23 09:42:41
         * status : A
         */

        @SerializedName("id")
        private String id;
        @SerializedName("name")
        private String name;
        @SerializedName("description")
        private String description;
        @SerializedName("dt_added")
        private String dtAdded;
        @SerializedName("dt_modified")
        private String dtModified;
        @SerializedName("status")
        private String status;

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getDescription() {
            return description;
        }

        public void setDescription(String description) {
            this.description = description;
        }

        public String getDtAdded() {
            return dtAdded;
        }

        public void setDtAdded(String dtAdded) {
            this.dtAdded = dtAdded;
        }

        public String getDtModified() {
            return dtModified;
        }

        public void setDtModified(String dtModified) {
            this.dtModified = dtModified;
        }

        public String getStatus() {
            return status;
        }

        public void setStatus(String status) {
            this.status = status;
        }
    }
}
