package com.abacusdesk.instafeed.instafeed.model;

import android.os.Parcel;

/**
 * Created by Daniel on 12/30/2014.
 */
public class RecordingItemModel{
    private String mName; // file name
    private String mFilePath; //file path
    private int mId; //id in database
    private int mLength; // length of recording in seconds
    private long mTime; // date/time of the recording

    public RecordingItemModel(String mName, String mFilePath, int mId, int mLength, long mTime) {
        this.mName = mName;
        this.mFilePath = mFilePath;
        this.mId = mId;
        this.mLength = mLength;
        this.mTime = mTime;
    }
    public RecordingItemModel()
    {

    }


    public String getFilePath() {
        return mFilePath;
    }

    public void setFilePath(String filePath) {
        mFilePath = filePath;
    }

    public int getLength() {
        return mLength;
    }

    public void setLength(int length) {
        mLength = length;
    }

    public int getId() {
        return mId;
    }

    public void setId(int id) {
        mId = id;
    }

    public String getName() {
        return mName;
    }

    public void setName(String name) {
        mName = name;
    }

    public long getTime() {
        return mTime;
    }

    public void setTime(long time) {
        mTime = time;
    }



}