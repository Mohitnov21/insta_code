package com.abacusdesk.instafeed.instafeed.Util;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

/**
 * Created by Administrator on 5/31/2017.
 */

public class MySharedPreferences {
    private  static  String PREF_HIGH_QUALITY="PREF_HIGH_QUALITY";

public  static  void setPrefHighQuality(Context context,boolean isEnabled){
    SharedPreferences preferences=PreferenceManager.getDefaultSharedPreferences(context);
    SharedPreferences.Editor editor =preferences.edit();
    editor.putBoolean(PREF_HIGH_QUALITY, isEnabled);
    editor.apply();
}

    public static boolean getPrefHighQuality(Context context) {
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);
        return preferences.getBoolean(PREF_HIGH_QUALITY, false);
    }
}
