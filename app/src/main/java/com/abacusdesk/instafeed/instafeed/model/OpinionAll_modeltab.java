package com.abacusdesk.instafeed.instafeed.model;

import java.util.List;

/**
 * Created by abacusdesk on 2017-08-08.
 */

public class OpinionAll_modeltab {


    /**
     * status : 200
     * message : success
     * data : [{"id":"101","title":"demo text title","description":"ly<\/B>\\r\\n\\r\\nAs an unwritten rule, most temples will never make cash donations to those who are financially distressed or have been affected by a natural calamity or some major accident. Instead, they will spend huge sums of money on religious programs such as Yagnas and Poojas; give away money and alms to the saints and sadhus performing such Yagnas and spend more on creating the right religious ambiance by erecting mega pandals or hiring religious heads from other parts of the country and even from abroad. \\r\\n\\r\\nTrusts of some of the big temples of India do contribute to the welfare of the society through educational, medical and social service institutions, the activities of which are largely controlled by a body appointed by the temple. Providing free meal to all visitors to the temple every day is a tradition observed by many temples. However, on an average, temples spend only a little over 10 per cent of its cash component.\\r\\n\\r\\n<B>No Accountability<\/B>\\r\\n\\r\\nMisuse of funds within the trust itself is a common phenomenon, not only among in temples but across various religious organizations. In most temples, there are groups within groups that form their own cartels which push their own agenda and use the money according to their whims and fancies. As they are not answerable to any authority, they know that no questions will be asked. \\r\\nTo deal with the problem, a concerted movement is needed.  Like-minded individuals, people forums, social service groups ","location_id":"12","total_up_votes":"1","total_down_votes":null,"total_comments":"2","total_views":"7","total_flags":null,"slug":"IN00101","user_id":"3924","dt_added":"2017-08-08 11:13:16","dt_modified":"2017-08-08 13:22:54","status":"A","image":"https://instafeed.org/storage/issue_pics/256x170-3da5a60b95c35c142c760a7c1a06d90e.jpg","first_name":"jitu","last_name":"kumar","nickname":"jitu","avatar":"https://instafeed.org/storage/avatar/80x80-1502037939-bt.jpg","username":"jkg","location":"Kanpur"},{"id":"100","title":"test rakesh","description":"test rakesh","location_id":null,"total_up_votes":null,"total_down_votes":"1","total_comments":"1","total_views":"13","total_flags":"1","slug":"IN00054","user_id":"4020","dt_added":"2017-08-03 00:00:01","dt_modified":"2017-08-03 00:00:01","status":"A","image":"https://instafeed.org/storage/issue_pics/256x170-5.jpg6.jpg7.jpg8.jpg","first_name":"Rakesh","last_name":"Bajeli","nickname":"Rakesh","avatar":"https://instafeed.org/storage/avatar/80x80-7213ccaea2dade04fde9b5a83a58e3c7.jpg","username":"rakeshbajeli41","location":null},{"id":"99","title":"Indian Mentality: A GIRL SHOULD NOT BE FAT","description":"\"Kitni patli ho gyi hai tu ab, phle kitni moti thi.. hum toh isi chinta mei they ki tri shaadi kse hogi\" My naani ji complimented me the other day. But was it actually a compliment?\r\n\r\nWhat did she mean by shaadi kese hogi.? \r\n\r\nAnd let me acknowledge you I am just 17 years old, old enough to be accused for being fat a zillion times and a zillion more to go for my entire lifetime until I attain a flat belly and pair of skinny, sexy legs.\r\n\r\nStereotypically in India being a girl and being fat do not go well together. It is like another curse that befalls on a girl she is hardly aware of. Even before she begins to enjoy her puberty her spark is subdued, Thanks to the society. \r\n\r\nMost homes find parents feeling themselves to be immensely guilty for having raised a fat girl child. She is constantly discouraged to go ahead and make choices of food and clothes on her own. She is blamed for overeating not knowing she avoids every meal that comes her way in a hope to shed some pounds.\r\n\r\nShe might try to find solace in friends but they are no good. In fact some are even worse. They call her names like bhains, motu, saandu.. She laughs it off every time but since she is accustomed doesn't mean it doesn't hurt her. These comments like stabs dig her heart deeper and deeper each time. \r\n\r\nFinally she is insecure. She is insecure of her body, her clothes, and her silhouette. She is insecure about stepping out of her room. She is insecure if someone would ever like her and like the parents mention, would someone ever marry her? \r\n\r\nAs she stands in front of the mirror her lips show a downward curve for she has been made to regret the shape of her body. Though there are plus size clothes all around in every type but she chooses to cover herself so her curves don't show. \r\n\r\nShe goes on hardcore dieting, starving herself all day long. She enrols herself into a gym that she finds hard to go each day amidst her college at 6 in the morning and misses it often. \r\n\r\nSome days are inexplicably hard for her.\r\n\r\nShe spends the night crying wetting her pillow wishing the world to be a better place where she could be accepted the way she is, where she is loved for her personality and where she is a little less 'undesirable'.\r\n\r\nShe questions to herself each night....\u2019 Is this all that matters? Why is it that my body represents me? Why do people judge on how I fat I am? Does being curvy make me offensive? The extra pounds that I carry...do they define me?'\r\nAnd the next morning her questions are answered when she is teased yet again for being a fatso.\r\n\r\nWhy are we like this? Why can't we just be casual about it.?Why does it become the hot topic in the gossip club? Why do we make them feel undesirable? Why do parents lament if their daughter is fat? Why do we say \"you are fat but beautiful.\" Why not \"Fat and beautiful\"?\r\n\r\nThere can be hundreds of such questions if I may go on but I feel this is apt for the understanding and the realisation of our fault.\r\nEvery human fat or thin desires one thing- Acceptance, acceptance and love for what they are and not what they look like. \r\nJudging, commenting, abusing, and boycotting only shows how low one can stop. \r\n","location_id":"2","total_up_votes":null,"total_down_votes":null,"total_comments":null,"total_views":"497","total_flags":null,"slug":"IN00053","user_id":"3994","dt_added":"2017-08-02 09:54:23","dt_modified":"2017-08-02 09:54:23","status":"A","image":"https://instafeed.org/storage/issue_pics/256x170-5.jpg6.jpg7.jpg8.jpg","first_name":"Sanya","last_name":"Arora","nickname":"Sanya","avatar":null,"username":"sanyaarora65","location":"Delhi"},{"id":"98","title":"From Adversity Comes Opportunity and Insight","description":"Did someone tell you, \u201cCheer up, things could be worse!\u201d and you found that things did get worse when you cheered up? That happens often enough, especially as you grow older. So, next time a person suggests you make lemonade out of the lemons life has been hurling at you, turn around and ask for the recipe to make a lemon soufflé or a lemon cake. Sometimes, you need to be combative \u2014 though avoid being combustible \u2014 when others get facetious about your distress.\r\n\r\nDeal with Insensitivity by Deepening Your Own Understanding\r\n\r\n\tTry and get into the skin of the individual who tries to be overly light hearted about your, or even her/his worries. It could emanate from an inability to understand the ramifications of a situation, or to deal with problems, or simply an inability to keep a cool head on one\u2019s shoulders when faced with a crisis. Making light of one\u2019s difficulties is helpful inasmuch that it helps to shake off well-meaning, but thoroughly useless advice. You know you\u2019re tougher than you\u2019re given credit for. Tough shouldn\u2019t mean hard-boiled though. \r\n\r\nTry and understand what put you into difficulties in the first place. It could be changes in the economic environment of the country which has had an adverse impact on your personal monetary situation. Just accept that some situations and events are beyond your control. If any of them affect you negatively, you must know how to take mitigatory action. You must swim, not sink when catastrophe hits you. Take stock of your own strengths, and do some soul searching to understand what could be the flaws in your make up which trip you.\r\n\r\nWatch Out for Secret Adversaries \r\n\r\nThere might be workplace issues which are wearing off on to you, and you might find yourself out on a limb for no fault of yours. The company where you work might be undergoing some major transformation which has no slot for you. The trick lies in staying ever alert to any rumblings below the surface. Never be lulled into complacence about your place in an organization just because you are currently the star performer. \r\n\r\nIn fact, that in itself is cause enough to attract office politics (one of the elephants in the room no one wants to talk about) and facilitate your exit. There will always be people in an organization who try and undermine the best performers, so that they don\u2019t suffer unfavorable comparison.\r\nDon\u2019t Underestimate the Role Adversity Plays in Sculpting Our Persona\r\nTechnological upgradations might have made your skills redundant. This last can usually be dealt with \u2014 depending on your personal circumstances \u2014 by upskilling or re-skilling. We discover our inner strength, and hidden talents when we are trying to deal with personal losses, a career setback, some serious illness, or even geographic relocation to pursue higher studies or a new job. Without difficulties in life, we\u2019ll never realize how adaptable or resilient we are. It takes adversity to help us identify who our real friends are, and who had been hanging around to have a good time. \r\n\r\nIn recent times the most inspiring tale of overcoming vicissitudes of fate must be writer J. K. Rowling\u2019s story. From contemplating suicide on a railway platform to living on social welfare to becoming the one of the most successful authors of all times, her life story holds numerous lessons for all of us. Rowling achieved what many parents and teachers had begun to think was impossible \u2014 she made reading a desirable pastime for adolescents again!  \r\n\r\nTake Stock of the Situation First\r\n\r\nAlways take a mental step backwards when you face a challenge. It\u2019s good to be realistic, but don\u2019t let your realism blind you to unforeseen opportunities. We\u2019ll consider some scenarios. It\u2019s normal to ask, \u201cWhy me?\u201d when hit by some major obstacle or disaster. You should look at someone less fortunate than you, and say to yourself, \u201cBut for the grace of God, there go I.\u201d Just list what you have on the credit side.\r\n\r\nTake heart from your circumstances: Superhit actor Akshay Kumar acted in 13 flop films consecutively. When he was about to give up hope, he asked himself what he had when he came to Mumbai (nothing), and what he possessed now. Once he realized that the city had brought him recognition and wealth both \u2014 he already owned two cars and four flats in Mumbai \u2014 he knew he must give it another shot. He hasn\u2019t looked back after Janwar, and is one of the most bankable stars today.\r\n\r\nTry this: If wherewithal is not a constraint, you could try and learn a new skill, art form, language, or anything that catches your fancy to overcome personal tragedy. You might even try and turn a hobby into a profession when you\u2019re in between jobs. It would earn you some much needed moolah, while the work would be pleasurable. Unless your voice is much worse than your problems, singing as you go about your work might serve to cheer you up.  \r\n\r\nConnect with people, and try to understand what makes them tick. When you scratch beneath the surface, you\u2019ll find that most people are struggling with their own demons.\r\n","location_id":"7","total_up_votes":null,"total_down_votes":null,"total_comments":null,"total_views":"671","total_flags":null,"slug":"IN00052","user_id":"3993","dt_added":"2017-08-01 18:09:21","dt_modified":"2017-08-01 18:09:21","status":"A","image":null,"first_name":"Kalpona","last_name":"Moitra","nickname":"Kalpona","avatar":null,"username":"ishaansatija007","location":"Kolkata"},{"id":"97","title":"Ever Wondered What Happens To All The Money And Gold Donated To Temples","description":"What do Indians do when faced with a distressing and hopeless situation in their life? They head to their favorite temple and promise the resident deity that if they come out of the situation unscathed, they will make a \u2018good\u2019 donation. Probably as a proof of their future intent, they will drop some inconsequential amount in the \u2018Hundi\u2019. We all identify with this situation because people who visit temples will never come out without making a monetary offering to the God. Across all religions, it is considered sacrilegious not to give God a part of your earning, regardless of whether you have the capability to do so. This is a \u2018tradition\u2019 that\u2019s deeply etched in our psyche.\r\n\r\nWe make donations and offerings to temples involuntarily. But do you ever wonder what happens to the money dropped in the Hundi?\r\n\r\nBefore we find out, it let\u2019s talk a bit about WGI? Bet you didn\u2019t know what that is. It is the World Giving Index, an annual report published by the Charities Aid Foundation. It ranks over 30 countries in the world according to how charitable they are. India\u2019s rank was a lowly 69 in 2014. Just in case it may interest you, our neighbors, Sri Lanka ranked 9 and even Bhutan ranked 11. So when it comes to charity, India has a real bad record and that temples have a lot to do with it. \r\n\r\n<B>Temples And Bad Money<\/B>\r\n\r\nTemples often attract \u2018Bad Money\u2019. It is a sort of unofficial exchange center for black money, illegal gold and even scrapped currency. Businesses that have huge stashes of illegal earnings donate them to the temple to earn some free goodwill and to wash off their sins. A major portion of the cash and gold holdings of the big temples in India constitute of black money and this is an open secret that the temples as well as the authorities and government are well aware of.\r\n\r\n<B>Who Makes How Much \u2013 A Brief Overview<\/B>\r\n\r\nHere is a brief view of the pile of wealth that the leading temples of India are sitting on. Leading the way is the Padmanabhaswamy Temple in Kerala with reserves of cash, gold and valuable amounting to one lakh crore. It is the richest Hindu temple in the world. The Tirumala Tirupati Devasthanam (TTD) collects around Rs.900 crores in its hundi annually. The temple also has gold reserves of around 52 tons.\r\n\r\nThe Shirdi Sai Baba Temple at Nashik, Maharashtra that accepts people of all caste, creed and religion collects Rs. 540 crores annually from devotees from valuable and cash. The Vaishno Devi Temple at Jammu comes next with annual earnings of Rs. 500 crores.\r\n\r\nThere are many other prominent and globally famous temples across India such as the Golden Temple, Amritsar, Punjab, the Meenakshi Temple, Madurai, the Guravayoor Temple and Sabarimala Temple of Kerala, Jagannatha Puri Temple, Orissa, Kashi Vishwanath Temple, Uttar Pradesh and many more. Together, they all must be collecting lakhs of crores annually in cash, gold and gifts. Any idea where all the money goes or for what purposes they are used?\r\n\r\n<B>What Studies Reveal<\/B>\r\n\r\nStudies about the giving nature of Indian temples show that temples don\u2019t really rush out to help people in distress. Giving away cash to the needy is not the spending model of the leading religious trusts and temples of India. They don\u2019t give charity because they temples think they don\u2019t have a social responsibility and the trust is only answerable to God.\r\n\r\n<B>Where Do They Spend Generally<\/B>\r\n\r\nAs an unwritten rule, most temples will never make cash donations to those who are financially distressed or have been affected by a natural calamity or some major accident. Instead, they will spend huge sums of money on religious programs such as Yagnas and Poojas; give away money and alms to the saints and sadhus performing such Yagnas and spend more on creating the right religious ambiance by erecting mega pandals or hiring religious heads from other parts of the country and even from abroad. \r\n\r\nTrusts of some of the big temples of India do contribute to the welfare of the society through educational, medical and social service institutions, the activities of which are largely controlled by a body appointed by the temple. Providing free meal to all visitors to the temple every day is a tradition observed by many temples. However, on an average, temples spend only a little over 10 per cent of its cash component.\r\n\r\n<B>No Accountability<\/B>\r\n\r\nMisuse of funds within the trust itself is a common phenomenon, not only among in temples but across various religious organizations. In most temples, there are groups within groups that form their own cartels which push their own agenda and use the money according to their whims and fancies. As they are not answerable to any authority, they know that no questions will be asked. \r\nTo deal with the problem, a concerted movement is needed.  Like-minded individuals, people forums, social service groups and others should spread awareness for establishing firm rules on how our religious institutions should be run.\r\n","location_id":"411","total_up_votes":null,"total_down_votes":null,"total_comments":null,"total_views":"1100","total_flags":null,"slug":"IN00051","user_id":"3991","dt_added":"2017-08-01 13:05:09","dt_modified":"2017-08-01 13:15:47","status":"A","image":null,"first_name":"Kanica","last_name":"Verma","nickname":"Kanica","avatar":null,"username":"megha2696","location":"Sangrur"},{"id":"71","title":"What Actually we are?","description":"In this video we tried to describe society's behavior towards women. \r\nIn many backward areas people are very narrow minded. They do injustice with women.  Women continue to face numerous problems, including violent victimization through rape, acid throwing, dowry killings etc.","location_id":"187","total_up_votes":"1","total_down_votes":null,"total_comments":null,"total_views":"1041","total_flags":null,"slug":"IN00033","user_id":"2","dt_added":"2017-06-27 14:37:58","dt_modified":"2017-06-27 14:37:58","status":"A","image":null,"first_name":"Instafeed","last_name":" ","nickname":"Instafeed","avatar":"https://instafeed.org/storage/avatar/80x80-d6a43e12b07d7a6cf5c0fca593aff5dc.jpg","username":"instafeed","location":"Faridabad"},{"id":"70","title":"Stop Animal Abuse","description":"In many areas people use animals for their ease of work. They are using animals for their own comfort or taste. They beat them to take extra work. In this video Bull is used for  extracting sugarcane juice, this video is recorded in Maharashtra. People have so many alternatives, they can use technology for this work but some people still using bull just because of cheap resource. People should stop animal abuse.\r\n\"ANIMALS HAVE AS MUCH RIGHT TO LIVE AS HUMAN BEINGS\"\r\n","location_id":"187","total_up_votes":null,"total_down_votes":null,"total_comments":null,"total_views":"1040","total_flags":null,"slug":"IN00032","user_id":"2","dt_added":"2017-06-27 09:36:28","dt_modified":"2017-06-27 11:02:11","status":"A","image":null,"first_name":"Instafeed","last_name":" ","nickname":"Instafeed","avatar":"https://instafeed.org/storage/avatar/80x80-d6a43e12b07d7a6cf5c0fca593aff5dc.jpg","username":"instafeed","location":"Faridabad"},{"id":"56","title":"Travel to Auli Utrakhand","description":"","location_id":"187","total_up_votes":"1","total_down_votes":null,"total_comments":null,"total_views":"990","total_flags":null,"slug":"IN00018","user_id":"3913","dt_added":"2017-05-22 12:12:27","dt_modified":"2017-05-22 12:12:27","status":"A","image":"https://instafeed.org/storage/issue_pics/256x170-f586d44ee6562328d5c1049797ab3acc.jpg","first_name":"Sonam","last_name":"Gupta","nickname":"Sonam","avatar":null,"username":"sonamgupta17041993","location":"Faridabad"},{"id":"53","title":"मानव मस्तिक के बारे में दिलचस्प तथ्य","description":"","location_id":"187","total_up_votes":"4","total_down_votes":null,"total_comments":null,"total_views":"1026","total_flags":null,"slug":"IN00015","user_id":"46","dt_added":"2017-05-11 16:46:53","dt_modified":"2017-05-11 16:46:53","status":"A","image":"https://instafeed.org/storage/issue_pics/256x170-69696fab0225b1789ce25df4f5940059.JPG","first_name":"Vijay","last_name":"Mishra","nickname":"Vijay","avatar":"https://instafeed.org/storage/avatar/80x80-984ff9feb960e625d401340b76d75458.jpg","username":"mishravijay15","location":"Faridabad"},{"id":"52","title":"Be Positive :) always..!!","description":"","location_id":"187","total_up_votes":"3","total_down_votes":null,"total_comments":"1","total_views":"999","total_flags":null,"slug":"IN00014","user_id":"31","dt_added":"2017-05-10 13:12:27","dt_modified":"2017-05-10 13:12:27","status":"A","image":"https://instafeed.org/storage/issue_pics/256x170-c508ac8d5623dc15ae83e4205e41fd20.jpg","first_name":"Pooja","last_name":"Sg","nickname":"Pooja","avatar":"https://instafeed.org/storage/avatar/80x80-e4a1302befaa3249e58242b348176ad4.jpg","username":"Poojasg","location":"Faridabad"},{"id":"51","title":"Ransomware Guide: Steps to keep your systems protected","description":"What is ransomware?\r\n\r\nRansomware is a malware that blocks the victim\u2019s access to his/her files. Some key characteristics of ransomware which sets it apart from other malwares, are listed below:\r\n1.\tShatterproof Encryption which means that you may face difficulty in decrypting the files on your own tools\r\n2.\tIt will intermix your file names, so it gets difficult for you to understand which data was affected\r\n3.\tAbility to encrypt all types of file you have on your computer; pictures, document, movie, music or any other.\r\n4.\tIt will add different type of file extensions in your file, most of which are related to ransomware\r\n5.\tIf your system is affected by the ransomware, a message pops-up to let you know that your data has been encrypted and you have to pay a specific amount of money to get it back. \r\n6.\tPayment is often demanded through \u2018bitcoins\u2019 for a limited period of time after which they demand more \r\n7.\tUses a complex set of evasion techniques to go undetected by traditional antiviruses.\r\n8.\tUses affected system as botnet (Robot+Network) for attacking on other computers on network.\r\n9.\tMany a times, it includes geographical targeting; the ransom note is translated into the victim\u2019s language for increasing their chances of receiving the ransom.\r\nCurrently, there are three types of \u2018ransomware\u2019 in circulation:\r\n1.\t Encrypting Ransomware, which integrates advanced encryption algorithms. It is designed to block system files and demand payment to provide the victim with the key that can decrypt the blocked Content. Examples include CryptoLocker, Locky, CrytpoWall, etc.\r\n2.\t Locker ransomware, which locks the victim out of the operating system, making it impossible to access the desktop and any apps or files. The files are not encrypted in this case, but the attackers still ask for a ransom to unlock the infected computer. Examples include the police-themed ransomware or Winlocker.\r\n3.\tAnother version pertaining to this type is the Master Boot Record (MBR) ransomware. The MBR is the section of a PC\u2019s hard drive which enables the operating system to boot up. When MBR ransomware strikes, the boot process can\u2019t complete as usual, and prompts a ransom note to be displayed on the screen. Examples include Satana and Petyaransomware.\r\nHowever, the most widespread type of ransomware is crypto-ransomware or encrypting ransomware. The cyber security community agrees that this is the most noticeable and troublesome cyber threat of the moment.\r\n\r\nSteps for Protecting Your Systems from Ransomware:\r\nI wish I could say that ransomware is not a life and death kind of situation, I\u2019m afraid it is. For instance, if you work at a hospital and you trigger a crypto-ransomware infection, it might actually endanger lives. Learning how to prevent ransomware attacks is a vital set of knowledge which is effective both at home and work. So here\u2019s what I want you to commit: \r\n\r\nLocal PC\r\n\r\n1.\tDon\u2019t store important data only on your PC and have atleast 2 backups of your data: on an external hard drive and in the cloud \u2013 One Drive/ Dropbox/Google Drive/etc.\r\n2.\tThe Dropbox/Google Drive/OneDrive/etc. applications on your computer will not be turned on by their own. Open them once a day, to sync your data, and close them once done. Ensure that your operating system and the soft wares are up to date, including the latest security updates.\r\n3.\tDon\u2019t use an administrator account on your computer daily instead use a guest account with restricted access.\r\n\r\nIn the Internet browser\r\n\r\n4.\tRemove the following plugins from your browsers: Adobe Flash, Adobe Reader, Java and Silverlight. In case you have to use them, set the browser to ask your permission for activating these plugins when required.\r\n5.\tAdjust your browsers\u2019 security and privacy settings for increased protection.\r\n6.\tRemove outdated plugins and add-ons from your browsers. Only keep the ones you use on a daily basis and keep them updated to the latest version and use an ad blocker to avoid the threat of potentially malicious ads.\r\n\r\nAnti-ransomware security/Safety  tools\r\n\r\n7.\tUse a reliable, paid antivirus product that includes an automatic update module and a real-time scanner.\r\n8.\tUnderstand the importance of having a traffic-filtering solution that can provide proactive anti-ransomware protection.\r\n\r\n Behavior on the Internet\r\n\r\n      9. Never open spam emails or emails from unknown senders.\r\n     10. Never download attachments from spam emails or suspicious emails.\r\n     11. Never click links in spam emails or suspicious emails.\r\n","location_id":"187","total_up_votes":"4","total_down_votes":null,"total_comments":"3","total_views":"1582","total_flags":null,"slug":"IN00013","user_id":"5","dt_added":"2017-05-10 13:02:04","dt_modified":"2017-05-10 13:04:54","status":"A","image":"https://instafeed.org/storage/issue_pics/256x170-4ba30b0c13bddbb83de27a085be6841a.jpg","first_name":"Sagar","last_name":"ARORA","nickname":"Sagar","avatar":"https://instafeed.org/storage/avatar/80x80-1f069d56ecce23433d4126aaf7b8719f.jpg","username":"segitips4u","location":"Faridabad"},{"id":"50","title":"\"Tripple Talaq\" the latest issue!!!!","description":"First, before understanding what triple talaq is, we must understand what a \u2018Nikah\u2019 (Marriage) stands for in Islam. Nikah is essentially a contract laid down in a \u2018Nikahnama\u2019 drawn between the husband and the wife. This contract can have conditions and has a compulsory \u2018consideration\u2019 (Meher) to be paid at the time of the marriage. \r\nSo to explore the question of triple talaq, one must understand that in Islam, everything is followed as per Sunnah (Deeds of the prophet). Hence, most Muslim women opposing 'triple talaq\u2019 want the Muslims to adopt \u2018Talaq-e-Sunnah\u2019 (Divorce as per the Prophet\u2019s sayings and Quranic dictation) and discard \u2018Talaq-e-Biddah\u2019 (Divorce as per a later formed mode of divorce which propagates instant divorce).","location_id":"187","total_up_votes":"5","total_down_votes":null,"total_comments":"2","total_views":"1083","total_flags":"1","slug":"IN00012","user_id":"3908","dt_added":"2017-05-07 22:54:17","dt_modified":"2017-05-07 22:54:17","status":"A","image":null,"first_name":"Bushra","last_name":"Syed","nickname":"Bushra","avatar":"https://instafeed.org/storage/avatar/80x80-5a16bf6be9ad6041fc4963a5e45e3132.jpg","username":"bushraaugust","location":"Faridabad"},{"id":"48","title":"Burning topic Triple talaq","description":"Burning topic Triple Talaq just want to share some facts about Talaq\r\n\r\nTalaq is a right given to men by islam to divorce his wife in case if the marriage cant be continued for some reason. It is similar to Khula, a right given to muslim women to seperate from her husband if she feels they cant live together. \r\nThere is one significant different between Talaq & khula . In women\u2019s case Islam give her extra freedom and authority i.e A woman can divorce her husband (khula) with immediate effect.\r\nBut in case of the talaq, once given, the husband has to wait for three months.\r\nThe triple talaq doesnt mean saying or messaging \u2018talaq\u2019 three times and ending marriage. Rather it means the person has to wait for a period of three months. Within the stipulated time if there is change in mind or the concerned problem is resolved mutually, they sure can continue the marriage.\r\nDivorced women remain in waiting for three periods, and it is not lawful for them to conceal what Allah has created in their wombs if they believe in Allah and the Last Day. And their husbands have more right to take them back in this [period] if they want reconciliation. And due to the wives is similar to what is expected of them, according to what is reasonable. But the men have a degree over them [in responsibility and authority]. And Allah is Exalted in Might and Wise\u201d The Quran 2:228\r\n\r\nFor the second time (which is not bound to happen immediately) if they face an arduous problem, talaq can be pronounced again with the same procedure.\r\n\u201cDivorce is twice. Then, either keep [her] in an acceptable manner or release [her] with good treatment\u2026 The Quran 2:229\r\nIn the mean time it the responsibilty of the family members to try to reconcile them. The muslim clerics (The Jama\u2019th) can also be approached .\r\nThe third time will be the final chance given to a muslim. Things become totaly tough for the husband.\r\n\r\nBut unfortunately people are missing it for their own benefits.  Many Muslims had misunderstood meaning of Triple Talaq and practicing it in wrong manner. Now a days, many male couple started giving Triple Talaq to their woman just by saying talaq 3 times instantaneous through social platform namely Skype, Facebook, Watsapp etc. But, this is against Islamic way of giving Talaq. And, I term this practise as \u201cInstantaneous Triple Talaq\u201d rather \u201cTriple Talaq\u201d.\r\nThey don\u2019t even release talaq (Divorce ) is the most hated thing by Allah . Islam is spiritually against divorce \r\n","location_id":"187","total_up_votes":"8","total_down_votes":null,"total_comments":"5","total_views":"1090","total_flags":"2","slug":"IN00010","user_id":"948","dt_added":"2017-05-05 12:35:49","dt_modified":"2017-05-05 12:35:49","status":"A","image":null,"first_name":"Samiya","last_name":"Khan","nickname":"Samiya","avatar":null,"username":"samiyakhan17","location":"Faridabad"},{"id":"46","title":"They Really Need Care.........","description":"","location_id":"187","total_up_votes":"4","total_down_votes":null,"total_comments":"3","total_views":"825","total_flags":"1","slug":"IN00008","user_id":"5","dt_added":"2017-05-02 13:04:21","dt_modified":"2017-05-02 13:04:21","status":"A","image":"https://instafeed.org/storage/issue_pics/256x170-8f81b1f943a56e0b655ea8b95ceb8509.jpg","first_name":"Sagar","last_name":"ARORA","nickname":"Sagar","avatar":"https://instafeed.org/storage/avatar/80x80-1f069d56ecce23433d4126aaf7b8719f.jpg","username":"segitips4u","location":"Faridabad"}]
     */

    private int status;
    private String message;
    private List<DataBean> data;

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public List<DataBean> getData() {
        return data;
    }

    public void setData(List<DataBean> data) {
        this.data = data;
    }

    public static class DataBean {
        /**
         * id : 101
         * title : demo text title
         * description : ly</B>\r\n\r\nAs an unwritten rule, most temples will never make cash donations to those who are financially distressed or have been affected by a natural calamity or some major accident. Instead, they will spend huge sums of money on religious programs such as Yagnas and Poojas; give away money and alms to the saints and sadhus performing such Yagnas and spend more on creating the right religious ambiance by erecting mega pandals or hiring religious heads from other parts of the country and even from abroad. \r\n\r\nTrusts of some of the big temples of India do contribute to the welfare of the society through educational, medical and social service institutions, the activities of which are largely controlled by a body appointed by the temple. Providing free meal to all visitors to the temple every day is a tradition observed by many temples. However, on an average, temples spend only a little over 10 per cent of its cash component.\r\n\r\n<B>No Accountability</B>\r\n\r\nMisuse of funds within the trust itself is a common phenomenon, not only among in temples but across various religious organizations. In most temples, there are groups within groups that form their own cartels which push their own agenda and use the money according to their whims and fancies. As they are not answerable to any authority, they know that no questions will be asked. \r\nTo deal with the problem, a concerted movement is needed.  Like-minded individuals, people forums, social service groups
         * location_id : 12
         * total_up_votes : 1
         * total_down_votes : null
         * total_comments : 2
         * total_views : 7
         * total_flags : null
         * slug : IN00101
         * user_id : 3924
         * dt_added : 2017-08-08 11:13:16
         * dt_modified : 2017-08-08 13:22:54
         * status : A
         * image : https://instafeed.org/storage/issue_pics/256x170-3da5a60b95c35c142c760a7c1a06d90e.jpg
         * first_name : jitu
         * last_name : kumar
         * nickname : jitu
         * avatar : https://instafeed.org/storage/avatar/80x80-1502037939-bt.jpg
         * username : jkg
         * location : Kanpur
         */

        private String id;
        private String title;
        private String description;
        private String location_id;

        private String total_up_votes;
        private Object total_down_votes;
        private String total_comments;
        private String total_views;
        private Object total_flags;

        private String slug;
        private String user_id;
        private String dt_added;
        private String dt_modified;
        private String status;
        private String image;
        private String first_name;
        private String last_name;
        private String nickname;
        private String avatar;
        private String username;
        private String location;
        private String video_thumb;
        private String is_like;

        public String getVideo_360x290() {
            return video_360x290;
        }

        public void setVideo_360x290(String video_360x290) {
            this.video_360x290 = video_360x290;
        }

        public String getImage_360x290() {
            return image_360x290;
        }

        public void setImage_360x290(String image_360x290) {
            this.image_360x290 = image_360x290;
        }

        private String video_360x290;
        private String image_360x290;

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getTitle() {
            return title;
        }

        public void setTitle(String title) {
            this.title = title;
        }

        public String getDescription() {
            return description;
        }

        public void setDescription(String description) {
            this.description = description;
        }

        public String getLocation_id() {
            return location_id;
        }

        public void setLocation_id(String location_id) {
            this.location_id = location_id;
        }

        public String getTotal_up_votes() {
            return total_up_votes;
        }

        public void setTotal_up_votes(String total_up_votes) {
            this.total_up_votes = total_up_votes;
        }

        public Object getTotal_down_votes() {
            return total_down_votes;
        }

        public void setTotal_down_votes(Object total_down_votes) {
            this.total_down_votes = total_down_votes;
        }

        public String getTotal_comments() {
            return total_comments;
        }

        public void setTotal_comments(String total_comments) {
            this.total_comments = total_comments;
        }

        public String getTotal_views() {
            return total_views;
        }

        public void setTotal_views(String total_views) {
            this.total_views = total_views;
        }

        public Object getTotal_flags() {
            return total_flags;
        }

        public void setTotal_flags(Object total_flags) {
            this.total_flags = total_flags;
        }

        public String getSlug() {
            return slug;
        }

        public void setSlug(String slug) {
            this.slug = slug;
        }

        public String getUser_id() {
            return user_id;
        }

        public void setUser_id(String user_id) {
            this.user_id = user_id;
        }

        public String getDt_added() {
            return dt_added;
        }

        public void setDt_added(String dt_added) {
            this.dt_added = dt_added;
        }

        public String getDt_modified() {
            return dt_modified;
        }

        public void setDt_modified(String dt_modified) {
            this.dt_modified = dt_modified;
        }

        public String getStatus() {
            return status;
        }

        public void setStatus(String status) {
            this.status = status;
        }

        public String getImage() {
            return image;
        }

        public void setImage(String image) {
            this.image = image;
        }

        public String getFirst_name() {
            return first_name;
        }

        public void setFirst_name(String first_name) {
            this.first_name = first_name;
        }

        public String getLast_name() {
            return last_name;
        }

        public void setLast_name(String last_name) {
            this.last_name = last_name;
        }

        public String getNickname() {
            return nickname;
        }

        public void setNickname(String nickname) {
            this.nickname = nickname;
        }

        public String getAvatar() {
            return avatar;
        }

        public void setAvatar(String avatar) {
            this.avatar = avatar;
        }

        public String getUsername() {
            return username;
        }

        public void setUsername(String username) {
            this.username = username;
        }

        public String getLocation() {
            return location;
        }

        public void setLocation(String location) {
            this.location = location;
        }

        public String getVideo_thumb() {
            return video_thumb;
        }

        public void setVideo_thumb(String video_thumb) {
            this.video_thumb = video_thumb;
        }

        public String getIs_like() {
            return is_like;
        }

        public void setIs_like(String is_like) {
            this.is_like = is_like;
        }
    }
}
