package com.abacusdesk.instafeed.instafeed;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.Signature;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.TextInputEditText;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.FragmentActivity;
import android.util.Base64;
import android.util.Log;
import android.util.Patterns;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.abacusdesk.instafeed.instafeed.Util.Alerts;
import com.abacusdesk.instafeed.instafeed.Util.Apis;
import com.abacusdesk.instafeed.instafeed.Util.AppSingleton;
import com.abacusdesk.instafeed.instafeed.Util.CommonFunctions;
import com.abacusdesk.instafeed.instafeed.Util.DbHandler;
import com.abacusdesk.instafeed.instafeed.Util.SaveSharedPreference;
import com.abacusdesk.instafeed.instafeed.Util.WebUrl;
import com.abacusdesk.instafeed.instafeed.custom_dialog.Dialogs;
import com.abacusdesk.instafeed.instafeed.fragments.Forgot_passwordfrag;
import com.abacusdesk.instafeed.instafeed.model.NewCategoryListModel;
import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NoConnectionError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.facebook.AccessToken;
import com.facebook.CallbackManager;
import com.facebook.FacebookAuthorizationException;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.appevents.AppEventsConstants;
import com.facebook.internal.Logger;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.auth.api.signin.GoogleSignInResult;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import org.json.JSONException;
import org.json.JSONObject;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

import com.facebook.appevents.AppEventsLogger;

public class Login_Activity extends FragmentActivity implements View.OnClickListener, GoogleApiClient.OnConnectionFailedListener {

    LinearLayout lnfb,lngoogle,lntwitter;
    Button btnlogin;
    TextView txtSignup,txtforgot;
    private EditText edemail,edpassword;
    String email, password;
    public static final String EMAIL_PATTERN = "^[_A-Za-z0-9-]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";
    private CallbackManager callbackManager;
    private GoogleSignInOptions gso;
    private GoogleApiClient mGoogleApiClient;

    private int RC_SIGN_IN = 100;
    String fname="",lname="",semail="",socialid="",provider="";

    ImageView imgclose;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.loginactivity_new);
        init();
        /*if (sendIntent.resolveActivity(getPackageManager()) != null) {
            startActivity(sendIntent);
        }*/
    }

    private void init(){
       edemail=(EditText) findViewById(R.id.ed_email);
       edpassword=(EditText) findViewById(R.id.ed_pswrd);

       imgclose=(ImageView)findViewById(R.id.img_close);
       imgclose.setOnClickListener(this);
       txtSignup=(TextView)findViewById(R.id.txt_signup);
       txtforgot=(TextView)findViewById(R.id.txt_forgot);
       btnlogin=(Button)findViewById(R.id.btn_login);
       lnfb=(LinearLayout)findViewById(R.id.ln_fb);
       lngoogle=(LinearLayout)findViewById(R.id.ln_gplus);

   //  lntwitter=(LinearLayout)findViewById(R.id.ln_tweet);
       lnfb.setOnClickListener(this);
       lngoogle.setOnClickListener(this);
   //  lntwitter.setOnClickListener(this);
       txtSignup.setOnClickListener(this);
       btnlogin.setOnClickListener(this);
       txtforgot.setOnClickListener(this);

       FacebookSdk.sdkInitialize(getApplicationContext());
       try{
           AppEventsLogger.activateApp(getApplication());
       } catch(Exception e){

       }

     try{
         callbackManager = CallbackManager.Factory.create();
         gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                 .requestEmail()
                 .build();

         //Initializing google api client
         mGoogleApiClient = new GoogleApiClient.Builder(this)
                 .enableAutoManage(this /* FragmentActivity */, this /* OnConnectionFailedListener */)
                 .addApi(Auth.GOOGLE_SIGN_IN_API, gso)
                 .build();
     }catch (Exception e){

     }
     //callbackManager = CallbackManager.Factory.create();
   }

    /**
     * This function assumes logger is an instance of AppEventsLogger and has been
     * created using AppEventsLogger.newLogger() call.
     */

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }

    private boolean valid() {
        email = edemail.getText().toString().trim();
        password = edpassword.getText().toString().trim();

        if (email.equals("")) {
            edemail.setError("Enter User Id");
            return false;
        }
        else if (email.matches("[0-9]+")&& email.length()<10 ) {
                edemail.setError("Minimum 10 digits should be there!");
                return false;

        } else if (!email.matches("[0-9]+")  && !email.matches(EMAIL_PATTERN) ) {
               edemail.setError("Enter Valid Email Id");
               return false;

        } else if (password.equals("")) {
            edpassword.setError("Enter Password");
            return false;
        } else if (password.length() < 6) {
            edpassword.setError("Password Should be 6 Charecter");
            return false;
        } else {
            return true;
        }
    }

    // {"status":200,"message":"success","data":{"news_id":"59","user_id":"3924","comment":"I sis","status":"A"}}

    private void hideKeyboard() {
        View view = getCurrentFocus();
        if (view != null) {
            ((InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE)).
                    hideSoftInputFromWindow(view.getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);
        }
    }

    public void facaBook() {
        if (CommonFunctions.isConnected(Login_Activity.this)) {
            LoginManager.getInstance().logInWithReadPermissions(Login_Activity.this, Arrays.asList("public_profile", "user_friends", "email"));
            callbackManager = CallbackManager.Factory.create();
            LoginManager.getInstance().registerCallback(callbackManager, new FacebookCallback<LoginResult>() {
                @Override
                public void onSuccess(LoginResult loginResult) {
                    Log.e("access", "" + AccessToken.getCurrentAccessToken().getToken().toString());
                    RequestData1();
                }

                @Override
                public void onCancel() {
                    //Toast.makeText(Login.this, "Login cancelled by user!", Toast.LENGTH_LONG).show();
                    System.out.println("Facebook Login failed!!");
                }

                @Override
                public void onError(FacebookException exception) {
                    // Toast.makeText(Login.this, "Facebook Login failed!!", Toast.LENGTH_LONG).show();
                    System.out.println("Facebook Login failed!!");
                    System.out.println("Facebook Login " + exception.toString());
                    if (exception instanceof FacebookAuthorizationException) {
                        if (AccessToken.getCurrentAccessToken() != null) {
                            LoginManager.getInstance().logOut();
                        }
                    }
                }
            });
        } else {
          // Dialogs.showCenterToast(MainFragment.this, "No Internet Connection");
        }
    }

    public void RequestData1() {
        GraphRequest request = GraphRequest.newMeRequest(AccessToken.getCurrentAccessToken(), new GraphRequest.GraphJSONObjectCallback() {
            @Override
            public void onCompleted(JSONObject object, GraphResponse response) {
                JSONObject json = response.getJSONObject();
                Log.e("Log_json", "" + json);
                try {
                    if (json != null) {
                        fname= json.getString("first_name");
                        lname= json.getString("last_name");
                        socialid=  json.getString("id");
                        provider="facebook";
                        if (json.has("email")) {
                           semail=json.getString("email");
                        }
                        Log.e("responsesuccess",""+fname+lname+socialid+provider);
                        socialLogin();
                        // ed_last.setText(json.getString("last_name"));
                    }
                } catch (Exception e) {
                   // e.printStackTrace();
                }
            }
        });

        Bundle parameters = new Bundle();
        parameters.putString("fields", "id, first_name, last_name, email,gender, birthday, location");
        request.setParameters(parameters);
        request.executeAsync();
    }

    private void signIn() {
        //Creating an intent
        Intent signInIntent = Auth.GoogleSignInApi.getSignInIntent(mGoogleApiClient);
        //Starting intent for result
        startActivityForResult(signInIntent, RC_SIGN_IN);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == RC_SIGN_IN) {
            GoogleSignInResult result = Auth.GoogleSignInApi.getSignInResultFromIntent(data);
            //Calling a new function to handle signin
            Log.e("res", "" + result.getStatus());
            handleSignInResult(result);
        }
        callbackManager.onActivityResult(requestCode, resultCode, data);
    }

    //After the signing we are calling this function
    private void handleSignInResult(GoogleSignInResult result) {
        //If the login succeed
        if (result.isSuccess()) {
            //Getting google account
            GoogleSignInAccount acct = result.getSignInAccount();
            //Displaying name and email
            Log.d("social googlweplus",""+acct.getId()+acct.getGivenName()+acct.getEmail());
            socialid=acct.getId();
            fname=acct.getGivenName();
            lname="";
            semail= acct.getEmail();
            provider="google";
            socialLogin();
            try{
                Auth.GoogleSignInApi.signOut(mGoogleApiClient);
            }catch (Exception e){

            }
        } else {
            //If login fails
            Toast.makeText(this, "Login Failed", Toast.LENGTH_LONG).show();
        }
    }

    @Override
    public void onClick(View v) {
          if (v==txtSignup){
              startActivity(new Intent(Login_Activity.this,Signup_Activity.class));
          }else if (v==btnlogin){
                     hideKeyboard();
                  if (CommonFunctions.isConnected(Login_Activity.this)){
                      if (valid()){
                        doLogin();
                      }else{
                    }
                  }else {
                      Toast.makeText(Login_Activity.this,"Internet not available",Toast.LENGTH_SHORT).show();
                  }
          }  else if (v==lnfb){
              facaBook();
          }   else if (v==txtforgot){
              startActivity(new Intent(Login_Activity.this,Forgot_passwordfrag.class));
              //getSupportFragmentManager().beginTransaction().replace(R.id.login_contain,new Forgot_passwordfrag()).addToBackStack(null).commit();
          }  else if (v==lngoogle){
               signIn();
          }else if (v==imgclose){
              finish();
          }
    }

    public void doLogin() {
        Dialogs.showProDialog(Login_Activity.this, "Loading");

        StringRequest stringRequest = new StringRequest(Request.Method.POST, Apis.login,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.e("log", "" + response);
                        try {
                            signupTrace(response);

                        } catch (JSONException e) {
                           // e.printStackTrace();
                        }
                        Dialogs.disDialog();
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Dialogs.disDialog();
                        DialogInterface.OnClickListener onClickTryAgain = new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                Dialogs.disDialog();
                                doLogin();
                            }
                        };
                        if (error instanceof TimeoutError) {
                            Dialogs.disDialog();
                            Alerts.timeoutErrorAlert(Login_Activity.this, onClickTryAgain);
                        } else if (error instanceof NoConnectionError) {
                            Dialogs.disDialog();
                            Alerts.internetConnectionErrorAlert(Login_Activity.this, onClickTryAgain);
                        }
                    }
                }
             )
        {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> map = new HashMap<String, String>();
                map.put("user",email);
                map.put("password",password);
                return map;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Content-Type", "application/x-www-form-urlencoded");
                return params;
            }
        };

        RequestQueue requestQueue = Volley.newRequestQueue(this);
        requestQueue.add(stringRequest);

    }

    private void signupTrace(String response ) throws JSONException {
        JSONObject jsonObject= new JSONObject(response);
        if(!jsonObject.getJSONObject("data").has("error")){
            Log.e("log", "" + response);
            Toast.makeText(Login_Activity.this,"Successfully logged in", Toast.LENGTH_SHORT).show();
            SaveSharedPreference.setUserID(Login_Activity.this,jsonObject.getJSONObject("data").getString("user_id"));
            SaveSharedPreference.setPrefToken(Login_Activity.this,jsonObject.getJSONObject("data").getString("token"));
            Log.e("sharedpreference",""+ SaveSharedPreference.getUserID(Login_Activity.this));
            startActivity(new Intent(Login_Activity.this,NavDrawerActivity.class));
            finish();
        }
        else if (jsonObject.getJSONObject("data").has("error")){
            Dialogs.showDialog(Login_Activity.this, jsonObject.getJSONObject("data").getString("error"));
        } else {
            Dialogs.showDialog(Login_Activity.this, "Server Failed");
        }
    }

    public void socialLogin() {
        Dialogs.showProDialog(Login_Activity.this, "Loading");
        StringRequest stringRequest = new StringRequest(Request.Method.POST, Apis.social,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {

                        Log.e("log", "" + response);
                        try {
                            checkSocial(response);
                        } catch (JSONException e) {
                           // e.printStackTrace();
                        }
                        Dialogs.disDialog();
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Dialogs.disDialog();
                        DialogInterface.OnClickListener onClickTryAgain = new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                Dialogs.disDialog();
                                socialLogin();
                            }
                        };
                        if (error instanceof TimeoutError) {
                            Dialogs.disDialog();
                            Alerts.timeoutErrorAlert(Login_Activity.this, onClickTryAgain);
                        } else if (error instanceof NoConnectionError) {
                            Dialogs.disDialog();
                            Alerts.internetConnectionErrorAlert(Login_Activity.this, onClickTryAgain);
                        }
                    }
                })
        {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> map = new HashMap<String, String>();
                map.put("social_id",socialid);
                map.put("provider_id",provider);
                map.put("first_name",fname);
                map.put("last_name",lname);
                map.put("email",semail);
                Log.e("responsesocial",""+map);
                return map;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Content-Type", "application/x-www-form-urlencoded");
                return params;
            }
        };

        RequestQueue requestQueue = Volley.newRequestQueue(this);
        requestQueue.add(stringRequest);
    }

    private void checkSocial(String response ) throws JSONException {
        JSONObject jsonObject= new JSONObject(response);
        if(!jsonObject.getJSONObject("data").has("error")){
            Log.e("log", "" + response);
            Toast.makeText(Login_Activity.this,"Successfully logged in", Toast.LENGTH_SHORT).show();
            SaveSharedPreference.setUserID(Login_Activity.this,jsonObject.getJSONObject("data").getString("user_id"));
            SaveSharedPreference.setPrefToken(Login_Activity.this,jsonObject.getJSONObject("data").getString("token"));
            if (email!=null && email.matches("[0-9]+")){
                SaveSharedPreference.setMobileLogin(Login_Activity.this,"true");
            }else {
                SaveSharedPreference.setMobileLogin(Login_Activity.this,"false");
            }
            startActivity(new Intent(Login_Activity.this,NavDrawerActivity.class));
            finish();
        }
        else if (jsonObject.getJSONObject("data").has("error")){
            Dialogs.showDialog(Login_Activity.this, jsonObject.getJSONObject("data").getString("error"));
        } else {
            Dialogs.showDialog(Login_Activity.this, "Server Failed");
        }
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }

    public static boolean hasPermissions(Context context, String... permissions) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M && context != null && permissions != null) {
            for (String permission : permissions) {
                if (ActivityCompat.checkSelfPermission(context, permission) != PackageManager.PERMISSION_GRANTED) {
                    return false;
                }
            }
        }
        return true;
    }

}
