package com.abacusdesk.instafeed.instafeed.Util;

/**
 * Created by ishaan on 6/7/2017.
 */

import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.preference.PreferenceManager;

import com.abacusdesk.instafeed.instafeed.Mobile_Signup;

public class SaveSharedPreference {

    static final String PREF_USER_NAME = "username";
    static final String PREF_LAST_NAME = "lastname";
    static final String PREF_USER_MOBILE = "mobile";
    static final String PREF_USER_EMAIL = "email";
    static final String PREF_USER_IMAGE = "image";
    static final String PREF_USER_AUTH = "place";
    static final String PREF_USER_ID = "id";
    static final String PREF_FIRSTNAME= "firstname";
    static final String PREF_GENDER="gender";
    static final String PREF_MOBILECODE="mobilecode";
    static final String PREF_MOBILENUMBER="5465465";
    static final String PREF_TOKEN="token";

    static final String FOLLOWERS="followers";
    static final String FOLLOWING="following";
    static final String MOBILE_LOGIN="mobilelogin";

    static final String IS_SPLASH="issplash";


    static SharedPreferences getSharedPreferences(Context ctx) {
        return PreferenceManager.getDefaultSharedPreferences(ctx);
    }

    public static String getPrefToken(Context ctx) {
        return getSharedPreferences(ctx).getString(PREF_TOKEN, "");
    }

    public static void setPrefToken(Context ctx, String token) {
        Editor editor = getSharedPreferences(ctx).edit();
        editor.putString(PREF_TOKEN, token);
        editor.commit();
    }

    public static void setMobileLogin(Context ctx, String mobilelogin) {
        Editor editor = getSharedPreferences(ctx).edit();
        editor.putString(MOBILE_LOGIN, mobilelogin);
        editor.commit();
    }

    public static String getMobileLogin(Context ctx) {
        return getSharedPreferences(ctx).getString(MOBILE_LOGIN, "");
    }

    public static void setIsSplash(Context ctx, String issplash) {
        Editor editor = getSharedPreferences(ctx).edit();
        editor.putString(IS_SPLASH, issplash);
        editor.commit();
    }

    public static String getIsSplash(Context ctx) {
        return getSharedPreferences(ctx).getString(IS_SPLASH, "");
    }
    public static void setFollowing(Context ctx, String following) {
        Editor editor = getSharedPreferences(ctx).edit();
        editor.putString(FOLLOWING, following);
        editor.commit();
    }

    public static String getFollowing(Context ctx) {
        return getSharedPreferences(ctx).getString(FOLLOWING, "");
    }

    public static void setFollowers(Context ctx, String folloewers) {
        Editor editor = getSharedPreferences(ctx).edit();
        editor.putString(FOLLOWERS, folloewers);
        editor.commit();
    }

    public static String getFollowers(Context ctx) {
        return getSharedPreferences(ctx).getString(FOLLOWERS, "");
    }

    public static void setFirstName(Context ctx, String userName) {
        Editor editor = getSharedPreferences(ctx).edit();
        editor.putString(PREF_FIRSTNAME, userName);
        editor.commit();
    }

    public static String getFirstName(Context ctx) {
        return getSharedPreferences(ctx).getString(PREF_FIRSTNAME, "");
    }

    public static void setGender(Context ctx, String userName) {
        Editor editor = getSharedPreferences(ctx).edit();
        editor.putString(PREF_GENDER, userName);
        editor.commit();
    }

    public static String getGender(Context ctx) {
        return getSharedPreferences(ctx).getString(PREF_GENDER, "");
    }
    public static void setMobilecode(Context ctx, String userName) {
        Editor editor = getSharedPreferences(ctx).edit();
        editor.putString(PREF_MOBILECODE, userName);
        editor.commit();
    }

    public static String getMobilecode(Context ctx) {
        return getSharedPreferences(ctx).getString(PREF_MOBILECODE, "");
    }

    public static void setMobileNum(Context ctx, String userName) {
        Editor editor = getSharedPreferences(ctx).edit();
        editor.putString(PREF_MOBILENUMBER, userName);
        editor.commit();
    }

    public static String getMobileNum(Context ctx) {
        return getSharedPreferences(ctx).getString(PREF_MOBILENUMBER, "");
    }


    public static void setUserName(Context ctx, String userName) {
        Editor editor = getSharedPreferences(ctx).edit();
        editor.putString(PREF_USER_NAME, userName);
        editor.commit();
    }

    public static String getUserName(Context ctx) {
        return getSharedPreferences(ctx).getString(PREF_USER_NAME, "");
    }

    public static void setLastName(Context ctx, String userName) {
        Editor editor = getSharedPreferences(ctx).edit();
        editor.putString(PREF_LAST_NAME, userName);
        editor.commit();
    }

    public static String getLastName(Context ctx) {
        return getSharedPreferences(ctx).getString(PREF_LAST_NAME, "");
    }

    public static void setMobile(Context ctx, String userName) {
        Editor editor = getSharedPreferences(ctx).edit();
        editor.putString(PREF_USER_MOBILE, userName);
        editor.commit();
    }

    public static String getMobile(Context ctx) {
        return getSharedPreferences(ctx).getString(PREF_USER_MOBILE, "");
    }

    public static void setUserID(Context ctx, String userid) {
        Editor editor = getSharedPreferences(ctx).edit();
        editor.putString(PREF_USER_ID, userid);
        editor.commit();
    }

    public static String getUserID(Context ctx) {
        return getSharedPreferences(ctx).getString(PREF_USER_ID, "");
    }


    public static void setUserEMAIL(Context ctx, String useremail) {
        Editor editor = getSharedPreferences(ctx).edit();
        editor.putString(PREF_USER_EMAIL, useremail);
        editor.commit();
    }

    public static String getUserEMAIL(Context ctx) {
        return getSharedPreferences(ctx).getString(PREF_USER_EMAIL, "");
    }

    public static void setUserIMAGE(Context ctx, String userimage) {
        Editor editor = getSharedPreferences(ctx).edit();
        editor.putString(PREF_USER_IMAGE, userimage);
        editor.commit();
    }

    public static String getUserIMAGE(Context ctx) {
        return getSharedPreferences(ctx).getString(PREF_USER_IMAGE, "");
    }


    public static void setUSERAuth(Context ctx, String place) {
        Editor editor = getSharedPreferences(ctx).edit();
        editor.putString(PREF_USER_AUTH, place);
        editor.commit();
    }

    public static String getUSERAuth(Context ctx) {
        return getSharedPreferences(ctx).getString(PREF_USER_AUTH, "");
    }

}