package com.abacusdesk.instafeed.instafeed;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TextInputEditText;
import android.support.design.widget.TextInputLayout;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Toast;

import com.abacusdesk.instafeed.instafeed.Util.Alerts;
import com.abacusdesk.instafeed.instafeed.Util.Apis;
import com.abacusdesk.instafeed.instafeed.Util.CommonFunctions;
import com.abacusdesk.instafeed.instafeed.Util.SaveSharedPreference;
import com.abacusdesk.instafeed.instafeed.custom_dialog.Dialogs;
import com.abacusdesk.instafeed.instafeed.fragments.Forgot_passwordfrag;
import com.abacusdesk.instafeed.instafeed.webservice.VolleySingleton;
import com.android.volley.AuthFailureError;
import com.android.volley.NoConnectionError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by abacusdesk on 2017-07-28.
 */

public class Mobile_Signup extends Activity implements View.OnClickListener {

    ImageView imgclose;
    TextInputEditText edmobile,edpassword,edconpassword;
    Button btnsignup,btnlogin;
    public static String mobile,password,conpassword;
    TextInputLayout inputmobile,inputpassword,inputconpassword;
    public static String otp_get="";
    public static String mobileno="";

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.signup_mobile);
        init();
    }

    private void init() {
        inputmobile = (TextInputLayout) findViewById(R.id.txtinput_mobile);
        inputpassword = (TextInputLayout) findViewById(R.id.txtinput_password);
        inputconpassword = (TextInputLayout) findViewById(R.id.txtinput_conpassword);

        edmobile = (TextInputEditText) findViewById(R.id.ed_mobile);
        edpassword = (TextInputEditText) findViewById(R.id.ed_password);
        edconpassword = (TextInputEditText) findViewById(R.id.ed_conpassword);

        btnsignup = (Button) findViewById(R.id.btn_signup);
        btnlogin = (Button) findViewById(R.id.btn_login);

        btnsignup.setOnClickListener(this);
        btnlogin.setOnClickListener(this);
    }

    private boolean valid() {
        mobile = inputmobile.getEditText().getText().toString();
        password=inputpassword.getEditText().getText().toString();
        conpassword=inputconpassword.getEditText().getText().toString();
        if (mobile.equals("")) {
            edmobile.setError("Enter Mobile No.");
            return false;
        } else if (mobile.length()<10) {
            edmobile.setError("Incorrect Mobile No.");
            return false;
        }  else if (password.length()<8) {
            edpassword.setError("Minimum 8 characters required");
            return false;
        } else if (!conpassword.equals(password)) {
            edconpassword.setError("Incorrect Password");
            return false;
        } else {
            return true;
        }
    }

    public  static void registerMobile(final Context context, final String temp) {
        Dialogs.showProDialog(context, "Loading");
        StringRequest stringRequest = new StringRequest(Request.Method.POST, Apis.mobilesignup,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.e("log", "" + response);
                        try {
                            getResponse(response,context,temp);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        Dialogs.disDialog();
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Dialogs.disDialog();
                        DialogInterface.OnClickListener onClickTryAgain = new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                Dialogs.disDialog();
                                registerMobile(context,temp);
                            }
                        };
                        if (error instanceof TimeoutError) {
                            Dialogs.disDialog();
                            Alerts.timeoutErrorAlert(context, onClickTryAgain);
                        } else if (error instanceof NoConnectionError) {
                            Dialogs.disDialog();
                            Alerts.internetConnectionErrorAlert(context, onClickTryAgain);
                        }
                    }
                })
        {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> map = new HashMap<String, String>();
                map.put("mobile",mobile);
                map.put("password",password);
                Log.e("map",""+map);
                return map;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Content-Type", "application/x-www-form-urlencoded");
                return params;
            }
        };
        VolleySingleton.getInstance(context).addToRequestQueue(stringRequest);
    }

    /*
    {
        "status": 200,
            "message": "success",
            "data": {
                "mobile": "8860837529",
                "otp": "884491"
    }
    }*/
    //{"status":200,"message":"error","data":{"message":"Mobile No already exists"}}

    private static void getResponse(String response,Context context,String temp) throws JSONException {
        if(!response.equals(null)){
            JSONObject jsonObject=new JSONObject(response);
            if (jsonObject.getString("message").equalsIgnoreCase("success")){
             Toast.makeText(context,jsonObject.getString("message"),Toast.LENGTH_SHORT).show();
               otp_get=jsonObject.getJSONObject("data").getString("otp");
               mobileno=jsonObject.getJSONObject("data").getString("mobile");
                if (temp.equals("true")){
                }else {
                    context.startActivity(new Intent(context,Otp_verification.class));
                }

                //  finish();
            }else if (jsonObject.getString("message").equalsIgnoreCase("error")){
                Toast.makeText(context,jsonObject.getJSONObject("data").getString("message"),Toast.LENGTH_SHORT).show();
            }
        }
    }

    @Override
    public void onClick(View v) {
        if (v==btnsignup){
            if (CommonFunctions.isConnected(Mobile_Signup.this)){
                if (valid()){
                    registerMobile(this,"false");
                }
            }
        }else if (v==btnlogin){
            Intent intent= new Intent(Mobile_Signup.this,Login_Activity.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(intent);
            finish();
        }
    }
}