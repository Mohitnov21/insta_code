package com.abacusdesk.instafeed.instafeed.customcamera;

/**
 * Created by abacusdesk on 2017-12-15.
 */

import android.app.AlertDialog;
import android.app.AlertDialog.Builder;
import android.content.ContentValues;
import android.content.Context;
import android.content.DialogInterface;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.Bitmap.CompressFormat;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.hardware.Camera;
import android.hardware.Camera.CameraInfo;
import android.hardware.Camera.ErrorCallback;
import android.hardware.Camera.Parameters;
import android.hardware.Camera.PictureCallback;
import android.media.CamcorderProfile;
import android.media.MediaRecorder;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.Environment;
import android.os.Handler;
import android.provider.MediaStore;
import android.provider.MediaStore.Images;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Base64;
import android.util.Log;
import android.util.Size;
import android.view.ActionMode;
import android.view.ContextThemeWrapper;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.Surface;
import android.view.SurfaceHolder;
import android.view.SurfaceHolder.Callback;
import android.view.SurfaceView;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.abacusdesk.instafeed.instafeed.NavDrawerActivity;
import com.abacusdesk.instafeed.instafeed.R;
import com.abacusdesk.instafeed.instafeed.custom_dialog.Dialogs;
import com.abacusdesk.instafeed.instafeed.fragments.CreateFragment;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.concurrent.TimeUnit;

import static com.abacusdesk.instafeed.instafeed.Util.ScreenConstants.MEDIA_FILE_NAME;
import static com.abacusdesk.instafeed.instafeed.Util.ScreenConstants.MEDIA_FILE_PATH;
import static com.abacusdesk.instafeed.instafeed.Util.ScreenConstants.MEDIA_TYPE;
import static com.abacusdesk.instafeed.instafeed.Util.ScreenConstants.MEDIA_TYPE_IMAGE;
import static com.abacusdesk.instafeed.instafeed.Util.ScreenConstants.MEDIA_TYPE_VIDEO;

public class CameraDemoActivity extends AppCompatActivity implements Callback,
        OnClickListener {

    private SurfaceView surfaceView;
    private SurfaceHolder surfaceHolder;
    private Camera camera;
    private Button flipCamera;
    private Button flashCameraButton;
    private Button captureImage,btn_next,btncancel,btn_check;
    private int cameraId;
    private boolean flashmode = false;
    private int rotation;
    private Cursor cursor;
    private int columnIndex;
    private RecyclerView recyclerView;
    private ArrayList<HashMap<String,String>> array_imahges;
    public static RelativeLayout relativeLayout;
    private boolean isMultiSelect = false;
    private ArrayList<String> selectedIds = new ArrayList<>();
    private ArrayList<String> selectedimages = new ArrayList<>();
    private ActionMode actionMode;
    private ImageAdapter adapter;
    private TextView txtcount;
    private FrameLayout framecheck;
    private LinearLayout lnlist;
    private Boolean is_picturetaken=false;
    //private GalleryPhotoAlbum album;
    //private ArrayList<GalleryPhotoAlbum> arrayListAlbums=new ArrayList<>();
    private MediaRecorder mediaRecorder;
    //fetching images and videos insgtances

    //video recording instances
    boolean recording = false;
    private FrameLayout myCameraPreview = null; // this layout contains surfaceview
    private boolean cameraFront = false;
    private boolean isCameraFlashOn;
    private boolean isVideoRecordingPause;
    private int setOrientationHint;
    // Defined CountDownTimer variables
    private  long startVideoRecordingTime=10*1000*60*10;
    private CountDownTimer countDownTimer;
    private long timeElapsed;
    private String saveUrl="/sdcard/myvideo.mp4";
    private Bundle extras=null;
    private boolean isTime,isUrl;
    private String videoDuration;
    private Parameters p;
    private TextView txttimer,txthint;

    String[] PERMISSIONS = {android.Manifest.permission.CAMERA,
            android.Manifest.permission.WRITE_EXTERNAL_STORAGE,
            android.Manifest.permission.ACCESS_FINE_LOCATION,
            android.Manifest.permission.INTERNET};

    public static final int PERMISSION_ALL = 1;
    private int count;
    private Boolean bool=false;
    private ImageView imgvideo;

    private ArrayList<HashMap<String,String>> array_hashselected=new ArrayList<>();

    private HashMap<String,
            ArrayList<HashMap<String,String>>> hashMapHashMap=new HashMap<>();

    private String mediatype="",mediapath="";
    private String path_video="";
    String filename="";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.camerademo);
        try {
            if (!hasPermissions(this, PERMISSIONS)) {
                ActivityCompat.requestPermissions(this, PERMISSIONS, PERMISSION_ALL);
            }
        } catch (Exception e1) {
            e1.printStackTrace();
        }
        init();
        // camera surface view /created
        new LongOperation().execute("");
        checkIntentExtra();
        //fetchbothmedias();
    }

    public static boolean hasPermissions(Context context, String... permissions) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M && context != null && permissions != null) {
            for (String permission : permissions) {
                if (ActivityCompat.checkSelfPermission(context, permission) != PackageManager.PERMISSION_GRANTED) {
                    return false;
                }
            }
        }
        return true;
    }

    private void init(){
        array_imahges = new ArrayList<>();
        imgvideo=(ImageView)findViewById(R.id.img_videoicon);

        cameraId = CameraInfo.CAMERA_FACING_BACK;
        btn_next = (Button) findViewById(R.id.btn_forward);
        btn_check = (Button) findViewById(R.id.btn_right);
        btn_check.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                is_picturetaken=false;
                isMultiSelect=false;
                if (mediatype.equalsIgnoreCase("image")){
                    callPostFragment(MEDIA_TYPE_IMAGE,mediapath,filename);
                }else if (mediatype.equalsIgnoreCase("video")){
                    callPostFragment(MEDIA_TYPE_VIDEO,mediapath,mediapath);
                }else {

                }
            }
        });

        txttimer=(TextView)findViewById(R.id.txt_timer);

        txthint=(TextView)findViewById(R.id.txt_hint);
        btncancel = (Button) findViewById(R.id.btn_cancel);

        btncancel.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                camera.startPreview();
                setVisibility(false);
            }
        });
        flipCamera = (Button) findViewById(R.id.flipCamera);
        flashCameraButton = (Button) findViewById(R.id.flash);
        captureImage = (Button) findViewById(R.id.captureImage);
        captureImage.setOnTouchListener(new View.OnTouchListener() {

            private Handler mHandler;
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if (event.getAction() == MotionEvent.ACTION_DOWN) {

                    bool=false;
                    if (mHandler != null) return true;
                    mHandler = new Handler();
                    Log.e("action","down");
                    mHandler.postDelayed(mAction, 600);
                } else if (event.getAction() == MotionEvent.ACTION_UP) {
                    if (bool==true){
                        startVideoRecording();
                        setVisibility(true);
                        if (!path_video.isEmpty()){

                            Log.e("pathvideo",""+path_video);
                            mediatype="video";
                            mediapath=path_video;
                        }
                        txthint.setVisibility(View.GONE);
                        camera.stopPreview();
                        btncancel.setVisibility(View.GONE);
                        imgvideo.setVisibility(View.VISIBLE);
                    }else {
                        Log.e("click","singletime");
                        takeImage();
                    }

                    if (mHandler == null) return true;
                    Log.e("action","up");
                    mHandler.removeCallbacks(mAction);
                    mHandler = null;
                }
                return false;
            }

            Runnable mAction = new Runnable() {
                @Override
                public void run() {
                   // System.out.println("Performing action...");
                    Log.e("2seconds","happened");
                    startVideoRecording();
                    lnlist.setVisibility(View.GONE);
                    setvisibility_write("gone");
                    txttimer.setVisibility(View.VISIBLE);
                    bool=true;
                    // mHandler.postDelayed(this, 500);
                }
            };
        });


        surfaceView = (SurfaceView) findViewById(R.id.surfaceView);

        lnlist = (LinearLayout) findViewById(R.id.ln_listbtn);
        framecheck=(FrameLayout) findViewById(R.id.frame_check);
        framecheck.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (!selectedIds.isEmpty()){
                    //finish();
                    CreateFragment fragment = new CreateFragment();
                    Bundle bundle = new Bundle();
                    bundle.putSerializable("arraylist",array_hashselected);
                    fragment.setArguments(bundle);
                    isMultiSelect=false;
                    is_picturetaken=false;
                    CameraDemoActivity.relativeLayout.setVisibility(View.GONE);
                    AppCompatActivity activity = (AppCompatActivity) view.getContext();

                    //FragmentTransaction ft = ((FragmentActivity)context).
                    activity.getSupportFragmentManager().beginTransaction()
                            .replace(R.id.relativecontainer,fragment,"createfrag")
                             .commit();

  /*                Mainfragment fragment = new Mainfragment();
                    Bundle bundle = new Bundle();
                    bundle.putSerializable("array",array_hashselected);
                    fragment.setArguments(bundle);

                    CameraDemoActivity.relativeLayout.setVisibility(View.GONE);
                    AppCompatActivity activity = (AppCompatActivity) view.getContext();

                    //FragmentTransaction ft = ((FragmentActivity)context).
                    activity.getSupportFragmentManager().beginTransaction()
                            .add(R.id.relativecontainer,fragment,"mainfragment")
                            .addToBackStack("mainfragment").commit();    */            }
            }
        });

        txtcount = (TextView) findViewById(R.id.txt_count);

        relativeLayout=(RelativeLayout)findViewById(R.id.relativeone);
        // relativeLayout.setVisibility(View.VISIBLE);

        recyclerView=(RecyclerView)findViewById(R.id.recyclerview);
        recyclerView.setLayoutManager
                (new LinearLayoutManager
                        (CameraDemoActivity.this, LinearLayoutManager.HORIZONTAL, false));
        adapter=new ImageAdapter(CameraDemoActivity.this,array_imahges);
        recyclerView.setAdapter(adapter);
        recyclerView.addOnItemTouchListener
                (new RecyclerItemClickListener(this, recyclerView,
                        new RecyclerItemClickListener.OnItemClickListener() {
                            @Override
                            public void onItemClick(View view, int position) {
                                if (isMultiSelect){
                                    Log.e("select","first");
                                    //if multiple selection is enabled then select item on single click else perform normal click on item.
                                    multiSelect(position);
                                }else {
                                    if (array_imahges.get(position).containsKey("image")){
                                        //bundle.putString("image",array_imahges.get(position).get("image"));
                                        callPostFragment(MEDIA_TYPE_IMAGE,
                                                array_imahges.get(position).get("image"),
                                                new File(array_imahges.get(position).get("image")).getName()
                                                );
                                    }else {
                                        //bundle.putString("image",array_imahges.get(position).get("video"));
                                        callPostFragment(MEDIA_TYPE_VIDEO,
                                                array_imahges.get(position).get("path"),
                                                new File(array_imahges.get(position).get("video")).getName());
                                    }


                                }
                            }

                            @Override
                            public void onItemLongClick(View view, int position) {
                                if (!isMultiSelect){
                                    Log.e("select","long");
                                    selectedIds = new ArrayList<>();
                                    isMultiSelect = true;

                                    // ActionMode.Callback
                                    /*if (actionMode == null){
                                        actionMode = startActionMode(CameraDemoActivity.this); //show ActionMode.
                                    }*/
                                }
                                multiSelect(position);
                            }
                        }));


        surfaceHolder = surfaceView.getHolder();
        surfaceHolder.addCallback(this);
        flipCamera.setOnClickListener(this);
       // captureImage.setOnClickListener(this);
        flashCameraButton.setOnClickListener(this);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);

        if (Camera.getNumberOfCameras() > 1) {
            flipCamera.setVisibility(View.VISIBLE);
        }
        if (!getBaseContext().getPackageManager().hasSystemFeature(
                PackageManager.FEATURE_CAMERA_FLASH)) {
            flashCameraButton.setVisibility(View.GONE);
        }
       // fetchallimages();
    }

    private void callPostFragment(String mediaType, String imgPath, String fileName) {
        //Put the value
        //Log.e("imgPath",imgPath+" "+fileName);
        Fragment ldf = new CreateFragment();
        Bundle args = new Bundle();
        args.putString(MEDIA_TYPE, mediaType);
        args.putString(MEDIA_FILE_PATH, imgPath);
        args.putString(MEDIA_FILE_NAME, fileName);

        isMultiSelect=false;
        CameraDemoActivity.relativeLayout.setVisibility(View.GONE);

        ldf.setArguments(args);
        getSupportFragmentManager().beginTransaction()
                .replace(R.id.relativecontainer, ldf).commit();
    }


    private void video_playingvisibility(){
        lnlist.setVisibility(View.GONE);
    }

    private boolean prepareMediaRecorder(){
        mediaRecorder = new MediaRecorder();
        try{
            if(isCameraFlashOn){  // camera flash on off
                setFlashOnOff(true);
            }
            camera.unlock();
        }catch(Exception e){
            //e.printStackTrace();
        }


        mediaRecorder.setCamera(camera);
        mediaRecorder.setAudioSource(MediaRecorder.AudioSource.CAMCORDER);
        mediaRecorder.setVideoSource(MediaRecorder.VideoSource.CAMERA);
        mediaRecorder.setProfile(CamcorderProfile.get(1, CamcorderProfile.QUALITY_HIGH));
        mediaRecorder.setOutputFile("/sdcard/" + getFileName_CustomFormat() + ".mp4");

        // mediaRecorder.setOutputFile(saveUrl);
        //mediaRecorder.setOutputFormat(MediaRecorder.OutputFormat.THREE_GPP);
        mediaRecorder.setPreviewDisplay(surfaceView.getHolder().getSurface());
        mediaRecorder.setOrientationHint(setOrientationHint);
        try {
            mediaRecorder.prepare();
         } catch (IllegalStateException e) {
            e.printStackTrace();
            releaseMediaRecorder();
            return false;
        } catch (IOException e) {
            e.printStackTrace();
            releaseMediaRecorder();
            return false;
        }
        return true;
    }

    //"/sdcard/" + getFileName_CustomFormat() + ".mp4"

    private String getFileName_CustomFormat() {
        SimpleDateFormat sdfDate = new SimpleDateFormat("yyyy-MM-dd HH_mm_ss");
        Date now = new Date();
        String strDate = sdfDate.format(now);
        path_video="/sdcard/"+strDate + ".mp4";
        return strDate;
    }

    public void startVideoRecordingTimer(){
        setupCountDown();
        countDownTimer.start();
    }

    private void startVideoRecording() {
        // TODO Auto-generated method stub
        // check if video recording already started
        if(recording){
            stopVideoRecording(); // stop recording and release camera
            stopRecordingTimer();
        }else{
            Log.d("preparedMediaRecorder", prepareMediaRecorder()+"");
            if(!prepareMediaRecorder()){
                Toast.makeText(CameraDemoActivity.this,
                        "Fail in prepareMediaRecorder()!\n - Ended -",
                        Toast.LENGTH_LONG).show();
                finish();
            }
            startVideoRecordingTimer();
            //Invisible view after start recording
           flipCamera.setVisibility(View.INVISIBLE);
           flashCameraButton.setVisibility(View.INVISIBLE);
            mediaRecorder.start();
            recording = true;
        }
    }

    /* this function is  not in working now
     * use to pause or resume video when recording  */

    private void playPauseVideoRecording() {
        if(isVideoRecordingPause){
            isVideoRecordingPause=false;
            //playPauseButton.setBackgroundResource(R.drawable.ic_play_circle_outline_white_48dp);
            mediaRecorder.reset();
            stopRecordingTimer();
        }else{
            isVideoRecordingPause=true;
           // playPauseButton.setBackgroundResource(R.drawable.ic_pause_circle_outline_white_48dp);
        }
    }
    // stop recording timer when stop video record
    private void stopRecordingTimer(){
        countDownTimer.cancel();
    }
    // set camera flash on off
    private void setFlashOnOff(boolean flash){
        if(flash){
            Parameters params = camera.getParameters();
            params.setFlashMode(Parameters.FLASH_MODE_TORCH);
            camera.setParameters(params);
        }else{
            Parameters params = camera.getParameters();
            params.setFlashMode(Parameters.FLASH_MODE_OFF);
            camera.setParameters(params);
        }
    }
    // stop video recording
    public void stopVideoRecording(){
        //Invisible view after start recording
        try{
           // cameraSwitchButton.setVisibility(View.VISIBLE);
           // flashButton.setVisibility(View.VISIBLE);
            mediaRecorder.stop();   // stop the recording
            releaseMediaRecorder(); // release the MediaRecorder object\
            if(isFlashAvailable(this))
                setFlashOnOff(false);
            //releaseCamera();
            recording = false;
            stopRecordingTimer();
          /*
           * intent use here to send back response to activity that start this current activity
           */
           /* Intent returnIntent = new Intent();
            // check that video save url coming from previous activity or not
            if(!isUrl){
                returnIntent.putExtra("url", saveUrl);
            }
            returnIntent.putExtra("videoDuration", videoDuration);
            setResult(1, returnIntent);
            finish();*/

        }catch(Exception e){
            e.printStackTrace();
        }

    }

    public void checkIntentExtra(){ // this function check  intent has extras values or not
        extras=getIntent().getExtras();
        if (extras != null) {
            if (extras.containsKey("time")) {
                startVideoRecordingTime = (extras.getInt("time")+1)*1000;
                isTime=true;
                // TODO: Do something with the value of isNew.
            }
            if(extras.containsKey("url")){
                saveUrl=extras.getString("url");
                isTime=false;
                isUrl=true;
                Log.d("saveUrl", saveUrl);
            }
        }else{
            Log.d("extras is null",extras+"");
            isTime=false;
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        releaseCamera();
        //myCameraSurfaceView.onPauseMySurfaceView();
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (camera!=null){
            camera.open();
        }
    }

    private void releaseMediaRecorder() {
        if (mediaRecorder != null) {
            mediaRecorder.reset();   // clear recorder configuration
            mediaRecorder.release();  // release the recorder object
            mediaRecorder = null;
            camera.lock();       // lock camera for later use
        }
    }

  /*    private void releaseCamera(){
        if (camera != null){
            camera.release();    // release the camera for other applications
            camera = null;
        }
    }*/

  /*
    @Override
    public void onBackPressed()
    {
        if(recording){
            stopVideoRecording();
        }else{
            releaseCamera();
            finish();
        }
        Log.d("onBackPressed","yes");
        // finish();
    }
   */

    // function setup countdown timer for video recording

    private void setupCountDown() {
        Log.d("startVideoRecordingTime", startVideoRecordingTime+"");
        countDownTimer = new CountDownTimer(startVideoRecordingTime, 1000) {
            public void onTick(long millisUntilFinished) {
                // check that video recording time coming previous activity or not
                if(!isTime){
                    // if time not coming from previous activity then time will increase
                    timeElapsed = startVideoRecordingTime - millisUntilFinished;
                    // Get video duration
                    videoDuration= String.format("%d :%d ",
                            TimeUnit.MILLISECONDS.toMinutes(timeElapsed),
                            TimeUnit.MILLISECONDS.toSeconds(timeElapsed) -
                                    TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes(timeElapsed)));
                    showRecordingTime(timeElapsed);
                }else{
                    timeElapsed = startVideoRecordingTime - millisUntilFinished;
                    // Get video duration
                    videoDuration= String.format("%d :%d ",
                            TimeUnit.MILLISECONDS.toMinutes(timeElapsed),
                            TimeUnit.MILLISECONDS.toSeconds(timeElapsed) -
                                    TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes(timeElapsed)));
                    showRecordingTime(millisUntilFinished);
                }
            }
            public void onFinish() {
                Log.d("finish", "yes");
                stopVideoRecording();
            }
        };
    }

    public void showRecordingTime(long time){
        Log.d("startVideoRecordingTime", startVideoRecordingTime+"");
        txttimer.setText(""+ String.format("%d :%d ",
                TimeUnit.MILLISECONDS.toMinutes(time),
                TimeUnit.MILLISECONDS.toSeconds(time) - TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes(time))));
    }
    /*
     * @return true if a flash is available, false if not
     */
    public static boolean isFlashAvailable(Context context) {
        return context.getPackageManager().hasSystemFeature(PackageManager.FEATURE_CAMERA_FLASH);
    }

    ///////////////////////////////////////////////////////////////////////////////////////////

    void setVisibility(Boolean value){
        if (value){
            is_picturetaken=true;
            btn_next.setVisibility(View.GONE);
            btn_check.setVisibility(View.VISIBLE);
            btncancel.setVisibility(View.VISIBLE);
            lnlist.setVisibility(View.GONE);

            txthint.setVisibility(View.GONE);
            flipCamera.setVisibility(View.GONE);
            flashCameraButton.setVisibility(View.GONE);
            captureImage.setVisibility(View.GONE);
        }else {
            is_picturetaken=false;
            // camera.startPreview();
            btn_next.setVisibility(View.GONE);
            btncancel.setVisibility(View.GONE);
            btncancel.setVisibility(View.GONE);

            txthint.setVisibility(View.VISIBLE);
            lnlist.setVisibility(View.VISIBLE);
            flipCamera.setVisibility(View.VISIBLE);
            flashCameraButton.setVisibility(View.VISIBLE);
            captureImage.setVisibility(View.VISIBLE);
        }
    }

    private void multiSelect(int position) {
        try{
            Log.e("image",""+position);
            HashMap<String,String> hashMap;
                if (selectedIds.contains(array_imahges.get(position).get("id"))){
                    selectedIds.remove(array_imahges.get(position).get("id"));

                    Log.e("arrayhash",""+array_hashselected);

                    for (int i=0;i<array_hashselected.size();i++){
                        if (array_hashselected.get(i).get("id")
                                .equalsIgnoreCase(array_imahges.get(position).get("id"))){
                            array_hashselected.remove(i);
                        }
                    }
                    Log.e("arraysel",""+array_hashselected);
/*                    if (array_hashselected.contains(array_imahges.get(position).get("id")));
                    {
                        array_hashselected.remove(array_imahges.get(position).get("id"));
                        Log.e("arrayhash2",""+array_hashselected);
                    }*/

                    /* if (!selectedimages.isEmpty())
                    if (array_imahges.get(position).containsKey("image")){
                        selectedimages.remove(array_imahges.get(position).get("image"));
                    }else {
                        selectedimages.remove(array_imahges.get(position).get("video"));
                        //array_videos.remove(array_imahges.get(position).get("video"));
                    }*/

                }else{
                     selectedIds.add(array_imahges.get(position).get("id"));

                     hashMap=new HashMap<>();
                     hashMap.put("id",array_imahges.get(position).get("id"));
                     hashMap.put("path",array_imahges.get(position).get("path"));

                 if (array_imahges.get(position).containsKey("image")){
                     hashMap.put("image",array_imahges.get(position).get("image"));
                 }else {
                     hashMap.put("video",array_imahges.get(position).get("video"));
                 }
                  //Integer.parseInt(array_imahges.get(position).get("id")),
                 array_hashselected.add(hashMap);
                 //hashMapHashMap.put(array_imahges.get(position).get("id"),array_hashselected);
                }

                Log.e("arrayhashed","ids"+array_hashselected);
                Log.e("select","iamges"+array_imahges.get(position).get("id"));
                Log.e("select","ids"+selectedIds);

                if (selectedIds.size() > 0){
                    txtcount.setText(""+selectedIds.size());
                }else {
                    txtcount.setText("");
                    isMultiSelect=false;
                }

                adapter.setSelectedIds(selectedIds);
                //adapter.notifyDataSetChanged();
                //}
        }catch (Exception e){
             Log.e("exception",""+e);
        }
    }

    @Override
    public void onBackPressed() {
        Log.e("called","backpressed");

        if (isMultiSelect==true){
            txtcount.setText("");
            Log.e("multiselct","first");
            selectedIds.clear();
            selectedimages.clear();
            adapter.setSelectedIds(selectedIds);
            isMultiSelect=false;
        }else {
            if (is_picturetaken){
                Log.e("multiselct","picture");
                setVisibility(false);

                if (camera==null){
                    openCamera(CameraInfo.CAMERA_FACING_BACK);
                }
                camera.startPreview();
                is_picturetaken=false;
                setvisibility_write("gone");
                imgvideo.setVisibility(View.GONE);
                txttimer.setVisibility(View.GONE);
            }else {
                NavDrawerActivity.frame_bottom.setVisibility(View.VISIBLE);
                super.onBackPressed();
            }

        }
    }

    private class LongOperation extends AsyncTask<String, Void, ArrayList<HashMap<String,String>>> {

        @Override
        protected ArrayList<HashMap<String,String>> doInBackground(String... params) {
             fetchbothmedias();
             //fetch_onlyimages();
            return array_imahges;
        }

        @Override
        protected void onPostExecute(ArrayList<HashMap<String,String>> arrayList) {
            //adapter=new ImageAdapter(CameraDemoActivity.this,arrayList);
            //recyclerView.setAdapter(adapter);
             adapter.notifyDataSetChanged();
        }

        @Override
        protected void onPreExecute() {}
    }

    private void fetchbothmedias() {
        array_imahges.clear();
        try {
            String[] projection = {
                    MediaStore.Files.FileColumns._ID,
                    MediaStore.Files.FileColumns.DATA,
                    MediaStore.Files.FileColumns.DATE_ADDED,
                    MediaStore.Files.FileColumns.MEDIA_TYPE,
                    MediaStore.Files.FileColumns.MIME_TYPE,
                    MediaStore.Files.FileColumns.TITLE
            };
            // Return only video and image metadata.
            String selection =
                      MediaStore.Files.FileColumns.MEDIA_TYPE + "="
                    + MediaStore.Files.FileColumns.MEDIA_TYPE_IMAGE
                    + " OR "
                    + MediaStore.Files.FileColumns.MEDIA_TYPE + "="
                    + MediaStore.Files.FileColumns.MEDIA_TYPE_VIDEO;

            Uri queryUri = MediaStore.Files.getContentUri("external");

            Cursor cursor = getContentResolver().query(
                    queryUri,
                    projection,
                    selection,
                    null, // Selection args (none).
                    MediaStore.Files.FileColumns.DATE_ADDED + " DESC" // Sort order.
            );

            //Cursor cursor = cursorLoader.loadInBackground();
            int image_column_index = cursor.getColumnIndex(MediaStore.Files.FileColumns._ID);

            this.count = cursor.getCount();
            //this.thumbnails = new Bitmap[this.count];
            //this.arrPath = new String[this.count];
            HashMap<String, String> hashMap;
            int i=0;
            for (i = 0; i < 100; i++) {
                cursor.moveToPosition(i);

                int id = cursor.getInt(image_column_index);
                int dataColumnIndex = cursor.getColumnIndex(MediaStore.Files.FileColumns.DATA);
                int type = cursor.getColumnIndex(MediaStore.Files.FileColumns.MEDIA_TYPE);

                String t = String.valueOf(cursor.getInt(type));
                //int thumb =cursor.getColumnIndexOrThrow(Images.Thumbnails.DATA);
                hashMap = new HashMap<>();
                hashMap.put("path", cursor.getString(dataColumnIndex));
                hashMap.put("id", "" + i);
                hashMap.put("type", "" + cursor.getInt(type));
                String result = "";
                //Log.e("type",""+t);
                if (t.equals("1")){
                   // Log.e("image","image"+result);
                    Cursor cursor2 = MediaStore.Images
                            .Thumbnails.queryMiniThumbnail(getContentResolver(), id,
                            Images.Thumbnails.MINI_KIND, null);
                    if (cursor2 != null && cursor2.getCount() > 0) {
                        cursor2.moveToFirst();
                        result = cursor2
                                .getString(cursor2
                                        .getColumnIndexOrThrow(MediaStore.Images.Thumbnails.DATA));
                        cursor2.close();
                    }

                    if (!result.isEmpty() && result!=null){
                        Log.e("image",""+result);
                        hashMap.put("image", result);
                        array_imahges.add(hashMap);
                    }
                }else if(t.equals("3")) {
                    String thumbd = getThumbnailPathForLocalFile(id);
                    if (thumbd != null && !thumbd.isEmpty()) {
                        hashMap.put("video", thumbd);
                        array_imahges.add(hashMap);
                        Log.e("thumb", "" + thumbd);
                    }
                }else {

                }
                if (i==50 ||i==25){
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            adapter.notifyDataSetChanged();
                        }
                    });
                }
            }

            Log.e("arryyguyg",""+array_imahges.size());
            Log.e("arryyg",""+array_imahges);
        }catch (Exception e){

        }finally {
            if( cursor != null && !cursor.isClosed())
                cursor.close();
        }
       /* adapter=new ImageAdapter(this,array_imahges);
        recyclerView.setAdapter(adapter);
       */
    }

    private void fetch_onlyimages(){
        array_imahges=new ArrayList<>();
        String[] projection = {MediaStore.Images.Thumbnails._ID};
        final String orderBy = Images.Media.DATE_TAKEN;
        cursor = getContentResolver().query(MediaStore.Images.Thumbnails.EXTERNAL_CONTENT_URI,
                projection,  // Which columns to return
                null,       // Return all rows
                null,
                Images.Thumbnails.DEFAULT_SORT_ORDER);
        columnIndex = cursor.getColumnIndexOrThrow(MediaStore.Images.Thumbnails._ID);

        HashMap<String,String> hashMap;
        for (int i=0;i<50;i++)
        {
            hashMap=new HashMap<>();
            cursor.moveToPosition(i);
            int imageID = cursor.getInt(columnIndex);

            Log.e("imageid",""+imageID);
            Log.e("iameg",""+Uri.withAppendedPath(MediaStore.Images.Thumbnails.EXTERNAL_CONTENT_URI,"" + imageID));

            hashMap.put("image",""+Uri.withAppendedPath(MediaStore.Images.Thumbnails.EXTERNAL_CONTENT_URI,"" + imageID));
            hashMap.put("id",""+i);
            array_imahges.add(hashMap);
        }
        Collections.reverse(array_imahges);
        Log.e("imagesonly",""+array_imahges);
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                adapter.notifyDataSetChanged();
            }
        });
    }

    public static String[] thumbColumns = { MediaStore.Video.Thumbnails.DATA };

    public String getThumbnailPathForLocalFile(int fileId) {
        MediaStore.Video.Thumbnails.getThumbnail(getContentResolver(),
                fileId, MediaStore.Video.Thumbnails.MICRO_KIND, null);

        Cursor thumbCursor = null;
        try {
            thumbCursor = getContentResolver().query(
                    MediaStore.Video.Thumbnails.EXTERNAL_CONTENT_URI,
                    thumbColumns, MediaStore.Video.Thumbnails.VIDEO_ID + " = "
                            + fileId, null, null);

            if (thumbCursor.moveToFirst()) {
                String thumbPath = thumbCursor.getString(thumbCursor
                        .getColumnIndex(MediaStore.Video.Thumbnails.DATA));

                return thumbPath;
            }

        } finally {
            if( thumbCursor != null && !thumbCursor.isClosed())
                thumbCursor.close();
        }

        return null;
    }

    public String BitMapToString(Bitmap bitmap){
        ByteArrayOutputStream baos=new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.PNG,100, baos);
        byte [] b=baos.toByteArray();
        String temp= Base64.encodeToString(b, Base64.DEFAULT);
        return temp;
    }

    @Override
    public void surfaceCreated(SurfaceHolder holder) {
        if (!openCamera(CameraInfo.CAMERA_FACING_BACK)) {
            alertCameraDialog();
        }
    }

    private boolean openCamera(int id) {
        boolean result = false;
        cameraId = id;
        releaseCamera();
        try {
            camera = Camera.open(cameraId);
        } catch (Exception e) {
            e.printStackTrace();
        }
        if (camera != null) {
            try {
                setUpCamera(camera);
                camera.setErrorCallback(new ErrorCallback() {
                    @Override
                    public void onError(int error, Camera camera) {
                       Log.e("print error","tretrt"+error);
                    }
                });
                camera.setPreviewDisplay(surfaceHolder);
                camera.startPreview();
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
                    //set camera to continually auto-focus


                   Camera.Parameters params = camera.getParameters();

                  // params.setFocusMode(Camera.Parameters.FOCUS_MODE_CONTINUOUS_PICTURE);

                    List<Camera.Size> allSizes = params.getSupportedPictureSizes();
                    Camera.Size size = allSizes.get(0); // get top size
                    for (int i = 0; i < allSizes.size(); i++) {
                        if (allSizes.get(i).width > size.width)
                            size = allSizes.get(i);
                    }
                    params.setPictureSize(size.width, size.height);
                    camera.setParameters(params);
                }
                result = true;
            } catch (IOException e) {
                e.printStackTrace();
                result = false;
                releaseCamera();
            }
        }
        return result;
    }

    private void setUpCamera(Camera c) {
        Camera.CameraInfo info = new Camera.CameraInfo();
        Camera.getCameraInfo(cameraId, info);
        rotation = getWindowManager().getDefaultDisplay().getRotation();
        int degree = 0;
        switch (rotation) {
            case Surface.ROTATION_0:
                degree = 0;
                break;
            case Surface.ROTATION_90:
                degree = 90;
                break;
            case Surface.ROTATION_180:
                degree = 180;
                break;
            case Surface.ROTATION_270:
                degree = 270;
                break;

            default:
                break;
        }

        if (info.facing == Camera.CameraInfo.CAMERA_FACING_FRONT) {
            // frontFacing
            rotation = (info.orientation + degree) % 330;
            rotation = (360 - rotation) % 360;
        } else {
            // Back-facing
            rotation = (info.orientation - degree + 360) % 360;
        }
        c.setDisplayOrientation(rotation);
        Parameters params = c.getParameters();

        showFlashButton(params);

        List<String> focusModes = params.getSupportedFlashModes();
        if (focusModes != null) {
            if (focusModes
                    .contains(Camera.Parameters.FOCUS_MODE_CONTINUOUS_PICTURE)) {
                params.setFlashMode(Camera.Parameters.FOCUS_MODE_CONTINUOUS_PICTURE);
            }
        }

        params.setRotation(rotation);
    }

    private void showFlashButton(Parameters params) {
        boolean showFlash = (getPackageManager().hasSystemFeature(
                PackageManager.FEATURE_CAMERA_FLASH) && params.getFlashMode() != null)
                && params.getSupportedFlashModes() != null
                && params.getSupportedFocusModes().size() > 1;

        flashCameraButton.setVisibility(showFlash ? View.VISIBLE
                : View.INVISIBLE);
    }

    private void releaseCamera() {
        try {
            if (camera != null) {
                camera.setPreviewCallback(null);
                camera.setErrorCallback(null);
                camera.stopPreview();
                camera.release();
                camera = null;
            }
        } catch (Exception e) {
            e.printStackTrace();
            Log.e("error", e.toString());
            camera = null;
        }
    }

    @Override
    public void surfaceChanged(SurfaceHolder holder, int format, int width,
                               int height) {

    }

    @Override
    public void surfaceDestroyed(SurfaceHolder holder) {

    }

    private void setvisibility_write(String act){
        LinearLayout lnwrite=(LinearLayout)findViewById(R.id.ln_write);
        if (act.equalsIgnoreCase("gone")){
            lnwrite.setVisibility(View.GONE);

        }else {
            lnwrite.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.flash:
                flashOnButton("");
                break;
            case R.id.flipCamera:
                flipCamera();
                break;
            case R.id.captureImage:
                Log.e("clicked","onetime");
                //startVideoRecording();
                // takeImage();
                break;

            default:
                break;
        }
    }


    /*

    ByteArrayOutputStream stream = new ByteArrayOutputStream();
    bmp.compress(Bitmap.CompressFormat.PNG, 100, stream);
    byte[] byteArray = stream.toByteArray();

    Intent in1 = new Intent(this, Activity2.class);
    in1.putExtra("image",byteArray);
    Then in Activity 2:

    byte[] byteArray = getIntent().getByteArrayExtra("image");
    Bitmap bmp = BitmapFactory.decodeByteArray(byteArray, 0, byteArray.length);
*/

    private void sendtoMainfrag(String filepath){

      Mainfragment fragment = new Mainfragment();
                    Bundle bundle = new Bundle();
                    bundle.putString("image",filepath);
                    fragment.setArguments(bundle);

                    Log.e("start","intent"+filepath);
                    CameraDemoActivity.relativeLayout.setVisibility(View.GONE);
                    AppCompatActivity activity = (AppCompatActivity)CameraDemoActivity.this;

                    //FragmentTransaction ft = ((FragmentActivity)context).
                    activity.getSupportFragmentManager().beginTransaction()
                            .add(R.id.relativecontainer,fragment,"mainfragment")
                            .addToBackStack("mainfragment").commit();

    }

    private void takeImage() {
        //Log.e("pivcture taken","called");
        setVisibility(true);
        setvisibility_write("gone");
        camera.takePicture(null, null, new PictureCallback() {
            private File imageFile;
            @Override
            public void onPictureTaken(final byte[] data, final Camera camera) {
                try {
                    camera.stopPreview();
                    Dialogs.showProDialog(CameraDemoActivity.this,"saving..");
                    Log.e("pivcture taken","ztaetrs");
                    // convert byte array into bitmap
                    new Thread(new Runnable() {
                        @Override
                        public void run() {
                            Bitmap loadedImage = null;
                            Bitmap rotatedBitmap = null;
                            loadedImage = BitmapFactory.decodeByteArray(data, 0,
                                    data.length);
                            Log.e("pivcture taken",""+loadedImage);
                            // rotate Image
                            Matrix rotateMatrix = new Matrix();
                            rotateMatrix.postRotate(rotation);
                            rotatedBitmap = Bitmap.createBitmap(loadedImage, 0, 0,
                                    loadedImage.getWidth(), loadedImage.getHeight(),
                                    rotateMatrix, false);
                            String state = Environment.getExternalStorageState();
                            File folder = null;
                            if (state.contains(Environment.MEDIA_MOUNTED)) {
                                folder = new File(Environment
                                        .getExternalStorageDirectory() + "/Demo");
                            } else {
                                folder = new File(Environment
                                        .getExternalStorageDirectory() + "/Demo");
                            }

                            boolean success = true;
                            if (!folder.exists()) {
                                success = folder.mkdirs();
                            }
                            if (success) {
                                Date date = new Date();
                                filename=new Timestamp(date.getTime()).toString();
                                imageFile = new File(folder.getAbsolutePath()
                                        + File.separator
                                        + filename
                                        + ".jpg");

                                try {
                                    imageFile.createNewFile();
                                } catch (IOException e) {
                                    e.printStackTrace();
                                }
                            } else {
                                Toast.makeText(getBaseContext(), "Image Not saved",
                                        Toast.LENGTH_SHORT).show();
                                return;
                            }
                            ByteArrayOutputStream ostream = new ByteArrayOutputStream();

                            // save image into gallery
                            rotatedBitmap.compress(CompressFormat.JPEG, 100, ostream);

                            FileOutputStream fout = null;
                            try {
                                fout = new FileOutputStream(imageFile);
                                fout.write(ostream.toByteArray());
                                fout.close();
                                ContentValues values = new ContentValues();

                                values.put(Images.Media.DATE_TAKEN,
                                        System.currentTimeMillis());
                                values.put(Images.Media.MIME_TYPE, "image/jpeg");
                                values.put(MediaStore.MediaColumns.DATA,
                                        imageFile.getAbsolutePath());

                                CameraDemoActivity.this.getContentResolver().insert(
                                        Images.Media.EXTERNAL_CONTENT_URI, values);

                                Log.e("imagepath",""+imageFile.getAbsolutePath());
                                Dialogs.dismissDialog();
                                mediatype="image";
                                mediapath=imageFile.getAbsolutePath();

                                Log.e("mediapath",""+mediapath);
                                Log.e("mediapath2",""+imageFile);
                              //  callPostFragment(MEDIA_TYPE_IMAGE,imageFile.getAbsolutePath(),
                              //  imageFile.getAbsolutePath());
                            } catch (FileNotFoundException e) {
                                e.printStackTrace();
                            }catch (Exception e){

                            }

                        }
                    }).start();


               /*     Mainfragment fragment = nenw Mainfragment();
                    Bundle bundle = new Bundle();
                    bundle.putString("image",imageFile.getAbsolutePath());
                    fragment.setArguments(bundle);

                    Log.e("start","intent"+imageFile.getAbsolutePath());
                    CameraDemoActivity.relativeLayout.setVisibility(View.GONE);
                    AppCompatActivity activity = (AppCompatActivity)CameraDemoActivity.this;

                    //FragmentTransaction ft = ((FragmentActivity)context).
                    activity.getSupportFragmentManager().beginTransaction()
                            .add(R.id.relativecontainer,fragment,"mainfragment")
                            .addToBackStack("mainfragment").commit();
       */
                } catch (Exception e) {
                    e.printStackTrace();
                }

            }
        });
    }

    private void flipCamera() {
        int id = (cameraId == CameraInfo.CAMERA_FACING_BACK ? CameraInfo.CAMERA_FACING_FRONT
                : CameraInfo.CAMERA_FACING_BACK);
        if (!openCamera(id)) {
            alertCameraDialog();
        }
    }

    private void alertCameraDialog() {
        AlertDialog.Builder dialog = createAlert(CameraDemoActivity.this,
                "Camera info", "error to open camera");
        dialog.setNegativeButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();

            }
        });

        dialog.show();
    }

    private Builder createAlert(Context context, String title, String message) {
        AlertDialog.Builder dialog = new AlertDialog.Builder(
                new ContextThemeWrapper(context,
                        android.R.style.Theme_Holo_Light_Dialog));
        if (title != null)
        dialog.setTitle(title);
        else
        dialog.setTitle("Information");
        dialog.setMessage(message);
        dialog.setCancelable(false);
        return dialog;

    }

    private void flashOnButton(String action) {
        if (camera != null) {
            try {
                Parameters param = camera.getParameters();
                param.setFlashMode(!flashmode ? Parameters.FLASH_MODE_TORCH
                        : Parameters.FLASH_MODE_OFF);
                camera.setParameters(param);
                flashmode = !flashmode;

                if (flashmode){
                    flashCameraButton.setBackgroundResource(R.drawable.flash_on);
                }else {
                    flashCameraButton.setBackgroundResource(R.drawable.flash_off);
                }

                if (action.equalsIgnoreCase("auto")){
                    List<String> flashModes = param.getSupportedFlashModes();
                    if (flashModes.contains(android.hardware.Camera.Parameters.FLASH_MODE_AUTO))
                    {
                        param.setFlashMode(Parameters.FLASH_MODE_AUTO);
                    }
                }

            } catch (Exception e) {
                // TODO: handle exception
            }

        }
    }


  /*    @Override
    public boolean onCreateActionMode(ActionMode actionMode, Menu menu) {
        return false;
    }

    @Override
    public boolean onPrepareActionMode(ActionMode actionMode, Menu menu) {
        return false;
    }

    @Override
    public boolean onActionItemClicked(ActionMode actionMode, MenuItem menuItem) {
        return false;
    }

    @Override
    public void onDestroyActionMode(ActionMode actionMode) {
        actionMode = null;
        isMultiSelect = false;
        selectedIds = new ArrayList<>();
        adapter.setSelectedIds(new ArrayList<String>());
    }*/

   /* @Override
    public void respond(String type) {
        Newsdetail_fragment frag = (MainFra) getSupportFragmentManager().findFragmentByTag("tempfrag");
        if (frag != null)
            frag.is_bookmark();
    }*/
}
