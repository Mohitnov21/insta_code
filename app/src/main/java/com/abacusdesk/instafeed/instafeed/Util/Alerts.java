package com.abacusdesk.instafeed.instafeed.Util;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.provider.Settings;
import android.support.v7.app.AlertDialog;
import android.util.Log;

import com.abacusdesk.instafeed.instafeed.R;


/**
 * Created by root on 7/1/17.
 */

public class Alerts {

    public static void internetConnectionErrorAlert(final Context context, DialogInterface.OnClickListener onClickTryAgainButton) {

        try {
            new AlertDialog.Builder(context)
                    .setCancelable(false)
                    .setIconAttribute(android.R.attr.alertDialogIcon)
                    .setTitle(context.getString(R.string.tv_no_internet_connection))
                    .setMessage(context.getString(R.string.tv_check_net))
                    .setPositiveButton(context.getString(R.string.tv_try_again), onClickTryAgainButton)
                    .setNegativeButton(context.getString(R.string.cancel), new DialogInterface.OnClickListener() {
                        @Override
                       public void onClick(DialogInterface dialog, int which) {
                       dialog.cancel();
        /*                    Intent i = new Intent(Settings.ACTION_SETTINGS);
                            ((Activity) context).startActivityForResult(i, 0);*/
                        }
                    })
                    .show();
        }catch (Exception e){

        }

    }

    public static void timeoutErrorAlert(Context context, DialogInterface.OnClickListener onClickTryAgainButton) {
        try {
            new AlertDialog.Builder(context)
                    .setCancelable(false)
                    .setIconAttribute(android.R.attr.alertDialogIcon)
                    .setTitle(context.getString(R.string.tv_network_error))
                    .setMessage(context.getString(R.string.tv_check_net))
                    .setPositiveButton(context.getString(R.string.tv_try_again), onClickTryAgainButton)
                    .show();
        }  catch (Exception e){
            Log.e("exeception occured",""+e);;

        }
    }

    public  static void showRetryDialog1(Activity context, DialogInterface.OnClickListener onClickTryAgainButton )
    {
        final android.support.v7.app.AlertDialog.Builder alertDialogBuilder = new android.support.v7.app.AlertDialog.Builder(context);
        alertDialogBuilder.setTitle(context.getString(R.string.tv_network_error));
        alertDialogBuilder.setMessage(context.getString(R.string.tv_check_net));
        alertDialogBuilder.setCancelable(false);
        alertDialogBuilder.setIconAttribute(android.R.attr.alertDialogIcon);

        // set positive button: Yes message
        alertDialogBuilder.setPositiveButton(context.getString(R.string.retry),onClickTryAgainButton);

        android.support.v7.app.AlertDialog alertDialog = alertDialogBuilder.create();
        // show alert
        alertDialog.show();
    }
    public static void alertBeforeDelete(final Activity context, DialogInterface.OnClickListener onClickTryAgainButton, String message) {

        new AlertDialog.Builder(context)
                .setCancelable(false)
                .setIconAttribute(android.R.attr.alertDialogIcon)
                .setTitle(context.getString(R.string.tv_alert))
                .setMessage(message)
                .setPositiveButton(context.getString(R.string.yes), onClickTryAgainButton)
                .setNegativeButton(context.getString(R.string.no), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                       dialog.dismiss();
                    }
                })
                .show();
    }
    public static void viewLoginDialog(final Activity context, DialogInterface.OnClickListener onClickTryAgainButton, String message) {

        new AlertDialog.Builder(context)
                .setCancelable(false)
                .setIconAttribute(android.R.attr.dialogMessage)
                .setTitle(context.getString(R.string.message))
                .setMessage(message)
                .setPositiveButton(context.getString(R.string.ok), onClickTryAgainButton)
                .show();
    }
}
