package com.abacusdesk.instafeed.instafeed.fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.abacusdesk.instafeed.instafeed.NavDrawerActivity;
import com.abacusdesk.instafeed.instafeed.R;
import com.abacusdesk.instafeed.instafeed.Util.Apis;
import com.abacusdesk.instafeed.instafeed.Util.AppSingleton;
import com.abacusdesk.instafeed.instafeed.Util.CommonFunctions;
import com.abacusdesk.instafeed.instafeed.Util.SaveSharedPreference;
import com.abacusdesk.instafeed.instafeed.adapter.Gid_Adapter;

import com.abacusdesk.instafeed.instafeed.custom_dialog.Dialogs;
import com.abacusdesk.instafeed.instafeed.model.NewsModel;
import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by ishaan on 7/7/2017.
 */

public class HomeFrag_news extends Fragment implements SwipeRefreshLayout.OnRefreshListener {

    private View view;
    NewsModel newsModel;
    private static RecyclerView recyclerView;
    ArrayList<NewsModel.DataBean>newsDataBeanList=new ArrayList<>();
    Gid_Adapter adapter;
    private LinearLayoutManager layoutManager;
    private SwipeRefreshLayout swipeRefreshLayout;
    public static ViewPager viewPager;
    public String Api_temp;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_news, container, false);

        swipeRefreshLayout = (SwipeRefreshLayout)view.findViewById(R.id.swipe_refresh_layout);
        swipeRefreshLayout.setOnRefreshListener(this);
        recyclerView = (RecyclerView) view.findViewById(R.id.recyclerView);
        setRecyclerView();
        try {
            if (CommonFunctions.isConnected(getActivity())){
                getNewsfeed();
            }
        } catch (Exception e) {
           // e.printStackTrace();
        }

        view.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if (keyCode == KeyEvent.KEYCODE_BACK) {
                    getActivity().getSupportFragmentManager().popBackStack();
                    return true;
                } else {
                    return false;
                }
            }
        });
        return view;
    }

    @Override
    public void setMenuVisibility(final boolean visible) {
        super.setMenuVisibility(visible);
        //inflate_View();
            if (visible) {
                Log.e("visible","");
                NavDrawerActivity.lnaudio.setVisibility(View.VISIBLE);

            }
            else {
            NavDrawerActivity.lnaudio.setVisibility(View.GONE);
          }
     }

    @Override
    public void onRefresh() {
        swipeRefreshLayout.setRefreshing(true);
        try {
            if (CommonFunctions.isConnected(getActivity())){
                getNewsfeed();
            }
        } catch (Exception e) {
           // e.printStackTrace();
        }
        swipeRefreshLayout.setRefreshing(false);
    }

    public void getNewsfeed() {
        // Dialogs.showProDialog(getActivity(), "Loading");
        String api="";
        if (!SaveSharedPreference.getUserID(getContext()).isEmpty()){
             api=Apis.news+"/"+SaveSharedPreference.getUserID(getContext());
            Log.e("userid api","called");
         }else {
            api=Apis.news;
            Log.e("without userid api","called");
        }
        StringRequest stringRequest = new StringRequest(Request.Method.GET,api,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                       // Log.e("log", "" + response);
                        getResponse(response);
                        Dialogs.disDialog();
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                       // Toast.makeText(getActivity(),"No Internet connection!", Toast.LENGTH_SHORT).show();
                        swipeRefreshLayout.setRefreshing(false);
                    }
                })
        {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Content-Type", "application/x-www-form-urlencoded");
                return params;
            }
        };

        //RequestQueue requestQueue = Volley.newRequestQueue(getActivity());
        //requestQueue.add(stringRequest);

        AppSingleton.getInstance(getContext()).addToRequestQueue(stringRequest);
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(
                6000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

    }

    public void refresh_Adapter() {
        Log.e("change refresh","called nav");
        //adapter.notifyDataSetChanged();
        getNewsfeed();
    }

    private void getResponse(String response ){
        if (!newsDataBeanList.isEmpty()){
            newsDataBeanList.clear();
        }
        if(!response.equals(null)){
            //Log.e("log", "" + response);
            Gson gson = new Gson();
            newsModel = gson.fromJson(response, NewsModel.class);
            int status = newsModel.getStatus();
            String msg = newsModel.getMessage();
            if (msg.equalsIgnoreCase("success") && status==200) {
                if (newsModel.getData()!=null && !newsModel.getData().isEmpty()){
                    for (int i = 0; i < newsModel.getData().size(); i++) {
                        try {
                            NewsModel.DataBean dataBean = newsModel.getData().get(i);
                            newsDataBeanList.add(dataBean);
                        } catch (Exception e){
                           // e.printStackTrace();
                        }
                    }
                }
                adapter.notifyDataSetChanged();
            }
        }
    }

    // Setting recycler view
    private void setRecyclerView() {
        try{
            adapter= new Gid_Adapter(getActivity(),newsDataBeanList);
        }  catch (Exception e){
           // e.printStackTrace();
        }

        recyclerView.setHasFixedSize(true);
        layoutManager=new LinearLayoutManager(getActivity());
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setNestedScrollingEnabled(false);
        //recyclerView.addItemDecoration(new SpacesItemDecoration(0));

        layoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        recyclerView.setAdapter(adapter);
    }
}
