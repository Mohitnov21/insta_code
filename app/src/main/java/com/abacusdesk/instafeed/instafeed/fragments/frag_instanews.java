package com.abacusdesk.instafeed.instafeed.fragments;

import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.abacusdesk.instafeed.instafeed.R;
import com.abacusdesk.instafeed.instafeed.Util.Alerts;
import com.abacusdesk.instafeed.instafeed.Util.Apis;
import com.abacusdesk.instafeed.instafeed.Util.AppSingleton;
import com.abacusdesk.instafeed.instafeed.Util.CommonFunctions;
import com.abacusdesk.instafeed.instafeed.Util.SaveSharedPreference;
import com.abacusdesk.instafeed.instafeed.adapter.Recycler_newAdapter;
import com.abacusdesk.instafeed.instafeed.custom_dialog.Dialogs;
import com.abacusdesk.instafeed.instafeed.model.NewsModel;
import com.android.volley.AuthFailureError;
import com.android.volley.NoConnectionError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.gson.Gson;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by ishaan on 6/19/2017.
 */

public class frag_instanews extends Fragment{
    View view;
    RecyclerView recyclerView;
    ArrayList<NewsModel.DataBean>newsDataBeanList=new ArrayList<>();
    NewsModel newsModel;
    Recycler_newAdapter adapter;
    public  String  Api_temp="";
    Map<String, String> map ;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
         view = inflater.inflate(R.layout.frag_recycler, container, false);
         recyclerView = (RecyclerView) view.findViewById(R.id.recyclerView);

        if (Publicprofile_Fragment.flag==true){
            Api_temp=Apis.public_news;
            map=new HashMap<>();
            map.put("username",Publicprofile_Fragment.username);
            Log.e("printmapublicprofile",""+map);
        }else {
            Api_temp=Apis.private_news;
            map=new HashMap<>();
            map.put("user", SaveSharedPreference.getUserID(getActivity()));
            map.put("token",SaveSharedPreference.getPrefToken(getActivity()));
            Log.e("printmappublicprofile",""+map);
        }
        if (CommonFunctions.isConnected(getActivity())){
            if (!newsDataBeanList.isEmpty()){
                newsDataBeanList.clear();
            }
            getNewsfeed();
        }else {
            Toast.makeText(getActivity(),"Internet not available", Toast.LENGTH_SHORT).show();
        }
        setRecyclerView();
        return view;
    }


    public void getNewsfeed() {
        Dialogs.showProDialog(getActivity(), "Loading");
        StringRequest stringRequest = new StringRequest(Request.Method.POST, Api_temp,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.e("log", "" + response);
                        try {
                            getResponse(response);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        Dialogs.disDialog();
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Dialogs.disDialog();
                    }
                })
         {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                return map;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Content-Type", "application/x-www-form-urlencoded");
                return params;
            }
        };

        RequestQueue requestQueue = Volley.newRequestQueue(getActivity());
        requestQueue.add(stringRequest);
    }

    private void getResponse(String response ) throws JSONException {
        if(!response.equals(null)){
            Log.e("log", "" + response);
            JSONObject jsonObject = new JSONObject(response);
            Object object = jsonObject.get("data");
            if (object instanceof JSONArray) {
            Gson gson = new Gson();
            newsModel = gson.fromJson(response, NewsModel.class);
            int status = newsModel.getStatus();
            String msg = newsModel.getMessage();
            if (msg.equalsIgnoreCase("success") && status==200) {
                if (newsModel.getData()!=null && !newsModel.getData().isEmpty()){
                    for (int i = 0; i < newsModel.getData().size(); i++) {
                        try {
                            NewsModel.DataBean dataBean = newsModel.getData().get(i);
                            newsDataBeanList.add(dataBean);
                        } catch (Exception e){
                            e.printStackTrace();
                        }
                    }
                }
                Log.e("databen data",""+newsDataBeanList);
                adapter.notifyDataSetChanged();
                TextView txtrecord=(TextView)view.findViewById(R.id.txt_records);
                if (newsDataBeanList.isEmpty()){
                    txtrecord.setVisibility(View.VISIBLE);
                }
            }
        }
        }
    }

    // poills blogs opiunions
    // working petition talents  news
    // Setting recycler view

    private void setRecyclerView() {
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        // recyclerView.addItemDecoration(mDividerItemDecoration);
        try{
            adapter= new Recycler_newAdapter(getActivity(),newsDataBeanList);
            recyclerView.setAdapter(adapter);
            adapter.notifyDataSetChanged();
            Log.e("responseset",""+newsDataBeanList);
        }catch (Exception e){
            e.printStackTrace();
        }
    }
}
