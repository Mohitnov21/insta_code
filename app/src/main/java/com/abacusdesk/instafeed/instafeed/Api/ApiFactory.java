package Api;

import android.content.Context;

import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by user on 8/19/2016.
 */

public class ApiFactory {

    static {
        baseUrl = ApiURL.BASE_LIVE_URL;
    }

    private static String baseUrl;

    private static Retrofit.Builder builder = new Retrofit.Builder().baseUrl(baseUrl).addConverterFactory(GsonConverterFactory.create()).addCallAdapterFactory(new ErrorCallback.ErrorHandlingCallAdapterFactory());

    public static <S> S createServiceWithNewBaseURL(String newBaseUrl, Context context, Class<S> serviceClass) {
        baseUrl = newBaseUrl;
        builder = new Retrofit.Builder().baseUrl(baseUrl).addConverterFactory(GsonConverterFactory.create()).addCallAdapterFactory(new ErrorCallback.ErrorHandlingCallAdapterFactory());
        Retrofit retrofit = builder.client(NetworkUtil.addHeader(context)).build();
        return retrofit.create(serviceClass);
    }

    public static <S> S createService(Context context, Class<S> serviceClass) {
        builder = new Retrofit.Builder().baseUrl(baseUrl).addConverterFactory(GsonConverterFactory.create()).addCallAdapterFactory(new ErrorCallback.ErrorHandlingCallAdapterFactory());
        Retrofit retrofit = builder.client(NetworkUtil.addHeader(context)).build();
        return retrofit.create(serviceClass);
    }

    public static Retrofit.Builder getBuilder() {
        return builder;
    }
}
