package com.abacusdesk.instafeed.instafeed.fragments;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Typeface;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.LevelListDrawable;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.support.annotation.Nullable;
import android.support.annotation.RequiresApi;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.MenuItemCompat;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.PopupMenu;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.Toolbar;
import android.text.Html;
import android.text.Spanned;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.MediaController;
import android.widget.TextView;
import android.widget.Toast;

import com.abacusdesk.instafeed.instafeed.Login_Activity;
import com.abacusdesk.instafeed.instafeed.NavDrawerActivity;
import com.abacusdesk.instafeed.instafeed.R;
import com.abacusdesk.instafeed.instafeed.Util.Alerts;
import com.abacusdesk.instafeed.instafeed.Util.Apis;
import com.abacusdesk.instafeed.instafeed.Util.CommonFunctions;
import com.abacusdesk.instafeed.instafeed.Util.SaveSharedPreference;
import com.abacusdesk.instafeed.instafeed.adapter.Bookmark_adapter;
import com.abacusdesk.instafeed.instafeed.adapter.Explore_Adapter;
import com.abacusdesk.instafeed.instafeed.adapter.List_Adapter;
import com.abacusdesk.instafeed.instafeed.adapter.RelatedPosts_adapter;
import com.abacusdesk.instafeed.instafeed.adapter.ViewPagerAdapter;
import com.abacusdesk.instafeed.instafeed.application.Change_like;
import com.abacusdesk.instafeed.instafeed.custom_dialog.Dialogs;
import com.abacusdesk.instafeed.instafeed.model.NewsDetail;
import com.abacusdesk.instafeed.instafeed.webservice.VolleySingleton;
import com.android.volley.AuthFailureError;
import com.android.volley.NoConnectionError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.bumptech.glide.Glide;
import com.google.gson.Gson;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Timer;

import cn.jzvd.JZVideoPlayer;

/**
 * Created by ishaan on 6/11/2017.
 */

public class Newsdetail_fragment extends Fragment implements View.OnClickListener {

    View view;
    NewsDetail newsDetail;
    TextView txtdescr, txtshortdescr, txtdate, txtplace, txttitle;
    TextView txtlike, txtdislike, txtcomment;
    ImageView img_back, img_forward;
    ImageView imgshare, imgonview;
    private String catid = "", postid = "", temp, temp2;

    private String vote = "";
    ArrayList<NewsDetail.DataBean.ImagesBean> arrayImages = new ArrayList<>();
    NewsDetail.DataBean dataBean3, dataBean, dataBean2, dataBean_vid, dataBean_audio;
    public List_Adapter adapter;
    ViewPagerAdapter viewPagerAdapter;
    ViewPager imgviewpager;
    Boolean flag = false;
    private ArrayList<HashMap<String, String>> arrayviewcontainer;

    private ArrayList<HashMap<String, String>> arrayhashComent;

    ArrayList<HashMap<String,String>> array_relatedposts = new ArrayList<>();
    RelatedPosts_adapter adapterrelated;
    private EditText edcomment;
    LinearLayout lncomment, lnlocation, lnlike, lndislike, ln_userdetail;
    TextView txt_ownername;
    String location;
    Bitmap imagebit;
    ImageView img_like, imgdislike;
    String temp_related = "";
    String imagetemp = "";
    MenuItem menubookmark;
    public boolean bookmark;
    Communicator comm;
    private String likestatus="";
    Change_like change_like;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.detail_finallayout, container, false);
        init();
        NavDrawerActivity.frame_bottom.setVisibility(View.GONE);
        getActivity().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_PAN | WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);
        return view;
    }

    private void init() {
        arrayviewcontainer = new ArrayList<>();
        arrayhashComent = new ArrayList<>();

        if (!arrayviewcontainer.isEmpty()) {
            arrayviewcontainer.clear();
        }

        if (!arrayhashComent.isEmpty())
            arrayhashComent.clear();

        Bundle bundle = this.getArguments();
        if (bundle != null) {
            postid = bundle.getString("postid");
            catid = bundle.getString("catid");
        }

        comment_click();
        is_bookmark();
        temp_related = Apis.related_news.replace("id", catid);

        ln_userdetail = (LinearLayout) view.findViewById(R.id.ln_publicprofile);
        txt_ownername = (TextView) view.findViewById(R.id.txt_owner);
        txtdate = (TextView) view.findViewById(R.id.txt_date);

        img_like = (ImageView) view.findViewById(R.id.img_like);
        imgdislike = (ImageView) view.findViewById(R.id.img_dislike);

        lnlike =    (LinearLayout) view.findViewById(R.id.ln_like);
        lndislike = (LinearLayout) view.findViewById(R.id.ln_down);

        lnlike   .setOnClickListener(this);
        lndislike.setOnClickListener(this);

        imgviewpager = (ViewPager) view.findViewById(R.id.img_viewpager);
        imgshare = (ImageView) view.findViewById(R.id.img_share);
        lncomment = (LinearLayout) view.findViewById(R.id.ln_comment);

        imgonview = (ImageView) view.findViewById(R.id.img_view);

        if (NavDrawerActivity.toolbar != null) {
            NavDrawerActivity.toolbar.setLogo(null);
        }
        img_back = (ImageView) view.findViewById(R.id.img_left);
        img_forward = (ImageView) view.findViewById(R.id.img_right);

        img_back.setOnClickListener(this);
        img_forward.setOnClickListener(this);
        ln_userdetail.setOnClickListener(this);

        lnlocation = (LinearLayout)view.findViewById(R.id.ln_location);
        txtlike = (TextView)       view.findViewById(R.id.txt_like);
        txtdislike = (TextView)    view.findViewById(R.id.txt_dislike);

        txtcomment = (TextView)    view.findViewById(R.id.txt_comment);

        imgshare.setOnClickListener(this);
        //txtlike.setOnClickListener(this);
        //txtdislike.setOnClickListener(this);
        lncomment.setOnClickListener(this);
        //txtcomment.setOnClickListener(this);

        //txtname=(TextView)view.findViewById(R.i+d.txt_name);
        txtshortdescr = (TextView) view.findViewById(R.id.txt_descrshort);
        txtdescr = (TextView) view.findViewById(R.id.txt_shortdescr);

        txtplace = (TextView) view.findViewById(R.id.txt_place);
        txttitle = (TextView) view.findViewById(R.id.txt_title);

        temp = Apis.news_detail.replace("catid", catid);
        temp2 = temp.replace("postid", postid);

        view.setFocusableInTouchMode(true);
        view.requestFocus();

        set_relatedPosts();

       /* view.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if (keyCode == KeyEvent.KEYCODE_BACK) {

                    viewPagerAdapter.notifyDataSetChanged();
                    getActivity().getSupportFragmentManager().popBackStack();
                    Log.e("backstack", "called");
                    return true;
                } else {
                    return false;
                }
            }
        });*/

        isLiked();
        getnewsDetail();
        if (!array_relatedposts.isEmpty()) {
            array_relatedposts.clear();
        } else {
            getrealtedPosts();
        }

        //getComments(getActivity());
        try {
            viewPagerAdapter = new ViewPagerAdapter(getActivity(), arrayviewcontainer, "news");
            imgviewpager.setAdapter(viewPagerAdapter);
            imgviewpager.setPageMargin(20);
            Log.e("contentcomment", "" + arrayImages);
        } catch (Exception e) {
           // e.printStackTrace();
        }

        // JCVideoPlayer.releaseAllVideos();
        imgviewpager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
            @Override
            public void onPageSelected(int position) {

                if (arrayviewcontainer.get(position).containsKey("video")) {

                    //JCVideoPlayer.releaseAllVideos();
                }
                int currentPage = imgviewpager.getCurrentItem();
                int totalPages = imgviewpager.getAdapter().getCount();

    int nextPage = currentPage + 1;
    int previousPage = currentPage - 1;

                if (nextPage >= totalPages) {
        img_forward.setVisibility(View.INVISIBLE);
        //nextPage = 0;
    } else {
        img_forward.setVisibility(View.VISIBLE);
    }

                if (previousPage < 0) {
        img_back.setVisibility(View.INVISIBLE);
    } else {
        img_back.setVisibility(View.VISIBLE);
    }

}

    @Override
    public void onPageScrollStateChanged(int state) {

    }

});
        }

    public interface Communicator {

        public void respond(String type);
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        try {
         /* Activity activity; if (context instanceof Activity){
                activity=(Activity) context;*/
            comm = (Communicator) getActivity();
            change_like = (Change_like) getActivity();
            //}
        } catch (Exception exception) {

        }
    }

    private void comment_click() {
        edcomment = (EditText) view.findViewById(R.id.ed_comment);
        edcomment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getCommentlist();
            }
        });
    }

    private void set_relatedPosts() {
        RecyclerView related_list = (RecyclerView) view.findViewById(R.id.recyclerView_related);
        related_list.setHasFixedSize(true);
        related_list.setNestedScrollingEnabled(false);
        related_list.setLayoutManager(new LinearLayoutManager(getActivity()));
        try {
            adapterrelated = new RelatedPosts_adapter(getActivity(), array_relatedposts, "news");
            related_list.setAdapter(adapterrelated);
        } catch (Exception e) {
            //e.printStackTrace();
        }
    }

    void set_nextitem() {
        int postemp = 0;
        TextView textitle = (TextView) view.findViewById(R.id.txt_related);
        ImageView imgrelated = (ImageView) view.findViewById(R.id.img_related);
        FrameLayout frame_one = (FrameLayout) view.findViewById(R.id.frame_container);
        frame_one.setVisibility(View.VISIBLE);

        try {
            postemp = array_relatedposts.size() - 1;
            if (array_relatedposts.get(postemp) != null) {

                if (!array_relatedposts.get(postemp).get("title").isEmpty()) {
                    textitle.setText("" + array_relatedposts.get(postemp).get("title"));
                }
                if (!array_relatedposts.get(postemp).get("video").isEmpty()) {
                    Glide.with(getActivity()).load(array_relatedposts.get(postemp).get("video")).error(R.drawable.newsdefault).into(imgrelated);
                } else {
                    Glide.with(getActivity()).load(array_relatedposts.get(postemp).get("image")).error(R.drawable.newsdefault).into(imgrelated);
                }
            }
        } catch (Exception e) {

        }

        final int finalPostemp = postemp;
        frame_one.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //JCVideoPlayer.releaseAllVideos();
                Newsdetail_fragment fragment = new Newsdetail_fragment();
                Bundle bundle = new Bundle();
                bundle.putString("catid", array_relatedposts.get(finalPostemp).get("catid"));
                bundle.putString("postid", array_relatedposts.get(finalPostemp).get("id"));
                fragment.setArguments(bundle);
                AppCompatActivity activity = (AppCompatActivity) v.getContext();
                activity.getSupportFragmentManager().beginTransaction()
                        .add(R.id.frame_container, fragment, "tempfrag").addToBackStack("tempfrag").commit();
            }
        });
    }

    void getrealtedPosts() {
        //Dialogs.showProDialog(getActivity(), "Loading");
        Log.e("temporaryurl", "" + temp2);
        StringRequest stringRequest = new StringRequest(Request.Method.GET, temp_related,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.e("log", "" + response);
                        Dialogs.disDialog();
                        try {
                            JSONObject jsonObject = new JSONObject(response);
                            if (jsonObject.getString("status").equals("200") && jsonObject.getString("message").equalsIgnoreCase("success")) {
                                HashMap<String, String> hashMap;
                                for (int i = 0; i < jsonObject.getJSONArray("data").length(); i++) {
                                    hashMap = new HashMap<>();
                                    hashMap.put("title", jsonObject.getJSONArray("data").getJSONObject(i).getString("title"));
                                    hashMap.put("image", jsonObject.getJSONArray("data").getJSONObject(i).getString("image_360x290"));
                                    hashMap.put("catid", jsonObject.getJSONArray("data").getJSONObject(i).getString("news_category_id"));
                                    hashMap.put("id", jsonObject.getJSONArray("data").getJSONObject(i).getString("id"));
                                    hashMap.put("video", jsonObject.getJSONArray("data").getJSONObject(i).getString("video_360x290"));
                                    array_relatedposts.add(hashMap);
                                }
                                adapterrelated.notifyDataSetChanged();
                                set_nextitem();
                            } else {

                            }

                        } catch (JSONException e) {
                            //e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Dialogs.disDialog();

                    }
                }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Content-Type", "application/x-www-form-urlencoded");
                return params;
            }
        };
        RequestQueue requestQueue = Volley.newRequestQueue(getActivity());
        requestQueue.add(stringRequest);
    }

    public void change_bookmark(){
        Log.e("onchangebokmark", "called" + bookmark);
        if (bookmark == true) {
            //menubookmark.setIcon(R.drawable.bookfil);
        }
    }

    public boolean getBookMark() {
        return bookmark;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Log.e("oncreate", "");
    }

   /* @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        // TODO your code to hide item her
        Log.e("settings", "skk");
        menu.findItem(R.id.menu_comment).setVisible(true);
        menu.findItem(R.id.menu_share).setVisible(true);
        menubookmark = menu.findItem(R.id.menu_bookmark);
        menubookmark.setVisible(true);

        *//*if (bookmark==true){
            menubookmark.setIcon(R.drawable.bookfil);
        }*//*
        //menu.findItem(R.id.menu_bookmark).setVisible(true);
        //menu.findItem(R.id.like_menu).setVisible(true);
        //menu.findItem(R.id.dislike_menu).setVisible(true);

        menu.findItem(R.id.action_form).setVisible(false);
        menu.findItem(R.id.action_settings).setVisible(false);
        super.onCreateOptionsMenu(menu, inflater);
    }*/

    public void sharecontent() {
        imagetemp = "";
        try {
            if (!dataBean_vid.getVideos().isEmpty()) {
                imagetemp = dataBean_vid.getVideos().get(0).getVideo_thumb();
            } else if (!dataBean2.getImages().isEmpty()) {
                imagetemp = dataBean2.getImages().get(0).getImage();
            }

            MyAsync obj = new MyAsync() {
                @Override
                protected void onPostExecute(Bitmap bmp) {
                    super.onPostExecute(bmp);
                    ByteArrayOutputStream bytes = new ByteArrayOutputStream();
                    if(bmp!=null)
                    bmp.compress(Bitmap.CompressFormat.JPEG, 100, bytes);
                    File f = new File(Environment.getExternalStorageDirectory() + File.separator + "temporary_file.jpg");
                    try {
                        f.createNewFile();
                        FileOutputStream fo = new FileOutputStream(f);
                        fo.write(bytes.toByteArray());

                    } catch (IOException e) {
                       // e.printStackTrace();
                    }
                }
            };
            if (!imagetemp.equals(""))
                obj.execute();

        } catch (Exception e) {

        }
        Intent sharingIntent = new Intent(Intent.ACTION_SEND);
        sharingIntent.setType("*/*");

        try {
            if (!imagetemp.equals("")){
                sharingIntent.putExtra(Intent.EXTRA_STREAM, Uri.parse("file:///sdcard/temporary_file.jpg"));
            }else {
               sharingIntent.setType("text/plain");
            }

            // sharingIntent.putExtra(Intent.EXTRA_TEXT,dataBean.getTitle());
            sharingIntent.putExtra(Intent.EXTRA_TEXT, dataBean.getTitle() + "\n" + Apis.news_Share + dataBean.getSlug());
            Log.e("shreinterjyt", "" + Uri.parse("file:///sdcard/temporary_file.jpg"));
            startActivity(Intent.createChooser(sharingIntent, "Share via"));
        } catch (Exception e) {
            Log.e("exception", "" + e);
        }
    }

    public void do_bookmark() {
        String api_temp = "";
        if (bookmark == true) {
            api_temp = Apis.news_unbookmark;
        } else {
            api_temp = Apis.news_bookmark;
        }
        StringRequest stringRequest = new StringRequest(Request.Method.POST, api_temp,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.e("log", "" + response);
                        JSONObject jsonObject = null;
                        try {
                            jsonObject = new JSONObject(response);
                            if (jsonObject.getString("status").equalsIgnoreCase("200") && jsonObject.getString("message").equalsIgnoreCase("success")) {
                                if (bookmark == true) {
                                     // menubookmark.setIcon(R.drawable.book_bold);
                                    if ( ((NavDrawerActivity) getActivity()).menubookmark!=null)
                                    ((NavDrawerActivity) getActivity()).menubookmark.setIcon(R.drawable.book_bold);
                                    Toast.makeText(getActivity(), jsonObject.getJSONObject("data").getString("message"), Toast.LENGTH_SHORT).show();
                                    bookmark = false;
                                    comm.respond("");
                                } else if (bookmark == false) {
                                   // menubookmark.setIcon(R.drawable.bookfil);
                                    if ( ((NavDrawerActivity) getActivity()).menubookmark!=null)
                                    ((NavDrawerActivity) getActivity()).menubookmark.setIcon(R.drawable.bookfil);
                                    Toast.makeText(getActivity(), "Post Bookmarked successfully", Toast.LENGTH_SHORT).show();
                                    bookmark = true;
                                    comm.respond("");
                                }
                            }
                            /*if (getActivity().getSupportFragmentManager().findFragmentByTag("bookmark") instanceof Bookmark_fragment) {
                                Log.e("first", "");
                                Bookmark_fragment frag = ((Bookmark_fragment)getActivity().getSupportFragmentManager().findFragmentByTag("bookmark"));
                                if (frag != null)
                                    frag.getall_bookmarks();
                            }*/
                        } catch (Exception e) {
                           // e.printStackTrace();
                        }
                    }
                },

                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Dialogs.disDialog();
                    }
                }) {

            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> map = new HashMap<String, String>();
                map.put("user", SaveSharedPreference.getUserID(getActivity()));
                map.put("token", SaveSharedPreference.getPrefToken(getActivity()));
                map.put("id", postid);
                return map;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Content-Type", "application/x-www-form-urlencoded");
                return params;
            }
        };
        VolleySingleton.getInstance(getActivity()).addToRequestQueue(stringRequest);
    }

    public void is_bookmark() {
        StringRequest stringRequest = new StringRequest(Request.Method.POST, Apis.news_isbookmark,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.e("log", "" + response);
                        JSONObject jsonObject = null;
                        try {
                            jsonObject = new JSONObject(response);
                            if (jsonObject.getString("status").equalsIgnoreCase("200") && jsonObject.getString("message").equalsIgnoreCase("success")) {
                                if (jsonObject.getJSONObject("data").getString("message").equalsIgnoreCase("Already bookmarked")) {
                                   if (((NavDrawerActivity) getActivity()).menubookmark!=null)
                                    ((NavDrawerActivity) getActivity()).menubookmark.setIcon(R.drawable.bookfil);
                                    bookmark = true;
                                 }else {
                                    if (((NavDrawerActivity) getActivity()).menubookmark!=null)
                                    ((NavDrawerActivity) getActivity()).menubookmark.setIcon(R.drawable.book_bold);
                                    bookmark = false;
                                }
                            }
                        } catch (Exception e) {
                            //e.printStackTrace();
                        }
                    }
                },

                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Dialogs.disDialog();
                    }
                })
        {

            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> map = new HashMap<String, String>();
                map.put("user", SaveSharedPreference.getUserID(getActivity()));
                map.put("token", SaveSharedPreference.getPrefToken(getActivity()));
                map.put("id", postid);
                return map;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Content-Type", "application/x-www-form-urlencoded");
                return params;
            }
        };
        VolleySingleton.getInstance(getActivity()).addToRequestQueue(stringRequest);
    }

   /* @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.menu_comment:
                try{
                    // Log.e("press",""+postid+""+catid);
                    CommentsList_fragment fragment = new CommentsList_fragment();
                    Bundle bundle = new Bundle();
                    bundle.putSerializable("arrayhash",arrayhashComent);
                    bundle.putString("postid",postid);
                    bundle.putString("type","news");
                    fragment.setArguments(bundle);
                    AppCompatActivity activity = (AppCompatActivity)getContext();
                    activity.getSupportFragmentManager().beginTransaction()
                            .add(R.id.frame_container,fragment,"comment").addToBackStack(null).commit();

                }catch (Exception e){

                }
                return true;

            case R.id.menu_share:
                return true;
            default:
                return false;
        }
    }*/

    private void nextPage() {
        int currentPage = imgviewpager.getCurrentItem();
        int totalPages = imgviewpager.getAdapter().getCount();

        int nextPage = currentPage + 1;
        if (nextPage >= totalPages) {

            nextPage = 0;
        }
        imgviewpager.setCurrentItem(nextPage, true);
    }

    private void previousPage() {
        int currentPage = imgviewpager.getCurrentItem();
        int totalPages = imgviewpager.getAdapter().getCount();

        int previousPage = currentPage - 1;
        if (previousPage < 0) {
            // We can't go back anymore.
            // Loop to the last page. If you don't want looping just
            // return here.
            previousPage = totalPages - 1;
        }
        imgviewpager.setCurrentItem(previousPage, true);
    }

    @Override
    public void onResume() {
        super.onResume();
        try {
            //JCVideoPlayer.releaseAllVideos();
        } catch (Exception e) {

        }
        //adapter.notifyDataSetChanged();
    }

    public void getnewsDetail() {
        Dialogs.showProDialog(getActivity(), "Loading");
        Log.e("temporaryurl", "" + temp2);
        StringRequest stringRequest = new StringRequest(Request.Method.GET, temp2,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.e("log", "" + response);
                        getResponse(response);
                        Dialogs.disDialog();
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Dialogs.disDialog();
                        DialogInterface.OnClickListener onClickTryAgain = new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                Dialogs.disDialog();
                                getnewsDetail();
                            }
                        };
                        if (error instanceof NoConnectionError) {
                            Dialogs.disDialog();
                            Alerts.internetConnectionErrorAlert(getContext(), onClickTryAgain);
                        }
                    }
                }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Content-Type", "application/x-www-form-urlencoded");
                return params;
            }
        };
        RequestQueue requestQueue = Volley.newRequestQueue(getActivity());
        requestQueue.add(stringRequest);
    }

    public static String getDate(String date) throws ParseException {
        String myFormat = "yyyy-MM-dd HH:mm:ss";
        DateFormat sdformat = new SimpleDateFormat(myFormat);
        DateFormat targetFormat = new SimpleDateFormat("MMMM dd, yyyy");
        String formattedDate = targetFormat.format(sdformat.parse(date));
        return formattedDate;
    }

    public void getResponse(String response) {
        if (!response.equals(null)) {
            Log.e("log", "" + response);
            Gson gson = new Gson();
            newsDetail = gson.fromJson(response, NewsDetail.class);
            int status = newsDetail.getStatus();
            String msg = newsDetail.getMessage();

            if (msg.equalsIgnoreCase("success") && status == 200) {
                try {
                    dataBean = newsDetail.getData().get(0);
                    dataBean2 = newsDetail.getData().get(2);
                    dataBean3 = newsDetail.getData().get(1);
                    dataBean_vid = newsDetail.getData().get(3);
                    dataBean_audio = newsDetail.getData().get(4);

                } catch (Exception e) {

                }
                if (dataBean != null) {
                    lnlocation.setVisibility(View.VISIBLE);
                    ln_userdetail.setVisibility(View.VISIBLE);
                    if (dataBean.getIs_anonymous().equalsIgnoreCase("Y")) {
                        txt_ownername.setText("Anonymous");
                        lnlocation.setVisibility(View.GONE);
                    } else {
                        txt_ownername.setText(dataBean.getUsername());
                        try {
                            if (dataBean.getLatitude() != null && dataBean.getLongitude() != null) {
                                if (CommonFunctions.isValidLatLng(getActivity(), Double.parseDouble((String) dataBean.getLatitude()), Double.parseDouble((String) dataBean.getLongitude()))) {
                                    location = CommonFunctions.getAddress(getActivity(), Double.parseDouble((String) dataBean.getLatitude()), Double.parseDouble((String) dataBean.getLongitude()));
                                    if (location != null) {
                                        txtplace.setText(location + "");
                                    } else if (dataBean.getLocation() != null) {
                                        txtplace.setText(dataBean.getLocation());
                                    } else {
                                      lnlocation.setVisibility(View.GONE);
                                    }
                                    Log.e("settled location", "" + location);
                                }
                            } else {
                                if (dataBean.getLocation() != null) {
                                    txtplace.setText(dataBean.getLocation());
                                } else {
                                    lnlocation.setVisibility(View.GONE);
                                }
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                    if (txtplace.getText().equals("") || txtplace.getText().equals(" ")) {
                        lnlocation.setVisibility(View.GONE);
                    }
                    if (NavDrawerActivity.toolbar != null) {
                        //NavDrawerActivity.toolbar.setLogo(null);
                        NavDrawerActivity.toolbar.setTitle(dataBean.getName());
                    }
                    //txtname.setText(dataBean.getName());
                    txttitle.setText(dataBean.getTitle());
                    if (txttitle.getText().toString().equals("")) {
                        txttitle.setVisibility(View.GONE);
                    }
                    Log.e("description",""+ dataBean.getDescription());
                    txtdescr.setText(dataBean.getDescription());
                    txtshortdescr.setText(dataBean.getShort_description());

                   /* textView.setText(bypass.markdownToSpannable(text,
                            new BypassPicassoImageGetter(textView, Picasso.with(MainActivity.this)))); */

                    if (txtdescr.getText().toString().isEmpty()) {
                        txtdescr.setVisibility(View.GONE);
                    }
                    if (txtshortdescr.getText().toString().equals("")) {
                        txtshortdescr.setVisibility(View.GONE);
                    }

                    try {
                        txtdate.setText("" + getDate(dataBean.getDt_added()));
                    } catch (ParseException e) {
                        e.printStackTrace();
                    }

                    if (dataBean.getTotal_comments() != null) {
                        txtcomment.setText(dataBean.getTotal_comments());
                    }

                    if (dataBean.getTotal_likes() != null) {
                        txtlike.setText("" + dataBean.getTotal_likes());
                    }
                    if (dataBean.getTotal_dislikes() != null) {
                        txtdislike.setText("" + dataBean.getTotal_dislikes());
                    }
                    if (dataBean.getTotal_views() != null) {
                        //  txtvisible.setText("" + dataBean.getTotal_views());
                    }
                    if (dataBean.getTotal_flags() != null) {
                        //    txtfav.setText("" + dataBean.getTotal_flags());
                    }
                } else {
                    txttitle.setVisibility(View.GONE);
                    txtdescr.setVisibility(View.GONE);
                    ln_userdetail.setEnabled(false);
                }
                showCommentList();
                if (!arrayviewcontainer.isEmpty()) {
                    arrayviewcontainer.clear();
                }
                showImages();

            }
        }
    }

    public void showCommentList() {
        if (!arrayhashComent.isEmpty())
            arrayhashComent.clear();

        if (!dataBean3.getComments().isEmpty()) {
            HashMap<String, String> hashMap;
            for (int i = 0; i < dataBean3.getComments().size(); i++) {
                try {
                    hashMap = new HashMap<>();
                    NewsDetail.DataBean.CommentsBean commentsBean = dataBean3.getComments().get(i);
                    hashMap.put("commentid", commentsBean.getId());
                    hashMap.put("comment", commentsBean.getComment());
                    hashMap.put("userid", commentsBean.getUser_id());
                    hashMap.put("avatar", commentsBean.getAvatar());
                    hashMap.put("username", commentsBean.getUsername());
                    arrayhashComent.add(hashMap);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
            // getComments(context);
            //adapter.notifyDataSetChanged();
        }
    }

    private void showImages() {
        if (dataBean2.getImages().size() != 0) {
            HashMap<String, String> hashMap;
            for (int i = 0; i < dataBean2.getImages().size(); i++) {
                try {
                    hashMap = new HashMap<>();
                    NewsDetail.DataBean.ImagesBean imagesBean = dataBean2.getImages().get(i);
                    hashMap.put("image", imagesBean.getImage_360x290());
                    //hashMap.put("image_high",imagesBean.getImage_360x290());
                    hashMap.put("imagezoom", imagesBean.getImage_zoom());
                    arrayviewcontainer.add(hashMap);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
            //
        }
        // viewPagerAdapter.notifyDataSetChanged();
        addAudio();
    }

    private void addAudio() {
        if (dataBean_audio.getAudio().size() != 0) {
            HashMap<String, String> hashMap;
            for (int i = 0; i < dataBean_audio.getAudio().size(); i++) {
                try {
                    hashMap = new HashMap<>();
                    NewsDetail.DataBean.AudioBean audioBean = dataBean_audio.getAudio().get(i);
                    hashMap.put("audio", audioBean.getAudio());
                    arrayviewcontainer.add(hashMap);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
        // viewPagerAdapter.notifyDataSetChanged();
        getVideos();
    }

    private void getVideos() {
        if (dataBean_vid.getVideos() != null && !dataBean_vid.getVideos().isEmpty()) {
            HashMap<String, String> hashMap;
            for (int i = 0; i < dataBean_vid.getVideos().size(); i++) {
                try {
                    hashMap = new HashMap<>();
                    NewsDetail.DataBean.VideosBean videosBean = dataBean_vid.getVideos().get(i);
                    hashMap.put("video", videosBean.getVideo());
                    hashMap.put("image", videosBean.getVideo_thumb());
                    arrayviewcontainer.add(hashMap);
                    // arrayvideos.add(videosBean);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
        Log.e("total_container", "" + arrayviewcontainer);
        viewPagerAdapter.notifyDataSetChanged();
        if (arrayviewcontainer.isEmpty()) {

            imgviewpager.setVisibility(View.GONE);
            imgonview.setVisibility(View.VISIBLE);
            imgonview.setImageResource(R.drawable.newsdefault);
            img_back.setVisibility(View.INVISIBLE);
            img_forward.setVisibility(View.INVISIBLE);
        } else if (arrayviewcontainer.size() >= 1) {

            int currentPage = imgviewpager.getCurrentItem();
            int totalPages = imgviewpager.getAdapter().getCount();

            int nextPage = currentPage + 1;
            int previousPage = currentPage - 1;

            if (nextPage >= totalPages) {
                img_forward.setVisibility(View.INVISIBLE);
                //nextPage = 0;
            } else {
                img_forward.setVisibility(View.VISIBLE);
            }

            if (previousPage < 0) {
                img_back.setVisibility(View.INVISIBLE);
            } else {
                img_back.setVisibility(View.VISIBLE);
            }

        }
    }

    @Override
    public void onPause() {
        super.onPause();
        try{
            if (ViewPagerAdapter.jzVideoPlayerStandard!=null){
                JZVideoPlayer.releaseAllVideos();
            }
        }catch (Exception e){

        }
        //JCVideoPlayer.releaseAllVideos();
    }

    public class MyAsync extends AsyncTask<Void, Void, Bitmap> {
        @Override
        protected Bitmap doInBackground(Void... params) {
            try {
                URL url = new URL(imagetemp);
                HttpURLConnection connection = (HttpURLConnection) url.openConnection();
                connection.setDoInput(true);
                connection.connect();
                InputStream input = connection.getInputStream();
                Bitmap myBitmap = BitmapFactory.decodeStream(input);
                return myBitmap;
            } catch (IOException e) {
              //  e.printStackTrace();
                return null;
            }
        }
    }

    public void likePost() {
        StringRequest stringRequest = new StringRequest(Request.Method.POST, Apis.vote,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.e("log", "" + response);
                        try {
                            JSONObject jsonObject = new JSONObject(response);
                            if (jsonObject.getString("status").equalsIgnoreCase("200")
                                    && jsonObject.getString("message").equalsIgnoreCase("success")) {
                                if (vote.equalsIgnoreCase("u")) {
                                    img_like.setImageResource(R.drawable.thumb_black);
                                    if (dataBean.getTotal_likes() != null) {
                                        int temp = Integer.parseInt(dataBean.getTotal_likes()) + 1;
                                        txtlike.setText("" + temp);
                                    }
                                  likestatus="1";
                                  change_like.changelike("news");
                                } else {

                                }
                            }
                        } catch (JSONException e) {
                           // e.printStackTrace();
                        }
                    }
                },

                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Dialogs.disDialog();
                    }
                }) {

            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> map = new HashMap<String, String>();
                map.put("user", SaveSharedPreference.getUserID(getActivity()));
                map.put("token", SaveSharedPreference.getPrefToken(getActivity()));
                map.put("id", postid);
                map.put("vote", vote);
                Log.e("mapcomment", "" + map);
                return map;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Content-Type", "application/x-www-form-urlencoded");
                return params;
            }
        };

        RequestQueue requestQueue = Volley.newRequestQueue(getActivity());
        requestQueue.add(stringRequest);
    }

    public void isLiked() {
        Log.e("temporaryurl", "" + temp2);
        StringRequest stringRequest = new StringRequest(Request.Method.POST, Apis.isVote,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.e("log", "" + response);
                        try {
                            JSONObject jsonObject = new JSONObject(response);
                            if (jsonObject.getString("status").equals("200")) {
                                String status = jsonObject.getJSONObject("data").getString("vote_status");
                                if (status.equalsIgnoreCase("d")) {
                                    likestatus="0";
                                } else if (status.equalsIgnoreCase("u")) {
                                    img_like.setImageResource(R.drawable.thumb_black);
                                    likestatus="1";
                                } else {
                                    likestatus="0";
                                }
                            }

                        } catch (JSONException e) {
                           // e.printStackTrace();
                        } catch (Exception e) {

                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                    }
                }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> map = new HashMap<String, String>();
                map.put("user", SaveSharedPreference.getUserID(getActivity()));
                map.put("token", SaveSharedPreference.getPrefToken(getActivity()));
                map.put("id", postid);
                Log.e("mapvote", "" + map);
                return map;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Content-Type", "application/x-www-form-urlencoded");
                return params;
            }
        };
        RequestQueue requestQueue = Volley.newRequestQueue(getActivity());
        requestQueue.add(stringRequest);
    }

    public void notifyadapter(){
        adapter.notifyDataSetChanged();
    }
    @Override
    public void onClick(View v) {
        if (v == lnlike) {
            Log.e("liked", "");
            if (SaveSharedPreference.getUserID(getContext()).equals("")) {
                Toast.makeText(getContext(), "Please Login to continue!", Toast.LENGTH_SHORT).show();
            } else {
                if (likestatus.equals("1")){
                    revert_like();

                }else {
                    vote = "u";
                    likePost();
                }

            }
        } else if (v == lndislike) {
            Log.e("disliked", "");
            if (SaveSharedPreference.getUserID(getContext()).equals("")) {
                Toast.makeText(getContext(), "Please Login to continue!", Toast.LENGTH_SHORT).show();
            } else {
                vote = "d";
                likePost();
            }
        } else if (v == lncomment) {
            try {
                CommentsList_fragment fragment = new CommentsList_fragment();
                Bundle bundle = new Bundle();
                bundle.putSerializable("arrayhash", arrayhashComent);
                bundle.putString("postid", postid);
                bundle.putString("type", "news");
                fragment.setArguments(bundle);
                AppCompatActivity activity = (AppCompatActivity) v.getContext();
                activity.getSupportFragmentManager().beginTransaction()
                        .add(R.id.frame_container, fragment, "comment").addToBackStack("comment").commit();

            } catch (Exception e) {

            }
        } else if (v == imgshare) {
            try {
                MyAsync obj = new MyAsync() {
                    @Override
                    protected void onPostExecute(Bitmap bmp) {
                        super.onPostExecute(bmp);

                        imagebit = bmp;
                        Log.e("imagebit2", "" + imagebit);
                        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
                        imagebit.compress(Bitmap.CompressFormat.JPEG, 100, bytes);
                        File f = new File(Environment.getExternalStorageDirectory() + File.separator + "temporary_file.jpg");
                        try {
                            f.createNewFile();
                            FileOutputStream fo = new FileOutputStream(f);
                            fo.write(bytes.toByteArray());

                        } catch (IOException e) {
                           // e.printStackTrace();
                        }
                    }
                };
                if (!dataBean2.getImages().isEmpty())
                    obj.execute();

            } catch (Exception e) {

            }

            Intent sharingIntent = new Intent(Intent.ACTION_SEND);
            sharingIntent.setType("*/*");
            try {
                if (!dataBean2.getImages().isEmpty())
                    sharingIntent.putExtra(Intent.EXTRA_STREAM, Uri.parse("file:///sdcard/temporary_file.jpg"));
                // sharingIntent.putExtra(Intent.EXTRA_TEXT,dataBean.getTitle());
                sharingIntent.putExtra(Intent.EXTRA_TEXT, dataBean.getTitle() + "\n" + Apis.news_Share + dataBean.getSlug());
                Log.e("shreinterjyt", "" + Uri.parse("file:///sdcard/temporary_file.jpg"));
                startActivity(Intent.createChooser(sharingIntent, "Share via"));
            } catch (Exception e) {
                Log.e("exception", "" + e);
            }
        } else if (v == img_back) {
            previousPage();
        } else if (v == img_forward) {
            nextPage();
        } else if (v == ln_userdetail) {
            if (!dataBean.getIs_anonymous().equalsIgnoreCase("Y")) {
                if (dataBean.getUser_id().equals(SaveSharedPreference.getUserID(getActivity()))) {
                    Publicprofile_Fragment.flag = false;
                    AppCompatActivity activity = (AppCompatActivity) v.getContext();
                    activity.getSupportFragmentManager().beginTransaction()
                            .add(R.id.frame_container, new Publicprofile_Fragment(), "public").addToBackStack("public").commit();
                } else {
                    Publicprofile_Fragment.flag = true;
                    Publicprofile_Fragment.username = dataBean.getUsername();
                    AppCompatActivity activity = (AppCompatActivity) v.getContext();
                    activity.getSupportFragmentManager().beginTransaction()
                            .add(R.id.frame_container, new Publicprofile_Fragment(), "public").addToBackStack("public").commit();
                }
            }
        }
    }


    public void revert_like() {
        StringRequest stringRequest = new StringRequest(Request.Method.POST,
                Apis.delete_Vote,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.e("log", "" + response);
                        try {
                            JSONObject jsonObject = new JSONObject(response);
                            if (jsonObject.getString("status").equals("200") &&
                                    jsonObject.getString("message").equals("success")) {
                                img_like.setImageResource(R.drawable.like_rr);
                                likestatus="0";
                                change_like.changelike("news");
                            }else {

                            }
                        } catch (JSONException e) {
                            // e.printStackTrace();
                        } catch (Exception e) {

                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Dialogs.disDialog();
                    }
                })
        {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> map = new HashMap<String, String>();
                map.put("user", SaveSharedPreference.getUserID(getContext()));
                map.put("token", SaveSharedPreference.getPrefToken(getContext()));
                map.put("id", postid);
                Log.e("mapvote", "" + map);
                return map;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Content-Type", "application/x-www-form-urlencoded");
                return params;
            }
        };
        RequestQueue requestQueue = Volley.newRequestQueue(getContext());
        requestQueue.add(stringRequest);
    }


    public void getCommentlist() {
        try {
            // Log.e("press",""+postid+""+catid);
            CommentsList_fragment fragment = new CommentsList_fragment();
            Bundle bundle = new Bundle();
            bundle.putSerializable("arrayhash", arrayhashComent);
            bundle.putString("postid", postid);
            bundle.putString("type", "news");
            fragment.setArguments(bundle);

            AppCompatActivity activity = (AppCompatActivity) getContext();
            activity.getSupportFragmentManager().beginTransaction()
                    .add(R.id.frame_container, fragment,"comment")
                    .addToBackStack("comment").commit();
        } catch (Exception e) {

        }

    }


}

