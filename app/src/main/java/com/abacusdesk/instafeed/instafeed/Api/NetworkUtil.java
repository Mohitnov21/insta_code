package Api;

import android.content.Context;

import java.io.IOException;
import java.util.concurrent.TimeUnit;

import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import okhttp3.logging.HttpLoggingInterceptor;


/**
 * Created by user on 8/19/2016.
 */

class NetworkUtil {

    public static OkHttpClient addHeader(final Context context) {

        OkHttpClient.Builder httpClient = new OkHttpClient.Builder();
        httpClient.addInterceptor(new Interceptor() {
            @Override
            public Response intercept(Chain chain) throws IOException {
                Request original = chain.request();
                Request.Builder builder = original.newBuilder();
                builder.method(original.method(), original.body());


                return chain.proceed(builder.build());
            }
        });

        HttpLoggingInterceptor logging = new HttpLoggingInterceptor();
        httpClient.readTimeout(60, TimeUnit.SECONDS);
        // set your desired log level
        logging.setLevel(HttpLoggingInterceptor.Level.BODY);
        httpClient.interceptors().add(logging);

        return httpClient.build();
    }


    public static OkHttpClient addLog(final Context context) {
        OkHttpClient.Builder httpClient = new OkHttpClient.Builder();
//        httpClient.addInterceptor(new Interceptor() {
//            @Override
//            public Response intercept(Interceptor.Chain chain) throws IOException {
//                Request original = chain.request();
//
//                Request request = original.newBuilder()
//                        .header(USER_ID, appPref.getUserId() + "")
//                        .header(USER_TOKEN, appPref.getUserToken())
//                        .header(DEVICE_ID, UtileClass.getDeviceId(context))
//                        .method(original.method(), original.body())
//                        .build();
//
//
//                return chain.proceed(request);
//            }
//        });

        HttpLoggingInterceptor logging = new HttpLoggingInterceptor();
// set your desired log level
        logging.setLevel(HttpLoggingInterceptor.Level.BODY);
        httpClient.interceptors().add(logging);

        return httpClient.build();
    }


}