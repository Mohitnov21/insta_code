package com.abacusdesk.instafeed.instafeed;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.ExpandableListView;
import android.widget.ImageButton;
import android.widget.TextView;

import com.abacusdesk.instafeed.instafeed.Util.SaveSharedPreference;

import java.util.ArrayList;

/**
 * Created by codecube on 24/5/17.
 */

public class NavigationDrawerAdapter extends BaseExpandableListAdapter {

    ArrayList<Screen> screens = new ArrayList<>();
    Context context;
    public  static  int HOME=1;
    public  static  int LOGOUT=2;


    public void update(ArrayList<Screen> screens,Context context) {
        this.screens = screens;
        this.context=context;
        notifyDataSetChanged();
    }

    @Override
    public int getGroupCount() {
        return screens.size();
    }

    @Override
    public int getChildrenCount(int groupPosition) {
        return screens.get(groupPosition).getSubScreens().size();
    }

    @Override
    public Screen getGroup(int groupPosition) {
        return screens.get(groupPosition);
    }

    @Override
    public SubScreen getChild(int groupPosition, int childPosition) {
        return screens.get(groupPosition).getSubScreens().get(childPosition);
    }

    @Override
    public long getGroupId(int groupPosition) {
        try {
            return Long.valueOf(screens.get(groupPosition).getId().trim());
        } catch (NumberFormatException e) {
            e.printStackTrace();
            return -1;
        }
    }

    @Override
    public long getChildId(int groupPosition, int childPosition) {
        try {
            return Long.valueOf(screens.get(groupPosition).getSubScreens().get(childPosition).getId().trim());
        } catch (NumberFormatException e) {
            e.printStackTrace();
            return -1;
        }
    }

    @Override
    public boolean hasStableIds() {
        return false;
    }

    @Override
    public View getGroupView(final int groupPosition, final boolean isExpanded, View convertView, final ViewGroup parent) {
        GroupViewHolder holder;
        if (convertView == null) {
            convertView = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_navigation_drawer_item, parent, false);
            holder = new GroupViewHolder();
            holder.name = (TextView) convertView.findViewById(R.id.name);
            holder.icon = (ImageButton) convertView.findViewById(R.id.icon);
            holder.count=(TextView)convertView.findViewById(R.id.count);
            convertView.setTag(holder);
        } else holder = (GroupViewHolder) convertView.getTag();
         holder.icon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (isExpanded) ((ExpandableListView) parent).collapseGroup(groupPosition);
                else ((ExpandableListView) parent).expandGroup(groupPosition);
            }
        });
        Screen screen = getGroup(groupPosition);
        holder.name.setText(screen.getName());
        if (getChildrenCount(groupPosition) < 1) holder.icon.setVisibility(View.GONE);
        else if (isExpanded) {
            holder.icon.setVisibility(View.VISIBLE);
            holder.icon.setImageResource(R.drawable.ic_forward_arrow);
        } else {
            holder.icon.setVisibility(View.VISIBLE);
            holder.icon.setImageResource(R.drawable.ic_down_arrow);
        }
        if(groupPosition==0){
            holder.name.setCompoundDrawablesWithIntrinsicBounds( R.drawable.blackhome_32, 0, 0, 0);
        } else if(groupPosition==1){
            if (SaveSharedPreference.getUserID(context).equals("")){
                holder.name.setCompoundDrawablesWithIntrinsicBounds( R.drawable.userlight_32, 0, 0, 0);
                holder.name.getResources().getColor(R.color.light_gray_new);
            }
             holder.name.setCompoundDrawablesWithIntrinsicBounds( R.drawable.blackuser_32, 0, 0, 0);

        }else if(groupPosition==2){
            if (SaveSharedPreference.getUserID(context).equals("")){
                holder.name.setCompoundDrawablesWithIntrinsicBounds( R.drawable.followerslight_32, 0, 0, 0);
            }
            holder.name.setCompoundDrawablesWithIntrinsicBounds( R.drawable.bookfil, 0, 0, 0);
            //  holder.count.setText(SaveSharedPreference.getFollowers(context));
        }
        else if(groupPosition==3){
            if (SaveSharedPreference.getUserID(context).equals("")){
                holder.name.setCompoundDrawablesWithIntrinsicBounds( R.drawable.followerslight_32, 0, 0, 0);
            }
            holder.name.setCompoundDrawablesWithIntrinsicBounds( R.drawable.blackfollowers_32, 0, 0, 0);
          //  holder.count.setText(SaveSharedPreference.getFollowers(context));
        }else if(groupPosition==4){
            if (SaveSharedPreference.getUserID(context).equals("")){
                holder.name.setCompoundDrawablesWithIntrinsicBounds( R.drawable.followinglight_32, 0, 0, 0);
            }
            holder.name.setCompoundDrawablesWithIntrinsicBounds( R.drawable.blackfollowing_32, 0, 0, 0);
          //  holder.count.setText(SaveSharedPreference.getFollowing(context));
        }
        else if(groupPosition==5){
            if (SaveSharedPreference.getUserID(context).equals("")){
                holder.name.setEnabled(false);
            }
            holder.name.setCompoundDrawablesWithIntrinsicBounds(R.drawable.mypost_icon, 0, 0, 0);
        }else if(groupPosition==6){
            if (SaveSharedPreference.getUserID(context).equals("")){
                holder.name.setEnabled(false);
            }
            holder.name.setCompoundDrawablesWithIntrinsicBounds( R.drawable.blackmypost_32, 0, 0, 0);
        }else if(groupPosition==7){
            if (SaveSharedPreference.getUserID(context).equals("")){
                holder.name.setCompoundDrawablesWithIntrinsicBounds( R.drawable.pencillight_32, 0, 0, 0);
            }
            holder.name.setCompoundDrawablesWithIntrinsicBounds( R.drawable.blackpencil_32, 0, 0, 0);
        }else if(groupPosition==8){
            holder.name.setCompoundDrawablesWithIntrinsicBounds( R.drawable.black_about_32, 0, 0, 0);
        }else if(groupPosition==9){
            holder.name.setCompoundDrawablesWithIntrinsicBounds( R.drawable.key, 0, 0, 0);
        }else if(groupPosition==10){
            if (SaveSharedPreference.getUserID(context).equals("")){
                holder.name.setEnabled(false);
            }
            holder.name.setCompoundDrawablesWithIntrinsicBounds( R.drawable.blacklogout_32, 0, 0, 0);
        }
        else {
            //holder.name.setCompoundDrawablesWithIntrinsicBounds( R.drawable.logout, 0, 0, 0);
        }

        if(groupPosition==0  ){
            holder.icon.setVisibility(View.INVISIBLE);
            holder.count.setVisibility(View.INVISIBLE);
        }
        if(groupPosition==1  ){
            holder.icon.setVisibility(View.INVISIBLE);
            holder.count.setVisibility(View.INVISIBLE);
        }if(groupPosition==2) {
            holder.icon.setVisibility(View.INVISIBLE);
            holder.count.setVisibility(View.INVISIBLE);
        }
        if(groupPosition==3) {
            holder.icon.setVisibility(View.INVISIBLE);
            holder.count.setVisibility(View.INVISIBLE);
        }else {
            holder.count.setVisibility(View.GONE);
        }
        return convertView;
    }

    @Override
    public View getChildView(int groupPosition, int childPosition, boolean isLastChild, View convertView, ViewGroup parent) {
        ChildViewHolder holder;
        if (convertView == null) {
            convertView = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_navigation_drawer_subitem, parent, false);
            holder = new ChildViewHolder();
            holder.name = (TextView) convertView.findViewById(R.id.name);
            convertView.setTag(holder);

        } else holder = (ChildViewHolder) convertView.getTag();
        SubScreen subScreen = getChild(groupPosition, childPosition);
        holder.name.setText(subScreen.getName());
        return convertView;
    }

    @Override
    public boolean isChildSelectable(int groupPosition, int childPosition) {
        return true;
    }

    private void showLog(String message) {
        Log.i(getClass().getSimpleName(), "" + message);
    }

    private class GroupViewHolder {
        TextView name,count;
        ImageButton icon;
    }

    private class ChildViewHolder {
        TextView name;
    }
}
