package com.abacusdesk.instafeed.instafeed.model;

import java.util.List;

/**
 * Created by abacusdesk on 2017-08-17.
 */

public class Blogdetail_model {


    /**
     * status : 200
     * message : success
     * data : [{"id":"50","title":"GOLDEN TEMPLE- SHRINE FOR HUMANITY","slug":"golden-temple-shrine-for-humanity-902398593","short_description":"Golden temple, Amritsar","description":"Sri Harimandir Sahib, Amritsar is not only the biggest religious place for the Sikhs, but also a symbol of human brotherhood and equality. It depicts the district identity, glory and heritage of the Sikhs. Irrespective of one's caste, creed or race they can seek spiritual solace and religious fulfillment without any obstacle.\r\nThe Golden Temple, Amritsar, India has a very different architecture as it's built at a level lower than the surrounding land level. Gurudwara teaches the lesson of egalitarianism and humility. Four entrance of this holy place from all four directions, signify that people belonging to every walk of life are equally welcome.\r\n\r\nHOW TO REACH AND ACCOMMODATIONS:\r\n\r\nBy Air- Amritsar has an extremely developed airport. Raja Saini International Airport located nearly 11km away from the city centre. There is also another airport Guru Ram Das International Airport that operates flights from different corners of the world.\r\n\r\nBy Rail- Having an extensive rail network, Amritsar is connected to all the major cities of the country. If you are traveling from Delhi, Shatabdi Express is the best option with 6 hours of train journey. The railway station of Amritsar is centrally located of hardly 15 minutes\u2019 drive from The Golden Temple. With high tourist traffic, there are facilities of ATM centre and tourist facility centre.\r\n\r\nBy Road- With the broad network of roads running through Amritsar, the connectivity is well versed. Grand Trunk Karnal Road connects Delhi to Amritsar. Some daily direct buses operate to Jammu, Katra, Chandigarh and Dharmshala. Amritsar is adequately connected by road to both the places within Punjab as well as outside the state.   \r\nAccommodation- Seven Niwas Asthans (Inns) are provided lodging facilities for pilgrims, maintained by Sri Harimandir Sahib Authorities. Bookings is made on the basis of first-come-first-serve. The seven inns are, as follows:\r\n1. Guru Ram Das Niwas\r\n2. Guru Nanak Niwas\r\n3. Guru Arjan Dev Niwas\r\n4. New Akal Rest House\r\n5. Guru Hargobind Nawas\r\n6. Mata Ganga Ji Niwas\r\n7. Guru Gobind Singh NRI Yatra Niwas\r\n \r\n \r\nGURU KA LANGAR:\r\nThis tradition was initiated by Guru Nanak Dev Ji, later established by the 3rd Guru Sri Guru Amar Dass Ji at Goindwal. Mughal King Akbar came and sat among the odinary people to share langar.\r\nThis community kitchen has helped the community in many ways. It has ensured the participation of women and children in the service of mankind. Women play a very important role in the preparations of meals and children help in serving food to the pangat. Langar has played a great role in upholding the virtue of sameness of all human beings, giving a welcome, secure and protected place.\r\nNormal families also volunteer to provide and prepare langar. Everyone is welcome here to share the Langar. Lastly, all the preparations, cooking and washing is done by volunteers or by voluntary helpers (Sewadars).\r\nThe Golden Temple Community Kitchen has an average of 75,000 devotees and tourists who come in for langar in the Community Kitchen daily, the number almost double on special occasions. On an average day, 100 Quintal wheat flour, 25 quintal cereals, 10 quintal rice, 5000 litre milk, 10 quintal sugar, 5 quintal pure ghee is use. Approximately, 100 LPG gas cylinders are used to prepare the meals with 100 employees and devotees in service to the kitchen.\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n","banner":"http://instafeed.org/storage/blog_banner/256x170-295954d7dfafe569bc8eaddbb2175706.jpg","banner_zoom":"http://instafeed.org/storage/blog_banner/748x420-295954d7dfafe569bc8eaddbb2175706.jpg","banner_original":"http://instafeed.org/storage/blog_banner/295954d7dfafe569bc8eaddbb2175706.jpg","banner_330x210":"http://instafeed.org/storage/blog_banner/330x210-295954d7dfafe569bc8eaddbb2175706.jpg","banner_80x80":"http://instafeed.org/storage/blog_banner/80x80-295954d7dfafe569bc8eaddbb2175706.jpg","banner_264x200":"http://instafeed.org/storage/blog_banner/264x200-295954d7dfafe569bc8eaddbb2175706.jpg","banner_748x420":"http://instafeed.org/storage/blog_banner/748x420-295954d7dfafe569bc8eaddbb2175706.jpg","banner_360x200":"http://instafeed.org/storage/blog_banner/360x200-295954d7dfafe569bc8eaddbb2175706.jpg","banner_256x170":"http://instafeed.org/storage/blog_banner/256x170-295954d7dfafe569bc8eaddbb2175706.jpg","banner_100x100":"http://instafeed.org/storage/blog_banner/100x100-295954d7dfafe569bc8eaddbb2175706.jpg","tags":null,"blog_category_id":"2","total_comments":"1","total_views":"861","user_id":"2","dt_added":"2017-04-21 14:49:34","dt_modified":"2017-07-22 12:57:54","status":"P"},{"comments":[{"id":"2","comment":"beautiful","blog_post_id":"50","user_id":"31","comment_id":null,"dt_added":"2017-04-25 12:46:34","dt_modified":"2017-04-25 12:46:34","status":"A"}]}]
     */

    private int status;
    private String message;
    private List<DataBean> data;

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public List<DataBean> getData() {
        return data;
    }

    public void setData(List<DataBean> data) {
        this.data = data;
    }

    public static class DataBean {
        /**
         * id : 50
         * title : GOLDEN TEMPLE- SHRINE FOR HUMANITY
         * slug : golden-temple-shrine-for-humanity-902398593
         * short_description : Golden temple, Amritsar
         * description : Sri Harimandir Sahib, Amritsar is not only the biggest religious place for the Sikhs, but also a symbol of human brotherhood and equality. It depicts the district identity, glory and heritage of the Sikhs. Irrespective of one's caste, creed or race they can seek spiritual solace and religious fulfillment without any obstacle.
         The Golden Temple, Amritsar, India has a very different architecture as it's built at a level lower than the surrounding land level. Gurudwara teaches the lesson of egalitarianism and humility. Four entrance of this holy place from all four directions, signify that people belonging to every walk of life are equally welcome.

         HOW TO REACH AND ACCOMMODATIONS:

         By Air- Amritsar has an extremely developed airport. Raja Saini International Airport located nearly 11km away from the city centre. There is also another airport Guru Ram Das International Airport that operates flights from different corners of the world.

         By Rail- Having an extensive rail network, Amritsar is connected to all the major cities of the country. If you are traveling from Delhi, Shatabdi Express is the best option with 6 hours of train journey. The railway station of Amritsar is centrally located of hardly 15 minutes’ drive from The Golden Temple. With high tourist traffic, there are facilities of ATM centre and tourist facility centre.

         By Road- With the broad network of roads running through Amritsar, the connectivity is well versed. Grand Trunk Karnal Road connects Delhi to Amritsar. Some daily direct buses operate to Jammu, Katra, Chandigarh and Dharmshala. Amritsar is adequately connected by road to both the places within Punjab as well as outside the state.
         Accommodation- Seven Niwas Asthans (Inns) are provided lodging facilities for pilgrims, maintained by Sri Harimandir Sahib Authorities. Bookings is made on the basis of first-come-first-serve. The seven inns are, as follows:
         1. Guru Ram Das Niwas
         2. Guru Nanak Niwas
         3. Guru Arjan Dev Niwas
         4. New Akal Rest House
         5. Guru Hargobind Nawas
         6. Mata Ganga Ji Niwas
         7. Guru Gobind Singh NRI Yatra Niwas


         GURU KA LANGAR:
         This tradition was initiated by Guru Nanak Dev Ji, later established by the 3rd Guru Sri Guru Amar Dass Ji at Goindwal. Mughal King Akbar came and sat among the odinary people to share langar.
         This community kitchen has helped the community in many ways. It has ensured the participation of women and children in the service of mankind. Women play a very important role in the preparations of meals and children help in serving food to the pangat. Langar has played a great role in upholding the virtue of sameness of all human beings, giving a welcome, secure and protected place.
         Normal families also volunteer to provide and prepare langar. Everyone is welcome here to share the Langar. Lastly, all the preparations, cooking and washing is done by volunteers or by voluntary helpers (Sewadars).
         The Golden Temple Community Kitchen has an average of 75,000 devotees and tourists who come in for langar in the Community Kitchen daily, the number almost double on special occasions. On an average day, 100 Quintal wheat flour, 25 quintal cereals, 10 quintal rice, 5000 litre milk, 10 quintal sugar, 5 quintal pure ghee is use. Approximately, 100 LPG gas cylinders are used to prepare the meals with 100 employees and devotees in service to the kitchen.








         * banner : http://instafeed.org/storage/blog_banner/256x170-295954d7dfafe569bc8eaddbb2175706.jpg
         * banner_zoom : http://instafeed.org/storage/blog_banner/748x420-295954d7dfafe569bc8eaddbb2175706.jpg
         * banner_original : http://instafeed.org/storage/blog_banner/295954d7dfafe569bc8eaddbb2175706.jpg
         * banner_330x210 : http://instafeed.org/storage/blog_banner/330x210-295954d7dfafe569bc8eaddbb2175706.jpg
         * banner_80x80 : http://instafeed.org/storage/blog_banner/80x80-295954d7dfafe569bc8eaddbb2175706.jpg
         * banner_264x200 : http://instafeed.org/storage/blog_banner/264x200-295954d7dfafe569bc8eaddbb2175706.jpg
         * banner_748x420 : http://instafeed.org/storage/blog_banner/748x420-295954d7dfafe569bc8eaddbb2175706.jpg
         * banner_360x200 : http://instafeed.org/storage/blog_banner/360x200-295954d7dfafe569bc8eaddbb2175706.jpg
         * banner_256x170 : http://instafeed.org/storage/blog_banner/256x170-295954d7dfafe569bc8eaddbb2175706.jpg
         * banner_100x100 : http://instafeed.org/storage/blog_banner/100x100-295954d7dfafe569bc8eaddbb2175706.jpg
         * tags : null
         * blog_category_id : 2
         * total_comments : 1
         * total_views : 861
         * user_id : 2
         * dt_added : 2017-04-21 14:49:34
         * dt_modified : 2017-07-22 12:57:54
         * status : P
         * comments : [{"id":"2","comment":"beautiful","blog_post_id":"50","user_id":"31","comment_id":null,"dt_added":"2017-04-25 12:46:34","dt_modified":"2017-04-25 12:46:34","status":"A"}]
         */

        private String id;
        private String title;
        private String slug;
        private String short_description;
        private String description;
        private String banner;
        private String banner_zoom;
        private String banner_original;
        private String banner_330x210;
        private String banner_80x80;
        private String banner_264x200;
        private String banner_748x420;
        private String banner_360x200;
        private String banner_256x170;
        private String banner_100x100;
        private Object tags;
        private String blog_category_id;
        private String total_comments;
        private String total_views;
        private String user_id;
        private String dt_added;
        private String dt_modified;
        private String status;
        private List<CommentsBean> comments;

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getTitle() {
            return title;
        }

        public void setTitle(String title) {
            this.title = title;
        }

        public String getSlug() {
            return slug;
        }

        public void setSlug(String slug) {
            this.slug = slug;
        }

        public String getShort_description() {
            return short_description;
        }

        public void setShort_description(String short_description) {
            this.short_description = short_description;
        }

        public String getDescription() {
            return description;
        }

        public void setDescription(String description) {
            this.description = description;
        }

        public String getBanner() {
            return banner;
        }

        public void setBanner(String banner) {
            this.banner = banner;
        }

        public String getBanner_zoom() {
            return banner_zoom;
        }

        public void setBanner_zoom(String banner_zoom) {
            this.banner_zoom = banner_zoom;
        }

        public String getBanner_original() {
            return banner_original;
        }

        public void setBanner_original(String banner_original) {
            this.banner_original = banner_original;
        }

        public String getBanner_330x210() {
            return banner_330x210;
        }

        public void setBanner_330x210(String banner_330x210) {
            this.banner_330x210 = banner_330x210;
        }

        public String getBanner_80x80() {
            return banner_80x80;
        }

        public void setBanner_80x80(String banner_80x80) {
            this.banner_80x80 = banner_80x80;
        }

        public String getBanner_264x200() {
            return banner_264x200;
        }

        public void setBanner_264x200(String banner_264x200) {
            this.banner_264x200 = banner_264x200;
        }

        public String getBanner_748x420() {
            return banner_748x420;
        }

        public void setBanner_748x420(String banner_748x420) {
            this.banner_748x420 = banner_748x420;
        }

        public String getBanner_360x200() {
            return banner_360x200;
        }

        public void setBanner_360x200(String banner_360x200) {
            this.banner_360x200 = banner_360x200;
        }

        public String getBanner_256x170() {
            return banner_256x170;
        }

        public void setBanner_256x170(String banner_256x170) {
            this.banner_256x170 = banner_256x170;
        }

        public String getBanner_100x100() {
            return banner_100x100;
        }

        public void setBanner_100x100(String banner_100x100) {
            this.banner_100x100 = banner_100x100;
        }

        public Object getTags() {
            return tags;
        }

        public void setTags(Object tags) {
            this.tags = tags;
        }

        public String getBlog_category_id() {
            return blog_category_id;
        }

        public void setBlog_category_id(String blog_category_id) {
            this.blog_category_id = blog_category_id;
        }

        public String getTotal_comments() {
            return total_comments;
        }

        public void setTotal_comments(String total_comments) {
            this.total_comments = total_comments;
        }

        public String getTotal_views() {
            return total_views;
        }

        public void setTotal_views(String total_views) {
            this.total_views = total_views;
        }

        public String getUser_id() {
            return user_id;
        }

        public void setUser_id(String user_id) {
            this.user_id = user_id;
        }

        public String getDt_added() {
            return dt_added;
        }

        public void setDt_added(String dt_added) {
            this.dt_added = dt_added;
        }

        public String getDt_modified() {
            return dt_modified;
        }

        public void setDt_modified(String dt_modified) {
            this.dt_modified = dt_modified;
        }

        public String getStatus() {
            return status;
        }

        public void setStatus(String status) {
            this.status = status;
        }

        public List<CommentsBean> getComments() {
            return comments;
        }

        public void setComments(List<CommentsBean> comments) {
            this.comments = comments;
        }

        public static class CommentsBean {
            /**
             * id : 2
             * comment : beautiful
             * blog_post_id : 50
             * user_id : 31
             * comment_id : null
             * dt_added : 2017-04-25 12:46:34
             * dt_modified : 2017-04-25 12:46:34
             * status : A
             */

            private String id;
            private String comment;
            private String blog_post_id;
            private String user_id;
            private Object comment_id;
            private String dt_added;
            private String dt_modified;
            private String status;

            public String getAvatar() {
                return avatar;
            }

            public void setAvatar(String avatar) {
                this.avatar = avatar;
            }

            public String getUsername() {
                return username;
            }

            public void setUsername(String username) {
                this.username = username;
            }

            private String avatar;
            private String username;

            public String getId() {
                return id;
            }

            public void setId(String id) {
                this.id = id;
            }

            public String getComment() {
                return comment;
            }

            public void setComment(String comment) {
                this.comment = comment;
            }

            public String getBlog_post_id() {
                return blog_post_id;
            }

            public void setBlog_post_id(String blog_post_id) {
                this.blog_post_id = blog_post_id;
            }

            public String getUser_id() {
                return user_id;
            }

            public void setUser_id(String user_id) {
                this.user_id = user_id;
            }

            public Object getComment_id() {
                return comment_id;
            }

            public void setComment_id(Object comment_id) {
                this.comment_id = comment_id;
            }

            public String getDt_added() {
                return dt_added;
            }

            public void setDt_added(String dt_added) {
                this.dt_added = dt_added;
            }

            public String getDt_modified() {
                return dt_modified;
            }

            public void setDt_modified(String dt_modified) {
                this.dt_modified = dt_modified;
            }

            public String getStatus() {
                return status;
            }

            public void setStatus(String status) {
                this.status = status;
            }
        }
    }
}
