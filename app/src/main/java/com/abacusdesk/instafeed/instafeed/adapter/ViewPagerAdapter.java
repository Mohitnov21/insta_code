package com.abacusdesk.instafeed.instafeed.adapter;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.view.PagerAdapter;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.MediaController;
import android.widget.SeekBar;
import android.widget.TextView;

import com.abacusdesk.instafeed.instafeed.R;
import com.abacusdesk.instafeed.instafeed.fragments.ZoomImage;
import com.squareup.picasso.Picasso;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.concurrent.TimeUnit;

import cn.jzvd.JZVideoPlayer;
import cn.jzvd.JZVideoPlayerStandard;
import nl.changer.audiowife.AudioWife;

public class ViewPagerAdapter extends PagerAdapter {

    private LayoutInflater inflater;
    private Context context;
    ArrayList<HashMap<String, String>> arrayhashmap;

    private String type="";
    //JCVideoPlayer jcVideoPlayer;
    static String audioUrl="";
    public  SeekBar seekBar;
    private double startTime = 0;
    private double finalTime = 0;
    private int forwardTime = 5000;
    private int backwardTime = 5000;
    public  int oneTimeOnly = 0;

    private TextView txttime,txttimeend;
    private Boolean isplay=false;
    static MediaController mediaController;
    public  ImageView btnplay,imgbackward,imgforward;
    public  boolean playPause;
    public  MediaPlayer mediaPlayer;
    boolean intialStage = true;
    public static JZVideoPlayerStandard jzVideoPlayerStandard;
    Handler myHandler = new Handler();
    public ViewPagerAdapter(Context context, ArrayList<HashMap<String, String>> arrayhashmap,String type) {
        this.context = context;
        this.type = type;
        this.arrayhashmap = arrayhashmap;
        inflater = LayoutInflater.from(context);
        notifyDataSetChanged();
    }

    @Override
    public int getCount() {
        return arrayhashmap.size();
    }

    @Override
    public Object instantiateItem(ViewGroup view, final int position) {
        View imageLayout;
        if (arrayhashmap.get(position).containsKey("video")) {
            imageLayout = inflater.inflate(R.layout.item_videoview,view, false);
            jzVideoPlayerStandard = (JZVideoPlayerStandard)
                    imageLayout.findViewById(R.id.videocontroller1);
            jzVideoPlayerStandard.setUp(arrayhashmap.get(position).get("video")
                    , JZVideoPlayerStandard.SCREEN_WINDOW_NORMAL, "");
            jzVideoPlayerStandard.mRetryBtn.setText("Retry");
          //jzVideoPlayerStandard.onStateError();
            JZVideoPlayer.WIFI_TIP_DIALOG_SHOWED=true;
            Picasso.with(imageLayout.getContext())
                    .load(arrayhashmap.get(position).get("image"))
                    .into(jzVideoPlayerStandard.thumbImageView);
            view.addView(imageLayout);
        } else if(arrayhashmap.get(position).containsKey("audio")){
            imageLayout = inflater.inflate(R.layout.item_audio, view, false);
            seekBar=(SeekBar)imageLayout.findViewById(R.id.seek_bar);
            seekBar.setClickable(true);
            seekBar.getProgressDrawable().setColorFilter(Color.WHITE, PorterDuff.Mode.SRC_IN);
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
                seekBar.getThumb().setColorFilter(Color.RED, PorterDuff.Mode.SRC_IN);
            }
            btnplay=(ImageView)imageLayout.findViewById(R.id.btn_play);
            imgbackward=(ImageView)imageLayout.findViewById(R.id.img_backward);
            imgforward=(ImageView)imageLayout.findViewById(R.id.img_forward);

            txttime=(TextView)imageLayout.findViewById(R.id.txt_time);
            txttimeend=(TextView)imageLayout.findViewById(R.id.txt_timeend);

            imgforward.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    int temp = (int)startTime;

                    if((temp+forwardTime)<=finalTime){
                        startTime = startTime + forwardTime;
                        mediaPlayer.seekTo((int) startTime);
                    }else{
                        Log.e("cannot","");
                    }
                }
            });

            imgbackward.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    int temp = (int)startTime;

                    if((temp-backwardTime)>0){
                        startTime = startTime - backwardTime;
                        mediaPlayer.seekTo((int) startTime);
                    }else{

                    }
                }
            });
            btnplay.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (!playPause) {
                        audioUrl=arrayhashmap.get(position).get("audio");
                        btnplay.setImageResource(R.drawable.img_btn_pause_pressed);
                        if (intialStage)
                            new Player()
                                    .execute(audioUrl);
                        else {
                            if (!mediaPlayer.isPlaying())
                                mediaPlayer.start();
                        }
                        playPause = true;
                    } else {
                        btnplay.setImageResource(R.drawable.img_btn_play_pressed);
                        if (mediaPlayer.isPlaying())
                            mediaPlayer.pause();
                        playPause = false;
                    }
                }
            });
           /* jzVideoPlayerStandard = (JZVideoPlayerStandard)
                    imageLayout.findViewById(R.id.videocontroller1);
            jzVideoPlayerStandard.setUp(arrayhashmap.get(position).get("audio")
                    , JZVideoPlayerStandard.SCREEN_WINDOW_NORMAL,"");
            jzVideoPlayerStandard.mRetryBtn.setText("Retry");
            //jzVideoPlayerStandard.setAllControlsVisiblity(1,1,1,1,1,1,0);
            Picasso.with(imageLayout.getContext())
                    .load(R.drawable.audio_black)
                    .into(jzVideoPlayerStandard.thumbImageView);
            // jzVideoPlayerStandard.showWifiDialog(0);
            JZVideoPlayer.WIFI_TIP_DIALOG_SHOWED=true;*/
            view.addView(imageLayout);
        }
        else {
            imageLayout = inflater.inflate(R.layout.item_viewimage, view, false);
            final ImageView imgview = (ImageView) imageLayout.findViewById(R.id.img_view);

            if (type.equalsIgnoreCase("news")){
                if (!arrayhashmap.get(position).get("image").isEmpty()) {
                    Picasso.with(context).load(arrayhashmap.get(position).get("image")).error(R.drawable.newsdefault).into(imgview);
                }
            }else if (type.equalsIgnoreCase("talents")){
                if (!arrayhashmap.get(position).get("image").isEmpty()) {
                    Picasso.with(context).load(arrayhashmap.get(position).get("image")).error(R.drawable.talentsdefault).into(imgview);
                }
            }else if (type.equalsIgnoreCase("explore")){
                if (!arrayhashmap.get(position).get("image").isEmpty()) {
                    Picasso.with(context).load(arrayhashmap.get(position).get("image")).error(R.drawable.exploredefault).into(imgview);
                }
            }else if (type.equalsIgnoreCase("opinions")){
                if (!arrayhashmap.get(position).get("image").isEmpty()) {
                    Picasso.with(context).load(arrayhashmap.get(position).get("image")).error(R.drawable.opiniondefault).into(imgview);
                }
            }else if (type.equalsIgnoreCase("petitions")){
                if (!arrayhashmap.get(position).get("image").isEmpty()) {
                    Picasso.with(context).load(arrayhashmap.get(position).get("image")).error(R.drawable.petitiondefault).into(imgview);
                }
            }else {
                if (!arrayhashmap.get(position).get("image").isEmpty()) {
                    Picasso.with(context).load(arrayhashmap.get(position).get("image")).error(R.drawable.noimage).into(imgview);
                }
            }

            Log.e("arrayimage",""+arrayhashmap);
            imgview.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                 // ZoomImage.imageShow=arrayhashmap.get(position).get("imagezoom");
                    Bundle bundle = new Bundle();
                    bundle.putSerializable("arrayimages",arrayhashmap);
                    bundle.putString("position",""+position);

                    Intent intent=new Intent(context,ZoomImage.class);
                    intent.putExtras(bundle);
                    context.startActivity(intent);
                }
            });
              view.addView(imageLayout);
            }
            return imageLayout;
        }

    class Player extends AsyncTask<String, Void, Boolean> {
        private ProgressDialog progress;

        @Override
        protected Boolean doInBackground(String... params) {
            // TODO Auto-generated method stub
            Boolean prepared;
            try {

                mediaPlayer = new MediaPlayer();
                mediaPlayer.setAudioStreamType(AudioManager.STREAM_MUSIC);
                mediaPlayer.setDataSource(params[0]);
                mediaPlayer.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
                    @Override
                    public void onCompletion(MediaPlayer mp) {
                        // TODO Auto-generated method stub
                        intialStage = true;
                        playPause=false;
                        btnplay.setImageResource(R.drawable.img_btn_play_pressed);
                        mediaPlayer.stop();
                        mediaPlayer.reset();
                        txttime.setText("00:00");
                        myHandler.removeCallbacksAndMessages(null);

                    }
                });
                mediaPlayer.prepare();
                prepared = true;
            } catch (IllegalArgumentException e) {
                Log.d("IllegarArgument", e.getMessage());
                prepared = false;
                e.printStackTrace();
            } catch (SecurityException e) {
                // TODO Auto-generated catch block
                prepared = false;
                e.printStackTrace();
            } catch (IllegalStateException e) {
                // TODO Auto-generated catch block
                prepared = false;
                e.printStackTrace();
            } catch (IOException e) {
                // TODO Auto-generated catch block
                prepared = false;
                e.printStackTrace();
            }
            return prepared;
        }

        @Override
        protected void onPostExecute(Boolean result) {
            // TODO Auto-generated method stub
            super.onPostExecute(result);
            if (progress.isShowing()) {
                progress.cancel();
            }
            Log.d("Prepared", "//" + result);
            mediaPlayer.start();

            finalTime = mediaPlayer.getDuration();
            startTime = mediaPlayer.getCurrentPosition();
            Log.e("meda final",""+finalTime);
            if (oneTimeOnly == 0) {
                seekBar.setMax((int) finalTime);
                oneTimeOnly = 1;
            }
            txttimeend.setText(String.format("%d min, %d sec",
                    TimeUnit.MILLISECONDS.toMinutes((long) finalTime),
                    TimeUnit.MILLISECONDS.toSeconds((long) finalTime) -
                            TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes((long)
                                    finalTime)))
            );

            txttime.setText(String.format("%d min, %d sec",
                    TimeUnit.MILLISECONDS.toMinutes((long) startTime),
                    TimeUnit.MILLISECONDS.toSeconds((long) startTime) -
                            TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes((long)
                                    startTime)))
            );
            seekBar.setProgress((int)startTime);
            myHandler.postDelayed(UpdateSongTime,100);
            intialStage = false;
        }

        private Runnable UpdateSongTime = new Runnable() {
            public void run() {
                startTime = mediaPlayer.getCurrentPosition();
                   // Log.e("timexurrent",""+startTime);
                    txttime.setText(String.format("%d min, %d sec",
                            TimeUnit.MILLISECONDS.toMinutes((long) startTime),
                            TimeUnit.MILLISECONDS.toSeconds((long) startTime) -
                                    TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.
                                            toMinutes((long) startTime)))
                    );
                    seekBar.setProgress((int)startTime);
                    myHandler.postDelayed(this, 100);
           }
        };

        public Player() {
            progress = new ProgressDialog(context);
        }
 }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == object;
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        container.removeView((View) object);
    }

}