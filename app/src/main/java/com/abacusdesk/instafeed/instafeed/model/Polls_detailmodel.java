package com.abacusdesk.instafeed.instafeed.model;

import java.util.List;

/**
 * Created by abacusdesk on 2017-08-10.
 */

public class Polls_detailmodel {


    /**
     * status : 200
     * message : success
     * data : [{"id":"48","title":"Hindi Films Are Reflective of Societal Mores in Unnoticed Ways","description":"Literature is supposed to mirror society. Though few people realize it, Hindi films are more reflective of the customs and values of the society they grow out of, as well as the changes occurring within it than most TV serials. ","location_id":"187","is_loc_poll":null,"user_id":"2","slug":"hindi-films-are-reflective-of-societal-mores-in-unnoticed-ways-235458957","dt_start":"2017-08-07 18:08:16","dt_end":"2017-08-20 18:08:16","total_views":"0","dt_added":"2017-08-10 12:41:30","dt_modified":"2017-08-10 13:08:15","status":"A"},{"options":[{"id":"133","poll_id":"48","option_title":"You\u2019ve gone incognito","description":"","sort_order":"0","votes":null,"user_id":"2","dt_added":"2017-08-10 12:41:30","dt_modified":"2017-08-10 13:07:44","status":"A"},{"id":"134","poll_id":"48","option_title":"How private browsing works","description":"","sort_order":"1","votes":"2","user_id":"2","dt_added":"2017-08-10 12:41:30","dt_modified":"2017-08-10 13:07:44","status":"A"},{"id":"135","poll_id":"48","option_title":"You\u2019ve gone incognito","description":"","sort_order":"2","votes":"2","user_id":"2","dt_added":"2017-08-10 12:41:30","dt_modified":"2017-08-10 13:07:44","status":"A"},{"id":"136","poll_id":"48","option_title":"How private browsing works","description":"","sort_order":"3","votes":"1","user_id":"2","dt_added":"2017-08-10 12:41:30","dt_modified":"2017-08-10 13:07:44","status":"A"}]},{"votes":{"total_votes":"6"}}]
     */

    private int status;
    private String message;
    private List<DataBean> data;

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public List<DataBean> getData() {
        return data;
    }

    public void setData(List<DataBean> data) {
        this.data = data;
    }

    public static class DataBean {
        /**
         * id : 48
         * title : Hindi Films Are Reflective of Societal Mores in Unnoticed Ways
         * description : Literature is supposed to mirror society. Though few people realize it, Hindi films are more reflective of the customs and values of the society they grow out of, as well as the changes occurring within it than most TV serials.
         * location_id : 187
         * is_loc_poll : null
         * user_id : 2
         * slug : hindi-films-are-reflective-of-societal-mores-in-unnoticed-ways-235458957
         * dt_start : 2017-08-07 18:08:16
         * dt_end : 2017-08-20 18:08:16
         * total_views : 0
         * dt_added : 2017-08-10 12:41:30
         * dt_modified : 2017-08-10 13:08:15
         * status : A
         * options : [{"id":"133","poll_id":"48","option_title":"You\u2019ve gone incognito","description":"","sort_order":"0","votes":null,"user_id":"2","dt_added":"2017-08-10 12:41:30","dt_modified":"2017-08-10 13:07:44","status":"A"},{"id":"134","poll_id":"48","option_title":"How private browsing works","description":"","sort_order":"1","votes":"2","user_id":"2","dt_added":"2017-08-10 12:41:30","dt_modified":"2017-08-10 13:07:44","status":"A"},{"id":"135","poll_id":"48","option_title":"You\u2019ve gone incognito","description":"","sort_order":"2","votes":"2","user_id":"2","dt_added":"2017-08-10 12:41:30","dt_modified":"2017-08-10 13:07:44","status":"A"},{"id":"136","poll_id":"48","option_title":"How private browsing works","description":"","sort_order":"3","votes":"1","user_id":"2","dt_added":"2017-08-10 12:41:30","dt_modified":"2017-08-10 13:07:44","status":"A"}]
         * votes : {"total_votes":"6"}
         */

        private String id;
        private String title;
        private String description;
        private String location_id;
        private Object is_loc_poll;
        private String user_id;
        private String slug;
        private String dt_start;
        private String dt_end;
        private String total_views;
        private String dt_added;
        private String dt_modified;
        private String status;
        private VotesBean votes;
        private List<OptionsBean> options;

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getTitle() {
            return title;
        }

        public void setTitle(String title) {
            this.title = title;
        }

        public String getDescription() {
            return description;
        }

        public void setDescription(String description) {
            this.description = description;
        }

        public String getLocation_id() {
            return location_id;
        }

        public void setLocation_id(String location_id) {
            this.location_id = location_id;
        }

        public Object getIs_loc_poll() {
            return is_loc_poll;
        }

        public void setIs_loc_poll(Object is_loc_poll) {
            this.is_loc_poll = is_loc_poll;
        }

        public String getUser_id() {
            return user_id;
        }

        public void setUser_id(String user_id) {
            this.user_id = user_id;
        }

        public String getSlug() {
            return slug;
        }

        public void setSlug(String slug) {
            this.slug = slug;
        }

        public String getDt_start() {
            return dt_start;
        }

        public void setDt_start(String dt_start) {
            this.dt_start = dt_start;
        }

        public String getDt_end() {
            return dt_end;
        }

        public void setDt_end(String dt_end) {
            this.dt_end = dt_end;
        }

        public String getTotal_views() {
            return total_views;
        }

        public void setTotal_views(String total_views) {
            this.total_views = total_views;
        }

        public String getDt_added() {
            return dt_added;
        }

        public void setDt_added(String dt_added) {
            this.dt_added = dt_added;
        }

        public String getDt_modified() {
            return dt_modified;
        }

        public void setDt_modified(String dt_modified) {
            this.dt_modified = dt_modified;
        }

        public String getStatus() {
            return status;
        }

        public void setStatus(String status) {
            this.status = status;
        }

        public VotesBean getVotes() {
            return votes;
        }

        public void setVotes(VotesBean votes) {
            this.votes = votes;
        }

        public List<OptionsBean> getOptions() {
            return options;
        }

        public void setOptions(List<OptionsBean> options) {
            this.options = options;
        }

        public static class VotesBean {
            /**
             * total_votes : 6
             */

            private String total_votes;

            public String getTotal_votes() {
                return total_votes;
            }

            public void setTotal_votes(String total_votes) {
                this.total_votes = total_votes;
            }
        }

        public static class OptionsBean {
            /**
             * id : 133
             * poll_id : 48
             * option_title : You’ve gone incognito
             * description :
             * sort_order : 0
             * votes : null
             * user_id : 2
             * dt_added : 2017-08-10 12:41:30
             * dt_modified : 2017-08-10 13:07:44
             * status : A
             */

            private String id;
            private String poll_id;
            private String option_title;
            private String description;
            private String sort_order;
            private Object votes;
            private String user_id;
            private String dt_added;
            private String dt_modified;
            private String status;

            public String getId() {
                return id;
            }

            public void setId(String id) {
                this.id = id;
            }

            public String getPoll_id() {
                return poll_id;
            }

            public void setPoll_id(String poll_id) {
                this.poll_id = poll_id;
            }

            public String getOption_title() {
                return option_title;
            }

            public void setOption_title(String option_title) {
                this.option_title = option_title;
            }

            public String getDescription() {
                return description;
            }

            public void setDescription(String description) {
                this.description = description;
            }

            public String getSort_order() {
                return sort_order;
            }

            public void setSort_order(String sort_order) {
                this.sort_order = sort_order;
            }

            public Object getVotes() {
                return votes;
            }

            public void setVotes(Object votes) {
                this.votes = votes;
            }

            public String getUser_id() {
                return user_id;
            }

            public void setUser_id(String user_id) {
                this.user_id = user_id;
            }

            public String getDt_added() {
                return dt_added;
            }

            public void setDt_added(String dt_added) {
                this.dt_added = dt_added;
            }

            public String getDt_modified() {
                return dt_modified;
            }

            public void setDt_modified(String dt_modified) {
                this.dt_modified = dt_modified;
            }

            public String getStatus() {
                return status;
            }

            public void setStatus(String status) {
                this.status = status;
            }
        }
    }
}
