package com.abacusdesk.instafeed.instafeed;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by codecube on 18/5/17.
 */

public class Screen implements Serializable{

    private String name;

   /* public Screen(String name, String id) {
        this.name = name;
        this.id = id;
    }
*/
    private String id;
    private ArrayList<String> ouList = new ArrayList<>();
    private ArrayList<SubScreen> subScreens = new ArrayList<>();

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public ArrayList<String> getOuList() {
        return ouList;
    }

    public void setOuList(ArrayList<String> ouList) {
        this.ouList = ouList;
    }

    public ArrayList<SubScreen> getSubScreens() {
        return subScreens;
    }

    public void setSubScreens(ArrayList<SubScreen> subScreens) {
        this.subScreens = subScreens;
    }
}
