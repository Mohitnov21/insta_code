package com.abacusdesk.instafeed.instafeed.fragments;

import android.content.DialogInterface;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.abacusdesk.instafeed.instafeed.R;
import com.abacusdesk.instafeed.instafeed.Util.Alerts;
import com.abacusdesk.instafeed.instafeed.Util.Apis;
import com.abacusdesk.instafeed.instafeed.Util.CommonFunctions;
import com.abacusdesk.instafeed.instafeed.Util.SaveSharedPreference;
import com.abacusdesk.instafeed.instafeed.adapter.Blog_Adapter;
import com.abacusdesk.instafeed.instafeed.adapter.Blog_profileAdapter;
import com.abacusdesk.instafeed.instafeed.model.Blog_model;
import com.abacusdesk.instafeed.instafeed.webservice.VolleySingleton;
import com.android.volley.AuthFailureError;
import com.android.volley.NoConnectionError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by ishaan on 6/30/2017.
 */


import com.abacusdesk.instafeed.instafeed.custom_dialog.Dialogs;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;


    /**
     * Created by ishaan on 6/19/2017.
     */

    public class frag_instablog  extends Fragment {

        private View view;
        Blog_model blog_model;
        private static RecyclerView recyclerView;
        ArrayList<Blog_model.DataBean> arrayList= new ArrayList<>();
        Blog_profileAdapter adapter;
        public  String  Api_temp="";
        Map<String, String> map ;
        TextView txtrecord;

        @Nullable
        @Override
        public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
            View view=inflater.inflate(R.layout.frag_blogslist,container,false);
            recyclerView = (RecyclerView) view.findViewById(R.id.recyclerView);

            txtrecord = (TextView) view.findViewById(R.id.txt_records);
            if (Publicprofile_Fragment.flag==true){
                Api_temp=Apis.public_blogs;
                map=new HashMap<>();
                map.put("username",Publicprofile_Fragment.username);
                Log.e("printmappubliblog",""+map);
            }else {
                Api_temp=Apis.private_blogs;
                // map.clear();
                map=new HashMap<>();
                map.put("user", SaveSharedPreference.getUserID(getActivity()));
                map.put("token",SaveSharedPreference.getPrefToken(getActivity()));
                Log.e("printmaprivateblog",""+map);
            }
            if (CommonFunctions.isConnected(getActivity())){
                if (!arrayList.isEmpty()){
                    arrayList.clear();
                }
                getBloglist();
            }else {
                Toast.makeText(getActivity(), "Internet not available", Toast.LENGTH_SHORT).show();
            }
            setRecyclerView();
            return view;
        }

        public void getBloglist() {
            //Dialogs.showProDialog(getActivity(), "Loading");
            StringRequest stringRequest = new StringRequest(Request.Method.POST, Api_temp,
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {
                            Log.e("response_blog", "" + response);
                            try {
                                getResponse(response);
                            } catch (JSONException e) {
                              //  e.printStackTrace();
                            }
                            Dialogs.disDialog();
                        }
                    },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            DialogInterface.OnClickListener onClickTryAgain = new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    //  Dialogs.disDialog();
                                    getBloglist();
                                }
                            };
                            if (error instanceof TimeoutError) {
                                //  Dialogs.disDialog();
                                Alerts.timeoutErrorAlert(getContext(), onClickTryAgain);
                            } else if (error instanceof NoConnectionError) {
                                //  Dialogs.disDialog();
                                Alerts.internetConnectionErrorAlert(getContext(), onClickTryAgain);
                            }
                        }
                    })
            {
                @Override
                protected Map<String, String> getParams() throws AuthFailureError {
                    return map;
                }

                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    Map<String, String> params = new HashMap<String, String>();
                    params.put("Content-Type", "application/x-www-form-urlencoded");
                    return params;
                }
            };
            VolleySingleton.getInstance(getActivity()).addToRequestQueue(stringRequest);
        }

        private void getResponse(String response ) throws JSONException {
            if(!response.equals(null)) {
                Log.e("log", "" + response);
                JSONObject jsonObject = new JSONObject(response);
                Object object = jsonObject.get("data");
                if (object instanceof JSONArray) {
                    Gson gson = new Gson();
                    blog_model = gson.fromJson(response, Blog_model.class);
                    int status = blog_model.getStatus();
                    String msg = blog_model.getMessage();
                    if (msg.equalsIgnoreCase("success") && status == 200) {
                        if (blog_model.getData() != null && !blog_model.getData().isEmpty()) {
                            for (int i = 0; i < blog_model.getData().size(); i++) {
                                try {
                                    Blog_model.DataBean dataBean = blog_model.getData().get(i);
                                    arrayList.add(dataBean);
                                } catch (Exception e) {
                                   // e.printStackTrace();
                                }
                            }
                        }
                        if (arrayList.isEmpty()) {
                            txtrecord.setVisibility(View.VISIBLE);
                        }
                      //  adapter.notifyDataSetChanged();
                    }
                }
            }
        }

        //Setting recycler view
        private void setRecyclerView() {
            recyclerView.setHasFixedSize(true);
            recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
            try{
                adapter= new Blog_profileAdapter(getActivity(),arrayList);
                recyclerView.setAdapter(adapter);
                adapter.notifyDataSetChanged();
                Log.e("adapter size",""+arrayList);
                //Log.e("responseset",""+newsDataBeanList);
            }catch (Exception e){
               // e.printStackTrace();
            }
        }
    }
