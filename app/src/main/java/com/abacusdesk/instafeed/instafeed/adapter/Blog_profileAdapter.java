package com.abacusdesk.instafeed.instafeed.adapter;

import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.abacusdesk.instafeed.instafeed.R;
import com.abacusdesk.instafeed.instafeed.Util.Alerts;
import com.abacusdesk.instafeed.instafeed.Util.Apis;
import com.abacusdesk.instafeed.instafeed.Util.SaveSharedPreference;
import com.abacusdesk.instafeed.instafeed.custom_dialog.Dialogs;
import com.abacusdesk.instafeed.instafeed.fragments.Blogdetail_fragment;
import com.abacusdesk.instafeed.instafeed.fragments.Explore_detailfragment;
import com.abacusdesk.instafeed.instafeed.fragments.Publicprofile_Fragment;
import com.abacusdesk.instafeed.instafeed.model.Blog_model;
import com.abacusdesk.instafeed.instafeed.model.Explorer_model;
import com.android.volley.AuthFailureError;
import com.android.volley.NoConnectionError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.squareup.picasso.Picasso;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by abacusdesk on 2017-09-18.
 */

public class Blog_profileAdapter  extends RecyclerView.Adapter<Blog_profileAdapter.MyViewHolder>{

        Context context;
        ArrayList<Blog_model.DataBean> arrayList;

        public Blog_profileAdapter(Context context, ArrayList<Blog_model.DataBean> arrayList){
            this.context=context;
            this.arrayList=arrayList;
        }

        public class MyViewHolder extends RecyclerView.ViewHolder {
            public TextView title,txtdescr;
            public ImageView imgpro, imgshare;
            public TextView date;
            public LinearLayout lndesc;
            //public LinearLayout lnrow;

            public MyViewHolder(View view) {
                super(view);
                this.title = (TextView) itemView.findViewById(R.id.txt_title);
                this.date = (TextView) itemView.findViewById(R.id.txt_date);
                this.imgshare = (ImageView) itemView.findViewById(R.id.img_share);
                this.imgpro = (ImageView) itemView.findViewById(R.id.img_pro);

                this.txtdescr = (TextView) itemView.findViewById(R.id.txt_descr);
                this.lndesc = (LinearLayout) itemView.findViewById(R.id.ln_desc);
            }
        }

        @Override
        public Blog_profileAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View itemView = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.item_profilelist, parent, false);
            return new Blog_profileAdapter.MyViewHolder(itemView);
        }

        @Override
        public void onBindViewHolder(Blog_profileAdapter.MyViewHolder holder, final int position) {
            String myFormat = "yyyy-MM-dd HH:mm:ss";
            DateFormat sdformat = new SimpleDateFormat(myFormat);
            DateFormat targetFormat = new SimpleDateFormat("MMMM dd, yyyy");

            final Blog_model.DataBean dataBean = arrayList.get(position);
            holder.txtdescr.setText(dataBean.getShort_description());
            holder.title.setText(dataBean.getTitle());

            try {
                String formattedDate = targetFormat.format(sdformat.parse(dataBean.getDt_added()));
                holder.date.setText(formattedDate);
            } catch (ParseException e) {
                e.printStackTrace();
            }
            try{
                Picasso.with(context).load(dataBean.getImage()).error(R.drawable.demo_news).into(holder.imgpro);
            }catch (Exception e){
            }

            holder.imgshare.setVisibility(View.INVISIBLE);
            holder.lndesc.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    Explore_detailfragment fragment = new Explore_detailfragment();
                    Bundle bundle = new Bundle();
                    bundle.putString("catid",dataBean.getBlog_category_id());
                    bundle.putString("postid",dataBean.getId());
                    fragment.setArguments(bundle);
                    AppCompatActivity activity = (AppCompatActivity) v.getContext();
                    activity.getSupportFragmentManager().beginTransaction().add(R.id.frame_container, fragment,"tempfrag").addToBackStack(null).commit();

                }
            });
        }

        @Override
        public int getItemCount() {
            return arrayList.size();
        }
    }