package com.abacusdesk.instafeed.instafeed.fontsClass;

import android.content.Context;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.widget.TextView;

/**
 * Created by abacusdesk on 2017-10-04.
 */

public class Content_regular  extends TextView {

        public Content_regular(Context context, AttributeSet attrs, int defStyle) {
            super(context, attrs, defStyle);
            init();
        }

        public Content_regular(Context context, AttributeSet attrs) {
            super(context, attrs);
            init();
        }

        public Content_regular(Context context) {
            super(context);
            init();
        }

        public void init() {
            Typeface tf = Typeface.createFromAsset(getContext().getAssets(),"fonts/DroidSerif-Regular.ttf");
            setTypeface(tf);

        }
}
