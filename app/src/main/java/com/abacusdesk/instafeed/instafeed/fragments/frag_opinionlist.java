package com.abacusdesk.instafeed.instafeed.fragments;

import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.abacusdesk.instafeed.instafeed.R;
import com.abacusdesk.instafeed.instafeed.Util.Alerts;
import com.abacusdesk.instafeed.instafeed.Util.Apis;
import com.abacusdesk.instafeed.instafeed.Util.CommonFunctions;
import com.abacusdesk.instafeed.instafeed.Util.SaveSharedPreference;
import com.abacusdesk.instafeed.instafeed.Util.SimpleDividerItemDecoration;
import com.abacusdesk.instafeed.instafeed.adapter.OpinionAdapter_tab;
import com.abacusdesk.instafeed.instafeed.adapter.Opinion_Adapter;
import com.abacusdesk.instafeed.instafeed.custom_dialog.Dialogs;
import com.abacusdesk.instafeed.instafeed.model.OpinionAll_modeltab;
import com.abacusdesk.instafeed.instafeed.model.OpinionModel_public;
import com.android.volley.AuthFailureError;
import com.android.volley.NoConnectionError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.gson.Gson;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by ishaan on 6/21/2017.
 */

public class frag_opinionlist extends Fragment {

    View view;
    RecyclerView recyclerView;
    OpinionAdapter_tab adapter;

    ArrayList<OpinionAll_modeltab.DataBean> arrayOpinion=new ArrayList<>();
    OpinionAll_modeltab opinionAll_modeltab;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater,
                             @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_news, container, false);
        recyclerView = (RecyclerView) view.findViewById(R.id.recyclerView);
        final SwipeRefreshLayout swipeRefreshLayout = (SwipeRefreshLayout)view.findViewById(R.id.swipe_refresh_layout);
        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                try {
                    if (CommonFunctions.isConnected(getActivity())){
                        getopinionslist();
                    }
                } catch (Exception e) {
                    // e.printStackTrace();
                }
                swipeRefreshLayout.setRefreshing(false);
            }
        });
        if (CommonFunctions.isConnected(getActivity())){
            getopinionslist();
        }else {
            //Toast.makeText(getActivity(), "Internet not available", Toast.LENGTH_SHORT).show();
        }
        setRecyclerView();
        return view;
    }

    public void getopinionslist() {
        String api="";
        if (!SaveSharedPreference.getUserID(getContext()).isEmpty()){
            api=Apis.opinions_all+"/"+SaveSharedPreference.getUserID(getContext());
            Log.e("userid api","called");
        }else {
            api=Apis.opinions_all;
            Log.e("without userid api","called");
        }
        StringRequest stringRequest = new StringRequest(Request.Method.POST,api,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.e("log", "" + response);
                        try {
                            getResponse(response);
                        } catch (JSONException e) {
                           // e.printStackTrace();
                        }
                        Dialogs.disDialog();
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Dialogs.disDialog();
                        DialogInterface.OnClickListener onClickTryAgain = new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                Dialogs.disDialog();
                                getopinionslist();
                            }
                        };
                      if (error instanceof NoConnectionError) {
                            Dialogs.disDialog();
                            Alerts.internetConnectionErrorAlert(getContext(), onClickTryAgain);
                      }
                    }
                })
        {
          @Override
          public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Content-Type", "application/x-www-form-urlencoded");
                return params;
            }
        };
        RequestQueue requestQueue = Volley.newRequestQueue(getActivity());
        requestQueue.add(stringRequest);
    }

    public void refresh_Adapter(){
        Log.e("change refresh","called nav");
        //adapter.notifyDataSetChanged();
        getopinionslist();
    }

    private void getResponse(String response ) throws JSONException {
        if (!arrayOpinion.isEmpty()){
            arrayOpinion.clear();
        }
        if(!response.equals(null)){
            Log.e("log", "" + response);
            JSONObject jsonObject = new JSONObject(response);
            Object object = jsonObject.get("data");
            if (object instanceof JSONArray) {
                Gson gson = new Gson();
                opinionAll_modeltab = gson.fromJson(response, OpinionAll_modeltab.class);
                int status = opinionAll_modeltab.getStatus();
                String msg = opinionAll_modeltab.getMessage();
                if (msg.equalsIgnoreCase("success") && status == 200) {
                    if (opinionAll_modeltab.getData() != null && !opinionAll_modeltab.getData().isEmpty()) {
                        for (int i = 0; i < opinionAll_modeltab.getData().size(); i++) {
                            try {
                                OpinionAll_modeltab.DataBean dataBean = opinionAll_modeltab.getData().get(i);
                                arrayOpinion.add(dataBean);
                            } catch (Exception e) {
                              //  e.printStackTrace();
                            }
                        }
                    }
                    adapter.notifyDataSetChanged();
                }
            }
        }
    }

    private void setRecyclerView() {
        try{
            adapter= new OpinionAdapter_tab(getActivity(),arrayOpinion);
        }  catch (Exception e){
            //e.printStackTrace();
        }

        recyclerView.setHasFixedSize(true);
        LinearLayoutManager layoutManager=new LinearLayoutManager(getActivity());
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setItemViewCacheSize(20);
        recyclerView.setDrawingCacheEnabled(true);
        layoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        recyclerView.setAdapter(adapter);
    }
}
