package com.abacusdesk.instafeed.instafeed.customcamera;

import android.app.Activity;
import android.net.Uri;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;


import com.abacusdesk.instafeed.instafeed.R;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by abacusdesk on 2017-12-19.
 */

public  class SelectedImagesAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

   // private ArrayList<HashMap<String,String>> arrayList;

    private ArrayList<HashMap<String,String>> arrayList;
    private Activity activity;
    private final int NORMAL_ROW = 0;

    public SelectedImagesAdapter(Activity activity, ArrayList<HashMap<String,String>> arrayList) {
        this.activity = activity;
        this.arrayList = arrayList;
    }

    @Override
    public int getItemViewType(int position) {
        return NORMAL_ROW;
    }

    @Override
    public int getItemCount() {
        return (null != arrayList ? arrayList.size() : 0);
    }

    @Override
    public void onBindViewHolder(final RecyclerView.ViewHolder holder, final int position) {

        //((NormalHolder)holder).imgmain.setImageURI(Uri.parse(arrayList.get(position)));

        if (arrayList.get(position).containsKey("image")){
            ((NormalHolder)holder).imgmain
                    .setImageURI(Uri.parse(arrayList.get(position).get("image")));
        }else {
            ((NormalHolder)holder).imgmain
                    .setImageURI(Uri.parse(arrayList.get(position).get("video")));
        }


        ((NormalHolder)holder).imgshade.setVisibility(View.GONE);

        ((NormalHolder)holder).imgmain.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

            }
        });
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = null;
        switch (viewType) {
            case NORMAL_ROW:
                view = LayoutInflater.from(parent.getContext()).inflate(R.layout.itemimage, parent, false);
                return new NormalHolder(view);
        }
        return null;
    }

    public  class NormalHolder extends RecyclerView.ViewHolder {
        public ImageView imgmain;
        public ImageView imgshade;

        public NormalHolder(View itemView) {
            super(itemView);
            this.imgmain = (ImageView) itemView.findViewById(R.id.img_item);
            this.imgshade = (ImageView) itemView.findViewById(R.id.bg_shade);
        }
    }

}
