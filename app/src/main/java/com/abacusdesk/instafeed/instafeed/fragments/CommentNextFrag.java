package com.abacusdesk.instafeed.instafeed.fragments;

import android.content.DialogInterface;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import com.abacusdesk.instafeed.instafeed.NavDrawerActivity;
import com.abacusdesk.instafeed.instafeed.R;
import com.abacusdesk.instafeed.instafeed.Util.Alerts;
import com.abacusdesk.instafeed.instafeed.Util.Apis;
import com.abacusdesk.instafeed.instafeed.Util.SaveSharedPreference;
import com.abacusdesk.instafeed.instafeed.Util.SimpleDividerItemDecoration;
import com.abacusdesk.instafeed.instafeed.adapter.RecyclerCommentAdapter;
import com.abacusdesk.instafeed.instafeed.custom_dialog.Dialogs;
import com.android.volley.AuthFailureError;
import com.android.volley.NoConnectionError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by abacusdesk on 2017-09-04.
 */

public class CommentNextFrag extends Fragment {

        View view;
        RecyclerView recyclerView;
        RecyclerCommentAdapter adapter;
        EditText edcomment;
        ImageView imgsend;
        static String type="";
        String Api_temp="",postid="";
        public static ArrayList<HashMap<String,String>> arrayhashComent=new ArrayList<>();

        @Nullable
        @Override
        public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
            view = inflater.inflate(R.layout.frag_instanewsprofile, container, false);
            recyclerView = (RecyclerView) view.findViewById(R.id.recyclerView);

            Bundle bundle = this.getArguments();
            if (bundle != null) {
                arrayhashComent= (ArrayList<HashMap<String, String>>) bundle.getSerializable("arrayhash");
                type=bundle.getString("type");
                postid=bundle.getString("postid");
            }

            if (type.equalsIgnoreCase("news")){
                Api_temp= Apis.comment;
            }else if (type.equalsIgnoreCase("talents")){
                Api_temp=Apis.talentall_comment;
            }else if (type.equalsIgnoreCase("explorer")){
                Api_temp=Apis.exploreall_comment;
            }else if (type.equalsIgnoreCase("opinions")){
                Api_temp=Apis.opinions_comment;
            }else if (type.equalsIgnoreCase("blogs")){
                Api_temp=Apis.Blogs_comment;
            }else if (type.equalsIgnoreCase("petitions")){
                Api_temp=Apis.petition_comment;
            }
            NavDrawerActivity.actionButton.setVisibility(View.GONE);

            edcomment=(EditText)view.findViewById(R.id.ed_comment);
            imgsend=(ImageView)view.findViewById(R.id.img_send);
            imgsend.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (edcomment.getText().toString().equals("")){
                        Toast.makeText(getContext(),"Comment can't be blank", Toast.LENGTH_SHORT).show();
                    } else if (SaveSharedPreference.getUserID(getContext()).equals("")){
                        Toast.makeText(getContext(),"please Login to continue!", Toast.LENGTH_SHORT).show();
                    }else {
                        postComment();
                    }

                }
            });


            NavDrawerActivity.toolbar.setBackgroundResource(0);
            NavDrawerActivity.toolbar.setLogo(null);
            NavDrawerActivity.toolbar.setBackgroundColor(getResources().getColor(R.color.white));
            NavDrawerActivity.toolbar.setTitle("Comments");
            NavDrawerActivity.toolbar.setTitleTextColor(Color.BLACK);
            view.setFocusableInTouchMode(true);
            view.requestFocus();
            view.setOnKeyListener(new View.OnKeyListener() {
                @Override
                public boolean onKey(View v, int keyCode, KeyEvent event) {
                    if (keyCode == KeyEvent.KEYCODE_BACK) {
                        // ViewPagerAdapter.stopVideo(getActivity());

                        NavDrawerActivity.actionButton.setVisibility(View.VISIBLE);
                        Log.e("backj pressed","back");
                        NavDrawerActivity.toolbar.setBackgroundColor(getResources().getColor(R.color.white));
                        NavDrawerActivity.toolbar.setTitle("");
                        NavDrawerActivity.toolbar.setLogo(R.drawable.instafeed_logospace);
                        NavDrawerActivity.toggle.syncState();
                        getActivity().getSupportFragmentManager().popBackStack();
                        return true;
                    } else {
                        return false;
                    }
                }
            });
            getComments();
            return view;
        }

        @Override
        public void onPause() {
            super.onPause();
            NavDrawerActivity.actionButton.setVisibility(View.VISIBLE);
            Log.e("backj pressed","back");
            NavDrawerActivity.toolbar.setBackgroundColor(getResources().getColor(R.color.white));
            NavDrawerActivity.toolbar.setTitle("");
            NavDrawerActivity.toolbar.setLogo(R.drawable.instafeed_logospace);
            NavDrawerActivity.toggle.syncState();

        }

        private void getComments(){
            try{
                adapter=new RecyclerCommentAdapter(getContext(),arrayhashComent,type);
                recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
                recyclerView.addItemDecoration(new SimpleDividerItemDecoration(getContext()));
                recyclerView.setNestedScrollingEnabled(false);
                recyclerView.setHasFixedSize(false);
                recyclerView.setAdapter(adapter);
                adapter.notifyDataSetChanged();

            } catch (Exception e){
                e.printStackTrace();
            }
        }
        public void postComment() {
            Dialogs.showProDialog(getActivity(), "Loading");
            StringRequest stringRequest = new StringRequest(Request.Method.POST, Api_temp ,
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {
                            Log.e("log", "" + response);
                            //resComment(response);
                        }
                    },
                    new Response.ErrorListener() {

                        @Override
                        public void onErrorResponse(VolleyError error) {
                            Dialogs.disDialog();
                            DialogInterface.OnClickListener onClickTryAgain = new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    Dialogs.disDialog();
                                    postComment();
                                }
                            };
                            if (error instanceof TimeoutError) {
                                Dialogs.disDialog();
                                Alerts.timeoutErrorAlert(getContext(), onClickTryAgain);
                            } else if (error instanceof NoConnectionError) {
                                Dialogs.disDialog();
                                Alerts.internetConnectionErrorAlert(getContext(), onClickTryAgain);
                            }
                        }
                    })
            {

                @Override
                protected Map<String, String> getParams() throws AuthFailureError {
                    Map<String, String> map = new HashMap<String, String>();
                    map.put("user", SaveSharedPreference.getUserID(getActivity()));
                    map.put("token", SaveSharedPreference.getPrefToken(getActivity()));
                    if (type.equalsIgnoreCase("blogs")){
                        map.put("post",postid);
                    }else {
                        map.put("id",postid);
                    }
                    map.put("comment",edcomment.getText().toString());
                    Log.e("mapcomment",""+map);
                    return map;
                }

                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    Map<String, String> params = new HashMap<String, String>();
                    params.put("Content-Type", "application/x-www-form-urlencoded");
                    return params;
                }
            };

            RequestQueue requestQueue = Volley.newRequestQueue(getActivity());
            requestQueue.add(stringRequest);
        }

       /* private void resComment(String response )  {
            try{
                JSONObject jsonObject=new JSONObject(response);
                if (jsonObject.getString("message").equalsIgnoreCase("success")){
                    Dialogs.disDialog();
                    if (type.equalsIgnoreCase("news")){

                        edcomment.getText().clear();
                      *//*  edcomment.getText().clear();
                        Newsdetail_fragment.getnewsDetail(getContext());
                        adapter.notifyDataSetChanged();*//*

                    }else if (type.equalsIgnoreCase("explorer")){
                        edcomment.getText().clear();
                        Explore_detailfragment.getExploredetail(getContext());
                        adapter.notifyDataSetChanged();
                    }else if (type.equalsIgnoreCase("talents")){
                        edcomment.getText().clear();
                        //Talentdetail_fragment.getTalentdetail(getContext());
                        adapter.notifyDataSetChanged();
                    }else if (type.equalsIgnoreCase("opinions")){
                        edcomment.getText().clear();
                        Opiniondetail_fragment.getOpiniondetail(getContext());
                        adapter.notifyDataSetChanged();
                    }else if (type.equalsIgnoreCase("blogs")){
                        edcomment.getText().clear();
                        Blogdetail_fragment.getBlogdetail(getContext());
                        adapter.notifyDataSetChanged();
                    }else if (type.equalsIgnoreCase("petitions")){
                        edcomment.getText().clear();
                        Petitiondetail_fragment.getPetitiondetail(getContext());
                        adapter.notifyDataSetChanged();
                    }

                    Toast.makeText(getActivity(),jsonObject.getString("message"),Toast.LENGTH_SHORT).show();

                }else {
                    Dialogs.disDialog();
                    Toast.makeText(getActivity(),jsonObject.getString("message"),Toast.LENGTH_SHORT).show();
                }
            }
            catch (JSONException jsonexception){
                Log.e("excep",""+jsonexception);
            }
            catch (Exception e){
                Log.e("excep",""+e);
            }

        }*/

    }

