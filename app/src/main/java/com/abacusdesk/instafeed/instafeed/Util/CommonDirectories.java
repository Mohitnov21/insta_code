package com.abacusdesk.instafeed.instafeed.Util;

import android.content.Context;
import android.os.Environment;

import java.io.File;

public class CommonDirectories {
	
	private Context context;
	
	public void getPrivateDir() {
		/*
		 * if you'd like the photos to remain private to your app only, you can instead use the 
		 * directory provided by getExternalFilesDir().
		 * 
		 * 
		 * Note: Files you save in the directories provided by getExternalFilesDir() are deleted 
		 * when the user uninstalls your app.
		 * */
		
//		return context.getExternalFilesDir();
	}

	public static File getCommonPicturesDir() {
//		<uses-permission android:name="android.permission.WRITE_EXTERNAL_STORAGE" />

		return Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES);
	}
	
	public static File getExternalCache(Context context) {
		return context.getExternalCacheDir();
	}
	
}
