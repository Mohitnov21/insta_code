package com.abacusdesk.instafeed.instafeed.model;

import java.util.List;

/**
 * Created by abacusdesk on 2017-07-14.
 */

public class Exploreall_model {


    private int status;
    private String message;
    private List<DataBean> data;

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public List<DataBean> getData() {
        return data;
    }

    public void setData(List<DataBean> data) {
        this.data = data;
    }

    public static class DataBean {
        /**
         * id : 36
         * explorer_category_id : 3
         * title : testing vertically & horizontally image
         * slug : testing-vertically-horizontally-image-616671639
         * short_description : test explorer image........
         * location_id : 141
         * total_up_votes : null
         * total_down_votes : null
         * total_comments : null
         * total_views : 3
         * total_flags : null
         * dt_added : 2017-07-13 17:53:10
         * dt_modified : 2017-07-13 17:53:10
         * status : P
         * user_id : 3905
         * first_name : Abhishek
         * last_name : Chauhan
         * nickname : Abhishek
         * avatar : http://instafeed.org/storage/avatar/80x80-3844825a5c347c3f039c127d02b9154d.jpg
         * username : abhishek
         * image : http://instafeed.org/storage/explorer_mime/befebc7c8740c97e18fd060ea9285de5.jpg
         * source : w
         * latitude : null
         * longitude : null
         */

        private String id;
        private String explorer_category_id;
        private String title;
        private String slug;
        private String short_description;
        private String location_id;

        private String total_up_votes;
        private Object total_down_votes;
        private String total_comments;
        private String total_views;

        private Object total_flags;
        private String dt_added;
        private String dt_modified;
        private String status;
        private String user_id;
        private String first_name;
        private String last_name;
        private String nickname;
        private String avatar;
        private String username;
        private String image;
        private String source;
        private Object latitude;
        private Object longitude;
        private String video_thumb;

        public String getIs_like() {
            return is_like;
        }

        public void setIs_like(String is_like) {
            this.is_like = is_like;
        }

        private String is_like;

        public String getImage_330x210() {
            return image_330x210;
        }

        public void setImage_330x210(String image_330x210) {
            this.image_330x210 = image_330x210;
        }

        public String getVideo_330x210() {
            return video_330x210;
        }

        public void setVideo_330x210(String video_330x210) {
            this.video_330x210 = video_330x210;
        }

        private String image_330x210;
        private String video_330x210;



        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getExplorer_category_id() {
            return explorer_category_id;
        }

        public void setExplorer_category_id(String explorer_category_id) {
            this.explorer_category_id = explorer_category_id;
        }

        public String getTitle() {
            return title;
        }

        public void setTitle(String title) {
            this.title = title;
        }

        public String getSlug() {
            return slug;
        }

        public void setSlug(String slug) {
            this.slug = slug;
        }

        public String getShort_description() {
            return short_description;
        }

        public void setShort_description(String short_description) {
            this.short_description = short_description;
        }

        public String getLocation_id() {
            return location_id;
        }

        public void setLocation_id(String location_id) {
            this.location_id = location_id;
        }

        public String getTotal_up_votes() {
            return total_up_votes;
        }

        public void setTotal_up_votes(String total_up_votes) {
            this.total_up_votes = total_up_votes;
        }

        public Object getTotal_down_votes() {
            return total_down_votes;
        }

        public void setTotal_down_votes(Object total_down_votes) {
            this.total_down_votes = total_down_votes;
        }

        public String getTotal_comments() {
            return total_comments;
        }

        public void setTotal_comments(String total_comments) {
            this.total_comments = total_comments;
        }

        public String getTotal_views() {
            return total_views;
        }

        public void setTotal_views(String total_views) {
            this.total_views = total_views;
        }

        public Object getTotal_flags() {
            return total_flags;
        }

        public void setTotal_flags(Object total_flags) {
            this.total_flags = total_flags;
        }

        public String getDt_added() {
            return dt_added;
        }

        public void setDt_added(String dt_added) {
            this.dt_added = dt_added;
        }

        public String getDt_modified() {
            return dt_modified;
        }

        public void setDt_modified(String dt_modified) {
            this.dt_modified = dt_modified;
        }

        public String getStatus() {
            return status;
        }

        public void setStatus(String status) {
            this.status = status;
        }

        public String getUser_id() {
            return user_id;
        }

        public void setUser_id(String user_id) {
            this.user_id = user_id;
        }

        public String getFirst_name() {
            return first_name;
        }

        public void setFirst_name(String first_name) {
            this.first_name = first_name;
        }

        public String getLast_name() {
            return last_name;
        }

        public void setLast_name(String last_name) {
            this.last_name = last_name;
        }

        public String getNickname() {
            return nickname;
        }

        public void setNickname(String nickname) {
            this.nickname = nickname;
        }

        public String getAvatar() {
            return avatar;
        }

        public void setAvatar(String avatar) {
            this.avatar = avatar;
        }

        public String getUsername() {
            return username;
        }

        public void setUsername(String username) {
            this.username = username;
        }

        public String getImage() {
            return image;
        }

        public void setImage(String image) {
            this.image = image;
        }

        public String getSource() {
            return source;
        }

        public void setSource(String source) {
            this.source = source;
        }

        public Object getLatitude() {
            return latitude;
        }

        public void setLatitude(Object latitude) {
            this.latitude = latitude;
        }

        public Object getLongitude() {
            return longitude;
        }

        public void setLongitude(Object longitude) {
            this.longitude = longitude;
        }

        public String getVideo_thumb() {
            return video_thumb;
        }

        public void setVideo_thumb(String video_thumb) {
            this.video_thumb = video_thumb;
        }
    }
}
