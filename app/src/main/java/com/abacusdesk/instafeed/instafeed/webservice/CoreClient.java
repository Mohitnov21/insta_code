package com.abacusdesk.instafeed.instafeed.webservice;

import android.content.Context;
import android.graphics.Bitmap;
import android.support.v4.util.LruCache;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.Volley;

/**
 * Created by codecube on 5/12/16.
 */

public class CoreClient {

    private static CoreClient instance;
    private final Context context;
    private RequestQueue requestQueue;
    private ImageLoader imageLoader;

    private CoreClient(Context context) {
        this.context = context.getApplicationContext();
    }

    public static CoreClient getInstance(Context context) {
        if(instance == null) {
            instance = new CoreClient(context);
        }
        return instance;
    }

    public RequestQueue getRequestQueue() {
        if(requestQueue == null) {
            requestQueue = Volley.newRequestQueue(context);
        }
        return requestQueue;
    }

    public void addToRequestQueue(Request request) {
        getRequestQueue().add(request);
    }

    public ImageLoader getImageLoader() {
        if(imageLoader == null) {
            imageLoader = new ImageLoader(getRequestQueue(), new ImageLoader.ImageCache() {
                private final LruCache<String, Bitmap> cache = new LruCache<String, Bitmap>(30);
                @Override
                public Bitmap getBitmap(String url) {
                    return cache.get(url);
                }

                @Override
                public void putBitmap(String url, Bitmap bitmap) {
                    cache.put(url, bitmap);
                }
            });
        }
        return imageLoader;
    }

    public void cancelRequest(Request request) {
        if(request != null) request.cancel();
    }

    public void cancelRequest(String tag) {
        if(requestQueue != null) requestQueue.cancelAll(tag);
    }
}
