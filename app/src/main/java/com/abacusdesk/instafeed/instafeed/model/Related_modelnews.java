package com.abacusdesk.instafeed.instafeed.model;

import java.util.List;

/**
 * Created by abacusdesk on 2017-10-05.
 */

public class Related_modelnews {


    /**
     * status : 200
     * message : success
     * data : [{"id":"2009","news_category_id":"6","title":"Baba Ramdev Under Scanner for Sending Expired Patanjali Goods to Flood Victims in Assam","slug":"baba-ramdev-under-scanner-for-sending-expired-patanjali-goods-to-flood-victims-in-assam-1882416420","short_description":"Baba Ramdev\u2019s Patanjali is under the scanner of the health department as the news of all expired F&B items been sent to the Assam flood victims has started surfacing, off lately. Patanjali is Baba Ramdev\u2019s ayurvedic brand, which is India\u2019s one of the biggest manufacturer of ayurvedic and organ","location_id":"187","total_likes":"1","total_dislikes":"0","total_comments":"2","total_views":"77","total_flags":"0","dt_added":"2017-09-28 13:17:43","dt_modified":"2017-09-28 13:17:43","status":"P","user_id":"3924","first_name":"jitu","last_name":"kumar","nickname":"jitu","avatar":"http://instafeed.org/storage/avatar/80x80-","username":"jkg","image":"http://instafeed.org/storage/news_mime/256x170-eb413a19c2af8319dc73ed8ec5b73214.jpg","image_zoom":"http://instafeed.org/storage/news_mime/748x420-eb413a19c2af8319dc73ed8ec5b73214.jpg","image_original":"http://instafeed.org/storage/news_mime/eb413a19c2af8319dc73ed8ec5b73214.jpg","image_100x100":"http://instafeed.org/storage/news_mime/100x100-eb413a19c2af8319dc73ed8ec5b73214.jpg","image_256x170":"http://instafeed.org/storage/news_mime/256x170-eb413a19c2af8319dc73ed8ec5b73214.jpg","image_264x200":"http://instafeed.org/storage/news_mime/264x200-eb413a19c2af8319dc73ed8ec5b73214.jpg","image_360x290":"http://instafeed.org/storage/news_mime/360x290-eb413a19c2af8319dc73ed8ec5b73214.jpg","video_thumb":"","video_original":"","video_zoom":"","video_100x100":"","video_256x170":"","video_264x200":"","video_360x290":"","source":"w","latitude":null,"longitude":null,"is_anonymous":"Y"},{"id":"1915","news_category_id":"6","title":"Google faces lawsuit based on gender discrimination in pay practices from former female employees","slug":"google-faces-lawsuit-based-on-gender-discrimination-in-pay-practices-from-former-female-employees-1818241514","short_description":"The Department of Labor sued Oracle America Inc in January, claiming it paid white men more than women and minorities with similar jobs. Microsoft Corp and Twitter Inc are facing sex bias lawsuits, and Qualcomm Inc last year settled claims for $19.5 million.\r\n\r\nMeanwhile, Uber Technologies Inc in Ju","location_id":null,"total_likes":"4","total_dislikes":"0","total_comments":"3","total_views":"207","total_flags":"0","dt_added":"2017-09-15 08:14:26","dt_modified":"2017-09-15 08:14:26","status":"P","user_id":"3924","first_name":"jitu","last_name":"kumar","nickname":"jitu","avatar":"http://instafeed.org/storage/avatar/80x80-","username":"jkg","image":"http://instafeed.org/storage/news_mime/256x170-36a8d3d983a18454b954b535e711219c.jpg","image_zoom":"http://instafeed.org/storage/news_mime/748x420-36a8d3d983a18454b954b535e711219c.jpg","image_original":"http://instafeed.org/storage/news_mime/36a8d3d983a18454b954b535e711219c.jpg","image_100x100":"http://instafeed.org/storage/news_mime/100x100-36a8d3d983a18454b954b535e711219c.jpg","image_256x170":"http://instafeed.org/storage/news_mime/256x170-36a8d3d983a18454b954b535e711219c.jpg","image_264x200":"http://instafeed.org/storage/news_mime/264x200-36a8d3d983a18454b954b535e711219c.jpg","image_360x290":"http://instafeed.org/storage/news_mime/360x290-36a8d3d983a18454b954b535e711219c.jpg","video_thumb":"","video_original":"","video_zoom":"","video_100x100":"","video_256x170":"","video_264x200":"","video_360x290":"","source":"w","latitude":null,"longitude":null,"is_anonymous":"N"},{"id":"1741","news_category_id":"6","title":"What happens to Aadhaar, Section 377?","slug":"what-happens-to-aadhaar-section-377-2123008930","short_description":"\t\t\t\t\t\r\nNEW DELHI: A nine-judge Supreme Court bench unanimously ruled on Thursday that privacy is a fundamental right , protected as an intrinsic part of the right to life and personal liberty and as part of the freedoms guaranteed by the Constitution . \r\n\r\nSource - TOI","location_id":"187","total_likes":"3","total_dislikes":"0","total_comments":"2","total_views":"173","total_flags":"0","dt_added":"2017-08-25 17:10:46","dt_modified":"2017-08-25 17:10:46","status":"P","user_id":"5","first_name":"Sagar","last_name":"Arora","nickname":"Sagar","avatar":"http://instafeed.org/storage/avatar/80x80-1f069d56ecce23433d4126aaf7b8719f.jpg","username":"segitips4u","image":"http://instafeed.org/storage/news_mime/256x170-070a6e1d24c9dd641104631ff1e2ad38.jpg","image_zoom":"http://instafeed.org/storage/news_mime/748x420-070a6e1d24c9dd641104631ff1e2ad38.jpg","image_original":"http://instafeed.org/storage/news_mime/070a6e1d24c9dd641104631ff1e2ad38.jpg","image_100x100":"http://instafeed.org/storage/news_mime/100x100-070a6e1d24c9dd641104631ff1e2ad38.jpg","image_256x170":"http://instafeed.org/storage/news_mime/256x170-070a6e1d24c9dd641104631ff1e2ad38.jpg","image_264x200":"http://instafeed.org/storage/news_mime/264x200-070a6e1d24c9dd641104631ff1e2ad38.jpg","image_360x290":"http://instafeed.org/storage/news_mime/360x290-070a6e1d24c9dd641104631ff1e2ad38.jpg","video_thumb":"","video_original":"","video_zoom":"","video_100x100":"","video_256x170":"","video_264x200":"","video_360x290":"","source":"w","latitude":null,"longitude":null,"is_anonymous":"Y"},{"id":"647","news_category_id":"6","title":"तेजरफ्तार फॉरच्युनर बेकाबू होकर डिवाइडर से उछल कर एसयूवी पर गिरी... लाइव वीडियो..","slug":"-1406942499","short_description":"जालंधर की तरफ से आ रही तेज रफ़्तार फॉरच्युनर बेकाबू होकर डिवाइडर पर चढ़ गयी और दूसरी और से आ रही महिंद्रा की एसयूवी पर","location_id":"470","total_likes":"1","total_dislikes":"0","total_comments":"3","total_views":"480","total_flags":"0","dt_added":"2017-06-20 03:22:50","dt_modified":"2017-09-16 06:17:34","status":"P","user_id":"3965","first_name":"Daleep","last_name":"Bhutani","nickname":"Daleep","avatar":null,"username":"bhutani","image":"http://instafeed.org/storage/news_mime/256x170-default_news.jpg","image_zoom":"http://instafeed.org/storage/news_mime/748x420-default_news.jpg","image_original":"http://instafeed.org/storage/news_mime/default_news.jpg","image_100x100":"http://instafeed.org/storage/news_mime/100x100-default_news.jpg","image_256x170":"http://instafeed.org/storage/news_mime/256x170-default_news.jpg","image_264x200":"http://instafeed.org/storage/news_mime/264x200-default_news.jpg","image_360x290":"http://instafeed.org/storage/news_mime/360x290-default_news.jpg","video_thumb":"http://instafeed.org/storage/news_mime/256x170-d7fe7c38bd94dd9fbe735aa254743af8.jpg","video_original":"http://instafeed.org/storage/news_mime/d7fe7c38bd94dd9fbe735aa254743af8.jpg","video_zoom":"http://instafeed.org/storage/news_mime/748x420-d7fe7c38bd94dd9fbe735aa254743af8.jpg","video_100x100":"http://instafeed.org/storage/news_mime/100x100-d7fe7c38bd94dd9fbe735aa254743af8.jpg","video_256x170":"http://instafeed.org/storage/news_mime/256x170-d7fe7c38bd94dd9fbe735aa254743af8.jpg","video_264x200":"http://instafeed.org/storage/news_mime/264x200-d7fe7c38bd94dd9fbe735aa254743af8.jpg","video_360x290":"http://instafeed.org/storage/news_mime/360x290-d7fe7c38bd94dd9fbe735aa254743af8.jpg","source":"w","latitude":null,"longitude":null,"is_anonymous":"N"},{"id":"239","news_category_id":"6","title":"(राष्\u200dट्रीय प्रौधोगिकी दिवस) National Technology Day ","slug":"national-technology-day-2004610344","short_description":"11 मई  1998 को भारत ने अपना दूसरा सफल परमाणु परीक्षण पोखरण (राजस्थान) में किया था।  यह सफल परीक्षण टेक्नोलॉजी के क्षेत्र ","location_id":"187","total_likes":"3","total_dislikes":"1","total_comments":"1","total_views":"1161","total_flags":null,"dt_added":"2017-06-19 15:31:10","dt_modified":"2017-07-22 11:56:04","status":"P","user_id":"46","first_name":"Vijay","last_name":"Mishra","nickname":"Vijay","avatar":"http://instafeed.org/storage/avatar/80x80-984ff9feb960e625d401340b76d75458.jpg","username":"mishravijay15","image":"http://instafeed.org/storage/news_mime/256x170-d511752d8fd933a4946175d99035138a.jpg","image_zoom":"http://instafeed.org/storage/news_mime/748x420-d511752d8fd933a4946175d99035138a.jpg","image_original":"http://instafeed.org/storage/news_mime/d511752d8fd933a4946175d99035138a.jpg","image_100x100":"http://instafeed.org/storage/news_mime/100x100-d511752d8fd933a4946175d99035138a.jpg","image_256x170":"http://instafeed.org/storage/news_mime/256x170-d511752d8fd933a4946175d99035138a.jpg","image_264x200":"http://instafeed.org/storage/news_mime/264x200-d511752d8fd933a4946175d99035138a.jpg","image_360x290":"http://instafeed.org/storage/news_mime/360x290-d511752d8fd933a4946175d99035138a.jpg","video_thumb":"","video_original":"","video_zoom":"","video_100x100":"","video_256x170":"","video_264x200":"","video_360x290":"","source":"w","latitude":null,"longitude":null,"is_anonymous":"N"}]
     */

    private int status;
    private String message;
    private List<DataBean> data;

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public List<DataBean> getData() {
        return data;
    }

    public void setData(List<DataBean> data) {
        this.data = data;
    }

    public static class DataBean {
        /**
         * id : 2009
         * news_category_id : 6
         * title : Baba Ramdev Under Scanner for Sending Expired Patanjali Goods to Flood Victims in Assam
         * slug : baba-ramdev-under-scanner-for-sending-expired-patanjali-goods-to-flood-victims-in-assam-1882416420
         * short_description : Baba Ramdev’s Patanjali is under the scanner of the health department as the news of all expired F&B items been sent to the Assam flood victims has started surfacing, off lately. Patanjali is Baba Ramdev’s ayurvedic brand, which is India’s one of the biggest manufacturer of ayurvedic and organ
         * location_id : 187
         * total_likes : 1
         * total_dislikes : 0
         * total_comments : 2
         * total_views : 77
         * total_flags : 0
         * dt_added : 2017-09-28 13:17:43
         * dt_modified : 2017-09-28 13:17:43
         * status : P
         * user_id : 3924
         * first_name : jitu
         * last_name : kumar
         * nickname : jitu
         * avatar : http://instafeed.org/storage/avatar/80x80-
         * username : jkg
         * image : http://instafeed.org/storage/news_mime/256x170-eb413a19c2af8319dc73ed8ec5b73214.jpg
         * image_zoom : http://instafeed.org/storage/news_mime/748x420-eb413a19c2af8319dc73ed8ec5b73214.jpg
         * image_original : http://instafeed.org/storage/news_mime/eb413a19c2af8319dc73ed8ec5b73214.jpg
         * image_100x100 : http://instafeed.org/storage/news_mime/100x100-eb413a19c2af8319dc73ed8ec5b73214.jpg
         * image_256x170 : http://instafeed.org/storage/news_mime/256x170-eb413a19c2af8319dc73ed8ec5b73214.jpg
         * image_264x200 : http://instafeed.org/storage/news_mime/264x200-eb413a19c2af8319dc73ed8ec5b73214.jpg
         * image_360x290 : http://instafeed.org/storage/news_mime/360x290-eb413a19c2af8319dc73ed8ec5b73214.jpg
         * video_thumb :
         * video_original :
         * video_zoom :
         * video_100x100 :
         * video_256x170 :
         * video_264x200 :
         * video_360x290 :
         * source : w
         * latitude : null
         * longitude : null
         * is_anonymous : Y
         */

        private String id;
        private String news_category_id;
        private String title;
        private String slug;
        private String short_description;
        private String location_id;
        private String total_likes;
        private String total_dislikes;
        private String total_comments;
        private String total_views;
        private String total_flags;
        private String dt_added;
        private String dt_modified;
        private String status;
        private String user_id;
        private String first_name;
        private String last_name;
        private String nickname;
        private String avatar;
        private String username;
        private String image;
        private String image_zoom;
        private String image_original;
        private String image_100x100;
        private String image_256x170;
        private String image_264x200;
        private String image_360x290;
        private String video_thumb;
        private String video_original;
        private String video_zoom;
        private String video_100x100;
        private String video_256x170;
        private String video_264x200;
        private String video_360x290;
        private String source;
        private Object latitude;
        private Object longitude;
        private String is_anonymous;

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getNews_category_id() {
            return news_category_id;
        }

        public void setNews_category_id(String news_category_id) {
            this.news_category_id = news_category_id;
        }

        public String getTitle() {
            return title;
        }

        public void setTitle(String title) {
            this.title = title;
        }

        public String getSlug() {
            return slug;
        }

        public void setSlug(String slug) {
            this.slug = slug;
        }

        public String getShort_description() {
            return short_description;
        }

        public void setShort_description(String short_description) {
            this.short_description = short_description;
        }

        public String getLocation_id() {
            return location_id;
        }

        public void setLocation_id(String location_id) {
            this.location_id = location_id;
        }

        public String getTotal_likes() {
            return total_likes;
        }

        public void setTotal_likes(String total_likes) {
            this.total_likes = total_likes;
        }

        public String getTotal_dislikes() {
            return total_dislikes;
        }

        public void setTotal_dislikes(String total_dislikes) {
            this.total_dislikes = total_dislikes;
        }

        public String getTotal_comments() {
            return total_comments;
        }

        public void setTotal_comments(String total_comments) {
            this.total_comments = total_comments;
        }

        public String getTotal_views() {
            return total_views;
        }

        public void setTotal_views(String total_views) {
            this.total_views = total_views;
        }

        public String getTotal_flags() {
            return total_flags;
        }

        public void setTotal_flags(String total_flags) {
            this.total_flags = total_flags;
        }

        public String getDt_added() {
            return dt_added;
        }

        public void setDt_added(String dt_added) {
            this.dt_added = dt_added;
        }

        public String getDt_modified() {
            return dt_modified;
        }

        public void setDt_modified(String dt_modified) {
            this.dt_modified = dt_modified;
        }

        public String getStatus() {
            return status;
        }

        public void setStatus(String status) {
            this.status = status;
        }

        public String getUser_id() {
            return user_id;
        }

        public void setUser_id(String user_id) {
            this.user_id = user_id;
        }

        public String getFirst_name() {
            return first_name;
        }

        public void setFirst_name(String first_name) {
            this.first_name = first_name;
        }

        public String getLast_name() {
            return last_name;
        }

        public void setLast_name(String last_name) {
            this.last_name = last_name;
        }

        public String getNickname() {
            return nickname;
        }

        public void setNickname(String nickname) {
            this.nickname = nickname;
        }

        public String getAvatar() {
            return avatar;
        }

        public void setAvatar(String avatar) {
            this.avatar = avatar;
        }

        public String getUsername() {
            return username;
        }

        public void setUsername(String username) {
            this.username = username;
        }

        public String getImage() {
            return image;
        }

        public void setImage(String image) {
            this.image = image;
        }

        public String getImage_zoom() {
            return image_zoom;
        }

        public void setImage_zoom(String image_zoom) {
            this.image_zoom = image_zoom;
        }

        public String getImage_original() {
            return image_original;
        }

        public void setImage_original(String image_original) {
            this.image_original = image_original;
        }

        public String getImage_100x100() {
            return image_100x100;
        }

        public void setImage_100x100(String image_100x100) {
            this.image_100x100 = image_100x100;
        }

        public String getImage_256x170() {
            return image_256x170;
        }

        public void setImage_256x170(String image_256x170) {
            this.image_256x170 = image_256x170;
        }

        public String getImage_264x200() {
            return image_264x200;
        }

        public void setImage_264x200(String image_264x200) {
            this.image_264x200 = image_264x200;
        }

        public String getImage_360x290() {
            return image_360x290;
        }

        public void setImage_360x290(String image_360x290) {
            this.image_360x290 = image_360x290;
        }

        public String getVideo_thumb() {
            return video_thumb;
        }

        public void setVideo_thumb(String video_thumb) {
            this.video_thumb = video_thumb;
        }

        public String getVideo_original() {
            return video_original;
        }

        public void setVideo_original(String video_original) {
            this.video_original = video_original;
        }

        public String getVideo_zoom() {
            return video_zoom;
        }

        public void setVideo_zoom(String video_zoom) {
            this.video_zoom = video_zoom;
        }

        public String getVideo_100x100() {
            return video_100x100;
        }

        public void setVideo_100x100(String video_100x100) {
            this.video_100x100 = video_100x100;
        }

        public String getVideo_256x170() {
            return video_256x170;
        }

        public void setVideo_256x170(String video_256x170) {
            this.video_256x170 = video_256x170;
        }

        public String getVideo_264x200() {
            return video_264x200;
        }

        public void setVideo_264x200(String video_264x200) {
            this.video_264x200 = video_264x200;
        }

        public String getVideo_360x290() {
            return video_360x290;
        }

        public void setVideo_360x290(String video_360x290) {
            this.video_360x290 = video_360x290;
        }

        public String getSource() {
            return source;
        }

        public void setSource(String source) {
            this.source = source;
        }

        public Object getLatitude() {
            return latitude;
        }

        public void setLatitude(Object latitude) {
            this.latitude = latitude;
        }

        public Object getLongitude() {
            return longitude;
        }

        public void setLongitude(Object longitude) {
            this.longitude = longitude;
        }

        public String getIs_anonymous() {
            return is_anonymous;
        }

        public void setIs_anonymous(String is_anonymous) {
            this.is_anonymous = is_anonymous;
        }
    }
}
