package com.abacusdesk.instafeed.instafeed.fontsClass;

import android.content.Context;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.widget.TextView;

/**
 * Created by abacusdesk on 2017-08-23.
 */

public class CustomRegularText  extends TextView {

        public CustomRegularText(Context context, AttributeSet attrs, int defStyle) {
            super(context, attrs, defStyle);
            init();
        }

        public CustomRegularText(Context context, AttributeSet attrs) {
            super(context, attrs);
            init();
        }

        public CustomRegularText(Context context) {
            super(context);
            init();
        }

        public void init() {
            Typeface tf = Typeface.createFromAsset(getContext().getAssets(),"fonts/Roboto-Regular.ttf");
            setTypeface(tf);

        }

    }