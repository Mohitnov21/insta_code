package com.abacusdesk.instafeed.instafeed;

import android.app.Dialog;
import android.content.BroadcastReceiver;
import android.content.ClipData;
import android.content.ContentResolver;
import android.content.ContentValues;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.content.res.Configuration;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.net.ConnectivityManager;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.annotation.RequiresApi;
import android.support.design.widget.NavigationView;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.GravityCompat;
import android.support.v4.view.MenuItemCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.PopupMenu;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.webkit.MimeTypeMap;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.ExpandableListView;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.abacusdesk.instafeed.instafeed.Util.Alerts;
import com.abacusdesk.instafeed.instafeed.Util.Apis;
import com.abacusdesk.instafeed.instafeed.Util.AppSingleton;
import com.abacusdesk.instafeed.instafeed.Util.CommonDirectories;
import com.abacusdesk.instafeed.instafeed.Util.CommonFunctions;
import com.abacusdesk.instafeed.instafeed.Util.DbHandler;
import com.abacusdesk.instafeed.instafeed.Util.FilePath;
import com.abacusdesk.instafeed.instafeed.Util.GpsActivation;
import com.abacusdesk.instafeed.instafeed.Util.NetworkReceiver;
import com.abacusdesk.instafeed.instafeed.Util.RealPathUtil;
import com.abacusdesk.instafeed.instafeed.Util.SaveSharedPreference;
import com.abacusdesk.instafeed.instafeed.Util.ScreenConstants;
import com.abacusdesk.instafeed.instafeed.Util.TimeUtilities;
import com.abacusdesk.instafeed.instafeed.Util.WebUrl;
import com.abacusdesk.instafeed.instafeed.application.Change_like;
import com.abacusdesk.instafeed.instafeed.custom_dialog.Dialogs;
import com.abacusdesk.instafeed.instafeed.customcamera.CameraDemoActivity;
import com.abacusdesk.instafeed.instafeed.fragments.Blogdetail_fragment;
import com.abacusdesk.instafeed.instafeed.fragments.Bookmark_fragment;
import com.abacusdesk.instafeed.instafeed.fragments.ChangePassword_fragment;
import com.abacusdesk.instafeed.instafeed.fragments.CommentsList_fragment;
import com.abacusdesk.instafeed.instafeed.fragments.CreateFragment;
import com.abacusdesk.instafeed.instafeed.fragments.Explore_detailfragment;
import com.abacusdesk.instafeed.instafeed.fragments.Follow_Fragment;
import com.abacusdesk.instafeed.instafeed.fragments.Fragment_search;
import com.abacusdesk.instafeed.instafeed.fragments.HomeFrag_news;
import com.abacusdesk.instafeed.instafeed.fragments.HomeTab_fragment;
import com.abacusdesk.instafeed.instafeed.fragments.Newsdetail_fragment;
import com.abacusdesk.instafeed.instafeed.fragments.Opiniondetail_fragment;
import com.abacusdesk.instafeed.instafeed.fragments.Petitiondetail_fragment;
import com.abacusdesk.instafeed.instafeed.fragments.Publicprofile_Fragment;
import com.abacusdesk.instafeed.instafeed.fragments.SaveOfflineFragment;
import com.abacusdesk.instafeed.instafeed.fragments.SuccessDialogFragment;
import com.abacusdesk.instafeed.instafeed.fragments.Talentdetail_fragment;
import com.abacusdesk.instafeed.instafeed.fragments.UpdateProfile_Fragment;
import com.abacusdesk.instafeed.instafeed.model.GridModel;
import com.abacusdesk.instafeed.instafeed.model.Location_model;
import com.abacusdesk.instafeed.instafeed.model.MediaDataModel;
import com.abacusdesk.instafeed.instafeed.model.NewCategoryListModel;
import com.abacusdesk.instafeed.instafeed.model.NewsDetail;
import com.abacusdesk.instafeed.instafeed.model.Search_model;
import com.abacusdesk.instafeed.instafeed.model.User_model;
import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NoConnectionError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.bumptech.glide.Glide;
import com.google.android.gms.ads.formats.NativeAd;
import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;
import com.google.gson.Gson;
import com.oguzdev.circularfloatingactionmenu.library.FloatingActionButton;
import com.oguzdev.circularfloatingactionmenu.library.FloatingActionMenu;
import com.oguzdev.circularfloatingactionmenu.library.SubActionButton;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static com.abacusdesk.instafeed.instafeed.Login_Activity.hasPermissions;
import static com.abacusdesk.instafeed.instafeed.Util.DbHandler.TABLE_EXPLORE_CATEGORY;
import static com.abacusdesk.instafeed.instafeed.Util.DbHandler.TABLE_NEWS_CATEGORY;
import static com.abacusdesk.instafeed.instafeed.Util.DbHandler.TABLE_TALENT_CATEGORY;
import static com.abacusdesk.instafeed.instafeed.Util.ScreenConstants.GALLERY_MODE;
import static com.abacusdesk.instafeed.instafeed.Util.ScreenConstants.KEY_AUDIO_FILE_NAME;
import static com.abacusdesk.instafeed.instafeed.Util.ScreenConstants.MEDIA_FILE_NAME;
import static com.abacusdesk.instafeed.instafeed.Util.ScreenConstants.MEDIA_FILE_PATH;
import static com.abacusdesk.instafeed.instafeed.Util.ScreenConstants.MEDIA_TYPE;
import static com.abacusdesk.instafeed.instafeed.Util.ScreenConstants.MEDIA_TYPE_AUDIO;
import static com.abacusdesk.instafeed.instafeed.Util.ScreenConstants.MEDIA_TYPE_IMAGE;
import static com.abacusdesk.instafeed.instafeed.Util.ScreenConstants.MEDIA_TYPE_VIDEO;
import static com.abacusdesk.instafeed.instafeed.Util.ScreenConstants.REQUEST_AUDIO_CAPTURE;
import static com.abacusdesk.instafeed.instafeed.Util.ScreenConstants.REQUEST_MICROPHONE;
import static com.abacusdesk.instafeed.instafeed.Util.ScreenConstants.REQUEST_VIDEO_CAPTURE;
import static com.abacusdesk.instafeed.instafeed.Util.ScreenConstants.SELECT_FILE;
import static com.abacusdesk.instafeed.instafeed.Util.ScreenConstants.PICK_IMAGE_MULTIPLE;
import static com.abacusdesk.instafeed.instafeed.Util.ScreenConstants.VIDEO_DIRECTORY_NAME;
import static com.abacusdesk.instafeed.instafeed.fragments.CreateFragment.AUDIO_DIRECTORY_NAME;
import static com.abacusdesk.instafeed.instafeed.fragments.CreateFragment.DIRECTORY_NAME;
import static com.abacusdesk.instafeed.instafeed.fragments.CreateFragment.REQUEST_CAMERA;

public class NavDrawerActivity extends AppCompatActivity implements View.OnClickListener,
        Newsdetail_fragment.Communicator,Change_like {

    List<String> listDataHeader;
    Map<String, List<String>> listDataChild;

    public NavigationDrawerAdapter navigationDrawerAdapter;
    private DrawerLayout drawer;
    BroadcastReceiver broadcastReceiver;
    private NavigationView navigationView;
    public static ImageView imgprofile;
    public static TextView txtname, txtemailheader;
    LinearLayout lnheader, lncamera, lngallery, lnedit, lnvideo, lnbottom;
    public static LinearLayout lnaudio;
    Location_model location_model;
    public static ArrayList<Location_model.DataBean> arrayLocation = new ArrayList<>();
    public static Boolean follower = false;
    User_model user_model;
    ArrayList<User_model.DataBean> arrayUsers = new ArrayList<>();
    public static ArrayList<String> location_strings = new ArrayList<>();
    ImageView imglogo;
    public static Toolbar toolbar;
    public static ActionBarDrawerToggle toggle;
    static String static_query = "";
    static String static_type = "";
    String apitemplocation = "";
    static String[] arraycategory = {"opinions", "news", "blogs", "polls", "petitions", "users", "talents", "explorer"};
    Search_model search_model;
    ArrayList<Search_model.DataBean> array_Search = new ArrayList<>();
    public static String type = "";
    Dialog dialog;
    private String fileName;
    private File videoFile;
    public boolean isBroadCastRegister = false;

    private String location_id;
    public static boolean mToolBarNavigationListenerIsRegistered = false;
    public MenuItem myActionMenuItem, menu_form, menubookmark, menucomment, menushare,menutextfeed;
    public static FloatingActionButton actionButton;
    public static SubActionButton button_camera, button_video, button_audio, button_gallery, button_text;
    public static FloatingActionMenu actionMenu;
    private File photoFile = null;
    private int video_flag = -1;
    String Filename, mCurrentPhotoPath, uid_image_path, video_path;
    public static LinearLayout ln_loggedin, ln_notlogin;
    public static Button btnlogin, btnsignup;
    String[] PERMISSIONS = {android.Manifest.permission.CAMERA, android.Manifest.permission.WRITE_EXTERNAL_STORAGE, android.Manifest.permission.ACCESS_FINE_LOCATION, android.Manifest.permission.INTERNET, android.Manifest.permission.READ_EXTERNAL_STORAGE, android.Manifest.permission.RECORD_AUDIO, android.Manifest.permission.RECEIVE_SMS, android.Manifest.permission.SEND_SMS, android.Manifest.permission.READ_SMS, android.Manifest.permission.READ_CONTACTS};
    public static final int PERMISSION_ALL = 1;
    View shadowview, included, header;

    DbHandler dbHandler;
    NewCategoryListModel newCategoryListModel;
    int count = 0;
    String mimeType = null;
    Tracker mTracker;
    ArrayList<HashMap<String,String>> videopatharray= new ArrayList<>();
    private String imageEncoded;
    private ArrayList<HashMap<String,String>> array_imahges;
    private LinearLayout ln_upload,lnsharesome;
    public static FrameLayout frame_bottom;
    //FirebaseInstanceId.getInstance().getToken();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_nav_drawer);
        try {
            if (!hasPermissions(this, PERMISSIONS)) {
                ActivityCompat.requestPermissions(this, PERMISSIONS, PERMISSION_ALL);
            }
        } catch (Exception e1) {
            e1.printStackTrace();
        }
        array_imahges=new ArrayList<>();

        setHome(new HomeTab_fragment());
        AnalyticsApplication application = (AnalyticsApplication) getApplication();
        mTracker = application.getDefaultTracker();

       // Log.i(TAG, "Setting screen name: " + name);
        mTracker.setScreenName("NavDrawerActivity");
        mTracker.send(new HitBuilders.ScreenViewBuilder().build());


        dbHandler = new DbHandler(NavDrawerActivity.this);
        dbHandler.open();
        fetchCategroyList();
        fetchExplorerCatList();
        fetchTalentCatList();
        broadcastReceiver = new NetworkReceiver();

        //new GpsActivation(NavDrawerActivity.this).enableGPS();
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        drawer = (DrawerLayout) findViewById(R.id.drawer_layout);

        //toolbar.setTitle("Instafeed");
        toolbar.setLogo(R.drawable.logo40);
        toolbar.setTitleTextColor(Color.WHITE);
        setSupportActionBar(toolbar);

        //View view3=findViewById(R.id.frame_container);
        included = findViewById(R.id.includedlayout);
        shadowview = (View) included.findViewById(R.id.shadowView);

        lnbottom = (LinearLayout) included.findViewById(R.id.ln_bottom);
        lnbottom.setVisibility(View.GONE);

        frame_bottom= (FrameLayout)  included.findViewById(R.id.frame_bottombar);
        ln_upload   = (LinearLayout) included.findViewById(R.id.ln_upload);
        lnsharesome = (LinearLayout) included.findViewById(R.id.ln_shareaome);

        lnaudio = (LinearLayout) included.findViewById(R.id.ln_audio);
        lncamera = (LinearLayout) included.findViewById(R.id.ln_camera);
        lnvideo = (LinearLayout) included.findViewById(R.id.ln_video);
        lnedit = (LinearLayout) included.findViewById(R.id.ln_edit);
        lngallery = (LinearLayout) included.findViewById(R.id.ln_gallery);

        lncamera.setOnClickListener(this);
        lnaudio.setOnClickListener(this);
        lnvideo.setOnClickListener(this);
        lnedit.setOnClickListener(this);
        lngallery.setOnClickListener(this);

        lnsharesome.setOnClickListener(this);
        ln_upload.setOnClickListener(this);

        shadowview.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (actionMenu.isOpen()) {
                    actionMenu.close(true);
                }
                shadowview.setVisibility(View.GONE);
            }
        });

        imglogo = (ImageView) findViewById(R.id.img_logo);
        navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationDrawerAdapter = new NavigationDrawerAdapter();

        toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);

        toggle.syncState();

        //toolbar.setLogo(R.drawable.instafeed_logospace);
        //toggle.setDrawerIndicatorEnabled(false);
        //toggle.setHomeAsUpIndicator(R.drawable.navigation_32);

        toggle.setToolbarNavigationClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!drawer.isDrawerOpen(GravityCompat.START)) {
                    drawer.openDrawer(GravityCompat.START);
                } else {
                    drawer.closeDrawer(GravityCompat.START);
                }
            }
        });
        getSupportFragmentManager().addOnBackStackChangedListener(new FragmentManager.OnBackStackChangedListener() {
            @Override
            public void onBackStackChanged() {
                if (getSupportFragmentManager().getBackStackEntryCount() > 0) {
                    getSupportActionBar().setDisplayHomeAsUpEnabled(false); // show back button

                   // lnbottom.setVisibility(View.GONE);
                    menu_form.setEnabled(false);
                    myActionMenuItem.setEnabled(false);
                    drawer.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED);

                    toggle.setDrawerIndicatorEnabled(false);
                    toggle.setHomeAsUpIndicator(R.drawable.back);
                    //toolbar.setLogo(null);
                    toolbar.setBackgroundColor(getResources().getColor(R.color.white));
                    //toolbar.setTitle("News");

                    Log.e("activity","Navdraweekemf");
                    toolbar.setTitleTextColor(Color.BLACK);
                    toolbar.setNavigationOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            onBackPressed();
                        }
                    });
                    if (getSupportFragmentManager().findFragmentByTag("createfrag") instanceof CreateFragment){
                        Log.e("createfragment",""+getCurrentFragment());
                        toolbar.setBackgroundColor(getResources().getColor(R.color.black));
                        toolbar.setTitleTextColor(Color.WHITE);
                        toggle.setHomeAsUpIndicator(R.drawable.back_white);
                        menubookmark.setVisible(false);
                        menucomment.setVisible(false);
                        menushare.setVisible(false);

                        menu_form.setVisible(false);
                        myActionMenuItem.setVisible(false);
                        menutextfeed.setVisible(true);
                    }

                    Log.e("currentfragment",""+getCurrentFragment());
                    if (getSupportFragmentManager().findFragmentByTag("tempfrag") instanceof Newsdetail_fragment){
                        if (getCurrentFragment()!=null){
                            if (getCurrentFragment() instanceof Newsdetail_fragment){
                                menubookmark.setVisible(true);
                                menucomment.setVisible(true);
                                menushare.setVisible(true);

                                menu_form.setVisible(false);
                                myActionMenuItem.setVisible(false);
                            }
                        }
                    }

                    if (getSupportFragmentManager().findFragmentByTag("comment") instanceof CommentsList_fragment){
                        Log.e("currentfragmentchala",""+getCurrentFragment());
                        menubookmark.setVisible(false);
                        menucomment.setVisible(false);
                        menushare.setVisible(false);

                        menu_form.setVisible(false);
                        myActionMenuItem.setVisible(false);
                    }

                    if (getSupportFragmentManager().findFragmentByTag("public") instanceof Publicprofile_Fragment) {
                        Log.e("firstcommentpublic", "firsrt");
                        if (getCurrentFragment()!=null) {
                            if (getCurrentFragment() instanceof Publicprofile_Fragment) {
                                menucomment.setVisible(false);
                                menushare.setVisible(false);
                                menubookmark.setVisible(false);
                                menu_form.setVisible(false);
                                myActionMenuItem.setVisible(false);
                            }
                        }
                    }

                } else {
                    //show hamburger
                   // lnbottom.setVisibility(View.VISIBLE);
                    getSupportActionBar().setDisplayHomeAsUpEnabled(true);
                    toolbar.setBackgroundColor(getResources().getColor(R.color.black));
                    toggle.setDrawerIndicatorEnabled(true);
                    toolbar.setLogo(R.drawable.logo40);
                    toolbar.setTitle("");
                    toolbar.setTitleTextColor(Color.WHITE);
                    toggle.syncState();

                    frame_bottom.setVisibility(View.VISIBLE);

                    drawer.setDrawerLockMode(DrawerLayout.LOCK_MODE_UNLOCKED);
                    menu_form.setVisible(true);
                    myActionMenuItem.setVisible(true);

                    menu_form.setEnabled(true);
                    myActionMenuItem.setEnabled(true);

                    menutextfeed.setVisible(false);
                    menucomment.setVisible(false);
                    menushare.setVisible(false);
                    menubookmark.setVisible(false);

                    toolbar.setNavigationOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            drawer.openDrawer(GravityCompat.START);
                        }
                    });
                }
            }
        });

        enableExpandableList();
        header = navigationView.getHeaderView(0);

        lnheader = (LinearLayout) header.findViewById(R.id.ln_profileheader);
        lnheader.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                drawer.closeDrawer(GravityCompat.START);
                // startActivity(new Intent(NavDrawerActivity.this,Publicprofile_Fragment.class));
                Publicprofile_Fragment.flag = false;
                NavDrawerActivity.this.getSupportFragmentManager().beginTransaction().add(R.id.frame_container, new Publicprofile_Fragment()).addToBackStack(null).commit();
            }
        });

        imgprofile = (ImageView) header.findViewById(R.id.imageView);
        txtname = (TextView) header.findViewById(R.id.textView);
        txtemailheader = (TextView) header.findViewById(R.id.text_email);

        ln_loggedin = (LinearLayout) header.findViewById(R.id.ln_login);
        ln_notlogin = (LinearLayout) header.findViewById(R.id.ln_notlogin);
        btnlogin = (Button) header.findViewById(R.id.btn_login);
        btnsignup = (Button) header.findViewById(R.id.btn_signup);
        btnsignup.setOnClickListener(this);
        btnlogin.setOnClickListener(this);

        updateProfile(NavDrawerActivity.this);
        if (CommonFunctions.isConnected(NavDrawerActivity.this)) {
            getUserdetails();
        } else {
            Toast.makeText(NavDrawerActivity.this, "Internet not available", Toast.LENGTH_SHORT).show();
        }
        //setFloatButton();
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        }
        if (getSupportFragmentManager().getBackStackEntryCount() >= 1) {
            Log.e("first", "firsrt");
            if (getSupportFragmentManager().findFragmentByTag("comment") instanceof CommentsList_fragment) {
                Log.e("firstcomment", "firsrt");
                //menubookmark.setVisible(true);
                menucomment.setVisible(true);
                menushare.setVisible(true);

                menu_form.setVisible(false);
                myActionMenuItem.setVisible(false);
            }

            if (getSupportFragmentManager().findFragmentByTag("public") instanceof Publicprofile_Fragment) {
                Log.e("firstcommentpublic", "firsrt");
                if (getCurrentFragment()!=null) {
                    if (getCurrentFragment() instanceof Publicprofile_Fragment) {
                        menucomment.setVisible(true);
                        menushare.setVisible(true);
                        //menubookmark.setVisible(true);
                        menu_form.setVisible(false);
                        myActionMenuItem.setVisible(false);

                        toolbar.setTitle(Publicprofile_Fragment.tolbartitle);
                        toolbar.setLogo(null);
                    }
                }
            }
            super.onBackPressed();

            if (getSupportFragmentManager().findFragmentByTag("public") instanceof Publicprofile_Fragment) {
                Log.e("firstcomment", "firsrt");
                if (getCurrentFragment()!=null) {
                    if (getCurrentFragment() instanceof Publicprofile_Fragment) {
                        Log.e("commentpublic", "firsrt");
                        menucomment.setVisible(false);
                        menushare.setVisible(false);
                        menubookmark.setVisible(false);
                        menu_form.setVisible(false);
                        myActionMenuItem.setVisible(false);

                        toolbar.setTitle("");
                        toolbar.setLogo(R.drawable.logo_black);
                    }
                }
            }

            if (getSupportFragmentManager().findFragmentByTag("search") instanceof Fragment_search) {
                Log.e("search", "firsrt");
                if (getCurrentFragment()!=null) {
                    if (getCurrentFragment() instanceof Fragment_search) {
                        Log.e("searchindide", "firsrt");
                        menucomment.setVisible(false);
                        menushare.setVisible(false);
                        menubookmark.setVisible(false);

                        toolbar.setTitle("");
                        toolbar.setLogo(R.drawable.logo_black);

                        menu_form.setVisible(false);
                        myActionMenuItem.setVisible(false);
                    }
                }

            }

            if (getSupportFragmentManager().findFragmentByTag("bookmark") instanceof Bookmark_fragment) {
                if (getCurrentFragment()!=null){
                    if (getCurrentFragment() instanceof Bookmark_fragment) {
                        Log.e("commentbook", "firsrt");
                        toolbar.setLogo(R.drawable.logo_black);
                        toolbar.setTitle("");
                        menucomment.setVisible(false);
                        menushare.setVisible(false);
                        menubookmark.setVisible(false);
                        menu_form.setVisible(false);
                        myActionMenuItem.setVisible(false);
                  }
               }
            }

                if (getSupportFragmentManager().findFragmentByTag("tempfrag") instanceof Newsdetail_fragment) {
                    if (getCurrentFragment()!=null){
                        if (getCurrentFragment() instanceof Newsdetail_fragment){
                            Newsdetail_fragment frag = ((Newsdetail_fragment)
                                    getSupportFragmentManager().findFragmentByTag("tempfrag"));
                            if (frag != null){
                                Log.e("commentnews", "firsrt");
                                menubookmark.setVisible(true);
                                menucomment.setVisible(true);
                                menushare.setVisible(true);

                                menu_form.setVisible(false);
                                myActionMenuItem.setVisible(false);

                                if (frag.getBookMark()){
                                    menubookmark.setIcon(R.drawable.bookfil);
                                }else{
                                    menubookmark.setIcon(R.drawable.book_bold);
                                }
                            }
                        }
                    }
                }


        } else {
            this.finish();
            super.onBackPressed();
        }
    }


    public void fetchCategroyList() {
        try {
            String query = "SELECT * FROM " + TABLE_NEWS_CATEGORY;
            count = dbHandler.rowCount(query);
            Log.e("count1", "" + count);
        } catch (Exception e) {

        }
        if (count < 1) {
            String url = WebUrl.NEWS_CATEGORY_URL;
            Log.d("fetchCategroyList", url);
            StringRequest stringRequest = new StringRequest(Request.Method.GET, url, new Response.Listener<String>() {
                @Override
                public void onResponse(String response) {
                    Log.e("fetchCategroyList", "fetchCategroyList " + response);
                    if (!response.equals(null)) {
                        Gson gson = new Gson();
                        newCategoryListModel = gson.fromJson(response, NewCategoryListModel.class);
                        int status = newCategoryListModel.getStatus();
                        String msg = newCategoryListModel.getMessage().trim();

                        if (status == 200 && msg.trim().equals("success")) {
                            for (int i = 0; i < newCategoryListModel.getData().size(); i++) {
                                NewCategoryListModel.DataBean dataBean = newCategoryListModel.getData().get(i);
                                dbHandler.insertCategory(dataBean.getName(), dataBean.getId());
                            }
                        }
                    }
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    error.printStackTrace();
                }
            });
            int socketTimeout = 600000; // 30 seconds. You can change it
            RetryPolicy policy = new DefaultRetryPolicy(socketTimeout,
                    DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                    DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);

            stringRequest.setRetryPolicy(policy);
            AppSingleton.getInstance(NavDrawerActivity.this).addToRequestQueue(stringRequest);
        }
    }

    public void fetchExplorerCatList() {
        try {
            String query = "SELECT * FROM " + TABLE_EXPLORE_CATEGORY;
            if (query != null) {
                count = dbHandler.rowCount(query);
                Log.e("count2", "" + count);

            }
        } catch (Exception e) {

        }

        if (count < 1) {
            String url = WebUrl.EXPLORE_CATEGORY_URL;
            Log.d("fetchExplorerCategroyLi", url);
            StringRequest stringRequest = new StringRequest(Request.Method.GET, url, new Response.Listener<String>() {
                @Override
                public void onResponse(String response) {
                    Log.e("fetchExplorerCatList", "fetchExplorerCatList: " + response);
                    if (!response.equals(null)) {
                        Gson gson = new Gson();
                        NewCategoryListModel newCategoryListModel = gson.fromJson(response, NewCategoryListModel.class);
                        int status = newCategoryListModel.getStatus();
                        String msg = newCategoryListModel.getMessage().trim();
                        if (status == 200 && msg.trim().equals("success")) {
                            // dbHandler.deleteOldExploreCategoryData();
                            for (int i = 0; i < newCategoryListModel.getData().size(); i++) {
                                NewCategoryListModel.DataBean dataBean = newCategoryListModel.getData().get(i);
                                dbHandler.insertExploreCategory(dataBean.getName(), dataBean.getId());
                            }
                        }
                    }
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    error.printStackTrace();
                }
            });
            int socketTimeout = 600000; // 30 seconds. You can change it
            RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
            stringRequest.setRetryPolicy(policy);
            AppSingleton.getInstance(NavDrawerActivity.this).addToRequestQueue(stringRequest);
        }
    }

    public void fetchTalentCatList() {
        try {
            String query = "SELECT * FROM " + TABLE_TALENT_CATEGORY;
            if (query != null) {
                count = dbHandler.rowCount(query);
                Log.e("count2", "" + count);
            }
        } catch (Exception e) {
            count = 0;
        }
        if (count < 1) {
            String url = WebUrl.TALENT_CATEGORY_URL;
            Log.d("fetchExplorerCategroyLi", url);
            StringRequest stringRequest = new StringRequest(Request.Method.GET, url, new Response.Listener<String>() {
                @Override
                public void onResponse(String response) {
                    Log.e("fetchExplorerCatList", "fetchExplorerCatList: " + response);
                    if (!response.equals(null)) {
                        Gson gson = new Gson();
                        NewCategoryListModel newCategoryListModel = gson.fromJson(response, NewCategoryListModel.class);
                        int status = newCategoryListModel.getStatus();
                        String msg = newCategoryListModel.getMessage().trim();
                        if (status == 200 && msg.trim().equals("success")) {
                            // dbHandler.deleteOldExploreCategoryData();
                            for (int i = 0; i < newCategoryListModel.getData().size(); i++) {
                                NewCategoryListModel.DataBean dataBean = newCategoryListModel.getData().get(i);
                                dbHandler.insertTalentCategory(dataBean.getName(), dataBean.getId());
                            }
                        }
                    }
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    error.printStackTrace();
                }
            });
            int socketTimeout = 600000; // 30 seconds. You can change it
            RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
            stringRequest.setRetryPolicy(policy);
            AppSingleton.getInstance(NavDrawerActivity.this).addToRequestQueue(stringRequest);
        }
    }

    private void registerBroadCastReceiver(BroadcastReceiver broadcastReceiver) {
        IntentFilter filter = new IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION);
        this.registerReceiver(broadcastReceiver, filter);
        isBroadCastRegister = true;
    }

    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        // Sync the toggle state after onRestoreInstanceState has occurred.
        toggle.syncState();
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        toggle.onConfigurationChanged(newConfig);
    }

    public static void updateProfile(Context context) {
        try {
            if (!SaveSharedPreference.getUserID(context).equals("")) {
                ln_loggedin.setVisibility(View.VISIBLE);
                ln_notlogin.setVisibility(View.GONE);
                Glide.with(context).load(SaveSharedPreference.getUserIMAGE(context)).error(R.drawable.user).into(imgprofile);
                txtname.setText(SaveSharedPreference.getUserName(context));
                txtemailheader.setText(SaveSharedPreference.getUserEMAIL(context));
            } else {
                ln_loggedin.setVisibility(View.GONE);
                ln_notlogin.setVisibility(View.VISIBLE);

            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void setFloatButton() {
        ImageView icon = new ImageView(this);
        icon.setImageResource(R.drawable.float_94);

        icon.setScaleType(ImageView.ScaleType.FIT_XY);
        actionButton = new FloatingActionButton.Builder(this).setLayoutParams(new FloatingActionButton.LayoutParams(140, 140))
                .setContentView(icon, new FloatingActionButton.LayoutParams(120, 120)).build();

        SubActionButton.Builder itemBuilder = new SubActionButton.Builder(this);
        // repeat many times:
        // SubActionButton.Builder rLSubBuilder = new SubActionButton.Builder(this);
        FloatingActionButton.LayoutParams params = new FloatingActionButton.LayoutParams(110, 110);

        // rLSubBuilder.setLayoutParams(params);

        ImageView itemIcon1 = new ImageView(this);
        itemIcon1.setImageResource(R.drawable.camera_circule_64);
        //itemIcon1.setLayoutParams(params);

        ImageView itemIcon2 = new ImageView(this);
        itemIcon2.setImageResource(R.drawable.video_circule_64);
        //itemIcon2.setLayoutParams(params);

        ImageView itemIcon3 = new ImageView(this);
        itemIcon3.setImageResource(R.drawable.voice_circule_64);
        //itemIcon3.setLayoutParams(params);

        ImageView itemIcon4 = new ImageView(this);
        itemIcon4.setImageResource(R.drawable.gallery_circule_64);

        ImageView itemIcon5 = new ImageView(this);
        itemIcon5.setImageResource(R.drawable.pencil_cirule_64);

        itemBuilder.setLayoutParams(params);
        // itemIcon4.setLayoutParams(params);

        button_text = itemBuilder.setContentView(itemIcon5).build();

        button_camera = itemBuilder.setContentView(itemIcon1).build();

        button_text.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (SaveSharedPreference.getUserID(NavDrawerActivity.this).equals("")) {
                    startActivity(new Intent(NavDrawerActivity.this, Login_Activity.class));
                } else {
                    updateDisplay(new CreateFragment());
                }
            }
        });
        button_camera.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (SaveSharedPreference.getUserID(NavDrawerActivity.this).equals("")) {
                    startActivity(new Intent(NavDrawerActivity.this, Login_Activity.class));
                } else if (ContextCompat.checkSelfPermission(NavDrawerActivity.this, android.Manifest.permission.CAMERA)
                        != PackageManager.PERMISSION_GRANTED) {
                    ActivityCompat.requestPermissions(NavDrawerActivity.this,
                            new String[]{android.Manifest.permission.CAMERA}, 0);
                } else {
                    cameraIntent();
                }
            }
        });
        button_video = itemBuilder.setContentView(itemIcon2).build();
        button_video.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (SaveSharedPreference.getUserID(NavDrawerActivity.this).equals("")) {
                    startActivity(new Intent(NavDrawerActivity.this, Login_Activity.class));
                } else if (ContextCompat.checkSelfPermission(NavDrawerActivity.this, android.Manifest.permission.CAMERA)
                        != PackageManager.PERMISSION_GRANTED) {
                    ActivityCompat.requestPermissions(NavDrawerActivity.this,
                            new String[]{android.Manifest.permission.CAMERA}, 0);
                } else {
                    createVideo();
                }
            }
        });

        button_audio = itemBuilder.setContentView(itemIcon3).build();
        button_audio.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (SaveSharedPreference.getUserID(NavDrawerActivity.this).equals("")) {
                    startActivity(new Intent(NavDrawerActivity.this, Login_Activity.class));
                } else if (ContextCompat.checkSelfPermission(NavDrawerActivity.this, android.Manifest.permission.RECORD_AUDIO) != PackageManager.PERMISSION_GRANTED) {
                    ActivityCompat.requestPermissions(NavDrawerActivity.this,
                            new String[]{android.Manifest.permission.RECORD_AUDIO},
                            REQUEST_MICROPHONE);
                } else {
                    recordAudio();
                }

            }
        });
        button_gallery = itemBuilder.setContentView(itemIcon4).build();
        button_gallery.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (SaveSharedPreference.getUserID(NavDrawerActivity.this).equals("")) {
                    startActivity(new Intent(NavDrawerActivity.this, Login_Activity.class));
                } else if (ContextCompat.checkSelfPermission(NavDrawerActivity.this, android.Manifest.permission.CAMERA)
                        != PackageManager.PERMISSION_GRANTED) {
                    ActivityCompat.requestPermissions(NavDrawerActivity.this,
                            new String[]{android.Manifest.permission.CAMERA},
                            0);
                } else {
                    galleryIntent();
                }

            }
        });
        //attach the sub buttons to the main button
        actionMenu = new FloatingActionMenu.Builder(this)
                .addSubActionView(button_camera)
                .addSubActionView(button_video)
                .addSubActionView(button_audio)
                .addSubActionView(button_gallery)
                .addSubActionView(button_text)
                .setRadius(270)
                .attachTo(actionButton)
                .build();

        actionMenu.setStateChangeListener(new FloatingActionMenu.MenuStateChangeListener() {
            @Override
            public void onMenuOpened(FloatingActionMenu floatingActionMenu) {
                Log.e("open", "float");
                shadowview.setVisibility(View.VISIBLE);
            }

            @Override
            public void onMenuClosed(FloatingActionMenu floatingActionMenu) {
                Log.e("close", "float");
                shadowview.setVisibility(View.GONE);
            }
        });
    }

    private void setHome(HomeTab_fragment dashboardFragment) {
        getSupportFragmentManager().beginTransaction().
                replace(R.id.frame_container, dashboardFragment,"hometab").commit();
    }

    private Fragment getCurrentFragment() {
        if (getSupportFragmentManager().getBackStackEntryCount() > 0) {
            String fragmentTag = getSupportFragmentManager()
                    .getBackStackEntryAt(getSupportFragmentManager().getBackStackEntryCount() - 1).getName();
            Fragment currentFragment = getSupportFragmentManager().findFragmentByTag(fragmentTag);

            return currentFragment;
        } else {
            return null;
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (isBroadCastRegister == false) {
            registerBroadCastReceiver(broadcastReceiver);
        }
        navigationDrawerAdapter.notifyDataSetChanged();
        //container_bottom_tab.setVisibility(View.GONE);
        //imgAddReport.setVisibility(View.GONE);
    }

    @Override
    public boolean onCreateOptionsMenu(final Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.nav_drawer, menu);

        myActionMenuItem = menu.findItem(R.id.action_settings);
        menu_form = menu.findItem(R.id.action_form);

        menubookmark = menu.findItem(R.id.menu_bookmark);
        menucomment = menu.findItem(R.id.menu_comment);
        menushare = menu.findItem(R.id.menu_share);
        menutextfeed = menu.findItem(R.id.menu_textfeed);

        final SearchView searchView = (SearchView) myActionMenuItem.getActionView();
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @RequiresApi(api = Build.VERSION_CODES.M)
            @Override
            public boolean onQueryTextSubmit(String query) {
                Log.e("cliacked", "on text submit");
                static_query = query;
                if (!searchView.isIconified()) {
                    searchView.setIconified(true);
                }
                myActionMenuItem.collapseActionView();
                dialog_category();
                return true;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                return true;
            }
        });
        MenuItemCompat.setOnActionExpandListener(myActionMenuItem, new MenuItemCompat.OnActionExpandListener() {
            @Override
            public boolean onMenuItemActionExpand(MenuItem item) {
                //toolbar.setBackgroundResource(0);
                // menu_form.setVisible(false);
                return true;
            }

            @Override
            public boolean onMenuItemActionCollapse(MenuItem item) {
                //toolbar.setBackgroundResource(R.drawable.bg_toolbar);
                try {
                    toolbar.setBackgroundColor(getResources().getColor(R.color.black));
                }catch (Exception e){

                }


             //toolbar.setLogo(R.drawable.instafeed_logospace);
             /*  menu_form.setVisible(true);
                menu_form.setIcon(R.drawable.upload_48);
                myActionMenuItem.setVisible(true);
                myActionMenuItem.setIcon(R.drawable.places_ic_search);*/

                //toolbar.setVisibility(View.VISIBLE);
                //toggle.syncState();
                // setHome(new DashboardFragment());
                //menu.clear();
                //refreshActionBarMenu(NavDrawerActivity.this);
                // Toast.makeText(NavDrawerActivity.this,"collapsed searchview ", Toast.LENGTH_SHORT).show();
                toggle.syncState();
                return true;
            }
        });
        return true;
    }

    public void doSearch() {
        Dialogs.showProDialog(NavDrawerActivity.this, "Loading");
        StringRequest stringRequest = new StringRequest(Request.Method.POST, Apis.search,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.e("logresponse", "" + response);
                        try {
                            res_search(response);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        // signupTrace(response);
                        Dialogs.disDialog();
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Dialogs.disDialog();
                        DialogInterface.OnClickListener onClickTryAgain = new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                Dialogs.disDialog();
                                doSearch();
                            }
                        };
                        if (error instanceof TimeoutError) {
                            Dialogs.disDialog();
                            Alerts.timeoutErrorAlert(NavDrawerActivity.this, onClickTryAgain);
                        } else if (error instanceof NoConnectionError) {
                            Dialogs.disDialog();
                            Alerts.internetConnectionErrorAlert(NavDrawerActivity.this, onClickTryAgain);
                        }
                    }
                }
        )
        {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> map = new HashMap<String, String>();
                map.put("query", static_query);
                map.put("type", static_type);
                Log.e("mapSearch", "" + map);
                return map;
            }

        };
        RequestQueue requestQueue = Volley.newRequestQueue(this);
        requestQueue.add(stringRequest);
    }

  /*private class LongOperation extends AsyncTask<String, Void, ArrayList<HashMap<String,String>>> {

        @Override
        protected ArrayList<HashMap<String,String>> doInBackground(String... params) {
            fetchbothmedias();
            //fetch_onlyimages();
            return array_imahges;
        }

        @Override
        protected void onPostExecute(ArrayList<HashMap<String,String>> arrayList) {

            //adapter=new ImageAdapter(CameraDemoActivity.this,arrayList);
            //recyclerView.setAdapter(adapter);
            adapter.notifyDataSetChanged();
        }

        @Override
        protected void onPreExecute() {}
    }
*/
    /*
    private void fetchbothmedias(){
        array_imahges.clear();
        try {
            String[] projection = {
                    MediaStore.Files.FileColumns._ID,
                    MediaStore.Files.FileColumns.DATA,
                    MediaStore.Files.FileColumns.DATE_ADDED,
                    MediaStore.Files.FileColumns.MEDIA_TYPE,
                    MediaStore.Files.FileColumns.MIME_TYPE,
                    MediaStore.Files.FileColumns.TITLE
            };
            // Return only video and image metadata.
            String selection =
                    MediaStore.Files.FileColumns.MEDIA_TYPE + "="
                            + MediaStore.Files.FileColumns.MEDIA_TYPE_IMAGE
                            + " OR "
                            + MediaStore.Files.FileColumns.MEDIA_TYPE + "="
                            + MediaStore.Files.FileColumns.MEDIA_TYPE_VIDEO;

            Uri queryUri = MediaStore.Files.getContentUri("external");

            Cursor cursor = getContentResolver().query(
                    queryUri,
                    projection,
                    selection,
                    null, // Selection args (none).
                    MediaStore.Files.FileColumns.DATE_ADDED + " DESC" // Sort order.
            );

            //Cursor cursor = cursorLoader.loadInBackground();
            int image_column_index = cursor.getColumnIndex(MediaStore.Files.FileColumns._ID);

            this.count = cursor.getCount();
            //this.thumbnails = new Bitmap[this.count];
            //this.arrPath = new String[this.count];
            HashMap<String, String> hashMap;
            int i=0;
            for (i = 0; i < 100; i++) {
                cursor.moveToPosition(i);

                int id = cursor.getInt(image_column_index);
                int dataColumnIndex = cursor.getColumnIndex(MediaStore.Files.FileColumns.DATA);
                int type = cursor.getColumnIndex(MediaStore.Files.FileColumns.MEDIA_TYPE);

                String t = String.valueOf(cursor.getInt(type));
                //int thumb =cursor.getColumnIndexOrThrow(Images.Thumbnails.DATA);
                hashMap = new HashMap<>();
                hashMap.put("path", cursor.getString(dataColumnIndex));
                hashMap.put("id", "" + i);
                hashMap.put("type", "" + cursor.getInt(type));
                String result = "";
                //Log.e("type",""+t);
                if (t.equals("1")){
                    // Log.e("image","image"+result);
                    Cursor cursor2 = MediaStore.Images
                            .Thumbnails.queryMiniThumbnail(getContentResolver(), id,
                                    MediaStore.Images.Thumbnails.MINI_KIND, null);
                    if (cursor2 != null && cursor2.getCount() > 0) {
                        cursor2.moveToFirst();
                        result = cursor2
                                .getString(cursor2
                                        .getColumnIndexOrThrow(MediaStore.Images.Thumbnails.DATA));
                        cursor2.close();
                    }

                    if (!result.isEmpty() && result!=null){
                        Log.e("image",""+result);
                        hashMap.put("image", result);
                        array_imahges.add(hashMap);
                    }
                }else if(t.equals("3")) {
                    String thumbd = getThumbnailPathForLocalFile(id);
                    if (thumbd != null && !thumbd.isEmpty()) {
                        hashMap.put("video", thumbd);
                        array_imahges.add(hashMap);
                        Log.e("thumb", "" + thumbd);
                    }
                }else {

                }
             *//*                if (i==50){
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            adapter.notifyDataSetChanged();
                        }
                    });
             }*//*
            }

            //Log.e("arryyguyg",""+array_imahges.size());
            //Log.e("arryyg",""+array_imahges);
        }catch (Exception e){

        }finally {
            if( cursor != null && !cursor.isClosed())
                cursor.close();
        }
       *//* adapter=new ImageAdapter(this,array_imahges);
        recyclerView.setAdapter(adapter);
       *//*
    }*/

    private void res_search(String response) throws JSONException {
        JSONObject jsonObject = new JSONObject(response);
        if (jsonObject.get("data") instanceof JSONArray) {
            Log.e("log", "" + response);
            ArrayList<HashMap<String,String>> arraysearch=new ArrayList<>();
            Gson gson = new Gson();
            if (jsonObject.getString("type").equalsIgnoreCase("users")) {
                if (!arrayUsers.isEmpty()) {
                    arrayUsers.clear();
                }
                user_model = gson.fromJson(response, User_model.class);
                int status = user_model.getStatus();
                String msg = user_model.getMessage();
                type = user_model.getType();
                Log.e("type here", "" + type);
                if (msg.equalsIgnoreCase("success") && status == 200) {
                    if (user_model.getData() != null && !user_model.getData().isEmpty()) {
                        for (int i = 0; i < user_model.getData().size(); i++) {
                            try {
                                User_model.DataBean dataBean = user_model.getData().get(i);
                                arrayUsers.add(dataBean);
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }
                    }
                    // if (!array_Search.isEmpty()){
                    Fragment_search fragment = new Fragment_search();
                    Bundle bundle = new Bundle();
                    bundle.putSerializable("array_search", arrayUsers);
                    fragment.setArguments(bundle);
                    getSupportFragmentManager().beginTransaction()
                            .add(R.id.frame_container, fragment,"search").addToBackStack("search").commit();
                }
            } else {
                if (!array_Search.isEmpty()) {
                    array_Search.clear();
                }

                HashMap<String,String> arrayhashmap;
                if (jsonObject.getString("type").equalsIgnoreCase("news")){
                    for (int i=0;i<jsonObject.getJSONArray("data").length();i++)
                    {
                        arrayhashmap=new HashMap<>();
                        arrayhashmap.put("id",jsonObject.getJSONArray("data").getJSONObject(i).getString("id"));
                        arrayhashmap.put("catid",jsonObject.getJSONArray("data").getJSONObject(i).getString("news_category_id"));
                        arrayhashmap.put("title",jsonObject.getJSONArray("data").getJSONObject(i).getString("title"));
                        arrayhashmap.put("description",jsonObject.getJSONArray("data").getJSONObject(i).getString("short_description"));
                        arraysearch.add(arrayhashmap);
                    }
                    type="news";
                    Fragment_search fragment = new Fragment_search();
                    Bundle bundle = new Bundle();
                    bundle.putSerializable("array_search", arraysearch);
                    fragment.setArguments(bundle);
                    getSupportFragmentManager().beginTransaction()
                            .add(R.id.frame_container, fragment,"search").addToBackStack("search").commit();
                }else if (jsonObject.getString("type").equalsIgnoreCase("talents")){
                    for (int i=0;i<jsonObject.getJSONArray("data").length();i++)
                    {
                        arrayhashmap=new HashMap<>();
                        arrayhashmap.put("id",jsonObject.getJSONArray("data").getJSONObject(i).getString("id"));
                        arrayhashmap.put("title",jsonObject.getJSONArray("data").getJSONObject(i).getString("title"));
                        arrayhashmap.put("description",jsonObject.getJSONArray("data").getJSONObject(i).getString("short_description"));
                        arraysearch.add(arrayhashmap);
                    }
                    type="talents";
                    Fragment_search fragment = new Fragment_search();
                    Bundle bundle = new Bundle();
                    bundle.putSerializable("array_search", arraysearch);
                    fragment.setArguments(bundle);
                    getSupportFragmentManager().beginTransaction()
                            .add(R.id.frame_container, fragment,"search").addToBackStack("search").commit();

                }else if (jsonObject.getString("type").equalsIgnoreCase("opinions")){
                    for (int i=0;i<jsonObject.getJSONArray("data").length();i++)
                    {
                        arrayhashmap=new HashMap<>();
                        arrayhashmap.put("id",jsonObject.getJSONArray("data").getJSONObject(i).getString("id"));
                        arrayhashmap.put("title",jsonObject.getJSONArray("data").getJSONObject(i).getString("title"));
                        arrayhashmap.put("description",jsonObject.getJSONArray("data").getJSONObject(i).getString("description"));
                        arraysearch.add(arrayhashmap);
                    }
                    type="opinions";
                    Fragment_search fragment = new Fragment_search();
                    Bundle bundle = new Bundle();
                    bundle.putSerializable("array_search",arraysearch);
                    fragment.setArguments(bundle);
                    getSupportFragmentManager().beginTransaction()
                            .add(R.id.frame_container, fragment,"search").addToBackStack("search").commit();
                }else {
                    type="";
                    Fragment_search fragment = new Fragment_search();
                    Bundle bundle = new Bundle();
                    bundle.putSerializable("array_search",arraysearch);
                    fragment.setArguments(bundle);
                    getSupportFragmentManager().beginTransaction()
                            .add(R.id.frame_container, fragment,"search").addToBackStack("search").commit();
                }


             //if (jsonObject.getJSONArray("data"))
              /*  search_model = gson.fromJson(response, Search_model.class);
                int status = search_model.getStatus();
                String msg = search_model.getMessage();
                type = search_model.getType();
                if (msg.equalsIgnoreCase("success") && status == 200) {
                    if (search_model.getData() != null && !search_model.getData().isEmpty()) {
                        for (int i = 0; i < search_model.getData().size(); i++) {
                            try {
                                Search_model.DataBean dataBean = search_model.getData().get(i);
                                array_Search.add(dataBean);
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }
                    }
              */ // if (!array_Search.isEmpty()){

                }
            }
        else {
            Fragment_search fragment = new Fragment_search();
            Bundle bundle = new Bundle();
            bundle.putSerializable("array_search", array_Search);
            fragment.setArguments(bundle);
            getSupportFragmentManager().beginTransaction()
                    .add(R.id.frame_container, fragment,"search").addToBackStack("search").commit();
            // AppCompatActivity activity = (AppCompatActivity) v.getContext();
            // getSupportFragmentManager().beginTransaction().add(R.id.frame_container, fragment,"tempfrag").addToBackStack(null).commit();
            //Toast.makeText(NavDrawerActivity.this,jsonObject.getJSONObject("data").getString("error"),Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on                 the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        //noinspection SimplifiableIfStatement
        //>>>replace>> add>>>replace>>> >>   2 load
        switch (item.getItemId()) {
            case R.id.action_form:
                if (SaveSharedPreference.getUserID(NavDrawerActivity.this).equals("")) {
                    startActivity(new Intent(NavDrawerActivity.this, Login_Activity.class));
                } else {
                    myActionMenuItem.collapseActionView();
                   /* NavDrawerActivity.this.getSupportFragmentManager()
                            .beginTransaction().add(R.id.frame_container, new CreateFragment())
                            .addToBackStack(null)
                            .commit();*/
                   SuccessDialogFragment.state=true;
                   startActivity(new Intent(NavDrawerActivity.this, CameraDemoActivity.class));
                }
                return true;

            case R.id.menu_bookmark:
                if (SaveSharedPreference.getUserID(NavDrawerActivity.this).equals("")) {
                    Toast.makeText(NavDrawerActivity.this,"please login to continue!", Toast.LENGTH_SHORT).show();
                }else {
                    if (getSupportFragmentManager().findFragmentByTag("tempfrag") instanceof Newsdetail_fragment) {
                        Log.e("first", "");
                        Newsdetail_fragment frag = ((Newsdetail_fragment) getSupportFragmentManager().findFragmentByTag("tempfrag"));
                        if (frag != null)
                            frag.do_bookmark();
                    }
                }
                return true;

            case R.id.menu_share:
                if (getSupportFragmentManager().findFragmentByTag("tempfrag") instanceof Newsdetail_fragment) {
                    Log.e("first", "");
                    Newsdetail_fragment frag = ((Newsdetail_fragment) getSupportFragmentManager().findFragmentByTag("tempfrag"));
                    if (frag != null)
                        frag.sharecontent();
                } else if (getSupportFragmentManager().findFragmentByTag("tempfrag") instanceof Talentdetail_fragment) {
                    Log.e("second", "");
                    Talentdetail_fragment frag = ((Talentdetail_fragment) getSupportFragmentManager().findFragmentByTag("tempfrag"));
                    if (frag != null)
                        frag.sharecontent();
                } else if (getSupportFragmentManager().findFragmentByTag("tempfrag") instanceof Explore_detailfragment) {
                    Log.e("second", "");
                    Explore_detailfragment frag = ((Explore_detailfragment) getSupportFragmentManager().findFragmentByTag("tempfrag"));
                    if (frag != null)
                        frag.sharecontent();
                } else if (getSupportFragmentManager().findFragmentByTag("tempfrag") instanceof Opiniondetail_fragment) {
                    Log.e("second", "");
                    Opiniondetail_fragment frag = ((Opiniondetail_fragment) getSupportFragmentManager().findFragmentByTag("tempfrag"));
                    if (frag != null)
                        frag.sharecontent();
                } else if (getSupportFragmentManager().findFragmentByTag("tempfrag") instanceof Blogdetail_fragment) {
                    Log.e("second", "");
                    Blogdetail_fragment frag = ((Blogdetail_fragment) getSupportFragmentManager().findFragmentByTag("tempfrag"));
                    if (frag != null)
                        frag.sharecontent();
                } else if (getSupportFragmentManager().findFragmentByTag("tempfrag") instanceof Petitiondetail_fragment) {
                    Log.e("second", "");
                    Petitiondetail_fragment frag = ((Petitiondetail_fragment) getSupportFragmentManager().findFragmentByTag("tempfrag"));
                    if (frag != null)
                        frag.sharecontent();
                }
                return true;

            case R.id.menu_comment:
                if (getSupportFragmentManager().findFragmentByTag("tempfrag") instanceof Newsdetail_fragment) {
                    Log.e("first", "");
                    Newsdetail_fragment frag = ((Newsdetail_fragment) getSupportFragmentManager().findFragmentByTag("tempfrag"));
                    if (frag != null)
                        frag.getCommentlist();
                } else if (getSupportFragmentManager().findFragmentByTag("tempfrag") instanceof Talentdetail_fragment) {
                    Log.e("second", "");
                    Talentdetail_fragment frag = ((Talentdetail_fragment) getSupportFragmentManager().findFragmentByTag("tempfrag"));
                    if (frag != null)
                        frag.getcommentlist();
                } else if (getSupportFragmentManager().findFragmentByTag("tempfrag") instanceof Explore_detailfragment) {
                    //Log.e("third", "");
                    Explore_detailfragment frag = ((Explore_detailfragment) getSupportFragmentManager().findFragmentByTag("tempfrag"));
                    if (frag != null)
                        frag.getcommentlist();
                } else if (getSupportFragmentManager().findFragmentByTag("tempfrag") instanceof Opiniondetail_fragment) {
                    //Log.e("third", "");
                    Opiniondetail_fragment frag = ((Opiniondetail_fragment) getSupportFragmentManager().findFragmentByTag("tempfrag"));
                    if (frag != null)
                        frag.getcommentlist();
                } else if (getSupportFragmentManager().findFragmentByTag("tempfrag") instanceof Blogdetail_fragment) {
                    //Log.e("third", "");
                    Blogdetail_fragment frag = ((Blogdetail_fragment) getSupportFragmentManager().findFragmentByTag("tempfrag"));
                    if (frag != null)
                        frag.getcommentlist();
                } else if (getSupportFragmentManager().findFragmentByTag("tempfrag") instanceof Petitiondetail_fragment) {
                    //Log.e("third", "");
                    Petitiondetail_fragment frag = ((Petitiondetail_fragment) getSupportFragmentManager().findFragmentByTag("tempfrag"));
                    if (frag != null)
                        frag.getcommentlist();
                }
                return true;
            default:
                break;
        }
        //return false;
        return super.onOptionsItemSelected(item);
    }

    public void updateDisplay(Fragment fragment) {
        getSupportFragmentManager().beginTransaction()
                .add(R.id.frame_container, fragment).addToBackStack(null).commit();
    }

    public static void enableViews(boolean enable, Context context) {
        // To keep states of ActionBar and ActionBarDrawerToggle synchronized,
        // when you enable on one, you disable on the other.
        // And as you may notice, the order for this operation is disable first, then enable - VERY VERY IMPORTANT.
        if (enable) {
            // Remove hamburger
            toggle.setDrawerIndicatorEnabled(false);
            toggle.setHomeAsUpIndicator(R.drawable.back);
            // Show back button
            //  context.getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            // when DrawerToggle is disabled i.e. setDrawerIndicatorEnabled(false), navigation icon
            // clicks are disabled i.e. the UP button will not work.
            // We need to add a listener, as in below, so DrawerToggle will forward
            // click events to this listener.
            if (!mToolBarNavigationListenerIsRegistered) {
                toggle.setToolbarNavigationClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        // Doesn't have to be onBackPressed

                    }
                });

                mToolBarNavigationListenerIsRegistered = true;
            }

        } else {
            // Remove back button
            //getSupportActionBar().setDisplayHomeAsUpEnabled(false);
            // Show hamburger
            toggle.setDrawerIndicatorEnabled(true);
            // Remove the/any drawer toggle listener
            toggle.setToolbarNavigationClickListener(null);
            mToolBarNavigationListenerIsRegistered = false;
        }
    }

    private void enableExpandableList() {

        drawer.closeDrawer(GravityCompat.START);

        listDataHeader = new ArrayList<String>();
        listDataChild = new HashMap<String, List<String>>();

        final ExpandableListView expListView = (ExpandableListView) findViewById(R.id.left_drawer);
        //  prepareListData(listDataHeader, listDataChild);
        expListView.setAdapter(navigationDrawerAdapter);
        ArrayList<Screen> screens = new ArrayList<>();
        // screens.add(new Screen("Home","0"));

        Screen screen1 = new Screen();
        screen1.setName(getString(R.string.nav_dashborad));
        screen1.setId(String.valueOf(ScreenConstants.HOME));

        Screen screen2 = new Screen();
        screen2.setName(getString(R.string.nav_followers));
        screen2.setId(String.valueOf(ScreenConstants.FOLLOWER));

        Screen screen3 = new Screen();
        screen3.setName(getString(R.string.nav_following));
        screen3.setId(String.valueOf(ScreenConstants.FOLLOWING));

        Screen screen4 = new Screen();
        screen4.setName(getString(R.string.nav_my_account));
        screen4.setId(String.valueOf(ScreenConstants.MY_ACCOUNT));

        Screen screen15 = new Screen();
        screen15.setName(getString(R.string.nav_change_password));
        screen15.setId(String.valueOf(ScreenConstants.CHANGE_PASSWORD));

        Screen screen11 = new Screen();
        screen11.setName(getString(R.string.nav_setting));
        screen11.setId(String.valueOf(ScreenConstants.SETTINGS));

        Screen screen_bookmark = new Screen();
        screen_bookmark.setId(String.valueOf(ScreenConstants.MYBOOKMARKS));
        screen_bookmark.setName(getString(R.string.mybookmarks));


        //SaveOfflineFragment
        SubScreen subScreen1 = new SubScreen();
        subScreen1.setId(String.valueOf(ScreenConstants.MY_ACCOUNT));
        subScreen1.setName(getString(R.string.nav_my_account));

        /*SubScreen subScreen2 = new SubScreen();
        subScreen2.setId(String.valueOf(ScreenConstants.CHANGE_PASSWORD));
        subScreen2.setName(getString(R.string.nav_change_password));*/

        SubScreen subScreen3 = new SubScreen();
        subScreen3.setId(String.valueOf(ScreenConstants.LOCATION));
        subScreen3.setName(getString(R.string.location));

        ArrayList<SubScreen> subScreens = new ArrayList<>();
        subScreens.add(subScreen1);
        //subScreens.add(subScreen2);
        subScreens.add(subScreen3);
        //screen4.setSubScreens(subScreens);

        Screen screen5 = new Screen();
        screen5.setName(getString(R.string.nav_create_post));
        screen5.setId(String.valueOf(ScreenConstants.CREATE_A_POST));

     /*   Screen screen7 = new Screen();
        screen7.setName(getString(R.string.location));
        screen7.setId(String.valueOf(ScreenConstants.LOCATION));
     */

        Screen screen8 = new Screen();
        screen8.setName(getString(R.string.nav_mypost));
        screen8.setId(String.valueOf(ScreenConstants.SAVE_OFFLINE));

        Screen screen12 = new Screen();
        screen12.setName(getString(R.string.about_us));
        screen12.setId(String.valueOf(ScreenConstants.ABOUT_US));

        Screen screen6 = new Screen();
        screen6.setName(getString(R.string.nav_logout));
        screen6.setId(String.valueOf(ScreenConstants.LOGOUT));

        Screen screen_mypost = new Screen();
        screen_mypost.setName(getString(R.string.mypost));
        screen_mypost.setId(String.valueOf(ScreenConstants.MYPOST));


        screens.add(screen1);
        screens.add(screen4);
        screens.add(screen_bookmark);
        screens.add(screen2);
        screens.add(screen3);
        screens.add(screen_mypost);
        //screens.add(screen7);
        screens.add(screen8);
        screens.add(screen5);
        screens.add(screen12);

        if (!SaveSharedPreference.getUserID(NavDrawerActivity.this).equals("")) {
            screens.add(screen15);
        }
        if (!SaveSharedPreference.getUserID(NavDrawerActivity.this).equals("")) {
            screens.add(screen6);
        }
        navigationDrawerAdapter.update(screens, NavDrawerActivity.this);
        expListView.setOnGroupClickListener(new ExpandableListView.OnGroupClickListener() {
            @Override
            public boolean onGroupClick(ExpandableListView parent, View v,
                                        int groupPosition, long id) {
                onScreenSelected(id);
                return false;
            }
        });

        // Listview Group expanded listener
        expListView.setOnGroupExpandListener(new ExpandableListView.OnGroupExpandListener() {
            @Override
            public void onGroupExpand(int groupPosition) {
                int count = navigationDrawerAdapter.getGroupCount();
                for (int i = 0; i < count; i++) {
                    if (i != groupPosition) {
                        expListView.collapseGroup(i);
                    }
                }
            }

        });

        // Listview Group collasped listener
        expListView.setOnGroupCollapseListener(new ExpandableListView.OnGroupCollapseListener() {
            @Override
            public void onGroupCollapse(int groupPosition) {

            }
        });

        // Listview on child click listener
        expListView.setOnChildClickListener(new ExpandableListView.OnChildClickListener() {
            @Override
            public boolean onChildClick(ExpandableListView parent, View v,
                                        int groupPosition, int childPosition, long id) {
                // TODO Auto-generated method stub
                onScreenSelected(id);
                drawer.closeDrawer(GravityCompat.START);

                return false;
            }
        });
    }

    private void onScreenSelected(long id) {
        switch ((int) id) {
            case ScreenConstants.HOME:
                drawer.closeDrawer(GravityCompat.START);

                getSupportFragmentManager().beginTransaction()
                        .replace(R.id.frame_container, new HomeTab_fragment()).commit();
                break;

            case ScreenConstants.SETTINGS:

                break;

            case ScreenConstants.MYBOOKMARKS:
                drawer.closeDrawer(GravityCompat.START);
                if (SaveSharedPreference.getUserID(NavDrawerActivity.this).equals("")) {
                    startActivity(new Intent(NavDrawerActivity.this, Login_Activity.class));
                } else {
                    NavDrawerActivity.this.getSupportFragmentManager()
                            .beginTransaction().add(R.id.frame_container, new Bookmark_fragment(), "bookmark").addToBackStack("bookmark").commit();
                }
                break;

            case ScreenConstants.LOGOUT:
                drawer.closeDrawer(GravityCompat.START);

                if (CommonFunctions.isConnected(NavDrawerActivity.this)) {
                    doLogout();
                } else {
                    Toast.makeText(NavDrawerActivity.this, "Internet not available", Toast.LENGTH_SHORT).show();
                }
                break;
            case ScreenConstants.CREATE_A_POST:
                drawer.closeDrawer(GravityCompat.START);
                if (SaveSharedPreference.getUserID(NavDrawerActivity.this).equals("")) {
                    startActivity(new Intent(NavDrawerActivity.this, Login_Activity.class));
                } else {
                    NavDrawerActivity.this.getSupportFragmentManager()
                            .beginTransaction().add(R.id.frame_container, new CreateFragment())
                            .addToBackStack(null)
                            .commit();
                }
                break;


            case ScreenConstants.LOCATION:
                drawer.closeDrawer(GravityCompat.START);
                //container_bottom_tab.setVisibility(View.GONE);
                getLocation_dlg();
                // updateDisplay(new CreateFragment());
                break;

            case ScreenConstants.MYPOST:
                drawer.closeDrawer(GravityCompat.START);
                if (SaveSharedPreference.getUserID(NavDrawerActivity.this).equals("")) {
                    Toast.makeText(NavDrawerActivity.this, "Please Login first", Toast.LENGTH_SHORT).show();
                } else {
                    Publicprofile_Fragment.flag = false;
                    NavDrawerActivity.this.getSupportFragmentManager().beginTransaction().
                            add(R.id.frame_container, new Publicprofile_Fragment()).addToBackStack(null).commit();
                }
                break;

            case ScreenConstants.CHANGE_PASSWORD:
                drawer.closeDrawer(GravityCompat.START);
                if (SaveSharedPreference.getUserID(NavDrawerActivity.this).equals("")) {
                    Toast.makeText(NavDrawerActivity.this, "Please Login first", Toast.LENGTH_SHORT).show();
                } else {
                    updateDisplay(new ChangePassword_fragment());
                }
                break;

            case ScreenConstants.FOLLOWER:
                drawer.closeDrawer(GravityCompat.START);
                if (SaveSharedPreference.getUserID(NavDrawerActivity.this).equals("")) {
                    Toast.makeText(NavDrawerActivity.this, "Please Login first", Toast.LENGTH_SHORT).show();
                } else {
                    follower = true;
                    updateDisplay(new Follow_Fragment());
                }
                break;

            case ScreenConstants.FOLLOWING:
                drawer.closeDrawer(GravityCompat.START);
                if (SaveSharedPreference.getUserID(NavDrawerActivity.this).equals("")) {
                    Toast.makeText(NavDrawerActivity.this, "Please Login first", Toast.LENGTH_SHORT).show();
                } else {
                    follower = false;
                    updateDisplay(new Follow_Fragment());
                }
                break;

            case ScreenConstants.MY_ACCOUNT:
                drawer.closeDrawer(GravityCompat.START);
                if (SaveSharedPreference.getUserID(NavDrawerActivity.this).equals("")) {
                    Toast.makeText(NavDrawerActivity.this, "Please Login first", Toast.LENGTH_SHORT).show();
                } else {
                    updateDisplay(new UpdateProfile_Fragment());
                }
                break;

            case ScreenConstants.SAVE_OFFLINE:
                drawer.closeDrawer(GravityCompat.START);
                updateDisplay(new SaveOfflineFragment());
                break;
        }
    }

    @RequiresApi(api = Build.VERSION_CODES.M)
    public void dialog_category() {
        final Dialog dialog = new Dialog(NavDrawerActivity.this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(true);
        dialog.setContentView(R.layout.dialog_category);
        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        lp.copyFrom(dialog.getWindow().getAttributes());
        lp.width = WindowManager.LayoutParams.MATCH_PARENT;
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
        dialog.getWindow().setAttributes(lp);

        final RadioGroup rgp = (RadioGroup) dialog.findViewById(R.id.rad_gp);
        RadioGroup.LayoutParams rprms;
        for (int i = 0; i < arraycategory.length; i++) {
            final RadioButton radioButton = new RadioButton(this);
            //radioButton.setTextAppearance(R.style.custom_radiobutton);
            radioButton.setText(arraycategory[i]);
            radioButton.setId(i + 1);
            rprms = new RadioGroup.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);
            rgp.addView(radioButton, rprms);
        }
        rgp.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            public void onCheckedChanged(RadioGroup rg, int checkedId) {
                for (int i = 0; i < rg.getChildCount(); i++) {
                    RadioButton btn = (RadioButton) rg.getChildAt(i);
                    if (btn.getId() == checkedId) {
                        static_type = btn.getText().toString();
                        Log.e("selcetd category are", "" + static_type);
                        dialog.dismiss();
                        doSearch();
                        // do something with text
                        return;
                    }
                }
            }
        });
        dialog.show();
    }

    public void getLocation_dlg() {
        dialog = new Dialog(NavDrawerActivity.this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(true);
        dialog.setContentView(R.layout.dialog_location);
        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        lp.copyFrom(dialog.getWindow().getAttributes());
        lp.width = WindowManager.LayoutParams.MATCH_PARENT;
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
        dialog.getWindow().setAttributes(lp);
        final AutoCompleteTextView auto_txtview = (AutoCompleteTextView) dialog.findViewById(R.id.auto_txtview);
        ArrayAdapter<String> adapter = new ArrayAdapter<String>
                (this, android.R.layout.select_dialog_item, location_strings);
        auto_txtview.setThreshold(1);    //will start working from first character
        auto_txtview.setAdapter(adapter);  //setting the adapter data into the AutoCompleteTextView
        auto_txtview.setTextColor(Color.BLACK);
        auto_txtview.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                parent.getItemAtPosition(position);
                dialog.dismiss();
            }
        });

        dialog.show();
    }

    public void getLocation() {
        // Dialogs.showProDialog(NavDrawerActivity.this, "Loading");
        StringRequest stringRequest = new StringRequest(Request.Method.POST, apitemplocation,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {

                        //Log.e("log", "" + response);
                        try {
                            resLocation(response);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        Dialogs.disDialog();
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Dialogs.disDialog();
                        DialogInterface.OnClickListener onClickTryAgain = new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                Dialogs.disDialog();
                                getLocation();
                            }
                        };
                        if (error instanceof TimeoutError) {
                            Dialogs.disDialog();
                            Alerts.timeoutErrorAlert(NavDrawerActivity.this, onClickTryAgain);
                        } else if (error instanceof NoConnectionError) {
                            Dialogs.disDialog();
                            Alerts.internetConnectionErrorAlert(NavDrawerActivity.this, onClickTryAgain);
                        }
                    }
                })

        {

            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> map = new HashMap<String, String>();
                map.put("user", SaveSharedPreference.getUserID(NavDrawerActivity.this));
                map.put("token", SaveSharedPreference.getPrefToken(NavDrawerActivity.this));
                map.put("location_id", location_id);
               // Log.e("userdetailmap", "" + map);
                return map;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Content-Type", "application/x-www-form-urlencoded");
                return params;
            }
        };
        RequestQueue requestQueue = Volley.newRequestQueue(this);
        requestQueue.add(stringRequest);
    }

    private void resLocation(String response) throws JSONException {
        if (!response.equals(null)) {
            JSONObject jsonObject = new JSONObject(response);
        }
    }

    public void doLogout() {
        // Dialogs.showProDialog(NavDrawerActivity.this, "Loading");
        StringRequest stringRequest = new StringRequest(Request.Method.POST, Apis.logout,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.e("log", "" + response);
                        try {
                            resLogout(response);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        Dialogs.disDialog();
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Dialogs.disDialog();
                        DialogInterface.OnClickListener onClickTryAgain = new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                Dialogs.disDialog();
                                doLogout();
                            }
                        };
                        if (error instanceof TimeoutError) {
                            Dialogs.disDialog();
                            Alerts.timeoutErrorAlert(NavDrawerActivity.this, onClickTryAgain);
                        } else if (error instanceof NoConnectionError) {
                            Dialogs.disDialog();
                            Alerts.internetConnectionErrorAlert(NavDrawerActivity.this, onClickTryAgain);
                        }
                    }
                }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> map = new HashMap<String, String>();
                map.put("user", SaveSharedPreference.getUserID(NavDrawerActivity.this));
                map.put("token", SaveSharedPreference.getPrefToken(NavDrawerActivity.this));
               // Log.e("userdetailmap", "" + map);
                return map;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Content-Type", "application/x-www-form-urlencoded");
                return params;
            }
        };
        RequestQueue requestQueue = Volley.newRequestQueue(this);
        requestQueue.add(stringRequest);
    }

    private void resLogout(String response) throws JSONException {
        if (!response.equals(null)) {
            JSONObject jsonObject = new JSONObject(response);
            if (jsonObject.getString("message").equalsIgnoreCase("success")) {
                Toast.makeText(NavDrawerActivity.this, jsonObject.getJSONObject("data").getString("message"), Toast.LENGTH_SHORT).show();
                SaveSharedPreference.setUserID(NavDrawerActivity.this, "");
                SaveSharedPreference.setPrefToken(NavDrawerActivity.this, "");
                SaveSharedPreference.setUserName(NavDrawerActivity.this, "");
                SaveSharedPreference.setFirstName(NavDrawerActivity.this, "");
                SaveSharedPreference.setUserIMAGE(NavDrawerActivity.this, "");
                setHome(new HomeTab_fragment());
                updateProfile(NavDrawerActivity.this);
                //Intent intent = new Intent(NavDrawerActivity.this, NavDrawerActivity.class);
                //intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                //startActivity(intent);
                //this.finish();
            } else {
                Toast.makeText(NavDrawerActivity.this, jsonObject.getJSONObject("data").getString("error"), Toast.LENGTH_SHORT).show();
            }
        }
    }

    public void getLocationdetails() {
        StringRequest stringRequest = new StringRequest(Request.Method.GET, "http://instafeed.org/api/locations",
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                       // Log.e("log", "" + response);
                        getResponse(response);
                        //Dialogs.disDialog();
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Dialogs.disDialog();
                        DialogInterface.OnClickListener onClickTryAgain = new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                Dialogs.disDialog();
                                getLocationdetails();
                            }
                        };
                        if (error instanceof TimeoutError) {
                            Dialogs.disDialog();
                            Alerts.timeoutErrorAlert(NavDrawerActivity.this, onClickTryAgain);
                        } else if (error instanceof NoConnectionError) {
                            Dialogs.disDialog();
                            Alerts.internetConnectionErrorAlert(NavDrawerActivity.this, onClickTryAgain);
                        }
                    }
                })

        {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Content-Type", "application/x-www-form-urlencoded");
                return params;
            }
        };
        RequestQueue requestQueue = Volley.newRequestQueue(this);
        requestQueue.add(stringRequest);

    }

    private void getResponse(String response) {
        if (!response.equals(null)) {

            if (!arrayLocation.isEmpty()) {
                arrayLocation.clear();
            }
            if (!location_strings.isEmpty()) {
                location_strings.clear();
            }
        //    Log.e("log", "" + response);
            Gson gson = new Gson();
            location_model = gson.fromJson(response, Location_model.class);
            int status = location_model.getStatus();
            String msg = location_model.getMessage();
            if (msg.equalsIgnoreCase("success") && status == 200) {
                for (int i = 0; i < location_model.getData().size(); i++) {
                    try {
                        Location_model.DataBean dataBean = location_model.getData().get(i);
                        arrayLocation.add(dataBean);
                        location_strings.add(dataBean.getDistrict_name());
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }

            }
        }
    }

    public void getUserdetails() {
        // Dialogs.showProDialog(NavDrawerActivity.this, "Loading");
        StringRequest stringRequest = new StringRequest(Request.Method.POST, Apis.profile,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                       // Log.e("log", "" + response);
                        try {
                            getDetails(response);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        Dialogs.disDialog();
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Dialogs.disDialog();
                        DialogInterface.OnClickListener onClickTryAgain = new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                Dialogs.disDialog();
                                getUserdetails();
                            }
                        };
                        if (error instanceof TimeoutError) {
                            Dialogs.disDialog();
                            Alerts.timeoutErrorAlert(NavDrawerActivity.this, onClickTryAgain);
                        } else if (error instanceof NoConnectionError) {
                            Dialogs.disDialog();
                            Alerts.internetConnectionErrorAlert(NavDrawerActivity.this, onClickTryAgain);
                        }
                    }
                }) {

            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> map = new HashMap<String, String>();
                map.put("user", SaveSharedPreference.getUserID(NavDrawerActivity.this));
                map.put("token", SaveSharedPreference.getPrefToken(NavDrawerActivity.this));
               // Log.e("userdetailmap", "" + map);
                return map;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Content-Type", "application/x-www-form-urlencoded");
                return params;
            }
        };
        RequestQueue requestQueue = Volley.newRequestQueue(this);
        requestQueue.add(stringRequest);
    }

    private void getDetails(String response) throws JSONException {
        JSONObject jsonObject = new JSONObject(response);
        // Dialogs.disDialog();
        if (jsonObject.getString("message").equals("success")) {
            Log.e("log", "" + response);
            SaveSharedPreference.setUserName(NavDrawerActivity.this, jsonObject.getJSONObject("data").getString("username"));
            SaveSharedPreference.setFirstName(NavDrawerActivity.this, jsonObject.getJSONObject("data").getString("first_name"));
            SaveSharedPreference.setLastName(NavDrawerActivity.this, jsonObject.getJSONObject("data").getString("last_name"));
            SaveSharedPreference.setUserEMAIL(NavDrawerActivity.this, jsonObject.getJSONObject("data").getString("email"));
            SaveSharedPreference.setUserID(NavDrawerActivity.this, jsonObject.getJSONObject("data").getString("user_id"));

            SaveSharedPreference.setUserIMAGE(NavDrawerActivity.this, jsonObject.getJSONObject("data").getString("avatar"));
            SaveSharedPreference.setFollowers(NavDrawerActivity.this, jsonObject.getJSONObject("data").getJSONObject("totals").getString("total_followers"));
            SaveSharedPreference.setFollowing(NavDrawerActivity.this, jsonObject.getJSONObject("data").getJSONObject("totals").getString("total_following"));
            updateProfile(NavDrawerActivity.this);
        } else if (jsonObject.getString("message").equals("error")) {
            //Toast.makeText(NavDrawerActivity.this,jsonObject.getString("message"),Toast.LENGTH_SHORT).show();
        }
        getLocationdetails();
        enableExpandableList();
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btn_login:
                startActivity(new Intent(NavDrawerActivity.this, Login_Activity.class));

                break;
            case R.id.btn_signup:
                startActivity(new Intent(NavDrawerActivity.this, Signup_Activity.class));
                break;

            case R.id.ln_camera:
                Log.e("camera", "called");
                if (SaveSharedPreference.getUserID(NavDrawerActivity.this).equals("")) {
                    startActivity(new Intent(NavDrawerActivity.this, Login_Activity.class));
                } else if (ContextCompat.checkSelfPermission(NavDrawerActivity.this, android.Manifest.permission.CAMERA)
                        != PackageManager.PERMISSION_GRANTED) {
                    ActivityCompat.requestPermissions(NavDrawerActivity.this,
                            new String[]{android.Manifest.permission.CAMERA}, 0);
                } else {
                    cameraIntent();
                }

                break;
            case R.id.ln_gallery:
                if (SaveSharedPreference.getUserID(NavDrawerActivity.this).equals("")) {
                    startActivity(new Intent(NavDrawerActivity.this, Login_Activity.class));
                } else if (ContextCompat.checkSelfPermission(NavDrawerActivity.this, android.Manifest.permission.CAMERA)
                        != PackageManager.PERMISSION_GRANTED) {
                    ActivityCompat.requestPermissions(NavDrawerActivity.this,
                            new String[]{android.Manifest.permission.CAMERA},
                            0);
                } else {
                    galleryIntent();
                }

                break;
            case R.id.ln_audio:
                if (SaveSharedPreference.getUserID(NavDrawerActivity.this).equals("")) {
                    startActivity(new Intent(NavDrawerActivity.this, Login_Activity.class));
                } else if (ContextCompat.checkSelfPermission(NavDrawerActivity.this, android.Manifest.permission.RECORD_AUDIO) != PackageManager.PERMISSION_GRANTED) {
                    ActivityCompat.requestPermissions(NavDrawerActivity.this,
                            new String[]{android.Manifest.permission.RECORD_AUDIO},
                            REQUEST_MICROPHONE);
                } else {
                    recordAudio();
                }
                break;
            case R.id.ln_video:
                if (SaveSharedPreference.getUserID(NavDrawerActivity.this).equals("")) {
                    startActivity(new Intent(NavDrawerActivity.this, Login_Activity.class));
                } else if (ContextCompat.checkSelfPermission(NavDrawerActivity.this, android.Manifest.permission.CAMERA)
                        != PackageManager.PERMISSION_GRANTED) {
                    ActivityCompat.requestPermissions(NavDrawerActivity.this,
                            new String[]{android.Manifest.permission.CAMERA}, 0);
                } else {
                    createVideo();
                }
                break;
            case R.id.ln_edit:
                if (SaveSharedPreference.getUserID(NavDrawerActivity.this).equals("")) {
                    startActivity(new Intent(NavDrawerActivity.this, Login_Activity.class));
                } else {
                    updateDisplay(new CreateFragment());
                }
                break;

            case R.id.ln_shareaome:
                if (SaveSharedPreference.getUserID(NavDrawerActivity.this).equals("")) {
                    startActivity(new Intent(NavDrawerActivity.this, Login_Activity.class));
                } else {
                    //startActivity(new Intent(NavDrawerActivity.this, Createf.class));
                   //  updateDisplay(new CreateFragment());
                    SuccessDialogFragment.state=false;
                    NavDrawerActivity.this.getSupportFragmentManager()
                            .beginTransaction().add(R.id.frame_container, new CreateFragment(),"createfrag")
                            .addToBackStack("createfrag")
                            .commit();
                }
                break;

            case R.id.ln_upload:
                if (SaveSharedPreference.getUserID(NavDrawerActivity.this).equals("")) {
                    startActivity(new Intent(NavDrawerActivity.this, Login_Activity.class));
                } else {
                    SuccessDialogFragment.state=true;
                    startActivity(new Intent(NavDrawerActivity.this, CameraDemoActivity.class));
                    //updateDisplay(new CreateFragment());
                }
                break;
            default:
                break;
        }
    }


  /*  public  void updateDisplay(Fragment fragment) {
        frame_splash1.setVisibility(View.GONE);
        frameLayout.setVisibility(View.VISIBLE);
        Fragment ldf = new CreateFragment();
        Bundle args = new Bundle();
        args.putString("splash","true");
        ldf.setArguments(args);
        getSupportFragmentManager().beginTransaction().add(R.id.container, ldf).addToBackStack(null).commit();
    }*/

    public void createVideo() {
        Intent intent = new Intent(MediaStore.ACTION_VIDEO_CAPTURE);
        File dir = CommonDirectories.getCommonPicturesDir();
        fileName = "VID_" + TimeUtilities.getTimeStampInString() + ".mp4";

        File videoDir = new File(dir.getAbsolutePath() + "/" + VIDEO_DIRECTORY_NAME);
        if (!videoDir.exists()) videoDir.mkdirs();

        videoFile = new File(videoDir, fileName);
        intent.putExtra(MediaStore.EXTRA_VIDEO_QUALITY, 1);
        intent.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(videoFile));
        startActivityForResult(intent, REQUEST_VIDEO_CAPTURE);
        return;
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (data == null) {
            data = new Intent();
        }
        super.onActivityResult(requestCode, resultCode, data);
        switch (requestCode) {
            case REQUEST_VIDEO_CAPTURE:
                if (resultCode == RESULT_OK) {
                    if (videoFile == null || !videoFile.exists()) {
                        Log.i(getClass().getSimpleName(), "videoFile is null ");
                        return;
                    }
                    callPostFragment(MEDIA_TYPE_VIDEO, videoFile.getAbsolutePath(), videoFile.getName());
                    Log.i(getClass().getSimpleName(), "File path: " + videoFile.getAbsolutePath());
                }
                break;
            case REQUEST_AUDIO_CAPTURE:
                if (resultCode == RESULT_OK) {
                    if (data != null)
                        getRecordData(data);
                }
                break;
            case SELECT_FILE:
                if (resultCode == RESULT_OK) {
                    if (data != null) {
                        Date date = new Date();
                        //  uid_image_path = rename(path(data), DIRECTORY_NAME + "-" + date.getTime() + ".jpg");
                        File mFileTemp = new File(Environment.getExternalStorageDirectory(), date.getTime() + ".jpg");
                        InputStream inputStream = null; // Got the bitmap .. Copy it to the temp file for cropping
                        try {
                            inputStream = getContentResolver().openInputStream(data.getData());
                            FileOutputStream fileOutputStream = new FileOutputStream(mFileTemp);
                            copyStream(inputStream, fileOutputStream);
                            fileOutputStream.close();
                            fileOutputStream.flush();
                            inputStream.close();
                            uid_image_path = mFileTemp.getPath();
                            callPostFragment(MEDIA_TYPE_IMAGE, uid_image_path, DIRECTORY_NAME + "-" + date.getTime() + ".jpg");
                            GALLERY_MODE = true;
                        } catch (FileNotFoundException e) {
                            e.printStackTrace();
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }
                }
                break;
            case REQUEST_CAMERA:
                // if (data != null)
                onCaptureImageResult();
                break;
            case PICK_IMAGE_MULTIPLE:
                if (data!=null){
                    String[] filePathColumn = {MediaStore.Images.Media.DATA};
                    Date date = new Date();
                    ArrayList<MediaDataModel> arrayList = new ArrayList<>();
                    if (data.getClipData() != null) {
                        ArrayList<Uri> mArrayUri = new ArrayList<Uri>();
                        arrayList = new ArrayList<>();
                        for (int i = 0; i < data.getClipData().getItemCount(); i++) {
                            ClipData.Item item = data.getClipData().getItemAt(i);
                            Uri uri = item.getUri();
                            mArrayUri.add(uri);
                            if (uri != null) {
                                String s = i + "";
                                if (mArrayUri.get(i).toString().length() > 4) {
                                    s = mArrayUri.get(i).toString().substring(mArrayUri.get(i).toString().length() - 3, mArrayUri.get(i).toString().length());
                                } else {
                                    s = i + "";
                                }
                                File mFileTemp = new File(Environment.getExternalStorageDirectory(), s + "" + i + date.getTime() + ".jpg");
                                InputStream inputStream = null; // Got the bitmap .. Copy it to the temp file for cropping
                                try {
                                    inputStream = getContentResolver().openInputStream(mArrayUri.get(i));
                                    FileOutputStream fileOutputStream = new FileOutputStream(mFileTemp);
                                    copyStream(inputStream, fileOutputStream);
                                    fileOutputStream.close();
                                    fileOutputStream.flush();
                                    inputStream.close();
                                    uid_image_path = mFileTemp.getPath();
                                    arrayList.add(new MediaDataModel(uid_image_path, mFileTemp.getName()));
                                    GALLERY_MODE = true;
                                } catch (FileNotFoundException e) {
                                    e.printStackTrace();
                                } catch (IOException e) {
                                    e.printStackTrace();
                                }
                            }
                            // Get the cursor
                          /*  Cursor cursor = getContentResolver().query(uri, filePathColumn, null, null, null);
                            // Move to first row
                            cursor.moveToFirst();

                            int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
                            imageEncoded  = cursor.getString(columnIndex);
                            cursor.close();*/

                        }
                    } else if (data.getData() != null) {
                        File mFileTemp = new File(Environment.getExternalStorageDirectory(), date.getTime() + ".jpg");
                        InputStream inputStream = null; // Got the bitmap .. Copy it to the temp file for cropping
                        try {
                            inputStream = getContentResolver().openInputStream(data.getData());
                            FileOutputStream fileOutputStream = new FileOutputStream(mFileTemp);
                            copyStream(inputStream, fileOutputStream);
                            fileOutputStream.close();
                            fileOutputStream.flush();
                            inputStream.close();
                            uid_image_path = mFileTemp.getPath();
                            arrayList.add(new MediaDataModel(uid_image_path, DIRECTORY_NAME + "-" + date.getTime() + ".jpg"));
                            GALLERY_MODE = true;
                        } catch (FileNotFoundException e) {
                            e.printStackTrace();
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }
                    sendImageMultiple(arrayList);
                }
                break;
            default:
                break;
        }
    }

    private void sendDAtaMultiple(ArrayList<HashMap<String, String>> arrayList) {
        Fragment ldf = new CreateFragment();
        Bundle args = new Bundle();
        args.putSerializable("arraylist", arrayList);
        ldf.setArguments(args);
        getSupportFragmentManager().beginTransaction().add(R.id.frame_container, ldf)
                .addToBackStack(null).commit();
    }

    private void sendImageMultiple(ArrayList<MediaDataModel> arrayList) {
        Fragment ldf = new CreateFragment();
        Bundle args = new Bundle();
        args.putSerializable("imageList", arrayList);
        ldf.setArguments(args);
        getSupportFragmentManager().beginTransaction().add(R.id.frame_container, ldf).addToBackStack(null).commit();
    }


    private void callPostFragment(String mediaType, String imgPath, String fileName) {
        //Put the value
        Log.e("imgPath",imgPath+" "+fileName);
        Fragment ldf = new CreateFragment();
        Bundle args = new Bundle();
        args.putString("splash", "false");
        args.putString(MEDIA_TYPE, mediaType);
        args.putString(MEDIA_FILE_PATH, imgPath);
        args.putString(MEDIA_FILE_NAME, fileName);
        ldf.setArguments(args);
        getSupportFragmentManager().beginTransaction().add(R.id.frame_container, ldf).addToBackStack(null).commit();
    }

    private void recordAudio() {
        Intent intent = new Intent(NavDrawerActivity.this, SoundRecordingActivity.class);
        startActivityForResult(intent, REQUEST_AUDIO_CAPTURE);
    }

    private void getRecordData(Intent data) {
        String MEDIA_TYPE_ = data.getStringExtra(MEDIA_TYPE);
        String MEDIA_FILE_PATH_ = data.getStringExtra(MEDIA_FILE_PATH);
        String KEY_AUDIO_FILE_NAME_ = data.getStringExtra(KEY_AUDIO_FILE_NAME);

        callPostFragment(MEDIA_TYPE_, MEDIA_FILE_PATH_, KEY_AUDIO_FILE_NAME_);
    }

    private void cameraIntent() {
        Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);

        if (takePictureIntent.resolveActivity(getPackageManager()) != null) {
            // Create the File where the photo should go

            try {
                photoFile = createImageFile();
                Log.d("PHOTO FILE", photoFile.getAbsolutePath());
            } catch (IOException ex) {
                // Error occurred while creating the File
            }
            // Continue only if the File was successfully created
            if (photoFile != null) {
                takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(photoFile));
                startActivityForResult(takePictureIntent, REQUEST_CAMERA);
            }
        }
    }

    private File createImageFile() throws IOException {

        Date date = new Date();
        // Create an image file name
        String extStorageDirectory = Environment.getExternalStoragePublicDirectory(
                Environment.DIRECTORY_PICTURES).toString();
        File directory = new File(extStorageDirectory + "/" + DIRECTORY_NAME);
        Log.d("TAG", "createImageFile: " + directory + "");
        if (!directory.exists()) {
            directory.mkdir();
        }
        Filename = DIRECTORY_NAME + "-" + date.getTime();

        File image = File.createTempFile(
                Filename,    /* prefix */
                ".jpg",         /* suffix */
                directory      /* directory */
        );

        // Save a file: path for use with ACTION_VIEW intents
        mCurrentPhotoPath = image.getAbsolutePath();
        Log.d("TAG", "createImageFile: " + mCurrentPhotoPath);
        return image;
    }

   /* private void galleryIntent() {
        Intent intent = new Intent();
        //intent.setAction(Intent.ACTION_GET_CONTENT);//
        intent.putExtra(Intent.EXTRA_ALLOW_MULTIPLE, true);
        intent.setAction(Intent.ACTION_GET_CONTENT);
        startActivityForResult(Intent.createChooser(intent, "Select File"), SELECT_FILE);
    }*/

    private void galleryIntent() {
       /* Intent intent = new Intent();
        intent.setType("image*//*");
        intent.putExtra(Intent.EXTRA_ALLOW_MULTIPLE, true);
        intent.setAction(Intent.ACTION_GET_CONTENT);*/
        Intent intent = new Intent(Intent.ACTION_GET_CONTENT).setType("image/*");
        intent.putExtra(Intent.EXTRA_ALLOW_MULTIPLE,true);
        startActivityForResult(Intent.createChooser(intent, "Select File"), PICK_IMAGE_MULTIPLE);
    }

    public Boolean getDeviceName() {
        try {
            String manufacturer = Build.MANUFACTURER;
            String model = Build.MODEL;
            if (manufacturer.contains("Redmi")) {
                return true;
            } else {
                return false;
            }
        } catch (Exception e) {
            return false;
        }
    }




    /*  try {
        Uri selectedMediaUri = data.getData();

        String[] columns = {MediaStore.Images.Media.DATA,
                MediaStore.Images.Media.MIME_TYPE};

        Cursor cursor = NavDrawerActivity.this.getContentResolver().query(selectedMediaUri, columns, null, null, null);
        cursor.moveToFirst();

        int pathColumnIndex = cursor.getColumnIndex(columns[0]);
        int mimeTypeColumnIndex = cursor.getColumnIndex(columns[1]);

        contentPath = cursor.getString(pathColumnIndex);
        mimeType = cursor.getString(mimeTypeColumnIndex);
        cursor.close();

    } catch (Exception e) {

    }
   */

    public Uri getImageContentUri(File imageFile) {
        String filePath = imageFile.getAbsolutePath();
        Cursor cursor = getContentResolver().query(
                MediaStore.Images.Media.EXTERNAL_CONTENT_URI,
                new String[]{MediaStore.Images.Media._ID},
                MediaStore.Images.Media.DATA + "=? ",
                new String[]{filePath}, null);


        if (cursor != null && cursor.moveToFirst()) {
            int id = cursor.getInt(cursor
                    .getColumnIndex(MediaStore.MediaColumns._ID));
            Uri baseUri = Uri.parse("content://media/external/images/media");
            return Uri.withAppendedPath(baseUri, "" + id);

        } else {
            if (imageFile.exists()) {
                ContentValues values = new ContentValues();
                values.put(MediaStore.Images.Media.DATA, filePath);

                return getContentResolver().insert(
                        MediaStore.Images.Media.EXTERNAL_CONTENT_URI, values);
            } else {
                return null;
            }
        }
    }


    public Uri getImageContentUri_next(File imageFile) {
        String filePath = imageFile.getAbsolutePath();
        Cursor cursor = getContentResolver().query(
                MediaStore.Images.Media.EXTERNAL_CONTENT_URI,
                new String[]{MediaStore.Images.Media._ID},
                MediaStore.Images.Media.DATA + "=? ",
                new String[]{filePath}, null);

        if (imageFile.exists()) {
            ContentValues values = new ContentValues();
            values.put(MediaStore.Images.Media.DATA, filePath);

            return getContentResolver().insert(
                    MediaStore.Images.Media.EXTERNAL_CONTENT_URI, values);
        } else {
            return null;
        }
    }

    private Uri try_Redmi(Uri uri) {
        File myimageFile = new File(path_fromuri(uri));
        Uri content_uri = getImageContentUri(myimageFile);
        return content_uri;
    }

    public String getMimeType(Uri uri) {
        String type = "";
        try {
            ContentResolver cR = getContentResolver();
            MimeTypeMap mime = MimeTypeMap.getSingleton();
            type = cR.getType(uri);
        } catch (Exception e) {
            Log.e("exception", "exp" + e);
        }
        return type;
    }

    private void onSelectFromGalleryResult(Intent data) {
        // handle image
        mimeType = getMimeType(data.getData());
        if(mimeType==null){
            Log.e("entered image", "image here");

        }
        Log.e("mime", "ju" + mimeType);
          if (data.getData()!=null) {
              try {
                  if (mimeType.contains("image") || mimeType.contains("jpeg")|| mimeType.contains("jpg")|| mimeType.contains("png")) {
                      //It's an image
                      Log.e("entered image", "image here");
                      Date date = new Date();
                    //  uid_image_path = rename(path(data), DIRECTORY_NAME + "-" + date.getTime() + ".jpg");
                      File mFileTemp = new File(Environment.getExternalStorageDirectory(),  date.getTime() + ".jpg");
                      InputStream inputStream = getContentResolver().openInputStream(data.getData()); // Got the bitmap .. Copy it to the temp file for cropping
                      FileOutputStream fileOutputStream = new FileOutputStream(mFileTemp);
                      copyStream(inputStream, fileOutputStream);
                      fileOutputStream.close();
                      fileOutputStream.flush();
                      inputStream.close();
                      uid_image_path = mFileTemp.getPath();
                      callPostFragment(MEDIA_TYPE_IMAGE, uid_image_path, DIRECTORY_NAME + "-" + date.getTime() + ".jpg");
                      GALLERY_MODE = true;
                  } else if (mimeType.contains("video")) {

                      Date date = new Date();
                      video_path = rename(path(data), DIRECTORY_NAME + "-" + date.getTime() + ".mp4");
                      Log.d(getClass().getSimpleName(), "onSelectFromGalleryResult: ");

                      callPostFragment(MEDIA_TYPE_VIDEO, video_path, new File(video_path).getName());

                      video_flag = 0;
                      GALLERY_MODE = true;
                  } else if (mimeType.contains("audio")) {
                      Log.d(getClass().getSimpleName(), "onSelectFromGalleryResult: audio");

                      String audio_path = rename(path(data), AUDIO_DIRECTORY_NAME + "-" + CommonFunctions.getTimeStampInString() + ".mp3");

                      callPostFragment(MEDIA_TYPE_AUDIO, audio_path, AUDIO_DIRECTORY_NAME + "-" + CommonFunctions.getTimeStampInString() + ".mp3");

                      GALLERY_MODE = true;
                  }
              } catch (Exception e) {

              }
          }else {
              if (data.getClipData() != null) {
                  ClipData mClipData = data.getClipData();
                  if (!videopatharray.isEmpty()){
                      videopatharray.clear();
                  }
                  for (int i = 0; i < mClipData.getItemCount(); i++) {

                      ClipData.Item item = mClipData.getItemAt(i);
                      Uri uri = item.getUri();

                      Date date = new Date();

                      String[] columns = { MediaStore.Images.Media.DATA,
                              MediaStore.Images.Media.MIME_TYPE };

                      Cursor cursor = getContentResolver().query(uri, columns, null, null, null);
                      if(cursor!=null){
                          cursor.moveToFirst();

                          int pathColumnIndex  = cursor.getColumnIndex( columns[0] );
                          int mimeTypeColumnIndex = cursor.getColumnIndex( columns[1] );

                          String contentPath = cursor.getString(pathColumnIndex);
                          String mimeType    = cursor.getString(mimeTypeColumnIndex);
                          cursor.close();
                      }

                      HashMap hashMap;

                      if(mimeType.startsWith("image"))
                      {
                         /* String uris= RealPathUtil.getRealPathFromURI_API19(NavDrawerActivity.this,uri);
                          uid_image_path=   rename(uris,DIRECTORY_NAME+"-"+date.getTime()+".jpg");
                          uid_image_path = rename(path(data), DIRECTORY_NAME + "-" + date.getTime() + ".jpg");*/
                          try {
                              File mFileTemp = new File(Environment.getExternalStorageDirectory(),  date.getTime() + ".jpg");
                              InputStream inputStream = getContentResolver().openInputStream(data.getData()); // Got the bitmap .. Copy it to the temp file for cropping
                              FileOutputStream fileOutputStream = new FileOutputStream(mFileTemp);
                              copyStream(inputStream, fileOutputStream);
                              fileOutputStream.close();
                              fileOutputStream.flush();
                              inputStream.close();
                              uid_image_path = mFileTemp.getPath();
                              Log.e("uidimagepath",""+uid_image_path);
                          } catch (IOException e) {
                              e.printStackTrace();
                          }

                          hashMap=new HashMap();
                          hashMap.put("image",uid_image_path);
                          videopatharray.add(hashMap);
                          GALLERY_MODE=true;
                      }
                      /*else if(mimeType.startsWith("video")) {
                          String uris= RealPathUtil.getRealPathVideoURI_API19(NavDrawerActivity.this,uri);

                          video_path =  rename(uris,DIRECTORY_NAME+"-"+date.getTime()+".mp4");
                          Log.e("videopath",""+video_path);
                          hashMap=new HashMap();
                          hashMap.put("video",video_path);
                          videopatharray.add(hashMap);

                          video_flag = 0;
                          GALLERY_MODE=true;
                      } else if(mimeType.startsWith("audio")) {
                          String uris= RealPathUtil.getRealPathAudioURI_API19(NavDrawerActivity.this,uri);

                          Log.d(getClass().getSimpleName(), "onSelectFromGalleryResult: audio");

                          String audio_path= rename(uris,AUDIO_DIRECTORY_NAME+"-"+ CommonFunctions.getTimeStampInString()+".mp3");
                          hashMap=new HashMap();
                          hashMap.put("audio",audio_path);
                          videopatharray.add(hashMap);

                          //callPostFragment(MEDIA_TYPE_AUDIO,audio_path,AUDIO_DIRECTORY_NAME+"-"+ CommonFunctions.getTimeStampInString()+".mp3");
                          GALLERY_MODE=true;
                      }
*/                      //uid_image_path = rename(uris, DIRECTORY_NAME + "-" + date.getTime() + ".jpg");
                      //imgList.add(new GridModel(MEDIA_TYPE_IMAGE, uid_image_path, DIRECTORY_NAME + "-" + date.getTime() + ".jpg"));

                      //setImage(uid_image_path);
                  }
                  sendDAtaMultiple(videopatharray);
              }
          }
    }

  /*   private void onSelectFromGalleryResult(Intent data) {
        //handle image
        Bitmap bm = null;
        String mimeType = null, contentPath;
        try {
            Uri selectedMediaUri = data.getData();

            String[] columns = {MediaStore.Images.Media.DATA,
                    MediaStore.Images.Media.MIME_TYPE};

            Cursor cursor = NavDrawerActivity.this.getContentResolver()
            .query(selectedMediaUri, columns, null, null, null);
            cursor.moveToFirst();

            int pathColumnIndex = cursor.getColumnIndex(columns[0]);
            int mimeTypeColumnIndex = cursor.getColumnIndex(columns[1]);

            contentPath = cursor.getString(pathColumnIndex);
            mimeType = cursor.getString(mimeTypeColumnIndex);
            cursor.close();

        } catch (Exception e) {

        }
        try {
            if (mimeType.startsWith("image")) {
                //It's an image
                if (data != null) {
                    try {
                        bm = MediaStore.Images.Media.getBitmap(getContentResolver(), data.getData());
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
                Date date = new Date();
                Log.e("path",""+data.getData());

                uid_image_path = rename(path(data), DIRECTORY_NAME + "-" + date.getTime() + ".jpg");

                //send this String path to another Fragement using bundle
                callPostFragment(MEDIA_TYPE_IMAGE, uid_image_path, DIRECTORY_NAME + "-" + date.getTime() + ".jpg");
                GALLERY_MODE = true;
            } else if (mimeType.startsWith("video")) {
                //It's a video Date date=new Date();
                Date date = new Date();
                video_path = rename(path(data), DIRECTORY_NAME + "-" + date.getTime() + ".mp4");
                Log.d(getClass().getSimpleName(), "onSelectFromGalleryResult: ");
                // video_path = commonFunctions.getRealPathFromURI(NavDrawerActivity.this, data.getData());
                Log.d("video_path", "onActivityResult: " + new File(video_path).getName());
                callPostFragment(MEDIA_TYPE_VIDEO, video_path, new File(video_path).getName());
                //  Toast.makeText(NavDrawerActivity.this, "Video saved to: " +video_path, Toast.LENGTH_LONG).show();
                video_flag = 0;
                GALLERY_MODE = true;
            } else if (mimeType.startsWith("audio")) {
                Log.d(getClass().getSimpleName(), "onSelectFromGalleryResult: audio");
                String audio_path = rename(path(data), AUDIO_DIRECTORY_NAME + "-" + CommonFunctions.getTimeStampInString() + ".mp3");
                callPostFragment(MEDIA_TYPE_AUDIO, audio_path, AUDIO_DIRECTORY_NAME + "-" + CommonFunctions.getTimeStampInString() + ".mp3");
                GALLERY_MODE = true;
            }
        }catch (Exception e){

        }
    }
*/

    private static void copyStream(InputStream input, OutputStream output) throws IOException {
      byte[] buffer = new byte[1024];
      int bytesRead;
      while ((bytesRead = input.read(buffer)) != -1) {
          output.write(buffer, 0, bytesRead);
      }
  }

    public String rename(String path, String name) {
        try {
            File dir = new File(path);
            File from = new File(dir.getParent(), dir.getName());
            String extStorageDirectory = Environment.getExternalStorageDirectory().toString();
            File directory = new File(extStorageDirectory + "/" + DIRECTORY_NAME);
            if (!directory.exists()) {
                directory.mkdir();
            }
            File to = new File(directory, name);
            InputStream in = new FileInputStream(from);
            OutputStream out = new FileOutputStream(to);

            // Transfer bytes from in to out
            byte[] buf = new byte[1024];
            int len;
            while ((len = in.read(buf)) > 0) {
                out.write(buf, 0, len);
            }
            in.close();
            out.close();

            return to.getAbsolutePath();
        } catch (Exception excep) {
            excep.printStackTrace();
            return "NOO" + excep;
        }
    }

    public String path_fromuri(Uri uri) {
        String realPath = null;
        try {
            realPath = FilePath.getPath(NavDrawerActivity.this, uri);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return realPath;
    }

    public String path(Intent data) {
        String realPath = null;
        try {
            realPath = FilePath.getPath(NavDrawerActivity.this, data.getData());
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return realPath;
    }

    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        if (requestCode == 0) {
            try {
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED
                        && grantResults[1] == PackageManager.PERMISSION_GRANTED) {
                }
            } catch (Exception e) {

            }
        }
    }

    private void onCaptureImageResult() {
        try{
            if (photoFile.getAbsolutePath()!=null){
                notifyMediaStoreScanner(photoFile.getAbsolutePath());
                GALLERY_MODE=false;
                callPostFragment(MEDIA_TYPE_IMAGE, photoFile.getAbsolutePath(), photoFile.getName());
            }
        }catch (Exception e){

        }
    }

    public final void notifyMediaStoreScanner(final String fileUri) {
        // Convert to file Object
        File file = new File(fileUri);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            // Write Kitkat version specific code for add entry to gallery database
            // Check for file existence
            if (file.exists()) {
                // Add / Move File
                Intent mediaScanIntent = new Intent(
                        Intent.ACTION_MEDIA_SCANNER_SCAN_FILE);
                Uri contentUri = Uri.fromFile(new File(fileUri));
                mediaScanIntent.setData(contentUri);
                sendBroadcast(mediaScanIntent);
            } else {
                // Delete File
                try {
                    getContentResolver().delete(
                            MediaStore.Images.Media.EXTERNAL_CONTENT_URI,
                            MediaStore.Images.Media.DATA + "='"
                                    + new File(fileUri).getPath() + "'", null);
                } catch (Exception e) {
                    e.printStackTrace();

                }
            }
        } else {
            sendBroadcast(new Intent(
                    Intent.ACTION_MEDIA_MOUNTED, Uri.parse("file://"
                    + new File(fileUri).getParent())));
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        this.unregisterReceiver(broadcastReceiver);
        isBroadCastRegister = false;
    }

    @Override
    public void respond(String type) {
        if (type.equalsIgnoreCase("bookmark")) {
            Log.e("isbookmak1", "");
            Newsdetail_fragment frag = (Newsdetail_fragment)
                    getSupportFragmentManager().findFragmentByTag("tempfrag");
            if (frag != null)
                frag.is_bookmark();
        } else {
            Bookmark_fragment frag = (Bookmark_fragment)
                    getSupportFragmentManager().findFragmentByTag("bookmark");
            if (frag != null)
                frag.getall_bookmarks();
        }
        // FragmentManager manager = getSupportFragmentManager();
    }

    @Override
    public void changelike(String change) {
        HomeTab_fragment frag = (HomeTab_fragment)
                getSupportFragmentManager().findFragmentByTag("hometab");
        if (frag != null){
            if (change.equalsIgnoreCase("news")){
                frag.fragmentname("1");
            }else if (change.equalsIgnoreCase("talents")){
                frag.fragmentname("2");
            }else if (change.equalsIgnoreCase("explore")){
                frag.fragmentname("3");
            }else if (change.equalsIgnoreCase("opinions")){
                frag.fragmentname("4");
            }

        }

    }

}
