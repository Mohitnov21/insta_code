package com.abacusdesk.instafeed.instafeed.fragments;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.abacusdesk.instafeed.instafeed.NavDrawerActivity;
import com.abacusdesk.instafeed.instafeed.R;

import static com.abacusdesk.instafeed.instafeed.Util.ScreenConstants.GALLERY_MODE;

/**
 * Created by Administrator on 6/14/2017.
 */

public class SuccessDialogFragment extends Fragment implements View.OnClickListener {
    Button btn_continue;
    TextView tv_sucess_text;
    View view;
    public static Boolean state=false;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        view=inflater.inflate(R.layout.success_dialog,container,false);
        btn_continue=(Button)view.findViewById(R.id.btn_continue);
        tv_sucess_text=(TextView)view.findViewById(R.id.tv_sucess_text);
        if(GALLERY_MODE== true){
            tv_sucess_text.setText(getString(R.string.mod_y_sucess_text));
        }else {
            tv_sucess_text.setText(getString(R.string.sucess_dialog_text));
        }
        btn_continue.setOnClickListener(this);
        view.setFocusableInTouchMode(true);
        view.requestFocus();
        view.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if (keyCode == KeyEvent.KEYCODE_BACK) {
                    if (state==false){
                        Log.e("satte","false");
                        getActivity().getSupportFragmentManager()
                                .beginTransaction()
                                .replace(R.id.frame_container, new HomeTab_fragment()).commit();
                        getActivity().getSupportFragmentManager().popBackStack();
                    }else {
                        Log.e("satte","true");
                        // getActivity().getSupportFragmentManager().beginTransaction().replace(R.id.container, new HomeTab_fragment()).commit();
                        //  getActivity().getSupportFragmentManager().popBackStack();
                        /* Intent i =new Intent(getActivity(), SplashActivity1.class);
                        i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        getActivity().startActivity(i);*/
                        getActivity().finish();
                        //getActivity().getSupportFragmentManager().popBackStack();

                    }
                    return true;
                } else {
                    return false;
                }
            }
        });
        return  view;
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.btn_continue:
               /* getActivity().getSupportFragmentManager().beginTransaction()
                        .replace(R.id.frame_container, new HomeTab_fragment()).commit();
                getActivity().getSupportFragmentManager().popBackStack();
               */
               //getActivity().getSupportFragmentManager().popBackStack();

                   Intent i =new Intent(getActivity(), NavDrawerActivity.class);
                   i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                   i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                   getActivity().startActivity(i);
                    //
                break;

            default:
                break;
        }
    }

}
