package com.abacusdesk.instafeed.instafeed.adapter;

/**
 * Created by ishaan on 6/9/2017.
 */

import android.app.Activity;
import android.content.Intent;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.abacusdesk.instafeed.instafeed.R;
import com.abacusdesk.instafeed.instafeed.Util.Apis;
import com.abacusdesk.instafeed.instafeed.fragments.Newsdetail_fragment;
import com.abacusdesk.instafeed.instafeed.model.NewsModel;
import com.squareup.picasso.Picasso;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;

public class RecyclerView_Adapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private ArrayList<NewsModel.DataBean> arrayList;
    private Activity activity;
    private final int NORMAL_ROW = 0, FIRST_ROW = 1;
    FragmentManager parentFragment;

    public RecyclerView_Adapter(Activity activity, ArrayList<NewsModel.DataBean> arrayList) {
        this.activity = activity;
        this.arrayList = arrayList;
        Log.e("arraylist size", "" + arrayList.size());
    }

    @Override
    public int getItemViewType(int position) {
        if (position==0) {
            return FIRST_ROW;
        } else {
            return NORMAL_ROW;
        }
    }


    @Override
    public int getItemCount(){
        return (null != arrayList ? arrayList.size() : 0);
    }

    @Override
    public void onBindViewHolder(final RecyclerView.ViewHolder holder, int position) {
            String myFormat = "yyyy-MM-dd HH:mm:ss";
            DateFormat sdformat = new SimpleDateFormat(myFormat);
            DateFormat targetFormat = new SimpleDateFormat("MMMM dd, yyyy");

          final NewsModel.DataBean dataBean = arrayList.get(position);
            if (position==0){
                ((FisrtViewHolder)holder).txtdescr.setText(dataBean.getShort_description());
                ((FisrtViewHolder)holder).title.setText(dataBean.getTitle());
                if (dataBean.getImage()!=null) {
                    Picasso.with(activity).load(dataBean.getImage()).error(R.drawable.demo_news).into(((FisrtViewHolder) holder).imgpro);

                }
                try {
                    String formattedDate = targetFormat.format(sdformat.parse(dataBean.getDt_added()));
                    ((FisrtViewHolder)holder).date.setText(formattedDate);
                } catch (ParseException e) {
                    e.printStackTrace();
                }
                ((FisrtViewHolder) holder).lnfirst.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        //Newsdetail_fragment.postid=dataBean.getId();
                        //Newsdetail_fragment.catid=dataBean.getNews_category_id();
                        AppCompatActivity activity = (AppCompatActivity) v.getContext();
                        activity.getSupportFragmentManager().beginTransaction().add(R.id.frame_container, new Newsdetail_fragment(),"tempfrag").addToBackStack(null).commit();
                    }
                });

                ((FisrtViewHolder) holder).imgshare.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent sharingIntent = new Intent(Intent.ACTION_SEND);
                        sharingIntent.setType("text/plain");
                        sharingIntent.putExtra(Intent.EXTRA_SUBJECT,dataBean.getTitle());
                        sharingIntent.putExtra(Intent.EXTRA_TEXT, Apis.news_Share+dataBean.getSlug()+" "+dataBean.getShort_description());
                        activity.startActivity(Intent.createChooser(sharingIntent, "Share via"));
                    }
                });
            } else {
                ((NormalHolder)holder).title.setText(dataBean.getTitle());
                if (dataBean.getShort_description()!=null){
                    ((NormalHolder)holder).txtshortdes.setText(dataBean.getShort_description());
                }

                if (dataBean.getIs_anonymous().equalsIgnoreCase("y")){
                    ((NormalHolder)holder).txtname.setText("Anonymous");
                }else if(dataBean.getUsername()!=null)
                {
                    ((NormalHolder)holder).txtname.setText(dataBean.getUsername());
                }

                if (dataBean.getImage()!=null){
                /* Picasso.Builder builder = new Picasso.Builder(activity);
                    builder.listener(new Picasso.Listener()
                    {
                        @Override
                        public void onImageLoadFailed(Picasso picasso, Uri uri, Exception exception)
                        {
                            exception.printStackTrace();
                        }
                    });
                 builder.build().load(dataBean.getImage()).into(((NormalHolder) holder).imgpro);*/
                    Picasso.with(activity).load(dataBean.getImage()).error(R.drawable.demo_news).into(((NormalHolder) holder).imgpro);
                  }
                try {
                    String formattedDate = targetFormat.format(sdformat.parse(dataBean.getDt_added()));
                    ((NormalHolder)holder).date.setText(formattedDate);
                } catch (ParseException e) {
                    e.printStackTrace();
                }
                 ((NormalHolder) holder).lnrow.setOnClickListener(new View.OnClickListener() {
                     @Override
                     public void onClick(View v) {
                        // Newsdetail_fragment.postid=dataBean.getId();
                        // Newsdetail_fragment.catid=dataBean.getNews_category_id();

                         AppCompatActivity activity = (AppCompatActivity) v.getContext();
                         activity.getSupportFragmentManager().beginTransaction()
                                 .add(R.id.frame_container, new Newsdetail_fragment(),"tempfrag").addToBackStack(null).commit();
                     }
                 });
                ((NormalHolder) holder).imgshare.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent sharingIntent = new Intent(Intent.ACTION_SEND);
                        sharingIntent.setType("text/plain");
                        sharingIntent.putExtra(Intent.EXTRA_SUBJECT,dataBean.getTitle());
                        sharingIntent.putExtra(Intent.EXTRA_TEXT, Apis.news_Share+dataBean.getSlug()+" "+dataBean.getShort_description());
                        activity.startActivity(Intent.createChooser(sharingIntent, "Share via"));
                    }
                });
            }
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = null;
        //Based on view type decide which type of view to supply with viewHolder
        switch (viewType) {
            case NORMAL_ROW:
                view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_rownews, parent, false);
                return new NormalHolder(view);

            case FIRST_ROW:
                view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_rowfirst, parent, false);
                return new FisrtViewHolder(view);
        }
        return null;
    }

    public static class FisrtViewHolder extends RecyclerView.ViewHolder {
        public TextView title,txtdescr;
        public ImageView imgpro, imgshare;
        public TextView date;
        public LinearLayout lnfirst;
        public FisrtViewHolder(View itemView) {
            super(itemView);
            this.lnfirst = (LinearLayout) itemView.findViewById(R.id.ln_first);
            this.title = (TextView) itemView.findViewById(R.id.txt_title);
            this.date = (TextView) itemView.findViewById(R.id.txt_date);
            this.txtdescr = (TextView) itemView.findViewById(R.id.txt_descr);
            this.imgpro = (ImageView) itemView.findViewById(R.id.img_pro);
            this.imgshare = (ImageView) itemView.findViewById(R.id.img_share);
        }
    }

    public static class NormalHolder extends RecyclerView.ViewHolder {

        public TextView title,txtshortdes,txtname;
        public ImageView imgpro, imgshare;
        public TextView date;
        public LinearLayout lnrow;

        public NormalHolder(View itemView) {
            super(itemView);
            this.lnrow = (LinearLayout) itemView.findViewById(R.id.ln_rowitem);
            this.txtshortdes = (TextView) itemView.findViewById(R.id.txt_shortdescr);
            this.txtname = (TextView) itemView.findViewById(R.id.txt_name);
            this.title = (TextView) itemView.findViewById(R.id.txt_title);
            this.date = (TextView) itemView.findViewById(R.id.txt_date);
            this.imgpro = (ImageView) itemView.findViewById(R.id.img_pro);
            this.imgshare = (ImageView) itemView.findViewById(R.id.img_share);
        }
    }
}