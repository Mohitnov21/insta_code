package com.abacusdesk.instafeed.instafeed;

import android.content.Context;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.widget.TextView;

/**
 * Created by abacusdesk on 2017-10-04.
 */

public class Content_italic extends TextView{

    public Content_italic(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        init();
    }

    public Content_italic(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public Content_italic(Context context) {
        super(context);
        init();
    }

    public void init() {
        Typeface tf = Typeface.createFromAsset(getContext().getAssets(),"fonts/DroidSerif-Italic.ttf");
        setTypeface(tf);

    }
}
