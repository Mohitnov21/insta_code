package com.abacusdesk.instafeed.instafeed.adapter;

import android.app.Activity;
import android.graphics.Bitmap;
import android.media.ThumbnailUtils;
import android.provider.MediaStore;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.abacusdesk.instafeed.instafeed.R;
import com.abacusdesk.instafeed.instafeed.Util.CommonFunctions;
import com.abacusdesk.instafeed.instafeed.model.GridModel;

import java.io.File;
import java.util.ArrayList;

import static com.abacusdesk.instafeed.instafeed.Util.ScreenConstants.MEDIA_TYPE_AUDIO;
import static com.abacusdesk.instafeed.instafeed.Util.ScreenConstants.MEDIA_TYPE_IMAGE;
import static com.abacusdesk.instafeed.instafeed.Util.ScreenConstants.MEDIA_TYPE_VIDEO;

/**
 * Created by Administrator on 6/21/2017.
 */

public class ImageRecyclerAdapter  extends RecyclerView.Adapter<ImageRecyclerAdapter.ImageRecyclerHolder> {

    //List<String> mListData;
    ArrayList<GridModel>imgList;

    public ImageRecyclerAdapter(Activity activity,ArrayList<GridModel>imgList) {
        this.imgList=imgList;
        //this.mListData = mListData;
    }

    @Override
    public ImageRecyclerAdapter.ImageRecyclerHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.img_row,
                viewGroup, false);
        return new ImageRecyclerAdapter.ImageRecyclerHolder(view);
    }

    @Override
    public void onBindViewHolder(ImageRecyclerAdapter.ImageRecyclerHolder myViewHolder, int i) {
        GridModel gridModel=imgList.get(i);

        if(gridModel.getFileType().equals(MEDIA_TYPE_IMAGE)) {
            Log.d("image Name",MEDIA_TYPE_IMAGE+",,,,,"+gridModel.getFileType());
            File file = new File(gridModel.getFilePath());

            Bitmap bitmap = CommonFunctions.extractThumbnail(file, 250, 250);
            myViewHolder.img.setImageBitmap(bitmap);
            Log.d("video Name",MEDIA_TYPE_VIDEO+",,,,,"+gridModel.getFileType());
        }else if(gridModel.getFileType().equals(MEDIA_TYPE_VIDEO)){

            Bitmap bMap = ThumbnailUtils.createVideoThumbnail(gridModel.getFilePath(), MediaStore.Video.Thumbnails.MICRO_KIND);

            myViewHolder.img.setImageBitmap(bMap);
        }else  if(gridModel.getFileType().equals(MEDIA_TYPE_AUDIO)){
            Log.d("audio Name",MEDIA_TYPE_AUDIO+",,,,,"+gridModel.getFileType());
           // audioPathList.add(gridModel.getFilePath());
            myViewHolder.img.setImageResource(R.drawable.music_player_plus);
        }

    }

    @Override
    public int getItemCount() {
        // return mListData == null ? 0 : 10;
        return (null != imgList ? imgList.size() : 0);
    }

    class ImageRecyclerHolder extends RecyclerView.ViewHolder {

    ImageView img;

        public ImageRecyclerHolder(View itemView) {
            super(itemView);
            img=(ImageView)itemView.findViewById(R.id.img_android);


        }
    }
}
