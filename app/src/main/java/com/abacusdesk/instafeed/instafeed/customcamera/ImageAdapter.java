package com.abacusdesk.instafeed.instafeed.customcamera;

/**
 * Created by abacusdesk on 2017-12-15.
 */

import android.app.Activity;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.support.v7.widget.RecyclerView;
import android.util.Base64;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.abacusdesk.instafeed.instafeed.R;

import java.util.ArrayList;
import java.util.HashMap;

public  class ImageAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private ArrayList<HashMap<String,String>> arrayList;
    private Activity activity;
    private final int NORMAL_ROW = 0;

    private String imagetemp="";
    private ArrayList<String> selectedIds = new ArrayList<>();

    public ImageAdapter(Activity activity, ArrayList<HashMap<String,String>> arrayList) {
        this.activity = activity;
        this.arrayList = arrayList;
    }

    @Override
    public int getItemViewType(int position) {

        return NORMAL_ROW;

    }

    @Override
    public int getItemCount() {
        return (null != arrayList ? arrayList.size() : 0);
    }

    public static Bitmap StringToBitMap(String encodedString){
        try {
            byte [] encodeByte= Base64.decode(encodedString, Base64.DEFAULT);
            Bitmap bitmap= BitmapFactory.decodeByteArray(encodeByte, 0, encodeByte.length);
            return bitmap;
        } catch(Exception e) {
            //e.getMessage();
            return null;
        }
    }

    @Override
    public void onBindViewHolder(final RecyclerView.ViewHolder holder, final int position) {

           if (arrayList.get(position).containsKey("image")){
               ((NormalHolder)holder).imgvideoicon.setVisibility(View.GONE);
               ((NormalHolder)holder).imgmain.setVisibility(View.VISIBLE);
               Log.e("position",""+arrayList.get(position).get("image"));

               if (!arrayList.get(position).get("image").isEmpty()
                       && arrayList.get(position).get("image")!=null){
                   ((NormalHolder)holder).imgmain.
                           setImageURI(Uri.parse(arrayList.get(position).get("image")));
               }
           }else if (arrayList.get(position).containsKey("video")){
               ((NormalHolder)holder).imgvideoicon.setVisibility(View.VISIBLE);
               ((NormalHolder)holder).imgmain.setVisibility(View.VISIBLE);

               Log.e("position","posi"+arrayList.get(position).get("video"));
               ((NormalHolder)holder).imgmain.
                       setImageURI(Uri.parse(arrayList.get(position).get("video")));

              /* ((NormalHolder)holder).imgmain
                       .setImageBitmap(StringToBitMap(arrayList.get(position).get("video")));
              */
           }

        if (selectedIds.contains(arrayList.get(position).get("id"))){
            ((NormalHolder)holder).imgshade.setVisibility(View.VISIBLE);
        }else {
            ((NormalHolder)holder).imgshade.setVisibility(View.INVISIBLE);
        }
    }

    public void setSelectedIds(ArrayList<String> selectedIds) {
        this.selectedIds = selectedIds;
        Log.e("select","notifydata");
        notifyDataSetChanged();
    }


    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = null;
        switch (viewType) {
            case NORMAL_ROW:
                view = LayoutInflater.from(parent.getContext()).inflate(R.layout.itemimage, parent, false);
                return new NormalHolder(view);

        }
        return null;
    }

    public static class NormalHolder extends RecyclerView.ViewHolder {

        public ImageView imgmain;
        public ImageView imgshade,imgvideoicon;


        public NormalHolder(View itemView) {
            super(itemView);

            this.imgmain = (ImageView) itemView.findViewById(R.id.img_item);
            this.imgshade = (ImageView) itemView.findViewById(R.id.bg_shade);
            this.imgvideoicon =(ImageView) itemView.findViewById(R.id.img_videoicon);
        }
    }


}
