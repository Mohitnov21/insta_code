package com.abacusdesk.instafeed.instafeed.model;

import java.util.List;

/**
 * Created by abacusdesk on 2017-07-14.
 */

public class Talent_allmodel {


    /**
     * status : 200
     * message : success
     * data : [{"id":"22","talent_category_id":"11","title":"testing vertical & horizontel image","slug":"test-vertical-horizontel-image-897166200","short_description":"test talents image........","location_id":"141","total_likes":null,"total_dislikes":null,"total_comments":null,"total_views":"13","total_flags":null,"dt_added":"2017-07-13 17:34:04","dt_modified":"2017-07-13 18:06:12","status":"P","user_id":"3905","first_name":"Abhishek","last_name":"Chauhan","nickname":"Abhishek","avatar":"http://instafeed.org/storage/avatar/80x80-3844825a5c347c3f039c127d02b9154d.jpg","username":"abhishek","image":"http://instafeed.org/storage/talent_mime/38a73b127d45e04f6e4f3c68d799c2be.jpg","source":"w","latitude":null,"longitude":null},{"id":"21","talent_category_id":"9","title":"testing vertical & horizontel banner","slug":"testing-vertical-horizontel-banner-482383814","short_description":"test talents image........","location_id":"141","total_likes":null,"total_dislikes":null,"total_comments":null,"total_views":"8","total_flags":null,"dt_added":"2017-07-13 17:33:04","dt_modified":"2017-07-13 18:06:27","status":"P","user_id":"3905","first_name":"Abhishek","last_name":"Chauhan","nickname":"Abhishek","avatar":"http://instafeed.org/storage/avatar/80x80-3844825a5c347c3f039c127d02b9154d.jpg","username":"abhishek","image":"http://instafeed.org/storage/talent_mime/647bf5eff979731a00dc72a15e02d15c.jpg","source":"w","latitude":null,"longitude":null},{"id":"20","talent_category_id":"10","title":"test vertical & horizontel banner","slug":"test-vertical-horizontel-banner-2111319216","short_description":"test talents image........","location_id":"141","total_likes":null,"total_dislikes":null,"total_comments":null,"total_views":"1","total_flags":null,"dt_added":"2017-07-13 17:31:15","dt_modified":"2017-07-13 18:06:40","status":"P","user_id":"3905","first_name":"Abhishek","last_name":"Chauhan","nickname":"Abhishek","avatar":"http://instafeed.org/storage/avatar/80x80-3844825a5c347c3f039c127d02b9154d.jpg","username":"abhishek","image":"http://instafeed.org/storage/talent_mime/439055437b059953238342e71bbba522.jpg","source":"w","latitude":null,"longitude":null},{"id":"19","talent_category_id":"1","title":"test vertical & horizontel image","slug":"test-vertical-horizontel-image-1013507040","short_description":"test talents image........","location_id":"141","total_likes":null,"total_dislikes":null,"total_comments":null,"total_views":"2","total_flags":null,"dt_added":"2017-07-13 17:30:55","dt_modified":"2017-07-13 17:30:55","status":"P","user_id":"3905","first_name":"Abhishek","last_name":"Chauhan","nickname":"Abhishek","avatar":"http://instafeed.org/storage/avatar/80x80-3844825a5c347c3f039c127d02b9154d.jpg","username":"abhishek","image":"http://instafeed.org/storage/talent_mime/25df3f47f47cafb280529c36fd573eeb.jpg","source":"w","latitude":null,"longitude":null},{"id":"18","talent_category_id":"4","title":"testing horizontallly","slug":"testing-horizontallly-2045597856","short_description":"test talents image........","location_id":"141","total_likes":null,"total_dislikes":null,"total_comments":null,"total_views":"2","total_flags":null,"dt_added":"2017-07-13 17:28:58","dt_modified":"2017-07-13 17:28:58","status":"P","user_id":"3905","first_name":"Abhishek","last_name":"Chauhan","nickname":"Abhishek","avatar":"http://instafeed.org/storage/avatar/80x80-3844825a5c347c3f039c127d02b9154d.jpg","username":"abhishek","image":"http://instafeed.org/storage/talent_mime/9a9356bf04a439606c29cd54ec96c6b2.jpg","source":"w","latitude":null,"longitude":null},{"id":"17","talent_category_id":"2","title":"testing horizontal","slug":"testing-horizontal-773731887","short_description":null,"location_id":"141","total_likes":null,"total_dislikes":null,"total_comments":null,"total_views":"5","total_flags":null,"dt_added":"2017-07-13 17:27:56","dt_modified":"2017-07-13 17:29:56","status":"P","user_id":"3905","first_name":"Abhishek","last_name":"Chauhan","nickname":"Abhishek","avatar":"http://instafeed.org/storage/avatar/80x80-3844825a5c347c3f039c127d02b9154d.jpg","username":"abhishek","image":"http://instafeed.org/storage/talent_mime/470d4aa3856a582f8e146be1d17d2432.jpg","source":"w","latitude":null,"longitude":null},{"id":"16","talent_category_id":"5","title":"testing vertical","slug":"testing-vertical-1050628766","short_description":"test talents image........","location_id":"141","total_likes":null,"total_dislikes":null,"total_comments":null,"total_views":"4","total_flags":null,"dt_added":"2017-07-13 17:26:55","dt_modified":"2017-07-13 17:26:55","status":"P","user_id":"3905","first_name":"Abhishek","last_name":"Chauhan","nickname":"Abhishek","avatar":"http://instafeed.org/storage/avatar/80x80-3844825a5c347c3f039c127d02b9154d.jpg","username":"abhishek","image":"http://instafeed.org/storage/talent_mime/55c123464d5e5000e8a43509eda1202c.jpg","source":"w","latitude":null,"longitude":null},{"id":"15","talent_category_id":"1","title":"test horizontal","slug":"test-horizontal-973358390","short_description":"test talents image........","location_id":"141","total_likes":null,"total_dislikes":null,"total_comments":null,"total_views":"2","total_flags":null,"dt_added":"2017-07-13 17:25:28","dt_modified":"2017-07-13 17:26:00","status":"P","user_id":"3905","first_name":"Abhishek","last_name":"Chauhan","nickname":"Abhishek","avatar":"http://instafeed.org/storage/avatar/80x80-3844825a5c347c3f039c127d02b9154d.jpg","username":"abhishek","image":"http://instafeed.org/storage/talent_mime/3440d6e521d4f9683db801bcbf55d04a.jpg","source":"w","latitude":null,"longitude":null},{"id":"14","talent_category_id":"1","title":"test vertical","slug":"test-vertical-1078486017","short_description":"test talents image........","location_id":"141","total_likes":null,"total_dislikes":null,"total_comments":null,"total_views":"4","total_flags":null,"dt_added":"2017-07-13 17:25:05","dt_modified":"2017-07-13 17:25:05","status":"P","user_id":"3905","first_name":"Abhishek","last_name":"Chauhan","nickname":"Abhishek","avatar":"http://instafeed.org/storage/avatar/80x80-3844825a5c347c3f039c127d02b9154d.jpg","username":"abhishek","image":"http://instafeed.org/storage/talent_mime/385da410b4feb87be8b6ee5c5b535361.jpg","source":"w","latitude":null,"longitude":null},{"id":"13","talent_category_id":"4","title":"talent testing","slug":"talent-testing-1296545164","short_description":"talent test succesfull talent test succesfull talent test succesfull","location_id":null,"total_likes":null,"total_dislikes":null,"total_comments":null,"total_views":"13","total_flags":null,"dt_added":"2017-07-11 17:25:39","dt_modified":"2017-07-11 17:25:39","status":"P","user_id":"3924","first_name":"jitu","last_name":"kumar","nickname":"","avatar":null,"username":"jkg","image":"http://instafeed.org/storage/talent_mime/64eac7fc8edda010fe71e7a0ad906add.jpg","source":"w","latitude":null,"longitude":null},{"id":"12","talent_category_id":"2","title":"DMC bans 2 Fortis doctors for medical negligence, gets strict with 'life savers'.","slug":"dmc-bans-2-fortis-doctors-for-medical-negligence-gets-strict-with-life-savers-406224848","short_description":null,"location_id":"187","total_likes":null,"total_dislikes":null,"total_comments":null,"total_views":"27","total_flags":null,"dt_added":"2017-07-01 13:08:11","dt_modified":"2017-07-01 13:08:11","status":"P","user_id":"3","first_name":"Mohit","last_name":"Kashyap","nickname":"Mohit","avatar":"http://instafeed.org/storage/avatar/80x80-63665cbb0dca0a4bac9913b7e01a08c8.jpg","username":"mohitk","image":"http://instafeed.org/storage/talent_mime/5.jpg6.jpg7.jpg8.jpg","source":"w","latitude":null,"longitude":null},{"id":"9","talent_category_id":"2","title":"Step2 Step Dance","slug":"step2-step-dance-1138903094","short_description":null,"location_id":"1","total_likes":null,"total_dislikes":null,"total_comments":null,"total_views":"33","total_flags":null,"dt_added":"2017-06-13 15:04:33","dt_modified":"2017-06-13 15:04:33","status":"P","user_id":"3962","first_name":"Adhesh","last_name":"Sharma","nickname":"Adhesh","avatar":"http://instafeed.org/storage/avatar/80x80-622c3f16e60cc59bdf7094e4d9b682da.jpg","username":"balajifilmsindia09","image":null,"source":"w","latitude":null,"longitude":null},{"id":"8","talent_category_id":"2","title":"Step-2 Dance: Maha Mukabla","slug":"step2-dance-maha-mukabla-848991190","short_description":null,"location_id":"1","total_likes":null,"total_dislikes":null,"total_comments":null,"total_views":"25","total_flags":null,"dt_added":"2017-06-13 15:01:22","dt_modified":"2017-06-13 15:01:22","status":"P","user_id":"3962","first_name":"Adhesh","last_name":"Sharma","nickname":"Adhesh","avatar":"http://instafeed.org/storage/avatar/80x80-622c3f16e60cc59bdf7094e4d9b682da.jpg","username":"balajifilmsindia09","image":null,"source":"w","latitude":null,"longitude":null},{"id":"3","talent_category_id":"2","title":"Watch aman srivastava performing live for Castrol... ","slug":"watch-aman-srivastava-performing-live-for-castrol-1914882768","short_description":null,"location_id":null,"total_likes":"1","total_dislikes":null,"total_comments":"1","total_views":"48","total_flags":null,"dt_added":"2017-05-26 10:54:45","dt_modified":"2017-05-26 10:54:45","status":"P","user_id":"3914","first_name":"Pragya","last_name":"Saxena","nickname":"Pragya","avatar":"http://instafeed.org/storage/avatar/80x80-70d873a096a1d84aadc2e695c4fd3170.jpg","username":"kanupragya","image":null,"source":"w","latitude":null,"longitude":null},{"id":"2","talent_category_id":"2","title":"Watch  Stand up comedian Aman Srivastava's performance ","slug":"watch-stand-up-comedian-aman-srivastavas-performance-326172181","short_description":null,"location_id":null,"total_likes":null,"total_dislikes":null,"total_comments":"0","total_views":"42","total_flags":null,"dt_added":"2017-05-26 10:49:37","dt_modified":"2017-05-26 10:49:37","status":"P","user_id":"3914","first_name":"Pragya","last_name":"Saxena","nickname":"Pragya","avatar":"http://instafeed.org/storage/avatar/80x80-70d873a096a1d84aadc2e695c4fd3170.jpg","username":"kanupragya","image":"http://instafeed.org/storage/talent_mime/5.jpg6.jpg7.jpg8.jpg","source":"w","latitude":null,"longitude":null}]
     */

    private int status;
    private String message;
    private List<DataBean> data;

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public List<DataBean> getData() {
        return data;
    }

    public void setData(List<DataBean> data) {
        this.data = data;
    }

    public static class DataBean {
        /**
         * id : 22
         * talent_category_id : 11
         * title : testing vertical & horizontel image
         * slug : test-vertical-horizontel-image-897166200
         * short_description : test talents image........
         * location_id : 141
         * total_likes : null
         * total_dislikes : null
         * total_comments : null
         * total_views : 13
         * total_flags : null
         * dt_added : 2017-07-13 17:34:04
         * dt_modified : 2017-07-13 18:06:12
         * status : P
         * user_id : 3905
         * first_name : Abhishek
         * last_name : Chauhan
         * nickname : Abhishek
         * avatar : http://instafeed.org/storage/avatar/80x80-3844825a5c347c3f039c127d02b9154d.jpg
         * username : abhishek
         * image : http://instafeed.org/storage/talent_mime/38a73b127d45e04f6e4f3c68d799c2be.jpg
         * source : w
         * latitude : null
         * longitude : null
         */

        private String id;
        private String talent_category_id;
        private String title;
        private String slug;
        private String short_description;
        private String location_id;

        private String total_likes;
        private Object total_dislikes;
        private String total_comments;
        private String total_views;
        private Object total_flags;
        private String dt_added;
        private String dt_modified;
        private String status;
        private String user_id;
        private String first_name;
        private String last_name;
        private String nickname;
        private String avatar;
        private String username;
        private String image;
        private String source;
        private Object latitude;
        private Object longitude;
        private String video_thumb;
        private String is_like;

        public String getVideo_330x210() {
            return video_330x210;
        }

        public void setVideo_330x210(String video_330x210) {
            this.video_330x210 = video_330x210;
        }

        public String getIs_like() {
            return is_like;
        }

        public void setIs_like(String is_like) {
            this.is_like = is_like;
        }

        public String getImage_330x210() {
            return image_330x210;
        }

        public void setImage_330x210(String image_330x210) {
            this.image_330x210 = image_330x210;
        }

        private String video_330x210;
        private String image_330x210;

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getTalent_category_id() {
            return talent_category_id;
        }

        public void setTalent_category_id(String talent_category_id) {
            this.talent_category_id = talent_category_id;
        }

        public String getTitle() {
            return title;
        }

        public void setTitle(String title) {
            this.title = title;
        }

        public String getSlug() {
            return slug;
        }

        public void setSlug(String slug) {
            this.slug = slug;
        }

        public String getShort_description() {
            return short_description;
        }

        public void setShort_description(String short_description) {
            this.short_description = short_description;
        }

        public String getLocation_id() {
            return location_id;
        }

        public void setLocation_id(String location_id) {
            this.location_id = location_id;
        }

        public String getTotal_likes() {
            return total_likes;
        }

        public void setTotal_likes(String total_likes) {
            this.total_likes = total_likes;
        }

        public Object getTotal_dislikes() {
            return total_dislikes;
        }

        public void setTotal_dislikes(Object total_dislikes) {
            this.total_dislikes = total_dislikes;
        }

        public String getTotal_comments() {
            return total_comments;
        }

        public void setTotal_comments(String total_comments) {
            this.total_comments = total_comments;
        }

        public String getTotal_views() {
            return total_views;
        }

        public void setTotal_views(String total_views) {
            this.total_views = total_views;
        }

        public Object getTotal_flags() {
            return total_flags;
        }

        public void setTotal_flags(Object total_flags) {
            this.total_flags = total_flags;
        }

        public String getDt_added() {
            return dt_added;
        }

        public void setDt_added(String dt_added) {
            this.dt_added = dt_added;
        }

        public String getDt_modified() {
            return dt_modified;
        }

        public void setDt_modified(String dt_modified) {
            this.dt_modified = dt_modified;
        }

        public String getStatus() {
            return status;
        }

        public void setStatus(String status) {
            this.status = status;
        }

        public String getUser_id() {
            return user_id;
        }

        public void setUser_id(String user_id) {
            this.user_id = user_id;
        }

        public String getFirst_name() {
            return first_name;
        }

        public void setFirst_name(String first_name) {
            this.first_name = first_name;
        }

        public String getLast_name() {
            return last_name;
        }

        public void setLast_name(String last_name) {
            this.last_name = last_name;
        }

        public String getNickname() {
            return nickname;
        }

        public void setNickname(String nickname) {
            this.nickname = nickname;
        }

        public String getAvatar() {
            return avatar;
        }

        public void setAvatar(String avatar) {
            this.avatar = avatar;
        }

        public String getUsername() {
            return username;
        }

        public void setUsername(String username) {
            this.username = username;
        }

        public String getImage() {
            return image;
        }

        public void setImage(String image) {
            this.image = image;
        }

        public String getSource() {
            return source;
        }

        public void setSource(String source) {
            this.source = source;
        }

        public Object getLatitude() {
            return latitude;
        }

        public void setLatitude(Object latitude) {
            this.latitude = latitude;
        }

        public Object getLongitude() {
            return longitude;
        }

        public void setLongitude(Object longitude) {
            this.longitude = longitude;
        }

        public String getVideo_thumb() {
            return video_thumb;
        }

        public void setVideo_thumb(String video_thumb) {
            this.video_thumb = video_thumb;
        }
    }
}


