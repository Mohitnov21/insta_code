package com.abacusdesk.instafeed.instafeed.adapter;

import android.content.Context;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.abacusdesk.instafeed.instafeed.NavDrawerActivity;
import com.abacusdesk.instafeed.instafeed.R;
import com.abacusdesk.instafeed.instafeed.fragments.Newsdetail_fragment;
import com.abacusdesk.instafeed.instafeed.model.Search_model;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by ishaan on 7/3/2017.
 */

public class Search_Adapter extends RecyclerView.Adapter<Search_Adapter.MyViewHolder> {

    Context context;
    //ArrayList<Search_model.DataBean> arrayList;
    ArrayList<HashMap<String,String>> arrayList;

    public Search_Adapter(Context context,ArrayList<HashMap<String,String>> arrayList){
        this.context=context;
        this.arrayList=arrayList;
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {

        public TextView txt_title,txt_descr;
        public ImageView imgpro;
        public TextView date;
        public LinearLayout lnitem;
        public CardView carditem;

        public MyViewHolder(View view) {
            super(view);
            this.txt_title = (TextView) itemView.findViewById(R.id.txt_title);
            this.txt_descr = (TextView) itemView.findViewById(R.id.txt_descr);
            this.lnitem=(LinearLayout)itemView.findViewById(R.id.ln_desc);
            this.date = (TextView) itemView.findViewById(R.id.txt_date);
            this.carditem = (CardView) itemView.findViewById(R.id.card_view);
        }
    }

    @Override
    public Search_Adapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_basiclist, parent, false);
        return new Search_Adapter.MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(Search_Adapter.MyViewHolder holder, final int position) {
        String myFormat = "yyyy-MM-dd HH:mm:ss";
        DateFormat sdformat = new SimpleDateFormat(myFormat);
        DateFormat targetFormat = new SimpleDateFormat("MMMM dd, yyyy");

        //final Search_model.DataBean dataBean = arrayList.get(position);
        holder.txt_descr.setText(arrayList.get(position).get("description"));
        holder.txt_title.setText(arrayList.get(position).get("title"));
        holder.carditem.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.e("yesclicked","clicked");
                Log.e("yesclicked","clicked"+NavDrawerActivity.type);
                if (NavDrawerActivity.type.equals("news")){

                    Newsdetail_fragment fragment = new Newsdetail_fragment();
                    Bundle bundle = new Bundle();
                    bundle.putString("catid", arrayList.get(position).get("catid"));
                    bundle.putString("postid",arrayList.get(position).get("id"));
                    fragment.setArguments(bundle);

                    AppCompatActivity activity = (AppCompatActivity) v.getContext();
                    activity.getSupportFragmentManager()
                            .beginTransaction().add(R.id.frame_container,fragment,"tempfrag")
                            .addToBackStack("tempfrag").commit();
                }else {

                }
            }
        });
        holder.lnitem.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.e("clicked",arrayList.get(position).get("title"));
            }
        });
        //Log.e("here",arrayList.get(position).get("title"));
        /*try {
            String formattedDate = targetFormat.format(sdformat.parse(dataBean.getDt_added()));
            holder.date.setText(formattedDate);
        } catch (Exception e) {
        }*/
/*
        holder.lnitem.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.e("click",""+NavDrawerActivity.type);
                if (NavDrawerActivity.type.equals("news")){
                    Newsdetail_fragment fragment = new Newsdetail_fragment();
                    Bundle bundle = new Bundle();
                    bundle.putString("catid", arrayList.get(position).get("catid"));
                    bundle.putString("postid",arrayList.get(position).get("id"));
                    fragment.setArguments(bundle);

                    AppCompatActivity activity = (AppCompatActivity) v.getContext();
                    activity.getSupportFragmentManager().beginTransaction().add(R.id.frame_container,fragment,"tempfrag").addToBackStack("tempfrag").commit();
             }
           }
        });*/
    }

    @Override
    public int getItemCount() {
        return arrayList.size();
    }
}
