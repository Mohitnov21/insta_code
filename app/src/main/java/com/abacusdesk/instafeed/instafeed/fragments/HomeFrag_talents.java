package com.abacusdesk.instafeed.instafeed.fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.abacusdesk.instafeed.instafeed.R;
import com.abacusdesk.instafeed.instafeed.Util.Apis;
import com.abacusdesk.instafeed.instafeed.Util.AppSingleton;
import com.abacusdesk.instafeed.instafeed.Util.CommonFunctions;
import com.abacusdesk.instafeed.instafeed.Util.SaveSharedPreference;
import com.abacusdesk.instafeed.instafeed.adapter.Talent_allAdapter;
import com.abacusdesk.instafeed.instafeed.custom_dialog.Dialogs;
import com.abacusdesk.instafeed.instafeed.model.Talent_allmodel;
import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;



/**
 * Created by ishaan on 7/7/2017.
 */

 public class HomeFrag_talents extends Fragment implements SwipeRefreshLayout.OnRefreshListener{

    private View view;
    Talent_allmodel talent_allmodel;
    private static RecyclerView recyclerView;
    ArrayList<Talent_allmodel.DataBean> talentdatabean=new ArrayList<>();
    Talent_allAdapter adapter;
    public static String catid="",postid="";
    private SwipeRefreshLayout swipeRefreshLayout;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_news, container, false);
        recyclerView = (RecyclerView) view.findViewById(R.id.recyclerView);
        swipeRefreshLayout = (SwipeRefreshLayout)view.findViewById(R.id.swipe_refresh_layout);
        swipeRefreshLayout.setOnRefreshListener(this);
        setRecyclerView();
        if (CommonFunctions.isConnected(getActivity())){
            getTalentList();
        } else {
           // Toast.makeText(getActivity(), "Internet not available", Toast.LENGTH_SHORT).show();
        }
        view.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if (keyCode == KeyEvent.KEYCODE_BACK) {
                    // ViewPagerAdapter.stopVideo(getActivity());
                    getActivity().getSupportFragmentManager().popBackStack();
                    return true;
                } else {
                    return false;
                }
            }
        });
        return view;
    }
    public void refresh_Adapter(){
        Log.e("change refresh","called nav");
        //adapter.notifyDataSetChanged();
        getTalentList();
    }

    @Override
    public void onRefresh() {
        swipeRefreshLayout.setRefreshing(true);
        try {
            if (CommonFunctions.isConnected(getActivity())){
                getTalentList();
            }
        } catch (Exception e) {
            //e.printStackTrace();
        }
        swipeRefreshLayout.setRefreshing(false);
    }

    public void getTalentList() {
        // Dialogs.showProDialog(getActivity(), "Loading");
        String api="";
        if (!SaveSharedPreference.getUserID(getContext()).isEmpty()){
            api=Apis.talent_all+"/"+SaveSharedPreference.getUserID(getContext());
            Log.e("userid api","called");
        }else {
            api=Apis.talent_all;
            Log.e("without userid api","called");
        }

        StringRequest stringRequest = new StringRequest(Request.Method.GET, api,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                       // Log.e("log", "" + response);
                        getResponse(response);
                        Dialogs.disDialog();
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                       // Toast.makeText(getActivity(),"weak Internet connection", Toast.LENGTH_SHORT).show();
                    }
                })
        {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Content-Type", "application/x-www-form-urlencoded");
                return params;
            }
        };
        AppSingleton.getInstance(getContext()).addToRequestQueue(stringRequest);
    }

    private void getResponse(String response )  {
        if (!talentdatabean.isEmpty()){
            talentdatabean.clear();
        }
        if(!response.equals(null)){
           // Log.e("log", "" + response);
            Gson gson = new Gson();
            talent_allmodel = gson.fromJson(response, Talent_allmodel.class);
            int status = talent_allmodel.getStatus();
            String msg = talent_allmodel.getMessage();
            if (msg.equalsIgnoreCase("success") && status==200) {
                if (talent_allmodel.getData()!=null && !talent_allmodel.getData().isEmpty()){
                    for (int i = 0; i < talent_allmodel.getData().size(); i++) {
                        try {
                            Talent_allmodel.DataBean dataBean = talent_allmodel.getData().get(i);
                            talentdatabean.add(dataBean);
                        } catch (Exception e){
                            e.printStackTrace();
                        }
                    }
                }
                adapter.notifyDataSetChanged();
            }

        }
    }

    // Setting recycler view
    private void setRecyclerView() {
        try{
            adapter= new Talent_allAdapter(getActivity(),talentdatabean);
        }  catch (Exception e){
            e.printStackTrace();
        }

        recyclerView.setHasFixedSize(true);
        LinearLayoutManager layoutManager=new LinearLayoutManager(getActivity());
        recyclerView.setLayoutManager(layoutManager);
        layoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        recyclerView.setAdapter(adapter);
    }

}

