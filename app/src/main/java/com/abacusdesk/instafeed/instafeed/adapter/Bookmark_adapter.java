package com.abacusdesk.instafeed.instafeed.adapter;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.abacusdesk.instafeed.instafeed.R;
import com.abacusdesk.instafeed.instafeed.Util.Apis;
import com.abacusdesk.instafeed.instafeed.fragments.Newsdetail_fragment;
import com.abacusdesk.instafeed.instafeed.model.NewsModel;
import com.squareup.picasso.Picasso;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;

/**
 * Created by abacusdesk on 2017-11-09.
 */

public class
Bookmark_adapter  extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

        private ArrayList<NewsModel.DataBean> arrayList;
        private Activity activity;
        private final int NORMAL_ROW = 0;

        public Bookmark_adapter(Activity activity, ArrayList<NewsModel.DataBean> arrayList) {
            this.activity = activity;
            this.arrayList = arrayList;
            Log.e("arraylist size", "" + arrayList.size());
        }

        @Override
        public int getItemViewType(int position) {

            return NORMAL_ROW;

        }

        @Override
        public int getItemCount() {
            return (null != arrayList ? arrayList.size() : 0);
        }


        @Override
        public void onBindViewHolder(final RecyclerView.ViewHolder holder, final int position) {
            String myFormat = "yyyy-MM-dd HH:mm:ss";
            DateFormat sdformat = new SimpleDateFormat(myFormat);
            DateFormat targetFormat = new SimpleDateFormat("MMMM dd, yyyy");


            final NewsModel.DataBean dataBean = arrayList.get(position);

            ((NormalHolder) holder).txtdescr.setText(dataBean.getShort_description());
            ((NormalHolder) holder).title.setText(dataBean.getTitle());

            try {
                String formattedDate = targetFormat.format(sdformat.parse(dataBean.getDt_added()));
                ((NormalHolder) holder).date.setText(formattedDate);
            } catch (ParseException e) {
                e.printStackTrace();
            }
            try{
                Picasso.with(activity).load(dataBean.getImage()).error(R.drawable.newsdefault).into(((NormalHolder) holder).imgpro);
            }catch (Exception e){
            }


            ((NormalHolder) holder).lndesc.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Newsdetail_fragment fragment = new Newsdetail_fragment();
                    Bundle bundle = new Bundle();
                    bundle.putString("catid", dataBean.getNews_category_id());
                    bundle.putString("postid", dataBean.getId());
                    fragment.setArguments(bundle);
                    AppCompatActivity activity = (AppCompatActivity) v.getContext();
                    activity.getSupportFragmentManager().beginTransaction()
                            .add(R.id.frame_container, fragment,"tempfrag").addToBackStack("tempfrag").commit();
                }
            });
        }

        @Override
        public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View view = null;
            //Based on view type decide which type of view to supply with viewHolder
            switch (viewType) {
                case NORMAL_ROW:
                    view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_bookmark, parent, false);
                    return new NormalHolder(view);

            }
            return null;
        }

        public  class NormalHolder extends RecyclerView.ViewHolder {

            public TextView title,txtdescr;
            public ImageView imgpro, imgshare;
            public TextView date;
            public LinearLayout lndesc;

            public NormalHolder(View view) {
                super(view);
                this.title = (TextView) itemView.findViewById(R.id.txt_title);
                this.date = (TextView) itemView.findViewById(R.id.txt_date);
                this.imgshare = (ImageView) itemView.findViewById(R.id.img_share);
                this.imgpro = (ImageView) itemView.findViewById(R.id.img_pro);

                this.txtdescr = (TextView) itemView.findViewById(R.id.txt_descr);
                this.lndesc = (LinearLayout) itemView.findViewById(R.id.ln_desc);
            }
        }
    }
