package com.abacusdesk.instafeed.instafeed.fragments;

import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.abacusdesk.instafeed.instafeed.NavDrawerActivity;
import com.abacusdesk.instafeed.instafeed.R;
import com.abacusdesk.instafeed.instafeed.Util.Alerts;
import com.abacusdesk.instafeed.instafeed.Util.Apis;
import com.abacusdesk.instafeed.instafeed.Util.CommonFunctions;
import com.abacusdesk.instafeed.instafeed.Util.SaveSharedPreference;
import com.abacusdesk.instafeed.instafeed.Util.SimpleDividerItemDecoration;
import com.abacusdesk.instafeed.instafeed.adapter.RecyclerCommentAdapter;
import com.abacusdesk.instafeed.instafeed.adapter.Recycler_newAdapter;
import com.abacusdesk.instafeed.instafeed.custom_dialog.Dialogs;
import com.abacusdesk.instafeed.instafeed.model.NewsModel;
import com.abacusdesk.instafeed.instafeed.model.Search_model;
import com.abacusdesk.instafeed.instafeed.model.User_model;
import com.android.volley.AuthFailureError;
import com.android.volley.NoConnectionError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import android.view.KeyEvent;

/**
 * Created by abacusdesk on 2017-08-15.talentdeta
 */
public class CommentsList_fragment extends Fragment {

    View view;
    RecyclerView recyclerView;
    RecyclerCommentAdapter adapter;
    EditText edcomment;
    LinearLayout lncomment;
    ImageView imgsend;
    static String type="",catid="";
    String Api_temp="",postid="",temp_title;
    ArrayList<HashMap<String,String>> arrayhashComent=new ArrayList<>();
    MenuItem item,menucomment,menushare,menubookmark;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Log.e("option","");
        //setHasOptionsMenu(true);
    }

   /* @Override
    public void onPrepareOptionsMenu(Menu menu) {
        menu.findItem(R.id.menu_comment).setVisible(false);
        menu.findItem(R.id.menu_share).setVisible(false);

        menucomment=menu.findItem(R.id.menu_comment);
        menushare=menu.findItem(R.id.menu_share);
        menubookmark=menu.findItem(R.id.menu_bookmark);

        Log.e("option2","");
        super.onPrepareOptionsMenu(menu);
    }*/

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {

        menu.findItem(R.id.menu_comment).setVisible(false);
        menu.findItem(R.id.menu_share).setVisible(false);

        menucomment=menu.findItem(R.id.menu_comment);
        menushare=menu.findItem(R.id.menu_share);
        menubookmark=menu.findItem(R.id.menu_bookmark);

        super.onCreateOptionsMenu(menu, inflater);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.frag_instanewsprofile, container, false);
        recyclerView = (RecyclerView) view.findViewById(R.id.recyclerView);
        lncomment=(LinearLayout)view.findViewById(R.id.ln_comment);
        getActivity().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_PAN);

        // getTargetFragment().setMenuVisibility(false);
        NavDrawerActivity.frame_bottom.setVisibility(View.GONE);
        lncomment.setVisibility(View.VISIBLE);
        Bundle bundle = this.getArguments();
        if (bundle != null) {
            if (bundle.containsKey("arrayhash")){
                arrayhashComent= (ArrayList<HashMap<String, String>>)
                        bundle.getSerializable("arrayhash");
            }
            if (bundle.containsKey("catid")){
                catid=bundle.getString("catid");
            }
             type=  bundle.getString("type");
             postid=bundle.getString("postid");
           }

           if (type.equalsIgnoreCase("news")){
             Api_temp=Apis.comment;
           }else if (type.equalsIgnoreCase("talents")){
               Api_temp=Apis.talentall_comment;
           }else if (type.equalsIgnoreCase("explorer")){
               Api_temp=Apis.exploreall_comment;
           }else if (type.equalsIgnoreCase("opinions")){
               Api_temp=Apis.opinions_comment;
           }else if (type.equalsIgnoreCase("blogs")){
               Api_temp=Apis.Blogs_comment;
           }else if (type.equalsIgnoreCase("petitions")){
               Api_temp=Apis.petition_comment;
           }
       // NavDrawerActivity.menucomment.setVisible(false);
       // NavDrawerActivity.menushare.setVisible(false);

        edcomment=(EditText)view.findViewById(R.id.ed_comment);
        imgsend=(ImageView)view.findViewById(R.id.img_send);
        imgsend.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (edcomment.getText().toString().equals("")){
                    Toast.makeText(getContext(),"Comment can't be blank", Toast.LENGTH_SHORT).show();
                } else if (SaveSharedPreference.getUserID(getContext()).equals("")){
                    Toast.makeText(getContext(),"please Login to continue!", Toast.LENGTH_SHORT).show();
                }else {
                    postComment();
                }

            }
        });

        //NavDrawerActivity.toolbar.setBackgroundResource(0);
       // NavDrawerActivity.toolbar.setBackgroundColor(getResources().getColor(R.color.white));

        temp_title=NavDrawerActivity.toolbar.getTitle().toString();
        NavDrawerActivity.toolbar.setLogo(null);
        NavDrawerActivity.toolbar.setTitle("Comments");
        NavDrawerActivity.toolbar.setTitleTextColor(Color.BLACK);

        view.setFocusableInTouchMode(true);
        view.requestFocus();
       /* view.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if (keyCode == KeyEvent.KEYCODE_BACK) {
                    // ViewPagerAdapter.stopVideo(getActivity());
                    NavDrawerActivity.toolbar.setBackgroundColor(getResources().getColor(R.color.white));
                    NavDrawerActivity.toolbar.setTitle(temp_title);
                   // menucomment.setVisible(true);
                    //menushare.setVisible(true);

                    //NavDrawerActivity.menucomment.setVisible(true);
                    //NavDrawerActivity.menushare.setVisible(true);

                    NavDrawerActivity.toggle.syncState();
                    getActivity().getSupportFragmentManager().popBackStack();

                   *//* if (getActivity().getSupportFragmentManager().findFragmentByTag("tempfrag") instanceof Newsdetail_fragment){
                        ((NavDrawerActivity)getActivity()).menucomment.setVisible(true);
                        ((NavDrawerActivity)getActivity()).menushare.setVisible(true);
                        ((NavDrawerActivity)getActivity()).menubookmark.setVisible(true);
                      menushare.setVisible(true);
                      menubookmark.setVisible(true);
                    }*//*
                    return true;
                } else {
                    return false;
                }
            }
        });*/

        if (arrayhashComent.isEmpty() && !catid.equals("")){
            getComments_list();
        }
        getComments();
        return view;
    }

    @Override
    public void onPause() {
        super.onPause();
        Log.e("backj pressed","back");
        NavDrawerActivity.toolbar.setTitle(temp_title);
        NavDrawerActivity.toolbar.setBackgroundColor(getResources().getColor(R.color.white));
        NavDrawerActivity.toggle.syncState();
    }

    private void getComments(){
        try{
            adapter=new RecyclerCommentAdapter(getContext(),arrayhashComent,type);
            recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
            recyclerView.addItemDecoration(new SimpleDividerItemDecoration(getContext()));
            recyclerView.setNestedScrollingEnabled(false);
            recyclerView.setHasFixedSize(false);
            recyclerView.setAdapter(adapter);
            adapter.notifyDataSetChanged();
        } catch (Exception e){
            e.printStackTrace();
        }
    }

    public void postComment() {
         Dialogs.showProDialog(getActivity(), "Loading");
        StringRequest stringRequest = new StringRequest(Request.Method.POST, Api_temp ,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.e("log", "" + response);
                        resComment(response);
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Dialogs.disDialog();
                        DialogInterface.OnClickListener onClickTryAgain = new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                Dialogs.disDialog();
                                postComment();
                            }
                        };
                        if (error instanceof TimeoutError) {
                            Dialogs.disDialog();
                            Alerts.timeoutErrorAlert(getContext(), onClickTryAgain);
                        } else if (error instanceof NoConnectionError) {
                            Dialogs.disDialog();
                            Alerts.internetConnectionErrorAlert(getContext(), onClickTryAgain);
                        }
                    }
                })
        {

            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> map = new HashMap<String, String>();
                map.put("user", SaveSharedPreference.getUserID(getActivity()));
                map.put("token", SaveSharedPreference.getPrefToken(getActivity()));
                 if (type.equalsIgnoreCase("blogs")){
                     map.put("post",postid);
                }else {
                     map.put("id",postid);
                 }
                map.put("comment",edcomment.getText().toString());
                Log.e("mapcomment",""+map);
                return map;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Content-Type", "application/x-www-form-urlencoded");
                return params;
            }
        };

        RequestQueue requestQueue = Volley.newRequestQueue(getActivity());
        requestQueue.add(stringRequest);
    }

    /*       hashMap.put("commentid",commentsBean.getId());
             hashMap.put("comment",commentsBean.getComment());
             hashMap.put("userid",commentsBean.getUser_id());
             hashMap.put("avatar",commentsBean.getAvatar());
             hashMap.put("username",commentsBean.getUsername());
    */
    /*{
        "status": 200,
            "message": "success",
            "data": {
                "news_id": "225",
                "user_id": "3924",
                "comment": "great",
                "status": "A"
    }
    }*/

    private void resComment(String response ) {
        try{
            JSONObject jsonObject=new JSONObject(response);
            if (jsonObject.getString("message").equalsIgnoreCase("success")){
                Dialogs.disDialog();
                HashMap hashMap=new HashMap();
                if (type.equalsIgnoreCase("news")){
                    hashMap.put("comment", jsonObject.getJSONObject("data").getString("comment"));
                    hashMap.put("userid",jsonObject.getJSONObject("data").getString("user_id"));
                    hashMap.put("username",SaveSharedPreference.getUserName(getContext()));
                    hashMap.put("avatar",SaveSharedPreference.getUserIMAGE(getContext()));
                    arrayhashComent.add(hashMap);
                    edcomment.getText().clear();
                    //Newsdetail_fragment.getnewsDetail(getContext());
                    adapter.notifyDataSetChanged();

                }else if (type.equalsIgnoreCase("explorer")){

                    hashMap.put("comment", jsonObject.getJSONObject("data").getString("comment"));
                    hashMap.put("userid",jsonObject.getJSONObject("data").getString("user_id"));
                    hashMap.put("username",SaveSharedPreference.getUserName(getContext()));
                    hashMap.put("avatar",SaveSharedPreference.getUserIMAGE(getContext()));
                    arrayhashComent.add(hashMap);
                    edcomment.getText().clear();
                    //Explore_detailfragment.getExploredetail(getContext());
                    adapter.notifyDataSetChanged();
                }else if (type.equalsIgnoreCase("talents")){
                    hashMap.put("comment", jsonObject.getJSONObject("data").getString("comment"));
                    hashMap.put("userid",jsonObject.getJSONObject("data").getString("user_id"));
                    hashMap.put("username",SaveSharedPreference.getUserName(getContext()));
                    hashMap.put("avatar",SaveSharedPreference.getUserIMAGE(getContext()));
                    arrayhashComent.add(hashMap);
                    edcomment.getText().clear();
                    //Talentdetail_fragment.getTalentdetail(getContext());
                    adapter.notifyDataSetChanged();
                }else if (type.equalsIgnoreCase("opinions")){
                    hashMap.put("comment", jsonObject.getJSONObject("data").getString("comment"));
                    hashMap.put("userid",jsonObject.getJSONObject("data").getString("user_id"));
                    hashMap.put("username",SaveSharedPreference.getUserName(getContext()));
                    hashMap.put("avatar",SaveSharedPreference.getUserIMAGE(getContext()));
                    arrayhashComent.add(hashMap);
                    edcomment.getText().clear();
                    //Opiniondetail_fragment.getOpiniondetail(getContext());
                    adapter.notifyDataSetChanged();
                }else if (type.equalsIgnoreCase("blogs")){
                    hashMap.put("comment", jsonObject.getJSONObject("data").getString("comment"));
                    hashMap.put("userid",jsonObject.getJSONObject("data").getString("user_id"));
                    hashMap.put("username",SaveSharedPreference.getUserName(getContext()));
                    hashMap.put("avatar",SaveSharedPreference.getUserIMAGE(getContext()));
                    arrayhashComent.add(hashMap);
                    edcomment.getText().clear();
                   // Blogdetail_fragment.getBlogdetail(getContext());
                    adapter.notifyDataSetChanged();
                }else if (type.equalsIgnoreCase("petitions")){
                    hashMap.put("comment", jsonObject.getJSONObject("data").getString("comment"));
                    hashMap.put("userid",jsonObject.getJSONObject("data").getString("user_id"));
                    hashMap.put("username",SaveSharedPreference.getUserName(getContext()));
                    hashMap.put("avatar",SaveSharedPreference.getUserIMAGE(getContext()));
                    arrayhashComent.add(hashMap);
                    edcomment.getText().clear();
//                    Petitiondetail_fragment.getPetitiondetail(getContext());
                    adapter.notifyDataSetChanged();
                }
                Toast.makeText(getActivity(),jsonObject.getString("message"),Toast.LENGTH_SHORT).show();
            }else {
                Dialogs.disDialog();
                Toast.makeText(getActivity(),jsonObject.getString("message"),Toast.LENGTH_SHORT).show();
            }
        }
        catch (JSONException jsonexception){
            Log.e("excep",""+jsonexception);
        }
        catch (Exception e){
            Log.e("excep",""+e);
        }
    }

    public void getComments_list() {
        Dialogs.showProDialog(getActivity(), "Loading");
        String temp2="";
        if (type.equalsIgnoreCase("news")){
            temp2= Apis.news_detail.replace("catid", catid)
                    .replace("postid", postid);
        }else if (type.equalsIgnoreCase("talents")){
            temp2= Apis.talent_detail.replace("catid", catid)
                    .replace("postid", postid);
        }else if (type.equalsIgnoreCase("explorer")){
            temp2= Apis.explore_detail.replace("catid", catid)
                    .replace("postid", postid);
        }else if (type.equalsIgnoreCase("opinions")){
            temp2= Apis.opinions_detail.replace("id", postid);
        }
        Log.e("apitempcommny",""+temp2);
        StringRequest stringRequest = new StringRequest(Request.Method.GET, temp2,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.e("log", "" + response);
                        getResponse(response);
                        Dialogs.disDialog();
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Dialogs.disDialog();
                        DialogInterface.OnClickListener onClickTryAgain = new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                Dialogs.disDialog();
                                //getnewsDetail();
                            }
                        };
                        if (error instanceof NoConnectionError) {
                            Dialogs.disDialog();
                            Alerts.internetConnectionErrorAlert(getContext(), onClickTryAgain);
                        }
                    }
                }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Content-Type", "application/x-www-form-urlencoded");
                return params;
            }
        };
        RequestQueue requestQueue = Volley.newRequestQueue(getActivity());
        requestQueue.add(stringRequest);
    }

    public void getResponse(String response) {
        //ArrayList<HashMap<String,String>> hashMapArrayList=new ArrayList<>();
        if (!response.equals(null)) {
            //Log.e("log", "" + response);
            try {
                JSONObject jsonObject =new JSONObject(response);
                if (jsonObject.getString("status").equalsIgnoreCase("200") &&
                        jsonObject.getString("message").equalsIgnoreCase("success")){
                    if (jsonObject.getJSONArray("data").length()!=0){
                        JSONArray jsonArray=jsonObject.getJSONArray("data")
                                .getJSONObject(1).getJSONArray("comments");
                        if (jsonArray.length()!=0){
                            HashMap<String,String> hashMap;
                            for (int i=0;i<jsonArray.length();i++){
                                hashMap=new HashMap<>();
                                hashMap.put("commentid",jsonArray.getJSONObject(i).getString("comment_id"));
                                hashMap.put("comment",jsonArray.getJSONObject(i).getString("comment"));
                                hashMap.put("userid",jsonArray.getJSONObject(i).getString("user_id"));
                                hashMap.put("avatar",jsonArray.getJSONObject(i).getString("avatar"));
                                hashMap.put("username",jsonArray.getJSONObject(i).getString("username"));
                                arrayhashComent.add(hashMap);
                            }
                            Log.e("arrayhasf",""+arrayhashComent);
                            adapter.notifyDataSetChanged();
                        }
                    }
                }
            } catch (JSONException e) {
                //e.printStackTrace();
            }catch(Exception e){

            }
        }
   }

  }

