package com.abacusdesk.instafeed.instafeed.adapter;

import android.content.Context;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.abacusdesk.instafeed.instafeed.R;
import com.abacusdesk.instafeed.instafeed.fragments.Opiniondetail_fragment;
import com.abacusdesk.instafeed.instafeed.fragments.Polls_detailfragment;
import com.abacusdesk.instafeed.instafeed.model.Blog_model;
import com.abacusdesk.instafeed.instafeed.model.Polls_listmodel;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;

/**
 * Created by abacusdesk on 2017-08-10.
 */

public class Poll_Adapter  extends RecyclerView.Adapter<Poll_Adapter.MyViewHolder>{

    Context context;
    ArrayList<Polls_listmodel.DataBean> arrayList;

    public Poll_Adapter(Context context, ArrayList<Polls_listmodel.DataBean> arrayList){
        this.context=context;
        this.arrayList=arrayList;
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {

        public TextView txt_title;
        public ImageView imgpro,imgshare;
        public TextView date;
        public CardView lndesc;

        public MyViewHolder(View view) {
            super(view);
            this.txt_title = (TextView) itemView.findViewById(R.id.txt_title);
            this.lndesc=(CardView)itemView.findViewById(R.id.ln_desc);
            this.date = (TextView) itemView.findViewById(R.id.txt_date);
            this.imgshare = (ImageView) itemView.findViewById(R.id.img_share);
        }
    }

    @Override
    public Poll_Adapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_pollslist, parent, false);
        return new  MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(Poll_Adapter.MyViewHolder holder, int position) {

        String myFormat = "yyyy-MM-dd HH:mm:ss";
        DateFormat sdformat = new SimpleDateFormat(myFormat);
        DateFormat targetFormat = new SimpleDateFormat("MMMM dd, yyyy");

        Log.e("bindview",""+arrayList.get(position));
        final Polls_listmodel.DataBean dataBean = arrayList.get(position);
        if (dataBean.getTitle()!=null){
            holder.txt_title.setText(""+dataBean.getTitle());
        }
        holder.lndesc.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Polls_detailfragment.id=dataBean.getId();
                AppCompatActivity activity = (AppCompatActivity) v.getContext();
                activity.getSupportFragmentManager().beginTransaction().add(R.id.frame_container, new Polls_detailfragment()).addToBackStack(null).commit();
            }
        });
        try {
            String formattedDate = targetFormat.format(sdformat.parse(dataBean.getDt_added()));
            holder.date.setText(formattedDate);
        } catch (ParseException e) {
            e.printStackTrace();
        }
    }

    @Override
    public int getItemCount() {
        return arrayList.size();
    }
}