package com.abacusdesk.instafeed.instafeed.Util;

/**
 * Created by Administrator on 6/29/2017.
 */

public class MasterTblModel {
    String masterPrimaryKey,postTitle,postDescription;

    public MasterTblModel(String masterPrimaryKey, String postTitle, String postDescription) {
        this.masterPrimaryKey = masterPrimaryKey;
        this.postTitle = postTitle;
        this.postDescription = postDescription;
    }

    public String getMasterPrimaryKey() {
        return masterPrimaryKey;
    }

    public void setMasterPrimaryKey(String masterPrimaryKey) {
        this.masterPrimaryKey = masterPrimaryKey;
    }

    public String getPostTitle() {
        return postTitle;
    }

    public void setPostTitle(String postTitle) {
        this.postTitle = postTitle;
    }

    public String getPostDescription() {
        return postDescription;
    }

    public void setPostDescription(String postDescription) {
        this.postDescription = postDescription;
    }
}

