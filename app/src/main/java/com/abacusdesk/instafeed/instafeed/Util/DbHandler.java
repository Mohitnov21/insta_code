package com.abacusdesk.instafeed.instafeed.Util;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

/**
 * Created by Administrator on 6/19/2017.
 */

public class DbHandler extends SQLiteOpenHelper {

    private static String DB = "InstaFeed_DB";
    static SQLiteDatabase sql;

    public static String TABLE_MASTER = "table_master";
    public static String TABLE_NEWS_CATEGORY = "table_newsCategory";
    public static String TABLE_EXPLORE_CATEGORY = "table_explore_category";
    public static String TABLE_TALENT_CATEGORY = "table_talent_category";

    public static String TABLE_POST = "table_post";
    public static String COL_NEWS_ID = "col_newsId";
    public static String COL_PRIMARY_KEY = "col_primary_key";
    public static String COL_TOKEN = "col_token";
    public static String COL_USERID = "col_userId";
    public static String COL_DEVICEID = "col_deviceId";
    public static String COL_MAC_ADDRESS = "col_macAddress";
    public static String COL_IS_POSTED = "col_isPosted";
    public static String COL_POST_CATEGORY = "col_postCategory";
    public static String COL_TITLE = "col_postTitle";
    public static String COL_DESCRIPTION = "col_description";
    public static String COL_FILE_TYPE = "col_fileType";
    public static String COL_FILE_NAME = "col_fileName";
    public static String COL_FILE_PATH = "col_filePath";
    public static String COL_FILE_CAPTURED_LAT = "col_fileCapturedLat";
    public static String COL_FILE_CAPTURED_LONG = "col_fileCapturedLong";
    public static String COL_FILE_CREATEDON = "col_fileCreatedOn";
    public static String COL_FILE_POSTEDON = "col_filePostedOn";
    public static String COL_FILE_POSTED_LAT = "col_filePostedLat";
    public static String COL_FILE_POSTED_LONG = "col_filePostedLong";
    public static String COL_MODE = "COL_MODE";
    public static String COL_CATEGORY_TIITLE = "col_category_title";
    public static String COL_CATEGORY_ID = "col_category_id";
    public static String COL_IS_ANONYMOUS = "col_is_anonymous";
    public static String COL_CATEGORY_TYPE = "col_category_type";

    public DbHandler(Context context) {
        super(context, DB, null, 1);
    }
    // column1 INTEGER AUTOINCREMENT,

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(" create table " + TABLE_MASTER + "(" + COL_PRIMARY_KEY + " INTEGER PRIMARY KEY AUTOINCREMENT," + COL_DEVICEID + " text," + COL_MAC_ADDRESS + " text," + COL_TOKEN + " text," + COL_USERID + " text, " + COL_IS_POSTED + " text ," + COL_POST_CATEGORY + " text, " + COL_TITLE + " text ," + COL_DESCRIPTION + " text ," + COL_FILE_CREATEDON + " text," + COL_FILE_POSTEDON + " text ," + COL_FILE_CAPTURED_LAT + " text ," + COL_FILE_CAPTURED_LONG + " text," + COL_FILE_POSTED_LAT + " text," + COL_FILE_POSTED_LONG + " text," + COL_MODE + " text," + COL_IS_ANONYMOUS + " text," + COL_CATEGORY_TYPE + " INTEGER )");
        db.execSQL(" create table " + TABLE_POST + "(" + COL_PRIMARY_KEY + " INTEGER PRIMARY KEY AUTOINCREMENT," + COL_NEWS_ID + " text," + COL_FILE_TYPE + " text," + COL_FILE_NAME + " text," + COL_FILE_PATH + " text )");
        db.execSQL(" create table " + TABLE_NEWS_CATEGORY + "(" + COL_CATEGORY_TIITLE + " text," + COL_CATEGORY_ID + " text )");
        db.execSQL(" create table " + TABLE_EXPLORE_CATEGORY + "(" + COL_CATEGORY_TIITLE + " text," + COL_CATEGORY_ID + " text )");
        db.execSQL(" create table " + TABLE_TALENT_CATEGORY + "(" + COL_CATEGORY_TIITLE + " text," + COL_CATEGORY_ID + " text )");
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

    }

    public DbHandler open() {
        sql = this.getWritableDatabase();
        return this;
    }

    public void close() {
        if (sql != null) {
            sql.close();
        }
    }

    public long insertPost(String deviceId, String prefToken, String userID, String isPosted, String newsCategoryId, String capturedLat, String capturedLong, String createdOn, String title, String description, String mod, String isAnonymous, int categoryType) {
        try {
            sql = this.getWritableDatabase();
            ContentValues cv = new ContentValues();
            cv.put(COL_DEVICEID, deviceId);
            cv.put(COL_TOKEN, prefToken);
            cv.put(COL_USERID, userID);
            cv.put(COL_IS_POSTED, isPosted);
            cv.put(COL_POST_CATEGORY, newsCategoryId);
            cv.put(COL_FILE_CAPTURED_LAT, capturedLat);
            cv.put(COL_FILE_CAPTURED_LONG, capturedLong);
            cv.put(COL_FILE_CREATEDON, createdOn);
            cv.put(COL_TITLE, title);
            cv.put(COL_DESCRIPTION, description);
            cv.put(COL_MODE, mod);
            cv.put(COL_IS_ANONYMOUS, isAnonymous);
            cv.put(COL_CATEGORY_TYPE, categoryType);
            long i = sql.insert(TABLE_MASTER, null, cv);
            Log.e("insertPost save", i + "");
            return i;

        } catch (Exception e) {
            e.printStackTrace();
            return -1;
        } finally {
            sql.close();
        }
    }

    public long insertCategory(String title, String id) {
        try {
            sql = this.getWritableDatabase();
            ContentValues cv = new ContentValues();
            cv.put(COL_CATEGORY_TIITLE, title);
            cv.put(COL_CATEGORY_ID, id);
            long i = sql.insert(TABLE_NEWS_CATEGORY, null, cv);
            Log.e("insertCategory save", i + "");
            return i;

        } catch (Exception e) {
            e.printStackTrace();
            return -1;
        } finally {
            sql.close();
        }
    }

    public long insertExploreCategory(String title, String id) {
        try {
            sql = this.getWritableDatabase();
            ContentValues cv = new ContentValues();
            cv.put(COL_CATEGORY_TIITLE, title);
            cv.put(COL_CATEGORY_ID, id);
            long i = sql.insert(TABLE_EXPLORE_CATEGORY, null, cv);
            Log.e("insertExploreCategory", i + "");
            return i;
        } catch (Exception e) {
            e.printStackTrace();
            return -1;
        } finally {
            sql.close();
        }
    }

    public long insertTalentCategory(String title, String id) {
        try {
            sql = this.getWritableDatabase();
            ContentValues cv = new ContentValues();
            cv.put(COL_CATEGORY_TIITLE, title);
            cv.put(COL_CATEGORY_ID, id);
            long i = sql.insert(TABLE_TALENT_CATEGORY, null, cv);
            Log.e("insertExploreCategory", i + "");
            return i;
        } catch (Exception e) {
            e.printStackTrace();
            return -1;
        } finally {
            sql.close();
        }
    }

    public Cursor categroryData() {
        SQLiteDatabase db = this.getReadableDatabase();
        Log.i("m in ", "m out");
        Cursor cur = db.rawQuery("SELECT * FROM " + TABLE_NEWS_CATEGORY, null);
        int cnt = cur.getCount();
        Log.i("categroryData", cnt + "");
        return cur;
    }

    public Cursor exploreCategroryData() {
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cur = db.rawQuery("SELECT * FROM " + TABLE_EXPLORE_CATEGORY, null);
        int cnt = cur.getCount();
        Log.i("exploreCatData Count", cnt + "");
        return cur;
    }

    public Cursor talentCategroryData() {
        Cursor cur=null;
        try {
            SQLiteDatabase db = this.getReadableDatabase();
            cur = db.rawQuery("select DISTINCT tbl_name from sqlite_master where tbl_name = '"
                    + TABLE_TALENT_CATEGORY + "'", null);
            if (cur.getCount()>0){
                 cur = db.rawQuery("SELECT * FROM " + TABLE_TALENT_CATEGORY, null);
                int cnt = cur.getCount();
                Log.i("exploreCatData Count", cnt + "");
                return cur;
            }else {
                db.execSQL(" create table " + TABLE_TALENT_CATEGORY + "(" + COL_CATEGORY_TIITLE + " text," + COL_CATEGORY_ID + " text )");
                cur = db.rawQuery("SELECT * FROM " + TABLE_TALENT_CATEGORY, null);
                int cnt = cur.getCount();
                Log.i("exploreCatData Count", cnt + "");
                return cur;
            }
        }catch (SQLiteException e){

        }
        return cur;
    }

    public long insert_filesData(String newsId, String fileType, String fileName, String filePath) {
        try {
            sql = this.getWritableDatabase();
            ContentValues cv = new ContentValues();
            cv.put(COL_NEWS_ID, newsId);
            cv.put(COL_FILE_TYPE, fileType);
            cv.put(COL_FILE_NAME, fileName);
            cv.put(COL_FILE_PATH, filePath);
            long i = sql.insert(TABLE_POST, null, cv);
            Log.e("insert_filesData save", i + "");
            return i;
        } catch (Exception e) {
            e.printStackTrace();
            return -1;

        } finally {
            sql.close();
        }
    }

    public boolean update_master(String isPosted, String postedLat, String postedLong, String postedOn, String newsId) {
        try {
            sql = this.getWritableDatabase();
            ContentValues cv = new ContentValues();
            cv.put(COL_IS_POSTED, isPosted);
            cv.put(COL_FILE_POSTED_LAT, postedLat);
            cv.put(COL_FILE_POSTED_LONG, postedLong);
            cv.put(COL_FILE_POSTEDON, postedOn);
            long i1 = sql.update(TABLE_MASTER, cv, COL_PRIMARY_KEY + "='" + newsId + "'", null);
            Log.e("update_master", i1 + "");
            return true;

        } catch (Exception e) {
            e.printStackTrace();
            return false;
        } finally {
            sql.close();
        }
    }

    public int rowCount(String query) {
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cur = db.rawQuery(query, null);
        int cnt = cur.getCount();
        return cnt;
    }

    public boolean update_tb_post(String postedLat, String postedLong, String postedOn, String newsId) {
        try {
            sql = this.getWritableDatabase();
            ContentValues cv = new ContentValues();
            cv.put(COL_FILE_POSTED_LAT, postedLat);
            cv.put(COL_FILE_POSTED_LONG, postedLong);
            cv.put(COL_FILE_POSTEDON, postedOn);
            long i1 = sql.update(TABLE_POST, cv, COL_NEWS_ID + "='" + newsId + "'", null);
            Log.e("update_profile", i1 + "");
            return true;

        } catch (Exception e) {
            e.printStackTrace();
            return false;
        } finally {
            sql.close();
        }
    }


    public Boolean update_profileImagePath(String img_url, String usercode) {
        ContentValues cv = new ContentValues();
        cv.put("col_profileimage", img_url);
        long ii = sql.update(" tb_profile ", cv, "col_usercode" + "='" + usercode + "'", null);
        Log.i("update_profileImagePath", ii + "");
        sql.close();
        // Toast.makeText(ctx, "In getApplication" + ii, Toast.LENGTH_LONG).show();
        return true;
    }


    /**
     * Delete the reminder with the given rowId
     *
     * @param taskCode id of reminder to delete
     * @return true if deleted, false otherwise
     */

    public boolean deleteReminder(String taskCode) {
        return sql.delete(" tb_todo_list ", " COL_TASK_CODE  =" + taskCode, null) > 0;
    }

    public Cursor fetch_UnPostedData(String status) {
        SQLiteDatabase db = this.getReadableDatabase();
        Log.i("m in ", "m out");
        Cursor cur = db.rawQuery("SELECT * FROM " + TABLE_MASTER + " where " + COL_IS_POSTED + "='" + status + "'", null);
        int cnt = cur.getCount();
        Log.i("fetch_UnPostedData", cnt + "");
        return cur;
    }

    public Cursor fetch_LastRecord() {
        SQLiteDatabase db = this.getReadableDatabase();
        Log.i("m in ", "m out");
        Cursor cursor = db.rawQuery(" SELECT * FROM  " + TABLE_MASTER, null);
        cursor.moveToLast();
        return cursor;
    }

    public Cursor fetch_MediaFiles(String newsId) {
        SQLiteDatabase db = this.getReadableDatabase();
        Log.i("m in ", "m out");
        Cursor cur = db.rawQuery("SELECT * FROM " + TABLE_POST + " where " + COL_NEWS_ID + "='" + newsId + "'", null);
        int cnt = cur.getCount();
        Log.i("fetch_MediaFiles", cnt + "");
        return cur;
    }

    public void delete_postData(String newsId) {
        SQLiteDatabase db = this.getWritableDatabase();
        String sql = "DELETE FROM " + TABLE_POST + "  WHERE " + COL_NEWS_ID + "=" + newsId;
        Log.i("sql data", sql);

        db.execSQL(sql);
    }

    public void delete_masterRowData(String newsId) {
        SQLiteDatabase db = this.getWritableDatabase();
        String sql = "DELETE FROM " + TABLE_MASTER + "  WHERE " + COL_PRIMARY_KEY + "=" + newsId;
        Log.i("sql data", sql);
        db.execSQL(sql);
    }
}