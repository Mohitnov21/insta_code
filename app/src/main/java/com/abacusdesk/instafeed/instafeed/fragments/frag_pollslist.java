package com.abacusdesk.instafeed.instafeed.fragments;

import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.abacusdesk.instafeed.instafeed.R;
import com.abacusdesk.instafeed.instafeed.Util.Alerts;
import com.abacusdesk.instafeed.instafeed.Util.Apis;
import com.abacusdesk.instafeed.instafeed.Util.CommonFunctions;
import com.abacusdesk.instafeed.instafeed.adapter.OpinionAdapter_tab;
import com.abacusdesk.instafeed.instafeed.adapter.Poll_Adapter;
import com.abacusdesk.instafeed.instafeed.custom_dialog.Dialogs;
import com.abacusdesk.instafeed.instafeed.model.OpinionAll_modeltab;
import com.abacusdesk.instafeed.instafeed.model.Polls_listmodel;
import com.android.volley.AuthFailureError;
import com.android.volley.NoConnectionError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.gson.Gson;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by ishaan on 6/21/2017.
 */

public class frag_pollslist extends Fragment {

    View view;
    RecyclerView recyclerView;
    Poll_Adapter adapter;

    ArrayList<Polls_listmodel.DataBean> arrayPolls=new ArrayList<>();
    Polls_listmodel polls_listmodel;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_pollslist, container, false);
        recyclerView = (RecyclerView) view.findViewById(R.id.recyclerView);

        if (CommonFunctions.isConnected(getActivity())){
            if (!arrayPolls.isEmpty()){
                arrayPolls.clear();
            }
            getPollslist();
        }else {
           // Toast.makeText(getActivity(), "Internet not available", Toast.LENGTH_SHORT).show();
        }
        setRecyclerView();
        return view;
    }

    public void getPollslist() {
        // Dialogs.showProDialog(getActivity(), "Loading");
        StringRequest stringRequest = new StringRequest(Request.Method.GET, Apis.Polls_list,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                       // Log.e("log", "" + response);
                        try {
                            getResponse(response);
                        } catch (JSONException e) {
                            //e.printStackTrace();
                        }
                        Dialogs.disDialog();
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {

                    }
                })
        {

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Content-Type", "application/x-www-form-urlencoded");
                return params;
            }
        };

        RequestQueue requestQueue = Volley.newRequestQueue(getActivity());
        requestQueue.add(stringRequest);
    }

    private void getResponse(String response ) throws JSONException {
        if(!response.equals(null)) {
            try {
                JSONObject jsonObject = new JSONObject(response);
                Object object = jsonObject.get("data");
                if (object instanceof JSONArray) {
                    Gson gson = new Gson();
                    polls_listmodel = gson.fromJson(response, Polls_listmodel.class);
                    int status = polls_listmodel.getStatus();
                    String msg = polls_listmodel.getMessage();
                    if (msg.equalsIgnoreCase("success") && status == 200) {
                        if (polls_listmodel.getData() != null && !polls_listmodel.getData().isEmpty()) {
                            for (int i = 0; i < polls_listmodel.getData().size(); i++) {
                                try {
                                    Polls_listmodel.DataBean dataBean = polls_listmodel.getData().get(i);
                                    arrayPolls.add(dataBean);
                                } catch (Exception e) {
                                    // e.printStackTrace();
                                }
                            }
                        }
                        adapter.notifyDataSetChanged();
                    }
                }
            }catch (Exception e){

            }
        }
    }

    private void setRecyclerView() {
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        // recyclerView.addItemDecoration(mDividerItemDecoration);
        try{
            adapter= new Poll_Adapter(getActivity(),arrayPolls);
            recyclerView.setAdapter(adapter);
            adapter.notifyDataSetChanged();
        }catch (Exception e){

        }
    }

}
