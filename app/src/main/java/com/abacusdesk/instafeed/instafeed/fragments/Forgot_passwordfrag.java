package com.abacusdesk.instafeed.instafeed.fragments;

import android.app.Activity;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TextInputEditText;
import android.support.design.widget.TextInputLayout;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.abacusdesk.instafeed.instafeed.R;
import com.abacusdesk.instafeed.instafeed.Util.Apis;
import com.abacusdesk.instafeed.instafeed.custom_dialog.Dialogs;
import com.abacusdesk.instafeed.instafeed.webservice.VolleySingleton;
import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by abacusdesk on 2017-07-25.
 */

public class Forgot_passwordfrag extends Activity {

    private View view;
    private TextInputLayout inputemail;
    private TextInputEditText edemail;
    private Button btnsubmit;
    private static String email;
    Toolbar toolbar;

    /* @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view=inflater.inflate(R.layout.forgot_fragment,container,false);
        init();
        if (CommonFunctions.isConnected(getActivity())){
            forgotPassword();
        }else {
            Toast.makeText(getActivity(), "Internet not available", Toast.LENGTH_SHORT).show();
        }
        return view;
    }*/

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.forgot_fragment);
        init();
    }

    void init(){
        toolbar = (Toolbar) findViewById(R.id.toolbar);

        //toolbar.setTitle("Forgot Password");
        // setSupportActionBar(toolbar);
        btnsubmit=(Button)findViewById(R.id.btn_submit);
        inputemail=(TextInputLayout)findViewById(R.id.txtinput_email);
        edemail=(TextInputEditText)findViewById(R.id.ed_email);
        btnsubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                email = inputemail.getEditText().getText().toString();
                if (email.equals("")) {
                    edemail.setError("Enter Email Id");
                }else {
                    forgotPassword();
                }
            }
        });
    }

    public void forgotPassword() {
        Dialogs.showProDialog(Forgot_passwordfrag.this, "Loading");
        StringRequest stringRequest = new StringRequest(Request.Method.POST, Apis.forgot_password,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.e("log", "" + response);
                        try {
                            getResponse(response);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        Dialogs.disDialog();
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Dialogs.disDialog();
                        //Toast.makeText(getActivity(), error.toString(), Toast.LENGTH_LONG).show();
                    }
                })
        {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> map = new HashMap<String, String>();
                map.put("email",email);
                return map;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Content-Type", "application/x-www-form-urlencoded");
                return params;
            }
        };
        VolleySingleton.getInstance(Forgot_passwordfrag.this).addToRequestQueue(stringRequest);
    }


    private void getResponse(String response ) throws JSONException {
        if(!response.equals(null)){
            JSONObject jsonObject=new JSONObject(response);
            if (jsonObject.getString("message").equalsIgnoreCase("success"));{
            Toast.makeText(Forgot_passwordfrag.this,jsonObject.getJSONObject("data").getString("message"),Toast.LENGTH_LONG).show();
            finish();
            }
          }
        }
    }

