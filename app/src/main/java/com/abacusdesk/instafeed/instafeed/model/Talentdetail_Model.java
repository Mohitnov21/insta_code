package com.abacusdesk.instafeed.instafeed.model;

import com.google.gson.Gson;

import java.util.List;

/**
 * Created by ishaan on 7/11/2017.
 */

public class Talentdetail_Model {


    /**
     * status : 200
     * message : success
     * data : [{"id":"13","talent_category_id":"4","name":"Market","title":"talent testing","slug":"talent-testing-1296545164","short_description":"talent test succesfull talent test succesfull talent test succesfull","description":"talent test succesfull talent test succesfull talent test succesfull","total_likes":null,"total_dislikes":null,"total_comments":null,"total_views":"4","total_flags":null,"dt_added":"2017-07-11 17:25:39","status":"P","user_id":"3924","first_name":"jitu","last_name":"kumar","nickname":"","avatar":null,"username":"jkg","source":"w","latitude":null,"longitude":null},{"comments":[{"id":"8","comment":"talent","comment_id":null,"dt_added":"2017-07-11 17:29:10","dt_modified":"2017-07-11 17:29:10","status":"A","user_id":"3924","first_name":"Vijay","last_name":"Undre","nickname":"Vijay","avatar":null,"username":"vijayimmense"}]},{"images":[{"id":"5","image":"http://instafeed.org/storage/talent_mime/6c71a160c0807814cdbfbefc39cc8ac4.jpg"},{"id":"6","image":"http://instafeed.org/storage/talent_mime/64eac7fc8edda010fe71e7a0ad906add.jpg"}]},{"videos":[{"id":"7","video":"http://instafeed.org/storage/talent_mime/7b16328c572b04d8ec8c9e784a6781d0.mp4","video_thumb":"http://instafeed.org/storage/talent_mime/7b16328c572b04d8ec8c9e784a6781d0.jpg","vimeo_video_id":null,"vimeo_response":null}]},{"audio":[{"id":"2","audio":"840db9ebc4143aaff393cf9cfe028d5c.mp3"}]}]
     */

    private int status;
    private String message;
    private List<DataBean> data;

    public static Talentdetail_Model objectFromData(String str) {

        return new Gson().fromJson(str, Talentdetail_Model.class);
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public List<DataBean> getData() {
        return data;
    }

    public void setData(List<DataBean> data) {
        this.data = data;
    }

    public static class DataBean {
        /**
         * id : 13
         * talent_category_id : 4
         * name : Market
         * title : talent testing
         * slug : talent-testing-1296545164
         * short_description : talent test succesfull talent test succesfull talent test succesfull
         * description : talent test succesfull talent test succesfull talent test succesfull
         * total_likes : null
         * total_dislikes : null
         * total_comments : null
         * total_views : 4
         * total_flags : null
         * dt_added : 2017-07-11 17:25:39
         * status : P
         * user_id : 3924
         * first_name : jitu
         * last_name : kumar
         * nickname :
         * avatar : null
         * username : jkg
         * source : w
         * latitude : null
         * longitude : null
         * comments : [{"id":"8","comment":"talent","comment_id":null,"dt_added":"2017-07-11 17:29:10","dt_modified":"2017-07-11 17:29:10","status":"A","user_id":"3924","first_name":"Vijay","last_name":"Undre","nickname":"Vijay","avatar":null,"username":"vijayimmense"}]
         * images : [{"id":"5","image":"http://instafeed.org/storage/talent_mime/6c71a160c0807814cdbfbefc39cc8ac4.jpg"},{"id":"6","image":"http://instafeed.org/storage/talent_mime/64eac7fc8edda010fe71e7a0ad906add.jpg"}]
         * videos : [{"id":"7","video":"http://instafeed.org/storage/talent_mime/7b16328c572b04d8ec8c9e784a6781d0.mp4","video_thumb":"http://instafeed.org/storage/talent_mime/7b16328c572b04d8ec8c9e784a6781d0.jpg","vimeo_video_id":null,"vimeo_response":null}]
         * audio : [{"id":"2","audio":"840db9ebc4143aaff393cf9cfe028d5c.mp3"}]
         */

        private String id;
        private String talent_category_id;
        private String name;
        private String title;
        private String slug;
        private String short_description;
        private String description;
        private String total_likes;
        private String total_dislikes;
        private Object total_comments;
        private String total_views;
        private Object total_flags;
        private String dt_added;
        private String status;
        private String user_id;
        private String first_name;
        private String last_name;
        private String nickname;
        private Object avatar;
        private String username;
        private String source;
        private Object latitude;
        private Object longitude;
        private List<CommentsBean> comments;
        private List<ImagesBean> images;
        private List<VideosBean> videos;
        private List<AudioBean> audio;

        public static DataBean objectFromData(String str) {

            return new Gson().fromJson(str, DataBean.class);
        }

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getTalent_category_id() {
            return talent_category_id;
        }

        public void setTalent_category_id(String talent_category_id) {
            this.talent_category_id = talent_category_id;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getTitle() {
            return title;
        }

        public void setTitle(String title) {
            this.title = title;
        }

        public String getSlug() {
            return slug;
        }

        public void setSlug(String slug) {
            this.slug = slug;
        }

        public String getShort_description() {
            return short_description;
        }

        public void setShort_description(String short_description) {
            this.short_description = short_description;
        }

        public String getDescription() {
            return description;
        }

        public void setDescription(String description) {
            this.description = description;
        }

        public String getTotal_likes() {
            return total_likes;
        }

        public void setTotal_likes(String total_likes) {
            this.total_likes = total_likes;
        }

        public String getTotal_dislikes() {
            return total_dislikes;
        }

        public void setTotal_dislikes(String total_dislikes) {
            this.total_dislikes = total_dislikes;
        }

        public Object getTotal_comments() {
            return total_comments;
        }

        public void setTotal_comments(Object total_comments) {
            this.total_comments = total_comments;
        }

        public String getTotal_views() {
            return total_views;
        }

        public void setTotal_views(String total_views) {
            this.total_views = total_views;
        }

        public Object getTotal_flags() {
            return total_flags;
        }

        public void setTotal_flags(Object total_flags) {
            this.total_flags = total_flags;
        }

        public String getDt_added() {
            return dt_added;
        }

        public void setDt_added(String dt_added) {
            this.dt_added = dt_added;
        }

        public String getStatus() {
            return status;
        }

        public void setStatus(String status) {
            this.status = status;
        }

        public String getUser_id() {
            return user_id;
        }

        public void setUser_id(String user_id) {
            this.user_id = user_id;
        }

        public String getFirst_name() {
            return first_name;
        }

        public void setFirst_name(String first_name) {
            this.first_name = first_name;
        }

        public String getLast_name() {
            return last_name;
        }

        public void setLast_name(String last_name) {
            this.last_name = last_name;
        }

        public String getNickname() {
            return nickname;
        }

        public void setNickname(String nickname) {
            this.nickname = nickname;
        }

        public Object getAvatar() {
            return avatar;
        }

        public void setAvatar(Object avatar) {
            this.avatar = avatar;
        }

        public String getUsername() {
            return username;
        }

        public void setUsername(String username) {
            this.username = username;
        }

        public String getSource() {
            return source;
        }

        public void setSource(String source) {
            this.source = source;
        }

        public Object getLatitude() {
            return latitude;
        }

        public void setLatitude(Object latitude) {
            this.latitude = latitude;
        }

        public Object getLongitude() {
            return longitude;
        }

        public void setLongitude(Object longitude) {
            this.longitude = longitude;
        }

        public List<CommentsBean> getComments() {
            return comments;
        }

        public void setComments(List<CommentsBean> comments) {
            this.comments = comments;
        }

        public List<ImagesBean> getImages() {
            return images;
        }

        public void setImages(List<ImagesBean> images) {
            this.images = images;
        }

        public List<VideosBean> getVideos() {
            return videos;
        }

        public void setVideos(List<VideosBean> videos) {
            this.videos = videos;
        }

        public List<AudioBean> getAudio() {
            return audio;
        }

        public void setAudio(List<AudioBean> audio) {
            this.audio = audio;
        }

        public static class CommentsBean {
            /**
             * id : 8
             * comment : talent
             * comment_id : null
             * dt_added : 2017-07-11 17:29:10
             * dt_modified : 2017-07-11 17:29:10
             * status : A
             * user_id : 3924
             * first_name : Vijay
             * last_name : Undre
             * nickname : Vijay
             * avatar : null
             * username : vijayimmense
             */

            private String id;
            private String comment;
            private Object comment_id;
            private String dt_added;
            private String dt_modified;
            private String status;
            private String user_id;
            private String first_name;
            private String last_name;
            private String nickname;
            private Object avatar;
            private String username;

            public static CommentsBean objectFromData(String str) {

                return new Gson().fromJson(str, CommentsBean.class);
            }

            public String getId() {
                return id;
            }

            public void setId(String id) {
                this.id = id;
            }

            public String getComment() {
                return comment;
            }

            public void setComment(String comment) {
                this.comment = comment;
            }

            public Object getComment_id() {
                return comment_id;
            }

            public void setComment_id(Object comment_id) {
                this.comment_id = comment_id;
            }

            public String getDt_added() {
                return dt_added;
            }

            public void setDt_added(String dt_added) {
                this.dt_added = dt_added;
            }

            public String getDt_modified() {
                return dt_modified;
            }

            public void setDt_modified(String dt_modified) {
                this.dt_modified = dt_modified;
            }

            public String getStatus() {
                return status;
            }

            public void setStatus(String status) {
                this.status = status;
            }

            public String getUser_id() {
                return user_id;
            }

            public void setUser_id(String user_id) {
                this.user_id = user_id;
            }

            public String getFirst_name() {
                return first_name;
            }

            public void setFirst_name(String first_name) {
                this.first_name = first_name;
            }

            public String getLast_name() {
                return last_name;
            }

            public void setLast_name(String last_name) {
                this.last_name = last_name;
            }

            public String getNickname() {
                return nickname;
            }

            public void setNickname(String nickname) {
                this.nickname = nickname;
            }

            public Object getAvatar() {
                return avatar;
            }

            public void setAvatar(Object avatar) {
                this.avatar = avatar;
            }

            public String getUsername() {
                return username;
            }

            public void setUsername(String username) {
                this.username = username;
            }
        }

        public static class ImagesBean {
            /**
             * id : 77
             * image : https://instafeed.org/storage/explorer_mime/256x170-af67a4081336abc28db2138cd2edc5fd.jpg
             * image_zoom : https://instafeed.org/storage/explorer_mime/748x420-af67a4081336abc28db2138cd2edc5fd.jpg
             * image_original : https://instafeed.org/storage/explorer_mime/af67a4081336abc28db2138cd2edc5fd.jpg
             * image_80x80 : https://instafeed.org/storage/explorer_mime/80x80-af67a4081336abc28db2138cd2edc5fd.jpg
             * image_100x100 : https://instafeed.org/storage/explorer_mime/100x100-af67a4081336abc28db2138cd2edc5fd.jpg
             * image_256x170 : https://instafeed.org/storage/explorer_mime/256x170-af67a4081336abc28db2138cd2edc5fd.jpg
             * image_264x200 : https://instafeed.org/storage/explorer_mime/264x200-af67a4081336abc28db2138cd2edc5fd.jpg
             * image_330x210 : https://instafeed.org/storage/explorer_mime/330x210-af67a4081336abc28db2138cd2edc5fd.jpg
             * image_360x200 : https://instafeed.org/storage/explorer_mime/360x200-af67a4081336abc28db2138cd2edc5fd.jpg
             */

            private String id;
            private String image;
            private String image_zoom;
            private String image_original;
            private String image_80x80;
            private String image_100x100;
            private String image_256x170;
            private String image_264x200;
            private String image_330x210;
            private String image_360x200;

            public String getId() {
                return id;
            }

            public void setId(String id) {
                this.id = id;
            }

            public String getImage() {
                return image;
            }

            public void setImage(String image) {
                this.image = image;
            }

            public String getImage_zoom() {
                return image_zoom;
            }

            public void setImage_zoom(String image_zoom) {
                this.image_zoom = image_zoom;
            }

            public String getImage_original() {
                return image_original;
            }

            public void setImage_original(String image_original) {
                this.image_original = image_original;
            }

            public String getImage_80x80() {
                return image_80x80;
            }

            public void setImage_80x80(String image_80x80) {
                this.image_80x80 = image_80x80;
            }

            public String getImage_100x100() {
                return image_100x100;
            }

            public void setImage_100x100(String image_100x100) {
                this.image_100x100 = image_100x100;
            }

            public String getImage_256x170() {
                return image_256x170;
            }

            public void setImage_256x170(String image_256x170) {
                this.image_256x170 = image_256x170;
            }

            public String getImage_264x200() {
                return image_264x200;
            }

            public void setImage_264x200(String image_264x200) {
                this.image_264x200 = image_264x200;
            }

            public String getImage_330x210() {
                return image_330x210;
            }

            public void setImage_330x210(String image_330x210) {
                this.image_330x210 = image_330x210;
            }

            public String getImage_360x200() {
                return image_360x200;
            }

            public void setImage_360x200(String image_360x200) {
                this.image_360x200 = image_360x200;
            }
        }



        public static class VideosBean {
            /**
             * id : 7
             * video : http://instafeed.org/storage/talent_mime/7b16328c572b04d8ec8c9e784a6781d0.mp4
             * video_thumb : http://instafeed.org/storage/talent_mime/7b16328c572b04d8ec8c9e784a6781d0.jpg
             * vimeo_video_id : null
             * vimeo_response : null
             */

            private String id;
            private String video;
            private String video_thumb;
            private Object vimeo_video_id;
            private Object vimeo_response;

            public static VideosBean objectFromData(String str) {

                return new Gson().fromJson(str, VideosBean.class);
            }

            public String getId() {
                return id;
            }

            public void setId(String id) {
                this.id = id;
            }

            public String getVideo() {
                return video;
            }

            public void setVideo(String video) {
                this.video = video;
            }

            public String getVideo_thumb() {
                return video_thumb;
            }

            public void setVideo_thumb(String video_thumb) {
                this.video_thumb = video_thumb;
            }

            public Object getVimeo_video_id() {
                return vimeo_video_id;
            }

            public void setVimeo_video_id(Object vimeo_video_id) {
                this.vimeo_video_id = vimeo_video_id;
            }

            public Object getVimeo_response() {
                return vimeo_response;
            }

            public void setVimeo_response(Object vimeo_response) {
                this.vimeo_response = vimeo_response;
            }
        }

        public static class AudioBean {
            /**
             * id : 2
             * audio : 840db9ebc4143aaff393cf9cfe028d5c.mp3
             */

            private String id;
            private String audio;

            public static AudioBean objectFromData(String str) {

                return new Gson().fromJson(str, AudioBean.class);
            }

            public String getId() {
                return id;
            }

            public void setId(String id) {
                this.id = id;
            }

            public String getAudio() {
                return audio;
            }

            public void setAudio(String audio) {
                this.audio = audio;
            }
        }
    }
}
