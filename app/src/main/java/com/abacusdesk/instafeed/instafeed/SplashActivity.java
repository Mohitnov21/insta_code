package com.abacusdesk.instafeed.instafeed;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Window;
import android.widget.ArrayAdapter;
import android.widget.FrameLayout;

import com.abacusdesk.instafeed.instafeed.Util.AppSingleton;
import com.abacusdesk.instafeed.instafeed.Util.DbHandler;
import com.abacusdesk.instafeed.instafeed.Util.SaveSharedPreference;
import com.abacusdesk.instafeed.instafeed.Util.WebUrl;
import com.abacusdesk.instafeed.instafeed.fragments.HomeTab_fragment;
import com.abacusdesk.instafeed.instafeed.model.NewCategoryListModel;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.google.gson.Gson;

import static com.abacusdesk.instafeed.instafeed.Util.DbHandler.TABLE_EXPLORE_CATEGORY;
import static com.abacusdesk.instafeed.instafeed.Util.DbHandler.TABLE_NEWS_CATEGORY;

public class SplashActivity extends AppCompatActivity  {
    public static int SPLASH_TIME_OUT = 2000;
    DbHandler dbHandler;
    NewCategoryListModel newCategoryListModel;
    private static final int countrow = 1;
    int count = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.activity_splash);
        if (!SaveSharedPreference.getUserID(SplashActivity.this).equals("")) {
            Log.e("first", "screen");
            startActivity(new Intent(SplashActivity.this, NavDrawerActivity.class));
            finish();
        }else {
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    Log.e("second","screen");
                    if (SaveSharedPreference.getIsSplash(SplashActivity.this).equals("true")) {
                        startActivity(new Intent(SplashActivity.this, NavDrawerActivity.class));
                        finish();
                    } else {
                        startActivity(new Intent(SplashActivity.this, IntroScreens.class));
                        finish();
                    }
                }
            }, SPLASH_TIME_OUT);
        }
    }
 }
