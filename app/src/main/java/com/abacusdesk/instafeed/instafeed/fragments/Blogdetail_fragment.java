package com.abacusdesk.instafeed.instafeed.fragments;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.support.annotation.Nullable;
import android.support.annotation.RequiresApi;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.method.ScrollingMovementMethod;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.abacusdesk.instafeed.instafeed.NavDrawerActivity;
import com.abacusdesk.instafeed.instafeed.R;
import com.abacusdesk.instafeed.instafeed.Util.Alerts;
import com.abacusdesk.instafeed.instafeed.Util.Apis;
import com.abacusdesk.instafeed.instafeed.Util.SaveSharedPreference;
import com.abacusdesk.instafeed.instafeed.adapter.RecyclerCommentAdapter;
import com.abacusdesk.instafeed.instafeed.adapter.RelatedPosts_adapter;
import com.abacusdesk.instafeed.instafeed.adapter.ViewPagerAdapter;
import com.abacusdesk.instafeed.instafeed.custom_dialog.Dialogs;
import com.abacusdesk.instafeed.instafeed.model.Blogdetail_model;
import com.abacusdesk.instafeed.instafeed.model.Opiniondetail_model;
import com.android.volley.AuthFailureError;
import com.android.volley.NoConnectionError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.bumptech.glide.Glide;
import com.google.gson.Gson;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Timer;

import cn.jzvd.JZVideoPlayer;

/**
 * Created by abacusdesk on 2017-08-03.
 */

public class Blogdetail_fragment extends Fragment implements View.OnClickListener{

    View view,view2,view1;
    Blogdetail_model blogdetail_model;
    TextView txtname,txtdescr,txtshortdescr,txtdate,txtplace,txttitle;
    TextView txtusername;
    ImageView img_back,img_forward;
    public  EditText edcomment;

    ArrayList<Blogdetail_model.DataBean> arrayList=new ArrayList<>();
    ImageView imgshare;

    public  String id="",catid="",postid="",temp,temp2,comment="";

    private String vote="";

    Blogdetail_model.DataBean dataBean_detail,databean_comment;
    public  ArrayList<HashMap<String,String>> arrayhashComent=new ArrayList<>();
    public  RecyclerCommentAdapter adapter;
    ViewPagerAdapter viewPagerAdapter;
    FrameLayout lnlistview;
    ViewPager imgviewpager;
    Boolean flag=false;

    LinearLayout ln_userdetail,lncomment,lnlocation,lnlike,lndislike,lnlikesection;
    public  ArrayList<HashMap<String,String>> arrayviewcontainer=new ArrayList<>();

    Boolean like=false,dislike=false,fav=false;
    TextView txt_ownername,txtlike,txtdislike,txtcomment;
    ImageView imgonview;
    String location;
    Bitmap imagebit;
    ArrayList<HashMap<String,String>> array_relatedposts=new ArrayList<>();
    RelatedPosts_adapter adapterrelated;
    String temp_related="";

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.detail_finallayout, container, false);
        init();
        getActivity().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_PAN |WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);
        return view;
    }

    private void init(){
        comment_click();
        if (!arrayviewcontainer.isEmpty()){
            arrayviewcontainer.clear();
        }
        if (!arrayhashComent.isEmpty()){
            arrayhashComent.clear();
        }
        Bundle bundle = this.getArguments();
        if (bundle != null) {
            postid=bundle.getString("postid");
            catid=bundle.getString("catid");
        }

        if (NavDrawerActivity.toolbar!=null){
            NavDrawerActivity.toolbar.setLogo(null);
            NavDrawerActivity.toolbar.setTitle("Blogs");
        }
        ln_userdetail=(LinearLayout)view.findViewById(R.id.ln_publicprofile);

        lnlikesection=(LinearLayout)view.findViewById(R.id.ln_likesection);
        lnlikesection.setVisibility(View.GONE);

        imgviewpager=(ViewPager)view.findViewById(R.id.img_viewpager);
        imgshare=(ImageView)view.findViewById(R.id.img_share);
        lncomment=(LinearLayout)view.findViewById(R.id.ln_comment);



        lnlike=(LinearLayout)view.findViewById(R.id.ln_like);
        lndislike=(LinearLayout)view.findViewById(R.id.ln_down);

        lnlike.setVisibility(View.GONE);
        lndislike.setVisibility(View.GONE);

        view2=(View)view.findViewById(R.id.view2);
        view1=(View)view.findViewById(R.id.view1);

        //  view2.setVisibility(View.GONE);
        //view1.setVisibility(View.GONE);

        imgonview=(ImageView)view.findViewById(R.id.img_view);

        //txt_ownername=(TextView)view.findViewById(R.id.txt_owner);
        //txt_ownername.setVisibility(View.GONE);

        img_back=(ImageView)view.findViewById(R.id.img_left);
        img_forward=(ImageView)view.findViewById(R.id.img_right);

        img_back.setVisibility(View.GONE);
        img_forward.setVisibility(View.GONE);

        //ln_userdetail.setOnClickListener(this);

        lnlocation=(LinearLayout)view.findViewById(R.id.ln_location);
        lnlocation.setVisibility(View.GONE);

        txtlike=(TextView)view.findViewById(R.id.txt_like);
        txtdislike=(TextView)view.findViewById(R.id.txt_dislike);

        txtcomment=(TextView)view.findViewById(R.id.txt_comment);

        imgshare.setOnClickListener(this);
        txtlike.setOnClickListener(this);
        txtdislike.setOnClickListener(this);
        lncomment.setOnClickListener(this);
        txtcomment.setOnClickListener(this);



        txtname=(TextView)view.findViewById(R.id.txt_name);
        txtdescr=(TextView)view.findViewById(R.id.txt_shortdescr);
        txtshortdescr=(TextView)view.findViewById(R.id.txt_descrshort);

        txtdate=(TextView)view.findViewById(R.id.txt_date);
        txtplace=(TextView)view.findViewById(R.id.txt_place);
        txttitle=(TextView)view.findViewById(R.id.txt_title);

        temp= Apis.Blogs_detail.replace("catid",catid);
        temp2=temp.replace("postid", postid);
        Log.e("catid",""+catid+"postid"+""+postid);


        view.setFocusableInTouchMode(true);
        view.requestFocus();
        view.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if (keyCode == KeyEvent.KEYCODE_BACK) {
                    // ViewPagerAdapter.stopVideo(getActivity());
                    getActivity().getSupportFragmentManager().popBackStack();
                    return true;
                } else {
                    return false;
                }
            }
        });
        getBlogdetail(getActivity());
        init_related();
        //getComments(getActivity());
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Log.e("oncreate","");
        setHasOptionsMenu(true);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater){
        // TODO your code to hide item here
        Log.e("settings","");
        menu.findItem(R.id.menu_comment).setVisible(true);
        menu.findItem(R.id.menu_share).setVisible(true);
        menu.findItem(R.id.action_form).setVisible(false);
        menu.findItem(R.id.action_settings).setVisible(false);
        super.onCreateOptionsMenu(menu, inflater);
    }

    private void comment_click(){
        EditText edcomment=(EditText)view.findViewById(R.id.ed_comment);
        edcomment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getcommentlist();
            }
        });
    }


    /*  @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.menu_comment:
                try {
                    CommentsList_fragment fragment = new CommentsList_fragment();
                    Bundle bundle = new Bundle();
                    bundle.putSerializable("arrayhash",arrayhashComent);
                    bundle.putString("postid",postid);
                    bundle.putString("type","blogs");
                    fragment.setArguments(bundle);
                    AppCompatActivity activity = (AppCompatActivity)getContext();
                    activity.getSupportFragmentManager().beginTransaction()
                            .add(R.id.frame_container,fragment,"comment").addToBackStack(null).commit();

                }catch (Exception e){

                }

                return true;
            case R.id.menu_share:
                try{
                    MyAsync obj = new MyAsync() {
                        @Override
                        protected void onPostExecute(Bitmap bmp) {
                            super.onPostExecute(bmp);

                            imagebit = bmp;
                            Log.e("imagebit2", "" + imagebit);
                            ByteArrayOutputStream bytes = new ByteArrayOutputStream();
                            imagebit.compress(Bitmap.CompressFormat.JPEG, 100, bytes);
                            File f = new File(Environment.getExternalStorageDirectory() + File.separator + "temporary_file.jpg");
                            try {
                                f.createNewFile();
                                FileOutputStream fo = new FileOutputStream(f);
                                fo.write(bytes.toByteArray());

                            } catch (IOException e) {
                                e.printStackTrace();
                            }
                        }
                    };
                    if (!dataBean_detail.getBanner_80x80().isEmpty())
                        obj.execute();
                }catch (Exception e){

                }
                Intent sharingIntent = new Intent(Intent.ACTION_SEND);
                try{
                    sharingIntent.setType(
                    if (!dataBean_detail.getBanner_80x80().isEmpty())
                        sharingIntent.putExtra(Intent.EXTRA_STREAM, Uri.parse("file:///sdcard/temporary_file.jpg"));
                    //sharingIntent.putExtra(Intent.EXTRA_SUBJECT,dataBean_detail.getTitle());
                    sharingIntent.putExtra(Intent.EXTRA_TEXT,dataBean_detail.getTitle()+"\n"+ Apis.Base2+"blogs/"+dataBean_detail.getSlug());
                    Log.e("shreinterjyt",""+sharingIntent);
                    startActivity(Intent.createChooser(sharingIntent, "Share via"));
                }catch (Exception e){
                    Log.e("exception",""+e);
                }
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }*/

    public void sharecontent(){
        try{
            MyAsync obj = new MyAsync() {
                @Override
                protected void onPostExecute(Bitmap bmp) {
                    super.onPostExecute(bmp);

                    imagebit = bmp;
                    Log.e("imagebit2", "" + imagebit);
                    ByteArrayOutputStream bytes = new ByteArrayOutputStream();
                    imagebit.compress(Bitmap.CompressFormat.JPEG, 100, bytes);
                    File f = new File(Environment.getExternalStorageDirectory() + File.separator + "temporary_file.jpg");
                    try {
                        f.createNewFile();
                        FileOutputStream fo = new FileOutputStream(f);
                        fo.write(bytes.toByteArray());

                    } catch (IOException e) {
                       // e.printStackTrace();
                    }
                }
            };
            if (!dataBean_detail.getBanner_80x80().isEmpty())
                obj.execute();
        }catch (Exception e){

        }
        Intent sharingIntent = new Intent(Intent.ACTION_SEND);
        try{
            sharingIntent.setType("*/*");
            if (!dataBean_detail.getBanner_80x80().isEmpty()){
                sharingIntent.putExtra(Intent.EXTRA_STREAM, Uri.parse("file:///sdcard/temporary_file.jpg"));
            }else {
                sharingIntent.setType("text/plain");
            }
            //sharingIntent.putExtra(Intent.EXTRA_SUBJECT,dataBean_detail.getTitle());
            sharingIntent.putExtra(Intent.EXTRA_TEXT,dataBean_detail.getTitle()+"\n"+ Apis.Base2+"blogs/"+dataBean_detail.getSlug());
            Log.e("shreinterjyt",""+sharingIntent);
            startActivity(Intent.createChooser(sharingIntent, "Share via"));
        }catch (Exception e){
            Log.e("exception",""+e);
        }
    }

    public void getcommentlist(){
        try {
            CommentsList_fragment fragment = new CommentsList_fragment();
            Bundle bundle = new Bundle();
            bundle.putSerializable("arrayhash",arrayhashComent);
            bundle.putString("postid",postid);
            bundle.putString("type","blogs");
            fragment.setArguments(bundle);
            AppCompatActivity activity = (AppCompatActivity)getContext();
            activity.getSupportFragmentManager().beginTransaction()
                    .add(R.id.frame_container,fragment,"comment").addToBackStack(null).commit();

        }catch (Exception e){

        }
    }

    private void init_related(){
        temp_related=Apis.related_blogs.replace("id",catid);
        set_relatedPosts();
        if (!array_relatedposts.isEmpty()){
            array_relatedposts.clear();
        }else {
            getrealtedPosts();
        }
    }

    private void set_relatedPosts(){
        RecyclerView related_list=(RecyclerView)view.findViewById(R.id.recyclerView_related);
        related_list.setHasFixedSize(true);
        related_list.setNestedScrollingEnabled(false);
        related_list.setLayoutManager(new LinearLayoutManager(getActivity()));
        try{
            adapterrelated=new RelatedPosts_adapter(getActivity(),array_relatedposts,"blogs");
            related_list.setAdapter(adapterrelated);
        }catch (Exception e){
           // e.printStackTrace();
        }
    }

    private void set_nextitem() {
        int postemp=0;
        TextView textitle=(TextView)view.findViewById(R.id.txt_related);
        ImageView imgrelated=(ImageView) view.findViewById(R.id.img_related);
        FrameLayout frame_one=(FrameLayout)view.findViewById(R.id.frame_container);
        frame_one.setVisibility(View.VISIBLE);

        try {
            postemp=array_relatedposts.size()-1;
            if (array_relatedposts.get(postemp)!=null){

                if (!array_relatedposts.get(postemp).get("title").isEmpty()){
                    textitle.setText(""+array_relatedposts.get(postemp).get("title"));
                }
                if (!array_relatedposts.get(postemp).get("image").isEmpty()){
                    Glide.with(getActivity()).load(array_relatedposts.get(postemp).get("image")).error(R.drawable.noimage).into(imgrelated);
                }
            }
        } catch (Exception e){

        }

        final int finalPostemp = postemp;
        frame_one.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Blogdetail_fragment fragment = new Blogdetail_fragment();
                Bundle bundle = new Bundle();
                bundle.putString("catid",array_relatedposts.get(finalPostemp).get("catid"));
                bundle.putString("postid",array_relatedposts.get(finalPostemp).get("id"));
                fragment.setArguments(bundle);

                AppCompatActivity activity = (AppCompatActivity) v.getContext();
                activity.getSupportFragmentManager().beginTransaction()
                        .add(R.id.frame_container, fragment,"tempfrag").addToBackStack(null).commit();

            }
        });
    }

    private void getrealtedPosts() {
        Log.e("temporaryurl",""+temp2);
        StringRequest stringRequest = new StringRequest(Request.Method.GET,temp_related ,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.e("log", "" + response);
                        Dialogs.disDialog();
                        try {
                            JSONObject jsonObject=new JSONObject(response);
                            if (jsonObject.getString("status").equals("200")&& jsonObject.getString("message").equalsIgnoreCase("success")){
                                HashMap<String,String> hashMap;
                                for (int i=0;i<jsonObject.getJSONArray("data").length();i++){
                                    hashMap=new HashMap<>();
                                    hashMap.put("title",jsonObject.getJSONArray("data").getJSONObject(i).getString("title"));
                                    hashMap.put("image",jsonObject.getJSONArray("data").getJSONObject(i).getString("banner_330x210"));
                                    hashMap.put("catid",jsonObject.getJSONArray("data").getJSONObject(i).getString("blog_category_id"));
                                    hashMap.put("id",jsonObject.getJSONArray("data").getJSONObject(i).getString("id"));
                                    hashMap.put("video","");
                                    array_relatedposts.add(hashMap);
                                }
                                adapterrelated.notifyDataSetChanged();
                                set_nextitem();
                            }else {

                            }

                        } catch (JSONException e) {
                            //e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Dialogs.disDialog();
                    }
                })
        {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Content-Type", "application/x-www-form-urlencoded");
                return params;
            }
        };
        RequestQueue requestQueue = Volley.newRequestQueue(getActivity());
        requestQueue.add(stringRequest);
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    public  void editComment(final Context context, final String comment, final String comment_id) {
        StringRequest stringRequest = new StringRequest(Request.Method.POST, Apis.opinions_commentedit,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.e("log", "" + response);
                        try {
                            responseEdit(response,context);
                        } catch (JSONException e) {
                            //e.printStackTrace();
                        }

                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Dialogs.disDialog();
                       // Toast.makeText(context, error.toString(), Toast.LENGTH_LONG).show();
                    }
                })
        {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> map = new HashMap<String, String>();
                map.put("user", SaveSharedPreference.getUserID(context));
                map.put("token", SaveSharedPreference.getPrefToken(context));
                map.put("id",comment_id);
                map.put("comment",comment);
                Log.e("mapcomment",""+map);
                return map;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Content-Type", "application/x-www-form-urlencoded");
                return params;
            }
        };

        RequestQueue requestQueue = Volley.newRequestQueue(context);
        requestQueue.add(stringRequest);
    }

    public  void responseEdit(String response,Context context ) throws JSONException {
        JSONObject jsonObject=new JSONObject(response);
        if (jsonObject.getString("message").equalsIgnoreCase("success")){
            edcomment.getText().clear();
            //adapter.notifyDataSetChanged();
            //getOpiniondetail(context);
            Toast.makeText(context,jsonObject.getString("message"),Toast.LENGTH_SHORT).show();
        }else {
            Toast.makeText(context,jsonObject.getString("message"),Toast.LENGTH_SHORT).show();
        }
    }

    public  void getBlogdetail(final Context context) {
        Dialogs.showProDialog(context, "Loading");
        Log.e("temporaryurl",""+temp2);
        StringRequest stringRequest = new StringRequest(Request.Method.GET,temp2,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.e("log", "" + response);
                        Dialogs.disDialog();
                        try {
                            getResponse(response,context);
                        } catch (JSONException e) {
                           // e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Dialogs.disDialog();
                        DialogInterface.OnClickListener onClickTryAgain = new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                Dialogs.disDialog();
                                getBlogdetail(context);
                            }
                        };
                    if (error instanceof NoConnectionError) {
                            Dialogs.disDialog();
                            Alerts.internetConnectionErrorAlert(context, onClickTryAgain);
                        }
                    }
                })
        {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Content-Type", "application/x-www-form-urlencoded");
                return params;
            }
        };
        RequestQueue requestQueue = Volley.newRequestQueue(context);
        requestQueue.add(stringRequest);
    }

    public  String getDate(String date) throws ParseException {
        String myFormat = "yyyy-MM-dd HH:mm:ss";
        DateFormat sdformat = new SimpleDateFormat(myFormat);
        DateFormat targetFormat = new SimpleDateFormat("MMMM dd, yyyy");
        String formattedDate = targetFormat.format(sdformat.parse(date));
        return formattedDate;
    }

    public  void getResponse(String response,Context context ) throws JSONException {
        if (!response.equals(null)) {
            Log.e("log", "" + response);
            JSONObject jsonObject = new JSONObject(response);
            Object object = jsonObject.get("data");
            if (object instanceof JSONArray) {
                Gson gson = new Gson();
                blogdetail_model = gson.fromJson(response, Blogdetail_model.class);
                int status = blogdetail_model.getStatus();
                String msg = blogdetail_model.getMessage();

                if (msg.equalsIgnoreCase("success") && status == 200) {
                    dataBean_detail = blogdetail_model.getData().get(0);
                    databean_comment = blogdetail_model.getData().get(1);

                    if (dataBean_detail != null) {
                        imgonview.setVisibility(View.VISIBLE);
                        ln_userdetail.setVisibility(View.VISIBLE);
                        if (dataBean_detail.getBanner() != "null") {
                            Glide.with(context).load(dataBean_detail.getBanner_330x210()).error(R.drawable.noimage).into(imgonview);
                        }

                        txttitle.setText(dataBean_detail.getTitle());

                        txtdescr.setText(dataBean_detail.getDescription());
                        txtshortdescr.setText(dataBean_detail.getShort_description());

                        if (txtshortdescr.getText().toString().equals("")){
                            txtshortdescr.setVisibility(View.GONE);
                        }

                        if (dataBean_detail.getTotal_views() != null) {
                            // txtvisible.setText("" + dataBean_detail.getTotal_views());
                        }
                        try {
                            txtdate.setText("" + getDate(dataBean_detail.getDt_added()));
                        } catch (ParseException e) {
                           // e.printStackTrace();
                        }
                    } else {
                        ln_userdetail.setEnabled(false);
                        ln_userdetail.setClickable(false);
                    }
                    showCommentList(context);
                    if (!arrayviewcontainer.isEmpty()) {
                        arrayviewcontainer.clear();
                    }
                    // showImages(context);
                }
            }
        }
    }

    private void showCommentList(Context context){
        if (!arrayhashComent.isEmpty()){
            arrayhashComent.clear();
        }
        if (!databean_comment.getComments().isEmpty()){
            HashMap<String,String> hashMap;
            for (int i = 0; i < databean_comment.getComments().size(); i++) {
                try {
                    hashMap=new HashMap<>();
                    Blogdetail_model.DataBean.CommentsBean commentsBean= databean_comment.getComments().get(i);
                    hashMap.put("commentid",commentsBean.getId());
                    hashMap.put("comment",commentsBean.getComment());
                    hashMap.put("userid",commentsBean.getUser_id());
                    hashMap.put("avatar",commentsBean.getAvatar());
                    hashMap.put("username",commentsBean.getUsername());
                    arrayhashComent.add(hashMap);
                } catch (Exception e){
                    //e.printStackTrace();
                }
            }
            // getComments(context);
            //adapter.notifyDataSetChanged();
        }
    }

    public void likePost() {
        //Dialogs.showProDialog(getActivity(), "Loading");
        Log.e("temporaryurl",""+temp2);
        StringRequest stringRequest = new StringRequest(Request.Method.POST, Apis.Blogs_fav ,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.e("log", "" + response);
                        try {
                            resComment(response);
                        } catch (JSONException e) {
                           // e.printStackTrace();
                        }
                        // Dialogs.disDialog();
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Dialogs.disDialog();
                        DialogInterface.OnClickListener onClickTryAgain = new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                Dialogs.disDialog();
                                likePost();
                            }
                        };
                        if (error instanceof TimeoutError) {
                            Dialogs.disDialog();
                            Alerts.timeoutErrorAlert(getContext(), onClickTryAgain);
                        } else if (error instanceof NoConnectionError) {
                            Dialogs.disDialog();
                        }
                    }
                })
        {

            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> map = new HashMap<String, String>();
                map.put("user", SaveSharedPreference.getUserID(getActivity()));
                map.put("token", SaveSharedPreference.getPrefToken(getActivity()));
                map.put("id", id);
                map.put("fav","");
                Log.e("mapcomment",""+map);
                return map;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Content-Type", "application/x-www-form-urlencoded");
                return params;
            }
        };
        RequestQueue requestQueue = Volley.newRequestQueue(getActivity());
        requestQueue.add(stringRequest);
    }

    public void postComment() {
        //  Dialogs.showProDialog(getActivity(), "Loading");
        Log.e("temporaryurl",""+temp2);
        StringRequest stringRequest = new StringRequest(Request.Method.POST, Apis.Blogs_comment ,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.e("log", "" + response);
                        try {
                            resComment(response);
                            //txtcomment.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.comment_click, 0);
                        } catch (JSONException e) {
                           // e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Dialogs.disDialog();
                        DialogInterface.OnClickListener onClickTryAgain = new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                Dialogs.disDialog();
                                postComment();
                            }
                        };
                        if (error instanceof TimeoutError) {
                            Dialogs.disDialog();
                            Alerts.timeoutErrorAlert(getContext(), onClickTryAgain);
                        } else if (error instanceof NoConnectionError) {
                            Dialogs.disDialog();
                            Alerts.internetConnectionErrorAlert(getContext(), onClickTryAgain);
                        }
                    }
                })
        {

            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> map = new HashMap<String, String>();
                map.put("user", SaveSharedPreference.getUserID(getActivity()));
                map.put("token",SaveSharedPreference.getPrefToken(getActivity()));
                map.put("id",id);
                map.put("comment",comment);
                Log.e("mapcomment",""+map);
                return map;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Content-Type", "application/x-www-form-urlencoded");
                return params;
            }
        };

        RequestQueue requestQueue = Volley.newRequestQueue(getActivity());
        requestQueue.add(stringRequest);
    }

    private void resComment(String response ) throws JSONException {
        JSONObject jsonObject=new JSONObject(response);
        if (jsonObject.getString("message").equalsIgnoreCase("success")){
            edcomment.getText().clear();
            getBlogdetail(getActivity());
            lnlistview.setVisibility(View.VISIBLE);
            flag=true;
            Toast.makeText(getActivity(),jsonObject.getString("message"),Toast.LENGTH_SHORT).show();
        }else {
            Toast.makeText(getActivity(),jsonObject.getString("message"),Toast.LENGTH_SHORT).show();
        }
    }

    // TODO on like colour of like button should be changed
    // similarly dislike and also set up icons fav may be modidification in how the comments section should be shown
    // some ids which are invalid in detail news section and make change the profile picture of newsfeed
    // islike paramter in detail section

    public  class MyAsync extends AsyncTask<Void, Void, Bitmap> {

        @Override
        protected Bitmap doInBackground(Void... params) {

            try {
                if (!dataBean_detail.getBanner_80x80().isEmpty()){
                    URL url = new URL(dataBean_detail.getBanner_80x80());
                    HttpURLConnection connection = (HttpURLConnection) url.openConnection();
                    connection.setDoInput(true);
                    connection.connect();
                    InputStream input = connection.getInputStream();
                    Bitmap myBitmap = BitmapFactory.decodeStream(input);
                    return myBitmap;
                }else {
                    return null;
                }
            } catch (IOException e) {
                //e.printStackTrace();
                return null;
            }catch(Exception e){
                return null;
            }
        }
    }

    @Override
    public void onClick(View v) {
        if (v==imgshare){
            try{
                MyAsync obj = new MyAsync() {
                    @Override
                    protected void onPostExecute(Bitmap bmp) {
                        super.onPostExecute(bmp);

                        imagebit = bmp;
                        Log.e("imagebit2", "" + imagebit);
                        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
                        imagebit.compress(Bitmap.CompressFormat.JPEG, 100, bytes);
                        File f = new File(Environment.getExternalStorageDirectory() + File.separator + "temporary_file.jpg");
                        try {
                            f.createNewFile();
                            FileOutputStream fo = new FileOutputStream(f);
                            fo.write(bytes.toByteArray());

                        } catch (IOException e) {
                            //e.printStackTrace();
                        }
                    }
                };
                if (!dataBean_detail.getBanner_80x80().isEmpty())
                    obj.execute();
            }catch (Exception e){

            }
            Intent sharingIntent = new Intent(Intent.ACTION_SEND);
            try{
                sharingIntent.setType("*/*");
                if (!dataBean_detail.getBanner_80x80().isEmpty())
                    sharingIntent.putExtra(Intent.EXTRA_STREAM, Uri.parse("file:///sdcard/temporary_file.jpg"));
                //sharingIntent.putExtra(Intent.EXTRA_SUBJECT,dataBean_detail.getTitle());
                sharingIntent.putExtra(Intent.EXTRA_TEXT,dataBean_detail.getTitle()+"\n"+ Apis.Base2+"blogs/"+dataBean_detail.getSlug());
                Log.e("shreinterjyt",""+sharingIntent);
                startActivity(Intent.createChooser(sharingIntent, "Share via"));
            }catch (Exception e){
                Log.e("exception",""+e);
            }
        }else if (v==lncomment){
            try {
                CommentsList_fragment fragment = new CommentsList_fragment();
                Bundle bundle = new Bundle();
                bundle.putSerializable("arrayhash",arrayhashComent);
                bundle.putString("postid",postid);
                bundle.putString("type","blogs");
                fragment.setArguments(bundle);
                AppCompatActivity activity = (AppCompatActivity) v.getContext();
                activity.getSupportFragmentManager().beginTransaction()
                        .add(R.id.frame_container,fragment,"comment").addToBackStack(null).commit();

            }catch (Exception e){

            }

        }


    }
}