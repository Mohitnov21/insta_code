package com.abacusdesk.instafeed.instafeed.model;

import com.google.gson.Gson;

import java.util.List;

/**
 * Created by ishaan on 7/4/2017.
 */

public class User_model {


    /**
     * status : 200
     * message : success
     * data : [{"id":"857","username":"sanuj8860","first_name":"Anuj","middle_name":null,"last_name":"Sharma",
     * "avatar":"/var/www/html/api/img/default-avatar.jpg"},
     * {"id":"44","username":"iycpooja","first_name":"Mily","middle_name":null,"last_name":"Sg","avatar":"/var/www/html/api/img/default-avatar.jpg"},{"id":"2728","username":"gurjaravi30","first_name":"Ajayveer","middle_name":null,"last_name":"Baisla","avatar":"/var/www/html/api/img/default-avatar.jpg"},{"id":"2867","username":"bharatjoshi28796","first_name":"Bharat","middle_name":null,"last_name":"Joshi","avatar":"/var/www/html/api/img/default-avatar.jpg"},{"id":"2004","username":"virajkumar607","first_name":"Suraj","middle_name":null,"last_name":"Kumar","avatar":"/var/www/html/api/img/default-avatar.jpg"},{"id":"2908","username":"princeknojia","first_name":"Prince","middle_name":null,"last_name":"Knojia","avatar":"/var/www/html/api/img/default-avatar.jpg"},{"id":"1035","username":"neerajbalhara94","first_name":"Neeraj","middle_name":null,"last_name":"Balhara","avatar":"/var/www/html/api/img/default-avatar.jpg"},{"id":"3163","username":"rajansapra007","first_name":"RAJAN","middle_name":null,"last_name":"Sapra","avatar":"/var/www/html/api/img/default-avatar.jpg"},{"id":"178","username":"sahiljn1998","first_name":"Sahil","middle_name":null,"last_name":"Jain","avatar":"/var/www/html/api/img/default-avatar.jpg"},{"id":"2986","username":"anejasaurabh44","first_name":"Saurabh","middle_name":null,"last_name":"Aneja","avatar":"/var/www/html/api/img/default-avatar.jpg"},{"id":"2592","username":"nyjilwillis003","first_name":"Nyjil","middle_name":null,"last_name":"William","avatar":"/var/www/html/api/img/default-avatar.jpg"},{"id":"2074","username":"rohan18031998g","first_name":"Rakesj","middle_name":null,"last_name":"Rathore","avatar":"/var/www/html/api/img/default-avatar.jpg"},{"id":"2111","username":"baljeet501","first_name":"BALJEET","middle_name":null,"last_name":"SINGH","avatar":"/var/www/html/api/img/default-avatar.jpg"},{"id":"693","username":"kumarinderjeet80","first_name":"Inderjeet","middle_name":null,"last_name":"Kumar","avatar":"/var/www/html/api/img/default-avatar.jpg"},{"id":"1427","username":"kumar9997ajay","first_name":"Ajay","middle_name":null,"last_name":"Kumar","avatar":"/var/www/html/api/img/default-avatar.jpg"}]
     * type : users
     */

    private int status;
    private String message;
    private String type;
    private List<DataBean> data;

    public static User_model objectFromData(String str) {

        return new Gson().fromJson(str, User_model.class);
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public List<DataBean> getData() {
        return data;
    }

    public void setData(List<DataBean> data) {
        this.data = data;
    }

    public static class DataBean {
        /**
         * id : 857
         * username : sanuj8860
         * first_name : Anuj
         * middle_name : null
         * last_name : Sharma
         * avatar : /var/www/html/api/img/default-avatar.jpg
         */

        private String id;
        private String username;
        private String first_name;
        private Object middle_name;
        private String last_name;
        private String avatar;

        public static DataBean objectFromData(String str) {

            return new Gson().fromJson(str, DataBean.class);
        }

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getUsername() {
            return username;
        }

        public void setUsername(String username) {
            this.username = username;
        }

        public String getFirst_name() {
            return first_name;
        }

        public void setFirst_name(String first_name) {
            this.first_name = first_name;
        }

        public Object getMiddle_name() {
            return middle_name;
        }

        public void setMiddle_name(Object middle_name) {
            this.middle_name = middle_name;
        }

        public String getLast_name() {
            return last_name;
        }

        public void setLast_name(String last_name) {
            this.last_name = last_name;
        }

        public String getAvatar() {
            return avatar;
        }

        public void setAvatar(String avatar) {
            this.avatar = avatar;
        }
    }
}
