package com.abacusdesk.instafeed.instafeed;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TextInputEditText;
import android.support.design.widget.TextInputLayout;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.abacusdesk.instafeed.instafeed.Util.Apis;
import com.abacusdesk.instafeed.instafeed.Util.CommonFunctions;
import com.abacusdesk.instafeed.instafeed.Util.SaveSharedPreference;
import com.abacusdesk.instafeed.instafeed.custom_dialog.Dialogs;
import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by ishaan on 6/7/2017.
 */

public class Signup_Activity extends Activity implements View.OnClickListener{

    //ImageView imgclose;
    TextInputEditText edfname,edlname,edpassword,edconpassword,edemail;
    Button btnsignup,btnlogin;
    String fname,lname,pssword,conpssword,email;
    TextInputLayout inputfname,inputlname,inputemail,inputpassword,inputconpassword ;
    TextView txtmobilesignup;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.register_layout);
        init();
    }

    private void init(){
        txtmobilesignup=(TextView)findViewById(R.id.txt_mobilesignup);

        inputfname=(TextInputLayout)findViewById(R.id.txtinput_fname);
        inputlname=(TextInputLayout)findViewById(R.id.txtinput_lname);
        inputemail=(TextInputLayout)findViewById(R.id.txtinput_email);
        inputpassword=(TextInputLayout)findViewById(R.id.input_password);
        inputconpassword=(TextInputLayout)findViewById(R.id.textinput_conpaswrd);

        edfname=(TextInputEditText)findViewById(R.id.ed_fname);
        edlname=(TextInputEditText)findViewById(R.id.ed_lastname);
        edpassword=(TextInputEditText)findViewById(R.id.ed_pssword);
        edconpassword=(TextInputEditText)findViewById(R.id.ed_conpassword);
        edemail=(TextInputEditText)findViewById(R.id.ed_email);

        btnsignup=(Button)findViewById(R.id.btn_signup);
        btnlogin=(Button)findViewById(R.id.btn_login);

        btnsignup.setOnClickListener(this);
        btnlogin.setOnClickListener(this);
        txtmobilesignup.setOnClickListener(this);
    }

    private boolean valid() {
        fname = inputfname.getEditText().getText().toString();
        lname = inputlname.getEditText().getText().toString();
        pssword = inputpassword.getEditText().getText().toString();
        conpssword = inputconpassword.getEditText().getText().toString();
        email = inputemail.getEditText().getText().toString();

        if (fname.equals("")) {
            edfname.setError("Enter First Name");
            return false;
        } else if (lname.equals("")) {
            edlname.setError("Enter Last Name");
            return false;
        } else if (email.equals("")) {
            edemail.setError("Enter Email Id");
            return false;
        } else if (!email.matches(Login_Activity.EMAIL_PATTERN)) {
            edemail.setError("Enter Valid Email Id");
            return false;
        } else if (pssword.equals("")) {
            edpassword.setError("Enter Password");
            return false;
        } else if (pssword.length() < 6) {
            edpassword.setError("Password Should be 6 Charecters");
            return false;
        }else if (!pssword.equals(conpssword)) {
            edconpassword.setError("Enter correct Password");
            return false;
        } else {
            return true;
        }
    }

    public void doSignup() {
        Dialogs.showProDialog(Signup_Activity.this, "Loading");
        StringRequest stringRequest = new StringRequest(Request.Method.POST, Apis.signup,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.e("log", "" + response);
                        try {
                            signupTrace(response);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        Dialogs.disDialog();
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Dialogs.disDialog();
                       // Toast.makeText(Signup_Activity.this,"", Toast.LENGTH_LONG).show();
                    }
                })

        {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> map = new HashMap<String, String>();
                map.put("first_name",fname);
                map.put("last_name",lname);
                map.put("email",email);
                map.put("password",pssword);
                map.put("confirm_password",conpssword);
                return map;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Content-Type", "application/x-www-form-urlencoded");
                return params;
            }
        };

        RequestQueue requestQueue = Volley.newRequestQueue(this);
        requestQueue.add(stringRequest);
       // AppController.getInstance().getRequestQueue().getCache().remove(key);
    }

  /*  {
            "status": 200,
            "message": "success",
            "data": {
                "user_id": 3924,
                "token": "$1$K8D8l1Ax$2jrGKRczuExYoU1oCetA4.",
                "token_updated": "2017-06-07 16:52:16",
                "token_expires": "2017-06-08 04:52:16"
         }
    }*/

    private void signupTrace(String response ) throws JSONException {
        JSONObject jsonObject= new JSONObject(response);
        if(!jsonObject.getJSONObject("data").has("error")){
            Log.e("log", "" + response);
            SaveSharedPreference.setPrefToken(Signup_Activity.this,jsonObject.getJSONObject("data").getString("token"));
            SaveSharedPreference.setUserID(Signup_Activity.this,jsonObject.getJSONObject("data").getString("user_id"));
            Toast.makeText(Signup_Activity.this,jsonObject.getString("message"), Toast.LENGTH_SHORT).show();
            startActivity(new Intent(Signup_Activity.this,NavDrawerActivity.class));
            finish();
        }
        else if (jsonObject.getJSONObject("data").has("error")){
            Dialogs.showDialog(Signup_Activity.this, jsonObject.getJSONObject("data").getString("error"));
        } else {
            Dialogs.showDialog(Signup_Activity.this, "Server Failed");
        }
    }

    @Override
    public void onClick(View v) {

         if (v==btnsignup){
            if (CommonFunctions.isConnected(Signup_Activity.this)){
                if (valid()){
                    doSignup();
                }
            }else {
                Toast.makeText(Signup_Activity.this,"Internet not available", Toast.LENGTH_SHORT).show();
            }
        }
        else if (v==btnlogin){
            Intent intent= new Intent(Signup_Activity.this,Login_Activity.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(intent);
            finish();
        }else if (v==txtmobilesignup){
            startActivity(new Intent(Signup_Activity.this,Mobile_Signup.class));
            finish();
        }
    }
}
