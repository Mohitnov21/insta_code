package com.abacusdesk.instafeed.instafeed.fragments;

import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.abacusdesk.instafeed.instafeed.NavDrawerActivity;
import com.abacusdesk.instafeed.instafeed.R;
import com.abacusdesk.instafeed.instafeed.Util.Apis;
import com.abacusdesk.instafeed.instafeed.Util.SaveSharedPreference;
import com.abacusdesk.instafeed.instafeed.adapter.Bookmark_adapter;
import com.abacusdesk.instafeed.instafeed.custom_dialog.Dialogs;
import com.abacusdesk.instafeed.instafeed.model.NewsModel;
import com.abacusdesk.instafeed.instafeed.webservice.VolleySingleton;
import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.google.gson.Gson;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by abacusdesk on 2017-11-09.
 */

public class Bookmark_fragment extends Fragment{

    View view;
    RecyclerView listrecycler;

    NewsModel newsModel;
    ArrayList<NewsModel.DataBean> newsDataBeanList=new ArrayList<>();
    Bookmark_adapter adapter;

    public Bookmark_fragment(){

    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.bookmarks_all, container, false);
        init();
        return view;
    }

    private void init(){
        listrecycler=(RecyclerView)view.findViewById(R.id.recycler_bookmarks);
        if (NavDrawerActivity.toolbar!=null){
            NavDrawerActivity.toolbar.setLogo(R.drawable.logo_black);
        }
        setRecyclerView();
        if (isAdded()){
            getall_bookmarks();
        }
    }

    public void getall_bookmarks() {
        //Dialogs.showProDialog(getActivity(), "Loading");
        StringRequest stringRequest = new StringRequest(Request.Method.POST, Apis.news_allbookmark,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.e("log", "" + response);
                        getResponse(response);
                       // Dialogs.disDialog();
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Dialogs.disDialog();
                        Toast.makeText(getActivity(),"Internet not available", Toast.LENGTH_SHORT).show();
                    }
                })
        {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                HashMap<String,String> hashMap=new HashMap<String, String>();
                hashMap.put("user", SaveSharedPreference.getUserID(getContext()));
                hashMap.put("token",SaveSharedPreference.getPrefToken(getContext()));
                return hashMap;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Content-Type", "application/x-www-form-urlencoded");
                return params;
            }
        };
        VolleySingleton.getInstance(getActivity()).addToRequestQueue(stringRequest);
    }

    private void getResponse(String response )  {
        if (!newsDataBeanList.isEmpty()){
            newsDataBeanList.clear();
        }
        if(!response.equals(null)){
            Log.e("log", "" + response);
            Gson gson = new Gson();
            newsModel = gson.fromJson(response, NewsModel.class);
            int status = newsModel.getStatus();
            String msg = newsModel.getMessage();
            if (msg.equalsIgnoreCase("success") && status==200) {
                if (newsModel.getData()!=null && !newsModel.getData().isEmpty()){
                    for (int i = 0; i < newsModel.getData().size(); i++) {
                        try {
                            NewsModel.DataBean dataBean = newsModel.getData().get(i);
                            newsDataBeanList.add(dataBean);
                        } catch (Exception e){
                            e.printStackTrace();
                        }
                    }
                }
                adapter.notifyDataSetChanged();
            }
        }
    }
    // Setting recycler view
    private void setRecyclerView() {
        try{
            adapter= new Bookmark_adapter(getActivity(),newsDataBeanList);
        }  catch (Exception e){
            e.printStackTrace();
        }

        listrecycler.setHasFixedSize(true);
        LinearLayoutManager layoutManager=new LinearLayoutManager(getActivity());
        listrecycler.setLayoutManager(layoutManager);
        listrecycler.setNestedScrollingEnabled(false);
        //recyclerView.addItemDecoration(new SpacesItemDecoration(0));
        layoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        listrecycler.setAdapter(adapter);
    }

}
