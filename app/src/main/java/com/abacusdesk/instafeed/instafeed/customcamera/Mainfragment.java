package com.abacusdesk.instafeed.instafeed.customcamera;

import android.content.Context;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;


import com.abacusdesk.instafeed.instafeed.R;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by abacusdesk on 2017-12-18.
 */

public class Mainfragment extends Fragment {

    View view;
    ImageView imageView,imagecheck;
    String image="";
    RecyclerView recyclerView;
    FrameLayout framecheck;
    ArrayList<HashMap<String,String>> arrayList;
    ViewPager viewpage_images;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view=inflater.inflate(R.layout.mainfragment,null,false);
        view.setFocusableInTouchMode(true);
        view.requestFocus();
        view.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if (keyCode == KeyEvent.KEYCODE_BACK) {
                    getActivity().getSupportFragmentManager().popBackStack();
                    CameraDemoActivity.relativeLayout.setVisibility(View.VISIBLE);
                    return true;
                } else {
                    return false;
                }
            }
        });
       init();
       return view;
    }

    private void init(){
        arrayList=new ArrayList<>();
        Bundle bundle=getArguments();
        if (bundle!=null){
            image=bundle.getString("image");
            if (bundle.containsKey("array")){
               arrayList= (ArrayList<HashMap<String,String>>)
                       bundle.getSerializable("array");
            }
        }

        viewpage_images=(ViewPager)view.findViewById(R.id.viewpage_images);
        setviewpager();
        recyclerView=(RecyclerView)view.findViewById(R.id.recycler_selected);
        setRecyclerView();
        imageView=(ImageView)view.findViewById(R.id.imagview);
        imagecheck=(ImageView)view.findViewById(R.id.image_check);

        if (image!=null && !image.isEmpty()){

            Log.e("imagehere",""+image);

            if (image.length()<100){
                Log.e("imagehere1",""+image);
                imageView.setImageURI(Uri.parse(image));
            }/*else {
                Log.e("imagehere2",""+image);
                Bitmap bitmap=ImageAdapter.StringToBitMap(image);
                imageView.setImageBitmap(bitmap);
            }*/

           /* if (ImageAdapter.StringToBitMap(image)!=null){
                imageView.setImageBitmap
                        (ImageAdapter.StringToBitMap(image));
            }else {
                imageView.setImageURI(Uri.parse(image));
            }*/
            imageView.setVisibility(View.VISIBLE);
        }else {
            //imageView.setImageURI(Uri.parse(arrayList.get(0)));
            imageView.setVisibility(View.GONE);
            viewpage_images.setVisibility(View.VISIBLE);
        }
        imagecheck.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
              /*  AppCompatActivity activity = (AppCompatActivity)view.getContext();
                activity.getSupportFragmentManager().beginTransaction()
                        .add(R.id.surfaceView,,"mainfragment")
                        .addToBackStack("mainfragment").commit();*/

            }
        });
    }

    private void setviewpager(){
    viewpage_images.setAdapter(new SlidAdapter(getContext(),arrayList));
    viewpage_images.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
               //comm.respond(""+position);
            }

            @Override
            public void onPageSelected(int position) {

            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });
    }

    private void setRecyclerView(){
        recyclerView.setLayoutManager
                (new LinearLayoutManager
                        (getContext(), LinearLayoutManager.HORIZONTAL, false));
        recyclerView.setAdapter(new SelectedImagesAdapter(getActivity(),arrayList));
        recyclerView.addOnItemTouchListener
                (new RecyclerItemClickListener(getContext(), recyclerView,
                        new RecyclerItemClickListener.OnItemClickListener() {
                            @Override
                            public void onItemClick(View view, int position) {
                                viewpage_images.setCurrentItem(position);
                            }

                            @Override
                            public void onItemLongClick(View view, int position) {

                   }
             }));
    }

}
