package com.abacusdesk.instafeed.instafeed.model;

import com.google.gson.Gson;

import java.util.List;

/**
 * Created by ishaan on 7/6/2017.
 */

public class Newsslider_model {

    /**
     * status : 200
     * message : success
     * data : [{"id":"1069","news_category_id":"11","title":"rest","slug":"rest-1499315334","short_description":"testvhbajabmbsufcs","location_id":"3","total_likes":null,"total_dislikes":null,"total_comments":null,"total_views":"21","total_flags":null,"dt_added":"2017-07-06 09:58:54","dt_modified":"2017-07-06 09:58:54","status":"P","video":"http://instafeed.org/storage/news_mime/1499315334-VID_20170706_095423.mp4","video_thumb":"http://instafeed.org/storage/news_mime/256x170-1499315334-VID_20170706_095423.jpg","user_id":"31","first_name":"Pooja","last_name":"Sg","nickname":"Pooja","avatar":"http://instafeed.org/storage/avatar/80x80-e4a1302befaa3249e58242b348176ad4.jpg","username":"Poojasg","source":"m","latitude":"28.36397013","longitude":"77.32187221"},{"id":"1068","news_category_id":"11","title":"bramela city center test\n","slug":"bramela-city-center-test\n-1499293626","short_description":"again plz test for video \nthis is in Brampton.\n","location_id":"3","total_likes":null,"total_dislikes":null,"total_comments":null,"total_views":"11","total_flags":"0","dt_added":"2017-07-06 03:57:06","dt_modified":"2017-07-06 03:57:06","status":"P","video":"http://instafeed.org/storage/news_mime/1499293626-VID_20170705_145828.mp4","video_thumb":"http://instafeed.org/storage/news_mime/256x170-1499293626-VID_20170705_145828.jpg","user_id":"32","first_name":"Vikas","last_name":"Behl","nickname":"Vikas","avatar":null,"username":"vikasbehl","source":"m","latitude":"43.71064923","longitude":"-79.72649998"},{"id":"1036","news_category_id":"11","title":"test audio video image","slug":"test-audio-video-image-1499250211","short_description":"ggggggghh","location_id":"3","total_likes":null,"total_dislikes":null,"total_comments":null,"total_views":"4","total_flags":"0","dt_added":"2017-07-05 15:53:31","dt_modified":"2017-07-05 15:53:31","status":"P","video":"http://instafeed.org/storage/news_mime/1499250213-VID_20170705_155241.mp4","video_thumb":"http://instafeed.org/storage/news_mime/256x170-1499250213-VID_20170705_155241.jpg","user_id":"3924","first_name":"jitu","last_name":"kumar","nickname":"","avatar":null,"username":"jkg","source":"m","latitude":"28.35934492","longitude":"77.28472324"},{"id":"1032","news_category_id":"11","title":"accident in Faridabad Gurgoan Highway","slug":"accident-in-faridabad-gurgoan-highway-1499248247","short_description":"accident spoted at Faridabad gurgoan highway with Beers","location_id":"3","total_likes":null,"total_dislikes":null,"total_comments":null,"total_views":"17","total_flags":"0","dt_added":"2017-07-05 15:20:47","dt_modified":"2017-07-05 15:20:47","status":"P","video":"http://instafeed.org/storage/news_mime/1499248247-VID_20170705_151755.mp4","video_thumb":"http://instafeed.org/storage/news_mime/256x170-1499248247-VID_20170705_151755.jpg","user_id":"5","first_name":"Sagar","last_name":"ARORA","nickname":"Sagar","avatar":"http://instafeed.org/storage/avatar/80x80-1f069d56ecce23433d4126aaf7b8719f.jpg","username":"segitips4u","source":"m","latitude":"28.3635714","longitude":"77.2853661"},{"id":"965","news_category_id":"11","title":"test video","slug":"test-video-1499194288","short_description":"test video","location_id":"3","total_likes":null,"total_dislikes":null,"total_comments":null,"total_views":"3","total_flags":"0","dt_added":"2017-07-05 00:21:28","dt_modified":"2017-07-05 00:21:28","status":"P","video":"http://instafeed.org/storage/news_mime/1499194288-VID_20170705_002036.mp4","video_thumb":"http://instafeed.org/storage/news_mime/256x170-1499194288-VID_20170705_002036.jpg","user_id":"43","first_name":"Gautam","last_name":"Rampal","nickname":"Gautam","avatar":"http://instafeed.org/storage/avatar/80x80-8fe219e748348362e968e62b0689dc82.jpg","username":"gautamrampal219","source":"m","latitude":"28.35929126","longitude":"77.28481713"},{"id":"901","news_category_id":"11","title":"fbzjz","slug":"fbzjz-1499151239","short_description":"vgBvhzjjsknBhjzbns","location_id":"3","total_likes":null,"total_dislikes":null,"total_comments":null,"total_views":"3","total_flags":"0","dt_added":"2017-07-04 12:23:59","dt_modified":"2017-07-04 12:23:59","status":"P","video":"http://instafeed.org/storage/news_mime/1499151239-VID_20170704_121732.mp4","video_thumb":"http://instafeed.org/storage/news_mime/256x170-1499151239-VID_20170704_121732.jpg","user_id":"31","first_name":"Pooja","last_name":"Sg","nickname":"Pooja","avatar":"http://instafeed.org/storage/avatar/80x80-e4a1302befaa3249e58242b348176ad4.jpg","username":"Poojasg","source":"m","latitude":"28.3483975","longitude":"77.3344176"}]
     */

    private int status;
    private String message;
    private List<DataBean> data;

    public static Newsslider_model objectFromData(String str) {

        return new Gson().fromJson(str, Newsslider_model.class);
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public List<DataBean> getData() {
        return data;
    }

    public void setData(List<DataBean> data) {
        this.data = data;
    }

    public static class DataBean {
        /**
         * id : 1069
         * news_category_id : 11
         * title : rest
         * slug : rest-1499315334
         * short_description : testvhbajabmbsufcs
         * location_id : 3
         * total_likes : null
         * total_dislikes : null
         * total_comments : null
         * total_views : 21
         * total_flags : null
         * dt_added : 2017-07-06 09:58:54
         * dt_modified : 2017-07-06 09:58:54
         * status : P
         * video : http://instafeed.org/storage/news_mime/1499315334-VID_20170706_095423.mp4
         * video_thumb : http://instafeed.org/storage/news_mime/256x170-1499315334-VID_20170706_095423.jpg
         * user_id : 31
         * first_name : Pooja
         * last_name : Sg
         * nickname : Pooja
         * avatar : http://instafeed.org/storage/avatar/80x80-e4a1302befaa3249e58242b348176ad4.jpg
         * username : Poojasg
         * source : m
         * latitude : 28.36397013
         * longitude : 77.32187221
         */

        private String id;
        private String news_category_id;
        private String title;
        private String slug;
        private String short_description;
        private String location_id;
        private Object total_likes;
        private Object total_dislikes;
        private Object total_comments;
        private String total_views;
        private Object total_flags;
        private String dt_added;
        private String dt_modified;
        private String status;
        private String video;
        private String video_thumb;
        private String user_id;
        private String first_name;
        private String last_name;
        private String nickname;
        private String avatar;
        private String username;
        private String source;
        private String latitude;
        private String longitude;

        public static DataBean objectFromData(String str) {

            return new Gson().fromJson(str, DataBean.class);
        }

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getNews_category_id() {
            return news_category_id;
        }

        public void setNews_category_id(String news_category_id) {
            this.news_category_id = news_category_id;
        }

        public String getTitle() {
            return title;
        }

        public void setTitle(String title) {
            this.title = title;
        }

        public String getSlug() {
            return slug;
        }

        public void setSlug(String slug) {
            this.slug = slug;
        }

        public String getShort_description() {
            return short_description;
        }

        public void setShort_description(String short_description) {
            this.short_description = short_description;
        }

        public String getLocation_id() {
            return location_id;
        }

        public void setLocation_id(String location_id) {
            this.location_id = location_id;
        }

        public Object getTotal_likes() {
            return total_likes;
        }

        public void setTotal_likes(Object total_likes) {
            this.total_likes = total_likes;
        }

        public Object getTotal_dislikes() {
            return total_dislikes;
        }

        public void setTotal_dislikes(Object total_dislikes) {
            this.total_dislikes = total_dislikes;
        }

        public Object getTotal_comments() {
            return total_comments;
        }

        public void setTotal_comments(Object total_comments) {
            this.total_comments = total_comments;
        }

        public String getTotal_views() {
            return total_views;
        }

        public void setTotal_views(String total_views) {
            this.total_views = total_views;
        }

        public Object getTotal_flags() {
            return total_flags;
        }

        public void setTotal_flags(Object total_flags) {
            this.total_flags = total_flags;
        }

        public String getDt_added() {
            return dt_added;
        }

        public void setDt_added(String dt_added) {
            this.dt_added = dt_added;
        }

        public String getDt_modified() {
            return dt_modified;
        }

        public void setDt_modified(String dt_modified) {
            this.dt_modified = dt_modified;
        }

        public String getStatus() {
            return status;
        }

        public void setStatus(String status) {
            this.status = status;
        }

        public String getVideo() {
            return video;
        }

        public void setVideo(String video) {
            this.video = video;
        }

        public String getVideo_thumb() {
            return video_thumb;
        }

        public void setVideo_thumb(String video_thumb) {
            this.video_thumb = video_thumb;
        }

        public String getUser_id() {
            return user_id;
        }

        public void setUser_id(String user_id) {
            this.user_id = user_id;
        }

        public String getFirst_name() {
            return first_name;
        }

        public void setFirst_name(String first_name) {
            this.first_name = first_name;
        }

        public String getLast_name() {
            return last_name;
        }

        public void setLast_name(String last_name) {
            this.last_name = last_name;
        }

        public String getNickname() {
            return nickname;
        }

        public void setNickname(String nickname) {
            this.nickname = nickname;
        }

        public String getAvatar() {
            return avatar;
        }

        public void setAvatar(String avatar) {
            this.avatar = avatar;
        }

        public String getUsername() {
            return username;
        }

        public void setUsername(String username) {
            this.username = username;
        }

        public String getSource() {
            return source;
        }

        public void setSource(String source) {
            this.source = source;
        }

        public String getLatitude() {
            return latitude;
        }

        public void setLatitude(String latitude) {
            this.latitude = latitude;
        }

        public String getLongitude() {
            return longitude;
        }

        public void setLongitude(String longitude) {
            this.longitude = longitude;
        }
    }
}
