package com.abacusdesk.instafeed.instafeed.model;

import java.util.List;

/**
 * Created by abacusdesk on 2017-07-24.
 */

public class Talent_ProfileModel {

    /**
     * status : 200
     * message : success
     * data : [{"id":"13","talent_category_id":"4","title":"talent testing","slug":"talent-testing-1296545164","short_description":"talent test succesfull talent test succesfull talent test succesfull","location_id":null,"total_likes":null,"total_dislikes":null,"total_comments":null,"total_views":"27","total_flags":null,"user_id":"3924","dt_added":"2017-07-11 17:25:39","dt_modified":"2017-07-11 17:25:39","n_lat":null,"n_long":null,"source":"w","status":"P"}]
     */

    private int status;
    private String message;
    private List<DataBean> data;

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public List<DataBean> getData() {
        return data;
    }

    public void setData(List<DataBean> data) {
        this.data = data;
    }

    public static class DataBean {

        /**
         * id : 13
         * talent_category_id : 4
         * title : talent testing
         * slug : talent-testing-1296545164
         * short_description : talent test succesfull talent test succesfull talent test succesfull
         * location_id : null
         * total_likes : null
         * total_dislikes : null
         * total_comments : null
         * total_views : 27
         * total_flags : null
         * user_id : 3924
         * dt_added : 2017-07-11 17:25:39
         * dt_modified : 2017-07-11 17:25:39
         * n_lat : null
         * n_long : null
         * source : w
         * status : P
         */

        private String id;
        private String talent_category_id;
        private String title;
        private String slug;
        private String short_description;
        private Object location_id;
        private Object total_likes;
        private Object total_dislikes;
        private Object total_comments;
        private String total_views;
        private Object total_flags;
        private String user_id;
        private String dt_added;
        private String dt_modified;
        private Object n_lat;
        private Object n_long;
        private String source;
        private String status;

        public String getImage() {
            return image;
        }

        public void setImage(String image) {
            this.image = image;
        }

        private String image;

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getTalent_category_id() {
            return talent_category_id;
        }

        public void setTalent_category_id(String talent_category_id) {
            this.talent_category_id = talent_category_id;
        }

        public String getTitle() {
            return title;
        }

        public void setTitle(String title) {
            this.title = title;
        }

        public String getSlug() {
            return slug;
        }

        public void setSlug(String slug) {
            this.slug = slug;
        }

        public String getShort_description() {
            return short_description;
        }

        public void setShort_description(String short_description) {
            this.short_description = short_description;
        }

        public Object getLocation_id() {
            return location_id;
        }

        public void setLocation_id(Object location_id) {
            this.location_id = location_id;
        }

        public Object getTotal_likes() {
            return total_likes;
        }

        public void setTotal_likes(Object total_likes) {
            this.total_likes = total_likes;
        }

        public Object getTotal_dislikes() {
            return total_dislikes;
        }

        public void setTotal_dislikes(Object total_dislikes) {
            this.total_dislikes = total_dislikes;
        }

        public Object getTotal_comments() {
            return total_comments;
        }

        public void setTotal_comments(Object total_comments) {
            this.total_comments = total_comments;
        }

        public String getTotal_views() {
            return total_views;
        }

        public void setTotal_views(String total_views) {
            this.total_views = total_views;
        }

        public Object getTotal_flags() {
            return total_flags;
        }

        public void setTotal_flags(Object total_flags) {
            this.total_flags = total_flags;
        }

        public String getUser_id() {
            return user_id;
        }

        public void setUser_id(String user_id) {
            this.user_id = user_id;
        }

        public String getDt_added() {
            return dt_added;
        }

        public void setDt_added(String dt_added) {
            this.dt_added = dt_added;
        }

        public String getDt_modified() {
            return dt_modified;
        }

        public void setDt_modified(String dt_modified) {
            this.dt_modified = dt_modified;
        }

        public Object getN_lat() {
            return n_lat;
        }

        public void setN_lat(Object n_lat) {
            this.n_lat = n_lat;
        }

        public Object getN_long() {
            return n_long;
        }

        public void setN_long(Object n_long) {
            this.n_long = n_long;
        }

        public String getSource() {
            return source;
        }

        public void setSource(String source) {
            this.source = source;
        }

        public String getStatus() {
            return status;
        }

        public void setStatus(String status) {
            this.status = status;
        }
    }
}
