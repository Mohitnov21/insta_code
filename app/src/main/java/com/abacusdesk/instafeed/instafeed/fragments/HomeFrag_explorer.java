package com.abacusdesk.instafeed.instafeed.fragments;

import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.Toast;

import com.abacusdesk.instafeed.instafeed.R;
import com.abacusdesk.instafeed.instafeed.Util.Alerts;
import com.abacusdesk.instafeed.instafeed.Util.Apis;
import com.abacusdesk.instafeed.instafeed.Util.AppSingleton;
import com.abacusdesk.instafeed.instafeed.Util.CommonFunctions;
import com.abacusdesk.instafeed.instafeed.Util.SaveSharedPreference;
import com.abacusdesk.instafeed.instafeed.adapter.ExploreAll_adapter;
import com.abacusdesk.instafeed.instafeed.adapter.HomeNews_Adapter;
import com.abacusdesk.instafeed.instafeed.adapter.Talent_allAdapter;
import com.abacusdesk.instafeed.instafeed.custom_dialog.Dialogs;
import com.abacusdesk.instafeed.instafeed.model.Exploreall_model;
import com.android.volley.AuthFailureError;
import com.android.volley.NoConnectionError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.gson.Gson;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;


/**
 * Created by ishaan on 7/7/2017.
 */

public class HomeFrag_explorer extends Fragment implements SwipeRefreshLayout.OnRefreshListener{

    private View view;
    Exploreall_model exploreall_model;
    private  RecyclerView recyclerView;
    ArrayList<Exploreall_model.DataBean> exploredatbean=new ArrayList<>();
    ExploreAll_adapter adapter;
    public static String catid="",postid="";
    SwipeRefreshLayout swipeRefreshLayout;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_news, container, false);
        recyclerView = (RecyclerView) view.findViewById(R.id.recyclerView);
        swipeRefreshLayout = (SwipeRefreshLayout)view.findViewById(R.id.swipe_refresh_layout);
        swipeRefreshLayout.setOnRefreshListener(this);

        setRecyclerView();
        if (CommonFunctions.isConnected(getActivity())){
            getExploreList();
        } else {
           // Toast.makeText(getActivity(), "Internet not available", Toast.LENGTH_SHORT).show();
        }
        view.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if (keyCode == KeyEvent.KEYCODE_BACK) {

                    getActivity().getSupportFragmentManager().popBackStack();
                    return true;
                } else {
                    return false;
                }
            }
        });
        return view;
    }

    @Override
    public void onRefresh() {
        swipeRefreshLayout.setRefreshing(true);
        try {
            if (CommonFunctions.isConnected(getActivity())){
                getExploreList();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void getExploreList() {
        String api="";
        if (!SaveSharedPreference.getUserID(getContext()).isEmpty()){
            api=Apis.explore_all+"/"+SaveSharedPreference.getUserID(getContext());
            Log.e("userid api","called");
        }else {
            api=Apis.explore_all;
            Log.e("without userid api","called");
        }
        StringRequest stringRequest = new StringRequest(Request.Method.GET,api,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                       // Log.e("log", "" + response);
                        try {
                            getResponse(response);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {

                    }
                })
        {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Content-Type", "application/x-www-form-urlencoded");
                return params;
            }
        };

        RequestQueue requestQueue = Volley.newRequestQueue(getActivity());
        requestQueue.add(stringRequest);
    }

    public void refresh_Adapter(){
        Log.e("change refresh","called nav");
        //adapter.notifyDataSetChanged();
        getExploreList();
    }

    private void getResponse(String response ) throws JSONException {
        if(!response.equals(null)){
            if (!exploredatbean.isEmpty()){
                exploredatbean.clear();
            }
            swipeRefreshLayout.setRefreshing(false);
          //  Log.e("log", "" + response);
            JSONObject jsonObject = new JSONObject(response);
            Object object = jsonObject.get("data");
            if (object instanceof JSONArray) {
                Gson gson = new Gson();
                exploreall_model = gson.fromJson(response, Exploreall_model.class);
                int status = exploreall_model.getStatus();
                String msg = exploreall_model.getMessage();
                if (msg.equalsIgnoreCase("success") && status == 200) {
                    if (exploreall_model.getData() != null && !exploreall_model.getData().isEmpty()) {
                        for (int i = 0; i < exploreall_model.getData().size(); i++) {
                            try {
                                Exploreall_model.DataBean dataBean = exploreall_model.getData().get(i);
                                exploredatbean.add(dataBean);
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }
                    }
                    adapter.notifyDataSetChanged();
                }
            }

        }
    }

    private void setRecyclerView() {
        try{
            adapter= new ExploreAll_adapter(getActivity(),exploredatbean);
        }  catch (Exception e){
            e.printStackTrace();
        }

        recyclerView.setHasFixedSize(true);
        LinearLayoutManager layoutManager=new LinearLayoutManager(getActivity());
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setItemViewCacheSize(20);
        recyclerView.setDrawingCacheEnabled(true);
        layoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        recyclerView.setAdapter(adapter);
    }
}
