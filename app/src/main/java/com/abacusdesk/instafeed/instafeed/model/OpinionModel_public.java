package com.abacusdesk.instafeed.instafeed.model;

import com.google.gson.Gson;

import java.util.List;

/**
 * Created by ishaan on 6/29/2017.
 */

public class OpinionModel_public {


    /**
     * status : 200
     * message : success
     * data : [{"id":"73","title":"text","description":"Lorem Ipsum is simply dummy text of the printing and typesetting industry\r\nLorem Ipsum is simply dummy text of the printing and typesetting industry\r\nLorem Ipsum is simply dummy text of the printing and typesetting industry","location_id":null,"total_up_votes":null,"total_down_votes":null,"total_comments":null,"total_views":"0","total_flags":null,"slug":"IN00035","user_id":"3924","dt_added":"2017-06-28 17:14:31","dt_modified":"2017-06-28 17:14:31","status":"A"},{"id":"72","title":"opinion test","description":"Lorem Ipsum is simply dummy text of the printing and typesetting industry\r\nLorem Ipsum is simply dummy text of the printing and typesetting industry\r\nLorem Ipsum is simply dummy text of the printing and typesetting industry","location_id":null,"total_up_votes":null,"total_down_votes":null,"total_comments":null,"total_views":"0","total_flags":null,"slug":"IN00034","user_id":"3924","dt_added":"2017-06-28 17:12:35","dt_modified":"2017-06-28 17:12:35","status":"A"}]
     */

    private int status;
    private String message;
    private List<DataBean> data;

    public static OpinionModel_public objectFromData(String str) {

        return new Gson().fromJson(str, OpinionModel_public.class);
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public List<DataBean> getData() {
        return data;
    }

    public void setData(List<DataBean> data) {
        this.data = data;
    }

    public static class DataBean {
        /**
         * id : 73
         * title : text
         * description : Lorem Ipsum is simply dummy text of the printing and typesetting industry
         Lorem Ipsum is simply dummy text of the printing and typesetting industry
         Lorem Ipsum is simply dummy text of the printing and typesetting industry
         * location_id : null
         * total_up_votes : null
         * total_down_votes : null
         * total_comments : null
         * total_views : 0
         * total_flags : null
         * slug : IN00035
         * user_id : 3924
         * dt_added : 2017-06-28 17:14:31
         * dt_modified : 2017-06-28 17:14:31
         * status : A
         */

        private String id;
        private String title;
        private String description;
        private Object location_id;
        private Object total_up_votes;
        private Object total_down_votes;
        private Object total_comments;
        private String total_views;
        private Object total_flags;
        private String slug;
        private String user_id;
        private String dt_added;
        private String dt_modified;
        private String status;
        private String image;
        public static DataBean objectFromData(String str) {

            return new Gson().fromJson(str, DataBean.class);
        }

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getTitle() {
            return title;
        }

        public void setTitle(String title) {
            this.title = title;
        }

        public String getDescription() {
            return description;
        }

        public void setDescription(String description) {
            this.description = description;
        }

        public Object getLocation_id() {
            return location_id;
        }

        public void setLocation_id(Object location_id) {
            this.location_id = location_id;
        }

        public Object getTotal_up_votes() {
            return total_up_votes;
        }

        public void setTotal_up_votes(Object total_up_votes) {
            this.total_up_votes = total_up_votes;
        }

        public Object getTotal_down_votes() {
            return total_down_votes;
        }

        public void setTotal_down_votes(Object total_down_votes) {
            this.total_down_votes = total_down_votes;
        }

        public Object getTotal_comments() {
            return total_comments;
        }

        public void setTotal_comments(Object total_comments) {
            this.total_comments = total_comments;
        }

        public String getTotal_views() {
            return total_views;
        }

        public void setTotal_views(String total_views) {
            this.total_views = total_views;
        }

        public Object getTotal_flags() {
            return total_flags;
        }

        public void setTotal_flags(Object total_flags) {
            this.total_flags = total_flags;
        }

        public String getSlug() {
            return slug;
        }

        public void setSlug(String slug) {
            this.slug = slug;
        }

        public String getUser_id() {
            return user_id;
        }

        public void setUser_id(String user_id) {
            this.user_id = user_id;
        }

        public String getDt_added() {
            return dt_added;
        }

        public void setDt_added(String dt_added) {
            this.dt_added = dt_added;
        }

        public String getDt_modified() {
            return dt_modified;
        }

        public void setDt_modified(String dt_modified) {
            this.dt_modified = dt_modified;
        }

        public String getStatus() {
            return status;
        }

        public void setStatus(String status) {
            this.status = status;
        }

        public String getImage() {
            return image;
        }

        public void setImage(String image) {
            this.image = image;
        }
    }
}
