package com.abacusdesk.instafeed.instafeed.adapter;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Bitmap;
import android.media.ThumbnailUtils;
import android.os.Handler;
import android.provider.MediaStore;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.abacusdesk.instafeed.instafeed.Login_Activity;
import com.abacusdesk.instafeed.instafeed.R;
import com.abacusdesk.instafeed.instafeed.SplashActivity;
import com.abacusdesk.instafeed.instafeed.Util.CommonFunctions;
import com.abacusdesk.instafeed.instafeed.Util.DbHandler;
import com.abacusdesk.instafeed.instafeed.Util.MasterTblModel;
import com.abacusdesk.instafeed.instafeed.model.GridModel;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.resource.drawable.GlideDrawable;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import static com.abacusdesk.instafeed.instafeed.SplashActivity.SPLASH_TIME_OUT;
import static com.abacusdesk.instafeed.instafeed.Util.ScreenConstants.MEDIA_TYPE_AUDIO;
import static com.abacusdesk.instafeed.instafeed.Util.ScreenConstants.MEDIA_TYPE_IMAGE;
import static com.abacusdesk.instafeed.instafeed.Util.ScreenConstants.MEDIA_TYPE_VIDEO;
import static com.abacusdesk.instafeed.instafeed.fragments.SaveOfflineFragment.saveOfflineAdapter;

/**
 * Created by Administrator on 6/26/2017.
 */

public class SaveOfflineAdapter extends RecyclerView.Adapter<SaveOfflineAdapter.SaveOfflineViewHolder> {
    ArrayList<MasterTblModel> masterTableList;
    HashMap<String, List<GridModel>> mapList;
    DbHandler dbHandler;
    ProgressDialog progressDialog;

    Activity activity;

    public SaveOfflineAdapter(Activity activity, ArrayList<MasterTblModel> masterTableList, HashMap<String, List<GridModel>> mapList) {
        this.activity = activity;
        progressDialog = CommonFunctions.showDialog(activity);
        dbHandler = new DbHandler(activity);
        this.masterTableList = masterTableList;
        this.mapList = mapList;
    }

    @Override
    public SaveOfflineViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.row_show_offline,
                viewGroup, false);
        return new SaveOfflineViewHolder(view);
    }

    @Override
    public void onBindViewHolder(SaveOfflineViewHolder myViewHolder, final int position) {
        final MasterTblModel masterTblModel = masterTableList.get(position);
        try {
            List<GridModel> gridList = mapList.get(masterTblModel.getMasterPrimaryKey());

            if(gridList.size()>0) {
                GridModel gridModel = gridList.get(0);
                if (gridModel.getFileType().equals(MEDIA_TYPE_IMAGE)) {
                    Glide
                            .with(activity)
                            .load(gridModel.getFilePath())
                            .error(R.drawable.demo_news)
                            .into((myViewHolder.img_post));

                } else if (gridModel.getFileType().equals(MEDIA_TYPE_VIDEO)) {
                    Bitmap bMap = ThumbnailUtils.createVideoThumbnail(gridModel.getFilePath(), MediaStore.Video.Thumbnails.MICRO_KIND);
                    Bitmap bitmap1 = bMap.createScaledBitmap(bMap, 100, 100, true);
                    myViewHolder.img_post.setImageBitmap(bitmap1);
                } else if (gridModel.getFileType().equals(MEDIA_TYPE_AUDIO)) {
                    myViewHolder.img_post.setImageResource(R.drawable.music_player_plus);
                } else {

                }
            }
        }catch (Exception e){
            e.printStackTrace();
        }

        myViewHolder.img_delete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                progressDialog.setMessage("Deleting Offline Post...");
                progressDialog.show();
                try {
                    dbHandler.open();
                    dbHandler.delete_masterRowData(masterTblModel.getMasterPrimaryKey());
                    dbHandler.delete_postData(masterTblModel.getMasterPrimaryKey());
                    masterTableList.remove(position);

                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            notifyItemChanged(position);
                            progressDialog.dismiss();
                        }
                    }, SPLASH_TIME_OUT);
                }catch (Exception e) {
                    e.printStackTrace();
                }

            }
        });
        myViewHolder.title.setText(masterTblModel.getPostTitle());
        myViewHolder.description.setText(masterTblModel.getPostDescription());
    }


    @Override
    public int getItemCount() {
        return masterTableList == null ? 0 : masterTableList.size();
    }

    class SaveOfflineViewHolder extends RecyclerView.ViewHolder {
        ImageView img_post,img_delete;
        TextView title,description;

        public SaveOfflineViewHolder(View itemView) {
            super(itemView);
            img_post=(ImageView)itemView.findViewById(R.id.img_post);
            img_delete=(ImageView)itemView.findViewById(R.id.img_delete);
            title=(TextView)itemView.findViewById(R.id.tv_title);
            description=(TextView)itemView.findViewById(R.id.tv_description);
        }
    }
}