package com.abacusdesk.instafeed.instafeed.model;

import java.util.List;

/**
 * Created by ishaan on 6/11/2017.
 */

public class NewsDetail {

    /**
     * status : 200
     * message : success
     * data : [{"id":"1511","news_category_id":"9","name":"Lifestyle","title":"check news","slug":"check-news-2010251518","short_description":"test","description":"Pages that you view in incognito tabs won\u2019t stick around in your browser\u2019s history, cookie store or search history after you\u2019ve closed all of your incognito tabs. Any files you download or bookmarks you create will be kept.\r\n\r\nHowever, you aren\u2019t invisible. Going incognito doesn\u2019t hide your browsing from your employer, your Internet service provider or the websites that you visit.\r\nPages that you view in incognito tabs won\u2019t stick around in your browser\u2019s history, cookie store or search history after you\u2019ve closed all of your incognito tabs. Any files you download or bookmarks you create will be kept.\r\n\r\nHowever, you aren\u2019t invisible. Going incognito doesn\u2019t hide your browsing from your employer, your Internet service provider or the websites that you visit.","total_likes":null,"total_dislikes":null,"total_comments":"1","total_views":"8","total_flags":"0","dt_added":"2017-07-21 18:17:42","status":"P","user_id":"2","first_name":"Instafeed","last_name":" ","nickname":"Instafeed","avatar":"https://instafeed.org/storage/avatar/80x80-d6a43e12b07d7a6cf5c0fca593aff5dc.jpg","username":"instafeed","source":"w","latitude":null,"longitude":null,"is_anonymous":"N"},{"comments":[{"id":"126","comment":"helloo","comment_id":null,"dt_added":"2017-07-21 18:20:03","dt_modified":"2017-07-21 18:20:03","status":"A","user_id":"3905","first_name":"Abhishek","last_name":"Chauhan","nickname":"Abhishek","avatar":"https://instafeed.org/storage/avatar/80x80-3844825a5c347c3f039c127d02b9154d.jpg","username":"abhishek"}]},{"images":[{"id":"1117","image":"https://instafeed.org/storage/news_mime/256x170-1e9f16b41715a707dcdf6182a71e7809.jpg","image_zoom":"https://instafeed.org/storage/news_mime/748X420-1e9f16b41715a707dcdf6182a71e7809.jpg","image_original":"https://instafeed.org/storage/news_mime/1e9f16b41715a707dcdf6182a71e7809.jpg","image_100x100":"https://instafeed.org/storage/news_mime/100x100-1e9f16b41715a707dcdf6182a71e7809.jpg","image_256x170":"https://instafeed.org/storage/news_mime/256x170-1e9f16b41715a707dcdf6182a71e7809.jpg","image_264x200":"https://instafeed.org/storage/news_mime/264x200-1e9f16b41715a707dcdf6182a71e7809.jpg","image_360x290":"https://instafeed.org/storage/news_mime/360x290-1e9f16b41715a707dcdf6182a71e7809.jpg"}]},{"videos":[{"id":"189","video":"https://instafeed.org/storage/news_mime/0b898f315a005bebc6017c4a26d361ea.mp4","video_thumb":"https://instafeed.org/storage/news_mime/256x170-0b898f315a005bebc6017c4a26d361ea.jpg","vimeo_video_id":null,"vimeo_response":null}]},{"audio":[{"id":"99","audio":"https://instafeed.org/storage/news_mime/59820b4ef7b875ceaf5164e8b5d13d6b.mp3"}]}]
     */

    private int status;
    private String message;
    private List<DataBean> data;

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public List<DataBean> getData() {
        return data;
    }

    public void setData(List<DataBean> data) {
        this.data = data;
    }

    public static class DataBean {
        /**
         * id : 1511
         * news_category_id : 9
         * name : Lifestyle
         * title : check news
         * slug : check-news-2010251518
         * short_description : test
         * description : Pages that you view in incognito tabs won’t stick around in your browser’s history, cookie store or search history after you’ve closed all of your incognito tabs. Any files you download or bookmarks you create will be kept.

         However, you aren’t invisible. Going incognito doesn’t hide your browsing from your employer, your Internet service provider or the websites that you visit.
         Pages that you view in incognito tabs won’t stick around in your browser’s history, cookie store or search history after you’ve closed all of your incognito tabs. Any files you download or bookmarks you create will be kept.

         However, you aren’t invisible. Going incognito doesn’t hide your browsing from your employer, your Internet service provider or the websites that you visit.
         * total_likes : null
         * total_dislikes : null
         * total_comments : 1
         * total_views : 8
         * total_flags : 0
         * dt_added : 2017-07-21 18:17:42
         * status : P
         * user_id : 2
         * first_name : Instafeed
         * last_name :
         * nickname : Instafeed
         * avatar : https://instafeed.org/storage/avatar/80x80-d6a43e12b07d7a6cf5c0fca593aff5dc.jpg
         * username : instafeed
         * source : w
         * latitude : null
         * longitude : null
         * is_anonymous : N
         * comments : [{"id":"126","comment":"helloo","comment_id":null,"dt_added":"2017-07-21 18:20:03","dt_modified":"2017-07-21 18:20:03","status":"A","user_id":"3905","first_name":"Abhishek","last_name":"Chauhan","nickname":"Abhishek","avatar":"https://instafeed.org/storage/avatar/80x80-3844825a5c347c3f039c127d02b9154d.jpg","username":"abhishek"}]
         * images : [{"id":"1117","image":"https://instafeed.org/storage/news_mime/256x170-1e9f16b41715a707dcdf6182a71e7809.jpg","image_zoom":"https://instafeed.org/storage/news_mime/748X420-1e9f16b41715a707dcdf6182a71e7809.jpg","image_original":"https://instafeed.org/storage/news_mime/1e9f16b41715a707dcdf6182a71e7809.jpg","image_100x100":"https://instafeed.org/storage/news_mime/100x100-1e9f16b41715a707dcdf6182a71e7809.jpg","image_256x170":"https://instafeed.org/storage/news_mime/256x170-1e9f16b41715a707dcdf6182a71e7809.jpg","image_264x200":"https://instafeed.org/storage/news_mime/264x200-1e9f16b41715a707dcdf6182a71e7809.jpg","image_360x290":"https://instafeed.org/storage/news_mime/360x290-1e9f16b41715a707dcdf6182a71e7809.jpg"}]
         * videos : [{"id":"189","video":"https://instafeed.org/storage/news_mime/0b898f315a005bebc6017c4a26d361ea.mp4","video_thumb":"https://instafeed.org/storage/news_mime/256x170-0b898f315a005bebc6017c4a26d361ea.jpg","vimeo_video_id":null,"vimeo_response":null}]
         * audio : [{"id":"99","audio":"https://instafeed.org/storage/news_mime/59820b4ef7b875ceaf5164e8b5d13d6b.mp3"}]
         */

        private String id;
        private String news_category_id;
        private String name;
        private String title;
        private String slug;
        private String short_description;
        private String description;
        private String total_likes;
        private String total_dislikes;
        private String total_comments;
        private String total_views;
        private String total_flags;
        private String dt_added;
        private String status;
        private String user_id;
        private String first_name;
        private String last_name;
        private String nickname;
        private String avatar;

        public String getLocation() {
            return location;
        }

        public void setLocation(String location) {
            this.location = location;
        }

        private String location;
        private String username;
        private String source;
        private Object latitude;
        private Object longitude;
        private String is_anonymous;
        private List<CommentsBean> comments;
        private List<ImagesBean> images;
        private List<VideosBean> videos;
        private List<AudioBean> audio;
       // location": "Faridabad",
        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getNews_category_id() {
            return news_category_id;
        }

        public void setNews_category_id(String news_category_id) {
            this.news_category_id = news_category_id;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getTitle() {
            return title;
        }

        public void setTitle(String title) {
            this.title = title;
        }

        public String getSlug() {
            return slug;
        }

        public void setSlug(String slug) {
            this.slug = slug;
        }

        public String getShort_description() {
            return short_description;
        }

        public void setShort_description(String short_description) {
            this.short_description = short_description;
        }

        public String getDescription() {
            return description;
        }

        public void setDescription(String description) {
            this.description = description;
        }

        public String getTotal_likes() {
            return total_likes;
        }

        public void setTotal_likes(String total_likes) {
            this.total_likes = total_likes;
        }

        public String getTotal_dislikes() {
            return total_dislikes;
        }

        public void setTotal_dislikes(String total_dislikes) {
            this.total_dislikes = total_dislikes;
        }

        public String getTotal_comments() {
            return total_comments;
        }

        public void setTotal_comments(String total_comments) {
            this.total_comments = total_comments;
        }

        public String getTotal_views() {
            return total_views;
        }

        public void setTotal_views(String total_views) {
            this.total_views = total_views;
        }

        public String getTotal_flags() {
            return total_flags;
        }

        public void setTotal_flags(String total_flags) {
            this.total_flags = total_flags;
        }

        public String getDt_added() {
            return dt_added;
        }

        public void setDt_added(String dt_added) {
            this.dt_added = dt_added;
        }

        public String getStatus() {
            return status;
        }

        public void setStatus(String status) {
            this.status = status;
        }

        public String getUser_id() {
            return user_id;
        }

        public void setUser_id(String user_id) {
            this.user_id = user_id;
        }

        public String getFirst_name() {
            return first_name;
        }

        public void setFirst_name(String first_name) {
            this.first_name = first_name;
        }

        public String getLast_name() {
            return last_name;
        }

        public void setLast_name(String last_name) {
            this.last_name = last_name;
        }

        public String getNickname() {
            return nickname;
        }

        public void setNickname(String nickname) {
            this.nickname = nickname;
        }

        public String getAvatar() {
            return avatar;
        }

        public void setAvatar(String avatar) {
            this.avatar = avatar;
        }

        public String getUsername() {
            return username;
        }

        public void setUsername(String username) {
            this.username = username;
        }

        public String getSource() {
            return source;
        }

        public void setSource(String source) {
            this.source = source;
        }

        public Object getLatitude() {
            return latitude;
        }

        public void setLatitude(Object latitude) {
            this.latitude = latitude;
        }

        public Object getLongitude() {
            return longitude;
        }

        public void setLongitude(Object longitude) {
            this.longitude = longitude;
        }

        public String getIs_anonymous() {
            return is_anonymous;
        }

        public void setIs_anonymous(String is_anonymous) {
            this.is_anonymous = is_anonymous;
        }

        public List<CommentsBean> getComments() {
            return comments;
        }

        public void setComments(List<CommentsBean> comments) {
            this.comments = comments;
        }

        public List<ImagesBean> getImages() {
            return images;
        }

        public void setImages(List<ImagesBean> images) {
            this.images = images;
        }

        public List<VideosBean> getVideos() {
            return videos;
        }

        public void setVideos(List<VideosBean> videos) {
            this.videos = videos;
        }

        public List<AudioBean> getAudio() {
            return audio;
        }

        public void setAudio(List<AudioBean> audio) {
            this.audio = audio;
        }

        public static class CommentsBean {
            /**
             * id : 126
             * comment : helloo
             * comment_id : null
             * dt_added : 2017-07-21 18:20:03
             * dt_modified : 2017-07-21 18:20:03
             * status : A
             * user_id : 3905
             * first_name : Abhishek
             * last_name : Chauhan
             * nickname : Abhishek
             * avatar : https://instafeed.org/storage/avatar/80x80-3844825a5c347c3f039c127d02b9154d.jpg
             * username : abhishek
             */

            private String id;
            private String comment;
            private Object comment_id;
            private String dt_added;
            private String dt_modified;
            private String status;
            private String user_id;
            private String first_name;
            private String last_name;
            private String nickname;
            private String avatar;
            private String username;

            public String getId() {
                return id;
            }

            public void setId(String id) {
                this.id = id;
            }

            public String getComment() {
                return comment;
            }

            public void setComment(String comment) {
                this.comment = comment;
            }

            public Object getComment_id() {
                return comment_id;
            }

            public void setComment_id(Object comment_id) {
                this.comment_id = comment_id;
            }

            public String getDt_added() {
                return dt_added;
            }

            public void setDt_added(String dt_added) {
                this.dt_added = dt_added;
            }

            public String getDt_modified() {
                return dt_modified;
            }

            public void setDt_modified(String dt_modified) {
                this.dt_modified = dt_modified;
            }

            public String getStatus() {
                return status;
            }

            public void setStatus(String status) {
                this.status = status;
            }

            public String getUser_id() {
                return user_id;
            }

            public void setUser_id(String user_id) {
                this.user_id = user_id;
            }

            public String getFirst_name() {
                return first_name;
            }

            public void setFirst_name(String first_name) {
                this.first_name = first_name;
            }

            public String getLast_name() {
                return last_name;
            }

            public void setLast_name(String last_name) {
                this.last_name = last_name;
            }

            public String getNickname() {
                return nickname;
            }

            public void setNickname(String nickname) {
                this.nickname = nickname;
            }

            public String getAvatar() {
                return avatar;
            }

            public void setAvatar(String avatar) {
                this.avatar = avatar;
            }

            public String getUsername() {
                return username;
            }

            public void setUsername(String username) {
                this.username = username;
            }
        }

        public static class ImagesBean {
            /**
             * id : 1117
             * image : https://instafeed.org/storage/news_mime/256x170-1e9f16b41715a707dcdf6182a71e7809.jpg
             * image_zoom : https://instafeed.org/storage/news_mime/748X420-1e9f16b41715a707dcdf6182a71e7809.jpg
             * image_original : https://instafeed.org/storage/news_mime/1e9f16b41715a707dcdf6182a71e7809.jpg
             * image_100x100 : https://instafeed.org/storage/news_mime/100x100-1e9f16b41715a707dcdf6182a71e7809.jpg
             * image_256x170 : https://instafeed.org/storage/news_mime/256x170-1e9f16b41715a707dcdf6182a71e7809.jpg
             * image_264x200 : https://instafeed.org/storage/news_mime/264x200-1e9f16b41715a707dcdf6182a71e7809.jpg
             * image_360x290 : https://instafeed.org/storage/news_mime/360x290-1e9f16b41715a707dcdf6182a71e7809.jpg
             */

            private String id;
            private String image;
            private String image_zoom;
            private String image_original;
            private String image_100x100;
            private String image_256x170;
            private String image_264x200;
            private String image_360x290;

            public String getId() {
                return id;
            }

            public void setId(String id) {
                this.id = id;
            }

            public String getImage() {
                return image;
            }

            public void setImage(String image) {
                this.image = image;
            }

            public String getImage_zoom() {
                return image_zoom;
            }

            public void setImage_zoom(String image_zoom) {
                this.image_zoom = image_zoom;
            }

            public String getImage_original() {
                return image_original;
            }

            public void setImage_original(String image_original) {
                this.image_original = image_original;
            }

            public String getImage_100x100() {
                return image_100x100;
            }

            public void setImage_100x100(String image_100x100) {
                this.image_100x100 = image_100x100;
            }

            public String getImage_256x170() {
                return image_256x170;
            }

            public void setImage_256x170(String image_256x170) {
                this.image_256x170 = image_256x170;
            }

            public String getImage_264x200() {
                return image_264x200;
            }

            public void setImage_264x200(String image_264x200) {
                this.image_264x200 = image_264x200;
            }

            public String getImage_360x290() {
                return image_360x290;
            }

            public void setImage_360x290(String image_360x290) {
                this.image_360x290 = image_360x290;
            }
        }

        public static class VideosBean {
            /**
             * id : 189
             * video : https://instafeed.org/storage/news_mime/0b898f315a005bebc6017c4a26d361ea.mp4
             * video_thumb : https://instafeed.org/storage/news_mime/256x170-0b898f315a005bebc6017c4a26d361ea.jpg
             * vimeo_video_id : null
             * vimeo_response : null
             */

            private String id;
            private String video;
            private String video_thumb;
            private Object vimeo_video_id;
            private Object vimeo_response;

            public String getId() {
                return id;
            }

            public void setId(String id) {
                this.id = id;
            }

            public String getVideo() {
                return video;
            }

            public void setVideo(String video) {
                this.video = video;
            }

            public String getVideo_thumb() {
                return video_thumb;
            }

            public void setVideo_thumb(String video_thumb) {
                this.video_thumb = video_thumb;
            }

            public Object getVimeo_video_id() {
                return vimeo_video_id;
            }

            public void setVimeo_video_id(Object vimeo_video_id) {
                this.vimeo_video_id = vimeo_video_id;
            }

            public Object getVimeo_response() {
                return vimeo_response;
            }

            public void setVimeo_response(Object vimeo_response) {
                this.vimeo_response = vimeo_response;
            }
        }

        public static class AudioBean {
            /**
             * id : 99
             * audio : https://instafeed.org/storage/news_mime/59820b4ef7b875ceaf5164e8b5d13d6b.mp3
             */

            private String id;
            private String audio;

            public String getId() {
                return id;
            }

            public void setId(String id) {
                this.id = id;
            }

            public String getAudio() {
                return audio;
            }

            public void setAudio(String audio) {
                this.audio = audio;
            }
        }
    }
}