package com.abacusdesk.instafeed.instafeed.fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Spinner;
import android.widget.TextView;

import com.abacusdesk.instafeed.instafeed.NavDrawerActivity;
import com.abacusdesk.instafeed.instafeed.R;
import com.abacusdesk.instafeed.instafeed.adapter.RecyclerView_Adapter;
import com.abacusdesk.instafeed.instafeed.adapter.Search_Adapter;
import com.abacusdesk.instafeed.instafeed.adapter.User_Adapter;
import com.abacusdesk.instafeed.instafeed.model.Search_model;
import com.abacusdesk.instafeed.instafeed.model.User_model;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by ishaan on 7/1/2017.
 */

public class Fragment_search extends Fragment {

    View view;
    RecyclerView recyclerView;

   // ArrayList<Search_model.DataBean> arraySearch;
    ArrayList<HashMap<String,String>> arraySearch;
    ArrayList<User_model.DataBean> arrayUser;

    Search_Adapter adapter;
    User_Adapter user_adapter;
    TextView txtrecord;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Log.e("option","");
        setHasOptionsMenu(true);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_search, container, false);
        recyclerView=(RecyclerView)view.findViewById(R.id.recyclerView);
        Bundle bundle = this.getArguments();
        try {
            if (bundle != null) {
                if (NavDrawerActivity.type.equals("users")){
                    arrayUser= (ArrayList<User_model.DataBean>)bundle.getSerializable("array_search");
                }else {
                    arraySearch= (ArrayList<HashMap<String, String>>) bundle.getSerializable("array_search");
                }
            }
        }catch (Exception e){
            Log.e("excepseafch",""+e);
        }
        init();
        //adapter.notifyDataSetChanged();
        return view;
    }

    private void init() {
      txtrecord=(TextView)view.findViewById(R.id.txt_record);
        if (NavDrawerActivity.type.equalsIgnoreCase("users")){
            if (arrayUser.isEmpty()){
                txtrecord.setVisibility(View.VISIBLE);
            }else {
                txtrecord.setVisibility(View.GONE);
            }
            recyclerView.setAdapter(new User_Adapter(getContext(),arrayUser));
            recyclerView.setHasFixedSize(true);
            recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
            try{
                user_adapter= new User_Adapter(getActivity(),arrayUser);
                recyclerView.setAdapter(user_adapter);
                user_adapter.notifyDataSetChanged();
            }  catch (Exception e){
                e.printStackTrace();
            }
        }else {
            if (arraySearch.isEmpty()){
                txtrecord.setVisibility(View.VISIBLE);
            }else {
                txtrecord.setVisibility(View.GONE);
            }
            //recyclerView.setAdapter(new Search_Adapter(getContext(),arraySearch));
            recyclerView.setHasFixedSize(true);
            recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
            // recyclerView.addItemDecoration(mDividerItemDecoration);
            try{
                adapter= new Search_Adapter(getActivity(),arraySearch);
                recyclerView.setAdapter(adapter);
                Log.e("responseset",""+arraySearch);
            }  catch (Exception e){
                //e.printStackTrace();
            }
        }
    }
}
