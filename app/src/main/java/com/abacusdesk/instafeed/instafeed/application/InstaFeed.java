package com.abacusdesk.instafeed.instafeed.application;

import android.app.Application;
import android.os.Build;
import android.os.StrictMode;
import android.support.multidex.MultiDexApplication;

import com.abacusdesk.instafeed.instafeed.R;

import org.acra.ACRA;
import org.acra.ReportField;
import org.acra.ReportingInteractionMode;
import org.acra.annotation.ReportsCrashes;

/**
 * Created by Administrator on 6/5/2017.
 */

@ReportsCrashes(mailTo ="jitender@abacusdesk.co.in",
        customReportContent = {
                ReportField.APP_VERSION_CODE,
                ReportField.APP_VERSION_NAME,
                ReportField.ANDROID_VERSION,
                ReportField.PHONE_MODEL,
                ReportField.REPORT_ID,
                ReportField.CUSTOM_DATA,
                ReportField.STACK_TRACE},
        mode = ReportingInteractionMode.TOAST,
        resToastText = R.string.crash_toast_text,
        forceCloseDialogAfterToast = false, // optional, default false
        formKey = "InstaFeed")


public class InstaFeed extends MultiDexApplication {

    @Override
    public void onCreate() {
        super.onCreate();
        StrictMode.VmPolicy.Builder builder = new StrictMode.VmPolicy.Builder();
        StrictMode.setVmPolicy(builder.build());
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR2) {
            builder.detectFileUriExposure();
        }
    }

    @Override
    public void onLowMemory() {
        super.onLowMemory();
    }
}
