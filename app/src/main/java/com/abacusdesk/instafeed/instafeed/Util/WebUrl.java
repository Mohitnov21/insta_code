package com.abacusdesk.instafeed.instafeed.Util;

/**
 * Created by Administrator on 6/10/2017.
 */

public class WebUrl {
    public static final String BASE_URL = "http://instafeed.org/api/";
    //   public static final String BASE_URL="http://instafeed.org/api/";

    public static final String UPLOAD_NEWS_URL = BASE_URL + "news/post";
    public static final String UPLOAD_EXPLORE_URL = BASE_URL + "explore/post";
    public static final String UPLOAD_TALENT_URL = BASE_URL + "talents/post";

    //  public static final String UPLOAD_NEWS_URL="http://instafeed.org/api/news/post";
    public static final String NEWS_CATEGORY_URL = BASE_URL + "news/category";
    public static final String EXPLORE_CATEGORY_URL = BASE_URL + "explore/category";
    public static final String TALENT_CATEGORY_URL = BASE_URL + "talents/category";
}
