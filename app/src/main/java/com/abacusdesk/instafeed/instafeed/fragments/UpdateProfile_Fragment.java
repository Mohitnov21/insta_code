package com.abacusdesk.instafeed.instafeed.fragments;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.media.ExifInterface;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.provider.VoicemailContract;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.abacusdesk.instafeed.instafeed.AndroidMultiPartEntity;
import com.abacusdesk.instafeed.instafeed.NavDrawerActivity;
import com.abacusdesk.instafeed.instafeed.R;
import com.abacusdesk.instafeed.instafeed.Util.Alerts;
import com.abacusdesk.instafeed.instafeed.Util.Apis;
import com.abacusdesk.instafeed.instafeed.Util.CommonFunctions;
import com.abacusdesk.instafeed.instafeed.Util.SaveSharedPreference;
import com.abacusdesk.instafeed.instafeed.adapter.CustomAdapter;
import com.abacusdesk.instafeed.instafeed.custom_dialog.Dialogs;
import com.abacusdesk.instafeed.instafeed.model.NewsModel;
import com.abacusdesk.instafeed.instafeed.model.Profile_model;
import com.android.volley.AuthFailureError;
import com.android.volley.NoConnectionError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.resource.transcode.BitmapBytesTranscoder;
import com.bumptech.glide.util.Util;
import com.google.gson.Gson;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.mime.content.ByteArrayBody;
import org.apache.http.entity.mime.content.StringBody;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;
import java.util.concurrent.ExecutionException;

import static android.R.attr.orientation;
import static android.app.Activity.RESULT_OK;
import static com.abacusdesk.instafeed.instafeed.Util.ImageUtils.rotateBitmap;
import static org.acra.ACRA.log;

/**
 * Created by abacusdesk on 2017-07-25.
 */

public class UpdateProfile_Fragment extends Fragment implements View.OnClickListener {

    EditText edfname,edmname,edlname,edprofileurl,edcontact,edemail,edbio,edwebsite,edbirthday;
    Spinner spin_gender;
    View view;
    private String[] gender = {"select Gender","Male","Female"};
    private int mMonth,mYear,mDay;
    Profile_model profile_model;
    private Button btn_editprofile;
    private AutoCompleteTextView auto_txtview;
    private Boolean flag=false;
    ArrayList<Profile_model.DataBean> array_Profile= new ArrayList<>();
    private  String sex="";
    private  String location_id="",loc_id="";
    TextView txtusername,txtfulname;
    String selection;
    String api_temp="",location_name,birthday="";
    Bitmap bitmap;
    int statusCode;
    Uri fileUri;
    ImageView imgprofile,imgdemo;
    long totalSize = 0;
    FrameLayout frameLayout;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container,@Nullable Bundle savedInstanceState) {
        view=inflater.inflate(R.layout.actiivty_profile,container,false);
        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        init();
        getActivity().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_PAN |WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);
        if(isAdded()){
            try {
                getUserdetails();
            }catch(Exception e){

            }
        }
    }

    private void init(){
        ImageView imgpick=(ImageView)view.findViewById(R.id.img_selectphoto);
        frameLayout=(FrameLayout)view.findViewById(R.id.img_frame);
        imgprofile=(ImageView) view.findViewById(R.id.img_profile);
        if (NavDrawerActivity.toolbar!=null){
            NavDrawerActivity.toolbar.setLogo(R.drawable.logo_black);
        }

        imgpick.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                pickImage();
            }
        });
        btn_editprofile=(Button) view.findViewById(R.id.btn_profile);
        txtfulname=(TextView) view.findViewById(R.id.txt_fullname);
        txtusername=(TextView) view.findViewById(R.id.txt_username);

        edfname=(EditText)view.findViewById(R.id.ed_fname);
        edlname=(EditText)view.findViewById(R.id.ed_lastname);
        edmname=(EditText)view.findViewById(R.id.ed_middlename);
        edprofileurl=(EditText)view.findViewById(R.id.ed_profileurl);
        edcontact=(EditText)view.findViewById(R.id.ed_contact);
        edemail=(EditText)view.findViewById(R.id.ed_email);
        edbio=(EditText)view.findViewById(R.id.ed_bio);
        edwebsite=(EditText)view.findViewById(R.id.ed_website);
        edbirthday=(EditText)view.findViewById(R.id.ed_birthdate);
        spin_gender=(Spinner)view.findViewById(R.id.spinner1);
        auto_txtview=(AutoCompleteTextView)view.findViewById(R.id.auto_txtview);

        edfname.setEnabled(false);
        edlname.setEnabled(false);
        edmname.setEnabled(false);
        edprofileurl.setEnabled(false);
        edcontact.setEnabled(false);
        edfname.setEnabled(false);
        edemail.setEnabled(false);

        edbio.setEnabled(false);
        edwebsite.setEnabled(false);
        edbirthday.setEnabled(false);
        spin_gender.setEnabled(false);
        auto_txtview.setEnabled(false);

        edbirthday.setOnClickListener(this);
        btn_editprofile.setOnClickListener(this);

        ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(getContext(),android.R.layout.simple_spinner_item,gender);
        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spin_gender.setAdapter(dataAdapter);

      spin_gender.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                if (spin_gender.getSelectedItem().toString().equalsIgnoreCase("male")){
                    sex="M";
                }else if (spin_gender.getSelectedItem().toString().equalsIgnoreCase("female")){
                    sex="F";
                }else {
                    sex="";
                }
               // Toast.makeText(getContext(),sex, Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
      auto_txtview.setThreshold(1);     //will start working from first char\// cter
      CustomAdapter adapter = new CustomAdapter(getContext(),NavDrawerActivity.arrayLocation);
        auto_txtview.setAdapter(adapter);
        auto_txtview.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                 selection = (String)parent.getItemAtPosition(position);
                 Log.e("selected",""+selection);
            }
        });
    }

    @Override
    public void onClick(View v) {
        if (v==edbirthday){
            final Calendar c = Calendar.getInstance();
            mYear = c.get(Calendar.YEAR);
            mMonth = c.get(Calendar.MONTH);
            mDay = c.get(Calendar.DAY_OF_MONTH);
            DatePickerDialog datePickerDialog = new DatePickerDialog(getContext(),
                    new DatePickerDialog.OnDateSetListener() {
                        @Override
                        public void onDateSet(DatePicker view, int year,
                                              int monthOfYear, int dayOfMonth) {
                            String myFormat = "dd-MM-yyyy";
                            DateFormat sdformat = new SimpleDateFormat(myFormat);
                            DateFormat targetFormat = new SimpleDateFormat("dd MM yyyy");
                            Date date = null;
                            try {
                                String actualdate = dayOfMonth + " " + (monthOfYear + 1) + " " + year;
                                date = targetFormat.parse(dayOfMonth + " " + (monthOfYear + 1) + " " + year);
                                log.e("date", "" + actualdate);
                                String formattedDate = sdformat.format(date);

                                Date current =c.getTime();
                                int diff1 =new Date().compareTo(current);

                                if(diff1<0){
                                    Toast.makeText(getContext(), "Please select a valid date",  Toast.LENGTH_LONG).show();
                                    return;
                                }
                                else{
                                    edbirthday.setText(formattedDate);
                                }
                            } catch (ParseException e) {
                                e.printStackTrace();
                            }
                        }
                    }, mYear, mMonth, mDay);
            datePickerDialog.show();
        }else if (v==btn_editprofile){
            if (flag==false){
                btn_editprofile.setBackgroundResource(R.drawable.btn_bglight);
                edfname.setEnabled(true);
                edlname.setEnabled(true);
                edmname.setEnabled(true);
                edprofileurl.setEnabled(false);
                edcontact.setEnabled(true);
                edfname.setEnabled(true);
                if (SaveSharedPreference.getMobileLogin(getContext()).equals("true")){
                    edemail.setEnabled(true);
                }else {
                    edemail.setEnabled(false);
                }
                edbio.setEnabled(true);
                edwebsite.setEnabled(true);
                edbirthday.setEnabled(true);
                spin_gender.setEnabled(true);
                auto_txtview.setEnabled(true);
                btn_editprofile.setText("Save Profile");
                flag=true;
          } else {
                btn_editprofile.setBackgroundResource(R.drawable.button_bg_rounded_corners);
                if (CommonFunctions.isConnected(getContext())){
                   if (valid()){
                       Log.e("locationid"," "+location_id);
                       sendUserdetails();
                   }
                }

            }
        }
    }

    private boolean valid() {
        try{
            Log.e("actual",""+edbirthday.getText().toString());
            birthday=CommonFunctions.sendDateNTime(edbirthday.getText().toString());
            Log.e("birthday",""+birthday);
        }catch (Exception e){

        }

        if (!auto_txtview.getText().toString().equals("")){
            try{
                for (int i=0;i<NavDrawerActivity.arrayLocation.size();i++){
                    if (NavDrawerActivity.arrayLocation.get(i).getDistrict_name().equalsIgnoreCase(auto_txtview.getText().toString())){
                        location_id= NavDrawerActivity.arrayLocation.get(i).getId();
                    }
                }
            }catch (Exception e){
                e.printStackTrace();
            }
        }
        if (edfname.getText().toString().equals("")) {
            edfname.setError("Enter First Name");
            return false;
        } else if (edlname.getText().toString().equals("")) {
            edlname.setError("Enter Last Name");
            return false;
        } else if (auto_txtview.getText().toString().equals("")) {
            auto_txtview.setError("Choose any Location");
            return false;
        }else if (location_id.equals("")){
            auto_txtview.setError("please select from above fields");
            return false;
        }else {
            return true;
        }
    }

    public void getUserdetails() {
        // Dialogs.showProDialog(NavDrawerActivity.this, "Loading");
        StringRequest stringRequest = new StringRequest(Request.Method.POST, Apis.profile,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.e("log", "" + response);
                        try {
                            getDetails(response);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        Dialogs.disDialog();
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Dialogs.disDialog();
                        DialogInterface.OnClickListener onClickTryAgain = new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                Dialogs.disDialog();
                                getUserdetails();
                            }
                        };
                        if (error instanceof TimeoutError) {
                            Dialogs.disDialog();
                            Alerts.timeoutErrorAlert(getContext(), onClickTryAgain);
                        } else if (error instanceof NoConnectionError) {
                            Dialogs.disDialog();
                            Alerts.internetConnectionErrorAlert(getContext(), onClickTryAgain);
                        }
                    }
                })

        {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> map = new HashMap<String, String>();
                map.put("user", SaveSharedPreference.getUserID(getContext()));
                map.put("token", SaveSharedPreference.getPrefToken(getContext()));
                Log.e("userdetailmap",""+map);
                return map;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Content-Type", "application/x-www-form-urlencoded");
                return params;
            }
        };
        RequestQueue requestQueue = Volley.newRequestQueue(getContext());
        requestQueue.add(stringRequest);
    }

    private void getDetails(String response ) throws JSONException {
        if (response != null) {
            Log.e("log", "" + response);
            JSONObject jsonObject=new JSONObject(response);
            Object object=jsonObject.get("data");
            if (object instanceof JSONObject) {
                Gson gson = new Gson();
                profile_model = gson.fromJson(response, Profile_model.class);
                int status = profile_model.getStatus();
                String msg = profile_model.getMessage();
                if (msg.equalsIgnoreCase("success") && status == 200) {
                    if (profile_model.getData() != null) {

                        txtusername.setText("#" + profile_model.getData().getUsername());
                        if (profile_model.getData().getFirst_name()!=null) {
                            txtfulname.setText(profile_model.getData().getFirst_name());
                        }
                        if (profile_model.getData().getLast_name()!=null) {
                            txtfulname.setText(profile_model.getData().getFirst_name() + " " + profile_model.getData().getLast_name());
                        }

                        edfname.setText(profile_model.getData().getFirst_name());
                        if (profile_model.getData().getLast_name() != null) {
                            edlname.setText(profile_model.getData().getLast_name());
                        }
                        if (profile_model.getData().getMiddle_name() != null) {
                            edmname.setText(profile_model.getData().getMiddle_name().toString());
                        }
                        if (profile_model.getData().getBirth_date() != null) {
                            try {
                                edbirthday.setText(CommonFunctions.getDateNTime(profile_model.getData().getBirth_date().toString()));
                            } catch (Exception e) {

                            }
                        }
                        if (profile_model.getData().getPhone() != null) {
                            edcontact.setText(profile_model.getData().getPhone().toString());
                        }
                        if (profile_model.getData().getProfile_url() != null) {
                            edprofileurl.setText(profile_model.getData().getProfile_url());
                        }
                        if (profile_model.getData().getEmail() != null) {
                            edemail.setText(profile_model.getData().getEmail());
                        }
                        if (profile_model.getData().getLocation_id() != null) {
                            location_id=profile_model.getData().getLocation_id();
                            getLocation();
                        }
                        if (profile_model.getData().getBio()!= null) {
                            edbio.setText(profile_model.getData().getBio());
                        }
                        if (profile_model.getData().getWebsite() != null) {
                            edwebsite.setText(profile_model.getData().getWebsite());
                        }
                        if (profile_model.getData().getSex() != null) {
                            if (profile_model.getData().getSex().equals("M")) {
                                spin_gender.setSelection(1);
                            } else if (profile_model.getData().getSex().equals("F")){
                                spin_gender.setSelection(2);
                            }else {
                                spin_gender.setSelection(0);
                            }
                        }
                        try {
                            if (!profile_model.getData().getAvatar().equals("null")) {
                                //imgdemo.setVisibility(View.GONE);
                                if(getActivity()!=null)
                                Glide.with(getActivity()).load(profile_model.getData().getAvatar()).error(R.drawable.user).into(imgprofile);
                            }
                        }catch (Exception e){

                        }
                    }

                }
            }
        } else {

        }
    }

    public void sendUserdetails() {
        // Dialogs.showProDialog(NavDrawerActivity.this, "Loading");
        StringRequest stringRequest = new StringRequest(Request.Method.POST, Apis.profile_update,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.e("log", "" + response);
                        try {
                            responseUpdate(response);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        Dialogs.disDialog();
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Dialogs.disDialog();
                        DialogInterface.OnClickListener onClickTryAgain = new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                Dialogs.disDialog();
                                sendUserdetails();
                            }
                        };
                        if (error instanceof TimeoutError) {
                            Dialogs.disDialog();
                            Alerts.timeoutErrorAlert(getContext(), onClickTryAgain);
                        } else if (error instanceof NoConnectionError) {
                            Dialogs.disDialog();
                            Alerts.internetConnectionErrorAlert(getContext(), onClickTryAgain);
                        }
                    }
                })
        {


            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> map = new HashMap<String, String>();
                map.put("user", SaveSharedPreference.getUserID(getContext()));
                map.put("token", SaveSharedPreference.getPrefToken(getContext()));
                map.put("first_name",edfname.getText().toString());
                map.put("middle_name", edmname.getText().toString());
                map.put("last_name", edlname.getText().toString());
                map.put("username",SaveSharedPreference.getUserName(getContext()));
                map.put("birth_date",birthday);
                map.put("sex", sex);
                map.put("location_id",location_id);
                map.put("bio",edbio.getText().toString());
                map.put("website",edwebsite.getText().toString());
                map.put("phone", edcontact.getText().toString());
                Log.e("updated details",""+map);
                return map;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Content-Type", "application/x-www-form-urlencoded");
                return params;
            }
        };
        RequestQueue requestQueue = Volley.newRequestQueue(getContext());
        requestQueue.add(stringRequest);
    }

    private void responseUpdate(String response ) throws JSONException {
        if (response!=null){
       JSONObject jsonobject=new JSONObject(response);
        if (jsonobject.getString("status").equalsIgnoreCase("200") && jsonobject.getString("message").equalsIgnoreCase("success")){
            Toast.makeText(getContext(),"Profile Updated Successfully",Toast.LENGTH_SHORT).show();
            edfname.setEnabled(false);
            edlname.setEnabled(false);
            edmname.setEnabled(false);
            edprofileurl.setEnabled(false);
            edcontact.setEnabled(false);
            edfname.setEnabled(false);
            edemail.setEnabled(false);
            edbio.setEnabled(false);
            edwebsite.setEnabled(false);
            edbirthday.setEnabled(false);
            spin_gender.setEnabled(false);
            auto_txtview.setEnabled(false);
            btn_editprofile.setText("Edit Profile");
            flag=false;
        }else {
            Toast.makeText(getContext(),jsonobject.getString("message"),Toast.LENGTH_SHORT).show();
          }
     }
    }

    public void pickImage() {
        final Dialog dialog2 = new Dialog(getContext());
        dialog2.requestWindowFeature(Window.FEATURE_NO_TITLE); //before
        dialog2.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        dialog2.setContentView(R.layout.dialog_pickimage);
        WindowManager.LayoutParams lp = dialog2.getWindow().getAttributes();
        Window window = dialog2.getWindow();
        lp.copyFrom(window.getAttributes());
        lp.width = WindowManager.LayoutParams.MATCH_PARENT;
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
        window.setAttributes(lp);
        lp.gravity = Gravity.CENTER;

        TextView txtCamera = (TextView) dialog2.findViewById(R.id.txt_camera);
        TextView txtGallery = (TextView) dialog2.findViewById(R.id.txt_gallery);
        TextView txtremove = (TextView) dialog2.findViewById(R.id.txt_imgremove);

        txtCamera.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                File f = new File(Environment.getExternalStorageDirectory(), "temp.jpg");
                intent.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(f));
                startActivityForResult(intent, 0);
                dialog2.dismiss();
            }
        });

        txtGallery.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
   //       Intent pickPhoto = new Intent(Intent.ACTION_PICK,
   //       MediaStore.Images.Media.EXTERNAL_CONTENT_URI);


                Intent intent = new Intent();
                intent.setType("image/*");
                intent.setAction(Intent.ACTION_GET_CONTENT);
                startActivityForResult(Intent.createChooser(intent, "Select Picture"),1);
                dialog2.dismiss();
            }
        });

        txtremove.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                /*bitmap = BitmapFactory.decodeResource(getActivity().getResources(),
                        R.drawable.user);
                new UploadFileToServer().execute();*/
                if (SaveSharedPreference.getUserIMAGE(getContext()).equals("")){
                    Toast.makeText(getContext(),"No Profile Pic!", Toast.LENGTH_SHORT).show();
                }else {
                    deleteProfile_pic();
                }

                dialog2.dismiss();
            }
        });
        dialog2.show();
    }

    private Bitmap changeorientation(String path){
        ExifInterface exif = null;
        int orientation = 0;
        Bitmap bmRotated = null;
        try {
            exif = new ExifInterface(path);
             orientation = exif.getAttributeInt(ExifInterface.TAG_ORIENTATION,
                    ExifInterface.ORIENTATION_UNDEFINED);
             bmRotated = rotateBitmap(bitmap, orientation);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return bmRotated;
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        switch (requestCode) {
            case 0:
                if (resultCode == RESULT_OK) {
                    File f = null;
                    try {
                         f = new File(Environment.getExternalStorageDirectory().toString());
                        for (File temp : f.listFiles()) {
                            if (temp.getName().equals("temp.jpg")) {
                                f = temp;
                            }
                        }
                    }catch (Exception e){
                    }
                    try {
                        BitmapFactory.Options bitmapOptions = new BitmapFactory.Options();
                        bitmap = BitmapFactory.decodeFile(f.getAbsolutePath(), bitmapOptions);

                        if (bitmap!=null){
                            imgprofile.setImageBitmap(bitmap);
                            new UploadFileToServer().execute();
                        }
                        /*bitmap1=changeorientation(f.getAbsolutePath());
                        if (bitmap1!=null){
                            imgprofile.setImageBitmap(bitmap1);
                        }else {
                            imgprofile.setImageBitmap(bitmap);
                        }*/
                        // imgdemo.setVisibility(View.GONE);

                    } catch (Exception e) {

                    }
                }
                break;

            case 1:
                if (data!=null)
                fileUri = data.getData();
                try {
                    if (fileUri!=null){
                        bitmap = MediaStore.Images.Media.getBitmap(getActivity().getContentResolver(), fileUri);
                        //bitmap=changeorientation(fileUri.getPath());

                        Log.e("bitmap",""+bitmap);
                        imgprofile.setImageBitmap(bitmap);
                        //imgdemo.setVisibility(View.GONE);
                        new UploadFileToServer().execute();
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                }
                break;
        }
    }

    private class UploadFileToServer extends AsyncTask<Void, Integer, String> {

        @Override
        protected void onPreExecute() {
            // setting progress bar to zero
            Dialogs.showProDialog(getContext(), "Updating");
            super.onPreExecute();
        }

        @Override
        protected String doInBackground(Void... params) {
            return uploadFile();
        }

        @SuppressWarnings("deprecation")
        private String uploadFile() {
            String responseString = null;

            ByteArrayOutputStream bos = new ByteArrayOutputStream();
            bitmap.compress(Bitmap.CompressFormat.JPEG, 75, bos);
            byte[] data = bos.toByteArray();

            HttpClient httpclient = new DefaultHttpClient();
            HttpPost httppost = new HttpPost(Apis.update_pic);

            try {
                AndroidMultiPartEntity entity = new AndroidMultiPartEntity(
                        new AndroidMultiPartEntity.ProgressListener() {

                            @Override
                            public void transferred(long num) {
                                publishProgress((int) ((num / (float) totalSize) * 100));
                            }
                        });

                ByteArrayBody bab = new ByteArrayBody(data, "bt.jpg");
                entity.addPart("image", bab);
                entity.addPart("token", new StringBody(SaveSharedPreference.getPrefToken(getContext())));
                entity.addPart("user", new StringBody(SaveSharedPreference.getUserID(getContext())));

                totalSize = entity.getContentLength();
                httppost.setEntity(entity);

                HttpResponse response = httpclient.execute(httppost);
                HttpEntity r_entity = response.getEntity();

                statusCode = response.getStatusLine().getStatusCode();
                responseString = EntityUtils.toString(r_entity);
                JSONObject jsonObject = new JSONObject(responseString);
                Log.e("respon", "" + responseString);

                if (statusCode == 200) {
                    // Server response
                    try {
                        SaveSharedPreference.setUserIMAGE(getContext(),jsonObject.getJSONObject("data")
                                .getJSONObject("avatar").getString("200x200") );
                    }catch (Exception e){

                    }
                } else if (statusCode == 201) {
                    // Server response
                    try {
                        SaveSharedPreference.setUserIMAGE(getContext(),jsonObject.getJSONObject("data")
                                .getJSONObject("avatar").getString("200x200") );
                    }catch (Exception e){

                    }

                } else {
                    responseString = "Error occurred! Http Status Code: " +
                            EntityUtils.toString(r_entity) + statusCode;
                }
            } catch (ClientProtocolException e) {
                responseString = e.toString();
            } catch (Exception e) {
                responseString = e.toString();
            }

            return responseString;
        }

        @Override
        protected void onPostExecute(String result) {
            Dialogs.disDialog();
            try {
                if (statusCode == 200) {
                    Toast.makeText(getContext(),"Updated Successfully",Toast.LENGTH_SHORT).show();
                    //if (bitmap==null)
                    if (bitmap!=null){
                        if (!SaveSharedPreference.getUserIMAGE(getContext()).isEmpty()){
                            Glide.with(getContext()).load(SaveSharedPreference.getUserIMAGE(getContext())).error(R.drawable.user).into(imgprofile);
                        }
                    }
                    Log.e("bitmap2",""+bitmap);
                    NavDrawerActivity.updateProfile(getContext());
                } else if (statusCode == 201) {
                    Toast.makeText(getContext(),"Updated Successfully",Toast.LENGTH_SHORT).show();
                    Dialogs.showCenterToast(getContext(),"Updated Successfully");
                } else {

                }
            }catch (Exception e){

            }
        }

    }

    public void deleteProfile_pic() {
        StringRequest stringRequest = new StringRequest(Request.Method.POST, Apis.private_picdelete,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.e("log", "" + response);
                        try {
                            JSONObject jsonObject=new JSONObject(response);
                            if (jsonObject.getString("status").equals("200") && jsonObject.getString("message").equalsIgnoreCase("success")){
                                SaveSharedPreference.setUserIMAGE(getContext(),"");
                                imgprofile.setImageResource(R.drawable.user);
                                NavDrawerActivity.updateProfile(getContext());
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        Dialogs.disDialog();
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Dialogs.disDialog();
                        DialogInterface.OnClickListener onClickTryAgain = new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                Dialogs.disDialog();
                                deleteProfile_pic();
                            }
                        };
                        if (error instanceof TimeoutError) {
                            Dialogs.disDialog();
                            Alerts.timeoutErrorAlert(getContext(), onClickTryAgain);
                        } else if (error instanceof NoConnectionError) {
                            Dialogs.disDialog();
                            Alerts.internetConnectionErrorAlert(getContext(), onClickTryAgain);
                        }
                    }
                })
        {


            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> map = new HashMap<String, String>();
                map.put("user", SaveSharedPreference.getUserID(getContext()));
                map.put("token", SaveSharedPreference.getPrefToken(getContext()));
                return map;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Content-Type", "application/x-www-form-urlencoded");
                return params;
            }
        };
        RequestQueue requestQueue = Volley.newRequestQueue(getContext());
        requestQueue.add(stringRequest);
    }

    public void getLocation() {
        api_temp=Apis.getlocation.replace("id2",location_id);
        //  Dialogs.showProDialog(getActivity(), "Loading");
        StringRequest stringRequest = new StringRequest(Request.Method.GET,api_temp,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.e("log", "" + response);
                        try {
                            resLocation(response);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        // Dialogs.disDialog();
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        ///   Dialogs.disDialog();
                        DialogInterface.OnClickListener onClickTryAgain = new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                //  Dialogs.disDialog();
                                getLocation();
                            }
                        };
                        if (error instanceof TimeoutError) {
                            //  Dialogs.disDialog();
                            Alerts.timeoutErrorAlert(getContext(), onClickTryAgain);
                        } else if (error instanceof NoConnectionError) {
                            //  Dialogs.disDialog();
                            Alerts.internetConnectionErrorAlert(getContext(), onClickTryAgain);
                        }
                    }
                })
        {

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Content-Type", "application/x-www-form-urlencoded");
                return params;
            }
        };

        try {
            RequestQueue requestQueue = Volley.newRequestQueue(getContext());
            requestQueue.add(stringRequest);
        }catch (Exception e){

        }
    }

    private void resLocation(String response ) throws JSONException {
        if(!response.equals(null)) {
            Log.e("log", "" + response);
           JSONObject jsonObject=new JSONObject(response);
            if (jsonObject.getString("status")
                    .equals("200")&&jsonObject.getString("message").equals("success")){
                location_name=jsonObject.getJSONArray("data").getJSONObject(0).getString("district_name");
                auto_txtview.setText(location_name);
            }
        }
    }

}
