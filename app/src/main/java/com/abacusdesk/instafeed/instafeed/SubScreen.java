package com.abacusdesk.instafeed.instafeed;

import java.io.Serializable;

/**
 * Created by codecube on 18/5/17.
 */

public class SubScreen implements Serializable{

    private String name;
    private String id;
    private String flag;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getFlag() {
        return flag;
    }

    public void setFlag(String flag) {
        this.flag = flag;
    }
}
