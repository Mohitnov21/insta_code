package com.abacusdesk.instafeed.instafeed.webservice;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkResponse;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.Response.ErrorListener;
import com.android.volley.Response.Listener;
import com.android.volley.toolbox.HttpHeaderParser;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.HashMap;
import java.util.Map;

public class CustomRequestFileUpload extends Request<String> {

    private final String fileKey;
    private File file;
    private Listener<String> listener;

    public CustomRequestFileUpload(int method, String url, File file, String fileKey,
                                   Listener<String> listener, ErrorListener errorListener) {
        super(method, url, errorListener);
        this.file = file;
        this.fileKey = fileKey;
        this.listener = listener;
    }

    @Override
    public Map<String, String> getHeaders() throws AuthFailureError {
        Map<String, String> map = new HashMap<String, String>();
        String key = fileKey;
        map.put(key, file.getName());
        return map;
    }

    @Override
    public String getBodyContentType() {
        return "multipart/form-data; charset=utf-8";
    }

    @Override
    public byte[] getBody() throws AuthFailureError {
        try {
            ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
            FileInputStream fileInputStream = new FileInputStream(file);
            byte[] buffer = new byte[4096];
            int bytesRead = -1;
            while ((bytesRead = fileInputStream.read(buffer)) != -1) {
                byteArrayOutputStream.write(buffer, 0, bytesRead);
            }
            fileInputStream.close();
            return byteArrayOutputStream.toByteArray();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
            Response.error(new ParseError(e));
        } catch (IOException e) {
            e.printStackTrace();
            Response.error(new ParseError(e));
        }
        return super.getBody();
    }

    @Override
    protected Response<String> parseNetworkResponse(NetworkResponse response) {
        String data = "No response";
        try {
            data = new String(response.data,
                    HttpHeaderParser.parseCharset(response.headers));
            Response.success(data, HttpHeaderParser.parseCacheHeaders(response));

        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
            Response.error(new ParseError(e));
        }
        return Response.success(data, HttpHeaderParser.parseCacheHeaders(response));
    }

    @Override
    protected void deliverResponse(String response) {
        this.listener.onResponse(response);
    }

}
