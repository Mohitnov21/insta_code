package com.abacusdesk.instafeed.instafeed.model;

import java.util.List;

/**
 * Created by ishaan on 7/11/2017.
 */

public class Exploredetail_model {


    /**
     * status : 200
     * message : success
     * data : [{"id":"41","explorer_category_id":"10","name":"Lifestyle","title":"test explorer","slug":"test-explorer-1867958090","short_description":"explorer","description":"explorer","total_up_votes":"1","total_down_votes":null,"total_comments":"1","total_views":"3","total_flags":null,"dt_added":"2017-07-27 18:04:47","status":"P","loc_lat":"28.65021800","loc_lng":"77.30270590","loc_car":"loc by car","loc_train":"loc by train","loc_walk":"loc by walk","user_id":"3905","first_name":"Abhishek","last_name":"Chauhan","nickname":"Abhishek","avatar":"https://instafeed.org/storage/avatar/80x80-3844825a5c347c3f039c127d02b9154d.jpg","username":"abhishek","source":"w","latitude":null,"longitude":null},{"comments":[{"id":"12","comment":"he;lllooo","comment_id":null,"dt_added":"2017-07-27 18:05:09","dt_modified":"2017-07-27 18:05:09","status":"A","user_id":"3905","first_name":"Abhishek","last_name":"Chauhan","nickname":"Abhishek","avatar":"https://instafeed.org/storage/avatar/80x80-3844825a5c347c3f039c127d02b9154d.jpg","username":"abhishek"}]},{"images":[{"id":"77","image":"https://instafeed.org/storage/explorer_mime/256x170-af67a4081336abc28db2138cd2edc5fd.jpg","image_zoom":"https://instafeed.org/storage/explorer_mime/748x420-af67a4081336abc28db2138cd2edc5fd.jpg","image_original":"https://instafeed.org/storage/explorer_mime/af67a4081336abc28db2138cd2edc5fd.jpg","image_80x80":"https://instafeed.org/storage/explorer_mime/80x80-af67a4081336abc28db2138cd2edc5fd.jpg","image_100x100":"https://instafeed.org/storage/explorer_mime/100x100-af67a4081336abc28db2138cd2edc5fd.jpg","image_256x170":"https://instafeed.org/storage/explorer_mime/256x170-af67a4081336abc28db2138cd2edc5fd.jpg","image_264x200":"https://instafeed.org/storage/explorer_mime/264x200-af67a4081336abc28db2138cd2edc5fd.jpg","image_330x210":"https://instafeed.org/storage/explorer_mime/330x210-af67a4081336abc28db2138cd2edc5fd.jpg","image_360x200":"https://instafeed.org/storage/explorer_mime/360x200-af67a4081336abc28db2138cd2edc5fd.jpg"},{"id":"78","image":"https://instafeed.org/storage/explorer_mime/256x170-d21dec15be42d819b200c05422a22c20.jpg","image_zoom":"https://instafeed.org/storage/explorer_mime/748x420-d21dec15be42d819b200c05422a22c20.jpg","image_original":"https://instafeed.org/storage/explorer_mime/d21dec15be42d819b200c05422a22c20.jpg","image_80x80":"https://instafeed.org/storage/explorer_mime/80x80-d21dec15be42d819b200c05422a22c20.jpg","image_100x100":"https://instafeed.org/storage/explorer_mime/100x100-d21dec15be42d819b200c05422a22c20.jpg","image_256x170":"https://instafeed.org/storage/explorer_mime/256x170-d21dec15be42d819b200c05422a22c20.jpg","image_264x200":"https://instafeed.org/storage/explorer_mime/264x200-d21dec15be42d819b200c05422a22c20.jpg","image_330x210":"https://instafeed.org/storage/explorer_mime/330x210-d21dec15be42d819b200c05422a22c20.jpg","image_360x200":"https://instafeed.org/storage/explorer_mime/360x200-d21dec15be42d819b200c05422a22c20.jpg"}]},{"videos":[{"id":"9","video":"https://instafeed.org/storage/explorer_mime/7535ef04d3ce5c4202aa85d441bb4329.mp4","video_thumb":"https://instafeed.org/storage/explorer_mime/256x170-7535ef04d3ce5c4202aa85d441bb4329.jpg","vimeo_video_id":null,"vimeo_response":null}]}]
     */

    private int status;
    private String message;
    private List<DataBean> data;

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public List<DataBean> getData() {
        return data;
    }

    public void setData(List<DataBean> data) {
        this.data = data;
    }

    public static class DataBean {
        /**
         * id : 41
         * explorer_category_id : 10
         * name : Lifestyle
         * title : test explorer
         * slug : test-explorer-1867958090
         * short_description : explorer
         * description : explorer
         * total_up_votes : 1
         * total_down_votes : null
         * total_comments : 1
         * total_views : 3
         * total_flags : null
         * dt_added : 2017-07-27 18:04:47
         * status : P
         * loc_lat : 28.65021800
         * loc_lng : 77.30270590
         * loc_car : loc by car
         * loc_train : loc by train
         * loc_walk : loc by walk
         * user_id : 3905
         * first_name : Abhishek
         * last_name : Chauhan
         * nickname : Abhishek
         * avatar : https://instafeed.org/storage/avatar/80x80-3844825a5c347c3f039c127d02b9154d.jpg
         * username : abhishek
         * source : w
         * latitude : null
         * longitude : null
         * comments : [{"id":"12","comment":"he;lllooo","comment_id":null,"dt_added":"2017-07-27 18:05:09","dt_modified":"2017-07-27 18:05:09","status":"A","user_id":"3905","first_name":"Abhishek","last_name":"Chauhan","nickname":"Abhishek","avatar":"https://instafeed.org/storage/avatar/80x80-3844825a5c347c3f039c127d02b9154d.jpg","username":"abhishek"}]
         * images : [{"id":"77","image":"https://instafeed.org/storage/explorer_mime/256x170-af67a4081336abc28db2138cd2edc5fd.jpg","image_zoom":"https://instafeed.org/storage/explorer_mime/748x420-af67a4081336abc28db2138cd2edc5fd.jpg","image_original":"https://instafeed.org/storage/explorer_mime/af67a4081336abc28db2138cd2edc5fd.jpg","image_80x80":"https://instafeed.org/storage/explorer_mime/80x80-af67a4081336abc28db2138cd2edc5fd.jpg","image_100x100":"https://instafeed.org/storage/explorer_mime/100x100-af67a4081336abc28db2138cd2edc5fd.jpg","image_256x170":"https://instafeed.org/storage/explorer_mime/256x170-af67a4081336abc28db2138cd2edc5fd.jpg","image_264x200":"https://instafeed.org/storage/explorer_mime/264x200-af67a4081336abc28db2138cd2edc5fd.jpg","image_330x210":"https://instafeed.org/storage/explorer_mime/330x210-af67a4081336abc28db2138cd2edc5fd.jpg","image_360x200":"https://instafeed.org/storage/explorer_mime/360x200-af67a4081336abc28db2138cd2edc5fd.jpg"},{"id":"78","image":"https://instafeed.org/storage/explorer_mime/256x170-d21dec15be42d819b200c05422a22c20.jpg","image_zoom":"https://instafeed.org/storage/explorer_mime/748x420-d21dec15be42d819b200c05422a22c20.jpg","image_original":"https://instafeed.org/storage/explorer_mime/d21dec15be42d819b200c05422a22c20.jpg","image_80x80":"https://instafeed.org/storage/explorer_mime/80x80-d21dec15be42d819b200c05422a22c20.jpg","image_100x100":"https://instafeed.org/storage/explorer_mime/100x100-d21dec15be42d819b200c05422a22c20.jpg","image_256x170":"https://instafeed.org/storage/explorer_mime/256x170-d21dec15be42d819b200c05422a22c20.jpg","image_264x200":"https://instafeed.org/storage/explorer_mime/264x200-d21dec15be42d819b200c05422a22c20.jpg","image_330x210":"https://instafeed.org/storage/explorer_mime/330x210-d21dec15be42d819b200c05422a22c20.jpg","image_360x200":"https://instafeed.org/storage/explorer_mime/360x200-d21dec15be42d819b200c05422a22c20.jpg"}]
         * videos : [{"id":"9","video":"https://instafeed.org/storage/explorer_mime/7535ef04d3ce5c4202aa85d441bb4329.mp4","video_thumb":"https://instafeed.org/storage/explorer_mime/256x170-7535ef04d3ce5c4202aa85d441bb4329.jpg","vimeo_video_id":null,"vimeo_response":null}]
         */

        private String id;
        private String explorer_category_id;
        private String name;
        private String title;
        private String slug;
        private String short_description;
        private String description;
        private String total_up_votes;
        private String total_down_votes;
        private String total_comments;
        private String total_views;
        private Object total_flags;
        private String dt_added;
        private String status;
        private String loc_lat;
        private String loc_lng;
        private String loc_car;
        private String loc_train;
        private String loc_walk;
        private String user_id;
        private String first_name;
        private String last_name;
        private String nickname;
        private String avatar;
        private String username;
        private String source;
        private Object latitude;
        private Object longitude;
        private List<CommentsBean> comments;
        private List<ImagesBean> images;
        private List<VideosBean> videos;

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getExplorer_category_id() {
            return explorer_category_id;
        }

        public void setExplorer_category_id(String explorer_category_id) {
            this.explorer_category_id = explorer_category_id;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getTitle() {
            return title;
        }

        public void setTitle(String title) {
            this.title = title;
        }

        public String getSlug() {
            return slug;
        }

        public void setSlug(String slug) {
            this.slug = slug;
        }

        public String getShort_description() {
            return short_description;
        }

        public void setShort_description(String short_description) {
            this.short_description = short_description;
        }

        public String getDescription() {
            return description;
        }

        public void setDescription(String description) {
            this.description = description;
        }

        public String getTotal_up_votes() {
            return total_up_votes;
        }

        public void setTotal_up_votes(String total_up_votes) {
            this.total_up_votes = total_up_votes;
        }

        public String getTotal_down_votes() {
            return total_down_votes;
        }

        public void setTotal_down_votes(String total_down_votes) {
            this.total_down_votes = total_down_votes;
        }

        public String getTotal_comments() {
            return total_comments;
        }

        public void setTotal_comments(String total_comments) {
            this.total_comments = total_comments;
        }

        public String getTotal_views() {
            return total_views;
        }

        public void setTotal_views(String total_views) {
            this.total_views = total_views;
        }

        public Object getTotal_flags() {
            return total_flags;
        }

        public void setTotal_flags(Object total_flags) {
            this.total_flags = total_flags;
        }

        public String getDt_added() {
            return dt_added;
        }

        public void setDt_added(String dt_added) {
            this.dt_added = dt_added;
        }

        public String getStatus() {
            return status;
        }

        public void setStatus(String status) {
            this.status = status;
        }

        public String getLoc_lat() {
            return loc_lat;
        }

        public void setLoc_lat(String loc_lat) {
            this.loc_lat = loc_lat;
        }

        public String getLoc_lng() {
            return loc_lng;
        }

        public void setLoc_lng(String loc_lng) {
            this.loc_lng = loc_lng;
        }

        public String getLoc_car() {
            return loc_car;
        }

        public void setLoc_car(String loc_car) {
            this.loc_car = loc_car;
        }

        public String getLoc_train() {
            return loc_train;
        }

        public void setLoc_train(String loc_train) {
            this.loc_train = loc_train;
        }

        public String getLoc_walk() {
            return loc_walk;
        }

        public void setLoc_walk(String loc_walk) {
            this.loc_walk = loc_walk;
        }

        public String getUser_id() {
            return user_id;
        }

        public void setUser_id(String user_id) {
            this.user_id = user_id;
        }

        public String getFirst_name() {
            return first_name;
        }

        public void setFirst_name(String first_name) {
            this.first_name = first_name;
        }

        public String getLast_name() {
            return last_name;
        }

        public void setLast_name(String last_name) {
            this.last_name = last_name;
        }

        public String getNickname() {
            return nickname;
        }

        public void setNickname(String nickname) {
            this.nickname = nickname;
        }

        public String getAvatar() {
            return avatar;
        }

        public void setAvatar(String avatar) {
            this.avatar = avatar;
        }

        public String getUsername() {
            return username;
        }

        public void setUsername(String username) {
            this.username = username;
        }

        public String getSource() {
            return source;
        }

        public void setSource(String source) {
            this.source = source;
        }

        public Object getLatitude() {
            return latitude;
        }

        public void setLatitude(Object latitude) {
            this.latitude = latitude;
        }

        public Object getLongitude() {
            return longitude;
        }

        public void setLongitude(Object longitude) {
            this.longitude = longitude;
        }

        public List<CommentsBean> getComments() {
            return comments;
        }

        public void setComments(List<CommentsBean> comments) {
            this.comments = comments;
        }

        public List<ImagesBean> getImages() {
            return images;
        }

        public void setImages(List<ImagesBean> images) {
            this.images = images;
        }

        public List<VideosBean> getVideos() {
            return videos;
        }

        public void setVideos(List<VideosBean> videos) {
            this.videos = videos;
        }

        public static class CommentsBean {
            /**
             * id : 12
             * comment : he;lllooo
             * comment_id : null
             * dt_added : 2017-07-27 18:05:09
             * dt_modified : 2017-07-27 18:05:09
             * status : A
             * user_id : 3905
             * first_name : Abhishek
             * last_name : Chauhan
             * nickname : Abhishek
             * avatar : https://instafeed.org/storage/avatar/80x80-3844825a5c347c3f039c127d02b9154d.jpg
             * username : abhishek
             */

            private String id;
            private String comment;
            private Object comment_id;
            private String dt_added;
            private String dt_modified;
            private String status;
            private String user_id;
            private String first_name;
            private String last_name;
            private String nickname;
            private String avatar;
            private String username;

            public String getId() {
                return id;
            }

            public void setId(String id) {
                this.id = id;
            }

            public String getComment() {
                return comment;
            }

            public void setComment(String comment) {
                this.comment = comment;
            }

            public Object getComment_id() {
                return comment_id;
            }

            public void setComment_id(Object comment_id) {
                this.comment_id = comment_id;
            }

            public String getDt_added() {
                return dt_added;
            }

            public void setDt_added(String dt_added) {
                this.dt_added = dt_added;
            }

            public String getDt_modified() {
                return dt_modified;
            }

            public void setDt_modified(String dt_modified) {
                this.dt_modified = dt_modified;
            }

            public String getStatus() {
                return status;
            }

            public void setStatus(String status) {
                this.status = status;
            }

            public String getUser_id() {
                return user_id;
            }

            public void setUser_id(String user_id) {
                this.user_id = user_id;
            }

            public String getFirst_name() {
                return first_name;
            }

            public void setFirst_name(String first_name) {
                this.first_name = first_name;
            }

            public String getLast_name() {
                return last_name;
            }

            public void setLast_name(String last_name) {
                this.last_name = last_name;
            }

            public String getNickname() {
                return nickname;
            }

            public void setNickname(String nickname) {
                this.nickname = nickname;
            }

            public String getAvatar() {
                return avatar;
            }

            public void setAvatar(String avatar) {
                this.avatar = avatar;
            }

            public String getUsername() {
                return username;
            }

            public void setUsername(String username) {
                this.username = username;
            }
        }

        public static class ImagesBean {
            /**
             * id : 77
             * image : https://instafeed.org/storage/explorer_mime/256x170-af67a4081336abc28db2138cd2edc5fd.jpg
             * image_zoom : https://instafeed.org/storage/explorer_mime/748x420-af67a4081336abc28db2138cd2edc5fd.jpg
             * image_original : https://instafeed.org/storage/explorer_mime/af67a4081336abc28db2138cd2edc5fd.jpg
             * image_80x80 : https://instafeed.org/storage/explorer_mime/80x80-af67a4081336abc28db2138cd2edc5fd.jpg
             * image_100x100 : https://instafeed.org/storage/explorer_mime/100x100-af67a4081336abc28db2138cd2edc5fd.jpg
             * image_256x170 : https://instafeed.org/storage/explorer_mime/256x170-af67a4081336abc28db2138cd2edc5fd.jpg
             * image_264x200 : https://instafeed.org/storage/explorer_mime/264x200-af67a4081336abc28db2138cd2edc5fd.jpg
             * image_330x210 : https://instafeed.org/storage/explorer_mime/330x210-af67a4081336abc28db2138cd2edc5fd.jpg
             * image_360x200 : https://instafeed.org/storage/explorer_mime/360x200-af67a4081336abc28db2138cd2edc5fd.jpg
             */

            private String id;
            private String image;
            private String image_zoom;
            private String image_original;
            private String image_80x80;
            private String image_100x100;
            private String image_256x170;
            private String image_264x200;
            private String image_330x210;
            private String image_360x200;

            public String getId() {
                return id;
            }

            public void setId(String id) {
                this.id = id;
            }

            public String getImage() {
                return image;
            }

            public void setImage(String image) {
                this.image = image;
            }

            public String getImage_zoom() {
                return image_zoom;
            }

            public void setImage_zoom(String image_zoom) {
                this.image_zoom = image_zoom;
            }

            public String getImage_original() {
                return image_original;
            }

            public void setImage_original(String image_original) {
                this.image_original = image_original;
            }

            public String getImage_80x80() {
                return image_80x80;
            }

            public void setImage_80x80(String image_80x80) {
                this.image_80x80 = image_80x80;
            }

            public String getImage_100x100() {
                return image_100x100;
            }

            public void setImage_100x100(String image_100x100) {
                this.image_100x100 = image_100x100;
            }

            public String getImage_256x170() {
                return image_256x170;
            }

            public void setImage_256x170(String image_256x170) {
                this.image_256x170 = image_256x170;
            }

            public String getImage_264x200() {
                return image_264x200;
            }

            public void setImage_264x200(String image_264x200) {
                this.image_264x200 = image_264x200;
            }

            public String getImage_330x210() {
                return image_330x210;
            }

            public void setImage_330x210(String image_330x210) {
                this.image_330x210 = image_330x210;
            }

            public String getImage_360x200() {
                return image_360x200;
            }

            public void setImage_360x200(String image_360x200) {
                this.image_360x200 = image_360x200;
            }
        }

        public static class VideosBean {
            /**
             * id : 9
             * video : https://instafeed.org/storage/explorer_mime/7535ef04d3ce5c4202aa85d441bb4329.mp4
             * video_thumb : https://instafeed.org/storage/explorer_mime/256x170-7535ef04d3ce5c4202aa85d441bb4329.jpg
             * vimeo_video_id : null
             * vimeo_response : null
             */

            private String id;
            private String video;
            private String video_thumb;
            private Object vimeo_video_id;
            private Object vimeo_response;

            public String getId() {
                return id;
            }

            public void setId(String id) {
                this.id = id;
            }

            public String getVideo() {
                return video;
            }

            public void setVideo(String video) {
                this.video = video;
            }

            public String getVideo_thumb() {
                return video_thumb;
            }

            public void setVideo_thumb(String video_thumb) {
                this.video_thumb = video_thumb;
            }

            public Object getVimeo_video_id() {
                return vimeo_video_id;
            }

            public void setVimeo_video_id(Object vimeo_video_id) {
                this.vimeo_video_id = vimeo_video_id;
            }

            public Object getVimeo_response() {
                return vimeo_response;
            }

            public void setVimeo_response(Object vimeo_response) {
                this.vimeo_response = vimeo_response;
            }
        }
    }
}
