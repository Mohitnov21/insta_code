package com.abacusdesk.instafeed.instafeed.model;

import com.google.gson.Gson;

import java.util.List;

/**
 * Created by ishaan on 6/28/2017.
 */

public class Explorer_model {


    /**
     * status : 200
     * message : success
     * data : [{"id":"21","title":"explorer title","slug":"explorer-title-1122261430","short_description":"Lorem Ipsum is simply dummy text of the printing and typesetting industry\r\nLorem Ipsum is simply dummy text of the printing and typesetting industry\r\nLorem Ipsum is simply dummy text of the printing and typesetting industry","description":"Lorem Ipsum is simply dummy text of the printing and typesetting industry\r\nLorem Ipsum is simply dummy text of the printing and typesetting industry\r\nLorem Ipsum is simply dummy text of the printing and typesetting industry","tags":null,"explorer_category_id":"4","total_comments":null,"total_views":null,"total_up_votes":null,"total_down_votes":null,"total_flags":null,"location_id":null,"address":"New Delhi, Delhi, India","loc_lat":"0.00000000","loc_lng":"0.00000000","loc_car":"","loc_train":"","loc_walk":"","user_id":"3924","dt_added":"2017-06-28 17:48:11","dt_modified":"2017-06-28 17:48:11","status":"P"},{"id":"20","title":"explored testing","slug":"explored-testing-895274568","short_description":"Lorem Ipsum is simply dummy text of the printing and typesetting industry\r\nLorem Ipsum is simply dummy text of the printing and typesetting industry\r\nLorem Ipsum is simply dummy text of the printing and typesetting industry","description":"Lorem Ipsum is simply dummy text of the printing and typesetting industry\r\nLorem Ipsum is simply dummy text of the printing and typesetting industry\r\nLorem Ipsum is simply dummy text of the printing and typesetting industry","tags":null,"explorer_category_id":"5","total_comments":null,"total_views":null,"total_up_votes":null,"total_down_votes":null,"total_flags":null,"location_id":null,"address":"New Delhi, Delhi, India","loc_lat":"0.00000000","loc_lng":"0.00000000","loc_car":"","loc_train":"","loc_walk":"","user_id":"3924","dt_added":"2017-06-28 17:10:01","dt_modified":"2017-06-28 17:10:01","status":"P"}]
     */

    private int status;
    private String message;
    private List<DataBean> data;

    public static Explorer_model objectFromData(String str) {

        return new Gson().fromJson(str, Explorer_model.class);
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public List<DataBean> getData() {
        return data;
    }

    public void setData(List<DataBean> data) {
        this.data = data;
    }

    public static class DataBean {
        /**
         * id : 21
         * title : explorer title
         * slug : explorer-title-1122261430
         * short_description : Lorem Ipsum is simply dummy text of the printing and typesetting industry
         Lorem Ipsum is simply dummy text of the printing and typesetting industry
         Lorem Ipsum is simply dummy text of the printing and typesetting industry
         * description : Lorem Ipsum is simply dummy text of the printing and typesetting industry
         Lorem Ipsum is simply dummy text of the printing and typesetting industry
         Lorem Ipsum is simply dummy text of the printing and typesetting industry
         * tags : null
         * explorer_category_id : 4
         * total_comments : null
         * total_views : null
         * total_up_votes : null
         * total_down_votes : null
         * total_flags : null
         * location_id : null
         * address : New Delhi, Delhi, India
         * loc_lat : 0.00000000
         * loc_lng : 0.00000000
         * loc_car :
         * loc_train :
         * loc_walk :
         * user_id : 3924
         * dt_added : 2017-06-28 17:48:11
         * dt_modified : 2017-06-28 17:48:11
         * status : P
         */

        private String id;
        private String title;
        private String slug;
        private String short_description;
        private String description;
        private Object tags;
        private String explorer_category_id;
        private Object total_comments;
        private Object total_views;
        private Object total_up_votes;
        private Object total_down_votes;
        private Object total_flags;
        private Object location_id;
        private String address;
        private String loc_lat;
        private String loc_lng;
        private String loc_car;
        private String loc_train;

        public String getImage() {
            return image;
        }

        public void setImage(String image) {
            this.image = image;
        }

        private String image;
        private String loc_walk;
        private String user_id;
        private String dt_added;
        private String dt_modified;
        private String status;

        public static DataBean objectFromData(String str) {

            return new Gson().fromJson(str, DataBean.class);
        }

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getTitle() {
            return title;
        }

        public void setTitle(String title) {
            this.title = title;
        }

        public String getSlug() {
            return slug;
        }

        public void setSlug(String slug) {
            this.slug = slug;
        }

        public String getShort_description() {
            return short_description;
        }

        public void setShort_description(String short_description) {
            this.short_description = short_description;
        }

        public String getDescription() {
            return description;
        }

        public void setDescription(String description) {
            this.description = description;
        }

        public Object getTags() {
            return tags;
        }

        public void setTags(Object tags) {
            this.tags = tags;
        }

        public String getExplorer_category_id() {
            return explorer_category_id;
        }

        public void setExplorer_category_id(String explorer_category_id) {
            this.explorer_category_id = explorer_category_id;
        }

        public Object getTotal_comments() {
            return total_comments;
        }

        public void setTotal_comments(Object total_comments) {
            this.total_comments = total_comments;
        }

        public Object getTotal_views() {
            return total_views;
        }

        public void setTotal_views(Object total_views) {
            this.total_views = total_views;
        }

        public Object getTotal_up_votes() {
            return total_up_votes;
        }

        public void setTotal_up_votes(Object total_up_votes) {
            this.total_up_votes = total_up_votes;
        }

        public Object getTotal_down_votes() {
            return total_down_votes;
        }

        public void setTotal_down_votes(Object total_down_votes) {
            this.total_down_votes = total_down_votes;
        }

        public Object getTotal_flags() {
            return total_flags;
        }

        public void setTotal_flags(Object total_flags) {
            this.total_flags = total_flags;
        }

        public Object getLocation_id() {
            return location_id;
        }

        public void setLocation_id(Object location_id) {
            this.location_id = location_id;
        }

        public String getAddress() {
            return address;
        }

        public void setAddress(String address) {
            this.address = address;
        }

        public String getLoc_lat() {
            return loc_lat;
        }

        public void setLoc_lat(String loc_lat) {
            this.loc_lat = loc_lat;
        }

        public String getLoc_lng() {
            return loc_lng;
        }

        public void setLoc_lng(String loc_lng) {
            this.loc_lng = loc_lng;
        }

        public String getLoc_car() {
            return loc_car;
        }

        public void setLoc_car(String loc_car) {
            this.loc_car = loc_car;
        }

        public String getLoc_train() {
            return loc_train;
        }

        public void setLoc_train(String loc_train) {
            this.loc_train = loc_train;
        }

        public String getLoc_walk() {
            return loc_walk;
        }

        public void setLoc_walk(String loc_walk) {
            this.loc_walk = loc_walk;
        }

        public String getUser_id() {
            return user_id;
        }

        public void setUser_id(String user_id) {
            this.user_id = user_id;
        }

        public String getDt_added() {
            return dt_added;
        }

        public void setDt_added(String dt_added) {
            this.dt_added = dt_added;
        }

        public String getDt_modified() {
            return dt_modified;
        }

        public void setDt_modified(String dt_modified) {
            this.dt_modified = dt_modified;
        }

        public String getStatus() {
            return status;
        }

        public void setStatus(String status) {
            this.status = status;
        }
    }
}
