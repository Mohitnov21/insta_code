package com.abacusdesk.instafeed.instafeed.fragments;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.support.annotation.Nullable;
import android.support.annotation.RequiresApi;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.abacusdesk.instafeed.instafeed.NavDrawerActivity;
import com.abacusdesk.instafeed.instafeed.R;
import com.abacusdesk.instafeed.instafeed.Util.Alerts;
import com.abacusdesk.instafeed.instafeed.Util.Apis;
import com.abacusdesk.instafeed.instafeed.Util.CommonFunctions;
import com.abacusdesk.instafeed.instafeed.Util.SaveSharedPreference;
import com.abacusdesk.instafeed.instafeed.adapter.Comment_Adapter;
import com.abacusdesk.instafeed.instafeed.adapter.RecyclerCommentAdapter;
import com.abacusdesk.instafeed.instafeed.adapter.ViewPagerAdapter;
import com.abacusdesk.instafeed.instafeed.custom_dialog.Dialogs;
import com.abacusdesk.instafeed.instafeed.model.Opiniondetail_model;
import com.abacusdesk.instafeed.instafeed.model.Petitiondetail_model;
import com.android.volley.AuthFailureError;
import com.android.volley.NoConnectionError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.bumptech.glide.Glide;
import com.google.gson.Gson;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Timer;

/**
 * Created by abacusdesk on 2017-08-08.
 */

public class Petitiondetail_fragment extends Fragment implements View.OnClickListener{

    View view,view1;
    Petitiondetail_model petitiondetail_model;
    TextView txtdescr,txtshortdescr,txtdate,txtplace,txttitle;
    TextView txtcomment,txtvisible,txtusername;
    ImageView img_back,img_forward;
    public  EditText edcomment;

    ImageView imgshare;

    public  String id="",postid="",temp,temp2,comment="";

    public  ArrayList<Petitiondetail_model.DataBean.CommentsBean> arrayComment= new ArrayList<>();
    // ArrayList<NewsDetail.DataBean.ImagesBean> arrayImages= new ArrayList<>();
    Petitiondetail_model.DataBean dataBean_detail,databean_comment,databean_images,databean_videos;
    public  ArrayList<HashMap<String,String>> arrayhashComent=new ArrayList<>();
    public  RecyclerCommentAdapter adapter;
    ViewPagerAdapter viewPagerAdapter;
    FrameLayout lnlistview;
    ViewPager imgviewpager;
    Boolean flag=false;

    LinearLayout ln_userdetail,lncomment,lnlocation,lndown,lnlike,lnlikesection;
    public  ArrayList<HashMap<String,String>> arrayviewcontainer=new ArrayList<>();
    Boolean like=false,dislike=false,fav=false;
    TextView txt_ownername,txtlike,txtdislike,txtname;
    ImageView img_userhead,imgonview;
    String location;
    Button btn_signpetition;
    Bitmap imagebit;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.detail_finallayout, container, false);
        init();
        getActivity().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_PAN |WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);
        return view;
    }

    private void init(){
        comment_click();
        if (!arrayviewcontainer.isEmpty()){
            arrayviewcontainer.clear();
        }
        if (!arrayhashComent.isEmpty()){
            arrayhashComent.clear();
        }

        Bundle bundle = this.getArguments();
        if (bundle != null) {
            id=bundle.getString("id");
        }

        if (NavDrawerActivity.toolbar!=null){
            NavDrawerActivity.toolbar.setLogo(null);
            NavDrawerActivity.toolbar.setTitle("Petitions");
        }
        ln_userdetail=(LinearLayout)view.findViewById(R.id.ln_publicprofile);
        lnlikesection=(LinearLayout)view.findViewById(R.id.ln_likesection);
        lnlikesection.setVisibility(View.GONE);

        lndown=(LinearLayout)view.findViewById(R.id.ln_down);
        lnlike=(LinearLayout)view.findViewById(R.id.ln_like);

        init_related();
        imgviewpager=(ViewPager)view.findViewById(R.id.img_viewpager);
        imgshare=(ImageView)view.findViewById(R.id.img_share);
        lncomment=(LinearLayout)view.findViewById(R.id.ln_comment);

        imgonview=(ImageView)view.findViewById(R.id.img_view);
        txt_ownername=(TextView)view.findViewById(R.id.txt_owner);
       // NavDrawerActivity.actionMenu.close(true);
        img_back=(ImageView)view.findViewById(R.id.img_left);
        img_forward=(ImageView)view.findViewById(R.id.img_right);
        view1=(View)view.findViewById(R.id.view1);
        //view1.setVisibility(View.GONE);

        lnlike.setVisibility(View.GONE);
        lndown.setVisibility(View.GONE);

        img_back.setOnClickListener(this);
        img_forward.setOnClickListener(this);
        ln_userdetail.setOnClickListener(this);

        lnlocation=(LinearLayout)view.findViewById(R.id.ln_location);
        txtlike=(TextView)view.findViewById(R.id.txt_like);
        txtdislike=(TextView)view.findViewById(R.id.txt_dislike);

        txtcomment=(TextView)view.findViewById(R.id.txt_comment);

        imgshare.setOnClickListener(this);
        txtlike.setOnClickListener(this);
        txtdislike.setOnClickListener(this);
        lncomment.setOnClickListener(this);
        txtcomment.setOnClickListener(this);

        //txtname=(TextView)view.findViewById(R.id.txt_name);

        txtdescr=(TextView)view.findViewById(R.id.txt_shortdescr);
        txtshortdescr=(TextView)view.findViewById(R.id.txt_descrshort);

        txtdate=(TextView)view.findViewById(R.id.txt_date);
        txtplace=(TextView)view.findViewById(R.id.txt_place);
        txttitle=(TextView)view.findViewById(R.id.txt_title);


        btn_signpetition=(Button)view.findViewById(R.id.btn_signpetition);

        btn_signpetition.setOnClickListener(this);

        imgshare.setOnClickListener(this);

        temp= Apis.petition_detail.replace("id",id);

        view.setFocusableInTouchMode(true);
        view.requestFocus();
        view.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if (keyCode == KeyEvent.KEYCODE_BACK) {
                    // ViewPagerAdapter.stopVideo(getActivity());
                    getActivity().getSupportFragmentManager().popBackStack();
                    return true;
                } else {
                    return false;
                }
            }
        });

        getPetitiondetail(getActivity());
        //getComments(getActivity());
        try{
            viewPagerAdapter=new ViewPagerAdapter(getActivity(),arrayviewcontainer,"petitions");
            imgviewpager.setAdapter(viewPagerAdapter);

        } catch (Exception e){
            e.printStackTrace();
        }
        // JCVideoPlayer.releaseAllVideos();
        imgviewpager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }
            @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
            @Override
            public void onPageSelected(int position) {

                int currentPage = imgviewpager.getCurrentItem();
                int totalPages = imgviewpager.getAdapter().getCount();

                int nextPage = currentPage+1;
                int previousPage = currentPage-1;

                if (nextPage >= totalPages) {
                    img_forward.setVisibility(View.INVISIBLE);
                    //nextPage = 0;
                }else {
                    img_forward.setVisibility(View.VISIBLE);
                }

                if (previousPage < 0) {
                    img_back.setVisibility(View.INVISIBLE);
                }else {
                    img_back.setVisibility(View.VISIBLE);
                }

            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }

        });

    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Log.e("oncreate","");
        setHasOptionsMenu(true);
    }

    private void comment_click(){
        EditText edcomment=(EditText)view.findViewById(R.id.ed_comment);
        edcomment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getcommentlist();
            }
        });
    }

  /*  @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {
            case R.id.menu_comment:
                try{
                    CommentsList_fragment fragment = new CommentsList_fragment();
                    Bundle bundle = new Bundle();
                    bundle.putSerializable("arrayhash",arrayhashComent);
                    bundle.putString("postid",id);
                    bundle.putString("type","petitions");
                    fragment.setArguments(bundle);
                    AppCompatActivity activity = (AppCompatActivity) getContext();
                    activity.getSupportFragmentManager().beginTransaction()
                            .add(R.id.frame_container,fragment,"comment").addToBackStack(null).commit();
                }catch (Exception e){

                }
                return true;
            case R.id.menu_share:
                try {
                    MyAsync obj = new MyAsync() {
                        @Override
                        protected void onPostExecute(Bitmap bmp) {
                            super.onPostExecute(bmp);

                            imagebit = bmp;
                            Log.e("imagebit2", "" + imagebit);
                            ByteArrayOutputStream bytes = new ByteArrayOutputStream();
                            imagebit.compress(Bitmap.CompressFormat.JPEG, 100, bytes);
                            File f = new File(Environment.getExternalStorageDirectory() + File.separator + "temporary_file.jpg");
                            try {
                                f.createNewFile();
                                FileOutputStream fo = new FileOutputStream(f);
                                fo.write(bytes.toByteArray());

                            } catch (IOException e) {
                                e.printStackTrace();
                            }
                        }
                    };
                    if (!databean_images.getImages().isEmpty())
                        obj.execute();

                }  catch (Exception e){

                }

                Intent sharingIntent = new Intent(Intent.ACTION_SEND);
                //sharingIntent.putExtra(Intent.EXTRA_TEXT,urlShare+dataBean_detail.getSlug());
                try{
                    if (!databean_images.getImages().isEmpty())
                        sharingIntent.putExtra(Intent.EXTRA_STREAM, Uri.parse("file:///sdcard/temporary_file.jpg"));
                    // sharingIntent.putExtra(Intent.EXTRA_SUBJECT,dataBean_detail.getTitle());
                    sharingIntent.putExtra(Intent.EXTRA_TEXT, dataBean_detail.getTitle()+"\n"+Apis.petition_share+dataBean_detail.getSlug());
                    Log.e("shreinterjyt",""+sharingIntent);
                    startActivity(Intent.createChooser(sharingIntent, "Share via"));
                }catch (Exception e){
                    Log.e("exception",""+e);
                }
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }*/

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        // TODO your code to hide item here
        Log.e("settings","");
        menu.findItem(R.id.menu_comment).setVisible(true);
        menu.findItem(R.id.menu_share).setVisible(true);
        menu.findItem(R.id.action_form).setVisible(false);
        menu.findItem(R.id.action_settings).setVisible(false);
        super.onCreateOptionsMenu(menu, inflater);
    }

    public void sharecontent(){
        try {
            MyAsync obj = new MyAsync() {
                @Override
                protected void onPostExecute(Bitmap bmp) {
                    super.onPostExecute(bmp);

                    imagebit = bmp;
                    Log.e("imagebit2", "" + imagebit);
                    ByteArrayOutputStream bytes = new ByteArrayOutputStream();
                    imagebit.compress(Bitmap.CompressFormat.JPEG, 100, bytes);
                    File f = new File(Environment.getExternalStorageDirectory() + File.separator + "temporary_file.jpg");
                    try {
                        f.createNewFile();
                        FileOutputStream fo = new FileOutputStream(f);
                        fo.write(bytes.toByteArray());

                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            };
            if (!databean_images.getImages().isEmpty())
                obj.execute();

        }  catch (Exception e){

        }

        Intent sharingIntent = new Intent(Intent.ACTION_SEND);
        sharingIntent.setType("*/*");
        //sharingIntent.putExtra(Intent.EXTRA_TEXT,urlShare+dataBean_detail.getSlug());
        try{
            if (!databean_images.getImages().isEmpty()){
                sharingIntent.putExtra(Intent.EXTRA_STREAM, Uri.parse("file:///sdcard/temporary_file.jpg"));
            }else {
                sharingIntent.setType("text/plain");
            }
            // sharingIntent.putExtra(Intent.EXTRA_SUBJECT,dataBean_detail.getTitle());
            sharingIntent.putExtra(Intent.EXTRA_TEXT, dataBean_detail.getTitle()+"\n"+Apis.petition_share+dataBean_detail.getSlug());
            Log.e("shreinterjyt",""+sharingIntent);
            startActivity(Intent.createChooser(sharingIntent, "Share via"));
        }catch (Exception e){
            Log.e("exception",""+e);
        }
    }

    public void getcommentlist(){
        try{
            CommentsList_fragment fragment = new CommentsList_fragment();
            Bundle bundle = new Bundle();
            bundle.putSerializable("arrayhash",arrayhashComent);
            bundle.putString("postid",id);
            bundle.putString("type","petitions");
            fragment.setArguments(bundle);
            AppCompatActivity activity = (AppCompatActivity) getContext();
            activity.getSupportFragmentManager().beginTransaction()
                    .add(R.id.frame_container,fragment,"comment").addToBackStack(null).commit();
        }catch (Exception e){

        }
    }

    private void init_related(){
        LinearLayout ln_recyclerrelated=(LinearLayout)view.findViewById(R.id.ln_recycler);
        ln_recyclerrelated.setVisibility(View.GONE);
    }

    private void nextPage() {
        int currentPage = imgviewpager.getCurrentItem();
        int totalPages = imgviewpager.getAdapter().getCount();

        int nextPage = currentPage+1;
        if (nextPage >= totalPages) {
            // We can't go forward anymore.
            // Loop to the first page. If you don't want looping just
            // return here.
            nextPage = 0;
        }
        imgviewpager.setCurrentItem(nextPage, true);
    }

    private void previousPage() {
        int currentPage = imgviewpager.getCurrentItem();
        int totalPages = imgviewpager.getAdapter().getCount();

        int previousPage = currentPage-1;
        if (previousPage < 0) {
            // We can't go back anymore.
            // Loop to the last page. If you don't want looping just
            // return here.
            previousPage = totalPages - 1;
        }
        imgviewpager.setCurrentItem(previousPage, true);
    }

    @Override
    public void onResume() {
        super.onResume();
        // adapter.notifyDataSetChanged();
    }

    public   void isPetitionSigned(final Context context) {
        //Dialogs.showProDialog(context, "Loading");
        Log.e("temporaryurl",""+temp);
        StringRequest stringRequest = new StringRequest(Request.Method.POST,Apis.petition_Issign ,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.e("log", "" + response);
                        btn_signpetition.setVisibility(View.VISIBLE);
                        try {
                        JSONObject jsonObject=new JSONObject(response);
                          if (jsonObject.getString("status").equals("200") && jsonObject.getString("message").equalsIgnoreCase("success")){
                            if (jsonObject.getJSONObject("data").getString("is_sign").equalsIgnoreCase("true")){
                                btn_signpetition.setText("Petition Signed");
                                btn_signpetition.setEnabled(false);
                            }
                           }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        Dialogs.disDialog();
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Dialogs.disDialog();
                        DialogInterface.OnClickListener onClickTryAgain = new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                Dialogs.disDialog();
                                isPetitionSigned(context);
                            }
                        };
                        if (error instanceof TimeoutError) {
                            Dialogs.disDialog();
                            Alerts.timeoutErrorAlert(context, onClickTryAgain);
                        } else if (error instanceof NoConnectionError) {
                            Dialogs.disDialog();
                            Alerts.internetConnectionErrorAlert(context, onClickTryAgain);
                        }
                    }
                })

        {

            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> map = new HashMap<String, String>();
                map.put("user", SaveSharedPreference.getUserID(context));
                map.put("token", SaveSharedPreference.getPrefToken(context));
                map.put("id",id);;
                Log.e("mapcomment",""+map);
                return map;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Content-Type", "application/x-www-form-urlencoded");
                return params;
            }
        };
        RequestQueue requestQueue = Volley.newRequestQueue(context);
        requestQueue.add(stringRequest);
    }

    public  void editComment(final Context context, final String comment, final String comment_id) {
        StringRequest stringRequest = new StringRequest(Request.Method.POST, Apis.petition_commentedit ,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.e("log", "" + response);
                        try {
                            responseEdit(response,context);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Dialogs.disDialog();
                        //Toast.makeText(context, error.toString(), Toast.LENGTH_LONG).show();
                    }
                })
         {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> map = new HashMap<String, String>();
                map.put("user", SaveSharedPreference.getUserID(context));
                map.put("token", SaveSharedPreference.getPrefToken(context));
                map.put("id",comment_id);
                map.put("comment",comment);
                Log.e("mapcomment",""+map);
                return map;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Content-Type", "application/x-www-form-urlencoded");
                return params;
            }
        };

        RequestQueue requestQueue = Volley.newRequestQueue(context);
        requestQueue.add(stringRequest);
    }

    public  void responseEdit(String response,Context context ) throws JSONException {
        JSONObject jsonObject=new JSONObject(response);
        if (jsonObject.getString("message").equalsIgnoreCase("success")){
            edcomment.getText().clear();

            adapter.notifyDataSetChanged();
            getPetitiondetail(context);
            Toast.makeText(context,jsonObject.getString("message"),Toast.LENGTH_SHORT).show();

        }else {
            Toast.makeText(context,jsonObject.getString("message"),Toast.LENGTH_SHORT).show();
        }
    }

    public   void getPetitiondetail(final Context context) {
        Dialogs.showProDialog(context, "Loading");
        Log.e("temporaryurl",""+temp);
        StringRequest stringRequest = new StringRequest(Request.Method.GET,temp ,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.e("log", "" + response);
                        try {
                            getResponse(response,context);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        Dialogs.disDialog();
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Dialogs.disDialog();
                        DialogInterface.OnClickListener onClickTryAgain = new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                Dialogs.disDialog();
                                getPetitiondetail(context);
                            }
                        };
                        if (error instanceof TimeoutError) {
                            Dialogs.disDialog();
                            Alerts.timeoutErrorAlert(context, onClickTryAgain);
                        } else if (error instanceof NoConnectionError) {
                            Dialogs.disDialog();
                            Alerts.internetConnectionErrorAlert(context, onClickTryAgain);
                        }
                    }
                })

        {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Content-Type", "application/x-www-form-urlencoded");
                return params;
            }
        };
        RequestQueue requestQueue = Volley.newRequestQueue(context);
        requestQueue.add(stringRequest);
    }

    public  String getDate(String date) throws ParseException {
        String myFormat = "yyyy-MM-dd HH:mm:ss";
        DateFormat sdformat = new SimpleDateFormat(myFormat);
        DateFormat targetFormat = new SimpleDateFormat("MMMM dd, yyyy");
        String formattedDate = targetFormat.format(sdformat.parse(date));
        return formattedDate;
    }

    public  void getResponse(String response,Context context ) throws JSONException {
        if(!response.equals(null)) {
            Log.e("log", "" + response);
            JSONObject jsonObject=new JSONObject(response);
            Object object=jsonObject.get("data");
            if (object instanceof JSONArray) {
                Gson gson = new Gson();
                petitiondetail_model = gson.fromJson(response,Petitiondetail_model.class);
                int status = petitiondetail_model.getStatus();
                String msg = petitiondetail_model.getMessage();
                if (msg.equalsIgnoreCase("success") && status == 200) {
                    dataBean_detail = petitiondetail_model.getData().get(0);
                    databean_comment = petitiondetail_model.getData().get(1);
                    databean_images = petitiondetail_model.getData().get(2);

                    if (dataBean_detail != null) {
                        lnlocation.setVisibility(View.VISIBLE);
                        ln_userdetail.setVisibility(View.VISIBLE);
                        Log.e(",jhkdlfn",""+dataBean_detail.getAvatar());

                        txt_ownername.setText(dataBean_detail.getUsername());

                        try{
                            if (dataBean_detail.getLocation()!=null)
                            {
                                txtplace.setText(dataBean_detail.getLocation());
                            }else {
                                lnlocation.setVisibility(View.GONE);
                            }
                        }
                        catch (Exception e){
                            e.printStackTrace();
                        }
                   }

                        /* if (dataBean_detail.getTotal_signs()!=null){
                             txtname.setText("Total Signs:"+dataBean_detail.getTotal_signs());
                         }*/

                   txttitle.setText(dataBean_detail.getTitle());
                   Log.e("description", "" + dataBean_detail.getDescription());

                    txtdescr.setText(dataBean_detail.getDescription());
                    txtshortdescr.setText(dataBean_detail.getShort_description());

                    if (txtshortdescr.getText().toString().equals("")){
                        txtshortdescr.setVisibility(View.GONE);
                    }
                       if (dataBean_detail.getTotal_comments()!=null){
                           txtcomment.setText(dataBean_detail.getTotal_comments());
                       }

                        if (dataBean_detail.getTotal_views() != null) {
                           // txtvisible.setText("" + dataBean_detail.getTotal_views());
                        }

                        try {
                            txtdate.setText("" + getDate(dataBean_detail.getDt_added()));
                        } catch (ParseException e) {
                            e.printStackTrace();
                        }

                    } else {
                        ln_userdetail.setEnabled(false);
                        ln_userdetail.setClickable(false);
                    }
                    isPetitionSigned(context);
                    showCommentList(context);
                    if (!arrayviewcontainer.isEmpty()) {
                        arrayviewcontainer.clear();
                    }
                    showImages(context);
                }
            }
        }

    private  void showCommentList(Context context){
        if (!arrayhashComent.isEmpty()){
            arrayhashComent.clear();
        }
        if (!databean_comment.getComments().isEmpty()){
            HashMap<String,String> hashMap;
            for (int i = 0; i < databean_comment.getComments().size(); i++) {
                try {
                    hashMap=new HashMap<>();
                    Petitiondetail_model.DataBean.CommentsBean commentsBean= databean_comment.getComments().get(i);
                    hashMap.put("commentid",commentsBean.getId());
                    hashMap.put("comment",commentsBean.getComment());
                    hashMap.put("userid",commentsBean.getUser_id());
                    hashMap.put("avatar",commentsBean.getAvatar());
                    hashMap.put("username",commentsBean.getUsername());
                    arrayhashComent.add(hashMap);
                }  catch (Exception e){
                    e.printStackTrace();
                }
            }
            //getComments(context);
            //adapter.notifyDataSetChanged();
        }
    }

    private  void showImages(Context context){
        if (databean_images.getImages().size()!=0){
            HashMap<String,String> hashMap;
            for (int i = 0; i < databean_images.getImages().size(); i++) {
                try {
                    hashMap=new HashMap<>();
                    Petitiondetail_model.DataBean.ImagesBean imagesBean= databean_images.getImages().get(i);
                    hashMap.put("image",imagesBean.getImage_330x210());
                    hashMap.put("imagezoom",imagesBean.getImage_zoom());
                    arrayviewcontainer.add(hashMap);
                }catch (Exception e){
                    e.printStackTrace();
                }
            }
        }
        getVideos(context);
        // viewPagerAdapter.notifyDataSetChanged();
    }

    private  void getVideos(Context context){
        Log.e("total_container",""+arrayviewcontainer);
        viewPagerAdapter.notifyDataSetChanged();
        if (arrayviewcontainer.isEmpty()){
            imgviewpager.setVisibility(View.GONE);
            imgonview.setVisibility(View.VISIBLE);
            imgonview.setImageResource(R.drawable.petitiondefault);
            img_back.setVisibility(View.INVISIBLE);
            img_forward.setVisibility(View.INVISIBLE);
        }else if (arrayviewcontainer.size()>=1){

            int currentPage = imgviewpager.getCurrentItem();
            int totalPages = imgviewpager.getAdapter().getCount();

            int nextPage = currentPage+1;
            int previousPage = currentPage-1;

            if (nextPage >= totalPages) {
                img_forward.setVisibility(View.INVISIBLE);
                //nextPage = 0;
            }else {
                img_forward.setVisibility(View.VISIBLE);
            }

            if (previousPage < 0) {
                img_back.setVisibility(View.INVISIBLE);
            }else {
                img_back.setVisibility(View.VISIBLE);
            }

        }
    }

    @Override
    public void onPause() {
        super.onPause();
    }

    public void postComment() {
        Log.e("temporaryurl",""+temp2);
        StringRequest stringRequest = new StringRequest(Request.Method.POST, Apis.petition_comment ,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.e("log", "" + response);
                        try {
                            resComment(response);
                            txtcomment.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.comment_click, 0);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {

                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Dialogs.disDialog();
                        DialogInterface.OnClickListener onClickTryAgain = new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                Dialogs.disDialog();
                                postComment();
                            }
                        };
                        if (error instanceof TimeoutError) {
                            Dialogs.disDialog();
                            Alerts.timeoutErrorAlert(getContext(), onClickTryAgain);
                        } else if (error instanceof NoConnectionError) {
                            Dialogs.disDialog();
                            Alerts.internetConnectionErrorAlert(getContext(), onClickTryAgain);
                        }
                    }
                })
        {


            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> map = new HashMap<String, String>();
                map.put("user", SaveSharedPreference.getUserID(getActivity()));
                map.put("token", SaveSharedPreference.getPrefToken(getActivity()));
                map.put("id",id);
                map.put("comment",comment);
                Log.e("mapcomment",""+map);
                return map;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Content-Type", "application/x-www-form-urlencoded");
                return params;
            }
        };

        RequestQueue requestQueue = Volley.newRequestQueue(getActivity());
        requestQueue.add(stringRequest);
    }

    private void resComment(String response ) throws JSONException {
        JSONObject jsonObject=new JSONObject(response);
        if (jsonObject.getString("message").equalsIgnoreCase("success")){
            edcomment.getText().clear();
            getPetitiondetail(getActivity());
            lnlistview.setVisibility(View.VISIBLE);
            flag=true;
            Toast.makeText(getActivity(),jsonObject.getString("message"),Toast.LENGTH_SHORT).show();

        }else {
            Toast.makeText(getActivity(),jsonObject.getString("message"),Toast.LENGTH_SHORT).show();
        }
    }


    public void signPetition() {
        StringRequest stringRequest = new StringRequest(Request.Method.POST, Apis.petition_sign ,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.e("log", "" + response);
                        try {
                            JSONObject jsonObject= new JSONObject(response);
                            if (jsonObject.getString("status").equals("200") && jsonObject.getString("message").equalsIgnoreCase("success")){
                                btn_signpetition.setText("Signed");
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {

                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Dialogs.disDialog();
                        DialogInterface.OnClickListener onClickTryAgain = new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                Dialogs.disDialog();
                                signPetition();
                            }
                        };
                        if (error instanceof TimeoutError) {
                            Dialogs.disDialog();
                            Alerts.timeoutErrorAlert(getContext(), onClickTryAgain);
                        } else if (error instanceof NoConnectionError) {
                            Dialogs.disDialog();
                            Alerts.internetConnectionErrorAlert(getContext(), onClickTryAgain);
                        }
                    }
                })
        {


            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> map = new HashMap<String, String>();
                map.put("user", SaveSharedPreference.getUserID(getActivity()));
                map.put("token", SaveSharedPreference.getPrefToken(getActivity()));
                map.put("id",id);
                Log.e("mapsigned",""+map);
                return map;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Content-Type", "application/x-www-form-urlencoded");
                return params;
            }
        };

        RequestQueue requestQueue = Volley.newRequestQueue(getActivity());
        requestQueue.add(stringRequest);
    }
    // TODO on like colour of like button should be changed
    // similarly dislike and also set up icons fav may be modidification in how the comments section should be shown
    // some ids which are invalid in detail news section and make change the profile picture of newsfeed
    // islike paramter in detail section

    public  class MyAsync extends AsyncTask<Void, Void, Bitmap> {

        @Override
        protected Bitmap doInBackground(Void... params) {
            try {
                if (!databean_images.getImages().isEmpty()){
                    URL url = new URL(databean_images.getImages().get(0).getImage_100x100());
                    HttpURLConnection connection = (HttpURLConnection) url.openConnection();
                    connection.setDoInput(true);
                    connection.connect();
                    InputStream input = connection.getInputStream();
                    Bitmap myBitmap = BitmapFactory.decodeStream(input);
                    return myBitmap;
                }else {
                    return null;
                }
            } catch (IOException e) {
                e.printStackTrace();
                return null;
            }catch(Exception e){
                return null;
            }
        }
    }


    @Override
    public void onClick(View v) {
        if (v==img_back){
            previousPage();
        } else if (v==img_forward){
            nextPage();
        } else if (v==btn_signpetition){
            signPetition();
        } else if (v==ln_userdetail) {
            if (dataBean_detail.getUser_id().equals(SaveSharedPreference.getUserID(getActivity()))) {
                Publicprofile_Fragment.flag = false;
                AppCompatActivity activity = (AppCompatActivity) v.getContext();
                activity.getSupportFragmentManager().beginTransaction()
                        .add(R.id.frame_container, new Publicprofile_Fragment(), "public").addToBackStack("public").commit();
            } else {
                Publicprofile_Fragment.flag = true;
                Publicprofile_Fragment.username = dataBean_detail.getUsername();
                AppCompatActivity activity = (AppCompatActivity) v.getContext();
                activity.getSupportFragmentManager().beginTransaction()
                        .add(R.id.frame_container, new Publicprofile_Fragment(), "public").addToBackStack("public").commit();
            }
        }
    }
}