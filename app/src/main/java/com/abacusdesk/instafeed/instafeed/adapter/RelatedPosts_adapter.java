package com.abacusdesk.instafeed.instafeed.adapter;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;

import com.abacusdesk.instafeed.instafeed.R;
import com.abacusdesk.instafeed.instafeed.Util.SaveSharedPreference;
import com.abacusdesk.instafeed.instafeed.fragments.Blogdetail_fragment;
import com.abacusdesk.instafeed.instafeed.fragments.Explore_detailfragment;
import com.abacusdesk.instafeed.instafeed.fragments.Newsdetail_fragment;
import com.abacusdesk.instafeed.instafeed.fragments.Opiniondetail_fragment;
import com.abacusdesk.instafeed.instafeed.fragments.Petitiondetail_fragment;
import com.abacusdesk.instafeed.instafeed.fragments.Talentdetail_fragment;
import com.bumptech.glide.Glide;

import java.util.ArrayList;
import java.util.HashMap;

import cn.jzvd.JZVideoPlayer;


/**
 * Created by abacusdesk on 2017-10-05.
 */

public class RelatedPosts_adapter extends RecyclerView.Adapter<RelatedPosts_adapter.MyViewHolder> {

    Context context;
    ArrayList<HashMap<String,String>> arrayList;
    String type;

    public RelatedPosts_adapter(Context context,ArrayList<HashMap<String,String>> arrayList,String type) {
        this.context=context;
        this.arrayList=arrayList;
        this.type=type;
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView txtrelated;
        public ImageView imgrelated;
        public FrameLayout frame_one;

        public MyViewHolder(View view) {
            super(view);
            this.frame_one=(FrameLayout)view.findViewById(R.id.frame_container);
            this.imgrelated=(ImageView)view.findViewById(R.id.img_related);
            this.txtrelated=(TextView)view.findViewById(R.id.txt_related);
        }
    }

    @Override
    public RelatedPosts_adapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_realtedposts, parent, false);
        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(RelatedPosts_adapter.MyViewHolder holder, final int position) {

        holder.txtrelated.setText(arrayList.get(position).get("title"));
        if (type.equalsIgnoreCase("news")){
            try {
                if (!arrayList.get(position).get("video").isEmpty()){
                    Glide.with(context).load(arrayList.get(position).get("video")).error(R.drawable.newsdefault).into(holder.imgrelated);
                }else {
                    Glide.with(context).load(arrayList.get(position).get("image")).error(R.drawable.newsdefault).into(holder.imgrelated);
                }
            } catch (Exception e){

            }
        }else if (type.equalsIgnoreCase("talents")){
            try {
                if (!arrayList.get(position).get("video").isEmpty()){
                    Glide.with(context).load(arrayList.get(position).get("video")).error(R.drawable.talentsdefault).into(holder.imgrelated);
                }else {
                    Glide.with(context).load(arrayList.get(position).get("image")).error(R.drawable.talentsdefault).into(holder.imgrelated);
                }
            } catch (Exception e){

            }
        }else if (type.equalsIgnoreCase("explore")) {
            try {
                if (!arrayList.get(position).get("video").isEmpty()){
                    Glide.with(context).load(arrayList.get(position).get("video")).error(R.drawable.exploredefault).into(holder.imgrelated);
                }else {
                    Glide.with(context).load(arrayList.get(position).get("image")).error(R.drawable.exploredefault).into(holder.imgrelated);
                }
            } catch (Exception e){

            }
        }else if (type.equalsIgnoreCase("blogs")){
            try {
                if (!arrayList.get(position).get("video").isEmpty()){
                    Glide.with(context).load(arrayList.get(position).get("video")).error(R.drawable.noimage).into(holder.imgrelated);
                }else {
                    Glide.with(context).load(arrayList.get(position).get("image")).error(R.drawable.noimage).into(holder.imgrelated);
                }
            } catch (Exception e){

            }
        }

        holder.frame_one.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (type.equalsIgnoreCase("news")){
                    //JCVideoPlayer.releaseAllVideos();
                    try{
                        if (ViewPagerAdapter.jzVideoPlayerStandard!=null){
                            JZVideoPlayer.releaseAllVideos();
                        }
                    }catch (Exception e){

                    }
                    Newsdetail_fragment fragment = new Newsdetail_fragment();
                    Bundle bundle = new Bundle();
                    bundle.putString("catid", arrayList.get(position).get("catid"));
                    bundle.putString("postid",arrayList.get(position).get("id"));
                    fragment.setArguments(bundle);
                    AppCompatActivity activity = (AppCompatActivity) v.getContext();
                    activity.getSupportFragmentManager().beginTransaction()
                            .add(R.id.frame_container,fragment,"tempfrag").addToBackStack("tempfrag").commit();
                }else if (type.equalsIgnoreCase("talents")){
                    //JCVideoPlayer.releaseAllVideos();
                    if (ViewPagerAdapter.jzVideoPlayerStandard!=null){
                        JZVideoPlayer.releaseAllVideos();
                    }
                    Talentdetail_fragment fragment = new Talentdetail_fragment();
                    Bundle bundle = new Bundle();
                    bundle.putString("catid",arrayList.get(position).get("catid"));
                    bundle.putString("postid",arrayList.get(position).get("id"));
                    fragment.setArguments(bundle);

                    AppCompatActivity activity = (AppCompatActivity) v.getContext();
                   activity.getSupportFragmentManager().beginTransaction()
                            .add(R.id.frame_container,fragment,"tempfrag").addToBackStack(null).commit();

                } else if (type.equalsIgnoreCase("explore")) {
                    //JCVideoPlayer.releaseAllVideos();
                    try{
                        if (ViewPagerAdapter.jzVideoPlayerStandard!=null){
                            JZVideoPlayer.releaseAllVideos();
                        }
                    }catch (Exception e){

                    }
                    Explore_detailfragment fragment = new Explore_detailfragment();
                    Bundle bundle = new Bundle();
                    bundle.putString("catid",arrayList.get(position).get("catid"));
                    bundle.putString("postid",arrayList.get(position).get("id"));
                    fragment.setArguments(bundle);

                    AppCompatActivity activity = (AppCompatActivity) v.getContext();
                    activity.getSupportFragmentManager().beginTransaction().add(R.id.frame_container,fragment, "tempfrag").addToBackStack(null).commit();

                } else if (type.equalsIgnoreCase("blogs")){
                    //JCVideoPlayer.releaseAllVideos();
                    Blogdetail_fragment fragment = new Blogdetail_fragment();
                    Bundle bundle = new Bundle();
                    bundle.putString("catid",arrayList.get(position).get("catid"));
                    bundle.putString("postid",arrayList.get(position).get("id"));
                    fragment.setArguments(bundle);

                    AppCompatActivity activity = (AppCompatActivity) v.getContext();
                    activity.getSupportFragmentManager().beginTransaction()
                            .add(R.id.frame_container, fragment,"tempfrag").addToBackStack(null).commit();
                }
            }
        });

    }

    @Override
    public int getItemCount() {
        return arrayList.size();
    }

}

