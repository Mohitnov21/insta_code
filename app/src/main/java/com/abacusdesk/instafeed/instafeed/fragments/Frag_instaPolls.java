package com.abacusdesk.instafeed.instafeed.fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.abacusdesk.instafeed.instafeed.R;
import android.content.DialogInterface;

import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.widget.Toast;

import com.abacusdesk.instafeed.instafeed.Util.Alerts;
import com.abacusdesk.instafeed.instafeed.Util.Apis;
import com.abacusdesk.instafeed.instafeed.Util.CommonFunctions;
import com.abacusdesk.instafeed.instafeed.Util.SaveSharedPreference;

import com.abacusdesk.instafeed.instafeed.adapter.Poll_Adapter;
import com.abacusdesk.instafeed.instafeed.model.Polls_listmodel;
import com.abacusdesk.instafeed.instafeed.model.Profile_model;
import com.android.volley.AuthFailureError;
import com.android.volley.NoConnectionError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.gson.Gson;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
/**
 * Created by abacusdesk on 2017-07-24.
 */

public class Frag_instaPolls  extends Fragment{

    View view;
    RecyclerView recyclerView;

    //Explore_Adapter adapter;
   // ArrayList<Talent_ProfileModel.DataBean> array_talent=new ArrayList<>();
   // Talent_ProfileModel talent_profileModel;
   // Talent_profileAdapter talent_profileAdapter;

    ArrayList<Polls_listmodel.DataBean> array_polls=new ArrayList<>();
    Poll_Adapter poll_adapter;
    Polls_listmodel polls_listmodel;
    public  String  Api_temp="";
    Map<String, String> map ;
    TextView txtrecord;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.frag_recycler, container, false);
        recyclerView = (RecyclerView) view.findViewById(R.id.recyclerView);
        txtrecord=(TextView)view.findViewById(R.id.txt_records);
        if (Publicprofile_Fragment.flag==true){
            Api_temp= Apis.public_polls;
            map=new HashMap<>();
            map.put("username",Publicprofile_Fragment.username);
            Log.e("printpublicpolls",""+map);
        } else {
            Api_temp=Apis.private_polls;
            // map.clear();
            map=new HashMap<>();
            map.put("user", SaveSharedPreference.getUserID(getActivity()));
            map.put("token",SaveSharedPreference.getPrefToken(getActivity()));
            Log.e("printprivatepols",""+map);
        }

        if (CommonFunctions.isConnected(getActivity())){
            if (!array_polls.isEmpty()){
                array_polls.clear();
            }
            getPollsList();
        }else {
            Toast.makeText(getActivity(), "Internet not available", Toast.LENGTH_SHORT).show();
        }
        setRecyclerView();
        return view;
    }

    public void getPollsList() {
        //  Dialogs.showProDialog(getActivity(), "Loading");
        StringRequest stringRequest = new StringRequest(Request.Method.POST, Api_temp,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.e("response_polls", "" + response);
                        try {
                            getResponse(response);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        //Dialogs.disDialog();
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        ///   Dialogs.disDialog();
                        DialogInterface.OnClickListener onClickTryAgain = new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                //  Dialogs.disDialog();
                                getPollsList();
                            }
                        };
                        if (error instanceof TimeoutError) {
                            //  Dialogs.disDialog();
                            Alerts.timeoutErrorAlert(getContext(), onClickTryAgain);
                        } else if (error instanceof NoConnectionError) {
                            //  Dialogs.disDialog();
                            Alerts.internetConnectionErrorAlert(getContext(), onClickTryAgain);
                        }
                    }
                })
        {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                return map;
            }
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Content-Type", "application/x-www-form-urlencoded");
                return params;
            }
        };

        RequestQueue requestQueue = Volley.newRequestQueue(getActivity());
        requestQueue.add(stringRequest);
    }

    private void getResponse(String response ) throws JSONException {
        if(!response.equals(null)){
            Log.e("log", "" + response);
            JSONObject jsonObject = new JSONObject(response);
            Object object = jsonObject.get("data");
            if (object instanceof JSONArray) {
                Gson gson = new Gson();
                polls_listmodel = gson.fromJson(response, Polls_listmodel.class);
                int status = polls_listmodel.getStatus();
                String msg = polls_listmodel.getMessage();
                if (msg.equalsIgnoreCase("success") && status == 200) {
                    if (polls_listmodel.getData() != null && !polls_listmodel.getData().isEmpty()) {
                        for (int i = 0; i < polls_listmodel.getData().size(); i++) {
                            try {
                                Polls_listmodel.DataBean dataBean = polls_listmodel.getData().get(i);
                                array_polls.add(dataBean);
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }
                    }
                    if (array_polls.isEmpty()) {
                        txtrecord.setVisibility(View.VISIBLE);
                    }
                    poll_adapter.notifyDataSetChanged();
                }
            }
        }
    }

   // poills blogs opiunions
  // working petition talents  news
  // Setting recycler view

    private void setRecyclerView() {
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        // recyclerView.addItemDecoration(mDividerItemDecoration);
        try{
            poll_adapter= new Poll_Adapter(getActivity(),array_polls);
            recyclerView.setAdapter(poll_adapter);
            poll_adapter.notifyDataSetChanged();
        }catch (Exception e){
            e.printStackTrace();
        }
    }
}