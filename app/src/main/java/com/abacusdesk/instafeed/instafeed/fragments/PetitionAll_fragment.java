package com.abacusdesk.instafeed.instafeed.fragments;

import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.abacusdesk.instafeed.instafeed.NavDrawerActivity;
import com.abacusdesk.instafeed.instafeed.R;
import com.abacusdesk.instafeed.instafeed.Util.Alerts;
import com.abacusdesk.instafeed.instafeed.Util.Apis;
import com.abacusdesk.instafeed.instafeed.Util.CommonFunctions;
import com.abacusdesk.instafeed.instafeed.adapter.PetitionAdapter_tab;
import com.abacusdesk.instafeed.instafeed.adapter.Petition_Adapter;
import com.abacusdesk.instafeed.instafeed.adapter.Talent_allAdapter;
import com.abacusdesk.instafeed.instafeed.custom_dialog.Dialogs;
import com.abacusdesk.instafeed.instafeed.model.Petition_model;
import com.abacusdesk.instafeed.instafeed.model.Talent_allmodel;
import com.android.volley.AuthFailureError;
import com.android.volley.NoConnectionError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by abacusdesk on 2017-08-08.
 */

public class PetitionAll_fragment extends Fragment {

    private View view;
    Petition_model petition_model;
    private static RecyclerView recyclerView;
    ArrayList<Petition_model.DataBean> array_petition=new ArrayList<>();
    PetitionAdapter_tab adapter;
    public static String catid="",postid="";

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState){
        view = inflater.inflate(R.layout.fragment_news, container, false);
        recyclerView = (RecyclerView) view.findViewById(R.id.recyclerView);

        final SwipeRefreshLayout swipeRefreshLayout = (SwipeRefreshLayout)view.findViewById(R.id.swipe_refresh_layout);
        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                swipeRefreshLayout.setRefreshing(false);
            }
        });
        setRecyclerView();
        if (CommonFunctions.isConnected(getActivity())){
            if (!array_petition.isEmpty()){
                array_petition.clear();
            }
            getPetitions();
        } else {
           // Toast.makeText(getActivity(), "Internet not available", Toast.LENGTH_SHORT).show();
        }
        view.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if (keyCode == KeyEvent.KEYCODE_BACK) {
                    // ViewPagerAdapter.stopVideo(getActivity());

                    getActivity().getSupportFragmentManager().popBackStack();
                   // NavDrawerActivity.container_bottom_tab.setVisibility(View.GONE);
                   // NavDrawerActivity.imgAddReport.setVisibility(View.GONE);
                    return true;
                } else {
                    return false;
                }
            }
        });
        return view;
    }

    public void getPetitions() {
        // Dialogs.showProDialog(getActivity(), "Loading");
        StringRequest stringRequest = new StringRequest(Request.Method.GET, Apis.petition_all,
                new Response.Listener<String>() {

                    @Override
                    public void onResponse(String response) {
                       // Log.e("log", "" + response);
                        getResponse(response);
                        Dialogs.disDialog();
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                    }
                })
         {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Content-Type", "application/x-www-form-urlencoded");
                return params;
            }
        };

        RequestQueue requestQueue = Volley.newRequestQueue(getActivity());
        requestQueue.add(stringRequest);
    }

    private void getResponse(String response) {
        if(!response.equals(null)){
           // Log.e("log", "" + response);
            Gson gson = new Gson();
            petition_model = gson.fromJson(response, Petition_model.class);
            int status = petition_model.getStatus();
            String msg = petition_model.getMessage();
            if (msg.equalsIgnoreCase("success") && status==200) {
                if (petition_model.getData()!=null && !petition_model.getData().isEmpty()){
                    for (int i = 0; i < petition_model.getData().size(); i++) {
                        try {
                            Petition_model.DataBean dataBean = petition_model.getData().get(i);
                            array_petition.add(dataBean);
                        } catch (Exception e){
                           // e.printStackTrace();
                        }
                    }
                }
               // Log.e("databen data",""+array_petition);
                adapter.notifyDataSetChanged();
            }
        }
    }

    private void setRecyclerView() {
        try{
            adapter= new PetitionAdapter_tab(getActivity(),array_petition);
        }  catch (Exception e){
            //e.printStackTrace();
        }

        recyclerView.setHasFixedSize(true);
        LinearLayoutManager layoutManager=new LinearLayoutManager(getActivity());
        recyclerView.setLayoutManager(layoutManager);

        layoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        recyclerView.setAdapter(adapter);
    }
}
