package com.abacusdesk.instafeed.instafeed.Util;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

public class TimeUtilities {

	public static String getTimeStampInString() {
		return new SimpleDateFormat("yyyyMMdd_HHmmss", Locale.getDefault()).format(new Date());
	}
	
	public static long getTimeStampInLong() {
		return System.currentTimeMillis();
	}
}
