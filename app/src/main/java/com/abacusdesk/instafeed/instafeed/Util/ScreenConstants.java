package com.abacusdesk.instafeed.instafeed.Util;

/**
 * Created by Administrator on 5/29/2017.
 */

public class ScreenConstants  {

    public static final int HOME = 0;
    public static final int FOLLOWER = 1;
    public static final int FOLLOWING = 2;
    public static final int SETTINGS = 3;
    public static final int MY_ACCOUNT = 4;
    public static final int CHANGE_PASSWORD = 5;
    public static final int LOGOUT = 6;
    public static final int CREATE_A_POST = 7;
    public static final int LOCATION = 8;
    public static final int SAVE_OFFLINE= 9;
    public static final int HOME_START= 10;
    public static final int ABOUT_US= 11;
    public static final int MYPOST = 12;
    public static final int MYBOOKMARKS = 13;

    public static final String INSTAFEED_AUDIO_DIRECTORY="InstaFeedAudio";
    public static final String MEDIA_TYPE_AUDIO="audio";

    public static String KEY_AUDIO_FILE_NAME="AudioFileName";
    public static String KEY_AUDIO_FILE_PATH="AudioFilePath";
    public static String KEY_AUDIO_FILE_FIELD_ID="AudioFileId";
    public static String KEY_AUDIO_FILE_LENGTH="AudioFileLength";
    public static String KEY_AUDIO_FILE_TIME="AudioFileTime";
    public static final int REQUEST_CAMERA = 3, SELECT_FILE = 4,PICK_IMAGE_MULTIPLE=8;
    public static int MAKE_VIDEO_REQUEST = 6, PICK_VIDEO_REQUEST = 5;
    public static final int ACTIVITY_RECORD_SOUND = 0;
    public static final int REQUEST_MICROPHONE =8 ;
    public static final int REQUEST_VIDEO_CAPTURE = 111;
    public static final int REQUEST_AUDIO_RECORD = 112;
    public static final int REQUEST_AUDIO_CAPTURE = 112;
    public static String DIRECTORY_NAME = "InstaFeed";
    public static String VIDEO_DIRECTORY_NAME = "InstaFeedVedio";
    public static String AUDIO_DIRECTORY_NAME = "InstaFeedAudio";
    public static String IMAGE_DIRECTORY_NAME = "InstaFeedImage";

    public static String MEDIA_TYPE="media_type";
    public static String MEDIA_FILE_PATH="mediaFilePath";
    public static String MEDIA_FILE_NAME="mediaFileName";


    public static  String MEDIA_TYPE_IMAGE="image";
    public static  String MEDIA_TYPE_VIDEO="video";
    public static  int PICK_AUDIO_REQUEST = 7;
    public static  boolean GALLERY_MODE=false;
    public static  String GALLERY_MODE_YES="y";
    public static  String GALLERY_MODE_NO="n";


 // public static String STARTFOREGROUND_ACTION = "com.truiton.foregroundservice.action.startforeground";
  //  public static String STARTFOREGROUND_ACTION = "com.abacusdesk.instafeed.instafeed.SendOfflineData";
}
