package com.abacusdesk.instafeed.instafeed.model;

import com.google.gson.Gson;

import java.util.List;

/**
 * Created by ishaan on 7/3/2017.
 */

public class Search_model {

    /**
     * status : 200
     * message : success
     * data : [{"id":"225","news_category_id":"7",
     * "title":"Gujarat Lions in danger of elimination against Delhi Daredevils",
     * "status":"P","slug":"gujarat-lions-in-danger-of-elimination-against-delhi-daredevils-1732631305",
     * "short_description":"\r\n\r\nDelhi Daredevils are not out of the counting for the IPL 2017 play- offs, but Gujarat Lions are in a unsafe spot as the league stage of the tournament builds up to a possibly dizzying finale. An exciting chase of 186 against defending champions at the Feroz Shah Kotla on Tuesday has moved Darede","dt_added":"2017-06-20 06:19:58","total_comments":null,"total_views":"105","image":"3eb371b4d7b1c5d8523bceb7440947be.jpg","username":"instafeed","avatar":"d6a43e12b07d7a6cf5c0fca593aff5dc.jpg"},{"id":"224","news_category_id":"1","title":"IIT-Delhi works on its syllabus to ease pressure","status":"P","slug":"iitdelhi-works-on-its-syllabus-to-ease-pressure-1119817035","short_description":"\r\n\r\nThe institute is focusing on the practical aspect of eduction, away from the theories as it has traditionally taught\r\n\r\nAfter a student of the Indian Institute of Technology (IIT) Delhi reportedly attempted suicide, last month. The institute has decided to work on its first-year curriculum to le","dt_added":"2017-06-20 08:47:57","total_comments":null,"total_views":"69","image":"782f5c1b227fef17ea75273b660e3c65.jpg","username":"instafeed","avatar":"d6a43e12b07d7a6cf5c0fca593aff5dc.jpg"}]
     * type : news
     */

    private int status;
    private String message;
    private String type;
    private List<DataBean> data;

    public static Search_model objectFromData(String str) {

        return new Gson().fromJson(str, Search_model.class);
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public List<DataBean> getData() {
        return data;
    }

    public void setData(List<DataBean> data) {
        this.data = data;
    }

    public static class DataBean {

       /**
         * id : 225
         * news_category_id : 7
         * title : Gujarat Lions in danger of elimination against Delhi Daredevils
         * status : P
         * slug : gujarat-lions-in-danger-of-elimination-against-delhi-daredevils-1732631305
         * short_description :

         Delhi Daredevils are not out of the counting for the IPL 2017 play- offs, but Gujarat Lions are in a unsafe spot as the league stage of the tournament builds up to a possibly dizzying finale. An exciting chase of 186 against defending champions at the Feroz Shah Kotla on Tuesday has moved Darede
         * dt_added : 2017-06-20 06:19:58
         * total_comments : null
         * total_views : 105
         * image : 3eb371b4d7b1c5d8523bceb7440947be.jpg
         * username : instafeed
         * avatar : d6a43e12b07d7a6cf5c0fca593aff5dc.jpg
         */

        private String id;
        private String news_category_id;
        private String title;
        private String status;
        private String slug;
        private String short_description;
        private String dt_added;
        private Object total_comments;
        private String total_views;
        private String image;
        private String username;
        private String avatar;

        public static DataBean objectFromData(String str) {

            return new Gson().fromJson(str, DataBean.class);
        }

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getNews_category_id() {
            return news_category_id;
        }

        public void setNews_category_id(String news_category_id) {
            this.news_category_id = news_category_id;
        }

        public String getTitle() {
            return title;
        }

        public void setTitle(String title) {
            this.title = title;
        }

        public String getStatus() {
            return status;
        }

        public void setStatus(String status) {
            this.status = status;
        }

        public String getSlug() {
            return slug;
        }

        public void setSlug(String slug) {
            this.slug = slug;
        }

        public String getShort_description() {
            return short_description;
        }

        public void setShort_description(String short_description) {
            this.short_description = short_description;
        }

        public String getDt_added() {
            return dt_added;
        }

        public void setDt_added(String dt_added) {
            this.dt_added = dt_added;
        }

        public Object getTotal_comments() {
            return total_comments;
        }

        public void setTotal_comments(Object total_comments) {
            this.total_comments = total_comments;
        }

        public String getTotal_views() {
            return total_views;
        }

        public void setTotal_views(String total_views) {
            this.total_views = total_views;
        }

        public String getImage() {
            return image;
        }

        public void setImage(String image) {
            this.image = image;
        }

        public String getUsername() {
            return username;
        }

        public void setUsername(String username) {
            this.username = username;
        }

        public String getAvatar() {
            return avatar;
        }

        public void setAvatar(String avatar) {
            this.avatar = avatar;
        }
    }
}
