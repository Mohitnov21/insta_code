package com.abacusdesk.instafeed.instafeed.fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.abacusdesk.instafeed.instafeed.R;
import com.abacusdesk.instafeed.instafeed.Util.Apis;
import com.abacusdesk.instafeed.instafeed.Util.AppSingleton;
import com.abacusdesk.instafeed.instafeed.Util.CommonFunctions;
import com.abacusdesk.instafeed.instafeed.adapter.FeedAdapter;
import com.abacusdesk.instafeed.instafeed.custom_dialog.Dialogs;
import com.abacusdesk.instafeed.instafeed.model.NewsFeed;
import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by abacusdesk on 2017-12-13.
 */

public class NewsRss extends Fragment implements SwipeRefreshLayout.OnRefreshListener {

    private View view;
    private RecyclerView recyclerView;
    private NewsFeed newsFeed;
    private FeedAdapter adapter;
    private ArrayList<NewsFeed.DataBean> arrayList;
    private SwipeRefreshLayout swipeRefreshLayout;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState){
        view = inflater.inflate(R.layout.fragment_news, container, false);

        swipeRefreshLayout = (SwipeRefreshLayout)view.findViewById(R.id.swipe_refresh_layout);
        swipeRefreshLayout.setOnRefreshListener(this);

        recyclerView = (RecyclerView) view.findViewById(R.id.recyclerView);
        arrayList=new ArrayList<>();
        setRecyclerView();
        try {
            if (CommonFunctions.isConnected(getActivity())) {
                getRssfeed();
            }
        } catch (Exception e) {

        }
        return view;
    }

    public void getRssfeed() {
        // Dialogs.showProDialog(getActivity(), "Loading");
        StringRequest stringRequest = new StringRequest(Request.Method.GET, Apis.newsrss,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        getResponse(response);
                        Dialogs.disDialog();
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                    }
                })
        {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Content-Type", "application/x-www-form-urlencoded");
                return params;
            }
        };
        AppSingleton.getInstance(getContext()).addToRequestQueue(stringRequest);
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(
                6000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

    }

    private void getResponse(String response ){
        if (!arrayList.isEmpty()){
            arrayList.clear();
        }
        if(!response.equals(null)){
            //Log.e("log", "" + response);
            Gson gson = new Gson();
            newsFeed = gson.fromJson(response, NewsFeed.class);
            int status = newsFeed.getStatus();
            String msg = newsFeed.getMessage();
            if (msg.equalsIgnoreCase("success") && status==200) {
                if (newsFeed.getData()!=null && !newsFeed.getData().isEmpty()){
                    for (int i = 0; i < newsFeed.getData().size(); i++) {
                        try {
                            NewsFeed.DataBean dataBean = newsFeed.getData().get(i);
                            arrayList.add(dataBean);
                        } catch (Exception e){
                        }
                    }
                }
                adapter.notifyDataSetChanged();
            }
        }
    }

    private void setRecyclerView() {
        try{
            adapter= new FeedAdapter(getActivity(),arrayList);
        }  catch (Exception e){
        }
        recyclerView.setHasFixedSize(true);
        LinearLayoutManager layoutManager=new LinearLayoutManager(getActivity());
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setNestedScrollingEnabled(false);
        recyclerView.setItemViewCacheSize(20);
        recyclerView.setDrawingCacheEnabled(true);
        recyclerView.setDrawingCacheQuality(View.DRAWING_CACHE_QUALITY_HIGH);

        layoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        recyclerView.setAdapter(adapter);
    }

    @Override
    public void onRefresh() {
        swipeRefreshLayout.setRefreshing(true);
        try {
            if (CommonFunctions.isConnected(getActivity())){
                getRssfeed();
            }
        } catch (Exception e) {
            // e.printStackTrace();
        }
        swipeRefreshLayout.setRefreshing(false);
    }

}