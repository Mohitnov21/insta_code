package com.abacusdesk.instafeed.instafeed.fragments;

import android.widget.ImageView;

/**
 * Created by Administrator on 5/24/2017.
 */

public class GridObject {
    String title;

    public GridObject(String title, int imges) {
        this.title = title;
        this.imges = imges;
    }

    public int getImges() {
        return imges;
    }

    public void setImges(int imges) {
        this.imges = imges;
    }

    int imges;



    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }


}
