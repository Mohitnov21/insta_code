package com.abacusdesk.instafeed.instafeed.adapter;

import android.content.Context;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.abacusdesk.instafeed.instafeed.R;
import com.abacusdesk.instafeed.instafeed.fragments.Publicprofile_Fragment;
import com.abacusdesk.instafeed.instafeed.model.User_model;
import com.bumptech.glide.Glide;

import java.util.ArrayList;

/**
 * Created by ishaan on 7/4/2017.
 */


public class User_Adapter extends RecyclerView.Adapter<User_Adapter.MyViewHolder> {

        Context context;
        ArrayList<User_model.DataBean> arrayList;

        public User_Adapter(Context context, ArrayList<User_model.DataBean> arrayList){
            this.context=context;
            this.arrayList=arrayList;
            Log.e("size array",""+arrayList);
        }

        public class MyViewHolder extends RecyclerView.ViewHolder {

            public TextView txtusername,txtuserhashname;
            public ImageView imgpro;
            public Button btnfollow;
            public LinearLayout lnitem;

            public MyViewHolder(View view) {
                super(view);
                this.lnitem=(LinearLayout)itemView.findViewById(R.id.ln_item);
                this.txtusername = (TextView) itemView.findViewById(R.id.txt_username);
                this.txtuserhashname = (TextView) itemView.findViewById(R.id.txt_userhashname);
                this.imgpro=(ImageView) itemView.findViewById(R.id.user_img);
                this.btnfollow = (Button) itemView.findViewById(R.id.btn_follow);
            }
        }

        @Override
        public User_Adapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View itemView = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.item_userlist, parent, false);
            return new User_Adapter.MyViewHolder(itemView);
        }

        @Override
        public void onBindViewHolder(User_Adapter.MyViewHolder holder, int position) {

            final User_model.DataBean dataBean = arrayList.get(position);
            holder.txtusername.setText(dataBean.getFirst_name());
            holder.txtuserhashname.setText("#"+dataBean.getUsername());
            if (dataBean.getAvatar()!="null"){
                Glide.with(context).load(dataBean.getAvatar()).error(R.drawable.user).into(holder.imgpro);
            }
            holder.btnfollow.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Publicprofile_Fragment.flag=true;
                    Publicprofile_Fragment.username=dataBean.getUsername();
                    AppCompatActivity activity = (AppCompatActivity) v.getContext();
                    activity.getSupportFragmentManager().beginTransaction()
                            .replace(R.id.frame_container, new Publicprofile_Fragment(),"public").addToBackStack(null).commit();
                }
            });
           holder.lnitem.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Publicprofile_Fragment.flag=true;
                    Publicprofile_Fragment.username=dataBean.getUsername();
                    AppCompatActivity activity = (AppCompatActivity) v.getContext();
                    activity.getSupportFragmentManager().beginTransaction()
                            .replace(R.id.frame_container, new Publicprofile_Fragment(),"public").addToBackStack(null).commit();
                }
            });
        }

        @Override
        public int getItemCount() {
            return arrayList.size();
        }

 }
