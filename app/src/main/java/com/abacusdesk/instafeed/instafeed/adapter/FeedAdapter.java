package com.abacusdesk.instafeed.instafeed.adapter;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.abacusdesk.instafeed.instafeed.R;
import com.abacusdesk.instafeed.instafeed.Util.Apis;
import com.abacusdesk.instafeed.instafeed.Util.CommonUtils;
import com.abacusdesk.instafeed.instafeed.fragments.FeedDetail_fragment;
import com.abacusdesk.instafeed.instafeed.fragments.Newsdetail_fragment;
import com.abacusdesk.instafeed.instafeed.model.NewsFeed;
import com.abacusdesk.instafeed.instafeed.model.NewsModel;
import com.squareup.picasso.Picasso;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.TimeZone;

/**
 * Created by abacusdesk on 2017-12-13.
 */

public class FeedAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private ArrayList<NewsFeed.DataBean> arrayList;
    private Activity activity;
    private final int NORMAL_ROW = 0;
    private NewsFeed.DataBean dataBean2;
    private String imagetemp="";

    public FeedAdapter(Activity activity, ArrayList<NewsFeed.DataBean> arrayList) {
        this.activity = activity;
        this.arrayList = arrayList;
    }

    @Override
    public int getItemViewType(int position) {

        return NORMAL_ROW;

    }

    @Override
    public int getItemCount() {
        return (null != arrayList ? arrayList.size() : 0);
    }

    @Override
    public void onBindViewHolder(final RecyclerView.ViewHolder holder, final int position) {

       // String myFormat = "yyyy-MM-dd HH:mm:ss";
       // DateFormat sdformat = new SimpleDateFormat(myFormat);
       // DateFormat targetFormat = new SimpleDateFormat("MMMM dd, yyyy");

        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'");
        sdf.setTimeZone(TimeZone.getTimeZone("GMT"));
        DateFormat targetFormat = new SimpleDateFormat("MMMM dd, yyyy");

        final NewsFeed.DataBean dataBean = arrayList.get(position);
        try{
            if (dataBean.getTitle()!=null){
                String temp_str=dataBean.getTitle().toString().trim();
                temp_str = temp_str.substring(0,1).toUpperCase() + temp_str.substring(1);
                ((NormalHolder)holder).title.setText(temp_str);
            }
        }catch (Exception e){
            Log.e("exception lang",""+e);
        }

        ((NormalHolder)holder).imgshare.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dataBean2=arrayList.get(position);
                if (!dataBean.getImage_url().isEmpty()){
                    imagetemp=dataBean2.getImage_url();
                }
                share_image();
            }
        });
        ((NormalHolder) holder).lnvideo.setVisibility(View.GONE);
        if (dataBean.getImage_url()!=null && !dataBean.getImage_url().isEmpty()){
            Picasso.with(activity).load(dataBean.getImage_url()).fit()
                    .error(R.drawable.newsdefault).into(((NormalHolder) holder).imgmain);
            ((NormalHolder)holder).date.setVisibility(View.VISIBLE);
        }else {
            ((NormalHolder) holder).imgmain.setImageResource(R.drawable.newsdefault);
        }

        // String formattedDate = targetFormat.format(sdf.parse(dataBean.getDt_published()));
        try {
            ((NormalHolder)holder).date.setText(CommonUtils.dateformat(dataBean.getDt_published()));
        }catch (Exception e){
        }

        ((NormalHolder) holder).lnone.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                FeedDetail_fragment fragment = new FeedDetail_fragment();
                Bundle bundle = new Bundle();
                bundle.putString("id", dataBean.getId());
                fragment.setArguments(bundle);
                AppCompatActivity activity = (AppCompatActivity) v.getContext();
                activity.getSupportFragmentManager().beginTransaction()
                        .add(R.id.frame_container,fragment,"feed").addToBackStack("feed").commit();
            }
        });
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = null;
        //Based on view type decide which type of view to supply with viewHolder
        switch (viewType) {
            case NORMAL_ROW:
                view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_finalitem, parent, false);
                return new NormalHolder(view);

        }
        return null;
    }

    public static class NormalHolder extends RecyclerView.ViewHolder {

        public TextView title;
        public ImageView imgshare, imgmain;
        public TextView date;
        public LinearLayout lnone,lnvideo;

        public NormalHolder(View itemView) {
            super(itemView);

            this.title = (TextView) itemView.findViewById(R.id.txt_title);
            this.date = (TextView) itemView.findViewById(R.id.txt_date);

            this.imgshare = (ImageView) itemView.findViewById(R.id.img_share);

            this.imgmain = (ImageView) itemView.findViewById(R.id.img_main);
            this.lnone = (LinearLayout) itemView.findViewById(R.id.ln_one);
            this.lnvideo = (LinearLayout) itemView.findViewById(R.id.ln_video);
        }
    }


    public class MyAsync extends AsyncTask<Void, Void, Bitmap> {
        @Override
        protected Bitmap doInBackground(Void... params) {

            try {
                URL url = new URL(imagetemp);
                HttpURLConnection connection = (HttpURLConnection) url.openConnection();
                connection.setDoInput(true);
                connection.connect();
                InputStream input = connection.getInputStream();
                Bitmap myBitmap = BitmapFactory.decodeStream(input);
                return myBitmap;
            } catch (IOException e) {
                e.printStackTrace();
                return null;
            }
        }
    }

    private void share_image(){
        Log.e("tempimage",""+imagetemp);
        try {
            MyAsync obj = new MyAsync() {
                @Override
                protected void onPostExecute(Bitmap bmp) {
                    super.onPostExecute(bmp);

                    ByteArrayOutputStream bytes = new ByteArrayOutputStream();
                    if (bmp!=null)
                        bmp.compress(Bitmap.CompressFormat.JPEG, 100, bytes);
                    File f = new File(Environment.getExternalStorageDirectory() + File.separator + "temporary_file.jpg");
                    try {
                        f.createNewFile();
                        FileOutputStream fo = new FileOutputStream(f);
                        fo.write(bytes.toByteArray());

                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            };

            if (!imagetemp.isEmpty())
                obj.execute();

        } catch (Exception e){

        }

        Intent sharingIntent = new Intent(Intent.ACTION_SEND);
        sharingIntent.setType("*/*");
        try{
            if (!imagetemp.isEmpty()){
                sharingIntent.putExtra(Intent.EXTRA_STREAM, Uri.parse("file:///sdcard/temporary_file.jpg"));
            }else {
                sharingIntent.setType("text/plain");
            }

            sharingIntent.putExtra(Intent.EXTRA_TEXT, dataBean2.getTitle()+" "+dataBean2.getNews_source());
            Log.e("shreinterjyt",""+Uri.parse("file:///sdcard/temporary_file.jpg"));
            activity.startActivity(Intent.createChooser(sharingIntent, "Share via"));
        }catch (Exception e){
            Log.e("exception",""+e);
        }
    }
}