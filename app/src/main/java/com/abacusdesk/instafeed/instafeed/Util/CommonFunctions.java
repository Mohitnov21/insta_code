package com.abacusdesk.instafeed.instafeed.Util;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.ComponentName;
import android.content.Context;
import android.content.CursorLoader;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.location.Address;
import android.location.Geocoder;
import android.media.ThumbnailUtils;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Build;
import android.os.SystemClock;
import android.provider.MediaStore;
import android.provider.Settings;
import android.text.Editable;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.Toast;

import com.abacusdesk.instafeed.instafeed.R;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URISyntaxException;
import java.net.URL;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static com.facebook.FacebookSdk.getApplicationContext;

/**
 * Created by nitish on 24/6/16.
 */
public class CommonFunctions {
    public  static ProgressDialog showDialog(Context context){
        ProgressDialog progressDialog = new ProgressDialog(context);
        progressDialog.setMessage(context.getString(R.string.plz_wait));
        //progressDialog.setTitle(context.getString(R.string.login_btn));
        progressDialog.setCanceledOnTouchOutside(false);
        return progressDialog;
    }

    public String getRealPathFromURI(Activity context,Uri contentURI) {
     String result;
        String[] filePath = { MediaStore.Images.Media.DATA };
        Cursor c = context.getContentResolver().query(contentURI,filePath, null, null, null);
        c.moveToFirst();
        int columnIndex = c.getColumnIndex(filePath[0]);
        Log.i("Image Path on select",filePath.toString());
        result = c.getString(columnIndex);
        c.close();
        Log.i("Image Path on select",result);

        return result;
    }

    public static void setListViewHeightBasedOnChildren(ListView listView) {
        ListAdapter listAdapter = listView.getAdapter();
        if (listAdapter == null)
            return;

        int desiredWidth = View.MeasureSpec.makeMeasureSpec(listView.getWidth(), View.MeasureSpec.UNSPECIFIED);
        int totalHeight = 0;
        View view = null;
        for (int i = 0; i < listAdapter.getCount(); i++) {
            view = listAdapter.getView(i, view, listView);
            if (i == 0)
                view.setLayoutParams(new ViewGroup.LayoutParams(desiredWidth, ViewGroup.LayoutParams.WRAP_CONTENT));

            view.measure(desiredWidth, View.MeasureSpec.UNSPECIFIED);
            totalHeight += view.getMeasuredHeight();
        }
        ViewGroup.LayoutParams params = listView.getLayoutParams();
        params.height = totalHeight + (listView.getDividerHeight() * (listAdapter.getCount() - 1));
        listView.setLayoutParams(params);

    }

    public static Bitmap extractThumbnail(File file, int height, int width) {
        if(! file.exists()) return null;
        Bitmap bitmap = ThumbnailUtils.extractThumbnail(
                BitmapFactory.decodeFile(file.getAbsolutePath()), height, width);
        return  bitmap;
        // bitmap = MediaStore.Images.Media.getBitmap(getContentResolver(),
        // filePath);
    }

    public String _getRealPathFromURIForAudio(Context context, Uri contentUri) {
        String result;
        String[] proj = { MediaStore.Audio.Media.DATA };
        Cursor c = context.getContentResolver().query(contentUri,proj, null, null, null);
        c.moveToFirst();
        int columnIndex = c.getColumnIndex(proj[0]);
        Log.i("Image Path on select",proj.toString());
        result = c.getString(columnIndex);
        c.close();
        Log.i("Image Path on select",result);
        return result;
    }


    public static String getPath(Context context, Uri uri) throws URISyntaxException {
        if ("content".equalsIgnoreCase(uri.getScheme())) {
            String[] projection = { "_data" };
            Cursor cursor = null;

            try {
                cursor = context.getContentResolver().query(uri, projection, null, null, null);
                int column_index = cursor.getColumnIndexOrThrow("_data");
                if (cursor.moveToFirst()) {
                    return cursor.getString(column_index);
                }
            } catch (Exception e) {
                // Eat it
            }
        }
        else if ("file".equalsIgnoreCase(uri.getScheme())) {
            return uri.getPath();
        }

        return null;
    }

    public static boolean isConnected(Context context) {


        ConnectivityManager connectivityManager = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = connectivityManager.getActiveNetworkInfo();
        if (networkInfo == null)
            return false;
        boolean isConnected = networkInfo != null && networkInfo.isConnectedOrConnecting();
        return isConnected;
    }

    public void showDialog(final Context context, String Title, String Message) {
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(context);

        alertDialogBuilder.setTitle(Title);
        alertDialogBuilder.setMessage(Message);
        alertDialogBuilder.setCancelable(false);
        // set positive button: Yes message
        alertDialogBuilder.setPositiveButton("Request Now", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {

            }
        });
        alertDialogBuilder.setNegativeButton("Close Application", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
               Activity ac=(Activity) context;
                ac.finish();
            }
        });

        AlertDialog alertDialog = alertDialogBuilder.create();
        // show alert
        alertDialog.show();
    }
    public static boolean isValidLatLng(Context context,double lat, double lng){
        if(lat < -90 || lat > 90)
        {
            return false;
        }
        else if(lng < -180 || lng > 180)
        {
            return false;
        }
        return true;
    }

    public  static String  getDeviceId(){
        String  unique_id=null;
        try{
            unique_id = Settings.Secure.getString(getApplicationContext().getContentResolver(), Settings.Secure.ANDROID_ID);
            Log.d("getDeviceId", "getDeviceId: "+unique_id);
        }catch (Exception e){
            unique_id= "ID NOT AVAILABLE";
            e.printStackTrace();
        }
        return unique_id;
    }


    public static String ConvertMilliSecondsToFormattedDate(){
        try {
                // String dateFormat = "dd-MM-yyyy hh:mma ";
                String dateFormat = "yyyy-MM-dd hh:mm";
                SimpleDateFormat simpleDateFormat = new SimpleDateFormat(dateFormat);
                Calendar calendar = Calendar.getInstance();
                calendar.setTimeInMillis(System.currentTimeMillis());
                Log.d("reminder converterr",simpleDateFormat.format(calendar.getTime()));
                return simpleDateFormat.format(calendar.getTime());

        }
        catch (Exception ex){
            return "NA";
        }
    }

    public static Bitmap getBitmapFromURL(String src) {
        try {
            URL url = new URL(src);
            HttpURLConnection connection = (HttpURLConnection) url.openConnection();
            connection.setDoInput(true);
            connection.connect();
            InputStream input = connection.getInputStream();
            Bitmap myBitmap = BitmapFactory.decodeStream(input);
            return myBitmap;
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }
    }

    public static String getAddress(Context context,double lat, double lng) {
        String add="";
        Geocoder geocoder = new Geocoder(context, Locale.getDefault());
        try {
            List<Address> addresses = geocoder.getFromLocation(lat, lng, 1);
            if (!addresses.isEmpty()){
                Address obj = addresses.get(0);
                if (obj!=null){
                    add=add+obj.getAddressLine(0);
                   Log.e("address1",""+add);
                }
               /* if (obj.getSubLocality()!=null){
                    add= add+","+obj.getSubLocality();
                    Log.e("addresssub",""+add);
                }
                if (obj.getLocality()!=null){
                    add =add +","+ obj.getLocality();
                    Log.e("addresslocal",""+add);
                }
                if (obj.getAdminArea()!=null){
                    add =add+","+obj.getAdminArea();
                    Log.e("addressadmin",""+add);
                }
                if (obj.getCountryName()!=null){
                    add= add+","+obj.getCountryName();
                    Log.e("addresscountry",""+add);
                }*/
                Log.e("address2",""+add);
                //add= add+","+obj.getSubLocality();
                //add =add +","+ obj.getLocality();
                //add =add+","+obj.getAdminArea();
                //add= add+","+obj.getCountryName();
            }
            Log.e("addresslarge", "Address" + add);

        } catch (IOException e) {
            // TODO Auto-generated catch block
            //e.printStackTrace();
        }
        return add;
    }

    public static void showDialog1(final Context context, String Title, String Message) {
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(context);

        alertDialogBuilder.setTitle(Title);
        alertDialogBuilder.setMessage(Message);
        // set positive button: Yes message
        alertDialogBuilder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                Intent intent = new Intent(Intent.ACTION_MAIN);
                intent.setComponent(new ComponentName("com.android.settings",
                        "com.android.settings.Settings$DataUsageSummaryActivity"));
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                context.startActivity(intent);
            }
        });


        AlertDialog alertDialog = alertDialogBuilder.create();
        // show alert
        alertDialog.show();
    }

    public boolean isEmpty(EditText etText) {
        return etText.getText().toString().trim().length() == 0;
    }

    public void validateEditText(Editable s, EditText sa, String msg) {
        if (!TextUtils.isEmpty(s)) {
            sa.setError(null);
        } else {
            sa.setError(msg);
        }
    }

    public void showToast(Context context, String message, int length, int gravity, int offsetX, int offsetY) {
        Toast toast = Toast.makeText(context, message, length);
        toast.setGravity(gravity, offsetX, offsetY);
        toast.show();

    }

    public static String getDateNTime(String dateRaw) {
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH);
        Date da = null;
        try {
            da = formatter.parse(dateRaw);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return new SimpleDateFormat("dd-MM-yyyy", Locale.ENGLISH).format(da);
    }

    public static String sendDateNTime(String dateRaw) {
        SimpleDateFormat formatter = new SimpleDateFormat("dd-MM-yyyy", Locale.ENGLISH);
        Date da = null;
        Log.e("dateraw",""+formatter);
        try {
            da = formatter.parse(dateRaw);
            Log.e("da",""+da);
        } catch (ParseException e) {

        }
        return new SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH).format(da);
    }

    public static boolean emailValidator(String email) {
        boolean isValid = false;
        if (Build.VERSION.SDK_INT >= 8) {
            return android.util.Patterns.EMAIL_ADDRESS.matcher(email).matches();
        }
        String expression = "^[_A-Za-z0-9-]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";
        CharSequence inputStr = email;
        Pattern pattern = Pattern.compile(expression);
        Matcher matcher = pattern.matcher(inputStr);
        if (matcher.matches()) {
            isValid = true;
        }
        return isValid;
    }

    public static boolean emIdValidator(String id) {
        boolean isValid = false;

        String expression = "^([0-9]{3})*-([0-9]{4})*-([0-9]{7})*-([0-9])$";
        CharSequence inputStr = id;
        Pattern pattern = Pattern.compile(expression);
        Matcher matcher = pattern.matcher(inputStr);
        if (matcher.matches()) {
            isValid = true;
        }
        return isValid;
    }

    public static String getTimeStampInString() {
        return new SimpleDateFormat("yyyyMMdd_HHmmss", Locale.getDefault()).format(new Date());
    }

    public static long getTimeStampInLong() {
        return System.currentTimeMillis();
    }

    public Bitmap pathToBitmap( String photoPath)
    {
        BitmapFactory.Options options = new BitmapFactory.Options();
        options.inPreferredConfig = Bitmap.Config.ARGB_8888;
        Bitmap bitmap = BitmapFactory.decodeFile(photoPath, options);
        return bitmap;
    }
}
