package com.abacusdesk.instafeed.instafeed.adapter;

import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.abacusdesk.instafeed.instafeed.R;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by ishaan on 6/29/2017.
 */


import com.abacusdesk.instafeed.instafeed.Util.Alerts;
import com.abacusdesk.instafeed.instafeed.Util.Apis;
import com.abacusdesk.instafeed.instafeed.Util.SaveSharedPreference;
import com.abacusdesk.instafeed.instafeed.custom_dialog.Dialogs;
import com.abacusdesk.instafeed.instafeed.fragments.Opiniondetail_fragment;
import com.abacusdesk.instafeed.instafeed.fragments.Publicprofile_Fragment;
import com.abacusdesk.instafeed.instafeed.model.OpinionModel_public;
import com.android.volley.AuthFailureError;
import com.android.volley.NoConnectionError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.squareup.picasso.Picasso;

import org.json.JSONException;
import org.json.JSONObject;

    public class Opinion_Adapter extends RecyclerView.Adapter<Opinion_Adapter.MyViewHolder>{

        Context context;
        ArrayList<OpinionModel_public.DataBean> arrayList;

    public Opinion_Adapter(Context context, ArrayList<OpinionModel_public.DataBean> arrayList){
            this.context=context;
            this.arrayList=arrayList;
        }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView title,txtdescr;
        public ImageView imgpro, imgshare;
        public TextView date;
        public LinearLayout lndesc;
        //public LinearLayout lnrow;

        public MyViewHolder(View view) {
            super(view);
            this.title = (TextView) itemView.findViewById(R.id.txt_title);
            this.date = (TextView) itemView.findViewById(R.id.txt_date);
            this.imgshare = (ImageView) itemView.findViewById(R.id.img_share);
            this.imgpro = (ImageView) itemView.findViewById(R.id.img_pro);

            this.txtdescr = (TextView) itemView.findViewById(R.id.txt_descr);
            this.lndesc = (LinearLayout) itemView.findViewById(R.id.ln_desc);
        }
    }

        @Override
        public Opinion_Adapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View itemView = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.item_profilelist, parent, false);
            return new Opinion_Adapter.MyViewHolder(itemView);
        }

        @Override
        public void onBindViewHolder(Opinion_Adapter.MyViewHolder holder, final int position) {
            String myFormat = "yyyy-MM-dd HH:mm:ss";
            DateFormat sdformat = new SimpleDateFormat(myFormat);
            DateFormat targetFormat = new SimpleDateFormat("MMMM dd, yyyy");

            final OpinionModel_public.DataBean dataBean = arrayList.get(position);
            holder.txtdescr.setText(dataBean.getDescription());
            holder.title.setText(dataBean.getTitle());
            try {
                String formattedDate = targetFormat.format(sdformat.parse(dataBean.getDt_added()));
                holder.date.setText(formattedDate);
            } catch (ParseException e) {
                e.printStackTrace();
            }
            try{
                Picasso.with(context).load(dataBean.getImage()).error(R.drawable.demo_news).into(holder.imgpro);
            }catch (Exception e){

            }

            if (Publicprofile_Fragment.flag ==false){
                holder.imgshare.setVisibility(View.VISIBLE);
            }else {
                holder.imgshare.setVisibility(View.INVISIBLE);
            }

            holder.imgshare.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    final AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(context);
                    alertDialogBuilder.setMessage("Are you sure,You want to delete this post");
                    alertDialogBuilder.setPositiveButton("yes",
                            new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface arg0, int arg1) {
                                    deletePost(dataBean.getId(),position);
                                }
                            });

                    alertDialogBuilder.setNegativeButton("No",new DialogInterface.OnClickListener()
                    {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                        }
                    });
                    AlertDialog alertDialog = alertDialogBuilder.create();
                    alertDialog.show();

                }
            });

            holder.lndesc.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Opiniondetail_fragment fragment = new Opiniondetail_fragment();
                    Bundle bundle = new Bundle();
                    bundle.putString("id",dataBean.getId());
                    fragment.setArguments(bundle);

                    AppCompatActivity activity = (AppCompatActivity) v.getContext();
                    activity.getSupportFragmentManager().beginTransaction()
                            .add(R.id.frame_container,fragment,"tempfrag").addToBackStack(null).commit();

                }
            });
        }

    public void deletePost(final String postid, final int position) {
        //Dialogs.showProDialog(context, "Loading");
        StringRequest stringRequest = new StringRequest(Request.Method.POST, Apis.private_opinionsdelete,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            JSONObject jsonObject=new JSONObject(response);
                            if (jsonObject.getString("status").equals("200") && jsonObject.getString("message").equalsIgnoreCase("success")){
                                arrayList.remove(position);
                                notifyItemRemoved(position);
                                notifyItemRangeChanged(position,arrayList.size());
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Dialogs.disDialog();
                        DialogInterface.OnClickListener onClickTryAgain = new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                //Dialogs.disDialog();
                                deletePost(postid,position);
                            }
                        };
                        if (error instanceof TimeoutError) {
                            // Dialogs.disDialog();
                            Alerts.timeoutErrorAlert(context, onClickTryAgain);
                        } else if (error instanceof NoConnectionError) {
                            // Dialogs.disDialog();
                            Alerts.internetConnectionErrorAlert(context, onClickTryAgain);
                        }
                    }
                })
        {

            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> map = new HashMap<String, String>();
                map.put("user", SaveSharedPreference.getUserID(context));
                map.put("token", SaveSharedPreference.getPrefToken(context));
                map.put("id",postid);
                Log.e("mapcomment",""+map);
                return map;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Content-Type", "application/x-www-form-urlencoded");
                return params;
            }
        };

        RequestQueue requestQueue = Volley.newRequestQueue(context);
        requestQueue.add(stringRequest);
    }
        @Override
        public int getItemCount() {
            return arrayList.size();
        }
    }
