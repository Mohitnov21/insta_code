package com.abacusdesk.instafeed.instafeed.adapter;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.abacusdesk.instafeed.instafeed.R;
import com.abacusdesk.instafeed.instafeed.Util.Apis;
import com.abacusdesk.instafeed.instafeed.Util.SaveSharedPreference;
import com.abacusdesk.instafeed.instafeed.custom_dialog.Dialogs;
import com.abacusdesk.instafeed.instafeed.fragments.CommentsList_fragment;
import com.abacusdesk.instafeed.instafeed.fragments.Talentdetail_fragment;
import com.abacusdesk.instafeed.instafeed.model.Talent_allmodel;
import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;

import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpHead;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import static org.acra.ACRA.log;

/**
 * Created by abacusdesk on 2017-07-14.
 */

 public class Talent_allAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

        private ArrayList<Talent_allmodel.DataBean> arrayList;
        private Activity activity;
        private final int NORMAL_ROW = 0, FIRST_ROW = 1;
        private String imagetemp="";
        Talent_allmodel.DataBean dataBean2;

        public Talent_allAdapter(Activity activity, ArrayList<Talent_allmodel.DataBean> arrayList) {
            this.activity = activity;
            this.arrayList = arrayList;
            Log.e("arraylist size", "" + arrayList.size());
        }

        @Override
        public int getItemViewType(int position) {

                return NORMAL_ROW;

        }

        @Override
        public int getItemCount() {
            return (null != arrayList ? arrayList.size() : 0);
        }

        @Override
        public void onBindViewHolder(final RecyclerView.ViewHolder holder, final int position) {
            String myFormat = "yyyy-MM-dd HH:mm:ss";
            DateFormat sdformat = new SimpleDateFormat(myFormat);
            DateFormat targetFormat = new SimpleDateFormat("MMMM dd, yyyy");

            final Talent_allmodel.DataBean  dataBean = arrayList.get(position);
            //dataBean2=arrayList.get(position);

            if (!dataBean.getUsername().isEmpty() && dataBean.getUsername()!=null){
                ((NormalHolder)holder).txtname.setText(dataBean.getUsername());
            }else {
                ((NormalHolder)holder).txtname.setText("");
            }
            // ((NormalHolder)holder).title.setText(dataBean.getTitle());

            if (dataBean.getAvatar()!=null && !dataBean.getAvatar().isEmpty()){
                Picasso.with(activity).load(dataBean.getAvatar()).
                        error(R.drawable.user).into(((NormalHolder) holder).imgprofile);
            }else {
                ((NormalHolder) holder).imgprofile.setImageResource(R.drawable.user);
            }

            if (dataBean.getTotal_likes()!=null &&
                    !dataBean.getTotal_likes().toString().isEmpty()){
                ((NormalHolder)holder).txt_totallikes.setText(dataBean.getTotal_likes());
            }else {
                ((NormalHolder)holder).txt_totallikes.setText("0");
            }

            if (dataBean.getTotal_comments()!=null &&
                    !dataBean.getTotal_comments().equalsIgnoreCase("0") &&
                    !dataBean.getTotal_comments().isEmpty()){
                ((NormalHolder)holder).txt_totalcomments.setText(dataBean.getTotal_comments());
            }else {
                ((NormalHolder)holder).txt_totalcomments.setText("0");
                //((NormalHolder)holder).imgdot.setVisibility(View.GONE);
                //((NormalHolder)holder).txt_totalcomments.setText("0");
            }

            if (dataBean.getTotal_views()!=null && !dataBean.getTotal_views().isEmpty()){
                ((NormalHolder)holder).txt_totalviews.setText(dataBean.getTotal_views());
            }else {
                ((NormalHolder)holder).txt_totalviews.setText("0");
                //((NormalHolder)holder).txt_totalviews.setText("0");
            }
            try{
                if (dataBean.getTitle()!=null &&
                        !dataBean.getTitle().isEmpty()){
                    String temp_str=dataBean.getTitle().toString().trim();
                    temp_str = temp_str.substring(0,1).toUpperCase() + temp_str.substring(1);
                    ((NormalHolder)holder).txttitle.setText(temp_str);
                }else {
                    ((NormalHolder)holder).txttitle.setVisibility(View.GONE);
                }
            }catch (Exception e){
                Log.e("exception lang",""+e);
            }

              if (!dataBean.getVideo_thumb().equals("")){
                    ((NormalHolder) holder).imgvideoicon.setVisibility(View.VISIBLE);
                    Picasso.with(activity).load(dataBean.getVideo_330x210())
                            .error(R.drawable.talentsdefault).into(((NormalHolder) holder).imgmain);
                    ((NormalHolder)holder).txttime.setVisibility(View.VISIBLE);
                    try {
                        String formattedDate = targetFormat.format(sdformat.parse(dataBean.getDt_added()));
                        ((NormalHolder)holder).txttime.setText(formattedDate);
                    } catch (ParseException e) {
                        e.printStackTrace();
                    }
                } else if (dataBean.getImage()!=null) {
                    ((NormalHolder) holder).imgvideoicon.setVisibility(View.GONE);
                    Picasso.with(activity).load(dataBean.getImage_330x210())
                            .error(R.drawable.talentsdefault).into(((NormalHolder) holder).imgmain);
                    ((NormalHolder)holder).txttime.setVisibility(View.VISIBLE);
                    try {
                        String formattedDate = targetFormat.format(sdformat.parse(dataBean.getDt_added()));
                        ((NormalHolder)holder).txttime.setText(formattedDate);
                    } catch (ParseException e) {
                        e.printStackTrace();
                    }
                }
            //isLiked(((NormalHolder) holder),position);
            ((NormalHolder) holder).lncommment.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    CommentsList_fragment fragment = new CommentsList_fragment();
                    Bundle bundle = new Bundle();
                    //bundle.putSerializable("arrayhash", arrayhashComent);
                    bundle.putString("postid", dataBean.getId());
                    bundle.putString("type", "talents");
                    bundle.putString("catid",dataBean.getTalent_category_id());
                    fragment.setArguments(bundle);

                    AppCompatActivity activity = (AppCompatActivity)v.getContext();
                    activity.getSupportFragmentManager().beginTransaction()
                            .add(R.id.frame_container, fragment,"comment")
                            .addToBackStack("comment").commit();

                }
            });
            ((NormalHolder)holder).lnshare.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    try {
                        dataBean2=dataBean;
                        if (!dataBean.getVideo_thumb().isEmpty()){
                            imagetemp=dataBean.getVideo_thumb();
                        }else {
                            imagetemp=dataBean.getImage();
                        }
                        share_image();
                    }catch (Exception e){

                    }
                }
            });

            try {
                if (!SaveSharedPreference.getUserID(activity).equals("")
                        && SaveSharedPreference.getUserID(activity)!=null){
                    if (dataBean.getIs_like().equals("1")){
                        Log.e("userid api gid","called"+dataBean.getTitle());
                        ((NormalHolder) holder).imglike.setImageResource(R.drawable.thumb_black);
                        ((NormalHolder) holder).txtliked.setTextColor(activity.
                                getResources().getColor(R.color.black));
                        ((NormalHolder) holder).txtliked.setTypeface(null, Typeface.BOLD);
                    }else {
                        ((NormalHolder) holder).imglike.setImageResource(R.drawable.like_rr);
                        ((NormalHolder) holder).txtliked.setTextColor(activity.
                                getResources().getColor(R.color.light_gray_new));
                        ((NormalHolder) holder).txtliked.setTypeface(null, Typeface.NORMAL);
                        Log.e("userid api gid not","called"+dataBean.getTitle());
                    }
                }
            }catch (Exception e){
                Log.e("exception",""+e);
            }
            ((NormalHolder) holder).lnlike.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    dataBean2=arrayList.get(position);
                    if (SaveSharedPreference.getUserID(activity).isEmpty()){
                        Toast.makeText(activity,"please login to continue !", Toast.LENGTH_SHORT).show();
                    }else {
                        if (dataBean.getIs_like().equals("1")){
                            revert_like(((NormalHolder)holder),position);
                        }else {
                            likePost(((NormalHolder)holder),position);
                        }
                    }
                }
            });

            ((NormalHolder) holder).lnone.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Talentdetail_fragment fragment = new Talentdetail_fragment();
                        Bundle bundle = new Bundle();
                        bundle.putString("catid",dataBean.getTalent_category_id());
                        bundle.putString("postid",dataBean.getId());
                        fragment.setArguments(bundle);

                        AppCompatActivity activity = (AppCompatActivity) v.getContext();
                        activity.getSupportFragmentManager().beginTransaction()
                      .add(R.id.frame_container, fragment,"tempfrag").addToBackStack(null).commitAllowingStateLoss();
                 }
                });
        }


    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View view = null;
            //Based on view type decide which type of view to supply with viewHolder
            switch (viewType) {
                case NORMAL_ROW:
                    view = LayoutInflater.from(parent.getContext()).
                            inflate(R.layout.item_fblayout, parent, false);
                    return new NormalHolder(view);
             }
            return null;
        }

    public void likePost(final NormalHolder holder, final int position) {
        StringRequest stringRequest = new StringRequest(Request.Method.POST, Apis.talentall_vote,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.e("log", "" + response);
                        try {
                            JSONObject jsonObject = new JSONObject(response);
                            if (jsonObject.getString("status").equalsIgnoreCase("200") &&
                                    jsonObject.getString("message").equalsIgnoreCase("success")) {
                                holder.imglike.setImageResource(R.drawable.thumb_black);
                                holder.txtliked.setTextColor(activity.
                                        getResources().getColor(R.color.black));
                                holder.txtliked.setTypeface(null, Typeface.BOLD);

                                Talent_allmodel.DataBean dataBean_temp=arrayList.get(position);
                                dataBean_temp.setIs_like("1");
                                arrayList.set(position,dataBean_temp);
                                Log.e("after success",""+dataBean_temp.getIs_like());

                                if (dataBean_temp.getTotal_likes()!=null &&
                                        !dataBean_temp.getTotal_likes().isEmpty()){
                                    int temp=Integer.parseInt(dataBean_temp.getTotal_likes())+1;
                                    dataBean_temp.setTotal_likes(""+temp);
                                }else {
                                    int temp=1;
                                    dataBean_temp.setTotal_likes(""+temp);
                                }
                                holder.txt_totallikes.setText(dataBean_temp.getTotal_likes());
                            }
                        } catch (JSONException e) {
                            // e.printStackTrace();
                        }
                    }
                },

                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Dialogs.disDialog();
                    }
                }) {

            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> map = new HashMap<String, String>();
                map.put("user", SaveSharedPreference.getUserID(activity));
                map.put("token", SaveSharedPreference.getPrefToken(activity));
                map.put("id", dataBean2.getId());
                map.put("vote","u");
                Log.e("mapcomment", "" + map);
                return map;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Content-Type", "application/x-www-form-urlencoded");
                return params;
            }
        };

        RequestQueue requestQueue = Volley.newRequestQueue(activity);
        requestQueue.add(stringRequest);
    }

    public void revert_like(final NormalHolder holder, final int position) {
        Log.e("current position",""+position);
        StringRequest stringRequest = new StringRequest(Request.Method.POST, Apis.talentall_deletevote,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.e("log", "" + response);
                        try {
                            JSONObject jsonObject = new JSONObject(response);
                            if (jsonObject.getString("status").equals("200") &&
                                    jsonObject.getString("message").equals("success")) {
                                holder.imglike.setImageResource(R.drawable.like_rr);
                                holder.txtliked.setTextColor(activity.
                                        getResources().getColor(R.color.light_gray_new));
                                holder.txtliked.setTypeface(null, Typeface.NORMAL);


                                Talent_allmodel.DataBean dataBean_temp=arrayList.get(position);
                                dataBean_temp.setIs_like("0");
                                arrayList.set(position,dataBean_temp);
                                Log.e("after",""+dataBean_temp.getIs_like());
                                if (dataBean_temp.getTotal_likes()!=null &&
                                        !dataBean_temp.getTotal_likes().isEmpty()
                                        && !dataBean_temp.getTotal_likes().equalsIgnoreCase("0")){
                                    int temp=Integer.parseInt(dataBean_temp.getTotal_likes())-1;
                                    dataBean_temp.setTotal_likes(""+temp);
                                }else {
                                    int temp=0;
                                    dataBean_temp.setTotal_likes(""+temp);
                                }
                                holder.txt_totallikes.setText(dataBean_temp.getTotal_likes());
                            }else {

                            }
                        } catch (JSONException e) {
                            // e.printStackTrace();
                        } catch (Exception e) {

                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Dialogs.disDialog();
                    }
                })
        {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> map = new HashMap<String, String>();
                map.put("user", SaveSharedPreference.getUserID(activity));
                map.put("token", SaveSharedPreference.getPrefToken(activity));
                map.put("id", dataBean2.getId());
                Log.e("mapvote", "" + map);
                return map;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Content-Type", "application/x-www-form-urlencoded");
                return params;
            }
        };
        RequestQueue requestQueue = Volley.newRequestQueue(activity);
        requestQueue.add(stringRequest);
    }

    public static class NormalHolder extends RecyclerView.ViewHolder {

        public TextView txttitle,txtname,txttime,txtlikes,txtliked,
                txt_totallikes,txt_totalcomments,txt_totalviews;
        public ImageView imgshare, imgmain,imgprofile,imgbookmark;
        public ImageView imgvideoicon,imglike,imgdot;
        public LinearLayout lnone,lnvideo,lnlike,lncommment,lnshare,lnviews;

        public NormalHolder(View itemView) {
            super(itemView);
            this.txtname = (TextView) itemView.findViewById(R.id.txt_name);
            this.txttime = (TextView) itemView.findViewById(R.id.txt_time);
            this.txttitle = (TextView) itemView.findViewById(R.id.txt_title);
            this.txtlikes = (TextView) itemView.findViewById(R.id.txt_like);
            this.txtliked = (TextView) itemView.findViewById(R.id.txt_liked);

            //counter section
            this.txt_totallikes = (TextView) itemView.findViewById(R.id.txt_likecount);
            this.txt_totalcomments = (TextView) itemView.findViewById(R.id.total_comments);
            this.txt_totalviews = (TextView) itemView.findViewById(R.id.txt_totalviews);
            this.imgdot = (ImageView) itemView.findViewById(R.id.img_dot);
            this.lnviews = (LinearLayout) itemView.findViewById(R.id.ln_views);

            this.imgvideoicon = (ImageView) itemView.findViewById(R.id.img_videoicon);
            this.imgprofile = (ImageView) itemView.findViewById(R.id.img_profile);
            this.imgbookmark = (ImageView) itemView.findViewById(R.id.img_bookmark);
            this.imgmain = (ImageView) itemView.findViewById(R.id.img_main);
            this.imglike = (ImageView) itemView.findViewById(R.id.img_like);

            this.lnlike = (LinearLayout) itemView.findViewById(R.id.ln_like);
            this.lncommment = (LinearLayout) itemView.findViewById(R.id.ln_comment);
            this.lnshare = (LinearLayout) itemView.findViewById(R.id.ln_share);

            this.imgshare = (ImageView) itemView.findViewById(R.id.img_share);


            this.lnone = (LinearLayout) itemView.findViewById(R.id.ln_one);
            //this.lnvideo = (LinearLayout) itemView.findViewById(R.id.ln_video);
        }
    }

    public class MyAsync extends AsyncTask<Void, Void, Bitmap> {
        @Override
        protected Bitmap doInBackground(Void... params) {

            try {
                if (!imagetemp.isEmpty()) {
                    URL url = new URL(imagetemp);
                    HttpURLConnection connection = (HttpURLConnection) url.openConnection();
                    connection.setDoInput(true);
                    connection.connect();
                    InputStream input = connection.getInputStream();
                    Bitmap myBitmap = BitmapFactory.decodeStream(input);
                    return myBitmap;
                } else {
                    return null;
                }

            } catch (IOException e) {
                e.printStackTrace();
                return null;
            } catch (Exception e) {
                return null;
            }
        }
    }

    private void share_image() {
            Log.e("imagetemp",""+imagetemp);
            try {
                MyAsync obj = new MyAsync() {
                    @Override
                    protected void onPostExecute(Bitmap bmp) {
                        super.onPostExecute(bmp);
                        try {
                        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
                        if (bmp!=null)
                        bmp.compress(Bitmap.CompressFormat.JPEG, 100, bytes);
                        File f = new File(Environment.getExternalStorageDirectory() + File.separator + "temporary_file.jpg");

                            f.createNewFile();
                            FileOutputStream fo = new FileOutputStream(f);
                            fo.write(bytes.toByteArray());


                        } catch (IOException e) {
                            e.printStackTrace();
                        } catch (Exception e) {

                        }
                    }
                };
                if (!imagetemp.equals(""))
                    obj.execute();

            } catch (Exception e) {

            }

            Intent sharingIntent = new Intent(Intent.ACTION_SEND);
            sharingIntent.setType("*/*");
            try {
              if (!imagetemp.isEmpty())
               sharingIntent.putExtra(Intent.EXTRA_STREAM, Uri.parse("file:///sdcard/temporary_file.jpg"));

               sharingIntent.putExtra(Intent.EXTRA_TEXT,dataBean2.getTitle() + "\n" + Apis.talent_share + dataBean2.getSlug());
               Log.e("shreinterjyt", "" + sharingIntent);
                activity.startActivity(Intent.createChooser(sharingIntent, "Share via"));
            } catch (Exception e) {
                log.e("exception", "" + e);
            }
        }

 }