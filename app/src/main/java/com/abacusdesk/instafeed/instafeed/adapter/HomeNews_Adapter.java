package com.abacusdesk.instafeed.instafeed.adapter;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.abacusdesk.instafeed.instafeed.R;
import com.abacusdesk.instafeed.instafeed.fragments.Explore_detailfragment;
import com.abacusdesk.instafeed.instafeed.fragments.Newsdetail_fragment;
import com.abacusdesk.instafeed.instafeed.fragments.Talentdetail_fragment;
import com.squareup.picasso.Picasso;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by ishaan on 7/7/2017.
 */

 public class HomeNews_Adapter  extends RecyclerView.Adapter<HomeNews_Adapter.MyViewHolder> {
    private ArrayList<HashMap<String, String>> arrayList;
    private Activity activity;
    private String type;
    public static String urlApi="http://instafeed.org/";

    public HomeNews_Adapter(Activity activity, ArrayList<HashMap<String, String>> arrayList,String type) {
        this.activity = activity;
        this.arrayList = arrayList;
        this.type=type;
        notifyDataSetChanged();
    }

        public class MyViewHolder extends RecyclerView.ViewHolder {

            public TextView title,txtshortdes,txtname;
            public ImageView imgpro, imgshare;
            public TextView date;
            public LinearLayout lnrow;

            public MyViewHolder(View itemView) {
                super(itemView);
                this.lnrow = (LinearLayout) itemView.findViewById(R.id.ln_rowitem);
                this.txtshortdes = (TextView) itemView.findViewById(R.id.txt_shortdescr);
                this.txtname = (TextView) itemView.findViewById(R.id.txt_name);
                this.title = (TextView) itemView.findViewById(R.id.txt_title);
                this.date = (TextView) itemView.findViewById(R.id.txt_date);
                this.imgpro = (ImageView) itemView.findViewById(R.id.img_pro);
                this.imgshare = (ImageView) itemView.findViewById(R.id.img_share);
            }
        }

        @Override
        public HomeNews_Adapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View itemView = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.item_rownews, parent, false);
            return new HomeNews_Adapter.MyViewHolder(itemView);
        }

        @Override
        public void onBindViewHolder(HomeNews_Adapter.MyViewHolder holder, final int position) {
            String myFormat = "yyyy-MM-dd HH:mm:ss";
            DateFormat sdformat = new SimpleDateFormat(myFormat);
            DateFormat targetFormat = new SimpleDateFormat("MMMM dd, yyyy");
            try {
                String formattedDate = targetFormat.format(sdformat.parse(arrayList.get(position).get("dateadded")));
                holder.date.setText(formattedDate);
            } catch (ParseException e) {
                e.printStackTrace();
            }
            if (arrayList.get(position).get("shortdes")!="null"){
                Log.e("null condition",""+arrayList.get(position).get("shortdes"));
                holder.txtshortdes.setText(arrayList.get(position).get("shortdes"));
            }
            if (type.equalsIgnoreCase("news")){
                if (arrayList.get(position).get("isanonymous").equalsIgnoreCase("y")){
                    holder.txtname.setText("Anonymous");
                }else {
                    holder.txtname.setText(arrayList.get(position).get("username"));
                }
            }else if (arrayList.get(position).get("username")!="null"){
                holder.txtname.setText(arrayList.get(position).get("username"));
            }
            holder.imgshare.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent sharingIntent = new Intent(Intent.ACTION_SEND);
                    sharingIntent.setType("text/plain");
                    if (type.equalsIgnoreCase("news")){
                        sharingIntent.putExtra(Intent.EXTRA_SUBJECT,arrayList.get(position).get("title"));
                        sharingIntent.putExtra(Intent.EXTRA_TEXT,urlApi+"news/"+arrayList.get(position).get("slug")+" "+arrayList.get(position).get("shortdes"));
                        Log.e("shreinterjyt",""+sharingIntent);
                        activity.startActivity(Intent.createChooser(sharingIntent, "Share via"));

                    } else if (type.equalsIgnoreCase("explorer")){
                        sharingIntent.putExtra(Intent.EXTRA_SUBJECT,arrayList.get(position).get("title"));
                        sharingIntent.putExtra(Intent.EXTRA_TEXT,urlApi+"explorer/"+arrayList.get(position).get("slug")+" "+arrayList.get(position).get("shortdes"));
                        Log.e("shreinterjyt",""+sharingIntent);
                        activity.startActivity(Intent.createChooser(sharingIntent, "Share via"));

                    } else if (type.equalsIgnoreCase("talents")){
                        sharingIntent.putExtra(Intent.EXTRA_SUBJECT,arrayList.get(position).get("title"));
                        sharingIntent.putExtra(Intent.EXTRA_TEXT,urlApi+"talents/"+arrayList.get(position).get("slug")+" "+arrayList.get(position).get("shortdes"));
                        Log.e("shreinterjyt",""+sharingIntent);
                          activity.startActivity(Intent.createChooser(sharingIntent, "Share via"));
                    }
                }
            });
            holder.title.setText(arrayList.get(position).get("title"));
              if (!arrayList.get(position).get("image").isEmpty()){
                  Picasso.with(activity).load(arrayList.get(position).get("image")).error(R.drawable.demo_news).into(holder.imgpro);
                }
                holder.lnrow.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (type.equalsIgnoreCase("news")){
                       // Newsdetail_fragment.postid=arrayList.get(position).get("id");
                        //Newsdetail_fragment.catid=arrayList.get(position).get("catid");
                        AppCompatActivity activity = (AppCompatActivity) v.getContext();
                        activity.getSupportFragmentManager().beginTransaction().add(R.id.frame_container, new Newsdetail_fragment(),"tempfrag").addToBackStack(null).commit();

                    }else if (type.equalsIgnoreCase("explorer")){
                        Explore_detailfragment fragment = new Explore_detailfragment();
                        Bundle bundle = new Bundle();
                        bundle.putString("catid",arrayList.get(position).get("catid"));
                        bundle.putString("postid",arrayList.get(position).get("id"));
                        fragment.setArguments(bundle);

                        AppCompatActivity activity = (AppCompatActivity) v.getContext();
                        activity.getSupportFragmentManager().beginTransaction().add(R.id.frame_container, new Explore_detailfragment(),"tempfrag").addToBackStack(null).commit();

                    } else if (type.equalsIgnoreCase("talents")){

                        Talentdetail_fragment fragment = new Talentdetail_fragment();
                        Bundle bundle = new Bundle();
                        bundle.putString("catid",arrayList.get(position).get("catid"));
                        bundle.putString("postid",arrayList.get(position).get("id"));
                        fragment.setArguments(bundle);

                        AppCompatActivity activity = (AppCompatActivity) v.getContext();
                        activity.getSupportFragmentManager().beginTransaction().add(R.id.frame_container,fragment,"tempfrag").addToBackStack(null).commit();
                    }
                }
            });
        }

        @Override
        public int getItemCount() {
            return arrayList.size();
        }
}
