package com.abacusdesk.instafeed.instafeed;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.view.ViewPager;
import android.view.View;
import android.widget.Button;

import com.abacusdesk.instafeed.instafeed.Util.SaveSharedPreference;
import com.abacusdesk.instafeed.instafeed.adapter.SlidAdapter;
import com.abacusdesk.instafeed.instafeed.fragments.HomeTab_fragment;

import me.relex.circleindicator.CircleIndicator;

/**
 * Created by abacusdesk on 2017-08-28.
 */

public class IntroScreens extends Activity {

    private ViewPager
    viewPager;
    int currentposition;
    Button btnskip;
    private int array_images[]={R.drawable.intro_1,R.drawable.intro_2,R.drawable.intro_3};

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.introscreen_lay);
        init();
    }

    public void init() {
        viewPager = (ViewPager) findViewById(R.id.pager);
        btnskip=(Button)findViewById(R.id.btn_skip);
        btnskip.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SaveSharedPreference.setIsSplash(IntroScreens.this,"true");
                startActivity(new Intent(IntroScreens.this, NavDrawerActivity.class));
                finish();
            }
        });
        viewPager.setAdapter(new SlidAdapter(IntroScreens.this, array_images));

        CircleIndicator indicator = (CircleIndicator) findViewById(R.id.indicator);

        indicator.setViewPager(viewPager);
        viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

                currentposition=position;

            }

            @Override
            public void onPageSelected(int position) {

            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });
    }

    android.os.Handler handler= new android.os.Handler();
    Runnable runnable = new Runnable() {
        public void run() {
            if( currentposition >= 2){
                currentposition = 0;
            }else{

                currentposition = currentposition+1;
            }
            viewPager.setCurrentItem(currentposition, true);
            handler.postDelayed(runnable, 10000);
        }
    };

    @Override
    public void onPause() {
        super.onPause();
        if (handler!= null) {
            handler.removeCallbacks(runnable);
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        handler.postDelayed(runnable, 10000);
    }
}
