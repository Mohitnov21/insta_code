package com.abacusdesk.instafeed.instafeed.fragments;

import android.content.DialogInterface;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.abacusdesk.instafeed.instafeed.NavDrawerActivity;
import com.abacusdesk.instafeed.instafeed.fontsClass.CustomRegularText;
import com.abacusdesk.instafeed.instafeed.R;
import com.abacusdesk.instafeed.instafeed.Util.Alerts;
import com.abacusdesk.instafeed.instafeed.Util.Apis;
import com.abacusdesk.instafeed.instafeed.Util.SaveSharedPreference;
import com.abacusdesk.instafeed.instafeed.custom_dialog.Dialogs;
import com.abacusdesk.instafeed.instafeed.model.Polls_detailmodel;
import com.android.volley.AuthFailureError;
import com.android.volley.NoConnectionError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.gson.Gson;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by abacusdesk on 2017-08-10.
 */

public class Polls_detailfragment  extends Fragment implements View.OnClickListener{

    View view;
    TextView txttitle,txtdecription,txtstartdate,txtenddate,txtpollclosed;

    Polls_detailmodel polls_detailmodel;
    ArrayList<Polls_detailmodel.DataBean> array_polldetail=new ArrayList<>();
    Polls_detailmodel.DataBean dataBean_detail,databean_options,databean_votes;
    Polls_detailmodel.DataBean.VotesBean totalVotesBean;
    Polls_detailmodel.DataBean.OptionsBean optionsBean;

    public static String id="",pollid="",temp,optionid="",comment="";

    private String isvoted="";
    Boolean flag=false;
    Boolean like=false,dislike=false,fav=false;
    RadioGroup radiogrp;
    Button btnsubmit;
    LinearLayout lnprogress;
    LinearLayout lnradiobtn;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.pollsdetail_frag, container, false);
        init();
        getActivity().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_PAN |WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);
        return view;
    }

    private void init(){
        radiogrp=(RadioGroup)view.findViewById(R.id.radio_grp);

        txtstartdate=(TextView)view.findViewById(R.id.txt_startdate);
        txtenddate=(TextView)view.findViewById(R.id.txt_enddate);
        txtpollclosed=(TextView)view.findViewById(R.id.txt_pollclosed);

        lnprogress=(LinearLayout)view.findViewById(R.id.ln_progress);
        lnradiobtn=(LinearLayout)view.findViewById(R.id.ln_radiobtn);

        btnsubmit=(Button)view.findViewById(R.id.btn_submit);
        txttitle=(TextView)view.findViewById(R.id.txt_title);
        txtdecription=(TextView)view.findViewById(R.id.txt_descr);

        if (NavDrawerActivity.toolbar!=null){
            NavDrawerActivity.toolbar.setLogo(null);
            NavDrawerActivity.toolbar.setTitle("Polls");
        }
        temp= Apis.Polls_detail.replace("id",id);
        btnsubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (pollid.equals("") && optionid.equals("")){
                    Toast.makeText(getActivity(),"please select either one option !", Toast.LENGTH_SHORT).show();
                }else {
                    if (SaveSharedPreference.getUserID(getContext()).equals("")){
                        Toast.makeText(getActivity(),"Login! first", Toast.LENGTH_SHORT).show();
                    }else {
                        postVote();
                    }

                }
            }
        });

        view.setFocusableInTouchMode(true);
        view.requestFocus();
        view.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if (keyCode == KeyEvent.KEYCODE_BACK) {
               getActivity().getSupportFragmentManager().popBackStack();
                    return true;
                } else {
                    return false;
                }
            }
        });

       isVoted();
    }

    public  void getPollsdetail() {
        pollid="";
        optionid="";
      //  Dialogs.showProDialog(getActivity(), "Loading");
        Log.e("temporaryurl",""+temp);
        StringRequest stringRequest = new StringRequest(Request.Method.GET,temp ,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.e("log", "" + response);
                        try {
                            getResponse(response);
                        } catch (JSONException e) {
                            //e.printStackTrace();
                        } catch (ParseException e) {
                            //e.printStackTrace();
                        }
               //         Dialogs.disDialog();
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                       try {
                           Toast.makeText(getContext(),"weak Internet connection!", Toast.LENGTH_SHORT).show();
                       }catch (Exception e){

                       }

                    }
                })
        {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Content-Type", "application/x-www-form-urlencoded");
                return params;
            }
        };
        RequestQueue requestQueue = Volley.newRequestQueue(getActivity());
        requestQueue.add(stringRequest);
    }

    public  void getResponse(String response ) throws JSONException, ParseException {
        if (!response.equals(null)) {
            Log.e("log", "" + response);
            JSONObject jsonObject = new JSONObject(response);
            Object object = jsonObject.get("data");
            if (object instanceof JSONArray) {
                Gson gson = new Gson();
                polls_detailmodel = gson.fromJson(response, Polls_detailmodel.class);
                int status = polls_detailmodel.getStatus();
                String msg = polls_detailmodel.getMessage();

                if (msg.equalsIgnoreCase("success") && status == 200) {
                    dataBean_detail = polls_detailmodel.getData().get(0);
                    databean_options = polls_detailmodel.getData().get(1);
                     totalVotesBean=   polls_detailmodel.getData().get(2).getVotes();
                    Log.e("totalvotes_poll",""+polls_detailmodel.getData().get(2).getVotes().getTotal_votes());
                    Log.e("totalvotes",""+totalVotesBean.getTotal_votes());
                    if (dataBean_detail != null) {
                        txttitle.setText(dataBean_detail.getTitle());

                        txtstartdate.setText(Newsdetail_fragment.getDate(dataBean_detail.getDt_start()));
                        txtenddate.setText("/"+Newsdetail_fragment.getDate(dataBean_detail.getDt_end()));
                        txtdecription.setText(dataBean_detail.getDescription());
                        if (dataBean_detail.getStatus().equalsIgnoreCase("A")){
                             radiogrp.setVisibility(View.VISIBLE);
                             btnsubmit.setVisibility(View.VISIBLE);
                        }else {
                           radiogrp.setVisibility(View.GONE);
                           txtpollclosed.setVisibility(View.VISIBLE);
                           btnsubmit.setVisibility(View.GONE);
                        }
                    }
                }
            }
            RadioGroup.LayoutParams rprms;
            for (int i=0;i<databean_options.getOptions().size();i++){
                final RadioButton radioButton = new RadioButton(getActivity());
                try{
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                        radioButton.setTextAppearance(R.style.custom_radiobutton);
                    }
                } catch (Exception e){
                }

                radioButton.setText(databean_options.getOptions().get(i).getOption_title());
                radioButton.setId(i+1);
                rprms= new RadioGroup.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);
                radiogrp.addView(radioButton, rprms);
            }
            if (isvoted.equalsIgnoreCase("true")){
                lnradiobtn.setVisibility(View.GONE);
                setProgress();
                lnprogress.setVisibility(View.VISIBLE);
            }else {
                lnradiobtn.setVisibility(View.VISIBLE);
                btnsubmit.setVisibility(View.VISIBLE);
            }
            radiogrp.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
                public void onCheckedChanged(RadioGroup rg, int checkedId) {
                    for(int i=0; i<rg.getChildCount(); i++) {
                        RadioButton btn = (RadioButton) rg.getChildAt(i);
                        if(btn.getId() == checkedId) {
                            pollid=databean_options.getOptions().get(i).getPoll_id();
                            optionid=databean_options.getOptions().get(i).getId();
                            Log.e("poll did",""+pollid+"ids"+optionid);
                            return;
                        }
                    }
                }
            });
        }
    }

    private void setProgress(){
        lnprogress.removeAllViews();
        LinearLayout linearLayout=null;
        lnprogress.removeView(linearLayout);
        ProgressBar progressBar;
        for (int i=0;i<databean_options.getOptions().size();i++){
            LinearLayout.LayoutParams layoutParams=new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);

            CustomRegularText textView=new CustomRegularText(getActivity());
            layoutParams.weight=1;
            layoutParams.setMargins(0,20,0,10);
            textView.setLayoutParams(layoutParams);
            textView.setTextColor(Color.BLACK);
            textView.setTextSize(14);
            textView.setText(databean_options.getOptions().get(i).getOption_title());

           //LinearLayout.LayoutParams layoutParams2=new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
            CustomRegularText text2=new CustomRegularText(getActivity());
            //text2.setLayoutParams(layoutParams);
            text2.setGravity(Gravity.RIGHT);
            text2.setTextSize(14);

            int totalvotes= Integer.parseInt(polls_detailmodel.getData().get(2).getVotes().getTotal_votes());
            if (databean_options.getOptions().get(i).getVotes()!=null){
                int temp=Integer.parseInt(databean_options.getOptions().get(i).getVotes().toString());
                int percentage= (int)((temp * 100.0) / totalvotes);
                text2.setText(""+percentage+"%");
            }else {
                text2.setText(""+0+"%");
            }

            LinearLayout lnhorizontal=new LinearLayout(getActivity());
            lnhorizontal.setOrientation(LinearLayout.HORIZONTAL);
            lnhorizontal.setWeightSum(1);
            lnhorizontal.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT));
            lnhorizontal.addView(textView);
            lnhorizontal.addView(text2);

            progressBar=new ProgressBar(getContext(), null, android.R.attr.progressBarStyleHorizontal);
            progressBar.setIndeterminate(false);
            progressBar.setMinimumHeight(50);
            progressBar.setScaleY(5f);
            //progressBar.setProgressDrawable(R.drawable.progress_bg);
            progressBar.getProgressDrawable().setColorFilter(
                    Color.RED, android.graphics.PorterDuff.Mode.SRC_IN);
            progressBar.setMax(totalvotes);
            progressBar.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT));

            ViewGroup.LayoutParams layout_vertical=new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);

            linearLayout=new LinearLayout(getActivity());
            linearLayout.setOrientation(LinearLayout.VERTICAL);
            linearLayout.setLayoutParams(layout_vertical);
            layoutParams.setMargins(0,0,0,10);
            linearLayout.addView(lnhorizontal);
            linearLayout.addView(progressBar);
            if (databean_options.getOptions().get(i).getVotes()!=null){
                progressBar.setProgress(Integer.parseInt(databean_options.getOptions().get(i).getVotes().toString()));
            } else {
                progressBar.setProgress(0);
            }
            lnprogress.addView(linearLayout);
        }
    }


    public void postVote() {
        //Dialogs.showProDialog(getActivity(),"Loading");
        StringRequest stringRequest = new StringRequest(Request.Method.POST, Apis.Polls_vote ,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.e("log", "" + response);
                        try {
                            JSONObject jsonObject =new JSONObject(response);
                            if (jsonObject.getString("status").equals("200") && jsonObject.getString("message").equalsIgnoreCase("success")){
                                Toast.makeText(getActivity(),"Your vote has been counted,successfully", Toast.LENGTH_SHORT).show();
                                btnsubmit.setEnabled(false);
                                btnsubmit.setText("voted");
                                isvoted="true";
                                getPollsdetail();
                            }else if (jsonObject.getString("message").equalsIgnoreCase("error")){
                                Toast.makeText(getActivity(),jsonObject.getJSONObject("data").getString("message"), Toast.LENGTH_SHORT).show();
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Dialogs.disDialog();
                        Toast.makeText(getActivity(),"No Internet conection", Toast.LENGTH_SHORT).show();
                    }
                })
        {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> map = new HashMap<String, String>();
                map.put("user", SaveSharedPreference.getUserID(getActivity()));
                map.put("token", SaveSharedPreference.getPrefToken(getActivity()));
                map.put("id",pollid);
                map.put("oid",optionid);
                Log.e("mapcomment",""+map);
                return map;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Content-Type", "application/x-www-form-urlencoded");
                return params;
            }
        };
        RequestQueue requestQueue = Volley.newRequestQueue(getActivity());
        requestQueue.add(stringRequest);
    }


    public void isVoted() {
        //Dialogs.showProDialog(getActivity(),"Loading");
        StringRequest stringRequest = new StringRequest(Request.Method.POST, Apis.Polls_Isvote ,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.e("log", "" + response);
                        try {
                            JSONObject jsonObject =new JSONObject(response);
                            if (jsonObject.getString("status").equals("200") && jsonObject.getString("message").equalsIgnoreCase("success")){
                                   if (jsonObject.getJSONObject("data").getString("is_vote").equalsIgnoreCase("true")){
                                    isvoted="true";
                                   }
                            }else if (jsonObject.getString("message").equalsIgnoreCase("error")){
                                Toast.makeText(getActivity(),jsonObject.getJSONObject("data").getString("message"), Toast.LENGTH_SHORT).show();
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        getPollsdetail();
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                      //  Dialogs.disDialog();

                    }
                })
        {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> map = new HashMap<String, String>();
                map.put("user", SaveSharedPreference.getUserID(getActivity()));
                map.put("token", SaveSharedPreference.getPrefToken(getActivity()));
                map.put("id",id);
                return map;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Content-Type", "application/x-www-form-urlencoded");
                return params;
            }
        };
        RequestQueue requestQueue = Volley.newRequestQueue(getActivity());
        requestQueue.add(stringRequest);
    }

    @Override
    public void onClick(View v) {

    }
}
