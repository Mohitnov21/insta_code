package com.abacusdesk.instafeed.instafeed.adapter;

import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Point;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.AsyncTask;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.PagerAdapter;
import android.util.Log;
import android.view.Display;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.abacusdesk.instafeed.instafeed.R;
import com.abacusdesk.instafeed.instafeed.fragments.TouchImageView2;
import com.abacusdesk.instafeed.instafeed.fragments.ZoomImage;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.Target;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by abacusdesk on 2017-09-13.
 */

public class ZoomSlide_Adapter  extends PagerAdapter {

        private ArrayList<String> arrayImages;
        private LayoutInflater inflater;
        private Context context;
        ImageView imageView;
        int positiontemp;
        Bitmap bm1;

       // ArrayList<Bitmap> arrayList=new ArrayList<Bitmap>();
      public ZoomSlide_Adapter(Context context,ArrayList<String> arrayImages,int positiontemp) {
            this.context = context;
            this.positiontemp=positiontemp;
            this.arrayImages = arrayImages;
            inflater = LayoutInflater.from(context);
        }

        @Override
        public int getCount() {
            return arrayImages.size();
        }

        @Override
        public Object instantiateItem(final ViewGroup view, final int position) {
            final View imageLayout = inflater.inflate(R.layout.singleimg_layout, view, false);

            final ImageView imgload=(ImageView)imageLayout.findViewById(R.id.image);
            final FrameLayout frameLayout = (FrameLayout)imageLayout.findViewById(R.id.frame_one);
            final TouchImageView2 touchImageView = new TouchImageView2(context);

            // ZoomImage.img.setVisibility(View.VISIBLE);
            // imgload.setVisibility(View.VISIBLE);

            positiontemp=position;
            MyAsync obj = new MyAsync() {
                @Override
                protected void onPostExecute(Bitmap bmp) {
                    super.onPostExecute(bmp);

                    LinearLayout linearLayout=new LinearLayout(context);
                    FrameLayout.LayoutParams frameParams=new FrameLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
                    frameParams.gravity= Gravity.RIGHT|Gravity.TOP;
                    linearLayout.setLayoutParams(frameParams);

                    int id=21+1;
                    imageView= new ImageView(context);
                    imageView.setImageResource(R.drawable.cancel);
                    imageView.setId(id);

                    LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(30,30);
                    layoutParams.gravity= Gravity.RIGHT|Gravity.TOP;
                    layoutParams.setMargins(20,20,20,20);

                    imageView.setAdjustViewBounds(true);
                    imageView.setScaleType(ImageView.ScaleType.FIT_XY);
                    imageView.setLayoutParams(layoutParams);
                    linearLayout.addView(imageView);

                    linearLayout.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            ((Activity)context).finish();
                        }
                    });

                    try {
                        Log.e("touchimage",""+bm1);
                        Log.e("array",""+arrayImages.size());

                        touchImageView.setImageBitmap(bm1);
                        touchImageView.setMaxZoom(3f);

                        frameLayout.addView(touchImageView);
                        frameLayout.addView(linearLayout);
                        frameLayout.setVisibility(View.VISIBLE);
                       // ZoomImage.img.setVisibility(View.GONE);
                        view.addView(imageLayout);
                        //imgload.setVisibility(View.GONE);

                    } catch(Exception e)
                    {
                        Log.i("error" +e,"error" +e);
                    }

                }
            };
            obj.execute();
            return imageLayout;
        }

    public class MyAsync extends AsyncTask<Void, Void, Bitmap> {
        @Override
        protected Bitmap doInBackground(Void... params) {
            URL url = null;
            try {
                url = new URL(arrayImages.get(positiontemp));
                bm1 = BitmapFactory.decodeStream(url.openConnection().getInputStream());
               // arrayList.add(positiontemp,bm1);
                Log.e("bitmaptemp",""+bm1);

            } catch (FileNotFoundException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }catch (Exception e) {
                e.printStackTrace();
            }
            return null;
        }
    }

        @Override
        public boolean isViewFromObject(View view, Object object) {
            return view == ((FrameLayout) object);
        }

        @Override
        public void destroyItem(ViewGroup container, int position, Object object) {
            container.removeView((FrameLayout) object);
        }
 }