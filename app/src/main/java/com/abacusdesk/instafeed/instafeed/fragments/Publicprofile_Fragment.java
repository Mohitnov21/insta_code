package com.abacusdesk.instafeed.instafeed.fragments;

import android.content.DialogInterface;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.abacusdesk.instafeed.instafeed.NavDrawerActivity;
import com.abacusdesk.instafeed.instafeed.R;
import com.abacusdesk.instafeed.instafeed.Util.Alerts;
import com.abacusdesk.instafeed.instafeed.Util.Apis;
import com.abacusdesk.instafeed.instafeed.Util.SaveSharedPreference;
import com.abacusdesk.instafeed.instafeed.custom_dialog.Dialogs;
import com.abacusdesk.instafeed.instafeed.model.NewsModel;
import com.abacusdesk.instafeed.instafeed.webservice.VolleySingleton;
import com.android.volley.AuthFailureError;
import com.android.volley.NoConnectionError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.bumptech.glide.Glide;
import com.google.gson.Gson;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by ishaan on 6/15/2017.
 */

public class Publicprofile_Fragment extends Fragment{

   ImageView imgprofile,imglogo;
   Button btnfollow;
   TabLayout tabLayout;
   ViewPager viewPager;
   View view;
   TextView txtusername,txt_hashuser;

   public static String  username="",userid="";
   public static Boolean flag=false;
   public static String  Api_temp="";
   public static Boolean isfollow=false;
   public static String  Apitemp_follow="";
   public static String  tolbartitle="";

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.publicprofile_tabfrag, container, false);
        init();
        if (NavDrawerActivity.toolbar!=null) {
            tolbartitle=NavDrawerActivity.toolbar.getTitle().toString();

            NavDrawerActivity.toolbar.setTitle("");
            NavDrawerActivity.toolbar.setLogo(R.drawable.logo_black);
        }

     /* view.setFocusableInTouchMode(true);
        view.requestFocus();
        view.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if (keyCode == KeyEvent.KEYCODE_BACK) {
                    // ViewPagerAdapter.stopVideo(getActivity());
                    NavDrawerActivity.toolbar.setLogo(null);
                    getActivity().getSupportFragmentManager().popBackStack();
                    return true;
                } else {
                    return false;
                }
            }
        });*/

        return view;
    }

    private void init(){
        imglogo=(ImageView)view.findViewById(R.id.img_logo);
        btnfollow=(Button)view.findViewById(R.id.btn_follow);

        txt_hashuser=(TextView)view.findViewById(R.id.txt_hashusername);
        txtusername=(TextView)view.findViewById(R.id.txt_user_name);

        imgprofile=(ImageView)view.findViewById(R.id.img_profile);
        viewPager = (ViewPager)view.findViewById(R.id.viewPager);

        tabLayout = (TabLayout)view.findViewById(R.id.tabLayout);
        tabLayout.setupWithViewPager(viewPager);//setting tab over viewpager

        setupViewPager(viewPager);
        //setupTabIcons();

        if (flag==true){
            btnfollow.setVisibility(View.VISIBLE);
            Api_temp=Apis.public_profile;
        } else {
            Api_temp=Apis.private_profile;
            btnfollow.setVisibility(View.INVISIBLE);
        }
        getprofiledetails();
        btnfollow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
               if (isfollow==true){
                   Apitemp_follow=Apis.unfollow;
                   dofollow();
               }else {
                  Apitemp_follow= Apis.follow;
                  dofollow();

               }
            }
        });

        //Implementing tab selected listener over tablayout
        tabLayout.setOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                viewPager.setCurrentItem(tab.getPosition());//setting current selected item over viewpager
                switch (tab.getPosition()) {
                    case 0:
                       // Log.e("TAG","TAB1");
                        break;
                    case 1:
                       // Log.e("TAG","TAB2");
                        break;
                }
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {
            }
        });

    }

    public void dofollow( ) {
        //Dialogs.showProDialog(getActivity(), "Loading");
        StringRequest stringRequest = new StringRequest(Request.Method.POST,Apitemp_follow,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.e("log", "" + response);
                        resFollow(response);
                        Dialogs.disDialog();
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Dialogs.disDialog();

                    }
                })
        {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> map = new HashMap<String, String>();
                map.put("user", SaveSharedPreference.getUserID(getActivity()));
                map.put("token",SaveSharedPreference.getPrefToken(getActivity()));
                map.put("username",userid);
                Log.e("responsesocial",""+map);
                return map;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Content-Type", "application/x-www-form-urlencoded");
                return params;
            }
        };
        VolleySingleton.getInstance(getActivity()).addToRequestQueue(stringRequest);
    }

  /*  @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }


    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {

        menu.findItem(R.id.menu_comment).setVisible(false);
        menu.findItem(R.id.menu_share).setVisible(false);;
        menu.findItem(R.id.action_form).setVisible(false);
        menu.findItem(R.id.action_settings).setVisible(false);
        menu.findItem(R.id.menu_bookmark).setVisible(false);
        super.onCreateOptionsMenu(menu, inflater);
    }*/

    /*  {
        "status": 200,
            "message": "success",
            "data": {
        "is_following": false
    }
    }*/

    private void resFollow(String response )  {
        try {
            JSONObject jsonObject= new JSONObject(response);
            if (Apis.isfollow.equals(Apitemp_follow)){
                if (jsonObject.getString("status").equals("200")&& jsonObject.getString("message").equals("success")){
                    isfollow=jsonObject.getJSONObject("data").getBoolean("is_following");
                    if (isfollow==true){
                        btnfollow.setText("UnFollow");
                    }else {
                        btnfollow.setText("follow");
                    }
                    Log.e("isfollow",""+isfollow);
                }
            }else if(Apis.unfollow.equals(Apitemp_follow)){
                if (jsonObject.getString("status").equals("200")&& jsonObject.getString("message").equals("success")){
                    isfollow=false;
                    btnfollow.setText("follow");
                    Log.e("unfollow",""+isfollow);
                }
            }else {
                if (jsonObject.getString("status").equals("200")&& jsonObject.getString("message").equals("success")){
                    isfollow=true;
                    btnfollow.setText("UnFollow");
                    Log.e("follow",""+isfollow);
              } else {
                    Log.e("error",""+jsonObject.getString("message"));
                }
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public void getprofiledetails() {
       // Dialogs.showProDialog(getActivity(), "Loading");
        StringRequest stringRequest = new StringRequest(Request.Method.POST,Api_temp,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.e("log", "" + response);
                        getResponse(response);
                        Dialogs.disDialog();
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Dialogs.disDialog();
                    }
                })
        {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> map = new HashMap<String, String>();
                if (flag==true){
                    map.put("username",Publicprofile_Fragment.username);
                } else {
                    map.put("user", SaveSharedPreference.getUserID(getActivity()));
                    map.put("token",SaveSharedPreference.getPrefToken(getActivity()));
                }
                Log.e("responsesocial",""+map);
                return map;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Content-Type", "application/x-www-form-urlencoded");
                return params;
            }
        };
        VolleySingleton.getInstance(getActivity()).addToRequestQueue(stringRequest);
    }

    private void getResponse(String response )  {
        try {
            JSONObject jsonObject= new JSONObject(response);
            if (jsonObject.getString("status").equals("200")&& jsonObject.getString("message").equals("success")){
                if (!jsonObject.getJSONObject("data").equals("null")){
                    txt_hashuser.setText("#"+jsonObject.getJSONObject("data").getString("username"));
                    username=jsonObject.getJSONObject("data").getString("username");
                    userid=jsonObject.getJSONObject("data").getString("user_id");
                    if (!jsonObject.getJSONObject("data").getString("first_name").equals("null")){
                        txtusername.setText(jsonObject.getJSONObject("data").getString("first_name"));
                    }
                    if (!jsonObject.getJSONObject("data").getString("last_name").equals("null")){
                        txtusername.setText(jsonObject.getJSONObject("data").getString("first_name")+" "+jsonObject.getJSONObject("data").getString("last_name"));
                    }
                    if (!jsonObject.getJSONObject("data").getString("avatar").equals("null")){
                        try {
                            imglogo.setVisibility(View.INVISIBLE);
                            Glide.with(getActivity()).load(jsonObject.getJSONObject("data").getString("avatar")).error(R.drawable.user).into(imgprofile);
                        }catch (Exception e){
                            Log.e("exception",""+e);
                        }
                                      }
                }
            }else {
               // Toast.makeText(getActivity(),jsonObject.getString("message"),Toast.LENGTH_SHORT).show();
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        if (flag==true){
            Apitemp_follow=Apis.isfollow;
            dofollow();
        }
    }
    //Setting View Pager

    private void setupViewPager(ViewPager viewPager) {
        Publicprofile_Fragment.ViewPagerAdapter adapter =new ViewPagerAdapter(getChildFragmentManager());
        adapter.addFrag(new frag_instanews(),"News");
        adapter.addFrag(new Frag_instaTalents(),"Talents");
        adapter.addFrag(new frag_instaexplore(),"Lifestyle");
        adapter.addFrag(new frag_instaopinions(),"Opinions");

       // adapter.addFrag(new frag_instablog(),"Blogs");
       // adapter.addFrag(new frag_instavoice()," Petitions");
       //adapter.addFrag(new Frag_instaPolls(),"Polls");

        viewPager.setAdapter(adapter);
        adapter.notifyDataSetChanged();
    }


//View Pager fragments setting adapter class
class ViewPagerAdapter extends FragmentPagerAdapter {
    private final List<Fragment> mFragmentList = new ArrayList<>();//fragment arraylist
    private final List<String> mFragmentTitleList = new ArrayList<>();//title arraylist

    public ViewPagerAdapter(FragmentManager manager) {
        super(manager);
    }

    @Override
    public Fragment getItem(int position) {
        return mFragmentList.get(position);
    }

    @Override
    public int getCount() {
        return mFragmentList.size();
    }
    //adding fragments and title method
    public void addFrag(Fragment fragment, String title) {
        mFragmentList.add(fragment);
        mFragmentTitleList.add(title);
    }

    @Override
    public CharSequence getPageTitle(int position) {
        return mFragmentTitleList.get(position);
    }
  }

}
